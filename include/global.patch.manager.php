<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
// Patch Management Framework

function ApplyGlobalPatch($relPath) 
{ 
	if (file_exists("$relPath/include/global.patch.install.php"))
	{
		require_once("$relPath/include/global.patch.install.php");
		InstallGlobalPatch($relPath);
		
		unlink("$relPath/include/global.patch.install.php");
	}
}

?>
