<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/getheader.php');
require_once('../../include/db_functions.php');
$bulan = "";
if (isset($_REQUEST['bulan']))
	$bulan = $_REQUEST['bulan'];
	
$tahun = "";
if (isset($_REQUEST['tahun']))
	$tahun = $_REQUEST['tahun'];
	
$namabulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");	
OpenDb();
$sql = "SELECT departemen FROM tahunajaran t, kelas k, siswa s WHERE s.nis='".SI_USER_ID()."' AND s.idkelas=k.replid AND k.idtahunajaran=t.replid";
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$departemen = $row[0];

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../../style/style.css">
</HEAD>

<BODY>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">
  	<?php echo GetHeader($departemen)?>
<center>
  <font size="4"><strong>DAFTAR AGENDA SISWA</strong></font><br />
 </center><br /><br />

<br />
Periode : <?php echo$namabulan[$bulan-1]?> <?php echo$tahun?><br>
Siswa : <?php echo sI_USER_NAME()?><br><br>
<table width="100%" border="1" cellspacing="0" class='tab'>
  <tr>
    <th height="30" class="header" scope="row">Tanggal</th>
    <td height="30" class="header">Agenda</td>
    
  </tr>
  <?php
  OpenDb();
  $sql="SELECT tanggal FROM $g_db_umum.agenda WHERE nis='".SI_USER_ID()."' AND YEAR(tanggal)='$tahun' AND MONTH(tanggal)='$bulan' GROUP BY tanggal";
  $result=QueryDb($sql);
  if (@mysql_num_rows($result)>0){
  while ($row=@mysql_fetch_array($result)){
  
  ?>
  <tr>
    <td height="25" scope="row" valign="middle">&nbsp;&nbsp;<?php echo LongDateFormat($row['tanggal'])?></th>
    <?php 
	$sql1="SELECT * FROM $g_db_umum.agenda WHERE nis='".SI_USER_ID()."' AND tanggal='$row[tanggal]'";
  	$result1=QueryDb($sql1);
	$i=0;
	while ($row1=@mysql_fetch_array($result1)){
		$judul[$i]=$row1['judul'];
		$replid[$i]=$row1['replid'];	
	$i++;
	}
	?>
    <td height="25" align="left">
	<?php 
	for ($x=0;$x<=$i-1;$x++){
		?>
		<img title="Ubah !" src="../../images/ico/titik.png" border="0" height="10" width="10"/>&nbsp;<?php echo$judul[$x]?><br>
		<?php
	}
	?>
    </td>
    
  </tr>
  <?php 
  } } else { ?>
  <tr>
    <th height="25" scope="row" colspan="2" align="center">Tidak ada Agenda untuk bulan <?php echo$namabulan[$bulan-1]?></th>
  </tr>
  <?php 
  }
  CloseDb(); ?>
</table>
</td>
<tr>
</table>
</body></HTML>