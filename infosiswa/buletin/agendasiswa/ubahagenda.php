<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/getheader.php');
require_once('../../include/db_functions.php');
$replid=$_REQUEST['replid'];

if (isset($_REQUEST['simpan'])){
OpenDb();
$judul=$_REQUEST['judul'];
$komentar=$_REQUEST['komentar'];
$tgl=explode("-",$_REQUEST['tanggal']);
$tanggal=$tgl[2]."-".$tgl[1]."-".$tgl[0];
$idguru=SI_USER_ID();
$sql="UPDATE $g_db_umum.agenda SET tanggal='$tanggal',judul='$judul',komentar='$komentar' WHERE replid='$_REQUEST[replid]'";
//echo $sql;
//exit;
$result=QueryDb($sql);
CloseDb();
if ($result){
?>
<script language="javascript">
	opener.get_fresh('<?php echo$tgl[1]?>','<?php echo$tgl[2]?>');
	window.close();
</script>
<?php
	}
}
OpenDb();
$sql="SELECT * FROM $g_db_umum.agenda WHERE replid='$replid'";
$result=QueryDb($sql);
$row=@mysql_fetch_array($result);
CloseDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../style/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../style/calendar-win2k-1.css">
<title>Ubah Agenda Siswa</title>
<script type="text/javascript" src="../../script/calendar.js"></script>
<script type="text/javascript" src="../../script/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../script/calendar-setup.js"></script>
<script language="javascript" type="text/javascript" src="../../script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "silver",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",		
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,fullscreen,print,|,cut,copy,paste,pastetext,|,search,replace,|,bullist,numlist,|,hr,removeformat,|,sub,sup,|,charmap,image",
		theme_advanced_buttons3 : "tablecontrols",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		content_css : "../../style/content.css"
	});
	</script>
</head>
<body leftmargin="0" topmargin="0">
<form name="agenda" id="agenda" action="ubahagenda.php" method="POST" onSubmit="return validate()">
<table width="100%" border="0" cellspacing="5" cellpadding="2">
  <tr>
    <td colspan="2" scope="row"><strong><font size="2" color="#999999">Ubah Agenda :</font></strong><br /><br /></td>
  </tr>
  <tr>
    <td scope="row"><div align="left"><strong>Tanggal</strong></div></td>
    <td scope="row"><div align="left">
      <input title="Klik untuk membuka kalender !" type="text" name="tanggal" id="tanggal" size="25" readonly="readonly" class="disabled" value="<?php echo RegularDateFormat($row[tanggal]); ?>"/>
    <img title="Klik untuk membuka kalender !" src="../../images/ico/calendar_1.png" name="btntanggal" width="16" height="16" border="0" id="btntanggal"/></div></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" scope="row"><div align="left"><strong>Judul</strong></div></td>
    <td bgcolor="#FFFFFF" scope="row"><div align="left">
      <input type="text" name="judul" id="judul" size="50" value="<?php echo$row[judul]?>" />
	  <input type="hidden" name="replid" id="replid" size="50" value="<?php echo$replid?>" />
    </div></td>
  </tr>
  <tr>
    <th colspan="2" valign="top" scope="row"><fieldset><legend>Deskripsi</legend><textarea name="komentar" rows="20" id="komentar"><?php echo$row[komentar]?>
      </textarea></fieldset></th>
    </tr>
  <tr>
    <th colspan="2" scope="row" align="center"><input title="Simpan agenda !" type="submit" class="but" name="simpan" id="simpan" value="Simpan" />&nbsp;&nbsp;
    <input title="Tutup !" type="button" class="but" onClick="window.close();" name="tutup" id="tutup" value="Tutup" /></th>
  </tr>
</table>
</form>

</body>
<script type="text/javascript">
  Calendar.setup(
    {
	  inputField  : "tanggal",         
      ifFormat    : "%d-%m-%Y",  
      button      : "btntanggal"    
    }
   );
   Calendar.setup(
    {
	  inputField  : "tanggal",      
      ifFormat    : "%d-%m-%Y",   
      button      : "tanggal"     
    }
   );
  
</script>
</html>