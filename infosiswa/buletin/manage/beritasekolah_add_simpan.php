<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/getheader.php');
require_once('../../include/db_functions.php');
require_once('../../include/sessionchecker.php');

$sender=$_REQUEST['sender'];
if ($sender=="tambah")
{
   OpenDb();
   $jam=date(H).":".date(i).":00";
   $judul=CQ($_REQUEST['judul']);
   $tgl=explode("-",$_REQUEST['tanggal']);
   $tanggal=$tgl[2]."-".$tgl[1]."-".$tgl[0];
   $jenis=$_REQUEST['jenisberita'];
   $abstrak=CQ($_REQUEST['abstrak']);
   $isi=$_REQUEST['isi'];
   $isi=str_replace("'", "#sq;", $isi);
   $idpengirim=SI_USER_ID();
   $sql1="INSERT INTO $g_db_umum.beritasekolah SET judul='$judul', tanggal='".$tanggal." ".$jam."', jenisberita='$jenis',abstrak='$abstrak', isi='$isi', idpengirim='$idpengirim'";
   $result1=QueryDb($sql1);
   CloseDb();?>
   <script language="javascript">
      parent.beritasekolah_header.lihat();
   </script>
<?php
}
elseif ($sender=="ubah")
{
	OpenDb();
	$page=(int)$_REQUEST['page'];
	$bulan=$_REQUEST['bulan'];
	$tahun=$_REQUEST['tahun'];
	
	//KAlo dari ubah berita guru================================================================================================================================
	$judul=CQ($_REQUEST['judul']);
	$tgl=explode("-",$_REQUEST['tanggal']);
	$tanggal=$tgl[2]."-".$tgl[1]."-".$tgl[0];
	$jenisberita=$_REQUEST['jenisberita'];
	$abstrak=CQ($_REQUEST['abstrak']);
	$isi=$_REQUEST['isi'];
   $isi=str_replace("'", "#sq;", $isi);
	$idpengirim=SI_USER_ID();
	$replid=$_REQUEST['replid'];
	
	$sql18="UPDATE $g_db_umum.beritasekolah SET judul='$judul', tanggal='$tanggal', jenisberita='$jenisberita', abstrak='$abstrak', isi='$isi' WHERE replid='$replid'";
	$result18=QueryDb($sql18);
   CloseDb(); ?>
   <script language="javascript">
   document.location.href="beritasekolah_footer.php?page=<?php echo$page?>&tahun=<?php echo$tahun?>&bulan=<?php echo$bulan?>";
   </script>
<?php
}
?>