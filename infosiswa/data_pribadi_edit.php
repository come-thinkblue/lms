<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/rupiah.php');
require_once('../include/sessionchecker.php');

if (isset($_REQUEST['Simpan'])) {
		$nama=$_REQUEST['nama'];
		$panggilan=$_REQUEST['panggilan'];
		$kelamin=$_REQUEST['kelamin'];
		$tmplahir=$_REQUEST['tmplahir'];
		$tgllahir=$_REQUEST['tgllahir'];
		$agama=$_REQUEST['agama'];
		$warga=$_REQUEST['warga'];
		$anakke=$_REQUEST['anakke'];
		$jsaudara=$_REQUEST['jsaudara'];
		$kondisi=$_REQUEST['kondisi'];
		
		
		$status=$_REQUEST['status'];
		$bahasa=$_REQUEST['bahasa'];
		$alamatsiswa=$_REQUEST['alamatsiswa'];
		$telponsiswa=$_REQUEST['telponsiswa'];
		$hpsiswa=$_REQUEST['hpsiswa'];
		$emailsiswa=$_REQUEST['emailsiswa'];
		$berat=$_REQUEST['berat'];
		$tinggi=$_REQUEST['tinggi'];
		$darah=$_REQUEST['darah'];
		$kesehatan=$_REQUEST['kesehatan'];
		
		$asalsekolah=$_REQUEST['asalsekolah'];
		$ketsekolah=$_REQUEST['ketsekolah'];
		$namaayah=$_REQUEST['namaayah'];
		$namaibu=$_REQUEST['namaibu'];
		$pendidikanayah=$_REQUEST['pendidikanayah'];
		$pendidikanibu=$_REQUEST['pendidikanibu'];
		$pekerjaanayah=$_REQUEST['pekerjaanayah'];
		$pekerjaanibu=$_REQUEST['pekerjaanibu'];
		$penghasilanayah=$_REQUEST['penghasilanayah'];
		$penghasilanibu=$_REQUEST['penghasilanibu'];
		
		$emailayah=$_REQUEST['emailayah'];
		$emailibu=$_REQUEST['emailibu'];
		$wali=$_REQUEST['wali'];
		$alamatortu=$_REQUEST['alamatortu'];
		$telponortu=$_REQUEST['telponortu'];
		$hportu=$_REQUEST['hportu'];
		
		$hportu=$_REQUEST['nis'];
	
	OpenDb();
	
		OpenDb();
		
		$nama3 ="update siswa set nama='$nama', panggilan='$panggilan', kelamin='$kelamin', tmplahir='$tmplahir',
				tgllahir='$tgllahir', agama='$agama', warga='$warga', anakke='$anakke' ,
				jsaudara='$jsaudara', kondisi='$kondisi', status='$status', bahasa='$bahasa',
				alamatsiswa='$alamatsiswa', telponsiswa='$telponsiswa', hpsiswa='$hpsiswa', emailsiswa='$emailsiswa' 
				, berat='$berat', tinggi='$tinggi', darah='$darah', kesehatan='$kesehatan',
				asalsekolah='$asalsekolah', ketsekolah='$ketsekolah', namaayah='$namaayah', namaibu='$namaibu'
				, pendidikanayah='$pendidikanayah', pendidikanibu='$pendidikanibu', pekerjaanayah='$pekerjaanayah', pekerjaanibu='$pekerjaanibu',
				penghasilanayah='$penghasilanayah', penghasilanibu='$penghasilanibu', emailayah='$emailayah', emailibu='$emailibu'
				, wali='$wali', alamatortu='$alamatortu', telponortu='$telponortu', hportu='$hportu'
				where nis='$nis'";
		$result = QueryDb($nama3);
		CloseDb();
	
		if ($result) {
			?>
			<script language="javascript">
				
				window.close();
				header("Location:infosiswa_content.php");
			</script> 
		<?php
	}
	
}
?>

<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/cal2.js"></script>
<script language="javascript" src="../script/cal_conf2.js"></script>
<script language="javascript" src="../script/calendar.js"></script>
<script language="javascript" src="../script/calendar-setup.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>

<?php
$nis=$_REQUEST['nis'];

OpenDb();

$sql="SELECT c.nis, c.nama, c.panggilan, c.tahunmasuk, c.idkelas, c.suku, c.agama, c.status, c.kondisi, c.kelamin, c.tmplahir, DAY(c.tgllahir) AS tanggal, MONTH(c.tgllahir) AS bulan, YEAR(c.tgllahir) AS tahun, c.tgllahir, c.warga, c.anakke, c.jsaudara, c.bahasa, c.berat, c.tinggi, c.darah, c.foto, c.alamatsiswa, c.kodepossiswa, c.telponsiswa, c.hpsiswa, c.emailsiswa, c.kesehatan, c.asalsekolah, c.ketsekolah, c.namaayah, c.namaibu, c.almayah, c.almibu, c.pendidikanayah, c.pendidikanibu, c.pekerjaanayah, c.pekerjaanibu, c.wali, c.penghasilanayah, c.penghasilanibu, c.alamatortu, c.telponortu, c.hportu, c.emailayah, c.emailibu, c.alamatsurat, c.keterangan,t.departemen, t.tahunajaran, k.kelas, c.replid as rep  FROM siswa c, kelas k, tahunajaran t WHERE c.nis='$nis' AND k.replid = c.idkelas AND k.idtahunajaran = t.replid";
$result=QueryDb($sql);
$row_siswa = mysql_fetch_array($result); 
CloseDb();

?>
<!--<body leftmargin="0" topmargin="0">-->
<center><strong>EDIT DATA PRIBADI</strong></center>
<form action="data_pribadi_edit.php" name="main" onSubmit="return validate()">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr height="58">
			<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
			<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
				<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
				.: Tambah Data Siswa :.
				</div>
			</td>
			<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;
			</td>
		</tr>
		<tr height="150">
			<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
			<td width="0" style="background-color:#FFFFFF">
			<!-- CONTENT GOES HERE //--->
				<form name="main" method="post" enctype="multipart/form-data" onSubmit="return validate();">
					<input type="hidden" name="idkelas" value="<?php echo$idkelas?>">
					<input type="hidden" name="idtahunajaran" value="<?php echo$idtahunajaran?>">
					<input type="hidden" name="idtingkat" value="<?php echo$idtingkat?>">
					<input type="hidden" name="departemen" value="<?php echo$departemen?>">

				<table width="100%" border="0" cellspacing="0">
					<tr>
						<td width="45%" valign="top"><!-- Kolom Kiri-->      
							<table width="100%" border="0" cellspacing="0">
								<tr>
									<td height="30" colspan="3">
										<font size="2" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="Gray"><strong>Data Pribadi Siswa</strong></font>
										<!--hr width="300" style="line-height:1px; border-style:dashed" align="left" color="#000000"/-->
										<div style="border-bottom:1px dashed #666666; border-width:thinl; margin-bottom:5px; margin-top:3px;"></div>
									</td>
								</tr>
								<tr>
									<td width="22%"><strong>Angkatan</strong></td>
									<td colspan="2">  
										<select name="idangkatan" id="idangkatan" style="width:195px;" onKeyPress="return focusNext('tahunmasuk', event)" onFocus="panggil('idangkatan')" onBlur="unfokus('idangkatan')" >    		
										<?php // Olah untuk combo angkatan
										$sql_angkatan="SELECT * FROM $g_db_akademik.angkatan WHERE aktif=1 AND departemen='$departemen' ORDER BY replid DESC";
										$result_angkatan=QueryDB($sql_angkatan);
										while ($row_angkatan = mysql_fetch_array($result_angkatan)) {
										if ($idangkatan = "") 
										$idangkatan = $row_angkatan['angkatan'];
										?>
										<option value="<?php echo$row_angkatan['replid']?>"<?php echo StringIsSelected($row_angkatan['replid'], $idangkatan)?>><?php echo$row_angkatan['angkatan']?></option>
										<?php
										} 
										// Akhir Olah Data angkatan
										?>
										</select>        
									</td>
								</tr>
								<tr>
									<td><strong>Tahun Masuk</strong></td>
									<td colspan="2">
										<input type="text" name="tahunmasuk" id="tahunmasuk" size="5" maxlength="4" onFocus="showhint('Tahun masuk tidak boleh kosong!', this, event, '120px');panggil('tahunmasuk')" value="<?php echo$tahunmasuk?>"  onKeyPress="return focusNext('nis', event)" onBlur="unfokus('tahunmasuk')"/>
									</td>
								</tr>
								<tr>
									<td><strong>N I S</strong></td>
									<td colspan="2">
										<input type="text" name="nis" id="nis" size="20" maxlength="20" class="ukuran" value="<?php echo$nis?>"  onKeyPress="return focusNext('nisn', event)" onFocus="showhint('NIS Siswa tidak boleh kosong!', this, event, '120px');panggil('nis')" onBlur="unfokus('nis')"/>        </td>
								</tr>
								<tr>
									<td>N I S N</td>
									<td colspan="2">
										<input type="text" name="nisn" id="nisn" size="50" maxlength="50" class="ukuran" value="<?php echo$nisn?>"  onKeyPress="return focusNext('nama', event)" />
									</td>
								</tr>
								<tr>
									<td><strong>Nama</strong></td>
									<td colspan="2">
										<input type="text" name="nama" id="nama" size="30" maxlength="100" onFocus="showhint('Nama Lengkap Siswa tidak boleh kosong!', this, event, '120px');panggil('nama')"  value="<?php echo$nama?>"  onKeyPress="return focusNext('panggilan', event)" onBlur="unfokus('nama')"/></td>
								</tr>
								<tr>
									<td>Panggilan</td>
									<td colspan="2">
										<input type="text" name="panggilan" id="panggilan" size="30" maxlength="30" onFocus="showhint('Nama Panggilan tidak boleh lebih dari 30 karakter!', this, event, '120px');panggil('panggilan')"  value="<?php echo$panggilan?>"  onKeyPress="return focusNext('kelamin', event)" onBlur="unfokus('panggilan')"/></td>
								</tr>
								<tr>
									<td><strong>Jenis Kelamin</strong></td>
									<td colspan="2">&nbsp;
										<input type="radio" id="kelamin2" name="kelamin" value="l"  
								<?php if ($kelamin=="1")
									echo "checked";
									else
									echo "checked";
				?>
	   onkeypress="return focusNext('tmplahir', event)" />
	  Laki-laki&nbsp;&nbsp;<input type="radio" id="kelamin" name="kelamin" value="p"
	<?php if ($kelamin=="p")
								echo "checked"; ?>  onKeyPress="return focusNext('tmplahir', event)"/>&nbsp;Perempuan</td>
								</tr>
								<tr>
									<td><strong>Tempat Lahir</strong></td>
									<td colspan="2"><input type="text" name="tmplahir" id="tmplahir" size="30" maxlength="50" onFocus="showhint('Tempat Lahir tidak boleh kosong!', this, event, '120px');panggil('tmplahir')"  value="<?php echo$tmplahir?>"  onKeyPress="return focusNext('tgllahir', event)" onBlur="unfokus('tmplahir')"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr>
									<td><strong>Tanggal Lahir</strong></td>
									<td colspan="2">
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td>
													<div id="tgl_info">
														<select name="tgllahir" id="tgllahir" onKeyPress="return focusNext('blnlahir', event)" onFocus="panggil('tgllahir')" onBlur="unfokus('tgllahir')">
															<option value="">[Tgl]</option>  
															<?php 	for ($tgl=1;$tgl<=$n;$tgl++){ ?>
															<option value="<?php echo$tgl?>" <?php echo IntIsSelected($tgllahir, $tgl)?>><?php echo$tgl?></option>        
														<?php	}	?>
														</select>
													</div>                
												</td>
												<td>
													<select name="blnlahir" id="blnlahir"  onKeyPress="return focusNext('thnlahir', event)" onChange="change_bln()" onFocus="panggil('blnlahir')" onBlur="unfokus('blnlahir')">
														<option value="">[Bulan]</option>  
															<?php 	for ($i=1;$i<=12;$i++) { ?>
														<option value="<?php echo$i?>" <?php echo IntIsSelected($blnlahir, $i)?>><?php echo NamaBulan($i)?></option>	
														<?php	} ?>
													</select>
													<input type="text" name="thnlahir" id="thnlahir" size="5" maxlength="4" onFocus="showhint('Tahun Lahir tidak boleh kosong!', this, event, '120px');panggil('thnlahir')"  value="<?php echo$thnlahir?>"  onKeyPress="return focusNext('agama', event)" onBlur="unfokus('thnlahir')"/>                
												</td>
											</tr>
										</table>    	
									</td>
								</tr>
								<tr>
									<td><strong>Agama</strong></td>
									<td colspan="2">
										<div id="InfoAgama">
											<select name="agama" id="agama" class="ukuran"  onKeyPress="return focusNext('suku', event)" onFocus="panggil('agama')" onBlur="unfokus('agama')">
												<option value="">[Pilih Agama]</option>
												<?php // Olah untuk combo agama
												$sql_agama="SELECT replid,agama,urutan FROM $g_db_akademik.agama ORDER BY urutan";
												$result_agama=QueryDB($sql_agama);
												while ($row_agama = mysql_fetch_array($result_agama)) {
												?>
												<option value="<?php echo$row_agama['agama']?>"<?php echo StringIsSelected($row_agama['agama'],$agama)?>><?php echo$row_agama['agama']?></option>
												<?php
												} 
												// Akhir Olah Data agama
												?>
											</select>
											<?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
											<img src="../images/ico/tambah.png" onClick="tambah_agama();" onMouseOver="showhint('Tambah Agama!', this, event, '50px')" />    
											<?php } ?>
										</div>    	
									</td>
								</tr>
								<tr>
									<td><strong>Suku</strong></td>
									<td colspan="2">
										<div id="InfoSuku">
											<select name="suku" id="suku" class="ukuran"  onkeypress="return focusNext('status', event)" onFocus="panggil('suku')" onBlur="unfokus('suku')">
												<option value="">[Pilih Suku]</option>
												<?$sql_suku="SELECT suku,urutan,replid FROM $g_db_akademik.suku ORDER BY urutan";
													$result_suku=QueryDB($sql_suku);
													while ($row_suku = mysql_fetch_array($result_suku))
													{
												?>
												<option value="<?php echo$row_suku['suku']?>"<?php echo StringIsSelected($row_suku['suku'], $suku)?> >
												<?php echo$row_suku['suku']?>
												</option>
												<?}   ?>
											</select>
										<?phpif (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
										<img src="../images/ico/tambah.png" onClick="tambah_suku();" onMouseOver="showhint('Tambah Suku!', this, event, '50px')" />
										<?} ?>
										</div>
									</td>
								</tr>
								<tr>
									<td><strong>Status</strong></td>
									<td colspan="2">
										<div id="InfoStatus">
											<select name="status" id="status" class="ukuran"  onKeyPress="return focusNext('kondisi', event)" onFocus="panggil('status')" onBlur="unfokus('status')">
												<option value="">[Pilih Status]</option>
												<?php // Olah untuk combo status
												$sql_status="SELECT replid,status,urutan FROM $g_db_akademik.statussiswa ORDER BY urutan";
												$result_status=QueryDB($sql_status);
												while ($row_status = mysql_fetch_array($result_status)) {
												//	if ($status=="")
												//		$status=$row_status['status'];
												?>
												<option value="<?php echo$row_status['status']?>"<?php echo StringIsSelected($row_status['status'],$status)?> ><?php echo$row_status['status']?></option>
												<?php
												} 
		// Akhir Olah Data status
												?>
											</select>
											<?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
											<img src="../images/ico/tambah.png" onClick="tambah_status();" onMouseOver="showhint('Tambah Status!', this, event, '50px')"/>    
											<?php } ?>
										</div> 
									</td>
								</tr>
								<tr>
									<td><strong>Kondisi</strong></td>
									<td colspan="2">
										<div id="InfoKondisi">
											<select name="kondisi" id="kondisi" class="ukuran"  onKeyPress="return focusNext('warga', event)" onFocus="panggil('kondisi')" onBlur="unfokus('kondisi')">
												<option value="">[Pilih Kondisi]</option>
												<?php // Olah untuk combo kondisi
												$sql_kondisi="SELECT kondisi,urutan FROM $g_db_akademik.kondisisiswa ORDER BY urutan";
												$result_kondisi=QueryDB($sql_kondisi);
												while ($row_kondisi = mysql_fetch_array($result_kondisi)) {
												//	if ($kondisi=="")
												//		$kondisi=$row_kondisi['kondisi'];
												?>
												<option value="<?php echo$row_kondisi['kondisi']?>" <?php echo StringIsSelected($row_kondisi['kondisi'],$kondisi)?> ><?php echo$row_kondisi['kondisi']?>
												</option>
												<?php
												} 
									// Akhir Olah Data kondisi
												?>
											</select>
											<?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
											<img src="../images/ico/tambah.png" onClick="tambah_kondisi();" onMouseOver="showhint('Tambah Kondisi!', this, event, '50px')"/>
											<?php } ?>
										</div>
									</td>
								</tr>
								<tr>
									<td>Kewarganegaraan</td>
									<td colspan="2">
										<input type="radio" id="warga" name="warga" value="WNI" 
											<?php 	if ($warga=="WNI" || $warga=="")
											echo "checked";
												else
											echo "checked";
											?> onKeyPress="return focusNext('urutananak', event)"
									/>&nbsp;WNI&nbsp;&nbsp;
											<input type="radio" id="warga" name="warga" value="WNA"
											<?php 	if ($warga=="WNA")
											echo "checked"
											?> onKeyPress="return focusNext('urutananak', event)"
									/>&nbsp;WNA</td>
								</tr>
								<tr>
									<td>Anak ke</td>
									<td colspan="2">
										<input type="text" name="urutananak" id="urutananak" size="3" maxlength="3" onFocus="showhint('Urutan anak tidak boleh lebih dari 3 angka!', this, event, '120px');panggil('urutananak')" value="<?php echo$urutananak ?>"  onKeyPress="return focusNext('jumlahanak', event)" onBlur="unfokus('urutananak')"/>&nbsp;dari&nbsp;
										<input type="text" name="jumlahanak" id="jumlahanak" size="3" maxlength="3" onFocus="showhint('Jumlah saudara tidak boleh lebih dari 3 angka!', this, event, '120px');panggil('jumlahanak')"  value="<?php echo$jumlahanak?>"  onKeyPress="return focusNext('bahasa', event)" onBlur="unfokus('jumlahanak')"/>&nbsp;bersaudara</td>
								</tr>
								<tr>
									<td>Bahasa</td>
									<td colspan="2">
										<input type="text" name="bahasa" id="bahasa" size="30" maxlength="60" onFocus="showhint('Bahasa anak tidak boleh lebih dari 60 karakter!', this, event, '120px');panggil('bahasa')"  value="<?php echo$bahasa?>"  onKeyPress="return focusNext('alamatsiswa', event)" onBlur="unfokus('bahasa')"/></td>
								</tr>
								<tr>
									<td>Foto</td>
									<td colspan="2"><input type="file" name="file_data" id="file_data" size="25" style="width:215px" value="<?php echo$file_data?>"/></td>
								</tr>
								<tr>
									<td valign="top">Alamat</td>
									<td colspan="2">
										<textarea name="alamatsiswa" class="Ukuranketerangan" id="alamatsiswa" onFocus="showhint('Alamat siswa tidak boleh lebih dari 255 karakter!', this, event, '120px');panggil('alamatsiswa')" onKeyUp="change_alamat()"  onKeyPress="return focusNext('kodepos', event)"  onblur="unfokus('alamatsiswa')"><?php echo$alamatsiswa?></textarea></td>
								</tr>
								<tr>
									<td>Kode Pos</td>
									<td colspan="2">
										<input type="text" name="kodepos" id="kodepos" size="5" maxlength="8" onFocus="showhint('Kodepos tidak boleh lebih dari 8 angka!', this, event, '120px');panggil('kodepos')" value="<?php echo$kodepos?>"  onKeyPress="return focusNext('telponsiswa', event)" onBlur="unfokus('kodepos')"/></td>
								</tr>
								<tr>
									<td>Telepon</td>
									<td colspan="2">
										<input type="text" name="telponsiswa" id="telponsiswa" class="ukuran" maxlength="20" onFocus="showhint('Nomor Telepon tidak boleh lebih dari 20 angka!', this, event, '120px');panggil('telponsiswa')" value="<?php echo$telponsiswa?>"  onKeyPress="return focusNext('hpsiswa', event)" onBlur="unfokus('telponsiswa')"/></td>
								</tr>
								<tr>
									<td>Handphone</td>
									<td colspan="2">
										<input type="text" name="hpsiswa" id="hpsiswa" class="ukuran" maxlength="20" onFocus="showhint('Nomor ponsel tidak boleh lebih dari 20 angka!', this, event, '120px');panggil('hpsiswa')" value="<?php echo$hpsiswa?>" onKeyPress="return focusNext('emailsiswa', event)" onBlur="unfokus('hpsiswa')"/>      </td>
								</tr>
								<tr>
									<td>Email</td>
									<td colspan="2">
										<input type="text" name="emailsiswa" id="emailsiswa" size="30" maxlength="100" onFocus="showhint('Alamat email tidak boleh lebih dari 100 karakter!', this, event, '120px');panggil('emailsiswa')"  value="<?php echo$emailsiswa?>"  onKeyPress="return focusNext('dep_asal', event)" onBlur="unfokus('emailsiswa')"/></td>
								</tr>
								<tr>
									<td rowspan="2" valign="top">Asal Sekolah</td>
									<td>
										<div id="depInfo">
										<select name="dep_asal" id="dep_asal"  onKeyPress="return focusNext('sekolah', event)" onChange="change_departemen(0)" style="width:150px;" onFocus="panggil('dep_asal')" onBlur="unfokus('dep_asal')"> 
											<option value="">[Pilih Departemen]</option>
										<?php // Olah untuk combo departemen
										$sql_departemen="SELECT DISTINCT departemen FROM $g_db_akademik.asalsekolah ORDER BY departemen";   
										$result_departemen=QueryDB($sql_departemen);
										while ($row_dep = mysql_fetch_array($result_departemen)) {
										
										?>
										<option value="<?php echo$row_dep['departemen']?>" <?php echo StringIsSelected($row_dep['departemen'], $dep_asal)?>><?php echo$row_dep['departemen']?></option>
										<?php
											}
										?>
										</select>
										</div>        
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td>
												<div id="sekolahInfo">
												<select name="sekolah" id="sekolah" onKeyPress="return focusNext('ketsekolah', event)" style="width:150px;" onFocus="panggil('sekolah')"  onblur="unfokus('sekolah')">
												<option value="">[Pilih Asal Sekolah]</option>
												<?php // Olah untuk combo sekolah
												$sql_sekolah="SELECT sekolah FROM $g_db_akademik.asalsekolah WHERE departemen='$dep_asal' ORDER BY urutan";
												$result_sekolah=QueryDB($sql_sekolah);
												while ($row_sekolah = mysql_fetch_array($result_sekolah)) {
													
												?>
											   <option value="<?php echo$row_sekolah['sekolah']?>" <?php echo StringIsSelected($row_sekolah['sekolah'],$sekolah)?>>
												<?php echo$row_sekolah['sekolah']?>
												</option>
											  <?php
												} 
												// Akhir Olah Data sekolah
												?>
												</select></div>            </td>
												<td valign="middle">
												<?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
												<img src="../images/ico/tambah.png" onClick="tambah_asal_sekolah();" onMouseOver="showhint('Tambah Asal Sekolah!', this, event, '80px')"/>
												<?php } ?>            </td>
											</tr>
										</table>        
									</td>
								</tr>
								<tr>
									<td valign="top">Keterangan Asal <br />Sekolah      </td>
									<td colspan="2">
										<textarea name="ketsekolah" cols="30" rows="2" id="ketsekolah" onFocus="showhint('Keterangan sekolah asal tidak boleh lebih dari 255 karakter!', this, event, '120px');panggil('ketsekolah')" class="Ukuranketerangan" onKeyPress="return focusNext('gol', event)" onBlur="unfokus('ketsekolah')"><?php echo$ketsekolah?></textarea>     	</td>
								</tr>
							</table>    
								<script language='JavaScript'>
								//Tables('table', 1, 0);
								</script>
						</td><!-- Akhir Kolom Kiri-->
						<td width="1%" align="center" valign="middle" style="border-left:1px dashed #333333; border-width:thin"></td>
						<td width="*" valign="top"><!-- Kolom Kanan-->
							<table width="100%" border="0" cellspacing="0" id="table" >
								<tr>
									<td height="30" colspan="3" valign="top">
										<font size="2" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="Gray"><strong>Riwayat Kesehatan</strong></font>
										<!--hr width="375" style="line-height:1px; border-style:dashed" align="left" color="#000000"/-->
										<div style="border-bottom:1px dashed #666666; border-width:thinl; margin-bottom:5px; margin-top:3px;"></div>
									</td>
								</tr>
								<tr>
									<td width="20%" valign="top">Gol. Darah</td>
									<td colspan="2">
										<input type="radio" name="gol" id="gol" value="A" 
										<?php if ($gol=="A")
											   echo "checked"; 
											   ?> onKeyPress="return focusNext('berat', event)"/>&nbsp;A&nbsp;&nbsp;
										<input type="radio" name="gol" id="gol2" value="AB"
										<?php if ($gol=="B")
												echo "checked"; 
												?> onKeyPress="return focusNext('berat', event)"/>&nbsp;AB&nbsp;&nbsp;
										<input type="radio" name="gol" id="gol" value="B" 
										<?php if ($gol=="AB")
												echo "checked";
												?> onKeyPress="return focusNext('berat', event)"/>&nbsp;B&nbsp;&nbsp;
										<input type="radio" name="gol" id="gol" value="O" 
										<?php if ($gol=="O")
												echo "checked"
												?> onKeyPress="return focusNext('berat', event)"/>&nbsp;O&nbsp;&nbsp;
										<input type="radio" name="gol" id="gol" value="" 
										<?php if ($gol=="")
											echo "checked" ?> onKeyPress="return focusNext('berat', event)"/><em>(belum ada data)</em>
									</td>
								</tr>
								<tr>
									<td>Berat</td>
									<td colspan="2"><input name="berat" type="text" size="6" maxlength="6" id="berat" onFocus="showhint('Berat badan tidak boleh lebih dari 6 angka!', this, event, '120px');panggil('berat')" value="<?php echo$berat?>" onKeyPress="return focusNext('tinggi', event)" onBlur="unfokus('berat')"/>&nbsp;kg</td>
								</tr>
								<tr>
									<td>Tinggi</td>
									<td colspan="2"><input name="tinggi" type="text" size="6" maxlength="6" id="tinggi" onFocus="showhint('Tinggi badan tidak boleh lebih dari 6 angka!', this, event, '120px');panggil('tinggi')" value="<?php echo$tinggi?>" onKeyPress="return focusNext('kesehatan', event)" onBlur="unfokus('tinggi')" />&nbsp;cm</td>
								</tr>
								<tr>
									<td valign="top">Riwayat Penyakit</td>
									<td colspan="2"><textarea name="kesehatan" cols="30" id="kesehatan" onFocus="showhint('Riwayat penyakit tidak boleh lebih dari 255 karakter!', this, event, '120px');panggil('kesehatan')" class="Ukuranketerangan" onKeyPress="return focusNext('namaayah', event)" onBlur="unfokus('kesehatan')"><?php echo$kesehatan?></textarea></td>
								</tr>
								<tr>
									<td height="30" colspan="3">
									<font size="2" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="Gray"><strong>Data Orang Tua Siswa</strong></font>
									<!--hr width="375" style="line-height:1px; border-style:dashed" align="left" color="#000000"/-->
									<div style="border-bottom:1px dashed #666666; border-width:thinl; margin-bottom:5px; margin-top:3px;"></div>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td align="center" valign="middle" bgcolor="#DBD8F3"><strong>Ayah</strong></td>
									<td align="center" valign="middle" bgcolor="#E9AFCF"><strong>Ibu</strong></td>
								</tr>
								<tr>
									<td valign="top">Nama</td>
									<td bgcolor="#DBD8F3">
										<input name="namaayah" type="text" size="15" maxlength="100" id="namaayah" onFocus="showhint('Nama Ayah tidak boleh lebih dari 100 karakter!', this, event, '120px');panggil('namaayah')" value="<?php echo$namaayah?>" class="ukuran" onKeyPress="return focusNext('namaibu', event)" onBlur="unfokus('namaayah')" />    
										<br />
										<input type="checkbox" name="almayah" value="1"  title="Klik disini jika Ayah Almarhum"
										<?php if ($almayah=="1") echo "checked";?>/>&nbsp;&nbsp;<font color="#990000" size="1">(Almarhum)</font>        </td>
									<td bgcolor="#E9AFCF">
										<input name="namaibu" type="text" size="15" maxlength="100" id="namaibu" onFocus="showhint('Nama Ibu tidak boleh lebih dari 100 karakter!', this, event, '120px');panggil('namaibu')"  value="<?php echo$namaibu?>" class="ukuran" onKeyPress="return focusNext('Infopendidikanayah', event)" onBlur="unfokus('namaibu')"/>    
										<br />
										<input type="checkbox" name="almibu" value="1"  title="Klik disini jika Ibu Almarhumah"
										<?php if ($almibu=="1") echo "checked"; ?>/>&nbsp;&nbsp;
										<font color="#990000" size="1">(Almarhumah)</font>        </td>
								</tr>  	
								<tr>
									<td>Pendidikan</td>
									<td bgcolor="#DBD8F3">
										<div id = "pendidikanayahInfo">
										<select name="pendidikanayah" id="Infopendidikanayah" class="ukuran" onKeyPress="return focusNext('Infopendidikanibu', event)" onFocus="panggil('Infopendidikanayah')" style="width:140px" onBlur="unfokus('Infopendidikanayah')">
										<option value="">[Pilih Pendidikan]</option> 
										<?php // Olah untuk combo pendidikan ayah
										$sql_pend_ayah="SELECT pendidikan FROM $g_db_akademik.tingkatpendidikan ORDER BY pendidikan";
										$result_pend_ayah=QueryDB($sql_pend_ayah);
										while ($row_pend_ayah = mysql_fetch_array($result_pend_ayah)) {
										?>
										<option value="<?php echo$row_pend_ayah['pendidikan']?>"<?php echo StringIsSelected($row_pend_ayah['pendidikan'],$pendidikanayah)?>>
										<?php echo$row_pend_ayah['pendidikan']?>
										</option>
										<?php
										} 
										// Akhir Olah Data sekolah
										?>
										</select>
										</div>	  	</td>
									<td bgcolor="#E9AFCF">
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<div id = "pendidikanibuInfo">	
													<select name="pendidikanibu" id="Infopendidikanibu" class="ukuran" onKeyPress="return focusNext('Infopekerjaanayah', event)" onFocus="panggil('Infopendidikanibu')"  style="width:140px" onBlur="unfokus('Infopendidikanibu')">
														<option value="">[Pilih Pendidikan]</option>   
														<?php // Olah untuk combo sekolah
														$sql_pend_ibu="SELECT pendidikan FROM $g_db_akademik.tingkatpendidikan ORDER BY pendidikan";
														$result_pend_ibu=QueryDB($sql_pend_ibu);
														while ($row_pend_ibu = mysql_fetch_array($result_pend_ibu)) {
														//if ($pendidikanibu=="")
														//	$pendidikanibu=$row_pend_ibu['pendidikan'];
														?>
														<option value="<?php echo$row_pend_ibu['pendidikan']?>"<?php echo StringIsSelected($row_pend_ibu['pendidikan'],$pendidikanibu) ?> >
														<?php echo$row_pend_ibu['pendidikan']?></option>
														<?php
														} 
														// Akhir Olah Data sekolah
														?>
													</select>
													</div>                </td>
												<td>
													<?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
													<img src="../images/ico/tambah.png" onClick="tambah_pendidikan();" onMouseOver="showhint('Tambah Jenis Pendidikan!', this, event, '80px')"/>
													<?php } ?>                
												</td>
											</tr>
										</table>      	
									</td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td bgcolor="#DBD8F3">
										<div id="pekerjaanayahInfo">
										<select name="pekerjaanayah" id="Infopekerjaanayah" class="ukuran" onKeyPress="return focusNext('Infopekerjaanibu', event)" onFocus="panggil('Infopekerjaanayah')" style="width:140px" onBlur="unfokus('Infopekerjaanayah')">
											<option value="">[Pilih Pekerjaan]</option>
											<?php // Olah untuk combo sekolah
											$sql_kerja_ayah="SELECT pekerjaan FROM $g_db_akademik.jenispekerjaan ORDER BY pekerjaan ASC";
											$result_kerja_ayah=QueryDB($sql_kerja_ayah);
											while ($row_kerja_ayah = mysql_fetch_array($result_kerja_ayah)) {
											//if ($pekerjaanayah=="")
											//	$pekerjaanayah=$row_kerja_ayah['pekerjaan'];
											?>
											<option value="<?php echo$row_kerja_ayah['pekerjaan']?>"<?php echo StringIsSelected($row_kerja_ayah['pekerjaan'],$pekerjaanayah)?>>
											<?php echo$row_kerja_ayah['pekerjaan']?>
											</option>
											<?php
											} 

										// Akhir Olah Data sekolah
										?>
										</select>
										</div>	  	
									</td>
									<td bgcolor="#E9AFCF">
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<div id = "pekerjaanibuInfo">
													<select name="pekerjaanibu" id="Infopekerjaanibu"  class="ukuran" onKeyPress="return focusNext('penghasilanayah1', event)" onFocus="panggil('Infopekerjaanibu')" style="width:140px" onBlur="unfokus('Infopekerjaanibu')">
														<option value="">[Pilih Pekerjaan]</option>
														<?php // Olah untuk combo sekolah
														$sql_kerja_ibu="SELECT pekerjaan FROM $g_db_akademik.jenispekerjaan ORDER BY pekerjaan ";
														$result_kerja_ibu=QueryDB($sql_kerja_ibu);
														while ($row_kerja_ibu = mysql_fetch_array($result_kerja_ibu)) {
													//if ($pekerjaan=="")
													//	$pekerjaanibu=$row_kerja_ibu['pekerjaan'];
														?>
														<option value="<?php echo$row_kerja_ibu['pekerjaan']?>"<?php echo StringIsSelected($row_kerja_ibu['pekerjaan'],$pekerjaanibu)?>>
														<?php echo$row_kerja_ibu['pekerjaan']?></option>
														<?php
														} 
												// Akhir Olah Data sekolah
												?>
													</select>
													</div>                
												</td>
												<td>
													<?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
													<img src="../images/ico/tambah.png" onClick="tambah_pekerjaan();" onMouseOver="showhint('Tambah Jenis Pekerjaan!', this, event, '80px')" />
												<?php } ?>                
												</td>
											</tr>
										</table>      	
									</td>
								</tr>
								<tr>
									<td>Penghasilan</td>
									<td bgcolor="#DBD8F3">
										<input type="text" name="penghasilanayah1" id="penghasilanayah1" class="ukuran" maxlength="20" size="15" value="<?php echo formatRupiah($penghasilanayah) ?>" onKeyPress="return focusNext('penghasilanibu1', event)" onBlur="formatRupiah('penghasilanayah1');unfokus('penghasilanayah1')" onFocus="unformatRupiah('penghasilanayah1');panggil('penghasilanayah1')" onKeyUp="penghasilan_ayah()" />
										<input type="hidden" name="penghasilanayah" id="penghasilanayah" value="<?php echo$penghasilanayah?>">        </td>
									<td colspan="2" bgcolor="#E9AFCF">
										<input type="text" name="penghasilanibu1" id="penghasilanibu1" class="ukuran" maxlength="20"  value="<?php echo formatRupiah($penghasilanibu) ?>" onKeyPress="return focusNext('emailayah', event)" onBlur="formatRupiah('penghasilanibu1');unfokus('penghasilanibu1')" onFocus="unformatRupiah('penghasilanibu1');panggil('penghasilanibu1')" onKeyUp="penghasilan_ibu()" />
										<input type="hidden" name="penghasilanibu" id="penghasilanibu" value="<?php echo$penghasilanibu?>">       
									</td>
								</tr>
								<tr>
									<td>Email Ortu</td>
									<td bgcolor="#DBD8F3"><input name="emailayah" type="text" size="15" maxlength="100" id="emailayah" onFocus="showhint('Alamat email Ayah tidak boleh lebih dari 100 karakter!', this, event, '120px');panggil('emailayah')" value="<?php echo$emailayah?>" class="ukuran" onKeyPress="return focusNext('emailibu', event)" onBlur="unfokus('emailayah')" /></td>
									<td colspan="2" bgcolor="#E9AFCF"><input name="emailibu" type="text" size="15" maxlength="100" id="emailibu" onFocus="showhint('Alamat email Ibu tidak boleh lebih dari 100 karakter!', this, event, '120px');panggil('emailibu')" value="<?php echo$emailibu?>" class="ukuran" onKeyPress="return focusNext('namawali', event)" onBlur="unfokus('emailibu')" /></td>
								</tr>
								<tr>
									<td>Nama Wali</td>
									<td colspan="2">
									<input type="text" name="namawali" id="namawali" size="30" maxlength="100" value="<?php echo$namawali?>" onKeyPress="return focusNext('alamatortu', event)" onFocus="panggil('namawali')" onBlur="unfokus('namawali')"/></td>
								</tr>
								<tr>
									<td valign="top">Alamat Orang Tua<br /></td>
									<td colspan="2"><textarea name="alamatortu" cols="30" id="alamatortu" class="Ukuranketerangan" onKeyPress="return focusNext('telponortu', event)" onFocus="panggil('alamatortu')" onBlur="unfokus('alamatortu')"><?php echo$alamatortu?></textarea>       </td>
								</tr>
								<tr>
									<td>Telepon Ortu</td>
									<td colspan="2">
									<input type="text" name="telponortu" id="telponortu" class="ukuran" maxlength="20" value="<?php echo$telponortu?>" onKeyPress="return focusNext('hportu', event)" onFocus="panggil('telponortu')" onBlur="unfokus('telponortu')" /></td>
								</tr>
								<tr>
									<td>HP Ortu</td>
									<td colspan="2"><input type="text" name="hportu" id="hportu" class="ukuran" maxlength="20"  value="<?php echo$hportu?>" onKeyPress="return focusNext('alamatsurat', event)" onFocus="panggil('hportu')" onBlur="unfokus('hportu')"/></td>
								</tr>
								<!--
								<tr>
									<td>Email Ortu</td>
									<td colspan="2">
									<input type="text" name="emailortu" id="emailortu" maxlength="100" value="<?php echo$emailortu?>" class="ukuranemail" onKeyPress="return focusNext('alamatsurat', event)" onFocus="panggil('emailortu')"/>        </td>
								</tr>
								-->
								<tr>
									<td height="30" colspan="3">
									<font size="2" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="Gray"><strong>Data Lainnya</strong></font>
									<!--hr width="375" style="line-height:1px; border-style:dashed" align="left" color="#000000"/-->
									<div style="border-bottom:1px dashed #666666; border-width:thinl; margin-bottom:5px; margin-top:3px;"></div>
									</td>
								</tr>
								<tr>
									<td valign="top">Alamat Surat</td>
									<td colspan="2">
									<textarea name="alamatsurat" cols="30" id="alamatsurat" class="Ukuranketerangan" onKeyPress="return focusNext('keterangan', event)" onFocus="panggil('alamatsurat')" onBlur="unfokus('alamatsurat')"><?php echo$alamatsurat?></textarea>        </td>
								</tr>
								<tr>
									<td valign="top">Keterangan</td>
									<td colspan="2"><textarea id="keterangan" name="keterangan" class="Ukuranketerangan" onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('keterangan')" onBlur="unfokus('keterangan')"><?php echo$keterangan?></textarea></td>
								</tr>
							</table>    
							<script language='JavaScript'>
							//Tables('table', 1, 0);
							</script>

						</td>
						<!-- Akhir Kolom Kanan-->
					</tr>
					<tr height="50">
						<td valign="middle" align="right">
							<input type="Submit" value="Simpan" name="Simpan" id="Simpan" class="but" onFocus="panggil('Simpan')" onBlur="unfokus('Simpan')"/></td>
						<td align="center" valign="middle">&nbsp;</td>
						<td valign="middle"><input class="but" type="button" value="Tutup" name="Tutup"  onClick="tutup()" />
						</td>
					</tr>
				</table>
						</form>
<!-- END OF CONTENT //--->
			</td>
			<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
		</tr>
		<tr height="28">
			<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
			<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
			<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
		</tr>
	</table>
</form>
<script language='JavaScript'>
	    Tables('table', 0, 0);
	</script>
<!--</body>
</html>-->