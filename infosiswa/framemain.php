<?php
/**[N]**
 * ediide Education Community
 * Jaringan Informasi Bersama Antar Sekolah
 * 
 * @version: 2.3.0 (September 24, 2010)
 * @notes: ediide Education Community will be managed by Yayasan Indonesia Membaca (http://www.indonesiamembaca.net)
 * 
 * Copyright (C) 2009 Yayasan Indonesia Membaca (http://www.indonesiamembaca.net)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 **[N]**/ ?>
<title>Halaman Siswa</title>
<link rel="SHORTCUT ICON" href="../images/man.ico">

<frameset rows="72px,*,31px" frameborder="0" border="0" framespacing="0">
	    <frame name="header" src="header.php"  scrolling="no" noresize="noresize"/>
		<frameset cols="200px,*,31" frameborder="0" border="0" framespacing="0">
			<frame name="frameatas" src="sidebar.php" scrolling="yes" noresize="noresize" />
			<frameset cols="0,*,0" frameborder="0" border="0" framespacing="0">
				<frame name="frameright" />
				<frame name="content" src="framecenter.php"/>
				
			</frameset>
			 
	</frameset>
	<frame name="framebottom" src="framebottom.php" noresize="noresize" scrolling="no" />
</frameset>