<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
 <?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Penilaian Pelajaran</title>
<script language="javascript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

<?php
$nis = SI_USER_ID();

OpenDb();
$sql = "SELECT idkelas FROM siswa WHERE nis='$nis'";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$kelas = $row[0];

?>
function refresh() {	
	document.location.reload();
}
function tampil(pelajaran,kelas,nis,departemen) {	
	parent.isi.location.href="nilai_content.php?pelajaran="+pelajaran+"&kelas="+kelas+"&nis="+nis+"&departemen="+departemen;
}
</script>
</head>

<body>
<input type="hidden" name="nis" id="nis" value="<?php echo $nis?>">
<table border="0" width="100%" align="center" >
<!-- TABLE CENTER -->
<tr>	
	<td width="38%"><strong>Departemen </strong></td>
    <td width="*"> 
    	<select name="departemen" id="departemen" onChange="change_departemen()">
          <?php	$dep = getDepartemen(SI_USER_ACCESS());    
	foreach($dep as $value) {
		if ($departemen == "")
			$departemen = $value; ?>
          <option value="<?php echo $value ?>" <?php echo StringIsSelected($value, $departemen) ?> > 
            <?php echo $value ?> 
            </option>
            <?php	} ?>
        </select>
		
    </td>
</tr>   
<tr>
	<td colspan="2"><br />
    <table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
    
    <tr height="30">    	
    	<td width="5%" class="header" align="center">No</td>
        <td width="*" class="header" align="center">Pelajaran</td>
    </tr>
    <?php 	OpenDb();		
		$sql = "SELECT DISTINCT p.replid, p.nama FROM ujian u, pelajaran p, nilaiujian n WHERE u.idpelajaran = p.replid AND u.idkelas = $kelas AND u.replid = n.idujian AND n.nis = '$nis' ORDER BY p.nama";
		//echo '<br> sql '.$sql;
		$result = QueryDb($sql); 				
		while ($row = @mysql_fetch_array($result)) {
	?>
    <tr>   	
       	<td height="25" align="center" onclick="tampil('<?php echo $row[0]?>','<?php echo $kelas?>','<?php echo $nis?>','<?php echo $departemen?>')" style="cursor:pointer"><?php echo ++$cnt?></td>
        <td height="25" onclick="tampil('<?php echo $row[0]?>','<?php echo $kelas?>','<?php echo $nis?>','<?php echo $departemen?>')" style="cursor:pointer">
        <?php echo $row[1]?>	</td>
    </tr>
    <!-- END TABLE CONTENT -->
    <?php 	} ?>
	</table>
	  <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>
	</td>	
</tr>
<!-- END TABLE CENTER -->    
</table> 
   

</body>
</html>