<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
$waktu=split("-",$_REQUEST[bulan]);

OpenDb();
$sql = "SELECT k.kelas, DAY(p.tanggal), MONTH(p.tanggal), YEAR(p.tanggal), p.jam, pp.catatan, l.nama, g.nama, p.materi, pp.replid FROM presensipelajaran p, ppsiswa pp, $g_db_pegawai.pegawai g, kelas k, pelajaran l WHERE pp.idpp = p.replid AND p.idkelas = k.replid AND p.idpelajaran = l.replid AND p.gurupelajaran = g.nip AND pp.nis = '$_REQUEST[nis_awal]' AND MONTH(p.tanggal) =  '$waktu[0]' AND YEAR(p.tanggal) =  '$waktu[1]' AND pp.statushadir='$_REQUEST[status]' AND p.idkelas='$_REQUEST[kelas]'";
$result = QueryDb($sql);
//echo $_REQUEST[kelas];
//echo $_REQUEST[kelas]." ".$_REQUEST[departemen]." ".$_REQUEST[tahunajaran];
?>    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Detail Presensi Pelajaran</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../style/style.css">
<script language="JavaScript" src="../script/tables.js"></script>
<script language="JavaScript">
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0">
  <tr>
    <td><fieldset>
      <legend>Data Presensi bulan <?php echo NamaBulan($_REQUEST[bulan])?> <?php echo$waktu[1]?></legend>
        <table width="100%" border="1" cellspacing="0" class="tab" id="table">
          <tr>
            <td height="30" class="headerlong"><div align="center">Tgl</div></td>
            <td height="30" class="headerlong"><div align="center">Jam</div></td>
            <td height="30" class="headerlong"><div align="center">Kelas</div></td>
            <td height="30" class="headerlong"><div align="center">Catatan</div></td>
            <td height="30" class="headerlong"><div align="center">Pelajaran</div></td>
            <td height="30" class="headerlong"><div align="center">Guru</div></td>
            <td height="30" class="headerlong"><div align="center">Materi</div></td>
          </tr>
          <?php
		  while ($row=@mysql_fetch_row($result)){
		  ?>
          <tr>
            <td height="25" bgcolor="#FFFFFF"><div align="center">
              <?php echo$row[1].'-'.$row[2].'-'.substr($row[3],2,2)?>
            </div></td>
            <td height="25" bgcolor="#FFFFFF"><div align="center">
              <?php echo substr($row[4],0,5)?>
            </div></td>
            <td height="25" bgcolor="#FFFFFF" align="center"><?php echo$row[0]?></td>
            <td height="25" bgcolor="#FFFFFF"><?php echo$row[5]?></td>
            <td height="25" bgcolor="#FFFFFF"><?php echo$row[6]?></td>
            <td height="25" bgcolor="#FFFFFF"><?php echo$row[7]?></td>
            <td height="25" bgcolor="#FFFFFF"><?php echo$row[8]?></td>
          </tr>
          <?php
		  }
		  ?>
      </table>
     	<script language='JavaScript'>
			Tables('table', 1, 0);
		</script>
    </fieldset></td>
  </tr>
  <tr><td align="center"><br /><input name="kembali" class="but" type="button" value="Tutup" onClick="window.close()" /></td></tr>
</table>
</body>
</html>