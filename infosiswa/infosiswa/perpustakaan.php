<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/common.php');
OpenDb();
$idanggota=$_REQUEST[nis];
$sql = "SELECT pu.judul, p.tglpinjam, pu.replid, p.tglkembali,p.status,p.tglditerima FROM $g_db_perpustakaan.pinjam p, $g_db_perpustakaan.daftarpustaka d, $g_db_perpustakaan.pustaka pu WHERE p.idanggota = '$idanggota' AND p.kodepustaka=d.kodepustaka AND d.pustaka=pu.replid AND p.status<>'0'";
$result = QueryDb($sql);
$cnt=1;
?>
<div style="height:15px">&nbsp;</div>
<table width="100%" border="1" cellspacing="0" cellpadding="0" class="tab">
  <tr>
    <td height="25" align="center" class="header">No</td>
    <td height="25" align="center" class="header">Judul Pustaka</td>
    <td height="25" align="center" class="header">Tgl Pinjam</td>
    <td height="25" align="center" class="header">Tgl Kembali</td>
    <td align="center" class="header">Status</td>
  </tr>
  <?php if (@mysql_num_rows($result)>0) { ?>
  <?php while ($row = @mysql_fetch_row($result)) { ?>
  <tr>
    <td height="20" align="center"><?php echo$cnt?></td>
    <td height="20"><div style="padding-left:5px;"><?php echo$row[0]?></div></td>
    <td height="20" align="center"><?php echo ShortDateFormat($row[1])?></td>
    <td height="20" align="center"><?php echo ShortDateFormat($row[3])?></td>
    <td align="center">
	<?php
    if ($row[4]=='1')
		echo "<span style='color:#FF3300;font-weight:bold'>Belum dikembalikan</span>";
	elseif ($row[4]=='2')
		echo "Sudah dikembalikan pada<br>".ShortDateFormat($row[5]);		
	?>    </td>
  </tr>
  <?php $cnt++; ?>
  <?php } ?>
  <?php } else { ?>
  <tr>
    <td colspan="5" height="20" align="center">Belum ada data peminjaman</td>
  </tr>
  <?php } ?>
</table>