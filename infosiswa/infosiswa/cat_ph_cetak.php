<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
$nis = "";
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
OpenDb();
$sql = "SELECT t.departemen FROM tahunajaran t, kelas k, siswa s WHERE s.idkelas=k.replid AND k.idtahunajaran=t.replid AND s.nis='$nis'";
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$departemen = $row[0];
CloseDb();
OpenDb();
$res_nm_sis=QueryDb("SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'");
$row_nm_sis=@mysql_fetch_array($res_nm_sis);
$tglawal = "";
if (isset($_REQUEST['tglawal'])){
	$tglawal = $_REQUEST['tglawal'];
	//$tglawl = explode('-',$_REQUEST['tglawal']);
	//$tglawal = $tglawl[2]."-".$tglawl[1]."-".$tglawl[0];
	}
$tglakhir = "";
if (isset($_REQUEST['tglakhir'])){
	$tglakhir = $_REQUEST['tglakhir'];
	//$tglakhr =  explode('-',$_REQUEST['tglakhir']);
	//$tglakhir = $tglakhr[2]."-".$tglakhr[1]."-".$tglakhr[0];
	}
$sql="SELECT ph.tanggal1,ph.tanggal2,phsiswa.keterangan FROM $g_db_akademik.presensiharian ph, $g_db_akademik.phsiswa phsiswa WHERE phsiswa.nis='$nis' AND phsiswa.idpresensi=ph.replid AND ph.tanggal1>='$tglawal' AND ph.tanggal2<='$tglakhir' AND phsiswa.keterangan<>''";
$sql="SELECT ph.tanggal1,ph.tanggal2,phsiswa.keterangan FROM $g_db_akademik.presensiharian ph, $g_db_akademik.phsiswa phsiswa WHERE phsiswa.nis='$nis' AND phsiswa.idpresensi=ph.replid AND ph.tanggal1>='$tglawal' AND ph.tanggal2<='$tglakhir' AND phsiswa.keterangan<>''";
$result=QueryDb($sql);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ediide INFOGURU [Cetak Catatan Presensi Harian]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tables.js"></script>
<style type="text/css">
<!--
.style1 {
	color: #666666;
	font-weight: bold;
}
-->
</style>
</head>
<body >
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
<?php getHeader($departemen) ?>
	
<center>
  <font size="4"><strong>Catatan Presensi Harian</strong></font><br />
 </center><br /><br />
 
<table width="100%" border="0" cellspacing="0">
  <tr>
    <td><fieldset><legend class="style1">Presensi Harian</legend>
<table width="100%" border="0" cellspacing="0">
          <tr>
            <td width="11%"><strong>Siswa</strong></td>
            <td width="1%"><strong>:</strong></td>
            <td width="88%">[<?php echo$nis?>]&nbsp;<?php echo$row_nm_sis[nama]?></td>
          </tr>
          <tr>
            <td><strong>Periode</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo ShortDateFormat($_REQUEST['tglawal'])?> s.d. <?php echo ShortDateFormat($_REQUEST['tglakhir'])?></td>
          </tr>
        </table>	
    </fieldset></td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" class="tab">
  <tr class="header" height="30">
    <td width="3%" align="center">No.</td>
    <td width="42%" >Periode</td>
    <td width="55%" >Keterangan</td>
  </tr>
  <?php
  if (@mysql_num_rows($result)>0){
  $cnt=1;
  while ($row=@mysql_fetch_array($result)){
  	$a="";
	if ($cnt%2==0)
		$a="style='background-color:#FFFFCC'";
  ?>
  <tr height="25" <?php echo$a?>>
    <td align="center"><?php echo$cnt?></td>
    <td><?php echo ShortDateFormat($row[tanggal1])?> s.d. <?php echo ShortDateFormat($row[tanggal2])?></td>
    <td><?php echo$row[keterangan]?></td>
  </tr>
  <?php
  $cnt++;
  }
  } else {
  ?>
   <tr height="25">
    <td align="center" colspan="3">Tidak ada keterangan presensi untuk periode tsb.</td>
  </tr>
  <?php } ?>
</table>

    </td>
  </tr>
</table>
</td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>