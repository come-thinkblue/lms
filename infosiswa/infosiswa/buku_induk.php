<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');
//require_once('../library/departemen.php');

$NTT = new NumberToText();

OpenDb();
$nis = SI_USER_ID();		

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<script language="javascript" src="../script/tools.js"></script>
<style type="text/css">
<!--
.style1 {
	color: #FFFFFF;
	font-weight: bold;
}
.style4 {font-size: 12}
.style5 {color: #FFFFFF; font-weight: bold; font-size: 12; }
.style6 {
	font-size: 12px;
	font-weight: bold;
}
-->
</style>
<script language="javascript" src="../script/tables.js"></script>
</HEAD>
<BODY>

<table width="100%" border="0">
  
  <tr> 
    <td><table width="100%" border="0" bordercolor="#666666" cellpadding="0" cellspacing="0">
  <tr>
    <td width="6%" height="50" ><span class="style5">&nbsp;NIS </span><span class="style4"><br>
        <span class="style1">&nbsp;Nama </span></span></td>
    <td width="46%" height="40" ><span class="style5">
      : <?php echo $nis?>
    </span><span class="style4"><br>
	  <span class="style1">
	  : <?php
	$sql_get_nama="SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result_get_nama=QueryDb($sql_get_nama);
	$row_get_nama=@mysql_fetch_array($result_get_nama);
	echo $row_get_nama[nama];
    ?>
	   </span></span></td>
    <td width="47%" align="right" valign="bottom"><div align="right"><a href="laporan_buku_induk_content.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>">
  <img src="../images/ico/refresh.png" border="0">Refresh</a>
  <!--
    <a href="#" onClick="newWindow('laporan_buku_induk_cetak.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',750,636,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/print.png" border="0">Cetak</a>
    <a href="#" onClick="newWindow('laporan_buku_induk_word.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',122,122,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/word.png" border="0">Cetak Word</a>-->
  <br>
</div></td>
  </tr>
  
</table>
    
	</td>
</tr>
<tr>
	<td><fieldset><legend><strong>Penilaian Hasil Belajar</strong></legend>
	<table width="100%" border="1" class="tab" id="table" bordercolor="#000000">
	<thead>
		<tr>
		<td width="" rowspan="3" class="headerlong"><div align="center">No</div></td>
		<td width="" rowspan="3" class="headerlong"><div align="center">Pelajaran</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas X</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas XI</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas XII</div></td>
		</tr>
		<tr>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
		</tr>
		<tr>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
		</tr>
	</thead>
  	
	<tbody>
	<?php
		$sel = "SELECT replid FROM tingkat";
		$t = mysql_fetch_array(QueryDb($sel));
		$bt = $t['replid'] + 3;
		$sel2 = "SELECT replid FROM semester";
		$s = mysql_fetch_array(QueryDb($sel2));
		$bs = $s['replid'] + 1;
		for($h=0; $h<5; $h++){
		$sql = "SELECT replid, nama FROM pelajaran WHERE sifat_k13='$h' ORDER BY nama";
		$res = QueryDb($sql);
		echo "<tr><td colspan='8'>";
			if($h==0)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK A</b>";
			if($h==1)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK B</b>";
			if($h==2)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan IPA</b>";
			if($h==3)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan IPS</b>";
			if($h==4)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan AGAMA</b>";
		echo "</td></tr>";
		$awal = 0;
		$no = $awal + $no;
		while ($row = mysql_fetch_array($res)){
		$no++;
			echo "<tr>
				<td align='center'>".$no."</td>
				<td>".$row[nama]."</td>";
			for ($i=$t['replid']; $i < $bt; $i++) { 
				for ($j=$bs; $j >= $s['replid']; $j--) { 
				$sql2 = "SELECT n.nilaiangka FROM nap n, kelas k, infonap i, dasarpenilaian d WHERE i.replid=n.idinfo AND i.idkelas=k.replid AND k.idtingkat='$i' AND i.idsemester='$j' AND i.idpelajaran='$row[replid]' AND n.nis='$nis' AND d.dasarpenilaian='KI3' AND d.replid=n.idaturan";
				echo $sq;
				$res2 = QueryDb($sql2);
				$cek = mysql_num_rows($res2);
				if ($cek>0){
					while ($row2 = mysql_fetch_array($res2)) {
						echo "<td align='center'>".$row2[nilaiangka]."</td>";
					}
				}else{
					echo "<td></td>";
				}


				}
			}
			echo "</tr>";
		}
		}
	?>
	</tbody>
	</table>
	<script language='JavaScript'>
	   	Tables('table', 1, 0);
	</script>
	</fieldset></td>
  </tr>
</table>

</BODY>
</HTML>
<?php
CloseDb();
?>