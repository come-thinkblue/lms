<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('deleteimage.php');
require_once('../include/sessioninfo.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$op = "";
if (isset($_REQUEST['op']))
	$op = $_REQUEST['op'];
$tahun = "";
if (isset($_REQUEST['tahun']))
	$tahun = $_REQUEST['tahun'];
$bulan = "";
if (isset($_REQUEST['bulan']))
	$bulan = $_REQUEST['bulan'];
	
$namabulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");

OpenDb();

$sql = "SELECT bulan FROM $g_db_umum.gambartiny WHERE idguru='".SI_USER_ID()."' GROUP BY bulan";
$result = QueryDb($sql);
$num = @mysql_num_rows($result);
$b = 0;
while ($row = @mysql_fetch_array($result))
{
	$bln[$b]=$row['bulan'];
	$b++;
}

$sql = "SELECT tahun FROM $g_db_umum.gambartiny WHERE idguru='".SI_USER_ID()."' GROUP BY tahun";
$result = QueryDb($sql);
$t = 0;
while ($row = @mysql_fetch_array($result))
{
	$thn[$t]=$row['tahun'];
	$t++;
}

if ($op == "09vn4230984cn2048723n98423")
{
	$sql_del = "DELETE FROM $g_db_umum.gambartiny WHERE replid='$_REQUEST[replid]'";
	$result_del = QueryDb($sql_del);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link href="../style/style.css" rel="Stylesheet" />
    <title>Untitled Page</title>
    <script language="javascript" src="../script/tools.js"></script>
    <script language="javascript">
    function OpenGambar(addr) {
        //var addr = "gambar.php.php?replid=" + replid + "&table=" + $g_db_umum.gambartiny;
	    newWindow(addr, 'Gambar','720','630','resizable=1,scrollbars=1,status=0,toolbar=0');
    }
    
    function DelGambar(replid) {
        if (confirm("Apakah anda yakin akan menghapus gambar ini?")) {
            document.location.href = "uploaderbrowser.php?op=09vn4230984cn2048723n98423&replid="+replid;
        }
    }
        
    function PilihGambar(replid) {
        parent.opener.Accept(replid);
        parent.close();
    }
	function chg() {
        var tahun=document.getElementById('tahun').value;
		var bulan=document.getElementById('bulan').value;
		document.location.href = "uploaderbrowser.php?tahun="+tahun+"&bulan="+bulan;
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    Bulan: 
    <select name="bulan" id="bulan" onchange="chg()">
    	<?php
		for ($bi=0;$bi<=$b-1;$bi++){
		if ($bulan=="")
			$bulan=$bln[$bi];
		?>
        <option value="<?php echo$bln[$bi]?>" <?php echo StringIsSelected($bulan,$bln[$bi])?>><?php echo$namabulan[(int)$bln[$bi]-1]?></option>
        <?php
		}
		?>
    </select>&nbsp;<select name="tahun" id="tahun" onchange="chg()">
    	<?php
		for ($ti=0;$ti<=$t-1;$ti++){
		if ($tahun=="")
			$tahun=$thn[$ti];
		?>
        <option value="<?php echo$thn[$ti]?>" <?php echo StringIsSelected($tahun,$thn[$ti])?>><?php echo$thn[$ti]?></option>
        <?php
		}
		?>
    </select>
    <br /><br />
<?php
	if ($num>0)
	{
		$sql = "SELECT * FROM $g_db_umum.gambartiny WHERE idguru='".SI_USER_ID()."' AND tahun=".$tahun." AND bulan=".$bulan;
		$result = QueryDb($sql); ?>
		<table width="75%" border="0" cellspacing="0">
<?php		while ($row = @mysql_fetch_array($result))
		{
			$bln = $row['bulan'];
			$thn = $row['tahun'];
			$fn = $row['info1'];
			
			$imgAddr = "$FILESHARE_ADDR/media/$thn$bln/$fn";
			?>
			<tr>
				<td align="center">
					<img onclick="PilihGambar('<?php echo$imgAddr?>')"
						  src="<?php echo$imgAddr?>" width="80" height="80"/><br /><br />
					<input class="but" type="button" name="pilih" id="pilih" value="Pilih"
							 onclick="PilihGambar('<?php echo$imgAddr?>')" />
					<img style="cursor:pointer;" src="../images/ico/lihat.png"
						  onclick="OpenGambar('<?php echo$imgAddr?>')" title="Lihat Gambar Ini" />&nbsp;
					<img style="cursor:pointer;" src="../images/ico/hapus.png"
						  onclick="DelGambar('<?php echo$row[replid]?>')" title="Hapus Gambar Ini" />
				</td>  
			</tr>
			<tr>
				<td>Nama Gambar : <?php echo$row[namagambar]?><br />Keterangan Gambar : <?php echo$row[keterangan]?></td>  
			</tr>
			<tr>
				<td><hr /></td>  
			</tr>
<?php 	} ?>
</table>
<?php }
	CloseDb();
?>

	</div>
    </form>
</body>
</html>