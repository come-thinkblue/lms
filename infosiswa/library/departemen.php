<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');

function getDepartemen($access) {
	if ($access == "ALL") {
		$sql = "SELECT departemen FROM departemen where aktif=1 ORDER BY urutan ";
		$result = QueryDb($sql);
		$i = 0;
		while($row = mysql_fetch_row($result)) {
			$dep[$i] = $row[0];
			$i++;
		}
	} else {
		$dep[0] = $access;
	}
	return $dep;
}
?>