<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('departemen.php');

$flag = 0;
if (isset($_REQUEST['flag']))
	$flag = (int)$_REQUEST['flag'];
$nama = $_REQUEST['nama'];
$nis = $_REQUEST['nis'];
$departemen = $_REQUEST['departemen'];
OpenDb();
?>
<table border="0" width="100%" cellpadding="2" cellspacing="2" align="center">
<tr>
	<td>
	<input type="hidden" name="flag" id="flag" value="<?php echo$flag ?>" />
	<font size="2" color="#000000"><strong>Cari Siswa</strong></font>
 	</td>
</tr>
<tr>
    <td width="20%"><font color="#000000"><strong>Departemen</strong></font></td>
    <td><select name="depart" id="depart" onChange="change_departemen(1)" style="width:150px" onkeypress="return focusNext('nama', event)">
	<?php	$dep = getDepartemen(SI_USER_ACCESS());    
        foreach($dep as $value) {
            if ($departemen == "")
                $departemen = $value; ?>
        <option value="<?php echo$value ?>" <?php echo StringIsSelected($value, $departemen) ?> >
        <?php echo$value ?>
        </option>
        <?php	} ?>
  	</select>
    </td>
   	<td rowspan="2" width="15%" align="center">
    <input type="button" class="but" name="submit" id="submit" value="Cari" onclick="carilah()" style="width:70px;height:40px"/>
    </td>
</tr>
<tr>
    <td><font color="#000000"><strong>Nama Siswa</strong></font></td>
    <td><input type="text" name="nama" id="nama" value="<?php echo$_REQUEST['nama'] ?>" size="22" onKeyPress="return focusNext('submit', event)"/>
		&nbsp;
        <font color="#000000"><strong>NIS</strong></font>	
        <input type="text" name="nis" id="nis" value="<?php echo$_REQUEST['nis'] ?>" size="20" onKeyPress="return focusNext('submit', event)"/>     
  	</td>   
</tr>
<tr>
    <td align="center" colspan="3">
	<div id="caritabel">
<?php 
if (isset($_REQUEST['submit']) || $_REQUEST['submit'] == 1) { 
	OpenDb();    
   	
	if ((strlen($nama) > 0) && (strlen($nis) > 0))
		$sql = "SELECT s.nis, s.nama, k.kelas, t.departemen FROM $g_db_akademik.siswa s,$g_db_akademik.kelas k,$g_db_akademik.tingkat t, $g_db_akademik.departemen d WHERE s.nama LIKE '%$nama%' AND s.nis LIKE '%$nis%' AND k.replid=s.idkelas AND s.statusmutasi=0 AND s.alumni=0 AND s.aktif=1 AND k.idtingkat = t.replid AND t.departemen = d.departemen ORDER BY d.departemen, k.kelas, s.nama"; 	
	else if (strlen($nama) > 0)
		$sql = "SELECT s.nis, s.nama, k.kelas, t.departemen FROM $g_db_akademik.siswa s,$g_db_akademik.kelas k,$g_db_akademik.tingkat t, $g_db_akademik.departemen d WHERE s.nama LIKE '%$nama%' AND k.replid=s.idkelas AND s.statusmutasi=0 AND s.alumni=0 AND s.aktif=1 AND k.idtingkat = t.replid AND t.departemen = d.departemen ORDER BY d.departemen, k.kelas, s.nama"; 
	else if (strlen($nis) > 0)
		$sql = "SELECT s.nis, s.nama, k.kelas, t.departemen FROM $g_db_akademik.siswa s,$g_db_akademik.kelas k ,$g_db_akademik.tingkat t, $g_db_akademik.departemen d WHERE k.replid=s.idkelas AND s.nis LIKE '%$nis%' AND s.statusmutasi=0 AND s.alumni = 0 AND s.aktif=1 AND k.idtingkat = t.replid AND t.departemen = d.departemen ORDER BY d.departemen, k.kelas, s.nama"; 
	//else if ((strlen($nama) == 0) || (strlen($nis) == 0))
	//	$sql = "SELECT s.nis, s.nama, k.kelas FROM $g_db_akademik.siswa s,$g_db_akademik.kelas k,$g_db_akademik.tingkat ti,$g_db_akademik.tahunajaran ta WHERE k.replid=s.idkelas AND s.statusmutasi=0 AND k.idtahunajaran=ta.replid AND k.idtingkat=ti.replid AND ta.departemen='$departemen' AND ti.departemen='$departemen' AND s.aktif=1  ORDER BY s.nama"; 
	$result = QueryDb($sql); 
	if (@mysql_num_rows($result)>0){
?>   
	<br>
   	<table width="100%" id="table1" class="tab" align="center" cellpadding="2" cellspacing="0" border="1">
    <tr height="30">
        <td class="header" width="7%" align="center" height="30">No</td>
        <td class="header" width="15%" align="center" height="30">N I S</td>
        <td class="header" >Nama</td>
        <td class="header" align="center">Departemen</td>
        <td class="header" align="center">Kelas</td>
        <td class="header" width="10%">&nbsp;</td>
    </tr>
<?php
	$cnt = 0;
		while($row = mysql_fetch_row($result)) { ?>
   	<tr height="25" onClick="pilih('<?php echo$row[0]?>','<?php echo$row[1]?>')" style="cursor:pointer">
        <td align="center" ><?php echo++$cnt ?></td>
        <td align="center"><?php echo$row[0] ?></td>
        <td ><?php echo$row[1] ?></td>
        <td align="center"><?php echo$row[3] ?></td>
        <td align="center"><?php echo$row[2] ?></td>
        <td align="center"><input type="button" value="Pilih" onclick="pilih('<?php echo$row[0]?>','<?php echo$row[1]?>')" class="but"></td>
	</tr>
<?php } CloseDb(); ?>
 	</table>
<?php } else { ?>    		
	<table width="100%" align="center" cellpadding="2" cellspacing="0" border="0" id="table1">
	<tr height="30" align="center">
		<td>   
   
	<br /><br />	
	<font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br /><br />            
		Tambah data siswa di menu Pendataan Siswa pada bagian Kesiswaan. </b></font>	
	<br /><br />
   		</td>
    </tr>
    </table>
<?php 	} 
} else { ?>

<table width="100%" align="center" cellpadding="2" cellspacing="0" border="0" id="table1">
<tr height="30" align="center">
    <td>   

<br /><br />	
<font size="2" color="#757575"><b>Klik pada tombol "Cari" di atas untuk melihat data calon siswa <br />sesuai dengan NIS atau Nama Siswa berdasarkan <i>keyword</i> yang dimasukkan</b></font>	
<br /><br />
    </td>
</tr>
</table>


<?php }?>	
    </div>
	 </td>    
</tr>
<tr>
	<td align="center" colspan="3">
	<input type="button" class="but" name="tutup" id="tutup" value="Tutup" onclick="window.close()" style="width:80px;"/>
	</td>
</tr>
</table>