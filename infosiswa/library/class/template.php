<?php
/**[N]**

 **[N]**/ ?>
<?php

if(ereg("template.php", $PHP_SELF)) {
	header("location : index.php");
	die;
}

class Template {
	VAR $TAGS = array();
	VAR $THEME;
	VAR $CONTENT;

	public function DefineTag($tag_name,$var_name) {
		$this->TAGS[$tag_name] = $var_name;
	}

	public function DefineTheme($theme_name) {
		$this->THEME = $theme_name;
	}

	public function Parse() {
		$this->CONTENT = file($this->THEME);
		$this->CONTENT = implode("", $this->CONTENT);
		while(list($key,$val) = each ($this->TAGS)) {
			$this->CONTENT = ereg_replace($key,$val,$this->CONTENT);
		}
	}

	public function PrintProcess() {
		echo $this->CONTENT;
	}
}
?>