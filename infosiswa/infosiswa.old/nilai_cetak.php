<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php 
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/common.php');
require_once('../include/getheader.php');
require_once('../library/dpupdate.php');

$tahunajaran = $_REQUEST['tahunajaran']; 
$semester = $_REQUEST['semester']; 
$pelajaran = $_REQUEST['pelajaran']; 
$nis = $_REQUEST['nis'];
$kelas = $_REQUEST['kelas'];
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ediide INFOGURU [Cetak Laporan Nilai]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tables.js"></script>
</head>
<body >
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
	<?php 	OpenDb();
		$sql = "SELECT * FROM semester WHERE replid = '$semester'";
		$result = QueryDb($sql);
		$row = mysql_fetch_array($result);
    ?>
<?php getHeader($row[departemen]) ?>
	
<center>
  <font size="4"><strong>LAPORAN NILAI</strong></font><br />
 </center><br /><br />
  <table width="100%" border="0" height="100%">
            <tr>
                <td width="7%" valign="top">
                	
                <font size="2" color="#000000"><b>Departemen </b></font>
                <?php CloseDb(); ?>               </td>      	
                <td width="33%" valign="top">&nbsp;<?php echo$row['departemen']?></td>
                <td width="5%" valign="top"><?php 	OpenDb();
                    $sql = "SELECT * FROM semester WHERE replid = '$semester'";
                    $result = QueryDb($sql);
                    $row = mysql_fetch_array($result);
                    
                ?>	
                <font size="2" color="#000000"><b>Semester </b></font>
                <?php CloseDb(); ?>               </td>
              <td width="55%" valign="top">&nbsp;<?php echo$row['semester']?></td>
      </tr>
      <tr>
                <td valign="top">
                <?php 	OpenDb();
                    $sql = "SELECT * FROM tahunajaran WHERE replid = '$tahunajaran'";
                    $result = QueryDb($sql);
                   
                    $row = mysql_fetch_array($result);
                    
                ?>	
                <font size="2" color="#000000"><b>Tahun&nbsp;Ajaran </b></font>
                <?php CloseDb(); ?>               </td>      	
                <td valign="top">&nbsp;<?php echo$row['tahunajaran']?></td>
                <td valign="top"><?php 	OpenDb();
                    $sql = "SELECT * FROM kelas WHERE replid = '$kelas'";
                    $result = QueryDb($sql);
                    $row = mysql_fetch_array($result);
                    
                ?>	
                <font size="2" color="#000000"><b>Kelas </b></font>
                <?php CloseDb(); ?>               </td>
                <td valign="top">&nbsp;<?php echo$row['kelas']?></td>
      </tr>
            <tr>
              <td valign="top">
			    <?php 	OpenDb();
                    $sql = "SELECT * FROM pelajaran WHERE replid = '$pelajaran'";
                    $result = QueryDb($sql);
                   
                    $row = mysql_fetch_array($result);
                    
                ?>	
                <font size="2" color="#000000"><b>Pelajaran </b></font>
                <?php CloseDb(); ?></td>
              <td valign="top">&nbsp;<?php echo$row['nama']?></td>
              <td valign="top"><?php 	OpenDb();
                    $sql = "SELECT * FROM siswa WHERE nis = '$nis'";
                    $result = QueryDb($sql);
                    $row = mysql_fetch_array($result);
                    
                ?>	
                <font size="2" color="#000000"><b>Siswa </b></font>
                <?php CloseDb(); ?>               </td>
              <td valign="top">&nbsp;<?php echo$row['nis']."-".$row['nama']?></td>
            </tr>
            <tr>
              <td colspan="4" valign="top">&nbsp;</td>
            </tr>
   
            
	<?php	OpenDb();
        $sql = "SELECT j.replid, j.jenisujian FROM jenisujian j, ujian u WHERE j.idpelajaran = '$pelajaran' AND u.idjenis = j.replid GROUP BY j.jenisujian";
        
        $result = QueryDb($sql);
		if (mysql_num_rows($result) > 0) {
        while($row = @mysql_fetch_array($result)){			
    ?>
           	<tr>
                <td colspan="4"> 
                <br>
                <fieldset><legend><strong> <?php echo$row['jenisujian']?></strong></legend>
                <br />
		<?php 	OpenDb();		
            $sql1 = "SELECT u.tanggal, n.nilaiujian, n.keterangan FROM ujian u, pelajaran p, nilaiujian n WHERE u.idpelajaran = p.replid AND u.idkelas = '$kelas' AND u.idpelajaran = '$pelajaran' AND u.idsemester = '".$semester."' AND u.idjenis = '$row[replid]' AND u.replid = n.idujian AND n.nis = '$nis' ORDER BY u.tanggal";
            $result1 = QueryDb($sql1);
			
			if (@mysql_num_rows($result1) > 0){
		?>
           	
                <table border="1" width="100%" id="table1" class="tab" bordercolor="#000000">
                <tr class="header" align="center" height="30">		
                    <td width="10">No</td>
                    <td width="20%">Tanggal</td>
                    <td width="10%">Nilai</td>
                    <td width="*">Keterangan</td>
                </tr>
       	<?php	
				$sql2 = "SELECT AVG(n.nilaiujian) as rata FROM ujian u, pelajaran p, nilaiujian n WHERE u.idpelajaran = p.replid AND u.idkelas = '$kelas' AND u.idpelajaran = '$pelajaran' AND u.idsemester = '".$semester."' AND u.idjenis = '$row[replid]' AND u.replid = n.idujian AND n.nis = '$nis' ";
				$result2 = QueryDb($sql2);	
            	$row2 = @mysql_fetch_array($result2);
            	$rata = $row2[rata];
            	$cnt = 1;
                while($row1 = @mysql_fetch_array($result1)){			
         ?>
                <tr>        			
                    <td height="25" align="center"><?php echo$cnt?></td>
                    <td width="250" height="25" align="center"><?php echo format_tgl($row1[0])?></td>
                    <td width="10" height="25" align="center"><?php echo$row1[1]?></td>
                    <td height="25"><?php echo$row1[2]?></td>            
                </tr>	
        <?php		$cnt++;
                } ?>
                <tr>        			
                    <td colspan="2" height="25" align="center"><strong>Nilai rata rata</strong></td>
                    <td width="10" height="25" align="center"><?php echo round($rata,2)?></td>
                    <td height="25">&nbsp;</td>            
                </tr>
                </table>
                             
        <?php } else { ?>
        		<table width="100%" border="0" align="center" id="table1">          
                <tr>
                    <td align="center" valign="middle" height="50">
                    <font size = "2" color ="red"><b>Tidak ditemukan adanya data.</b></font>                    </td>
                </tr>
                </table>
                <!--<tr>        			
                    <td colspan="4" height="25" align="center">Tidak ada nilai</td>
                </tr>-->
       	<?php } ?>
                 </fieldset>                </td>	
            </tr>
   	<?php } ?>      
	<?php } else { ?>
    		<tr>
            	<td height="50" colspan="2" align="center" valign="middle">
                <table border="0" width="100%" id="table1" cellpadding="0" cellspacing="0">
        		<tr align="center" valign="middle" >
        			<td><font size = "2" color ="red"><b>Tidak ditemukan adanya data.<br /></font></td>
        		</tr>
        		</table>                </td>
          	</tr>
                
  	<?php } ?>
            </table>
  </td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>

</html>