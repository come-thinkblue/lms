<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
// Patch Management Framework

function ApplyModulePatch() 
{
	if (file_exists("../include/module.patch.install.php"))
	{
		require_once("../include/module.patch.install.php");
		InstallModulePatch('../');
		
		unlink("../include/module.patch.install.php");
	}
}

?>