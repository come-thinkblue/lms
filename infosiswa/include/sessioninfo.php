<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("jbsinfosiswa");
if(!isset($_SESSION)){ session_start();}

$SI_USER_LANDLORD = 0;
$SI_USER_MANAGER = 1;
$SI_USER_STAFF = 2;
$SI_USER_OSIS = 3;
$STAFF_DEPT = $_SESSION['departemen'];

function SI_USER_NAME() {
	return $_SESSION['nama'];
}

function SI_USER_NICKNAME() {
	return $_SESSION['panggilan'];
}

function SI_USER_ID() {
	return $_SESSION['login'];
}

function SI_USER_THEME() {
	return $_SESSION['theme'];
}


//function GetThemeDir() {
//	return $_SESSION['theme'];
//}
function SI_USER_LEVEL() {
	switch ($_SESSION['tingkat']){
	case 0:
		{
	global $SI_USER_LANDLORD;
	return $SI_USER_LANDLORD;
	break;
		}
		case 1:
		{
	global $SI_USER_MANAGER;
	return $SI_USER_MANAGER;
	break;
		}
		case 2:
		{
	global $SI_USER_STAFF;
	return $SI_USER_STAFF;
	break;
		}
		case 3:
		{
	global $SI_USER_OSIS;
	return $SI_USER_OSIS;
	break;
		}
	}
}
function SI_USER_ACCESS() {
	if ($_SESSION['tingkat']==2){
		return $_SESSION['departemen'];
	} else {
		return "ALL";
	}
}
?>