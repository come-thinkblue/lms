<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("jbsinfosiswa");
if(!isset($_SESSION)){ session_start();}

if(!isset($_SESSION["login"])) 
	header("location: login.php");
else
	header("location: framemain.php");
?>