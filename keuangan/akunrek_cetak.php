<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

$kategori = "";
if (isset($_GET['kategori']))
	$kategori = $_GET['kategori'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Cetak Kode Rekening Perkiraan]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader('yayasan')?>

<center><font size="4"><strong>REKENING PERKIRAAN</strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td><strong>Kategori :</strong></td>
    <td><strong><?php echo$kategori ?></strong></td>
</tr>
</table>
<br />

<table id="table" class="tab" style="border-collapse:collapse" border="1" width="100%" bordercolor="#000000">
	<tr height="30" align="center">
        <td class="header" width="50" >No</td>
        <td class="header" width="10%">Kode</td>
        <td class="header" width="20%">Nama</td>
        <td class="header">Keterangan</td>
	</tr>
<?php 	OpenDb();
	$sql = "SELECT * FROM rekakun WHERE kategori = '$kategori' ORDER BY kode";
	$result = QueryDb($sql);
	$no = 0;
	while ($row = mysql_fetch_array($result)) {	?>
    <tr height="25">
    	<td align="center"><?php echo++$no ?></td>
        <td align="center"><?php echo$row['kode'] ?></td>
        <td><?php echo$row['nama'] ?></td>
        <td><?php echo$row['keterangan'] ?></td>
    </tr>
<?php 	} 
	CloseDb();	?>
    </table>

</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>