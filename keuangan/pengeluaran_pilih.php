<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('library/departemen.php');

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

OpenDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
<script language="javascript">
function show_content(id) {
	var dep = document.getElementById('departemen').value;
	parent.content.location.href = "pengeluaran_content.php?idpengeluaran="+id+"&departemen="+dep;
}

function change_dep() {
	var dep = document.getElementById('departemen').value;
	document.location.href = "pengeluaran_pilih.php?departemen="+dep;
	parent.content.location.href = "pengeluaran_blank.php";
}
</script>
</head>

<body onload="document.getElementById('departemen').focus()">
<table border="0" width="100%" align="center">
<tr>
    <td align="left" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
	<!-- TABLE LINK -->
	<tr>
    <td align="left" width="100%">
    <p><strong>Departemen&nbsp;</strong> 
    <select name="departemen" id="departemen" onchange="change_dep()" style="width:150px;">
<?php	$dep = getDepartemen(getAccess());
    foreach($dep as $value) {
        if ($departemen == "")
            $departemen = $value; ?>
        <option value="<?php echo$value ?>" <?php echo StringIsSelected($departemen, $value) ?>  > <?php echo$value ?></option>
<?php		} ?>            
    </select>
    </td>
</tr>
</table>
<br /><br />
<?php	$sql = "SELECT replid AS id, nama FROM datapengeluaran WHERE aktif = 1 AND departemen='$departemen' ORDER BY nama";
	$request = QueryDb($sql);
	if (mysql_num_rows($request) > 0) {
?>	
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
    <tr height="30">
        <td class="header" width="4%" align="center">No</td>
        <td class="header" width="*" align="center">Pengeluaran</td>
    </tr>
<?php
	$cnt = 0;
	while ($row = mysql_fetch_array($request)) { ?>
    <tr height="25" onClick="show_content(<?php echo$row['id'] ?>)" style="cursor:pointer;">
    	<td align="center"><?php echo++$cnt?></td>
        <td><a href="JavaScript:show_content(<?php echo$row['id'] ?>)"><?php echo$row['nama'] ?></a></td>
    </tr>
<?php	} //end while ?>
	</table>
    <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>
<?php	} else { ?>
	&nbsp;
   
	<table width="100%" border="0" align="center">          
	<tr>
		<td align="center" valign="middle" height="300">
    		<font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br /><br />Tambah data pengeluaran pada departemen <?php echo$departemen?> di menu Jenis Pengeluaran pada bagian Pengeluaran. </b></font>        
		</td>
	</tr>
	</table>  
	
<?php } ?> 
	</td>
</tr>
<!-- END TABLE CENTER -->    
</table>    

<?php
CloseDb();
?>
</body>
</html>
<script language="javascript">
	var spryselect1 = new Spry.Widget.ValidationSelect("departemen");
</script>