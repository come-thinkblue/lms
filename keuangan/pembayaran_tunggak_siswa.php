<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/common.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('library/departemen.php');

$idtahunbuku = $_REQUEST['idtahunbuku'];
$idkategori = $_REQUEST['idkategori'];
$idpenerimaan = $_REQUEST['idpenerimaan'];
$departemen = $_REQUEST['departemen'];

$status = "Siswa";
if ($idkategori == "CSWJB" || $idkategori == "CSSKR") 
	$status = "Calon Siswa";

$page = 1;
if (isset($_REQUEST['page']))
	$page = $_REQUEST['page'];

$nitem = 15;
$minno = ($page - 1) * $nitem;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<link href="script/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="script/SpryTabbedPanels.js" type="text/javascript"></script>
<script language="JavaScript" src="script/tables.js"></script>
<script src="script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script src="script/ajax.js" type="text/javascript"></script>
<script language="javascript">

function pilih(id) 
{	
	parent.content.location.href = "pembayaran_tunggak_decide.php?id="+id+"&idkategori=<?php echo$idkategori?>&idpenerimaan=<?php echo$idpenerimaan?>&idtahunbuku=<?php echo$idtahunbuku?>&status=<?php echo$status?>&departemen=<?php echo$departemen?>";		
}

function change_page()
{
	page = document.getElementById('page').value;
	document.location.href = "pembayaran_tunggak_siswa.php?page="+page+"&idkategori=<?php echo$idkategori?>&idpenerimaan=<?php echo$idpenerimaan?>&idtahunbuku=<?php echo$idtahunbuku?>&departemen=<?php echo$departemen?>";	;
}
</script>
</head>

<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" style="background-color:#FFFFFF">
<input type="hidden" id="idtahunbuku" value="<?php echo$idtahunbuku ?>" />
<input type="hidden" id="idkategori" value="<?php echo$idkategori ?>" />
<input type="hidden" id="idpenerimaan" value="<?php echo$idpenerimaan ?>" />
<input type="hidden" id="departemen" value="<?php echo$departemen ?>" />
<input type="hidden" id="status" value="<?php echo$status ?>" />
<?php
OpenDb();
if ($idkategori == "JTT")
{
	$sql = "SELECT COUNT(DISTINCT s.nis) 
			    FROM besarjtt b, penerimaanjtt p, jurnal j, dbakademik.siswa s
			   WHERE b.idpenerimaan = '$idpenerimaan' AND b.replid = p.idbesarjtt AND p.idjurnal = j.replid 
			     AND j.idtahunbuku = '$idtahunbuku' AND b.lunas = 0 AND b.nis = s.nis";
}
else
{
	$sql = "SELECT COUNT(DISTINCT cs.replid)  
			    FROM besarjttcalon b, penerimaanjttcalon p, jurnal j, dbakademik.calonsiswa cs
			   WHERE b.idpenerimaan = '$idpenerimaan' AND b.replid = p.idbesarjttcalon AND p.idjurnal = j.replid 
			     AND j.idtahunbuku = '$idtahunbuku' AND b.lunas = 0 AND b.idcalon = cs.replid";	
}

$ndata = FetchSingle($sql);
if ($ndata == 0)
{ ?>
	<table width="100%" align="center" cellpadding="2" cellspacing="0" border="0" id="table">
	<tr height="200" align="center"><td>   
	<br /><br />	
	<font size = "2" color ="red"><b>Tidak ditemukan adanya data</b></font>	
	<br /><br />
	</td></tr></table>
<?php	
}
else
{
?>
  <h3>Data <?php echo$status?> Yang Memiliki Tunggakan</h3>
  <table width="95%" id="table" class="tab" align="center" border="1" bordercolor="#000000">
  	<tr height="30" class="header" align="center">
		<td width="7%">No</td>
    	<td width="15%" background="style/formbg2.gif">N I S</td>
    	<td background="style/formbg2.gif">Nama</td>
  	</tr>
<?php 
	 if ($idkategori == "JTT")
	 {
			$sql = "SELECT DISTINCT b.nis, s.nama 
			 			FROM besarjtt b, penerimaanjtt p, jurnal j, dbakademik.siswa s
					   WHERE b.idpenerimaan = '$idpenerimaan' AND b.replid = p.idbesarjtt AND p.idjurnal = j.replid 
						 AND j.idtahunbuku = '$idtahunbuku' AND b.lunas = 0 AND b.nis = s.nis ORDER BY nama LIMIT $minno, $nitem";
	 }
	 else
	 {
			$sql = "SELECT DISTINCT cs.nopendaftaran, cs.nama 
						FROM besarjttcalon b, penerimaanjttcalon p, jurnal j, dbakademik.calonsiswa cs
						WHERE b.replid = p.idbesarjttcalon AND p.idjurnal = j.replid 
						 AND j.idtahunbuku = '$idtahunbuku' AND b.lunas = 0 AND b.idcalon = cs.replid ORDER BY nama LIMIT $minno, $nitem";	
	 }
	 $res = QueryDb($sql);
	 
    $cnt = $minno + 1;
	 while($row = mysql_fetch_row($res))
	 { ?>
    	<tr height="25" onClick="pilih('<?php echo$row[0]?>','<?php echo$row[1]?>')" style="cursor:pointer" id="siswapilih<?php echo$cnt?>">
	 		<td align="center"><?php echo$cnt ?></td>
			<td align="center"><?php echo$row[0] ?></td>
			<td align="left"><?php echo$row[1] ?></td>
	 	</tr>
<?php		$cnt++;
	} ?>
  	</table>
	&nbsp;&nbsp;Halaman
  	<select name="page" id="page" onchange="change_page()">
  	<?php 
	$npage = ceil($ndata / $nitem);
	for ($i = 1; $i <= $npage; $i++) { ?>
  		<option value="<?php echo$i?>" <?php echo IntIsSelected($page, $i)?> ><?php echo$i?></option>
<?php } ?>		
  	</select>&nbsp;dari&nbsp;<?php echo$npage?>
<?php  
}
CloseDb();
?>

</body>
</html>