<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('library/departemen.php');

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
	
$idangkatan = 0;
if (isset($_REQUEST['idangkatan']))
	$idangkatan = (int)$_REQUEST['idangkatan'];

$idkelas = 0;
if (isset($_REQUEST['idkelas']))
	$idkelas = (int)$_REQUEST['idkelas'];

OpenDb(); 
?>
<table border="0" cellspacing="3" cellpadding="0" width="80%">
<tr>
    <td width="100">Departemen:</td>
    <td>
    <input type="text" name="departemen" size="15" id="departemen" value="<?php echo$departemen ?>" readonly="readonly" style="background-color:#CCCC00" />
    </td>
</tr>    
<tr>
    <td width="100">Angkatan:</td>
    <td>
    <select id="idangkatan" name="idangkatan" style="width:150px" onchange="change_ang()">
<?php      $sql = "SELECT replid, angkatan FROM dbakademik.angkatan WHERE departemen = '$departemen' ORDER BY replid";
        $result = QueryDb($sql);
        while($row = mysql_fetch_row($result)) {
            if ($idangkatan == 0)
                $idangkatan = $row[0]; ?>
            <option value="<?php echo$row[0]?>" <?php echo IntIsSelected($row[0], $idangkatan)?> > <?php echo$row[1]?></option>
<?php     } ?>
    </select>
    </td>
</tr>    
<tr>
    <td width="100">Kelas:</td>
    <td>
    <select id="idkelas" name="idkelas" style="width:150px" onchange="change_kel()">
<?php      $sql = "SELECT DISTINCT idkelas, kelas as namakelas FROM dbakademik.siswa, dbakademik.kelas, dbakademik.tingkat, dbakademik.tahunajaran  WHERE dbakademik.siswa.idkelas = dbakademik.kelas.replid AND idangkatan='$idangkatan' AND dbakademik.kelas.idtahunajaran = dbakademik.tahunajaran.replid AND dbakademik.kelas.idtingkat = dbakademik.tingkat.replid ORDER BY idkelas";
        $result = QueryDb($sql);
        while($row = mysql_fetch_row($result)) {
            if ($idkelas == 0)
                $idkelas = $row[0];  ?>
            <option value="<?php echo$row[0]?>" <?php echo IntIsSelected($row[0], $idkelas)?> > <?php echo$row[1]?></option>
<?php      } ?>
    </select>
    </td>
</tr>    
</table>
<table border="0" id="table" class="tab" cellpadding="2" cellspacing="0" width="100%">
<tr height="20">
    <td class="header" width="5%" align="center">No</td>
    <td class="header" width="30%" align="center">NIS</td>
    <td class="header">Nama</td>
</tr>
<?php 
$sql = "SELECT nis, nama FROM dbakademik.siswa WHERE idkelas = '$idkelas' ORDER BY nama";
$result = QueryDb($sql);
$no = 0;
while ($row = mysql_fetch_array($result)) {
?>
<input type="hidden" name="isnew<?php echo$no?>" id="isnew<?php echo$no?>" value="<?php echo$isnew ?>" />
<tr height="25">
    <td align="center"><font size="1"><?php echo++$no ?></font></td>
    <td align="center"><font size="1">
    <a href="JavaScript:show_bayar('<?php echo$row['nis'] ?>')"><?php echo$row['nis'] ?></a></font></td>
    <td><font size="1"><?php echo$row['nama'] ?></font></td>
</tr>
<?php
}
?>
</table>
<?php  CloseDb() ?>