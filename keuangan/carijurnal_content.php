<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('library/departemen.php');

$urut = "nokas";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	

$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];

$varbaris=30;
if (isset($_REQUEST['varbaris']))
	$varbaris = $_REQUEST['varbaris'];

$page=0;
if (isset($_REQUEST['page']))
	$page = $_REQUEST['page'];

$hal=0;
if (isset($_REQUEST['hal']))
	$hal = $_REQUEST['hal'];

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$idtahunbuku = 0;
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = (int)$_REQUEST['idtahunbuku'];

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];

$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];

$keyword = "";
if (isset($_REQUEST['keyword']))
	$keyword = $_REQUEST['keyword'];
	
$kriteria = "";
if (isset($_REQUEST['kriteria']))
	$kriteria = $_REQUEST['kriteria'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pencarian Jurnal</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
<script language="javascript" src="script/tooltips.js"></script>
<script language="javascript">
function edit(id) {
	var addr = "editjurnal.php?idjurnal="+id+"&departemen=<?php echo$departemen?>&jurnal=Umum";
	newWindow(addr, 'EditJurnal','680','630','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function refresh() {
	document.location.href = "carijurnal_content.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&keyword=<?php echo urlencode($keyword)?>";
}

function cetak() {
	var total=document.getElementById("total").value;
	var kriteria=document.getElementById("kriteria").value;
	var keyword=document.getElementById("keyword").value;
	
	if (keyword.length == 0)
		kriteria = "all"
			
	var addr = "carijurnal_cetak.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku ?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria="+kriteria+"&keyword=<?php echo urlencode($keyword)?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>&total="+total+"&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>";
	newWindow(addr, 'CetakCariJurnalUmum','780','580','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function excel() {
	var total=document.getElementById("total").value;
	var kriteria=document.getElementById("kriteria").value;
	var keyword=document.getElementById("keyword").value;
	
	if (keyword.length == 0)
		kriteria = "all"
			
	var addr = "carijurnal_excel.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku ?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria="+kriteria+"&keyword=<?php echo urlencode($keyword)?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>&total="+total;
	newWindow(addr, 'ExcelCariJurnalUmum','780','580','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function change_page(page) {
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href = "carijurnal_content.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&keyword=<?php echo$keyword?>&kriteria=<?php echo$kriteria?>&page="+page+"&hal="+page+"&varbaris="+varbaris;
}

function change_hal() {
	var hal = document.getElementById("hal").value;
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="carijurnal_content.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria=<?php echo$kriteria?>&keyword=<?php echo$keyword?>&page="+hal+"&hal="+hal+"&varbaris="+varbaris;
}

function change_baris() {
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="carijurnal_content.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria=<?php echo$kriteria?>&keyword=<?php echo$keyword?>&varbaris="+varbaris;
}
function change_urut(urut,urutan) {		
	if (urutan =="ASC"){
		urutan="DESC"
	} else {
		urutan="ASC"
	}
	var hal = document.getElementById("hal").value;
	var varbaris=document.getElementById("varbaris").value;
	var addr = "carijurnal_content.php?departemen=<?php echo$departemen?>&idtahunbuku=<?php echo$idtahunbuku?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria=<?php echo$kriteria?>&keyword=<?php echo$keyword?>&page="+hal+"&hal="+hal+"&varbaris="+varbaris+"&urut="+urut+"&urutan="+urutan;
	
	document.location.href = addr;
}
</script>
</head>
<body topmargin="0" leftmargin="0">
<input type="hidden" name="kriteria" id="kriteria" value="<?php echo$kriteria?>"/>
<input type="hidden" name="keyword" id="keyword" value="<?php echo$keyword?>"/>
<table border="0" width="100%" align="center" background="" style="background-repeat:no-repeat; background-attachment:fixed">
<!-- TABLE CENTER -->
<tr>
	<td>
    <?php
	OpenDb();
	
	if ($kriteria == '1')
		$sql_tot = "SELECT * FROM jurnal WHERE transaksi LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY tanggal";
	else if ($kriteria == '2')
		$sql_tot = "SELECT * FROM jurnal WHERE nokas LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY tanggal";
	else if ($kriteria == '3')
		$sql_tot = "SELECT * FROM jurnal WHERE keterangan LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY tanggal";
	else if ($kriteria == '4')
		$sql_tot = "SELECT * FROM jurnal WHERE petugas LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY tanggal";
	else if ($kriteria == "all")
		$sql_tot = "SELECT * FROM jurnal WHERE idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY tanggal";
	$result_tot = QueryDb($sql_tot);
	$total=ceil(mysql_num_rows($result_tot)/(int)$varbaris);
	$jumlah = mysql_num_rows($result_tot);
	$akhir = ceil($jumlah/5)*5;
	
	if ($kriteria == '1')
		$sql = "SELECT * FROM jurnal WHERE transaksi LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	else if ($kriteria == '2')
		$sql = "SELECT * FROM jurnal WHERE nokas LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	else if ($kriteria == '3')
		$sql = "SELECT * FROM jurnal WHERE keterangan LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	else if ($kriteria == '4')
		$sql = "SELECT * FROM jurnal WHERE petugas LIKE '%$keyword%' AND idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	else if ($kriteria == "all")
		$sql = "SELECT * FROM jurnal WHERE idtahunbuku='$idtahunbuku' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	
	$result = QueryDb($sql);
	if (mysql_num_rows($result) > 0) {
	?>
    <input type="hidden" name="total" id="total" value="<?php echo$total?>"/>
    <table border="0" width="100%" align="center">
    <tr>
        <td align="right">
        <a href="#" onClick="document.location.reload()"><img src="images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
        <a href="JavaScript:cetak()"><img src="images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp;
        <a href="JavaScript:excel()"><img src="images/ico/excel.png" border="0" onMouseOver="showhint('Buka di Ms Excel!', this, event, '50px')"/>&nbsp;Excel</a>&nbsp;
        </td>
    </tr>
    </table>
    <br />
    <table border="1" style="border-collapse:collapse" cellpadding="5" cellspacing="0" width="100%" class="tab" bordercolor="#000000">
    <tr height="30" align="center">
        <td width="4%" class="header">No</td>
        <td width="15%" class="header" onClick="change_urut('nokas','<?php echo$urutan?>')">No. Jurnal/Tanggal <?php echo change_urut('nokas',$urut,$urutan)?></td>  
        <td width="35%" class="header">Transaksi</td>
        <td class="header">Detail Jurnal</td>  
        <?php	if ((getLevel() != 2)) { ?> 
        <td width="3%" class="header">&nbsp;</td>
        <?php } ?> 
    </tr>
    <?php
    if ($page==0){
			$cnt = 1;
		}else{ 
			$cnt = (int)$page*(int)$varbaris+1;
		}
	
	
	while ($row = mysql_fetch_array($result)) {
		if ($cnt % 2 == 0)
			$bgcolor = "#FFFFB7";
		else
			$bgcolor = "#FFFFB7";
			
		 switch($row['sumber']) {	
            case 'jurnalumum':
                $jurnal = "Jurnal Umum"; break;
            case 'penerimaanjtt':
                $jurnal = "Penerimaan Iuran Wajib Siswa"; break;
            case 'penerimaaniuran':
                $jurnal = "Penerimaan Iuran Sukarela Siswa"; break;
            case 'penerimaanlain':
                $jurnal = "Penerimaan Lain-Lain"; break;
            case 'pengeluaran':
                $jurnal = "Pengeluaran"; break;
			case 'penerimaanjttcalon':
                $jurnal = "Penerimaan Iuran Wajib Calon Siswa"; break;
			case 'penerimaaniurancalon':
                $jurnal = "Penerimaan Iuran Sukarela Calon Siswa"; break;
        }
	?>
    <tr height="25">
        <td align="center" rowspan="2" bgcolor="<?php echo$bgcolor ?>"><font size="4"><strong><?php echo$cnt ?></strong></font></td>
        <td align="center" bgcolor="<?php echo$bgcolor ?>"><strong><?php echo$row['nokas']?></strong><br /><em><?php echo LongDateFormat($row['tanggal'])?></em></td>
        <td valign="top" bgcolor="<?php echo$bgcolor ?>"><?php echo$row['transaksi'] ?>
    <?php	if (strlen($row['keterangan']) > 0 )  { ?>
            <br /><strong>Keterangan:</strong><?php echo$row['keterangan'] ?> 
    <?php	} ?>    
        </td>
        <td rowspan="2" valign="top" bgcolor="#E8FFE8">    
        
            <table border="1" style="border-collapse:collapse" width="100%" height="100%" cellpadding="2" bgcolor="#FFFFFF" bordercolor="#000000">    
        <?php	$idjurnal = $row['replid'];
            $sql = "SELECT jd.koderek,ra.nama,jd.debet,jd.kredit FROM jurnaldetail jd, rekakun ra WHERE jd.idjurnal = '$idjurnal' AND jd.koderek = ra.kode ORDER BY jd.replid";    
            $result2 = QueryDb($sql); 
            while ($row2 = mysql_fetch_array($result2)) { ?>
            <tr height="25">
                <td width="8%" align="center"><?php echo$row2['koderek'] ?></td>
                <td width="*" align="left"><?php echo$row2['nama'] ?></td>
                <td width="23%" align="right"><?php echo formatRupiah($row2['debet']) ?></td>
                <td width="23%" align="right"><?php echo formatRupiah($row2['kredit']) ?></td>
            </tr>
        <?php	} ?>    
            </table>
        
        </td>
    <?php	if ((getLevel() != 2)) { ?>    
        <td rowspan="2" align="center">
    	<?php	if ($row['sumber'] == "jurnalumum") { ?>
            <a href="JavaScript:edit(<?php echo$idjurnal ?>)"><img src="images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Jurnal Umum!', this, event, '80px')"/></a>
    	<?php	} else {?>
    		<img src="images/ico/ubah_x.png" border="0" onMouseOver="showhint('Ubah Jurnal pada Jurnal <?php echo substr($jurnal,0,11)?>!', this, event, '120px')"/>
    	<?php 	} ?>
        </td>
    <?php } ?>
    </tr>
    <tr>    
        <td valign="top"><strong>Petugas: </strong><?php echo$row['petugas'] ?></td>
        <td valign="top">
        <strong>Sumber: </strong><?php echo$jurnal?>
   
        </td>
    </tr>
    <tr style="height:2px">
        <td colspan="5" bgcolor="#EFEFDE"></td>
    </tr>
    <?php
            $cnt++;
    }
    CloseDb();
    ?>
    </table>
    <?php	if ($page==0){ 
		$disback="style='display:none;'";
		$disnext="style=''";
		}
		if ($page<$total && $page>0){
		$disback="style=''";
		$disnext="style=''";
		}
		if ($page==$total-1 && $page>0){
		$disback="style=''";
		$disnext="style='display:none;'";
		}
		if ($page==$total-1 && $page==0){
		$disback="style='display:none;'";
		$disnext="style='display:none;'";
		}
	?>
    </td>
</tr> 
<tr>
    <td>
    <table border="0"width="100%" align="center"cellpadding="0" cellspacing="0">	
    <tr>
       	<td width="30%" align="left" colspan="2">Halaman
        <input <?php echo$disback?> type="button" class="but" name="back" value=" << " onClick="change_page('<?php echo(int)$page-1?>')" onMouseOver="showhint('Sebelumnya', this, event, '75px')">
        <select name="hal" id="hal" onChange="change_hal()">
        <?php	for ($m=0; $m<$total; $m++) {?>
             <option value="<?php echo$m ?>" <?php echo IntIsSelected($hal,$m) ?>><?php echo$m+1 ?></option>
        <?php } ?>
     	</select>
        <input <?php echo$disnext?> type="button" class="but" name="next" value=" >> " onClick="change_page('<?php echo(int)$page+1?>')" onMouseOver="showhint('Berikutnya', this, event, '75px')">
	  	dari <?php echo$total?> halaman
		
		<?php 
     // Navigasi halaman berikutnya dan sebelumnya
        ?>
        
        
            <?php
            //for($a=0;$a<$total;$a++){
            //        if ($page==$a){
            //            echo  "<font face='verdana' color='red'><strong>".($a+1)."</strong></font> "; 
            //        }				
            //        else 
            //            { echo  "<a href='#' onClick=\"change_page('".$a."')\">".($a+1)."</a> "; 
            //        }
            //         
            //}
            ?>
             
  		<td width="30%" align="right"><!--Jumlah baris per halaman
      	<select name="varbaris" id="varbaris" onChange="change_baris()">
        <?php 	for ($m=20; $m <= $akhir; $m=$m+5) { ?>
        	<option value="<?php echo$m ?>" <?php echo IntIsSelected($varbaris,$m) ?>><?php echo$m ?></option>
        <?php 	} ?>
       
      	</select>-->
		<input type="hidden" name="varbaris" id="varbaris" value="<?php echo$varbaris?>">

		</td>
    </tr>
    </table>
<?php } else { ?>
    <table width="100%" border="0" align="center">          
    <tr>
        <td align="center" valign="middle" height="300">
            <font size = "2" color ="red"><b>Tidak ditemukan adanya data <br />
            Tambah data jurnal umum pada departemen <?php echo$departemen?> antara tanggal 
            <?php echo LongDateFormat($tanggal1)?> s/d <?php echo LongDateFormat($tanggal2)?><br />
            di Input Jurnal Umum pada bagian Jurnal Umum.  </font>
        </td>
    </tr>
    </table>  
<?php } ?>
	</td>
</tr>
</table>
</body>
</html>