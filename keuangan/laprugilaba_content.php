<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];
	
$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];
	
$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$idtahunbuku = 0;
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = (int)$_REQUEST['idtahunbuku'];

OpenDb();	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<title>Untitled Document</title>
<script language="javascript" src="script/tooltips.js"></script>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
<script language="javascript">
function cetak() {
	var addr = "laprugilaba_cetak.php?departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&idtahunbuku=<?php echo$idtahunbuku?>";
	newWindow(addr, 'RugiLaba','790','630','resizable=1,scrollbars=1,status=0,toolbar=0');
}
</script>
</head>

<body>
<table border="0" width="100%" align="center" background="" style="background-repeat:no-repeat; background-attachment:fixed">
<!-- TABLE CENTER -->
<tr>
	<td>
<?php	
	OpenDb();
	
   	$sql = "SELECT nama, kode, SUM(debet) AS debet, SUM(kredit) AS kredit FROM (
			  (SELECT DISTINCT j.replid, ra.nama, ra.kode, jd.debet, jd.kredit 
			     FROM rekakun ra, katerekakun k, jurnal j, jurnaldetail jd 
			     WHERE jd.idjurnal = j.replid AND jd.koderek = ra.kode AND j.idtahunbuku = '$idtahunbuku' 
			     AND j.tanggal BETWEEN '$tanggal1' AND '$tanggal2' AND ra.kategori = 'PENDAPATAN' 
			     GROUP BY j.replid, ra.nama, ra.kode 
			     ORDER BY ra.kode) AS X
			) GROUP BY nama, kode";	
    $result = QueryDb($sql);
	
	$sql1 = "SELECT nama, kode, SUM(debet) AS debet, SUM(kredit) AS kredit FROM (
	           (SELECT DISTINCT j.replid, ra.nama, ra.kode, jd.debet, jd.kredit 
			      FROM rekakun ra, katerekakun k, jurnal j, jurnaldetail jd 
				  WHERE jd.idjurnal = j.replid AND jd.koderek = ra.kode AND j.idtahunbuku = '$idtahunbuku' 
				  AND j.tanggal BETWEEN '$tanggal1' AND '$tanggal2' AND ra.kategori = 'BIAYA' 
				  GROUP BY j.replid, ra.nama, ra.kode 
				  ORDER BY ra.kode) AS X
		     ) GROUP BY nama, kode";
    $result1 = QueryDb($sql1);
	
	if ((mysql_num_rows($result) > 0) || (mysql_num_rows($result1) > 0)) 
	{ ?>
    
    <table border="0" width="80%" align="center">
    <tr>
        <td align="right">
        <a href="#" onClick="document.location.reload()"><img src="images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
        <a href="JavaScript:cetak()"><img src="images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;
        </td>
    </tr>
    </table>
    </br>
    <table border="0" cellpadding="5" cellspacing="5" width="80%" style="background-image:url(../img/bttablelong.png); background-repeat:repeat-x" bgcolor="#eef5dd" align="center">
    <tr height="30">
        <td colspan="6"><strong><font size="2">PENDAPATAN</font></strong></td>
    </tr>
    <?php
  	$cnt = 0;
	$totalpendapatan = 0;
	if (mysql_num_rows($result) > 0) 
	{
		while($row = mysql_fetch_array($result)) 
		{
			$debet = $row['kredit'] - $row['debet'];
			$debet = FormatRupiah($debet);
			$kredit = "$nbsp";
			
			$totalpendapatan += ($row['kredit'] - $row['debet']);  ?>
            
            <tr height="25">
                <td width="2%" align="right">&nbsp;</td>
                <td width="5%" align="left" valign="top"><?php echo$row['kode'] ?></td>
                <td align="left" width="*" valign="top"><?php echo$row['nama'] ?></td>
                <td align="right" width="18%" valign="top"><?php echo$debet ?></td> 
                <td align="right" width="18%" valign="top"><?php echo$kredit ?></td>
                <td width="20%">&nbsp;</td>
            </tr>
    <?php } //end while  
	}
	?>
    <tr height="30">
        <td>&nbsp;</td>
        <td colspan="4"><strong>SUB TOTAL PENDAPATAN</strong></td>
        <td align="right"><strong><?php echo formatRupiah($totalpendapatan) ?></strong></td>
    </tr>
    <tr height="5">
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr height="30">
        <td colspan="6"><strong><font size="2">BIAYA</font></strong></td>
    </tr>
    <?php
   
    $cnt = 0;
    $totalbiaya = 0;
	if (mysql_num_rows($result1) >0) {
		while($row = mysql_fetch_array($result1)) {
			$kredit = $row['debet'] - $row['kredit'];
			$kredit = FormatRupiah($kredit);
			$debet = "$nbsp";
			
			$totalbiaya += ($row['debet'] - $row['kredit']);
    ?>
    <tr height="25">
        <td width="2%" align="right">&nbsp;</td>
        <td width="5%" align="left" valign="top"><?php echo$row['kode'] ?></td>
        <td align="left" width="*" valign="top"><?php echo$row['nama'] ?></td>
        <td align="right" width="18%" valign="top"><?php echo$debet ?></td> 
        <td align="right" width="18%" valign="top"><?php echo$kredit ?></td>
        <td width="20%">&nbsp;</td>
    </tr>
    <?php  } //end while  
	}
	?>
    
    <tr height="30">
        <td>&nbsp;</td>
        <td colspan="4"><strong>SUB TOTAL BIAYA</strong></td>
        <td align="right"><strong><?php echo formatRupiah($totalbiaya) ?></strong></td>
    </tr>
    <tr height="5">
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">
        	<strong><font size="4"><?php if ($totalpendapatan < $totalbiaya) echo  "RUGI"; else echo  "LABA"; ?>
            </strong></font></td>
        <td colspan="2" align="right"><strong><font size="4"><?php echo formatRupiah($totalpendapatan - $totalbiaya) ?></font></strong></td>
    </tr>
    </table>
<?php } else { ?>
    <table width="100%" border="0" align="center">          
    <tr>
        <td align="center" valign="middle" height="300">
            <font size = "2" color ="red"><b>Tidak ditemukan adanya data transaksi keuangan pada departemen <?php echo$departemen?> antara tanggal <?php echo LongDateFormat($tanggal1)?> s/d <?php echo LongDateFormat($tanggal2)?><br />.</font>
            
        </td>
    </tr>
    </table>  
<?php } ?>
    </td>
</tr>
</table>
</body>
</html>