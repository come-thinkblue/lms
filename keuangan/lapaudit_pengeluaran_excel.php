<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Laporan_Audit_Pengeluaran.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];

$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];

$idtahunbuku = 0;
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = $_REQUEST['idtahunbuku'];	

$tgl1 = explode(' ',$tanggal1);
$tgl2 = explode(' ',$tanggal2);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Laporan Audit Perubahan Data Pengeluaran]</title>
</head>

<body>
<center><font size="4"><strong><font face="Verdana">LAPORAN AUDIT PERUBAHAN DATA PENGELUARAN</font></strong></font><font face="Verdana"><br /> 
  </font>
</center>
<br /><br />

<table border="0">
<tr>
	<td width="90"><font size="2" face="Arial"><strong>Departemen </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$departemen ?>
    </strong></font></td>
</tr>
<tr>
	<td width="90"><font size="2" face="Arial"><strong>Tanggal </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo LongDateFormat($tgl1[0]) . " s/d 	" . LongDateFormat($tgl2[0]) ?>
    </strong></font></td>
</tr>
</table>
<br />
<table class="tab" id="table" border="1" width="100%" align="left" cellpadding="5" cellspacing="0">
<tr height="30" align="center">
	<td width="4%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">No</font></strong></td>
    <td width="15%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Status Data</font></strong></td>
    <td width="10%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Tanggal</font></strong></td>
    <td width="15%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Jumlah</font></strong></td>
    <td width="*" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Keterangan</font></strong></td>
    <td width="15%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Petugas</font></strong></td>
</tr>
<?php
OpenDb();
$sql = "SELECT DISTINCT ai.petugas as petugasubah, j.transaksi, date_format(ai.tanggal, '%d-%b-%Y %H:%i:%s') as tanggalubah, ap.replid AS id, 
					ap.idaudit, ap.statusdata, j.nokas, date_format(ap.tanggal, '%d-%b-%Y') AS tanggal, ap.petugas, ap.keterangan, ap.jumlah, 
					ap.petugas, ai.alasan 
			 FROM auditpengeluaran ap, auditinfo ai, jurnal j 
			WHERE j.replid = ap.idjurnal AND j.idtahunbuku = '$idtahunbuku' AND ap.idaudit = ai.replid AND ai.departemen = '$departemen' 
			  AND ai.sumber='pengeluaran' AND ai.tanggal BETWEEN '$tanggal1 00:00:00' AND '$tanggal2 23:59:59'
		ORDER BY ap.idaudit DESC, ai.tanggal DESC, ap.statusdata ASC";
$result = QueryDb($sql);
$cnt = 0;
$no = 0;
while ($row = mysql_fetch_array($result)) {
	$statusdata = "Data Lama";
	$bgcolor = "#FFFFFF";
	if ($row['statusdata'] == 1) {
		$statusdata = "Data Perubahan";
		$bgcolor = "#FFFFB7";
	}
		
	if ($cnt % 2 == 0) { ?>
	<tr>
		<td rowspan="4" align="center" bgcolor="#CCCC66"><font size="2" face="Arial"><strong>
	    <?php echo++$no ?>
		</strong></font></td>
      <td colspan="6" align="left" bgcolor="#CCCC66"><font size="2" face="Arial"><em><strong>Perubahan dilakukan oleh 
        <?php echo$row['petugasubah'] . " tanggal " . $row['tanggalubah'] ?>
        </strong></em></font></td>
  </tr>
    <tr>
    	<td colspan="6" bgcolor="#E5E5E5"><font size="2" face="Arial"><strong>No. Jurnal :</strong> 
        <?php echo$row['nokas'] ?>
&nbsp;&nbsp;<strong>Alasan : </strong>
<?php echo$row['alasan'];?>
        <br />
        <strong>Transaksi :</strong> 
        <?php echo$row['transaksi'] ?>
    	</font></td>
  </tr>
<?php  } ?>
	<tr bgcolor="<?php echo$bgcolor?>">
		<td><font size="2" face="Arial">
	    <?php echo$statusdata ?>
		</font></td>
<td align="center"><font size="2" face="Arial">
        <?php echo$row['tanggal'] ?>
	    </font></td>
<td align="right"><font size="2" face="Arial">
        <?php echo$row['jumlah'] ?>
	    </font></td>
<td><font size="2" face="Arial">
        <?php echo$row['keterangan'] ?>
	    </font></td>
<td align="center"><font size="2" face="Arial">
        <?php echo$row['petugas']; ?>
	    </font></td>
  </tr>
<?php
	$cnt++;
}
CloseDb();
?>
</table>
</body>
</html>
<script language="javascript">window.print();</script>