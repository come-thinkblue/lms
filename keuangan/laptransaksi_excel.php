<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Laporan_Transaksi.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');


$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];
	
$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];
	
$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$kategori = "";
if (isset($_REQUEST['kategori']))
	$kategori = $_REQUEST['kategori'];

$idtahunbuku = 0;
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = (int)$_REQUEST['idtahunbuku'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Laporan Transaksi]</title>
</head>

<body>
<center><font size="4" face="Verdana"><strong>LAPORAN TRANSAKSI</strong></font><br /> 
</center>
<br /><br />
<table border="0">
<tr>
	<td width="90"><font size="2" face="Arial"><strong>Departemen </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$departemen ?>
    </strong></font></td>
</tr>
<tr>
	<td width="90"><font size="2" face="Arial"><strong>Tahun Buku </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php  OpenDb();
		$sql = "SELECT tahunbuku FROM tahunbuku WHERE replid = $idtahunbuku";
	   	$result = QueryDb($sql);
	   	$row = mysql_fetch_row($result);
	   	echo  $row[0];
    ?>
	</strong></font></td>
</tr>
<tr>
	<td><font size="2" face="Arial"><strong>Tanggal </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo LongDateFormat($tanggal1) ?> 
      s/d 
      <?php echo LongDateFormat($tanggal2) ?>
    </strong></font></td>
</tr>
</table>
<br />
<table class="tab" border="1" cellpadding="5" style="border-collapse:collapse" cellspacing="0" width="100%" align="left" bordercolor="#000000">
<tr height="30" align="center">
	<td width="4%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">No</font></strong></td>
    <td width="18%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">No. Jurnal/Tanggal</font></strong></td>
    <td width="8%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Petugas</font></strong></td>
    <td width="*" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Transaksi</font></strong></td>
    <td width="15%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Debet</font></strong></td>
    <td width="15%" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Kredit</font></strong></td>
</tr>
<?php
OpenDb();
$sql = "SELECT nokas, date_format(tanggal, '%d-%b-%Y') AS tanggal, petugas, transaksi, keterangan, debet, kredit FROM transaksilog WHERE departemen='$departemen' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' AND idtahunbuku = '$idtahunbuku'";
$result = QueryDb($sql);
$cnt = 0;
$totaldebet = 0;
$totalkredit = 0;
while($row = mysql_fetch_array($result)) {
	$bg1="#ffffff";
	if ($cnt==0 || $cnt%2==0)
		$bg1="#fcffd3";
	$totaldebet += $row['debet'];
	$totalkredit += $row['kredit'];
?>
<tr height="25" bgcolor="<?php echo$bg1?>">
	<td align="center" valign="top"><font size="2" face="Arial">
	  <?php echo++$cnt ?>
	</font></td>
    <td align="center" valign="top"><font size="2" face="Arial"><strong>
      <?php echo$row['nokas'] ?>
    </strong><br />
    <?php echo$row['tanggal'] ?>
    </font></td>
    <td align="center" valign="top"><font size="2" face="Arial">
      <?php echo$row['petugas'] ?>
    </font></td>
    <td align="left" valign="top"><font size="2" face="Arial">
      <?php echo$row['transaksi'] ?>
      <?php if ($row['keterangan'] <> "") { ?>
      <br />
      <strong>Keterangan: </strong>
      <?php echo$row['keterangan'] ?>
      <?php } ?>    
    </font></td>
    <td align="right" valign="top"><font size="2" face="Arial">
      <?php echo$row['debet'] ?>
    </font></td>
    <td align="right" valign="top"><font size="2" face="Arial">
      <?php echo$row['kredit'] ?>
    </font></td>
</tr>
<?php
}
CloseDb();
?>
<tr height="30">
	<td colspan="4" align="center" bgcolor="#999900">
    <font color="#FFFFFF" size="2" face="Arial"><strong>T O T A L</strong></font>    </td>
    <td align="right" bgcolor="#999900"><font color="#FFFFFF" size="2" face="Arial"><strong><?php echo$totaldebet ?></strong></font></td>
    <td align="right" bgcolor="#999900"><font color="#FFFFFF" size="2" face="Arial"><strong><?php echo$totalkredit ?></strong></font></td>
</tr>
</table>

</td></tr></table>

<script language="javascript">window.print();</script>

</body>
</html>