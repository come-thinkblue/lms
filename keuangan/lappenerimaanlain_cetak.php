<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

$urut=$_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];

$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];
	
$idpenerimaan = 0;
if (isset($_REQUEST['idpenerimaan']))
	$idpenerimaan = (int)$_REQUEST['idpenerimaan'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Penerimaan Lain</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>


<?php
OpenDb();
$sql = "SELECT nama FROM datapenerimaan WHERE replid= $idpenerimaan";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$namapenerimaan = $row[0];
?>

<center><font size="4"><strong>LAPORAN PENERIMAAN <?php echo strtoupper($namapenerimaan) ?></strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td width="90"><strong>Departemen </strong></td>
    <td><strong>: <?php echo$departemen ?></strong></td>
</tr>
<tr>
	<td width="90"><strong>Tanggal </strong></td>
    <td><strong>:  <?php echo LongDateFormat($tanggal1) . " s/d " . LongDateFormat($tanggal2) ?></strong></td>
</tr>

</table>
<br />

<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
<tr height="30" align="center">
	<td class="header" width="5%" align="center">No</td>
    <td class="header" width="15%">Tanggal</td>
  	<td class="header" width="15%">Sumber</td>
    <td class="header" width="15%" align="right">Jumlah</td>
    <td class="header" width="25%">Keterangan</td>
    <td class="header" width="10%">Petugas</td>
</tr>
<?php 
OpenDb();
$sql = "SELECT p.replid AS id, j.nokas, p.sumber, date_format(p.tanggal, '%d-%b-%Y') AS tanggal, p.keterangan, p.jumlah, p.petugas FROM penerimaanlain p, jurnal j, datapenerimaan dp WHERE j.replid = p.idjurnal AND p.idpenerimaan = dp.replid AND p.idpenerimaan = '$idpenerimaan' AND dp.departemen = '$departemen' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
//echo  $sql;
$result = QueryDb($sql);
//if ($page==0)
	$cnt = 0;
//else 
//	$cnt = (int)$page*(int)$varbaris;
$tot = 0;
while ($row = mysql_fetch_array($result)) {
	$tot += $row['jumlah'];
?>
<tr height="25">
	<td align="center"><?php echo++$cnt?></td>
    <td align="center"><?php echo"<strong>" . $row['nokas'] . "</strong><br>" . $row['tanggal']?></td>
    <td align="left"><?php echo$row['sumber'] ?></td>
    <td align="right"><?php echo formatRupiah($row['jumlah'])?></td>
    <td><?php echo$row['keterangan'] ?></td>
    <td><?php echo$row['petugas'] ?></td>
</tr>
<?php
}
?>
<tr height="35">
	<td bgcolor="#996600" colspan="3" align="center"><font color="#FFFFFF"><strong>T O T A L</strong></font></td>
    <td bgcolor="#996600" align="right"><font color="#FFFFFF"><strong><?php echo formatRupiah($tot) ?></strong></font></td>
    <td bgcolor="#996600" colspan="2">&nbsp;</td>
</tr>
</table>


</td>
</tr>
    </table>
</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>