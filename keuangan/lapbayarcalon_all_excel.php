<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 
require_once('library/jurnal.php');
require_once('library/repairdatajttcalon.php');

$idtahunbuku = $_REQUEST['idtahunbuku'];
$replid = $_REQUEST['replid'];
$tanggal1 = $_REQUEST['tanggal1'];
$tanggal2 = $_REQUEST['tanggal2'];

OpenDb();

$sql = "SELECT s.nama, s.nopendaftaran, k.kelompok, p.proses, p.departemen 
          FROM dbakademik.calonsiswa s, dbakademik.kelompokcalonsiswa k, dbakademik.prosespenerimaansiswa p 
			WHERE s.replid = '$replid' AND s.idkelompok = k.replid AND s.idproses = p.replid";
$row = FetchSingleRow($sql);
$namacalon = $row[0];
$kelompok = $row[2];
$proses = $row[3];
$no = $row[1];
$departemen = $row[4];
/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Data_Pembayaran_Calon_Siswa_'.$no.'.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Pembayaran Per Siswa</title>
</head>

<body>
<center><font size="4"><strong><font face="Verdana">DATA PEMBAYARAN CALON SISWA</font></strong></font><font face="Verdana"><br /> 
  </font>
</center>
<br /><br />
<table border="0">
<tr>
	<td><font size="2" face="Arial"><strong>Calon Siswa </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$no . " - " . $namacalon?>
    </strong></font></td>
</tr>
<tr>
	<td><font size="2" face="Arial"><strong>Proses </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$proses?>
    </strong></font></td>
</tr>
<tr>
	<td><font size="2" face="Arial"><strong>Kelompok </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$kelompok?>
    </strong></font></td>
</tr>

<tr>
	<td><font size="2" face="Arial"><strong>Tanggal </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo LongDateFormat($tanggal1) . " s/d " . LongDateFormat($tanggal2) ?>
    </strong></font></td>
</tr>
</table>
<br />

<table border="1" style="border-collapse:collapse" width="100%" bordercolor="#000000"> 
<?php
$sql = "SELECT DISTINCT b.replid AS id, b.besar, b.lunas, b.keterangan, d.nama 
          FROM besarjttcalon b, penerimaanjttcalon p, datapenerimaan d 
			WHERE p.idbesarjttcalon = b.replid AND b.idpenerimaan = d.replid AND b.idcalon='$replid' AND b.info2='$idtahunbuku'
			  AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY nama";
$result = QueryDb($sql);
while ($row = mysql_fetch_array($result))
{
	$idbesarjtt = $row['id'];
	$namapenerimaan = $row['nama']; 
	$besar = $row['besar'];
	$lunas = $row['lunas'];
	$keterangan = $row['keterangan'];
	
	$sql = "SELECT SUM(jumlah), SUM(info1) FROM penerimaanjttcalon WHERE idbesarjttcalon = '$idbesarjtt'";
	$row2 = FetchSingleRow($sql);
	$pembayaran = $row2[0] + $row2[1];
	$diskon = $row2[1];
	$sisa = $besar - $pembayaran;
	
	$sql = "SELECT jumlah, DATE_FORMAT(tanggal, '%d-%b-%Y') AS ftanggal, info1 FROM penerimaanjttcalon WHERE idbesarjttcalon='$idbesarjtt' ORDER BY tanggal DESC LIMIT 1";
	$result2 = QueryDb($sql);
	$byrakhir = 0;
	$dknakhir = 0;
	$tglakhir = "";
	if (mysql_num_rows($result2)) {
		$row2 = mysql_fetch_row($result2);
		$byrakhir = $row2[0];
		$tglakhir = $row2[1];
		$dknakhir = $row2[2];
	};	?>
    <tr height="35">
        <td colspan="4" bgcolor="#99CC00"><font size="2" face="Arial"><strong><em><?php echo$namapenerimaan?></em></strong></font></td>
  </tr>    
    <tr height="25">
        <td width="20%" bgcolor="#CCFF66"><font size="2" face="Arial"><strong>Total Bayaran</strong> </font></td>
<td width="15%" bgcolor="#FFFFFF" align="right"><font size="2" face="Arial">
        <?php echo$besar ?>
        </font></td>
      <td width="22%" bgcolor="#CCFF66" align="center"><font size="2" face="Arial"><strong>Pembayaran Terakhir</strong></font></td>
      <td width="43%" bgcolor="#CCFF66" align="center"><font size="2" face="Arial"><strong>Keterangan</strong></font></td>
  </tr>
    <tr height="25">
        <td bgcolor="#CCFF66"><font size="2" face="Arial"><strong>Jumlah Besar Pembayaran</strong> </font></td>
<td bgcolor="#FFFFFF" align="right"><font size="2" face="Arial">
        <?php echo$pembayaran ?>
        </font></td>
<td bgcolor="#FFFFFF" align="center" valign="top" rowspan="2"><font size="2" face="Arial">
        <?php echo$byrakhir . "<br><i>" . $tglakhir . "</i><br>(diskon: $dknakhir)"  ?> 
        </font></td>
<td bgcolor="#FFFFFF" align="left" valign="top" rowspan="2"><font size="2" face="Arial">
        <?php echo$keterangan ?>
        </font></td>
  </tr>
	<tr height="25">
        <td bgcolor="#CCFF66"><font size="2" face="Arial"><strong>Jumlah Diskon</strong> </font></td>
<td bgcolor="#FFFFFF" align="right"><font size="2" face="Arial">
        <?php echo$diskon ?>
        </font></td>
  </tr>
    <tr height="25">
        <td bgcolor="#CCFF66"><font size="2" face="Arial"><strong>Sisa Bayaran</strong> </font></td>
<td bgcolor="#FFFFFF" align="right"><font size="2" face="Arial">
        <?php echo$sisa ?>
        </font></td>
  </tr>
    <tr height="3">
        <td colspan="4" bgcolor="#E8E8E8">&nbsp;</td>
  </tr>
<?php 
} //while iuran wajib

$sql = "SELECT DISTINCT p.idpenerimaan, d.nama FROM penerimaaniurancalon p, datapenerimaan d WHERE p.idpenerimaan = d.replid AND p.idcalon='$replid' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY nama";
$result = QueryDb($sql);
while ($row = mysql_fetch_array($result)) {
	$idpenerimaan = $row['idpenerimaan'];
	$namapenerimaan = $row['nama'];
	
	$sql = "SELECT SUM(jumlah) FROM penerimaaniurancalon WHERE idpenerimaan='$idpenerimaan' AND idcalon='$replid'";
	$pembayaran = FetchSingle($sql);

	$sql = "SELECT jumlah, DATE_FORMAT(tanggal, '%d-%b-%Y') AS ftanggal FROM penerimaaniurancalon WHERE idpenerimaan='$idpenerimaan' AND idcalon='$replid' ORDER BY tanggal DESC LIMIT 1";
	$result2 = QueryDb($sql);
	$byrakhir = 0;
	$tglakhir = "";
	if (mysql_num_rows($result2)) {
		$row2 = mysql_fetch_row($result2);
		$byrakhir = $row2[0];
		$tglakhir = $row2[1];
	};	
?>
 	<tr height="35">
        <td colspan="4" bgcolor="#99CC00"><font size="2" face="Arial"><strong><em><?php echo$namapenerimaan?></em></strong></font></td>
  </tr>  
   	<tr height="25">
        <td width="22%" bgcolor="#CCFF66" align="center"><font size="2" face="Arial"><strong>Total Pembayaran</strong> </font></td>
      <td width="22%" bgcolor="#CCFF66" align="center"><font size="2" face="Arial"><strong>Pembayaran Terakhir</strong></font></td>
      <td width="50%" colspan="2" bgcolor="#CCFF66" align="center"><font size="2" face="Arial"><strong>Keterangan</strong></font></td>
  </tr>
    <tr height="25">
        <td bgcolor="#FFFFFF" align="center"><font size="2" face="Arial">
        <?php echo$pembayaran ?>
        </font></td>
<td bgcolor="#FFFFFF" align="center"><font size="2" face="Arial">
        <?php echo$byrakhir . "<br><i>" . $tglakhir . "</i>" ?>
        </font></td>
      <td colspan="2" bgcolor="#FFFFFF" align="left">&nbsp;</td>
</tr>
    <tr height="3">
        <td colspan="4" bgcolor="#E8E8E8">&nbsp;</td>
  </tr>
<?php
} //while iuran sukarela
?>
</table>

<?php
CloseDb();
?>

</table>

</body>
</html>
<script language="javascript">window.print();</script>