<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
function SimpanJurnal($idtahunbuku, $tanggal, $transaksi, $nokas, $keterangan, $petugas, $sumber, &$idjurnal) 
{
	//Simpan ke jurnal
	$success = 0;
	
	$sql = "INSERT INTO jurnal SET idtahunbuku=$idtahunbuku,tanggal='$tanggal',transaksi='$transaksi',nokas='$nokas',keterangan='$keterangan',petugas='$petugas', sumber='$sumber'";
	//echo  "$sql<br>";
	$result = QueryDbTrans($sql, $success);
	
	$idjurnal = 0;
	if ($success) {
		$sql = "SELECT last_insert_id()";
		$result = QueryDbTrans($sql, $success);
		if ($success) {
			$row = @mysql_fetch_row($result);	
			$idjurnal = $row[0];
		}	
	}

	return $success;
}

function SimpanDetailJurnal($idjurnal, $align, $koderek, $jumlah) 
{
	$success = 0;
	
	if ($align == "D")
		$sql = "INSERT INTO jurnaldetail SET idjurnal=$idjurnal,koderek='$koderek',debet=$jumlah";
	else
		$sql = "INSERT INTO jurnaldetail SET idjurnal=$idjurnal,koderek='$koderek',kredit=$jumlah";
	//echo  "$sql<br>";		
	QueryDbTrans($sql, $success);
	
	return $success;
}
?>