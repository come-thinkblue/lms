<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');

if (isset($_REQUEST['idpenerimaan']))
	$idpenerimaan = (int)$_REQUEST['idpenerimaan'];
	
if (isset($_REQUEST['idangkatan']))
	$idangkatan = (int)$_REQUEST['idangkatan'];

if (isset($_REQUEST['idtingkat']))
	$idtingkat = (int)$_REQUEST['idtingkat'];

if (isset($_REQUEST['idkelas']))
	$idkelas = (int)$_REQUEST['idkelas'];
	
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
	
$varbaris=30;
if (isset($_REQUEST['varbaris']))
	$varbaris = $_REQUEST['varbaris'];

$page=0;
if (isset($_REQUEST['page']))
	$page = $_REQUEST['page'];
	
$hal=0;
if (isset($_REQUEST['hal']))
	$hal = $_REQUEST['hal'];

$urut = "nama";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	

$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Pembayaran Siswa Per Kelas</title>
<script language="javascript" src="script/tooltips.js" type="application/javascript"></script>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
<script language="javascript">
function refresh() 
{	
	document.location.href = "lapbayarsiswa_kelas_skr.php?departemen=<?php echo$departemen?>&idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>";	
}

function cetak() {
	var total = document.getElementById("tes").value;
	var addr = "lapbayarsiswa_kelas_skr_cetak.php?idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>&departemen=<?php echo$departemen?>&total="+total;
	newWindow(addr, 'CetakLapPembayaranIuran','1000','580','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function excel() {
	var total = document.getElementById("tes").value;
	var addr = "lapbayarsiswa_kelas_skr_excel.php?departemen=<?php echo$departemen?>&idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>&departemen=<?php echo$departemen?>&total="+total;
	newWindow(addr, 'CetakLapPembayaranIuranExcel','1000','580','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function change_urut(urut,urutan) {		
	var varbaris=document.getElementById("varbaris").value;
		
	if (urutan =="ASC"){
		urutan="DESC"
	} else {
		urutan="ASC"
	}
	
	document.location.href = "lapbayarsiswa_kelas_skr.php?departemen=<?php echo$departemen?>&idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>&urut="+urut+"&urutan="+urutan+"&page=<?php echo$page?>&hal=<?php echo$hal?>&varbaris="+varbaris;
}

function change_page(page) {
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="lapbayarsiswa_kelas_skr.php?departemen=<?php echo$departemen?>&idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>&page="+page+"&hal="+page+"&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris="+varbaris;
}

function change_hal() {
	var hal = document.getElementById("hal").value;
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="lapbayarsiswa_kelas_skr.php?departemen=<?php echo$departemen?>&idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris="+varbaris+"&page="+hal+"&hal="+hal;
}

function change_baris() {
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="lapbayarsiswa_kelas_skr.php?departemen=<?php echo$departemen?>&idkelas=<?php echo$idkelas ?>&idangkatan=<?php echo$idangkatan ?>&idpenerimaan=<?php echo$idpenerimaan ?>&idtingkat=<?php echo$idtingkat?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris="+varbaris;
}
</script>
</head>

<body topmargin="0" leftmargin="0">
<?php
OpenDb();

$sql = "SELECT replid FROM tahunbuku WHERE departemen='$departemen' AND aktif=1";
$res = QueryDb($sql);
$row = @mysql_fetch_row($res);
$idtahunbuku = $row[0];
if ($idtahunbuku==""){
	echo "<script>";
	echo "alert ('Belum ada Tahun buku yang Aktif di departemen ".$departemen.". Silakan isi/aktifkan Tahun Buku di menu Referensi!');";
	echo "</script>";
	exit;
}
//$idtahunbuku = FetchSingle($sql);

if ($idtingkat == -1) 
{
	$sql = "SELECT MAX(jml) FROM ((SELECT p.nis, COUNT(p.replid) as jml 
								     FROM penerimaaniuran p, jurnal j, dbakademik.siswa s
									WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku
									  AND p.nis = s.nis AND s.idangkatan = $idangkatan
									  AND p.idpenerimaan = $idpenerimaan GROUP BY p.nis) as X)";
} 
else 
{
	if ($idkelas == -1)
		$sql = "SELECT MAX(jml) FROM ((SELECT p.nis, COUNT(p.replid) as jml 
										 FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k 
										WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
										  AND p.nis = s.nis AND s.idangkatan = $idangkatan AND p.idpenerimaan = $idpenerimaan 
										  AND s.idkelas = k.replid AND k.idtingkat = $idtingkat GROUP BY p.nis) as X)";
	else
		$sql = "SELECT MAX(jml) FROM ((SELECT p.nis, COUNT(p.replid) as jml 
								         FROM penerimaaniuran p, jurnal j, dbakademik.siswa s 
										WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
										  AND p.nis = s.nis AND s.idkelas = $idkelas AND s.idangkatan = $idangkatan 
										  AND p.idpenerimaan = $idpenerimaan GROUP BY p.nis) as X)";
}
	
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$max_n_bayar = $row[0];
$table_width = 520 + $max_n_bayar * 100;

$sql = "SELECT nama FROM datapenerimaan WHERE replid = $idpenerimaan";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$namapenerimaan = $row[0];
?>

<table border="0" width="100%" align="center" background="" style="background-repeat:no-repeat; background-attachment:fixed">
<!-- TABLE CENTER -->
<tr>
	<td>
<?php if ($max_n_bayar > 0) { ?>
    <table width="100%" border="0" align="center">
    <tr>
    	<td>
    	<a href="#" onClick="refresh()"><img src="images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;
    	<a href="JavaScript:cetak()"><img src="images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;
        <a href="JavaScript:excel()"><img src="images/ico/excel.png" border="0" onMouseOver="showhint('Buka dengan Microsoft Excel!', this, event, '50px')"/>&nbsp;Excel</a>&nbsp;
    	</td>
	</tr>
	</table>
	<br />
	<table class="tab" id="table" border="1" cellpadding="5" style="border-collapse:collapse" cellspacing="0" width="<?php echo$table_width ?>" align="left" bordercolor="#000000">
    <tr height="30" align="center" class="header">
        <td width="30" >No</td>
        <td width="90" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('nis','<?php echo$urutan?>')">N I S <?php echo change_urut('nis',$urut,$urutan)?></td>
        <td width="160" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('nama','<?php echo$urutan?>')">Nama <?php echo change_urut('nama',$urut,$urutan)?></td>
        <td width="50">Kelas</td>
    <?php	for($i = 0; $i < $max_n_bayar; $i++) { ?>
        <td width="100">Bayaran-<?php echo$i + 1 ?></td>
    <?php  } ?>
        <td width="100">Total Pembayaran</td>
        <!--<td width="200">Keterangan</td>-->
    </tr>
<?php

if ($idtingkat == -1) 
{
	$sql_tot = "SELECT DISTINCT p.nis, s.nama, k.kelas, t.tingkat 
	              FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t 
				 WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
				   AND p.nis = s.nis AND s.idkelas = k.replid AND s.idangkatan = $idangkatan 
				   AND p.idpenerimaan = $idpenerimaan AND k.idtingkat = t.replid ORDER BY s.nama";
	
	$sql = "SELECT DISTINCT p.nis, s.nama, k.kelas, t.tingkat 
	          FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t 
			 WHERE p.nis = s.nis AND s.idkelas = k.replid AND s.idangkatan = $idangkatan 
			   AND p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
			   AND p.idpenerimaan = $idpenerimaan AND k.idtingkat = t.replid 
		  ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
} 
else 
{
	if ($idkelas == -1)
	{
		$sql_tot = "SELECT DISTINCT p.nis, s.nama, k.kelas, t.tingkat 
		              FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t 
					 WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
					   AND p.nis = s.nis AND s.idkelas = k.replid AND k.idtingkat = $idtingkat 
					   AND s.idangkatan = $idangkatan AND p.idpenerimaan = $idpenerimaan AND k.idtingkat = t.replid ORDER BY s.nama";
		
		$sql = "SELECT DISTINCT p.nis, s.nama, k.kelas, t.tingkat 
		          FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t 
				 WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
				   AND p.nis = s.nis AND s.idkelas = k.replid AND k.idtingkat = $idtingkat AND s.idangkatan = $idangkatan 
				   AND p.idpenerimaan = $idpenerimaan AND k.idtingkat = t.replid ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
	} 
	else 
	{
		$sql_tot = "SELECT DISTINCT p.nis, s.nama, k.kelas 
		              FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k 
					 WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
					   AND p.nis = s.nis AND s.idkelas = k.replid AND s.idkelas = $idkelas 
					   AND s.idangkatan = $idangkatan AND p.idpenerimaan = $idpenerimaan ORDER BY s.nama";
		
		$sql = "SELECT DISTINCT p.nis, s.nama, k.kelas 
		          FROM penerimaaniuran p, jurnal j, dbakademik.siswa s, dbakademik.kelas k 
				 WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
				   AND p.nis = s.nis AND s.idkelas = k.replid AND s.idkelas = $idkelas 
				   AND s.idangkatan = $idangkatan AND p.idpenerimaan = $idpenerimaan ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
	}
}

$result_tot = QueryDb($sql_tot);
$total=ceil(mysql_num_rows($result_tot)/(int)$varbaris);
$jumlah = mysql_num_rows($result_tot);
$akhir = ceil($jumlah/5)*5;

$result = QueryDb($sql);
if ($page==0)
	$cnt = 0;
else 
	$cnt = (int)$page*(int)$varbaris;
$totalall = 0;

$totalall2 = 0;
while ($row4 = mysql_fetch_array($result_tot)) 
{
	$sql3 = "SELECT date_format(p.tanggal, '%d-%b-%y') as tanggal, jumlah 
	           FROM penerimaaniuran p, jurnal j
			  WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
			    AND nis = '$row4[nis]' AND idpenerimaan = $idpenerimaan";
	$result3 = QueryDb($sql3);
	$totalbayar2 = 0;
	while ($row3 = mysql_fetch_array($result3)) 
	{
		$totalbayar2 += $row3['jumlah']; 		
	}  
	$totalall2 += $totalbayar2;
}

while ($row = mysql_fetch_array($result)) 
{ 
	$nis = $row['nis'];
?>
	
    <tr height="40">
    	<td align="center"><?php echo++$cnt ?></td>
        <td align="center"><?php echo$row['nis'] ?></td>
        <td align="left"><?php echo$row['nama'] ?></td>
        <td align="center"><?php if ($idkelas == -1) echo  $row['tingkat']." - "; ?><?php echo$row['kelas'] ?></td>
<?php		$sql = "SELECT date_format(p.tanggal, '%d-%b-%y') as tanggal, jumlah 
                  FROM penerimaaniuran p, jurnal j
				 WHERE p.idjurnal = j.replid AND j.idtahunbuku = $idtahunbuku 
				   AND nis = '$nis' AND idpenerimaan = $idpenerimaan";
		$result2 = QueryDb($sql);
		$nbayar = mysql_num_rows($result2);
		$nblank = $max_n_bayar - $nbayar;
		
		$totalbayar = 0;
		while ($row2 = mysql_fetch_array($result2)) {
			$totalbayar += $row2['jumlah']; ?>
            <td>
                <table border="1" width="100%" style="border-collapse:collapse" cellpadding="0" cellspacing="0" bordercolor="#000000">
                <tr height="20"><td align="center"><?php echo formatRupiah($row2['jumlah']) ?></td></tr>
                <tr height="20"><td align="center"><?php echo$row2['tanggal'] ?></td></tr>
                </table>
            </td>
<?php		} //end for 
		$totalall += $totalbayar;

		for ($i = 0; $i < $nblank; $i++) { ?>        
            <td>
                <table border="1" width="100%" style="border-collapse:collapse" cellpadding="0" cellspacing="0" bordercolor="#000000">
                <tr height="20"><td align="center">&nbsp;</td></tr>
                <tr height="20"><td align="center">&nbsp;</td></tr>
                </table>
            </td>
<?php		} //end for ?>        
		<td align="right"><?php echo formatRupiah($totalbayar) ?></td>
        <!--<td align="right"><?php echo$row['keterangan'] ?></td>-->
    </tr>
<?php } //end for ?>
	<input type="hidden" name="tes" id="tes" value="<?php echo$total?>"/>
	<?php if ($page==$total-1){ ?>
	<tr height="30">
    	<td bgcolor="#999900" align="center" colspan="<?php echo4 + $max_n_bayar ?>"><font color="#FFFFFF"><strong>T O T A L</strong></font></td>
        <td bgcolor="#999900" align="right"><font color="#FFFFFF"><strong><?php echo formatRupiah($totalall2) ?></strong></font></td>
        <!--<td bgcolor="#999900">&nbsp;</td>-->
    </tr>
	<?php } ?>
	</table>
	<?php
    CloseDb();
    ?>
    <script language='JavaScript'>
        Tables('table', 1, 0);
    </script>
    <?php	if ($page==0){ 
		$disback="style='display:none;'";
		$disnext="style=''";
		}
		if ($page<$total && $page>0){
		$disback="style=''";
		$disnext="style=''";
		}
		if ($page==$total-1 && $page>0){
		$disback="style=''";
		$disnext="style='display:none;'";
		}
		if ($page==$total-1 && $page==0){
		$disback="style='display:none;'";
		$disnext="style='display:none;'";
		}
	?>
    </td>
</tr> 
<tr>
    <td>
    <table border="0"width="100%" align="center"cellpadding="0" cellspacing="0">	
    <tr>
       	<td width="30%" align="left" colspan="2">Halaman
		<input <?php echo$disback?> type="button" class="but" name="back" value=" << " onClick="change_page('<?php echo(int)$page-1?>')" onMouseOver="showhint('Sebelumnya', this, event, '75px')">
        <select name="hal" id="hal" onChange="change_hal()">
        <?php	for ($m=0; $m<$total; $m++) {?>
             <option value="<?php echo$m ?>" <?php echo IntIsSelected($hal,$m) ?>><?php echo$m+1 ?></option>
        <?php } ?>
     	</select>
		<input <?php echo$disnext?> type="button" class="but" name="next" value=" >> " onClick="change_page('<?php echo(int)$page+1?>')" onMouseOver="showhint('Berikutnya', this, event, '75px')">
	  	dari <?php echo$total?> halaman
		
 		</td>
        <td width="30%" align="right">Jumlah baris per halaman
      	<select name="varbaris" id="varbaris" onChange="change_baris()">
        <?php 	for ($m=5; $m <= $akhir; $m=$m+5) { ?>
        	<option value="<?php echo$m ?>" <?php echo IntIsSelected($varbaris,$m) ?>><?php echo$m ?></option>
        <?php 	} ?>
       
      	</select></td>
    </tr>
    </table>
<?php } else { ?>
<table width="100%" border="0" align="center">          
<tr>
	<td align="center" valign="middle" height="250">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data.
        <br />Tambah data pembayaran pada departemen <?php echo$departemen?> <?php if ($namakelas) echo  ", kelas ".$namakelas ?> dan kategori <?php echo$namapenerimaan?> di menu Penerimaan Pembayaran pada bagian Penerimaan.
        
        </b></font>
	</td>
</tr>
</table>  

<?php } ?>
    </td>
</tr>
</table>    
</body>
</html>