<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once("../include/config.php");
require_once("../include/db_functions.php");
require_once("../include/common.php");
OpenDb();
$idbarang = $_REQUEST[idbarang];
$sql = "SELECT * FROM $g_db_keuangan.barang WHERE replid='$idbarang'";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detail Barang</title>
<link href="../style/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<fieldset style="border:#336699 1px solid; background-color:#eaf4ff; height:100%" >
<legend style="background-color:#336699; color:#FFFFFF; font-size:10px; font-weight:bold; padding:5px">&nbsp;<?php echo$row[kode]?>&nbsp;</legend>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="29%" align="right"><strong>Kode Barang</strong></td>
    <td width="2" align="right"><strong>:</strong></td>
    <td width="33%"><?php echo stripslashes($row[kode])?></td>
    <td width="38%" rowspan="4" align="center" valign="middle"><img src="gambar.php?table=$g_db_keuangan.barang&replid=<?php echo$row[replid]?>" align="left" style="padding:2px" /></td>
  </tr>
  <tr>
    <td align="right"><strong>Nama Barang</strong></td>
    <td width="2" align="right"><strong>:</strong></td>
    <td><?php echo stripslashes($row[nama])?></td>
  </tr>
  <tr>
    <td align="right"><strong>Jumlah</strong></td>
    <td width="2" align="right"><strong>:</strong></td>
    <td><?php echo$row[jumlah]?>&nbsp;<?php echo stripslashes($row[satuan])?></td>
  </tr>
  <tr>
    <td align="right"><strong>Tanggal Perolehan</strong></td>
    <td width="2" align="right"><strong>:</strong></td>
    <td><?php echo substr($row[tglperolehan],8,2)."-".substr($row[tglperolehan],5,2)."-".substr($row[tglperolehan],0,4)?></td>
  </tr>  
  <tr>
    <td align="right"><strong>Kondisi</strong></td>
    <td width="2" align="right"><strong>:</strong></td>
    <td colspan="2"><?php echo stripslashes($row[kondisi])?></td>
  </tr>
  <tr>
    <td align="right"><strong>Keterangan</strong></td>
    <td width="2" align="right"><strong>:</strong></td>
    <td colspan="2"><?php echo stripslashes($row[keterangan])?></td>
  </tr>
  <tr>
    <td colspan="4" align="center"><input type="button" value="Tutup" onclick="window.close()" class="but" /></td>
  </tr>
</table>

</fieldset>
</body>
<?php
CloseDb();
?>
</html>