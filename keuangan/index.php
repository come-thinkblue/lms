<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php

require_once('include/config.php');

session_name("keuangan");
session_start();

if (isset($_SESSION['namakeuangan'])) 
	include("main.php");
else 
	include("login.php");
?>