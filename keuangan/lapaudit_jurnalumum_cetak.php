<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];

$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];

$tgl1 = explode(' ',$tanggal1);
$tgl2 = explode(' ',$tanggal2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Laporan Audit Perubahan Data Jurnal Umum]</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="960" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>


<center><font size="4"><strong>LAPORAN AUDIT PERUBAHAN DATA JURNAL UMUM</strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td width="90"><strong>Departemen </strong></td>
    <td><strong>: <?php echo$departemen ?></strong></td>
</tr>
<tr>
	<td width="90"><strong>Tanggal </strong></td>
    <td><strong>: <?php echo LongDateFormat($tgl1[0]) . " s/d 	" . LongDateFormat($tgl2[0]) ?></strong></td>
</tr>
</table>
<br />
<table class="tab" id="table" border="1" width="100%" align="left" cellpadding="5" cellspacing="0" bordercolor="#000000">
<tr height="30" align="center">
	<td class="header" width="4%">No</td>
    <td class="header" width="10%">Status Data</td>
    <td class="header" width="10%">Tanggal</td>
    <td class="header" width="15%">Keterangan</td>
    <td class="header" width="*">Detail Jurnal</td>
    <td class="header" width="15%">Petugas</td>
</tr>
<?php
OpenDb();
$sql = "SELECT DISTINCT ai.petugas AS petugasubah, j.transaksi, date_format(ai.tanggal, '%d-%b-%Y %H:%i:%s') as tanggalubah, aj.replid AS id, aj.idaudit, aj.status, aj.nokas, date_format(aj.tanggal, '%d-%b-%Y') AS tanggal, aj.petugas, aj.keterangan, aj.petugas, ai.alasan FROM auditjurnal aj, auditinfo ai, jurnal j WHERE aj.idaudit = ai.replid AND ai.idsumber = j.replid AND ai.departemen = '$departemen' AND ai.sumber='jurnalumum' AND ai.tanggal BETWEEN '$tanggal1 00:00:00' AND '$tanggal2 23:59:59' ORDER BY aj.idaudit DESC, ai.tanggal DESC, aj.status ASC";
$result = QueryDb($sql);
$cnt = 0;
$no = 0;
while ($row = mysql_fetch_array($result)) {
	$status = $row['status'];
	$idaudit = $row['idaudit'];
	$statusdata = "Data Lama";
	$bgcolor = "#FFFFFF";
	if ($row['status'] == 1) {
		$statusdata = "Data Perubahan";
		$bgcolor = "#FFFFB7";
	}       
		
	if ($cnt % 2 == 0) { ?>
	<tr>
		<td rowspan="4" align="center" bgcolor="#CCCC66"><strong><?php echo++$no ?></strong></td>
        <td colspan="6" align="left" bgcolor="#CCCC66"><em><strong>Perubahan dilakukan oleh <?php echo$row['petugasubah'] . " tanggal " . $row['tanggalubah'] ?></strong></em></td>
	</tr>
    <tr>
    	<td colspan="6" bgcolor="#E5E5E5">
        	<table cellpadding="0" cellspacing="0" style="border-collapse:collapse" width="100%" >
            <tr>
                <td width="20%"><strong>No. Jurnal : </strong><?php echo$row['nokas'] ?>
                <td valign="top" width="7%"><strong>Alasan : </td>
                <td rowspan="2" valign="top"></strong><?php echo$row['alasan']?></td>
            </tr>
            <tr>
                <td><strong>Transaksi : </strong><?php echo$row['transaksi'] ?></td>
            </tr>
            </table>
      	</td>
    </tr>
<?php  } ?>
	<tr bgcolor="<?php echo$bgcolor?>">
		<td><?php echo$statusdata ?></td>
	    <td align="center"><?php echo$row['tanggal'] ?></td>
	    <td align="left"><?php echo$row['keterangan'] ?></td>
	    <td bgcolor="#E8FFE8">
            <table cellpadding="5" cellspacing="0" border="1" style="border-collapse:collapse" width="100%" bgcolor="#FFFFFF">
    <?php		$nokas = $row['nokas'];
            $sql = "SELECT ajd.koderek, ra.nama, ajd.debet, ajd.kredit FROM auditjurnaldetail ajd, jurnal j, rekakun ra WHERE ajd.idjurnal = j.replid AND ajd.koderek = ra.kode AND j.nokas = '$nokas' AND ajd.status = '$status' AND idaudit='$idaudit' ORDER BY ajd.replid";
			$result2 = QueryDb($sql);            
			while ($row2 = mysql_fetch_row($result2)) {  ?>
            <tr>
            	<td width="*"><?php echo$row2[0] . " " . $row2[1] ?></td>
                <td width="30%" align="right"><?php echo formatRupiah($row2[2]) ?></td>
                <td width="30%" align="right"><?php echo formatRupiah($row2[3]) ?></td>
            </tr>
	<?php		} ?>            
            </table>
        
        </td>
	    <td align="center"><?php echo$row['petugas']; ?></td>
	</tr>
<?php
	$cnt++;
}
CloseDb();
?>
</table>

</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>