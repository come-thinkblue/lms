<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/sessioninfo.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];
	
$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];
	
$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$kriteria = 0;
if (isset($_REQUEST['kriteria']))
	$kriteria = (int)$_REQUEST['kriteria'];

$keyword = "";
if (isset($_REQUEST['keyword']))
	$keyword = $_REQUEST['keyword'];
	
$varbaris=30;
if (isset($_REQUEST['varbaris']))
	$varbaris = $_REQUEST['varbaris'];

$page=0;
if (isset($_REQUEST['page']))
	$page = $_REQUEST['page'];
	
$hal=0;
if (isset($_REQUEST['hal']))
	$hal = $_REQUEST['hal'];

$urut = "p.tanggal";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	

$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];
	
$idtahunbuku = 0;
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = (int)$_REQUEST['idtahunbuku'];	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script language="javascript" src="script/tooltips.js"></script>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
<script language="javascript">

function cetakbukti(id) {
	newWindow('buktipengeluaran.php?idtransaksi='+id, 'BuktiPengeluaran','360','600','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function edit(id) {
	newWindow('pengeluaran_edit.php?idtransaksi='+id, 'EditPengeluaran','500','550','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function refresh() {	
	document.location.href = "lappengeluaran_cari_content.php?departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria=<?php echo$kriteria?>&keyword=<?php echo urlencode($keyword)?>";
	//document.location.reload();
}

function cetak() {
	var total = document.getElementById("total").value;
	
	var addr = "lappengeluaran_cari_cetak.php?idtahunbuku=<?php echo$idtahunbuku?>&departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria=<?php echo$kriteria?>&keyword=<?php echo urlencode($keyword)?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>&total="+total;
	newWindow(addr, 'CetakCariDetailLapPengeluaran','780','580','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function excel() {
	var total = document.getElementById("total").value;
	
	var addr = "lappengeluaran_cari_excel.php?idtahunbuku=<?php echo$idtahunbuku?>&departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&kriteria=<?php echo$kriteria?>&keyword=<?php echo urlencode($keyword)?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>&total="+total;
	newWindow(addr, 'ExcelCariDetailLapPengeluaran','780','580','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function change_urut(urut,urutan) {		
	var varbaris=document.getElementById("varbaris").value;
		
	if (urutan =="ASC"){
		urutan="DESC"
	} else {
		urutan="ASC"
	}
	
	document.location.href = "lappengeluaran_cari_content.php?departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&urut="+urut+"&urutan="+urutan+"&page=<?php echo$page?>&hal=<?php echo$hal?>&varbaris="+varbaris+"&kriteria=<?php echo$kriteria?>&keyword=<?php echo urlencode($keyword)?>";
}

function change_page(page) {
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="lappengeluaran_cari_content.php?departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&page="+page+"&hal="+page+"&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris="+varbaris;
}

function change_hal() {
	var hal = document.getElementById("hal").value;
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="lappengeluaran_cari_content.php?departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris="+varbaris+"&page="+hal+"&hal="+hal;
}

function change_baris() {
	var varbaris=document.getElementById("varbaris").value;
	
	document.location.href="lappengeluaran_cari_content.php?departemen=<?php echo$departemen?>&tanggal1=<?php echo$tanggal1?>&tanggal2=<?php echo$tanggal2?>&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>&varbaris="+varbaris;
}
</script>
</head>

<body topmargin="0" marginheight="0" >
<table border="0" width="100%" align="center" background="" style="background-repeat:no-repeat; background-attachment:fixed">
<!-- TABLE CENTER -->
<tr>
	<td>
    <?php 
    if ($kriteria == 1)
        $sqlwhere = " AND p.namapemohon LIKE '%$keyword%'";
    else if ($kriteria == 2)
        $sqlwhere = " AND p.penerima LIKE '%$keyword%'";
    else if ($kriteria == 3)
        $sqlwhere = " AND p.petugas LIKE '%$keyword%'";
    else if ($kriteria == 4)
        $sqlwhere = " AND p.keperluan LIKE '%$keyword%'";
    else if ($kriteria == 5)
        $sqlwhere = " AND p.keterangan LIKE '%$keyword%'";
		
   	OpenDb();
	$sql_tot = "SELECT p.replid AS id, d.nama AS namapengeluaran, p.keperluan, p.keterangan, p.jenispemohon, 
	                   p.nip, p.nis, p.pemohonlain, p.penerima, date_format(p.tanggal, '%d-%b-%Y') as tanggal, date_format(p.tanggalkeluar, '%d-%b-%Y') as tanggalkeluar, 
					   p.petugas, p.jumlah 
			      FROM pengeluaran p, jurnal j, datapengeluaran d 
				 WHERE p.idjurnal = j.replid AND j.idtahunbuku = '$idtahunbuku' 
				   AND p.idpengeluaran = d.replid AND d.departemen = '$departemen' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2' $sqlwhere ORDER BY p.tanggal";         
	
    $sql = "SELECT p.replid AS id, d.nama AS namapengeluaran, p.keperluan, p.keterangan, p.jenispemohon, 
	               p.nip, p.nis, p.pemohonlain, p.penerima, date_format(p.tanggal, '%d-%b-%Y') as tanggal, date_format(p.tanggalkeluar, '%d-%b-%Y') as tanggalkeluar, 
				   p.petugas, p.jumlah 
		     FROM pengeluaran p, jurnal j, datapengeluaran d 
			WHERE p.idjurnal = j.replid AND j.idtahunbuku = '$idtahunbuku' 
			  AND p.idpengeluaran = d.replid AND d.departemen = '$departemen' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2' 
			      $sqlwhere 
		 ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
   	
	$result_tot = QueryDb($sql_tot);
	$total=ceil(mysql_num_rows($result_tot)/(int)$varbaris);
	$jumlah = mysql_num_rows($result_tot);
	$akhir = ceil($jumlah/5)*5;
    
    $result = QueryDb($sql);
	
	$totalbiayaB = 0;
    while ($rowB = mysql_fetch_array($result_tot)) {
        $totalbiayaB += $rowB['jumlah'];
	}

	if (mysql_num_rows($result) > 0) {
	?>
    <input type="hidden" name="total" id="total" value="<?php echo$total?>"/>
    <table border="0" width="100%" align="center">
    <tr>
        <td align="right">
        <a href="#" onClick="refresh()"><img src="images/ico/refresh.png" border="0"  onMouseOver="showhint('Refresh!', this, event, '50px')" />&nbsp;Refresh</a>&nbsp;&nbsp;
        <a href="JavaScript:cetak()"><img src="images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;
        <a href="JavaScript:excel()"><img src="images/ico/excel.png" border="0" onMouseOver="showhint('Buka di Ms Excel!', this, event, '50px')"/>&nbsp;Excel</a>&nbsp;
        </td>
    </tr>
    </table>
    <br />
   <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr align="center" class="header" height="30">
        <td width="4%">No</td>
        <td width="10%" height="30" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('p.tanggal','<?php echo$urutan?>')">Tanggal <?php echo change_urut('p.tanggal',$urut,$urutan)?></td>
        <td width="15%" height="30" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('namapengeluaran','<?php echo$urutan?>')">Pengeluaran <?php echo change_urut('namapengeluaran',$urut,$urutan)?></td>
        <td width="15%">Pemohon</td>
        <td width="10%" height="30" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('penerima','<?php echo$urutan?>')">Penerima <?php echo change_urut('penerima',$urut,$urutan)?></td>
        <td width="10%" height="30" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('jumlah','<?php echo$urutan?>')">Jumlah <?php echo change_urut('jumlah',$urut,$urutan)?></td>
        <td width="*" >Keperluan</td>
        <td width="7%" height="30" onMouseOver="background='style/formbg2agreen.gif';height=30;" onMouseOut="background='style/formbg2.gif';height=30;" background="style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('petugas','<?php echo$urutan?>')">Petugas <?php echo change_urut('petugas',$urut,$urutan)?></td>
        <td width="7%">&nbsp;</td>
    </tr>
    <?php
    
  	if ($page==0)
		$cnt = 0;
	else 
		$cnt = (int)$page*(int)$varbaris;
    $totalbiaya = 0;
    while ($row = mysql_fetch_array($result)) {
        
        if ($row['jenispemohon'] == 1) {
            $idpemohon = $row['nip'];
            $sql = "SELECT nama FROM $g_db_pegawai.pegawai WHERE nip = '$idpemohon'";
            $jenisinfo = "pegawai";
        } else if ($row['jenispemohon'] == 2) {
            $idpemohon = $row['nis'];
            $sql = "SELECT nama FROM dbakademik.siswa WHERE nis = '$idpemohon'";
            $jenisinfo = "siswa";
        } else {
            $idpemohon = "";
            $sql = "SELECT nama FROM pemohonlain WHERE replid = '$row[pemohonlain]'";
            $jenisinfo = "pemohon lain";
        }
        $result2 = QueryDb($sql);
        $row2 = mysql_fetch_row($result2);
        $namapemohon = $row2[0];
        
        $totalbiaya += $row['jumlah'];
    ?>
    <tr height="25">
        <td align="center" valign="top"><?php echo++$cnt ?></td>
        <td align="center" valign="top"><?php echo$row['tanggal'] ?></td>
        <td valign="top"><?php echo$row['namapengeluaran'] ?></td>
        <td valign="top"><?php echo$idpemohon?> <?php echo$namapemohon ?><br />
        <em>(<?php echo$jenisinfo ?>)</em>
        </td>
        <td valign="top"><?php echo$row['penerima'] ?></td>
        <td align="right" valign="top"><?php echo formatRupiah($row['jumlah']) ?></td>
        <td valign="top">
        <strong>Keperluan: </strong><?php echo$row['keperluan'] ?><br />
        <strong>Keterangan: </strong><?php echo$row['keterangan'] ?>
        </td>
        <td valign="top" align="center"><?php echo$row['petugas'] ?></td>
        <td valign="top" align="center">
        <a href="JavaScript:cetakbukti(<?php echo$row['id'] ?>)"><img src="images/ico/print.png" border="0" onMouseOver="showhint('Cetak Bukti Pengeluaran Kas!', this, event, '150px')"/></a>&nbsp;
    <?php  if (getLevel() != 2) { ?>        
        <a href="JavaScript:edit(<?php echo$row['id'] ?>)"><img src="images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Pembayaran Pengeluaran!', this, event, '100px')"/></a>
   
    <?php  } ?>    
        </td>
    </tr>
    <?php
    }
    CloseDb();
    ?>
    <?php if ($page==$total-1){ ?>
	<tr height="30">
        <td colspan="5" align="center" bgcolor="#999900">
        <font color="#FFFFFF"><strong>T O T A L</strong></font>
        </td>
        <td align="right" bgcolor="#999900"><font color="#FFFFFF"><strong><?php echo formatRupiah($totalbiayaB) ?></strong></font></td>
        <td colspan="3" bgcolor="#999900">&nbsp;</td>
    </tr>
	<?php } ?>
    </table>
    <script language='JavaScript'>
        Tables('table', 1, 0);
    </script>
    <?php CloseDb() ?>
     <?php	if ($page==0){ 
		$disback="style='display:none;'";
		$disnext="style=''";
		}
		if ($page<$total && $page>0){
		$disback="style=''";
		$disnext="style=''";
		}
		if ($page==$total-1 && $page>0){
		$disback="style=''";
		$disnext="style='display:none;'";
		}
		if ($page==$total-1 && $page==0){
		$disback="style='display:none;'";
		$disnext="style='display:none;'";
		}
	?>
    </td>
</tr> 
<tr>
    <td>
    <table border="0"width="100%" align="center"cellpadding="0" cellspacing="0">	
    <tr>
       	<td width="30%" align="left" colspan="2">Halaman
		<input <?php echo$disback?> type="button" class="but" name="back" value=" << " onClick="change_page('<?php echo(int)$page-1?>')" onMouseOver="showhint('Sebelumnya', this, event, '75px')">
        <select name="hal" id="hal" onChange="change_hal()">
        <?php	for ($m=0; $m<$total; $m++) {?>
             <option value="<?php echo$m ?>" <?php echo IntIsSelected($hal,$m) ?>><?php echo$m+1 ?></option>
        <?php } ?>
     	</select>
		<input <?php echo$disnext?> type="button" class="but" name="next" value=" >> " onClick="change_page('<?php echo(int)$page+1?>')" onMouseOver="showhint('Berikutnya', this, event, '75px')">
	  	dari <?php echo$total?> halaman
		
 		</td>
        <td width="30%" align="right">Jumlah baris per halaman
      	<select name="varbaris" id="varbaris" onChange="change_baris()">
        <?php 	for ($m=5; $m <= $akhir; $m=$m+5) { ?>
        	<option value="<?php echo$m ?>" <?php echo IntIsSelected($varbaris,$m) ?>><?php echo$m ?></option>
        <?php 	} ?>
       
      	</select></td>
    </tr>
    </table>
<?php } else { ?>
    <table width="100%" border="0" align="center">          
    <tr>
        <td align="center" valign="middle" height="250">
            <font size = "2" color ="red"><b>Tidak ditemukan adanya data.<br />Silahkan ulangi pencarian kembali. </font>
            
        </td>
    </tr>
    </table>  
<?php } ?>
    </td>
</tr>
</table>
</body>
</html>