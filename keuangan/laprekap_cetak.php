<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

$dept = $_REQUEST['dept'];
$idkategori = $_REQUEST['idkategori'];
$tanggal1 = $_REQUEST['tanggal1'];
$tanggal2 = $_REQUEST['tanggal2'];

function NamaJenis($id)
{
	if ($id == "JTT")
		return "Iuran Wajib Siswa";
	elseif ($id == "SKR")
		return "Iuran Sukarela Siswa";
	elseif ($id == "CSWJB")
		return "Iuran Wajib Calon Siswa";
	elseif ($id == "CSSKR")
		return "Iuran Sukarela Calon Siswa";
	elseif ($id == "LNN")
		return "Penerimaan Lainnya";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Laporan Pembayaran Per Siswa]</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">
<?php
$d = ($dept!='ALL')?$dept:'yayasan';
$d2 = ($dept!='ALL')?$dept:'(Semua departemen)';
?>
<?php echo GetHeader($d)?>

<center><font size="4"><strong>REKAPITULASI PENERIMAAN</strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td><strong>Departemen </strong></td>
    <td><strong>: <?php echo$d2 ?></strong></td>
</tr>
<tr>
	<td><strong>Jenis </strong></td>
    <td><strong>: <?php echo NamaJenis($idkategori) ?></strong></td>
</tr>
<tr>
	<td><strong>Tanggal </strong></td>
    <td><strong>: <?php echo LongDateFormat($tanggal1) . " s/d " . LongDateFormat($tanggal2) ?></strong></td>
</tr>
</table>
<br />

<table cellpadding="5" border="1" style="border-width:1px; border-color:#999; border-collapse:collapse;" cellspacing="0" align="left">
<?php
OpenDb();

if ($dept == "ALL")
{
	$sql = "SELECT departemen FROM dbakademik.departemen ORDER BY urutan";
	$dres = QueryDb($sql);
	$k = 0;
	while ($drow = mysql_fetch_row($dres))
		$darray[$k++] = $drow[0];
}
else
{
	$darray = array( $dept );
}

$total = 0;
for($k = 0; $k < count($darray); $k++)
{ 
	$dept = $darray[$k];
	$cnt = 0;
	
	$sql = "SELECT COUNT(replid) FROM tahunbuku WHERE departemen='$dept' AND aktif=1";
	$ntb = FetchSingle($sql);
	
	if ($ntb == 0)
		continue;
	
	$sql = "SELECT replid FROM tahunbuku WHERE departemen='$dept' AND aktif=1";
	$idtahunbuku = FetchSingle($sql);
	
	$subtotal = 0;
	$rarray = array();
	$sql = "SELECT replid, nama FROM $g_db_keuangan.datapenerimaan WHERE departemen='$dept' AND aktif=1 AND idkategori='$idkategori'";
	$pres = QueryDb($sql);
	while($prow = mysql_fetch_row($pres))
	{
		$idp = $prow[0];
		$pen = $prow[1];
		
		if ($idkategori == "JTT")
		{
			$sql = "SELECT SUM(p.jumlah), SUM(p.info1) 
			          FROM $g_db_keuangan.penerimaanjtt p, $g_db_keuangan.besarjtt b, $g_db_keuangan.datapenerimaan dp 
			         WHERE p.idbesarjtt = b.replid AND b.info2 = '$idtahunbuku' AND b.idpenerimaan = dp.replid
					     AND dp.replid = '$idp' AND dp.departemen='$dept' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2'";
		}
		elseif ($idkategori == "SKR")
		{
			$sql = "SELECT SUM(p.jumlah), 0 
			          FROM $g_db_keuangan.penerimaaniuran p, $g_db_keuangan.datapenerimaan dp, $g_db_keuangan.jurnal j
			         WHERE p.idpenerimaan = dp.replid
					     AND p.idjurnal = j.replid AND j.idtahunbuku = '$idtahunbuku' 
						  AND dp.replid = '$idp' AND dp.departemen='$dept' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2'";
		}
		elseif ($idkategori == "CSWJB")
		{
			$sql = "SELECT SUM(p.jumlah), SUM(p.info1) 
			          FROM $g_db_keuangan.penerimaanjttcalon p, $g_db_keuangan.besarjttcalon b, $g_db_keuangan.datapenerimaan dp 
			         WHERE p.idbesarjttcalon = b.replid AND b.info2 = '$idtahunbuku' AND b.idpenerimaan = dp.replid
					     AND dp.replid = '$idp' AND dp.departemen='$dept' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2'";
		}
		elseif ($idkategori == "CSSKR")
		{
			$sql = "SELECT SUM(p.jumlah), 0 
			          FROM $g_db_keuangan.penerimaaniurancalon p, $g_db_keuangan.datapenerimaan dp, $g_db_keuangan.jurnal j
			         WHERE p.idjurnal = j.replid AND j.idtahunbuku = '$idtahunbuku' AND p.idpenerimaan = dp.replid AND dp.replid = '$idp' 
					     AND dp.departemen='$dept' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2'";
		}
		elseif ($idkategori == "LNN")
		{
			$sql = "SELECT SUM(p.jumlah), 0 
			          FROM $g_db_keuangan.penerimaanlain p, $g_db_keuangan.datapenerimaan dp , $g_db_keuangan.jurnal j
			         WHERE p.idjurnal = j.replid AND j.idtahunbuku = '$idtahunbuku' AND p.idpenerimaan = dp.replid AND dp.replid = '$idp' 
					     AND dp.departemen='$dept' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2'";
		}
		
		$jres = QueryDb($sql);
		$jrow = mysql_fetch_row($jres);
		$jumlah = 0;
		if (!is_null($jrow[0]))
			$jumlah = $jrow[0];
		
		$subtotal = $subtotal + $jumlah;
		$rarray[$cnt][0] = $pen;
		$rarray[$cnt][1] = $jumlah;
			
		$cnt++;	
	}
	
	$total = $total + $subtotal;
	
	for($i = 0; $i < $cnt; $i++)
	{
		$pen = $rarray[$i][0];
		$jumlah = $rarray[$i][1];
		
		if ($i == 0) 
		{ ?>
        <tr>
        	<td colspan="4" align="right" bgcolor="#660099">
            <font color="#FFFFFF"><strong><em><?php echo$dept?></em></strong></font>
            </td>
        </tr>
<?php      } ?>
        <tr>
        	<td width="25" align="center" valign="top" bgcolor="#CCCCCC"><?php echo$i + 1?></td>
            <td width="350" align="left" valign="top"><?php echo$pen?></td>
            <td width="120" align="right" valign="top"><?php echo formatRupiah($jumlah)?></td>
<?php		if ($i == 0)
		{ ?>
        	<td width="120" rowspan="<?php echo$cnt?>" valign="middle" align="right" bgcolor="#FFECFF"><strong><?php echo formatRupiah($subtotal)?></strong></td>
<?php		} ?>        
        </tr>
<?php	 } 
}
CloseDb();
?>
    <tr height="40">
        <td colspan="3" align="right" valign="middle" bgcolor="#333333">
        <font color="#FFFFFF"><strong>T O T A L</strong></font>
        </td>
        <td valign="middle" align="right" bgcolor="#333333">
        <font color="#FFFFFF"><strong><?php echo formatRupiah($total)?></strong></font>
        </td>
    </tr>
</table>

</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>