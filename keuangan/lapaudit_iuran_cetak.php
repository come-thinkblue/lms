<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$tanggal1 = "";
if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];

$tanggal2 = "";
if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];
	
$idtahunbuku = 0;
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = $_REQUEST['idtahunbuku'];	

$calon = "";
if (isset($_REQUEST['calon']))
	$calon = $_REQUEST['calon'];
	
$tgl1 = explode(' ',$tanggal1);
$tgl2 = explode(' ',$tanggal2);

if ($calon == "calon") 
	$judul = "Calon ";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Laporan Audit Perubahan Data Iuran Sukarela <?php echo$judul?>Siswa]</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>


<center><font size="4"><strong>LAPORAN AUDIT PERUBAHAN DATA IURAN SUKARELA <?php echo strtoupper($judul)?>SISWA</strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td width="90"><strong>Departemen </strong></td>
    <td><strong>: <?php echo$departemen ?></strong></td>
</tr>
<tr>
	<td width="90"><strong>Tanggal </strong></td>
    <td><strong>: <?php echo LongDateFormat($tgl1[0]) . " s/d " . LongDateFormat($tgl2[0]) ?></strong></td>
</tr>
</table>
<br />
<table class="tab" id="table" border="1" width="100%" align="left" cellpadding="5" cellspacing="0">
<tr height="30" align="center">
	<td class="header" width="4%">No</td>
    <td class="header" width="15%">Status Data</td>
    <td class="header" width="10%">Tanggal</td>
    <td class="header" width="15%">Jumlah</td>
    <td class="header" width="*">Keterangan</td>
    <td class="header" width="15%">Petugas</td>
</tr>
<?php
OpenDb();
$sql = "SELECT DISTINCT ai.petugas as petugasubah, j.transaksi, date_format(ai.tanggal, '%d-%b-%Y %H:%i:%s') as tanggalubah, 
				   ap.replid AS id, ap.idaudit, ap.statusdata, j.nokas, date_format(ap.tanggal, '%d-%b-%Y') AS tanggal, ap.petugas, 
					ap.keterangan, ap.jumlah, ap.petugas, ai.alasan 
			 FROM auditpenerimaaniuran$calon ap, auditinfo ai, jurnal j 
			WHERE j.replid = ap.idjurnal AND j.idtahunbuku = '$idtahunbuku' AND ap.idaudit = ai.replid AND ai.departemen = '$departemen' 
			  AND ai.sumber='penerimaaniuran$calon' AND ai.tanggal BETWEEN '$tanggal1 00:00:00' AND '$tanggal2 23:59:59' 
		ORDER BY ap.idaudit DESC, ai.tanggal DESC, ap.statusdata ASC";
$result = QueryDb($sql);
$cnt = 0;
$no = 0;
while ($row = mysql_fetch_array($result)) {
	$statusdata = "Data Lama";
	$bgcolor = "#FFFFFF";
	if ($row['statusdata'] == 1) {
		$statusdata = "Data Perubahan";
		$bgcolor = "#FFFFB7";
	}
		
	if ($cnt % 2 == 0) { ?>
	<tr>
		<td rowspan="4" align="center" bgcolor="#CCCC66"><strong><?php echo++$no ?></strong></td>
        <td colspan="6" align="left" bgcolor="#CCCC66"><em><strong>Perubahan dilakukan oleh <?php echo$row['petugasubah'] . " tanggal " . $row['tanggalubah'] ?></strong></em></td>
	</tr>
    <tr>
    	<td colspan="6" bgcolor="#E5E5E5"><strong>No. Jurnal :</strong> <?php echo$row['nokas'] ?>   
         &nbsp;&nbsp;<strong>Alasan : </strong><?php echo$row['alasan'];?>
        <br /><strong>Transaksi :</strong> <?php echo$row['transaksi'] ?></td>
    </tr>
<?php  } ?>
	<tr bgcolor="<?php echo$bgcolor?>">
		<td><?php echo$statusdata ?></td>
	    <td align="center"><?php echo$row['tanggal'] ?></td>
	    <td align="right"><?php echo formatRupiah($row['jumlah']) ?></td>
	    <td><?php echo$row['keterangan'] ?></td>
	    <td align="center"><?php echo$row['petugas']; ?></td>
	</tr>
<?php
	$cnt++;
}
CloseDb();
?>
</table>

</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>