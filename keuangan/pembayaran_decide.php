<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
$idkategori = $_REQUEST['idkategori'];
$idpenerimaan = (int)$_REQUEST['idpenerimaan'];
$idtahunbuku = (int)$_REQUEST['idtahunbuku'];
$status = $_REQUEST['status'];

switch ($idkategori) {
	case 'JTT' 	: 
		$nis = (string)$_REQUEST['id'];
		header("Location: pembayaran_jtt.php?idkategori=$idkategori&idpenerimaan=$idpenerimaan&nis=$nis&idtahunbuku=$idtahunbuku");	
		break;
	case 'CSWJB':
		$id = (int)$_REQUEST['id'];
		header("Location: pembayaran_jttcalon.php?idkategori=$idkategori&idpenerimaan=$idpenerimaan&replid=$id&idtahunbuku=$idtahunbuku");
		break;			
	case 'SKR'	:
		$nis = (string)$_REQUEST['id'];
	header("Location: pembayaran_iuran.php?idkategori=$idkategori&idpenerimaan=$idpenerimaan&nis=$nis&idtahunbuku=$idtahunbuku");
		break;
	case 'CSSKR' :		
		$id = (int)$_REQUEST['id'];
		header("Location: pembayaran_iurancalon.php?idkategori=$idkategori&idpenerimaan=$idpenerimaan&replid=$id&idtahunbuku=$idtahunbuku");		
		break;	
		
}
?>