<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php');

$idkategori = $_REQUEST['idkategori'];
$idpenerimaan = (int)$_REQUEST['idpenerimaan'];
$replid = (int)$_REQUEST['replid'];
$idtahunbuku = (int)$_REQUEST['idtahunbuku'];

OpenDb();	

$sql = "SELECT departemen FROM tahunbuku WHERE replid='$idtahunbuku'"; 	
$result = QueryDb($sql);    
$row = mysql_fetch_row($result);	
$departemen = $row[0];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS SIMKEU [Penerimaan Pembayaran Sukarela Calon Siswa]</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>

<center><font size="4"><strong>PENERIMAAN PEMBAYARAN SUKARELA</strong></font><br /> </center><br /><br />
<table border="0">
<tr>
	<td width="120"><strong>Departemen</strong></td>
    <td><strong>: 
<?php	$sql = "SELECT departemen FROM tahunbuku WHERE replid='$idtahunbuku'"; 	
	$result = QueryDb($sql);    
	$row = mysql_fetch_row($result);	
	echo  $row[0]; ?>
    </strong></td>
</tr>
<tr>
	<td><strong>Tahun Buku</strong></td>
    <td><strong>:
<?php	$sql = "SELECT tahunbuku FROM tahunbuku WHERE replid='$idtahunbuku'"; 	
	$result = QueryDb($sql);    
	$row = mysql_fetch_row($result);
	echo  $row[0]; ?>
    </strong></td>
</tr>

<tr>
	<td><strong>Kategori</strong></td>
    <td><strong>:
<?php	$sql = "SELECT kategori FROM kategoripenerimaan WHERE kode='$idkategori' ORDER BY urutan";	
	$result = QueryDb($sql);    
	$row = mysql_fetch_row($result);
	echo  $row[0]; ?>
    </strong></td>
</tr>
<tr>
	<td><strong>Jenis Penerimaan</strong></td>
    <td><strong>:
<?php	$sql = "SELECT nama FROM datapenerimaan WHERE replid = '$idpenerimaan'"; 			
	$result = QueryDb($sql);    
	$row = mysql_fetch_row($result);
	echo  $row[0]; ?>
    </strong></td>
</tr>
</table>
<br />
<?php
$sql = "SELECT c.nopendaftaran, c.nama, c.telponsiswa as telpon, c.hpsiswa as hp, k.kelompok, c.alamatsiswa as alamattinggal, p.proses
         FROM dbakademik.calonsiswa c, dbakademik.kelompokcalonsiswa k, dbakademik.prosespenerimaansiswa p 
		   WHERE c.idkelompok = k.replid AND c.idproses = p.replid AND c.replid = '$replid'";


$result = QueryDb($sql);
if (mysql_num_rows($result) == 0) {
	CloseDb();
	exit();
} else {
	$row = mysql_fetch_array($result);
	$no = $row['nopendaftaran'];
	$nama = $row['nama'];
	$telpon = $row['telpon'];
	$hp = $row['hp'];
	$namakelompok = $row['kelompok'];
	$namaproses = $row['proses'];
	$alamattinggal = $row['alamattinggal'];

}

$sql = "SELECT nama FROM datapenerimaan WHERE replid = '$idpenerimaan'";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$namapenerimaan = $row[0];
?>

     	
<fieldset>
<legend></legend>
<table border="0" width="100%" cellpadding="2" cellspacing="2">
<tr height="25">
    <td colspan="4" class="header" align="center">Data Calon Siswa</td>
</tr>
<tr valign="top">                    
    <td width="15%"><strong>No Pendaftaran</strong></td>
    <td><strong>:</strong></td>
    <td><strong><?php echo$no ?></strong> </td><td rowspan="5" width="25%">
    <img src='<?php echo"library/gambar.php?replid=".$replid."&table=dbakademik.calonsiswa";?>' width='100' height='100'></td>
</tr>
<tr>
    <td valign="top"><strong>Nama</strong></td>
    <td valign="top"><strong>:</strong></td> 
    <td><strong><?php echo$nama ?></strong></td>
</tr>
<tr>
    <td valign="top"><strong>Proses</strong></td>
    <td valign="top"><strong>:</strong></td>
    <td><strong><?php echo$namaproses ?></strong></td>
</tr>
<tr>
    <td valign="top"><strong>Kelompok</strong></td>
    <td valign="top"><strong>:</strong></td>
    <td><strong><?php echo$namakelompok ?></strong></td>
</tr>
<tr>
    <td><strong>HP</strong></td>
     <td><strong>:</strong></td>
    <td><strong><?php echo$hp ?></strong></td>
</tr>
<tr>
    <td><strong>Telepon</strong></td>
     <td><strong>:</strong></td>
    <td><strong><?php echo$telpon ?></strong></td>
</tr>

<tr>
    <td valign="top"><strong>Alamat</strong></td>
    <td valign="top"><strong>:</strong></td>
    <td colspan="2" rowspan="2" valign="top"><strong>
      <?php echo$alamattinggal ?>
    </strong></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>           

</table>            
</fieldset>          
</td>
</tr>
<?php  
$sql = "SELECT p.replid AS id, j.nokas, date_format(p.tanggal, '%d-%b-%Y') as tanggal, p.keterangan, p.jumlah, p.petugas 
          FROM penerimaaniurancalon p, jurnal j 
			WHERE j.replid = p.idjurnal AND j.idtahunbuku = '$idtahunbuku' AND p.idpenerimaan = '$idpenerimaan' 
			  AND p.idcalon = '$replid' 
	   ORDER BY p.tanggal, p.replid";

$result = QueryDb($sql);    
?>
<tr>
<td align="center" colspan="2">
  
<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="center">
<tr height="30" align="center">
    <td class="header" width="5%">No</td>
    <td class="header" width="20%">No. Jurnal/Tgl</td>
    <td class="header" width="18%">Jumlah</td>
    <td class="header" width="*">Keterangan</td>
    <td class="header" width="15%">Petugas</td>
</tr>
<?php 

$cnt = 0;
$total = 0;
while ($row = mysql_fetch_array($result)) {
    $total += $row['jumlah'];
?>
<tr height="25">
    <td align="center"><?php echo++$cnt?></td>
    <td align="center"><?php echo"<strong>" . $row['nokas'] . "</strong><br><i>" . $row['tanggal']?></i></td>
    <td align="right"><?php echo formatRupiah($row['jumlah'])?></td>
    <td align="left"><?php echo$row['keterangan'] ?></td>
    <td align="center"><?php echo$row['petugas'] ?></td>
</tr>
<?php
}
?>
<tr height="35">
    <td bgcolor="#996600" colspan="2" align="center"><font color="#FFFFFF"><strong>T O T A L</strong></font></td>
    <td bgcolor="#996600" align="right"><font color="#FFFFFF"><strong><?php echo formatRupiah($total) ?></strong></font></td>
    <td bgcolor="#996600" colspan="3">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
            
		
</body>
</html>
<script language="javascript">window.print();</script>