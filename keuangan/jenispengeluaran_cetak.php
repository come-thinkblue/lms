<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS KEU [Daftar Jenis Pengeluaran]</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>


<center><font size="4"><strong>DAFTAR JENIS PENGELUARAN</strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td><strong>Departemen : <?php echo$departemen ?></strong> </td>
</tr>
</table>
<br />

	<table id="table" class="tab" border="1" style="border-collapse:collapse" width="100%" bordercolor="#000000">
	<tr height="30" align="center">
        <td class="header" width="5%">No</td>
        <td class="header" width="20%">Nama</td>
        <td class="header" width="30%">Kode Rekening</td>
        <td class="header" width="*">Keterangan</td>
	</tr>
<?php	OpenDb();
	$sql = "SELECT * FROM datapengeluaran WHERE departemen='$departemen' ORDER BY replid";
	$request = QueryDb($sql);
	$cnt = 0;
	while ($row = mysql_fetch_array($request)) { ?>
    <tr height="25">
    	<td align="center"><?php echo++$cnt?></td>
        <td><?php echo$row['nama'] ?></td>
        <td>
<?php			$sql = "SELECT nama FROM rekakun WHERE kode = '$row[rekkredit]'";
			$result = QueryDb($sql);
			$row2 = mysql_fetch_row($result);
			$namarekkredit = $row2[0];
	
			$sql = "SELECT nama FROM rekakun WHERE kode = '$row[rekdebet]'";
			$result = QueryDb($sql);
			$row2 = mysql_fetch_row($result);
			$namarekdebet = $row2[0]; ?>
		<strong>Rek. Kas:</strong> <?php echo$row['rekkredit'] . " " . $namarekkredit ?><br />
        <strong>Rek. Beban:</strong> <?php echo$row['rekdebet'] . " " . $namarekdebet ?>
        </td>
        <td><?php echo$row['keterangan'] ?></td>
    </tr>
    <?php
	}
	CloseDb();
	?>
    </table>
    
</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>