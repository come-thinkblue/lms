<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php

include('../charts/FusionCharts.php');	

require_once('inc/errorhandler.php');
require_once('inc/db_functions.php');
require_once('inc/sessioninfo.php');
//require_once('inc/common.php');
require_once('inc/config.php');
//require_once('cek.php');

OpenDb();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled-1</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="script/tooltips.js"></script>
<script type="text/javascript">
function get_fresh(){
	document.location.reload();
}

</script>
</head>
<?php
include ('menu2.php');
?>
<br/> <br/> <br/> <br/> <br/>
<body >
<!-- ImageReady Slices (Untitled-1) -->
<div id="content"> 
<div class="wrapper">



<?php 
$que1="SELECT Count(pegawai.nip) as pegawaiakademik  FROM pegawai where bagian='Akademik'";
$result1=mysql_query($que1);
$row1 = mysql_fetch_array($result1);

$que11="SELECT Count(pegawai.nip) as pegawaiakNonakademik  FROM pegawai where bagian='Non Akademik'";
$result11=mysql_query($que11);
$row11 = mysql_fetch_array($result11);

$que2="SELECT bagian, Count(pegawai.nip) as jumlah FROM $g_db_pegawai.pegawai GROUP BY pegawai.bagian";
$result2=mysql_query($que2);
$jumrows1=mysql_num_rows($result2);


$data ="<chart showvalues='1' caption='' numberprefix='$' canvasBorderColor='1D8BD1' canvasBorderAlpha='60'
 showlegend='1' enablesmartlabels='1' showlabels='0' showpercentvalues='1' logoURL='kecil.png'>";

$data .= "<set label='Jumlah Siswa' value='" . $row1['siswa'] . "' />";

if ($jumrows1>0) {
				$no1=0;
while ($row2 = mysql_fetch_array($result2)) {
					$data .= "<set label='" . $row2['bagian']."' value='" . $row2['jumlah'] . "' />";
					$no1++;
				}
				$data.="</chart>";
              }
?>
        <div class="fluid">
            <div class="widget gridWelcome">
              
                <div class="body">
                <h1 style="color:#128f97; font-weight:800;">Selamat Datang</h1>
                <h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;">Kepala MAN Kota Blitar</h1>
                <p>Sistem ini adalah sistem pelaporan terpadu. Anda dapat melihat laporan data siswa, keuangan, perpustakaan dan kepegawaiaan. 
                Di sistem ini diperlihatkan data-data secara statistik dalam model tabel dan grafik .</p></div>
                
        <ul class="middleNavA">
            <li><a href="pegawai/get_stat_detail.php" title="Pegawai"><img src="css/icon bawah/pegawai.png" alt="" /><span>Pegawai</span></a></li>
            <li><a href="referensi/tahunajaran.php" title="Setting Tahun Ajaran"><img src="css/icon bawah/tahunAjaran.png" alt="" /><span>Tahun Ajaran</span></a></li>
            <li><a href="referensi/tingkat.php" title="Setting Tingkat"><img src="css/icon bawah/tingkat.png" alt="" /><span>Tingkat</span></a></li>
            <li><a href="referensi/angkatan.php" title="Setting Angkatan"><img src="css/icon bawah/angkatan.png" alt="" /><span>Angkatan</span></a></li>
            <li><a href="referensi/kelas.php" title="Setting Kelas"><img src="css/icon bawah/kelas.png" alt="" /><span>Kelas</span></a></li>
            <li><a href="referensi/semester.php" title="Setting Semester"><img src="css/icon bawah/kalender.png" alt="" /><span>Semester</span></a></li>
        </ul>
            </div>
            <div class="widget grid5">
                <div class="whead"><h6>Grafik Data Perbandingan Jumlah Civitas Akademika</h6><div class="clear"></div></div>
                <div class="body">
                <?php echo renderChartHTML("../charts/Pie3D.swf", "", $data, "tes", "100%", "300", false);?>
                </div>
            </div>
            
    <?php
function jumlahData($count, $from){
$quePeg="SELECT Count(".$count.") as jumlah FROM ".$from."";
$resultPeg=mysql_query($quePeg);
$jumPeg=mysql_fetch_array($resultPeg);
$jumlah= $jumPeg['jumlah'];
return $jumlah;
}
	?> 
    
    
 <?php
function jmlKelas($x)
{
$kelas = $x;
$queryJml= "SELECT sum(jumlah) as jml from (SELECT
Count(siswa.nis)as jumlah, kelas.kelas FROM
siswa
Inner Join kelas ON siswa.idkelas = kelas.replid
WHERE
kelas.kelas REGEXP '$kelas'
GROUP BY
siswa.idkelas) as total";

$resultJml=mysql_query($queryJml);
$jumSis=mysql_fetch_array($resultJml);
$jumlah= $jumSis['jml'];
return $jumlah;

}
?>
          
            
            <div class="widget grid3">
                <div class="whead"><h6>Tabel Data</h6><div class="clear"></div></div>
                <div class="body">
                <table cellpadding="0" cellspacing="0" width="100%" class="tAlt wGeneral" >
                <thead>
                            <tr>
                   
    							<td width="60%">Unit</td>
   								<td >Jenis</td>
    							<td >Jumlah</td>
    							<td >Total</td>
 							 </tr>
                </thead>
                <tbody>
    
  		<tr>
    			<td rowspan="2"><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/pegawai.png" width="40" height="40"alt="" /><span>Pegawai</span></a></td>
    <td><a href="images/big.png" title="" class="lightbox"><span>Akademik</span></a></td>
    <?php $jumlahPegawaiakademik = jumlahData("pegawai.nip","$g_db_pegawai.pegawai where bagian='Akademik' "); ?>
    <td align="center"><strong class="income"><?php echo $jumlahPegawaiakademik;?></strong></td>
     <?php $jumlahPegawai = jumlahData("pegawai.nip","$g_db_pegawai.pegawai"); ?>
    <td rowspan="2" align="center"><strong class="income"><?php echo $jumlahPegawai;?></strong></td>
  </tr>
  <tr>
    <td><a href="images/big.png" title="" class="lightbox"><span>Non Akademik</span></a></td>
    <?php $jumlahPegawaiNonakademik = jumlahData("pegawai.nip","$g_db_pegawai.pegawai where bagian='Non Akademik'"); ?>
    <td align="center"><strong class="income"><?php echo $jumlahPegawaiNonakademik;?></strong></td>
    
  </tr>
  <tr>
    <td rowspan="3"><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/siswa.png" width="40" height="40"alt="" /><span>Jumlah Siswa</span></a></td>
    <td><a href="images/big.png" title="" class="lightbox"><span>Kelas 10</span></a></td>
    <td><strong class="income"><?php echo$kelasX=jmlKelas('X-') ?></strong></td>
    <td rowspan="3"><strong class="income"><?php echo$kelasX=jmlKelas('X') ?></strong></td>
  </tr>
  <tr>
    <td><a href="images/big.png" title="" class="lightbox"><span>Kelas 11</span></a></td>
    <td><strong class="income"><?php echo$kelasXI=jmlKelas('XI-') ?></strong></td>
  </tr>
  <tr>
    <td><a href="images/big.png" title="" class="lightbox"><span>Kelas 12</span></a></td>
    <td><strong class="income"><?php echo$kelasXII=jmlKelas('XII-') ?></strong></td>
  </tr>
  </tbody>
</table>
                        
                
                </div>
            </div>
        </div>


</div>


</div>
<!-- End ImageReady Slices -->
</body>
</html>