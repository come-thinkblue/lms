<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/sessionchecker.php');
require_once('../inc/config.php');
require_once('../inc/getheader.php');
require_once('../inc/common.php');
require_once('../inc/db_functions.php');
require_once('siswa.class.php');

OpenDb();
$SP = new CSiswa();
?>
<link href="../style/style.css" rel="stylesheet" type="text/css" />
<form name="frmPilih">
<table width="100%" border="0" cellspacing="2" cellpadding="1">
  <tr>
    <td width="8%" class="tab2">Departemen</td>
    <td width="92%">
    <div id="depInfo">
    <?php
	$SP->GetDep();
	?>
    </div>
    </td>
  </tr>
  <tr>
    <td class="tab2">Tingkat</td>
    <td>
    <div id="tktInfo">
    <?php
	$SP->GetTkt();
	?>
    </div>
    </td>
  </tr>
  <tr>
    <td class="tab2">Kelas</td>
    <td>
    <div id="klsInfo">
    <?php
	$SP->GetKls();
	?>
    </div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div id="sisInfo">
    <?php
	$SP->GetSis();
	?>
    </div>
    </td>
  </tr>
</table>
</form>