<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/getheader.php');
require_once('../inc/common.php');
require_once('../inc/sessionchecker.php');
require_once('../inc/db_functions.php');
require_once('siswa.class.php');


require_once('../inc/sessioninfo.php');


OpenDb();
$S = new CSiswa();
$S->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="../script/ajax.js" type="text/javascript"></script>
<script src="siswaui.js" type="text/javascript"></script>
<script src="infosiswa.js" type="text/javascript"></script>
<script src="../script/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../script/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">
<link href="../style/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/cal2.js"></script>
<script language="javascript" src="../script/cal_conf3.js"></script>
</head> 

<body>
<div id="waitBox" style="position:absolute; visibility:hidden;">
	<img src="../img/loading2.gif" border="0" />&nbsp;<span class="tab2">Please&nbsp;wait...</span>
</div>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="250" align="left" valign="top"><div id="list" style=" width:350px">
<?php		$S->ShowStudentList();	?>
	</td>
    <td width="*" align="left" valign="top">
		<div id="content"></div>
	</td>
  </tr>
</table>
</body>
</html>