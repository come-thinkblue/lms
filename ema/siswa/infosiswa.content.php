<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/sessionchecker.php');
require_once('../inc/config.php');
require_once('../inc/common.php');
require_once('../inc/db_functions.php');
require_once('infosiswa.class.php');

OpenDb();
$S = new CInfoSiswa();
?>
<table border='0' width='100%' cellpadding='2'>
<tr>
    <td align='left' valign='top'>
<?php      $S->ShowIdentity(); ?>            
<?php      $S->ShowReportComboBox(); ?>
        <br><br>
        <table border="0" cellpadding="2" width="680"><tr><td align="left" valign="top">
        <div id="infosiswa.content">
<?php      $S->ShowReportContent(); ?>
        </div>
        </td></tr></table>
    </td>
</tr>
</table>
<?php
CloseDb();
?>