<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/errorhandler.php');
require_once('../inc/sessionchecker.php');
require_once('../inc/common.php');
require_once('../inc/config.php');
require_once('../inc/getheader.php');
require_once('../inc/db_functions.php');
require_once('../inc/rupiah.php');
require_once('infosiswa.keuangan.class.php');

OpenDb();

$K = new CK();
?>
<form name="panelkeu">
<input type="hidden" name="nis_awal" id="nis_awal" value="<?php echo $_SESSION['infosiswa.nis'] ?> " />
<table border="0" cellpadding="2">
<tr>
    <td align='left'>
<?php  $K->ShowDepartemenComboBox(); ?>
    &nbsp;&nbsp;
<?php  $K->ShowTahunBukuComboBox(); ?>
    </td>
    <td align='left'></td>
</tr>
<tr>
    <td align='left' valign='top'>
<?php  $K->ShowFinanceReport(); ?>        
    </td>
</tr>
</table>
</form>
<?php
CloseDb();
?>