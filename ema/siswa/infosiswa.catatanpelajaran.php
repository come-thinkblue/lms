<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/sessionchecker.php');
require_once('../inc/config.php');
require_once('../inc/getheader.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');

$nis = $_SESSION["infosiswa.nis"];

$bulan_pjg = array(1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

OpenDb();
$sql_pel = "SELECT pel.nama as namapelajaran, ppsiswa.statushadir as statushadir, pp.tanggal as tanggal,
                   pp.jam as jam, pp.gurupelajaran as guru,ppsiswa.catatan as catatan,pel.replid as pelajaran
              FROM $g_db_akademik.pelajaran pel, $g_db_akademik.presensipelajaran pp, $g_db_akademik.ppsiswa ppsiswa
             WHERE ppsiswa.nis='$nis' AND ppsiswa.idpp=pp.replid AND pel.replid=pp.idpelajaran AND ppsiswa.catatan<>''
             GROUP BY pel.replid";
$res_pel = QueryDb($sql_pel);
?>
<table width="100%" border="0" cellspacing="5">
<tr>
<td width="150" valign="top">
    <div id="thn_catatan">Pelajaran
    <select name="pel" id="pel" onChange="ChangePelajaranCatatanPelajaran('<?php echo$nis?>')">
<?php  if (@mysql_num_rows($res_pel) > 0)
    {
        while ($row_pel=@mysql_fetch_array($res_pel))
        {
            if ($pelajaran=="")
            	$pelajaran=$row_pel[pelajaran];	?>
            <option value="<?php echo$row_pel[pelajaran]?>"><?php echo$row_pel[namapelajaran]?></option>
<?php	    }
	}
    else
    {	?>
        <option value="">Tidak ada data</option>
<?php	}	?>
    </select>
    </div>
</td>
<td>
	<div id="content_pp">
<?php	if ($pelajaran != "")
    {
		$sql_pp = "SELECT pel.nama as namapelajaran, ppsiswa.statushadir as statushadir, pp.tanggal as tanggal,
                          pp.jam as jam, pp.gurupelajaran as guru,ppsiswa.catatan as catatan
                     FROM $g_db_akademik.pelajaran pel, $g_db_akademik.presensipelajaran pp, $g_db_akademik.ppsiswa ppsiswa
                    WHERE ppsiswa.nis='$nis' AND ppsiswa.idpp=pp.replid AND pel.replid=pp.idpelajaran
                      AND ppsiswa.catatan<>'' AND pp.idpelajaran=$pelajaran";
		$res_pp=QueryDb($sql_pp);
		?>       
        <table width="100%" border="1" cellspacing="0" class="tab">
        <tr class="header" height="30">
            <td width="4%" align="center">No.</td>
            <td width="5%" align="center">Status</td>
            <td width="25%" align="center">Tanggal-Jam</td>
            <td width="38%" align="center">Guru</td>
        </tr>
<?php      if (@mysql_num_rows($res_pp)>0)
        {
            $cnt = 1;
            while ($row_pp=@mysql_fetch_array($res_pp))
            {
                $a="";
            	if ($cnt%2==0)
            		$a="style='background-color:#FFFFCC'"; ?>
                <tr height="25" <?php echo$a?> >
                    <td align="center" rowspan="2"><?php echo$cnt?></td>
                    <td align="center">
<?php              	switch ($row_pp[statushadir])
                    {
                        case 0:
                            echo "Hadir";
                            break;
                        case 1:
                            echo "Sakit";
                            break;
                        case 2:
                            echo "Ijin";
                            break;
                        case 3:
                            echo "Alpa";
                            break;
                        case 4:
                            echo "Cuti";
                            break;
                    } ?>
                    </td>
                    <td><?php echo ShortDateFormat($row_pp[tanggal])."-".$row_pp[jam]?></td>
                    <td>
                        [<?php echo$row_pp[guru]?>]&nbsp;
<?php        	            $res_gr=QueryDb("SELECT nama FROM $g_db_pegawai.pegawai WHERE nip='$row_pp[guru]'");
                        $row_gr=@mysql_fetch_array($res_gr);
                        echo $row_gr[nama];?>
                    </td>
                </tr>
                <tr <?php echo$a?>>
                    <td colspan="3"><?php echo$row_pp[catatan]?></td>
                </tr>
<?php          $cnt++;
            }
        }
        else
        { ?>
        <tr>
            <td align="center" colspan="5">Tidak ada Catatan</td>
        </tr>
<?php      } ?>
        </table>
<?php  } ?>

	</div>
</td>
</tr>
</table>
<?php
CloseDb();
?>