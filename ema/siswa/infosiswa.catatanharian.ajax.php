<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/sessionchecker.php');
require_once('../inc/config.php');
require_once('../inc/getheader.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');

$nis = "";
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
    
OpenDb();

$res_nm_sis=QueryDb("SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'");
$row_nm_sis=@mysql_fetch_array($res_nm_sis);
$tglawal = "";
if (isset($_REQUEST['tglawal'])){
	$tglawl = explode('-',$_REQUEST['tglawal']);
	$tglawal = $tglawl[2]."-".$tglawl[1]."-".$tglawl[0];
	
}
$tglakhir = "";
if (isset($_REQUEST['tglakhir'])){
	$tglakhr =  explode('-',$_REQUEST['tglakhir']);
	$tglakhir = $tglakhr[2]."-".$tglakhr[1]."-".$tglakhr[0];
	
}
$sql="SELECT ph.tanggal1,ph.tanggal2,phsiswa.keterangan FROM $g_db_akademik.presensiharian ph, $g_db_akademik.phsiswa phsiswa WHERE phsiswa.nis='$nis' AND phsiswa.idpresensi=ph.replid AND ph.tanggal1>='$tglawal' AND ph.tanggal2<='$tglakhir' AND phsiswa.keterangan<>''";
$result=QueryDb($sql);
?>
<style type="text/css">
<!--
.style1 {
	color: #666666;
	font-weight: bold;
}
-->
</style>
<table width="100%" border="0" cellspacing="0">
  <tr>
    <td><fieldset><legend class="style1">Presensi Harian</legend>
<table width="100%" border="0" cellspacing="0">
          <tr>
            <td width="11%"><strong>Siswa</strong></td>
            <td width="1%"><strong>:</strong></td>
            <td width="88%">[<?php echo$nis?>]&nbsp;<?php echo$row_nm_sis[nama]?></td>
          </tr>
          <tr>
            <td><strong>Periode</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo$_REQUEST['tglawal']?> s.d. <?php echo$_REQUEST['tglakhir']?></td>
          </tr>
        </table>	
    </fieldset></td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="1" cellspacing="0" class="tab">
  <tr class="header" height="30">
    <td width="3%" align="center">No.</td>
    <td width="42%" >Periode</td>
    <td width="55%" >Keterangan</td>
  </tr>
  <?php
  if (@mysql_num_rows($result)>0){
  $cnt=1;
  while ($row=@mysql_fetch_array($result)){
  	$a="";
	if ($cnt%2==0)
		$a="style='background-color:#FFFFCC'";
  ?>
  <tr height="25" <?php echo$a?>>
    <td align="center"><?php echo$cnt?></td>
    <td><?php echo ShortDateFormat($row[tanggal1])?> s.d. <?php echo ShortDateFormat($row[tanggal2])?></td>
    <td><?php echo$row[keterangan]?></td>
  </tr>
  <?php
  $cnt++;
  }
  } else {
  ?>
   <tr height="25">
    <td align="center" colspan="3">Tidak ada keterangan presensi untuk periode tsb.</td>
  </tr>
  <?php } ?>
</table>

    </td>
  </tr>
</table>
<?php CloseDb() ?>