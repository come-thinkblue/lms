<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016

 * **[N]**/ ?>
<?php
require_once('inc/config.php');
require_once('inc/db_functions.php');
require_once('inc/sessioninfo.php');
require_once('inc/sessionchecker.php');
OpenDb();
$sql = "SELECT YEAR(NOW()),MONTH(NOW()),DAY(NOW())";
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$y = $row[0];
$m = $row[1];
$d = $row[2]; 
CloseDb();
$month = array ('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS Ema</title>
<script type="text/javascript" src="script/dhtml/stmenu.js"></script>
<script type="text/javascript">
function logout(){
	if (confirm('Anda yakin akan keluar dari Aplikasi Ini?'))
		top.location.href = "logout.php";
}
</script>
<style>


.header
{
background-image:url(css/header.png);
background-repeat:repeat-x;
height:136px;
padding:15px;
z-index:-9999;
}
body
{
margin-left:0px;
margin-right:0px;
padding:0;
background-color:#5b6161;
}

.logoHead {
    background-image: url(css/logo.png);
    background-repeat: no-repeat;
    background-size: contain;
    width: 400px;
    height: 90px;
    margin-left: 10px;
    float: left;
}
[class*="icon-"] {
  font-family: 'fontello';
  font-style: normal;
  font-size: 3em;
  speak: none;
}
.icon-home:after { content: "\2602"; } 
.icon-cog:after { content: "\2699"; } 
.icon-cw:after { content: "\27f3"; } 
.icon-location:after { content: "\2629"; } 

* { 
  -webkit-box-sizing: border-box; 
  -moz-box-sizing:    border-box; 
  box-sizing:         border-box; 
  margin: 0;
  padding: 0;
}



a {
  text-decoration: none;
  color: #DD6C4F;
}

a:hover {
  text-decoration:underline;
}

a:focus { 
  outline: none;
}

.nav {
  list-style: none;
  text-align: center;
  width:600px;
  margin-left:50%;
  float:right;
  position:fixed;
  margin-top:10px;
 
}

.nav li {
  position: relative;
  display: inline-block;
  margin-right: -14px;
  z-index:10; /* See: http://css-tricks.com/fighting-the-space-between-inline-block-elements/ */
}



.nav a {
  display: block;
  background-color: #f7f7f7;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#c7c5c5));
  background-image: -webkit-linear-gradient(top, #fff, #c7c5c5); 
  background-image: -moz-linear-gradient(top, #fff, #c7c5c5); 
  background-image: -ms-linear-gradient(top, #fff, #c7c5c5); 
  background-image: -o-linear-gradient(top, #fff, #c7c5c5); 
  color: #a7a7a7;
  margin:15px;
  width: 70px;
  height: 70px;
  position: relative;
  text-align: center;
  line-height: 40px;
  border-radius: 50%;
  box-shadow: 0px 3px 8px #3e969e, inset 0px 5px 8px #fff;
  z-index:10;
  padding-top:5px;
  padding-bottom:10px;
  border:#cde9ea solid 3px;
  
}

.nav a img
{
width:55px;
height:55px;
}


.nav a:hover {
  text-decoration: none;
  color: #555;
  background: #f5f5f5;
   box-shadow: 0px 3px 8px #144447, inset 0px 5px 8px #fff;
  
}


.tool-tip{
	color: #fff;
	background-color: rgba( 0, 0, 0, .4);
	text-shadow: none;
	font-size: 11px;
	visibility: hidden;
	-webkit-border-radius: 7px; 
	-moz-border-radius: 7px; 
	-o-border-radius: 7px; 
	border-radius: 7px;	
	text-align: center;	
	opacity: 0;
	z-index: 999;
	padding:5px 10px 2px 10px;	
	position: absolute;
	cursor: default;
	-webkit-transition: all 200ms ease-in-out;
	-moz-transition: all 200ms ease-in-out;
	-ms-transition: all 200ms ease-in-out;
	-o-transition: all 200ms ease-in-out;
	transition: all 200ms ease-in-out;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	
}

.tool-tip,
.tool-tip.top{
	top: auto;
	bottom: 114%;
	left: 50%;		
}

.tool-tip.top:after,
.tool-tip:after{
	position: absolute;
	bottom: -12px;
	left: 50%;
	margin-left: -7px;
	content: ' ';
	height: 0px;
	width: 0px;
	border: 6px solid transparent;
    border-top-color: rgba( 0, 0, 0, .4);		
}

/* default heights, width and margin w/o Javscript */

.tool-tip,
.tool-tip.top{
	width:120px;;
	height: 25px;
	margin-left: -43px;
}
/* on hover of element containing tooltip default*/

*:not(.on-focus):hover > .tool-tip,
.on-focus input:focus + .tool-tip{
	visibility: visible;
	opacity: 1;
	-webkit-transition: all 200ms ease-in-out;
	-moz-transition: all 200ms ease-in-out;
	-ms-transition: all 200ms ease-in-out;
	-o-transition: all 200ms ease-in-out;
	transition: all 200ms ease-in-out;		
}


/* tool tip slide out */

*:not(.on-focus) > .tool-tip.slideIn,
.on-focus > .tool-tip{
	display: block;
}

.on-focus > .tool-tip.slideIn{
	z-index: -1;
}

.on-focus > input:focus + .tool-tip.slideIn{
	z-index: 1;
}

/* top slideIn */

*:not(.on-focus) > .tool-tip.slideIn,
*:not(.on-focus) > .tool-tip.slideIn.top,
.on-focus > .tool-tip.slideIn,
.on-focus > .tool-tip.slideIn.top{
	bottom: 50%;
}

*:not(.on-focus):hover > .tool-tip.slideIn,
*:not(.on-focus):hover > .tool-tip.slideIn.top,
.on-focus > input:focus + .tool-tip.slideIn,
.on-focus > input:focus + .tool-tip.slideIn.top{
	bottom: 85%;
}	




</style>

</head>
<body> 
<div class="header">
<div class="logoHead"></div>
			<ul class="nav">
			<!--  <li><div class="tool-tip slideIn top">Home</div><a href="home.php" target="content" ><img src="css/icon atas/home.png" alt="s" /></a></li> !-->
				<li><div class="tool-tip slideIn top">Info Akademik</div><a href="siswa/stat.php" target="content"><img src="css/icon atas/akademik.png" alt="s"/></a></li>
                <li><div class="tool-tip slideIn top">Kepegawaian</div><a href="pegawai/statpegawai.php" target="content"><img src="css/icon atas/info.png" alt="s" /></a></li>
				<li><div class="tool-tip slideIn top">Transaksi Keuangan</div><a href="keu/laptransaksi.php" target="content" ><img src="css/icon atas/keuangan.png" alt="s" /></a></li>
				<li><div class="tool-tip slideIn top">Perpustakaan</div><a href="pustaka/pus/stat.all.php" target="content" ><img src="css/icon atas/perpustakaan.png" alt="s" /></a></li>
                <li><div class="tool-tip slideIn top">Logout</div><a href="javascript:logout();"><img src="css/icon atas/logout.png" alt="s" /></a></li>
			</ul>

</div>

</body>

</html>