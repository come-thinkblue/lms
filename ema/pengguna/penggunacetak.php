<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/getheader.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');
$departemen = "yayasan";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS EMA [Cetak Daftar Pengguna]</title>
</head>

<body>

<table border="0" cellpadding="10" cellspacing="5" width="780" align="left">
<tr>
  <td align="left" valign="top" colspan="2">
<?php getHeader($departemen) ?>
	
<center>
  <font size="4"><strong>DAFTAR PENGGUNA</strong></font> <br />
 </center><br /><br />

<table width="100%" border="1" class="tab">
  <tr>
    <td height="25" align="center" class="header">No.</td>
    <td height="25" align="center" class="header">NIP</td>
    <td height="25" align="center" class="header">Nama</td>
    <td align="center" class="header">Last&nbsp;Login</td>
    <td height="25" align="center" class="header">Status</td>
  </tr>
  <?php
  $sql = "SELECT p.nip,p.nama,l.aktif,h.replid,h.lastlogin FROM $db_name_user.hakakses h, $db_name_sdm.pegawai p, $db_name_user.login l WHERE h.modul='EMA' AND p.nip=h.login AND h.login=l.login ORDER BY h.lastlogin";
  $result = QueryDb($sql);
  $num = @mysql_num_rows($result);
  if ($num>0){
  $cnt=1;
  while ($row = @mysql_fetch_row($result)){
    ?>
  <tr>
    <td align="center"><?php echo$cnt?></td>
    <td align="center"><?php echo$row[0]?></td>
    <td><?php echo$row[1]?></td>
    <td align="center"><?php echo$row[4]?></td>
    <td align="center">
    	<?php if ($row[2]==1){ ?>
    	Aktif
        <?php } else { ?>
        Tidak Aktif
    	<?php } ?>    
    </td>
  </tr>
  <?php
  $cnt++;
  }
  } else { 
  ?>
  <tr>
    <td colspan="6" align="center" class="nodata">Tidak ada data</td>
  </tr>
  <?php
  }
  ?>
</table></td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>

</html>