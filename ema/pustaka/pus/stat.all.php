<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../inc/config.php');
require_once('../../inc/db_functions.php');
require_once('../../inc/common.php');
require_once('../../inc/sessioninfo.php'); 
require_once('stat.all.class.php');
OpenDb();
$S = new CStat();
$S->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../../style/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../script/tables.js"></script>
<script type="text/javascript" src="../../script/tools.js"></script>
<script type="text/javascript" src="../../script/ajax.js"></script>
<script type="text/javascript" src="stat.all.js"></script>
</head>
<body topmargin="0">
	<div id="waitBox" style="position:absolute; visibility:hidden;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="32"><img src="../../img/loading.gif" width="32" height="32" border="0" /></td>
            <td>&nbsp;<span class="news_title1">Please&nbsp;wait...</span></td>
          </tr>
        </table>
    </div>
    <div id="content">
      <?php echo$S->Content()?>
    </div>
</body>
</html>
<?php CloseDb(); ?>