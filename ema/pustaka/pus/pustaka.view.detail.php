<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
require_once('../../inc/config.php');
require_once('../../inc/db_functions.php');
require_once('../../inc/common.php');
require_once('../../inc/rupiah.php');
$replid=$_REQUEST[replid];
$sender=$_REQUEST[sender];
OpenDb();
$sql = "SELECT * FROM $g_db_perpustakaan.pustaka WHERE replid='$replid'";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
$judul = $row[judul];
$harga = $row[harga];
$katalog = $row[katalog];
$penerbit = $row[penerbit];
$penulis = $row[penulis];
$tahun = $row[tahun];
$format = $row[format];
$keyword = $row[keyword];
$keteranganfisik = $row[keteranganfisik];
$abstraksi = $row[abstraksi];
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detail Pustaka</title>
<link href="../../style/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../scr/tools.js"></script>
<script language="javascript" src="pustaka.daftar.js"></script>
<style type="text/css">
<!--
.style1 {
	color: #666666;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="8%" align="right" valign="top"><span class="style1">Judul</span></td>
    <td colspan="2"><?php echo$judul?>    </td>
  </tr>
  <tr>
    <td align="right" valign="top"><?php if($sender!='opac'){ ?><span class="style1">Harga&nbsp;Satuan</span><?php } ?></td>
    <td width="92%"><?php if($sender!='opac'){ ?><?php echo formatRupiah($harga)?><?php } ?></td>
    <td width="92%" rowspan="7" valign="top">
    	<div style="margin-left:10px">
            <table width="125" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="125" align="center" valign="middle" bgcolor="#CCCCCC"><img src="../../inc/gambar.php?replid=<?php echo$replid?>&table=$g_db_perpustakaan.pustaka" /></td>
              </tr>
            </table>
   	    </div>    </td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Katalog</strong></td>
    <td>
	<?php 
		$sql = "SELECT kode,nama FROM $g_db_perpustakaan.katalog WHERE replid='$katalog'";
		$result = QueryDb($sql);
		$row = @mysql_fetch_row($result);
		echo $row[0]." - ".$row[1];
	?>    </td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Penerbit</strong></td>
    <td>
	<?php 
		$sql = "SELECT kode,nama FROM $g_db_perpustakaan.penerbit WHERE replid='$penerbit'";
		$result = QueryDb($sql);
		$row = @mysql_fetch_row($result);
		echo $row[0]." - ".$row[1];
	?>    </td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Penulis</strong></td>
    <td>
	<?php 
		$sql = "SELECT kode,nama FROM $g_db_perpustakaan.penulis WHERE replid='$penulis'";
		$result = QueryDb($sql);
		$row = @mysql_fetch_row($result);
		echo $row[0]." - ".$row[1];
	?>    </td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Tahun&nbsp;Terbit</strong></td>
    <td><?php echo$tahun?></td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Format</strong></td>
    <td>
	<?php 
		$sql = "SELECT kode,nama FROM $g_db_perpustakaan.format WHERE replid='$format'";
		$result = QueryDb($sql);
		$row = @mysql_fetch_row($result);
		echo $row[0]." - ".$row[1];
	?>    </td>
  </tr>
  <tr>
    <td align="right" valign="top" class="style1">Keyword</td>
    <td><?php echo$keyword?></td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Keterangan&nbsp;Fisik</strong></td>
    <td colspan="2"><?php echo$keteranganfisik?></td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Abstraksi</strong></td>
    <td colspan="2"><?php echo$abstraksi?></td>
  </tr>
  <tr>
    <td align="right" valign="top"><strong class="style1">Alokasi&nbsp;Jumlah</strong></td>
<td>
        <?php
		$sql = "SELECT p.nama,COUNT(d.replid) FROM $g_db_perpustakaan.daftarpustaka d, $g_db_perpustakaan.perpustakaan p WHERE d.pustaka='$replid' AND d.perpustakaan=p.replid GROUP BY d.perpustakaan ORDER BY p.nama";
		$result = QueryDb($sql);
		$num = @mysql_num_rows($result);
		if ($num>0){
		?>
        <table width="100%" border="1" cellspacing="0" cellpadding="0" class="tab">
          <tr>
            <td height="25" align="center" class="header">Perpustakaan</td>
            <td height="25" align="center" class="header">Jumlah</td>
          </tr>
          <?php
		  while ($row = @mysql_fetch_row($result)){
		  ?>
          <tr>
            <td height="20" align="center"><?php echo$row[0]?></td>
            <td height="20" align="center"><?php echo$row[1]?></td>
          </tr>
          <?php
		  }
		  ?>
      </table>
        <?php
		}
		?>    </td>
    <td>&nbsp;</td>
  </tr>
</table>

</body>
</html>