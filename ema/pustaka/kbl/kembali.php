<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/sessionchecker.php');
require_once('../inc/common.php');
require_once('../inc/rupiah.php');
require_once('../inc/db_functions.php');
require_once('kembali.class.php');
OpenDb();
$K = new CKembali();
$K->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../scr/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../scr/tables.js"></script>
<script type="text/javascript" src="../scr/tools.js"></script>
<script type="text/javascript" src="../scr/rupiah.js"></script>
<script language="javascript" src="../scr/jquery/jquery-1.2.6.js"></script>
<script language="javascript" src="../scr/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="kembali.js"></script>
</head>

<body <?php echo$K->OnLoad()?>>
	<div id="title" align="right">
        <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
        <font style="font-size:18px; color:#999999">Pengembalian Pustaka</font><br />
        <a href="pengembalian.php" class="welc">Pengembalian</a><span class="welc"> > Pengembalian Pustaka</span><br /><br /><br />
    </div>
    <div id="content">
      <?php echo$K->Content()?>
    </div>
</body>
<?php echo$K->OnFinish()?>
</html>
<?php CloseDb(); ?>