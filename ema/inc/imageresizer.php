<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
// =============================================================
//  RESIZE JPEG! IMAGE ONLY                                     
// =============================================================

function ResizeImage($foto, $newwidth, $newheight, $quality, $output)
{
	$uploadedfile = $foto['tmp_name'];	
	if (strlen($uploadedfile) != 0)
	{
		// get type
		$type = $foto['type'];
		
		// get image size
		list($width, $height) = getimagesize($uploadedfile);
	
		// get scalling factor
		$npercent = 1.0;
		if ($newwidth < $width || $newheight < $height) 
		{
			$scalewidth  = $newwidth / $width;
			$scaleheight = $newheight / $height;
			
			if ($scalewidth < $scaleheight)
				$npercent = $scalewidth;
			else
				$npercent = $scaleheight;
		}
		$newheight = $height * $npercent;
		$newwidth  = $width * $npercent;
		
		// echo "Resize to $newwidth x $newheight <br>";
		
		$tmp = imagecreatetruecolor($newwidth, $newheight);
		// imageantialias($tmp, TRUE);
		
		// Create source image
		if ($type == "image/jpeg")
			$src = imagecreatefromjpeg($uploadedfile);
		elseif ($type == "image/gif")
			$src = imagecreatefromgif($uploadedfile);
		elseif ($type == "image/png")
			$src = imagecreatefrompng($uploadedfile);
		
		// Resize and copy to src buffer
		imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		
		// Create output image
		if ($type == "image/jpeg")
			imagejpeg($tmp, $output, $quality);
		elseif ($type == "image/gif")
			imagegif($tmp, $output, $quality);
		elseif ($type == "image/png")
			imagepng($tmp, $output, $quality);
		
		imagedestroy($src);
		imagedestroy($tmp); 
		
		// Set read access by group & all people
		chmod($output, 0644);
		
		return true;
	}
	return false;
}

?>