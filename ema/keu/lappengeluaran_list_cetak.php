<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/sessionchecker.php');
require_once('../inc/getheader.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');
require_once('../inc/rupiah.php');
$tanggal1 = $_REQUEST['tanggal1'];
$tanggal2 = $_REQUEST['tanggal2'];
$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

$idtahunbuku = "";
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = $_REQUEST['idtahunbuku'];


$ndepartemen = $departemen;
$ntahunbuku = getname2('tahunbuku',$db_name_fina.'.tahunbuku','replid',$idtahunbuku);
$nperiode = LongDateFormat($tanggal1)." s.d. ".LongDateFormat($tanggal2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS EMA [Daftar Pengeluaran]</title>
</head>

<body>

<table border="0" cellpadding="10" cellspacing="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
<?php getHeader($departemen) ?>
	
<center>
  <font size="4"><strong>DAFTAR LAPORAN PENGELUARAN</strong></font><br />
 </center><br /><br />
<table width="100%">
<tr>
	<td width="7%" class="news_content1"><strong>Departemen</strong></td>
    <td width="93%" class="news_content1">: 
      <?php echo$departemen ?></td>
</tr>
<tr>
  <td class="news_content1"><strong>Tahun Buku</strong></td>
  <td class="news_content1">: 
      <?php echo$ntahunbuku ?></td>
  </tr>
<tr>
  <td class="news_content1"><strong>Periode</strong></td>
  <td class="news_content1">:
    <?php echo$nperiode ?></td>
  </tr>
</table>
<br />
<?php  OpenDb();   
            $sql = "SELECT d.replid AS id, d.nama, SUM(p.jumlah) AS jumlah FROM $db_name_fina.pengeluaran p, $db_name_fina.datapengeluaran d WHERE p.idpengeluaran = d.replid AND d.departemen = '$departemen' AND p.tanggal BETWEEN '$tanggal1' AND '$tanggal2' GROUP BY d.replid, d.nama ORDER BY d.nama";
            
            $result = QueryDb($sql);    
            if (mysql_num_rows($result) > 0) {   
        ?>    
            <table class="tab" id="table" border="1" style="border-collapse:collapse" width="95%" align="center" bordercolor="#000000">
            <tr height="30" align="center">
                <td width="10%" class="header">No</td>
                <td width="50%" class="header">Pengeluaran</td>
              <td width="*" class="header">Jumlah</td>
            </tr>
            <?php
            
            $cnt = 0;
            $total = 0;
            while ($row = mysql_fetch_array($result)) {
                $total += $row['jumlah'];
            ?>
            <tr height="25" onclicks="show_detail(<?php echo$row['id'] ?>)" styles="cursor:pointer">
                <td align="center"><?php echo++$cnt ?></td>
                <td align="left"><strong><?php echo$row['nama'] ?></strong></td>
              <td align="right"><?php echo formatRupiah($row['jumlah']) ?></td>
            </tr>
            <?php
            }
            CloseDb();
            ?>
            <tr height="30">
                <td bgcolor="#999900" colspan="2" align="center"><font color="#FFFFFF"><strong>T O T A L</strong></font></td>
              <td bgcolor="#999900" align="right">
              <font color="#FFFFFF"><strong><?php echo formatRupiah($total) ?></strong></font>		</td>
            </tr>
            </table>
      <script language='JavaScript'>
                Tables('table', 1, 0);
            </script>
           
        
        <?php	} else { ?>	
        
            <table width="100%" border="0" align="center">          
            <tr>
                <td align="center" valign="middle" height="300">    
                    <span class="err">Tidak ditemukan adanya data.</span>
              </td>
            </tr>
            </table>  
        <?php } ?>
  </td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>

</html>