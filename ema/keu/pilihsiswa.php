<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/sessionchecker.php');

require_once('../inc/getheader.php');
require_once('../inc/common.php');
require_once('../inc/db_functions.php');
OpenDb();
$departemen=$_REQUEST[departemen];
?>
<link href="../style/style.css" rel="stylesheet" type="text/css" />
<form name="frmPilih">
<table width="100%" border="0" cellspacing="2" cellpadding="1">
  <tr>
    <td width="4%" class="tab2">Tingkat</td>
    <td width="96%">
    <div id="tktInfo">
    <select name="tingkat" class="cmbfrm" id="tingkat" onchange="chg_tkt()">
	<?php
	$sql = "SELECT replid,tingkat FROM tingkat WHERE departemen='$departemen'";
	$result = QueryDb($sql);
	while ($row = @mysql_fetch_row($result)){
		if ($tingkat=="")
			$tingkat = $row[0];	
	?>
	<option value="<?php echo$row[0]?>"><?php echo$row[1]?></option>
	<?php
	}
	?>
	</select>
    </div>
    <?php
	$sql = "SELECT replid FROM tahunajaran WHERE departemen='$departemen' AND aktif=1";
	$result = QueryDb($sql);
	$row = @mysql_fetch_array($result);
	$ta = $row[replid];
	?>
    <input type="hidden" name="ta" id="ta" value="<?php echo$ta?>" />
    </td>
  </tr>
  <tr>
    <td class="tab2">Kelas</td>
    <td>
    <div id="klsInfo">
    <select name="kelas" class="cmbfrm" id="kelas" onchange="chg_kls()">
	<?php
	$sql = "SELECT replid,kelas FROM kelas WHERE idtingkat='$tingkat' AND idtahunajaran='$ta' AND aktif=1 ORDER BY kelas";
	$result = QueryDb($sql);
	while ($row = @mysql_fetch_row($result)){
		if ($kelas=="")
			$kelas = $row[0];	
	?>
	<option value="<?php echo$row[0]?>"><?php echo$row[1]?></option>
	<?php
	}
	?>
	</select>
    
    </div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div id="sisInfo">
    <table width="100%" border="1" class="tab">
      <tr>
        <td height="25" align="center" class="header">NIS</td>
        <td height="25" align="center" class="header">Nama</td>
        <td height="25" align="center" class="header">&nbsp;</td>
      </tr>
      <?php
	  $sql = "SELECT * FROM siswa WHERE idkelas=$kelas ORDER BY nama";
	  $result = QueryDb($sql);
	  $num = @mysql_num_rows($result);
	  if ($num>0){
	  while ($row = @mysql_fetch_array($result)){
	  ?>
      <tr>
        <td height="20" align="center"><?php echo$row[nis]?></td>
        <td height="20"><?php echo$row[nama]?></td>
        <td height="20" align="center"><input type="button" value=" > " class="cmbfrm2" onClick="pilihsiswa('<?php echo$row[nis]?>')" /></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td height="20" colspan="3" align="center" class="nodata">Tidak ada data</td>
      </tr>
	  <?php } ?>
    </table>
    </div>
    </td>
  </tr>
</table>
</form>