<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/sessionchecker.php');
require_once('../inc/getheader.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');
require_once('../inc/rupiah.php');
$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
$ndepartemen = $departemen;
	
$idtahunbuku = "";
if (isset($_REQUEST['idtahunbuku']))
	$idtahunbuku = $_REQUEST['idtahunbuku'];
$ntahunbuku = getname2('tahunbuku',$db_name_fina.'.tahunbuku','replid',$idtahunbuku);	

if (isset($_REQUEST['tanggal1']))
	$tanggal1 = $_REQUEST['tanggal1'];

if (isset($_REQUEST['tanggal2']))
	$tanggal2 = $_REQUEST['tanggal2'];
$nperiode = LongDateFormat($tanggal1)." s.d. ".LongDateFormat($tanggal2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JIBAS EMA [Cetak Transaksi Keuangan]</title>
</head>

<body>

<table border="0" cellpadding="10" cellspacing="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
<?php getHeader($departemen) ?>
	
<center>
  <font size="4"><strong>LAPORAN TRANSAKSI KEUANGAN</strong></font><br />
 </center><br /><br />
<table width="100%">
<tr>
	<td width="7%" class="news_content1"><strong>Departemen</strong></td>
    <td width="93%" class="news_content1">: 
      <?php echo$departemen ?></td>
    </tr>
<tr>
  <td class="news_content1"><strong>Tahun Buku</strong></td>
  <td class="news_content1">: 
      <?php echo$ntahunbuku ?></td>
  </tr>
<tr>
  <td class="news_content1"><strong>Periode</strong></td>
  <td class="news_content1">:
    <?php echo$nperiode ?></td>
  </tr>
</table>
<br />
<?php     
        OpenDb();
        //$sql_tot = "SELECT nokas, date_format(tanggal, '%d-%b-%Y') AS tanggal, petugas, transaksi, keterangan, debet, kredit FROM transaksilog WHERE departemen='$departemen' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' AND idtahunbuku = $idtahunbuku with ROLLUP";
        $sql_tot = "SELECT COUNT(nokas), SUM(debet) AS totdebet, SUM(kredit) AS totkredit FROM $db_name_fina.transaksilog WHERE departemen='$departemen' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' AND idtahunbuku = '$idtahunbuku'";
    
        $result_tot = QueryDb($sql_tot);
        $row_tot = mysql_fetch_row($result_tot);
        //$jumlah = $row_tot[0];
        //$total=ceil($jumlah/(int)$varbaris);
        //$akhir = ceil($jumlah/5)*5;
        
        $totaldebet = $row_tot[1];
        $totalkredit = $row_tot[2];
        
        
        $sql = "SELECT nokas, date_format(tanggal, '%d-%b-%Y') AS tanggal, petugas, transaksi, keterangan, debet, kredit FROM $db_name_fina.transaksilog WHERE departemen='$departemen' AND tanggal BETWEEN '$tanggal1' AND '$tanggal2' AND idtahunbuku = '$idtahunbuku' ORDER BY tanggal";
            
        $result = QueryDb($sql);	
        if (mysql_num_rows($result) > 0) {
    ?>    
      <input type="hidden" name="total" id="total" value="<?php echo$total?>"/>
      <table class="tab" id="table" border="1" cellpadding="5" style="border-collapse:collapse" cellspacing="0" width="100%" align="left" bordercolor="#000000">
        <tr height="30" align="center">
            <td width="4%" class="header" >No</td>
            <td width="18%" class="header">No. Jurnal/Tanggal</td>
            <td width="10%" class="header">Petugas</td>
            <td width="*" class="header" >Transaksi</td>
            <td width="15%" class="header">Debet</td>
            <td width="15%" class="header">Kredit</td>
        </tr>
    <?php		//if ($page==0)
                $cnt = 0;
            //else 
              //  $cnt = (int)$page*(int)$varbaris;
            
            while($row = mysql_fetch_array($result)) {
                
    ?>
        <tr height="25">
            <td align="center" valign="top"><?php echo++$cnt ?></td>
            <td align="center" valign="top"><strong><?php echo$row['nokas'] ?></strong><br /><?php echo$row['tanggal'] ?></td>
            <td valign="top" align="center"><?php echo$row['petugas'] ?></td>
            <td align="left" valign="top"><?php echo$row['transaksi'] ?>
            <?php if ($row['keterangan'] <> "") { ?>
            <br /><strong>Keterangan: </strong><?php echo$row['keterangan'] ?>
            <?php } ?>
            </td>
            <td align="right" valign="top"><?php echo formatRupiah($row['debet']) ?></td>
            <td align="right" valign="top"><?php echo formatRupiah($row['kredit']) ?></td>
        </tr>
    <?php
            }
            CloseDb();
        //echo 'total '.$total.' dan '.$page;	
        //if ($total-1 == $page) {
            
            
    ?>
       
        <tr height="30">
            <td colspan="4" align="center" bgcolor="#999900">
            <font color="#FFFFFF"><strong>T O T A L</strong></font>
            </td>
            <td align="right" bgcolor="#999900"><font color="#FFFFFF"><strong><?php echo formatRupiah($totaldebet) ?></strong></font></td>
            <td align="right" bgcolor="#999900"><font color="#FFFFFF"><strong><?php echo formatRupiah($totalkredit) ?></strong></font></td>
        </tr>
    <?php 	//} ?>
      </table>
    <?php } ?>
  </td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>

</html>