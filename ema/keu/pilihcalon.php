<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/sessionchecker.php');
require_once('../inc/getheader.php');
require_once('../inc/common.php');
require_once('../inc/db_functions.php');
OpenDb();
$departemen=$_REQUEST[departemen];
?>
<link href="../style/style.css" rel="stylesheet" type="text/css" />
<form name="frmPilih">
<table width="100%" border="0" cellspacing="2" cellpadding="1">
  <tr>
    <td width="4%" class="tab2">Proses&nbsp;Penerimaan</td>
    <td width="96%">
    <div id="prsInfo">
    <select name="proses" class="cmbfrm" id="proses" onchange="chg_prs()">
	<?php
	$sql = "SELECT replid,proses FROM prosespenerimaansiswa WHERE departemen='$departemen' AND aktif=1";
	$result = QueryDb($sql);
	while ($row = @mysql_fetch_row($result)){
		if ($proses=="")
			$proses = $row[0];	
	?>
	<option value="<?php echo$row[0]?>"><?php echo$row[1]?></option>
	<?php
	}
	?>
	</select>
    </div>
    </td>
  </tr>
  <tr>
    <td class="tab2">Kelompok</td>
    <td>
    <div id="klpInfo">
	<?php $sql = "SELECT replid,kelompok FROM kelompokcalonsiswa WHERE idproses='$proses'"; 
	//echo $sql;
	?>
    
	<select name="kelompok" class="cmbfrm" id="kelompok" onchange="chg_klp()">
	<?php
	
	$result = QueryDb($sql);
	while ($row = @mysql_fetch_row($result)){
		if ($kelompok=="")
			$kelompok = $row[0];	
	?>
	<option value="<?php echo$row[0]?>"><?php echo$row[1]?></option>
	<?php
	}
	?>
	</select>
    </div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    <div id="calInfo">
    <?php if ($kelompok!=""){ ?>
	<table width="100%" border="1" class="tab">
      <tr>
        <td height="25" align="center" class="header">No Pendaftaran</td>
        <td height="25" align="center" class="header">Nama</td>
        <td height="25" align="center" class="header">&nbsp;</td>
      </tr>
      <?php
	  $sql = "SELECT * FROM calonsiswa WHERE idkelompok='$kelompok' ORDER BY nama";
	  $result = QueryDb($sql);
	  $num = @mysql_num_rows($result);
	  if ($num>0){
	  while ($row = @mysql_fetch_array($result)){
	  ?>
      <tr>
        <td height="20" align="center"><?php echo$row[nopendaftaran]?></td>
        <td height="20"><?php echo$row[nama]?></td>
        <td height="20" align="center"><input type="button" value=" > " class="cmbfrm2" onClick="pilihcalon('<?php echo$row[replid]?>')" /></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td height="20" colspan="3" align="center" class="nodata">Tidak ada data</td>
      </tr>
	  <?php } ?>
    </table>
	<?php } ?>
    </div>
    </td>
  </tr>
</table>
</form>