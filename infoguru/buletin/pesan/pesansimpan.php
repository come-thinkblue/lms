<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/db_functions.php');
require_once('../../include/sessionchecker.php');

$balas=$_REQUEST['balas'];
$bulan=$_REQUEST['bulan'];
$tahun=$_REQUEST['tahun'];

$judul = CQ($_REQUEST['judul']);
$tgl=explode("-",$_REQUEST['tanggal']);
$tanggaltampil=$tgl[2]."-".$tgl[1]."-".$tgl[0];
$pesan = $_REQUEST['pesan'];
$pesan = str_replace("'", "#sq;", $pesan);
$idguru=SI_USER_ID();
OpenDb();

$success = true;
BeginTrans();

$sql="INSERT INTO $g_db_umum.pesan SET tanggalpesan=NOW(), tanggaltampil='$tanggalpesan',judul='$judul',
		pesan='$pesan',idguru='$idguru',nis=NULL,keguru=1";
QueryDbTrans($sql, $success);

if ($success)
{
	$sql="SELECT LAST_INSERT_ID()";
	$result=QueryDbTrans($sql, $success);
	$row=@mysql_fetch_row($result);
	$lastid=$row[0];
}

if ($success)
{
	$sql="INSERT INTO $g_db_umum.pesanterkirim SET judul='$judul',idpesan=$lastid";
	$result=QueryDbTrans($sql, $success);
}

//Ambil penerimanya
$jum=(int)$_REQUEST['jum']-1;
$receiverall=$_REQUEST['receiver'];
$x=0;
$receiver=explode("|",$receiverall);
while ($x<=$jum && $success)
{
	if ($receiver[$x]!="")
	{
		$sql="INSERT INTO $g_db_umum.tujuanpesan SET idpesan='$lastid', idpenerima='".$receiver[$x]."',baru='1'";
		$result=QueryDbTrans($sql, $success);
	}
	$x++;
}

if ($success)
{
	CommitTrans();
	CloseDb();	?>
<script language="javascript">
	alert ('Pesan telah dikirim');
	document.location.href="pesanguru_footer.php";
</script>
<?php
}
else
{
	RollbackTrans();
	CloseDb(); ?>
<script language="javascript">
	alert ('Gagal mengirimkan pesan');
	document.location.href="pesanguru_footer.php";
</script>
<?php	
}
?>