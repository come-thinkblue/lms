<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/config.php');
require_once('../../include/db_functions.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/sessionchecker.php');
$bulan="";
if (isset($_REQUEST['bulan']))
	$bulan=$_REQUEST['bulan'];

$tahun="";
if (isset($_REQUEST['tahun']))
	$tahun=$_REQUEST['tahun'];

$xxx="";
if (isset($_REQUEST['xxx']))
	$xxx=$_REQUEST['xxx'];

$departemen="";
if (isset($_REQUEST['departemen']))
	$departemen=$_REQUEST['departemen'];
$tahunajaran="";
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran=$_REQUEST['tahunajaran'];
$tingkat="";
if (isset($_REQUEST['tingkat']))
	$tingkat=$_REQUEST['tingkat'];
$kelas="";
if (isset($_REQUEST['kelas']))
	$kelas=$_REQUEST['kelas'];		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../style/style.css" rel="stylesheet" type="text/css" />
<title>Untitled Document</title>
<script language="javascript" type="text/javascript" src="../../script/tables.js"></script>
<script language="javascript" type="text/javascript">
function cek_all() {
	var x;
	var jum = document.tujuan.numsiswa.value;
	var ceked = document.tujuan.cek.checked;
	for (x=1;x<=jum;x++){
		if (ceked==true){
			document.getElementById("ceknis"+x).checked=true;
		} else {
			document.getElementById("ceknis"+x).checked=false;
		}
	}
}
</script>
</head>
<body>
<form name="tujuan" id="tujuan" action="pesansimpan.php">
<table width="100%" border="0" cellspacing="0" class="tab" id="table">
  <tr>
    <th width="18%" height="30" class="header" scope="row">No</th>
    <td width="3%" height="30" class="header"><input type="checkbox" name="cek" id="cek" onClick="cek_all()" title="Pilih semua" onMouseOver="showhint('Pilih semua', this, event, '120px')"/></td>
    <td width="26%" height="30" class="header">NIS</td>
    <td width="53%" height="30" class="header">Nama</td>
  </tr>
  <?php 
			OpenDb();
			$sql="SELECT * FROM $g_db_akademik.siswa WHERE idkelas='$kelas'  ORDER BY nama";
			$result=QueryDb($sql);
			$cnt=1;
			while ($row=@mysql_fetch_array($result)){
  ?>
   <tr>
    <th height="25" scope="row"><?php echo$cnt?></th>
    <td height="25"><input type="checkbox" name="ceknis<?php echo$cnt?>" id="ceknis<?php echo$cnt?>"/></td>
    <td height="25"><?php echo$row['nis']?><input type="hidden" name="nis<?php echo$cnt?>" id="nis<?php echo$cnt?>" value="<?php echo$row['nis']?>"/>
      <input type="hidden" name="kirimin<?php echo$cnt?>" id="kirimin<?php echo$cnt?>"/></td>
    <td height="25"><?php echo$row['nama']?></td>
  </tr>
  <?php 
  $cnt++;
  } 
  ?>
</table>
<input type="hidden" name="numsiswa" id="numsiswa" value="<?php echo$cnt-1?>" />
<input type="hidden" name="numsiswakirim" id="numsiswakirim"/>    

<script language='JavaScript'>
			Tables('table', 1, 0);
</script>
</body>
</html>