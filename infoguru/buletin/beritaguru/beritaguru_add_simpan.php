<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/db_functions.php');
require_once('../../include/sessionchecker.php');
require_once('../../include/fileutil.php');

$sender = $_REQUEST['sender'];

if ($sender == "tambah")
{
  OpenDb();
  
  $jam = date(H).":".date(i).":00";
  $judul = CQ($_REQUEST['judul']);
  
  $tgl = explode("-", $_REQUEST['tanggal']);
  $tanggal = $tgl[2]."-".$tgl[1]."-".$tgl[0];
  
  $abstrak = CQ($_REQUEST['abstrak']);
  
  $isi = $_REQUEST['isi'];
  $isi = str_replace("'", "#sq;", $isi);
  $idguru = SI_USER_ID();
  
  $sql1 = "INSERT INTO $g_db_umum.beritaguru
			  SET judul = '$judul', tanggal = '$tanggal $jam',
			      abstrak = '$abstrak', isi = '$isi', idguru = '$idguru'";
  QueryDb($sql1);
  CloseDb();?>
  <script language="javascript">
	parent.beritaguru_header.lihat();
  </script>
<?php
}
elseif ($sender == "ubah")
{
	OpenDb();
	
	$replid = $_REQUEST['replid'];
	$page = (int)$_REQUEST['page'];
	$bulan = $_REQUEST['bulan'];
	$tahun = $_REQUEST['tahun'];
	
	$judul = CQ($_REQUEST['judul']);
	
	$tgl = explode("-",$_REQUEST['tanggal']);
	$tanggal = $tgl[2]."-".$tgl[1]."-".$tgl[0];
	
	$abstrak = CQ($_REQUEST['abstrak']);
	
	$isi = $_REQUEST['isi'];
	$isi = str_replace("'", "#sq;", $isi);
	
	$idguru = SI_USER_ID();
	
	$sql18 = "UPDATE $g_db_umum.beritaguru
			     SET judul='$judul', tanggal='$tanggal',
				     abstrak='$abstrak', isi='$isi', idguru='$idguru'
			   WHERE replid='$replid'";
	$result18 = QueryDb($sql18);
	
	CloseDb(); ?>
<script language="javascript">
   document.location.href="beritaguru_footer.php?page=<?php echo$page?>&tahun=<?php echo$tahun?>&bulan=<?php echo$bulan?>";
</script>
<?php
}
?>