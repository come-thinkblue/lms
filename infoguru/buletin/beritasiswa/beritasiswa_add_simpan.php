<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/db_functions.php');
require_once('../../include/sessionchecker.php');

$sender=$_REQUEST['sender'];
if ($sender=="tambah")
{
 OpenDb();
 $judul=CQ($_REQUEST['judul']);
 $tgl=explode("-",$_REQUEST['tanggal']);
 $tanggal=$tgl[2]."-".$tgl[1]."-".$tgl[0];
 $jam=date(H).":".date(i).":00";
 $abstrak=CQ($_REQUEST['abstrak']);
 $isi=$_REQUEST['isi'];
 $isi = str_replace("'", "#sq;", $isi);
 $idguru=SI_USER_ID();
 $sql1="INSERT INTO $g_db_umum.beritasiswa
			 SET judul='$judul', tanggal='".$tanggal." ".$jam."',
				 abstrak='$abstrak', isi='$isi', idpengirim='$idguru'";
 $result1=QueryDb($sql1);
 CloseDb();
?>
<script language="javascript">
	parent.beritasiswa_header.lihat();
</script>
<?php
}
elseif ($sender=="ubah")
{
	OpenDb();
	$page=(int)$_REQUEST['page'];
	$bulan=$_REQUEST['bulan'];
	$tahun=$_REQUEST['tahun'];
	
	$judul=CQ($_REQUEST['judul']);
	$tgl=explode("-",$_REQUEST['tanggal']);
	$tanggal=$tgl[2]."-".$tgl[1]."-".$tgl[0];
	$abstrak=CQ($_REQUEST['abstrak']);
	$isi=$_REQUEST['isi'];
	$isi = str_replace("'", "#sq;", $isi);
	$idguru=SI_USER_ID();
	$replid=$_REQUEST['replid'];
	
	$sql18="UPDATE $g_db_umum.beritasiswa
			   SET judul='$judul', tanggal='$tanggal',
				   abstrak='$abstrak', isi='$isi', idpengirim='$idguru'
			 WHERE replid='$replid'";
	$result18=QueryDb($sql18);
	CloseDb();
?>
<script language="javascript">
document.location.href="beritasiswa_footer.php?page=<?php echo$page?>&tahun=<?php echo$tahun?>&bulan=<?php echo$bulan?>";
</script>
<?php } ?>