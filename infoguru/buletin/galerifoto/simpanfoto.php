<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../../include/common.php');
require_once('../../include/sessioninfo.php');
require_once('../../include/config.php');
require_once('../../include/db_functions.php');
require_once('../../include/imageresizer.php');
require_once('../../include/fileinfo.php');
require_once('../../include/fileutil.php');
require_once('../../include/sessionchecker.php');

$source = $_REQUEST["source"];
for ($i = 1; $i <= 3; $i++)
{
	$nama = $_REQUEST["nama".$i];
	$keterangan = $_REQUEST["keterangan".$i];
	$foto =	$_FILES["file".$i];
  	
	$idguru = SI_USER_ID();
	$salt = RandomString(7);
	
	$origfile = $foto['name'];
	$ext = GetFileExt($origfile);
	$fn = GetFileName($origfile);
	$fn = str_replace(" ", "", $fn);
	$fn = date('ymdHis') . "-" . $idguru . "-" . $salt . "-" . $fn;
	$fn = md5($fn) . $ext;
	
	$output1 = "$FILESHARE_UPLOAD_DIR/galeriguru/photos/";
	if (!is_dir($output1))
	{
		mkdir($output1, 0750, true);
		
		$fhtaccess = "$output1/.htaccess";
		$fhtaccess = str_replace("//", "/", $fhtaccess);
		if ($fp = @fopen($fhtaccess, "w"))
		{
			@fwrite($fp, "Options -Indexes\r\n");
			@fclose($fp);
		}
	}
	
	$output2 = "$FILESHARE_UPLOAD_DIR/galeriguru/thumbnails/";	
	if (!is_dir($output2))
	{
		mkdir($output2, 0740, true);
		
		$fhtaccess = "$output2/.htaccess";
		$fhtaccess = str_replace("//", "/", $fhtaccess);
		if ($fp = @fopen($fhtaccess, "w"))
		{
			@fwrite($fp, "Options -Indexes\r\n");
			@fclose($fp);
		}
	}

	if ($origfile != "")
	{
	   $output1 = "$output1/$fn";
		ResizeImage($foto, 800, 600, 70, $output1);
		
		$output2 = "$output2/$fn";
		ResizeImage($foto, 125, 75, 70, $output2);
	
		OpenDb();
		$sql = "INSERT INTO $g_db_umum.galerifoto
				   SET idguru='".SI_USER_ID()."', nama='$nama', keterangan='$keterangan', filename='$fn'";
		$result = QueryDb($sql);
		CloseDb();
	
		if (!$result)
		{ ?>
		<script language="javascript">
			alert ('Gagal menyimpan Gambar <?php echo$foto[name]?>');
			opener.ubah_profil();
			window.close()
		</script>
	<?php	}
	}
}
?>
<script language="javascript">
	opener.get_fresh();
	window.close()
</script>