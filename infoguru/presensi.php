<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
include('../charts/FusionCharts.php');	

require_once('include/errorhandler.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('include/common.php');
require_once('include/config.php');


OpenDb();

include('menu.php');

$page='p';
if (isset($_REQUEST[page]))
	$page = $_REQUEST[page];
?>
<html>
<head>
<title>pelajaran</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="script/tooltips.js"></script>
<script type="text/javascript">
function over(id){
	var actmenu = document.getElementById('actmenu').value;
	if (actmenu==id)
		return false;
		
	if (actmenu=='g')
		document.getElementById('img').src='images/p_over.png';
	else 
		document.getElementById('img').src='images/g_over.png';
}
function out(id){
	var actmenu = document.getElementById('actmenu').value;
	if (actmenu==id)
		return false;
	
	if (actmenu=='g')
		document.getElementById('img').src='images/g.png';
	else
		document.getElementById('img').src='images/p.png';
}
function show(id){
	if (id=='g'){
		document.getElementById('actmenu').value='g';
		document.getElementById('img').src='images/g.png';
		document.getElementById('slice_g').style.display='';
		document.getElementById('slice_p').style.display='none';
	} else {
		document.getElementById('actmenu').value='p';
		document.getElementById('img').src='images/p.png';
		document.getElementById('slice_p').style.display='';
		document.getElementById('slice_g').style.display='none';
	}	
}
</script>
</head>
<body  onLoad="show('<?php echo$page?>')">
<!-- ImageReady Slices (Untitled-1) -->
<div id="content" style="margin-top:20px;"> 
<div class="wrapper">

<?php 
$que1="SELECT Count(guru.nip) as jumlah, pelajaran.nama, pelajaran.kode FROM pelajaran INNER JOIN guru ON guru.idpelajaran = pelajaran.replid
GROUP BY guru.idpelajaran, pelajaran.nama ";
$result1=mysql_query($que1);
$jumrows1 =mysql_num_rows($result1);

$data ="<chart showvalues='1' caption='' canvasBorderColor='1D8BD1' canvasBorderAlpha='60'
  enablesmartlabels='1' showlabels='1' showpercentvalues='0' logoURL='kecil.png'>";

if ($jumrows1>0) {
				$no1=0;
while ($row2 = mysql_fetch_array($result1)) {
					$data .= "<set label='" . $row2['kode']."' value='" . $row2['jumlah'] . "' />";
					$no1++;
				}
				$data.="</chart>";
              }
?>

        <div class="fluid">
            <div class="widget gridWelcome" style="width:35%;">
              
                <div class="body">
				<?php $user=$nama;?>
                <h1 style="color:#128f97; font-weight:800;">Selamat Datang</h1>
				<h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;"><?php echo $user;?> </br></h1>
			  <h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;">di Absensi Siswa</div>
                <ul class="middleNavA">
            <li><a href="presensi/formpresensi_pelajaran.php" title="Cetak Form Pelajaran"><img src="css/icon_pelajaran/pelajaran.png" alt="" /><p>Cetak Form</p><p>Absensi</p></a></li>
            <li><a href="presensi/presensi_main.php" title="Input Presensi"><img src="css/icon_pelajaran/rpp.png" alt="" /><p>Input</p><p>Presensi</p></a></li>
                 <li><a href="presensi/lap_kelas_main.php" title="Laporan Presensi Pelajaran"><img src="css/icon_pelajaran/pelajaran.png" alt="" /><p>Laporan Presensi</p><p>Pelajaran</p></a></li>
            <li><a href="presensi/lap_refleksi_main.php" title="Input Presensi"><img src="css/icon_pelajaran/rpp.png" alt="" /><p>Refleksi</p><p>Mengajar</p></a></li>
                 <li><a href="presensi/statistik_siswa_main.php" title="Cetak Form Pelajaran"><img src="css/icon_pelajaran/pelajaran.png" alt="" /><p>Laporan</p><p>Presensi Kelas</p></a></li>
            <li><a href="presensi/statistik_kelas_main.php" title="Input Presensi"><img src="css/icon_pelajaran/rpp.png" alt="" /><p>Statistik</p><p>Prer Tingkat</p></a></li>
        </ul>
                 
    <?php
function jumlahData($count, $from){
$quePeg="SELECT Count(".$count.") as jumlah FROM ".$from."";
$resultPeg=mysql_query($quePeg);
$jumPeg=mysql_fetch_array($resultPeg);
$jumlah= $jumPeg['jumlah'];
return $jumlah;
}
	?>        
            
            
        </div>


</div>


</div>
<!-- End ImageReady Slices -->
</body>
</html>