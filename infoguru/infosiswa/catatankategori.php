<?php
/**[N]**
MAN Kota Blitar
 **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once("../include/sessionchecker.php");

OpenDb();
$op = "";
if (isset($_REQUEST['op']))
	$op = $_REQUEST['op'];

if ($op == "dw8dxn8w9ms8zs22") {
	$sql = "UPDATE $g_db_umum.catatankategori SET aktif = '$_REQUEST[newaktif]' WHERE replid = '$_REQUEST[replid]' ";
	QueryDb($sql);
} 
if ($op == "xm8r389xemx23xb2378e23") {
	$sql = "DELETE FROM $g_db_umum.catatankategori WHERE replid = '$_REQUEST[replid]'";
	$result = QueryDb($sql);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function tambah() {
	newWindow('catatankategori_add.php', 'TambahCatatanKategori','390','160','resizable=1,scrollbars=0,status=0,toolbar=0')
}

function refresh() {
	document.location.reload();
}

function setaktif(replid, aktif) {
	var msg;
	var newaktif;
	
	if (aktif == 1) {
		msg = "Apakah anda yakin akan mengubah kategori ini menjadi TIDAK AKTIF?";
		newaktif = 0;
	} else	{	
		msg = "Apakah anda yakin akan mengubah kategori ini menjadi AKTIF?";
		newaktif = 1;
	}
	
	if (confirm(msg)) 
		document.location.href = "catatankategori.php?op=dw8dxn8w9ms8zs22&replid="+replid+"&newaktif="+newaktif;
}

function edit(replid) {
	newWindow('catatankategori_edit.php?replid='+replid, 'UbahCatatanKejadian','385','149','resizable=1,scrollbars=0,status=0,toolbar=0')
}

function hapus(replid) {
	if (confirm("Apakah anda yakin akan menghapus kategori ini?"))
		document.location.href = "catatankategori.php?op=xm8r389xemx23xb2378e23&replid="+replid;
}

function cetak() {
	newWindow('catatankategori_cetak.php', 'CetakKategori','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
}
</script>
</head>

<body>

<table border="0" width="100%" height="100%">
<!-- TABLE BACKGROUND IMAGE -->
<tr><td align="center" valign="top" background="../images/b_departemen.png" style="margin:0;padding:0;background-repeat:no-repeat;">

<table border="0" width="100%" align="center">
<!-- TABLE CENTER -->
<tr height="300">
  <td align="left" valign="top">

	<table border="0"width="95%" align="center">
    <tr>
        <td align="right"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Kategori Catatan </font></td>
    </tr>
    <tr>
        <td align="right">
         <a href="../catatankejadian.php" target="framecenter">
          <font size="1" face="Verdana" color="#000000"><b>Catatan Kejadian Siswa</b></font></a>
          &nbsp>&nbsp <font size="1" face="Verdana" color="#000000">Kategori Catatan </font>
        </td>
    </tr>
     <tr>
      <td align="left">&nbsp;</td>
      </tr>
	</table>
	<br /><br />
    <?php
	$sql = "SELECT * FROM $g_db_umum.catatankategori ORDER BY replid";
	//echo $sql;    
	$result = QueryDb($sql);
	if (@mysql_num_rows($result) > 0){
	?>
    <table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
    <!-- TABLE CONTENT -->
    <tr><td align="right">
    
    <a href="#" onClick="document.location.reload()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
    <a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')" />&nbsp;Cetak</a>&nbsp;&nbsp;    
<?php	if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
	    <a href="JavaScript:tambah()"><img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')" />&nbsp;Tambah</a>
<?php	} ?>    
    </td></tr>
    </table><br />
    <table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="95%" align="center" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="49%" class="header" align="center">Kategori</td>
        <td width="7%" class="header" align="center">Aktif</td>
        <td width="30%" class="header" align="center">Keterangan</td>
        <?php		if (SI_USER_LEVEL() != $SI_USER_STAFF) {  ?> <td width="10%" class="header" align="center"></td><?php } ?>
    </tr>
<?php 	
	
	$cnt = 0;
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">
    	<td align="center"><?php echo++$cnt ?></td>
        <td><?php echo$row[kategori] ?></td>
        
        <td align="center">
        
<?php		if (SI_USER_LEVEL() == $SI_USER_STAFF) {  
			if ($row[aktif] == 1) { ?> 
            	<img src="../images/ico/aktif.png" border="0" onMouseOver="showhint('Status Aktif!', this, event, '50px')"/>
<?php			} else { ?>                
				<img src="../images/ico/nonaktif.png" border="0" onMouseOver="showhint('Status Tidak Aktif!', this, event, '50px')"/>
<?php			}
		} else { 
			if ($row[aktif] == 1) { ?>
				<a href="JavaScript:setaktif(<?php echo$row[replid] ?>, <?php echo$row[aktif] ?>)"><img src="../images/ico/aktif.png" border="0" onMouseOver="showhint('Status Aktif!', this, event, '50px')"/></a>
<?php			} else { ?>
				<a href="JavaScript:setaktif(<?php echo$row[replid] ?>, <?php echo$row[aktif] ?>)"><img src="../images/ico/nonaktif.png" border="0" onMouseOver="showhint('Status Tidak Aktif!', this, event, '50px')"/></a>
<?php			} //end if
		} //end if ?>        
        
        </td>
        <td><?php echo$row[keterangan] ?></td>
<?php		if (SI_USER_LEVEL() != $SI_USER_STAFF) {  ?>         
		<td align="center">
            <a href="JavaScript:edit(<?php echo$row[replid] ?>)"><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Kategori!', this, event, '75px')" /></a>&nbsp;
            <a href="JavaScript:hapus(<?php echo$row[replid] ?>)"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Kategori!', this, event, '75px')"/></a>
        </td>
<?php		} ?>  
    </tr>
<?php	} 
	 ?>	
	
    
    <!-- END TABLE CONTENT -->
    </table>
    <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>
	<br>
	<div align="left" style="margin-left:20px">
		* Kategori Catatan Siswa hanya dapat dikelola oleh Administrator LMS MAN Kota Blitar
	</div>
	</td></tr>
<!-- END TABLE CENTER -->    
</table>
<?php	} else { ?>

<table width="100%" border="0" align="center">

<tr>
	<td align="center" valign="middle" height="250" colspan="2">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data.
       <?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
        <br />Klik &nbsp;<a href="JavaScript:tambah()" ><font size = "2" color ="green">di sini</font></a>&nbsp;untuk mengisi data baru.
        <?php } ?>
        </p></b></font>
	</td>
</tr>
</table>  
<?php } ?> 
</td></tr>
<!-- END TABLE BACKGROUND IMAGE -->
</table>    

</body>
</html>
<?php CloseDb();?>