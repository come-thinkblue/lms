<?php
/**[N]**
MAN Kota Blitar
 **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once("../include/sessionchecker.php");

$nis = "";
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
$tahunajaran = "";
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
$idkategori = "";
if (isset($_REQUEST['idkategori']))
	$idkategori = $_REQUEST['idkategori'];	
OpenDb();
$res=QueryDb("SELECT kategori FROM $g_db_umum.catatankategori WHERE replid='$idkategori'");
$row=@mysql_fetch_array($res);
$namakat=$row[kategori];
CloseDb();	
OpenDb();
$res=QueryDb("SELECT tahunajaran FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'");
$row=@mysql_fetch_array($res);
$namathnajrn=$row[tahunajaran];
CloseDb();
$op = "";
if (isset($_REQUEST['op']))
	$op = $_REQUEST['op'];
if ($op=="kwe9823h98hd29h98hd9h"){
	OpenDb();
	$sql="DELETE FROM $g_db_umum.catatansiswa WHERE replid='$_REQUEST[replid]'";
	QueryDb($sql);
	?>
	<script language="javascript" type="text/javascript">
		parent.catatansiswamenu.willshow('<?php echo$idkategori?>');
	</script>
	<?php
	CloseDb();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../style/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../script/tables.js"></script>
<script language="javascript" type="text/javascript">
function get_fresh(){
	var nis = document.getElementById('nis').value;
	var idkategori = document.getElementById('idkategori').value;
	var tahunajaran = document.getElementById('tahunajaran').value;
	document.location.href="catatansiswacontent.php?nis="+nis+"&idkategori="+idkategori+"&tahunajaran="+tahunajaran;
}
function ubah(replid){
	var nis = document.getElementById('nis').value;
	var idkategori = document.getElementById('idkategori').value;
	var tahunajaran = document.getElementById('tahunajaran').value;
	document.location.href="catatansiswaedit.php?replid="+replid+"&nis="+nis+"&idkategori="+idkategori+"&tahunajaran="+tahunajaran;
}
function hapus(replid){
	var nis = document.getElementById('nis').value;
	var idkategori = document.getElementById('idkategori').value;
	var tahunajaran = document.getElementById('tahunajaran').value;
	if (confirm('Anda yakin akan menghapus catatan siswa ini ?'))
	document.location.href="catatansiswacontent.php?op=kwe9823h98hd29h98hd9h&replid="+replid+"&nis="+nis+"&idkategori="+idkategori+"&tahunajaran="+tahunajaran;
}
</script>
</head>

<body leftmargin="0" topmargin="0">
<input type="hidden" id="nis" name="nis" value="<?php echo$nis?>">
<input type="hidden" id="tahunajaran" name="tahunajaran" value="<?php echo$tahunajaran?>">
<input type="hidden" id="idkategori" name="idkategori" value="<?php echo$idkategori?>">
<strong>Catatan : 
<?php echo$namakat?>
<BR>
Tahun Ajaran : 
<?php echo$namathnajrn?>
</strong><BR><BR>
<table width="100%" border="1" cellspacing="0" bordercolor="#000000" class="tab" >
  <tr>
    <td height="30" class="header">No</td>
    <td height="30" class="header">Tanggal/Guru</td>
    <td height="30" class="header">Catatan</td>
    <td height="30" class="header">&nbsp;</td>
  </tr>
  <?php
  OpenDb();
  $sql="SELECT c.replid as replid,c.judul as judul, c.catatan as catatan, c.nip as nip, p.nama as nama, c.tanggal as tanggal ".
  	   "FROM $g_db_umum.catatansiswa c, $g_db_pegawai.pegawai p, $g_db_akademik.kelas k ".
	   "WHERE c.nis='$nis' AND c.idkelas=k.replid AND k.idtahunajaran='$tahunajaran' AND p.nip=c.nip AND c.idkategori='$idkategori'";
  //echo $sql;
  //exit;
  $result=QueryDb($sql);
  $num=@mysql_num_rows($result);
  if ($num>0){
  	$cnt=1;
	while ($row=@mysql_fetch_array($result)){
  ?>
  <tr>
    <td height="25" valign="top"><?php echo$cnt?></td>
    <td height="25" valign="top"><?php echo ShortDateFormat($row[tanggal])?><br />
    <?php echo$row[nip]?>-<?php echo$row[nama]?></td>
    <td height="25" valign="top"><strong><?php echo$row[judul]?></strong><br />
    <?php echo$row[catatan]?></td>
    <td height="25" valign="top">
	<?php
    if ($row[nip]==SI_USER_ID()){
	?>
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td><img onClick="ubah('<?php echo$row[replid]?>')" src="../images/ico/ubah.png" style="cursor:pointer" /></td>
        <td><img onClick="hapus('<?php echo$row[replid]?>')" src="../images/ico/hapus.png" style="cursor:pointer" /></td>
      </tr>
    </table>
    <?php } ?>
    </td>
  </tr>
  <?php $cnt++;
  	}
  } else { ?>
  <tr>
    <td height="25" colspan="4"><div align="center"><em>Tidak ada catatan Kejadian Siswa untuk NIS : 
      <?php echo$nis?>
    </em></div></td>
  </tr>
  <?php } ?>
</table>

</body>
</html>

<script language="javascript">
//var sprytextfield = new Spry.Widget.ValidationTextField("judul");
//var spryselect = new Spry.Widget.ValidationSelect("kategori");
</script>
<script language='JavaScript'>
	//Tables('table', 1, 0);
</script>