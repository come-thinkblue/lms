<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 * **[N]**/ ?>
<?php




include('../charts/FusionCharts.php');	

require_once('include/errorhandler.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('include/common.php');
require_once('include/config.php');


OpenDb();

include('menu.php');

$middle="0";
if (isset($_REQUEST['flag'])){
	$middle="1";
	} else {
	$middle="0";
	}

$page='p';
if (isset($_REQUEST[page]))
	$page = $_REQUEST[page];
?>
<html>
<head>
<title>pelajaran</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="script/tooltips.js"></script>
<script type="text/javascript">
function over(id){
	var actmenu = document.getElementById('actmenu').value;
	if (actmenu==id)
		return false;
		
	if (actmenu=='g')
		document.getElementById('img').src='images/p_over.png';
	else 
		document.getElementById('img').src='images/g_over.png';
}
function out(id){
	var actmenu = document.getElementById('actmenu').value;
	if (actmenu==id)
		return false;
	
	if (actmenu=='g')
		document.getElementById('img').src='images/g.png';
	else
		document.getElementById('img').src='images/p.png';
}
function show(id){
	if (id=='g'){
		document.getElementById('actmenu').value='g';
		document.getElementById('img').src='images/g.png';
		document.getElementById('slice_g').style.display='';
		document.getElementById('slice_p').style.display='none';
	} else {
		document.getElementById('actmenu').value='p';
		document.getElementById('img').src='images/p.png';
		document.getElementById('slice_p').style.display='';
		document.getElementById('slice_g').style.display='none';
	}	
}
</script>
</head>
	<body  onLoad="show('<?php echo$page?>')">
		<!-- ImageReady Slices (Untitled-1) -->
		<div id="content" style="margin-top:20px;"> 
			<div class="wrapper">
				<?php 
				$que1="SELECT Count(guru.nip) as jumlah, pelajaran.nama, pelajaran.kode FROM pelajaran INNER JOIN guru ON guru.idpelajaran = pelajaran.replid
				GROUP BY guru.idpelajaran, pelajaran.nama ";
				$result1=mysql_query($que1);
				$jumrows1 =mysql_num_rows($result1);

				$data ="<chart showvalues='1' caption='' canvasBorderColor='1D8BD1' canvasBorderAlpha='60'
				enablesmartlabels='1' showlabels='1' showpercentvalues='0' logoURL='kecil.png'>";

				if ($jumrows1>0) {
					$no1=0;
					while ($row2 = mysql_fetch_array($result1)) {
						$data .= "<set label='" . $row2['kode']."' value='" . $row2['jumlah'] . "' />";
						$no1++;
					}
					$data.="</chart>";
				}
				?>
				<?php 
				if ($login == 'landlord') { 
					?>
					<div class="fluid">
						<div class="widget gridWelcome" style="width:30%;">
							<div class="body">
								<?php $user=$nama;?>
								<h1 style="color:#128f97; font-weight:800;">Selamat Datang</h1>
								<h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;"><?php echo $user;?> </br></h1>
								<h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;">di Penilaian Siswa </br></h1>
								<p>Pada Halaman ini user terlebih dahulu harus melakukan beberapa penyettingan tentang guru dan pelajaran menu yang disediakan adalah sebagai berikut:</p></div>

								<ul class="middleNavA">
									<li><a href="penilaian/lap_pelajaran_main.php" title="Pendataan Pelajaran"><img src="css/icon_pelajaran/pelajaran.png" alt="" /><p>Laporan Nilai</p><p>Per Siswa</p></a></li>
									<li><a href="penilaian/lap_rapor_main.php" title="Setting RPP"><img src="css/icon_pelajaran/rpp.png" alt="" /><p>Nilai</p><p>Rapor Siswa</p></a></li>
					<?php 
				}
				else { 
					?>
					<div class="fluid">
						<div class="widget gridWelcome" style="width:30%;">
							<div class="body">
							<?php $user=$nama;?>
							<h1 style="color:#128f97; font-weight:800;">Selamat Datang</h1>
							<h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;"><?php echo $user;?> </br></h1>
							<h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;">di Penilaian Siswa </br></h1>
							<p>Pada Halaman ini user terlebih dahulu harus melakukan beberapa penyettingan tentang guru dan pelajaran menu yang disediakan adalah sebagai berikut:</p></div>

							<ul class="middleNavA">
								<li><a href="penilaian/formpenilaian.php" title="Cetak"><img src="css/icon_pelajaran/pelajaran.png" alt="" /><p>Cetak Form</p><p>Penilaian</p></a></li>
								<li><a href="penilaian/lihat_nilai_pelajaran.php" title="Setting RPP"><img src="css/icon_pelajaran/rpp.png" alt="" /><p>Penilaian</p><p>Pelajaran</p></a></li>
								<li><a href="penilaian/lihat_penentuan.php" title="Aspek Penilaian"><img src="css/icon_pelajaran/aspekPenilaian.png" alt="" /><p>Perhitungan</p> <p>Nilai Rapor</p> </a></li>
								<li><a href="penilaian/ujian_rpp_siswa.php" title="Setting Jenis Pengujian"><img src="css/icon_pelajaran/jenisPengujian.png" alt="" /><p>Input</p><p>RPP</p> </a></li>
								<li><a href="penilaian/ujian_rpp_kelas.php" title="Aturan Grading"><img src="css/icon_pelajaran/grading.png"alt="" /><p>Daftar</p><p>RPP</p> </a></li>
								<li><a href="penilaian/komentar_main.php" title="Aturan Grading"><img src="css/icon_pelajaran/grading.png"alt="" /><p>Komentar</p><p>Rapor</p> </a></li>
					<?php 
				} 
					?>
					</ul>
				</div>
				<div class="widget grid7">
					<div class="whead"><h6>Jumlah Guru Tiap Pelajaran</h6><div class="clear"></div></div>
					<div class="body">
						<?php echo renderChartHTML("../charts/Column3D.swf", "", $data, "tes", "100%", "300", false);?>
					</div>
				</div>
				<?php
				function jumlahData($count, $from){
					$quePeg="SELECT Count(".$count.") as jumlah FROM ".$from."";
					$resultPeg=mysql_query($quePeg);
					$jumPeg=mysql_fetch_array($resultPeg);
					$jumlah= $jumPeg['jumlah'];
					return $jumlah;
				}
				?>        
			</div>
		</div>
	</div>
	<!-- End ImageReady Slices -->
</body>
</html>