<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("guru");
if(!isset($_SESSION)){ session_start();}

unset($_SESSION['login']);
unset($_SESSION['nama']);
unset($_SESSION['tingkat']);
unset($_SESSION['departemen']);
unset($_SESSION['theme']);
unset($_SESSION['errtype']);
unset($_SESSION['errfile']);
unset($_SESSION['errno']);
unset($_SESSION['errmsg']);
unset($_SESSION['issend']);
?>
<script language="javascript">
top.window.location='../../man/index.php?link=infoguru';
</script>