<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once("../include/sessionchecker.php");
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');


$info = $_REQUEST['info'];
OpenDb();
$sql="SELECT i.deskripsi, j.departemen, t.tahunajaran, t.tglmulai, t.tglakhir FROM infojadwal i, jadwal j, tahunajaran t WHERE i.replid='$info' AND j.infojadwal = i.replid AND t.replid = i.idtahunajaran";

$result=QueryDb($sql);
CloseDb();
$row=@mysql_fetch_array($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Rekap Jadwal Guru]</title>
</head>

<body>

<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
<?php include("../library/headercetak.php") ?>
	
<center>
  <font size="4"><strong>REKAP JADWAL GURU</strong></font><br />
 </center><br /><br />

<br />
<table>
<tr>
	<td width="35%"><strong>Departemen</strong></td>
    <td><strong>: <?php echo$row['departemen']?></strong></td>
</tr>
<tr>
	<td><strong>Tahun Ajaran</strong></td>
    <td><strong>: <?php echo$row['tahunajaran']?></strong></td>
</tr>
<tr>
	<td width="10%"><strong>Info Jadwal</strong></td>
    <td><strong>: <?php echo$row['deskripsi']?></strong></td>
</tr>
<tr>
	<td><strong>Periode</strong></td>
    <td><strong>: <?php echo TglTextLong($row['tglmulai']).' s/d '. TglTextLong($row['tglakhir'])?></strong></td>
</tr>
</table>
<br />
<table border="1" width="100%" id="table" class="tab" align="center" cellpadding="2" style="border-collapse:collapse" cellspacing="2" bordercolor="#000000">
<tr height="15">
    <td width="4%" rowspan="2 "class="header" align="center">No</td>
    <td width="10%"rowspan="2" class="header" align="center">NIP</td>
    <td width="*"rowspan="2" class="header" align="center">Nama</td>
    <td colspan="6" width="60%" class="header" align="center">Jumlah</td>
</tr>
<tr height="15">
    <td width="8%" class="header" align="center">Mengajar</td>
    <td width="8%" class="header" align="center">Asistensi</td>
    <td width="8%" class="header" align="center">Tambahan</td>
    <td width="8%" class="header" align="center">Jam</td>
    <td width="8%" class="header" align="center">Kelas</td>
    <td width="8%" class="header" align="center">Hari</td>
</tr>
<?php 	OpenDb();
    
    $sql = "SELECT p.nip, p.nama, SUM(IF(j.status = 0, 1, 0)), SUM(IF(j.status = 1, 1, 0)), SUM(IF(j.status = 2, 1, 0)), SUM(j.njam), COUNT(DISTINCT(j.idkelas)), COUNT(DISTINCT(j.hari)) FROM jadwal j, $g_db_pegawai.pegawai p WHERE j.nipguru = p.nip AND j.infojadwal = '$info' GROUP BY j.nipguru ORDER BY p.nama";
    
    $result = QueryDb($sql);
    $cnt = 0;
    while ($row = mysql_fetch_row($result)) {
?>
<tr>
    <td align="center"><?php echo++$cnt?></td>
    <td align="center"><?php echo$row[0]?></td>        
    <td><?php echo$row[1]?></td>        
    <td align="center"><?php echo$row[2]?></td>        
    <td align="center"><?php echo$row[3]?></td>        
    <td align="center"><?php echo$row[4]?></td>        
    <td align="center"><?php echo$row[5]?></td>        
    <td align="center"><?php echo$row[6]?></td> 
    <td align="center"><?php echo$row[7]?></td>        
</tr>
<?php	} 
CloseDb() ?>	
<!-- END TABLE CONTENT -->
</table>

</td></tr>
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>