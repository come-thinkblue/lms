<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link rel="Stylesheet" href="../style/style.css">
    <title></title>
    <script language="javascript">
        function upload() {
            parent.content.location.href = "uploader.php";
        }
        
        function browse() {
            parent.content.location.href = "uploaderbrowser.php";
        }
    </script>
</head>
<body style="background-color: #F5F5F5" topmargin="0" leftmargin="0">
<table border="0" width="100%">
<tr>
    <td width="67%" align="left">
    <input type="button" value="Upload Gambar" onclick="upload()" class="but" />&nbsp;&nbsp;
    <input type="button" value="Lihat Gambar" onclick="browse()" class="but" />&nbsp;&nbsp;
    <input type="button" value="Tutup" onclick="parent.close();" class="but" />&nbsp;&nbsp;
    </td>
    <td width="33%" align="right">
    <font size="5" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="5" color="Gray">Upload Gambar</font><br />
    </td>
</tr>
</table>    
</body>
</html>