<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS MAN Kota Blitar
 * 
 * Copyright (C) 2013 MAN Kota Blitar
 * 
 **[N]**/ ?>
<?php
class Thumbnail
{
	var $image;
	var $path;
	var $resizeImage;
	var $type;
	var $filename;
	function Thumbnail()
		{
		        
		}
	
	function setImage($image)
		{
		
				$this->path=$image;
				$this->type="jpeg";
				if(!@$this->image=imagecreatefromjpeg($image))
					{
						$this->type="png";
						if(!@$this->image=imagecreatefrompng($image))
							{
								$this->type="gif";
								if(!@$this->image=imagecreatefromgif($image))
									{
										echo "unknow file type";
									}
	
							}
					}
				
		}
	
	function getFilename()
		{
			return $this->filename;
		}
		
	function setFilename($file)
		{
			$this->filename=$file;	
		}
	function getType()
		{
			return $this->type;
		}	
	function setType($type)
		{
			$this->type=$type;
		}
	

	function resize($width,$height)
		{
			@list($imageWidth,$imageHeight)=getimagesize($this->path);
			if(($width==""||$width==0)&&($height==""||$height==0))
				{
					$width=$imageWidth;
					$height=$imageHeight;		
				}
			@$this->resizeImage=imagecreatetruecolor($width,$height);
			@imagecopyresized ($this->resizeImage,$this->image,0,0,0,0,$width,$height,$imageWidth,$imageHeight);
			
		}

	
	function display()
		{
			$this->filename = tempnam("/tmp", "FOO"); 
			switch($this->type)
				{
					
					case "png":
						header("Content-type: image/png");	
						@imagepng($this->resizeImage,$this->filename);
						
						break;
					case "jpeg":
						header("Content-type: image/jpeg");	
						@imagejpeg($this->resizeImage,$this->filename);
						break;
					case "gif":
						header("Content-type: image/gif");	
						@imagegif($this->resizeImage,$this->filename);
						break;					
				}
			
		}
}

?>