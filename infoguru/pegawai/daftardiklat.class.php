<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
class DaftarDiklat
{    
    public $nip;
    public $nama;
    
    public function __construct()
    {
        $this->nip = $_REQUEST['nip'];

        $sql = "SELECT TRIM(CONCAT(IFNULL(gelarawal,''), ' ' , nama, ' ', IFNULL(gelarakhir,''))) AS nama FROM pegawai WHERE nip='$this->nip'";
        $result = QueryDb($sql);
        $row = mysql_fetch_row($result);
        $this->nama = $row[0];
        
        $id = $_REQUEST['id'];
        $op = $_REQUEST['op'];
        if ($op == "mnrmd2re2dj2mx2x2x3d2s33") 
        {
            $success = true;            
            BeginTrans();
            
            $sql = "DELETE FROM pegdiklat WHERE replid=$id";
            QueryDbTrans($sql, $success);
            
            if ($success)
            {
                $sql = "UPDATE peglastdata SET idpegdiklat=NULL WHERE idpegdiklat=$id AND nip='$this->nip'";
                QueryDbTrans($sql, $success);
            }
            
            if ($success)
                CommitTrans();
            else
                RollbackTrans();
        }
        elseif ($op == "cn0948cm2478923c98237n23") 
        {
            $success = true;            
            BeginTrans();
            
            $sql = "UPDATE pegdiklat SET terakhir=0 WHERE nip='$this->nip'";
            QueryDbTrans($sql, $success);
            
            if ($success)
            {
                $sql = "UPDATE pegdiklat SET terakhir=1 WHERE replid=$id";
                QueryDbTrans($sql, $success);
            }
            
            if ($success)
            {
                $sql = "UPDATE peglastdata SET idpegdiklat=$id WHERE nip='$this->nip'";
                QueryDbTrans($sql, $success);
            }
            
            if ($success)
                CommitTrans();
            else
                RollbackTrans();
        }
    }
}
?>