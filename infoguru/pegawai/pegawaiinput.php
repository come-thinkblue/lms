<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/sessionchecker.php");
require_once("../include/config.php");
require_once("../include/db_functions.php");
require_once("../include/common.php");
require_once('../include/theme.php');
require_once("../include/sessioninfo.php");
require_once("pegawaiinput.class.php");

OpenDb();
$P = new PegawaiInput();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar Kepegawaian</title>
<link rel="stylesheet" href="../style/style<?php echo GetThemeDir2()?>.css" />
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/ajax.js"></script>
<script language="javascript" src="pegawaiinput.js"></script>
<style type="text/css">
.style1 { color: #666666; font-style: italic; }
</style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#ffffff">
<div id="waitBox" style="position:absolute; visibility:hidden;">
<img src="../images/movewait.gif" border="0" />Silahkan&nbsp;tunggu...
</div>
<form method="post" enctype="multipart/form-data" name="main" onSubmit="return validate()">
<table border="0" cellpadding="5" cellspacing="0" width="100%" id="table56">
<tr>
	<td width="100%" align="right" style="border-bottom:thin dashed #CCCCCC; border-top:none; border-left:none; border-right:none;">
        <font style="background-color:#FFCC33; font-size:24px">&nbsp;&nbsp;</font>
        <font class="subtitle">Input Pegawai</font><br />
        <a href="pegawai.php">Kepegawaian</a> &gt; Input Pegawai
    </td>
</tr>
<tr><td>

<table border="0" cellpadding="5" cellspacing="0" width="100%">
<?php if (strlen($P->ERRMSG) > 0) { ?>
<tr>
	<td colspan="2" align="center">
        <table border="1" style="border-style:dashed; border-color:#CC0000; background-color:#FFFFCC" width="300">
        <tr><td align="center"><?php echo$P->ERRMSG?></td></tr>
        </table>    </td>
</tr>
<?php } ?>
<tr>
	<td align="right" valign="top"><strong>Status </strong>:</td>
    <td width="*" align="left" valign="top">
    <input type="radio" name="rbPNS" id="rbPNS" value="1"<?php echo StringIsChecked("PNS", $P->pns)?>/>&nbsp;PNS&nbsp;&nbsp;
    <input type="radio" name="rbPNS" id="rbPNS" value="2" <?php echo StringIsChecked("NON PNS", $P->pns)?> />&nbsp;Non PNS&nbsp;&nbsp;
    
    </td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Bagian </strong>:</td>
    <td width="*" align="left" valign="top">
<?php	 $sql = "SELECT bagian FROM $g_db_pegawai.bagianpegawai ORDER BY urutan";
	 $res = QueryDb($sql);
	 $checked = "checked='checked'";
	 while ($row = @mysql_fetch_row($res))
	 {
		 echo "<input type='radio' $checked name='rbBagian' id='rbBagian' value='$row[0]' " .  StringIsChecked($row[0], $DP->bagian) . " />&nbsp;$row[0]&nbsp;";
		 $checked = "";
	 } ?>
    </td>
</tr>

<tr>
	<td width="140" align="right" valign="top"><strong>Nama </strong>:</td>
    <td width="*" align="left" valign="top">
    
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
  
        <td width="260"><input type="text" name="txNama" id="txNama" size="40" maxlength="255" value="<?php echo$P->nama?>" onKeyPress="return focusNext('txGelarAkhir', event)"/>
    </tr>
  
    </table>    </td>
</tr>




<tr>
	<td align="right" valign="top">Panggilan:</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txPanggilan" id="txPanggilan" size="20" maxlength="30" value="<?php echo$P->panggilan?>" onKeyPress="return focusNext('txNIP', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top"><strong>NIP </strong>:</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txNIP" id="txNIP" size="20" maxlength="30" value="<?php echo$P->nip?>" onKeyPress="return focusNext('txNUPTK', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top">NUPTK :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txNUPTK" id="txNUPTK" size="20" maxlength="30" value="<?php echo$P->nuptk?>" onKeyPress="return focusNext('txNRG', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top">NRG :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txNRP" id="txNRP" size="20" maxlength="30" value="<?php echo$P->nrg?>" onKeyPress="return focusNext('txTmpLahir', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top">NIK :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txNIK" id="txNIK" size="20" maxlength="30" value="<?php echo$P->nik?>" onKeyPress="return focusNext('txTmpLahir', event)"/>
	</td>
</tr>

<tr>
	<td align="right" valign="top">Ibu Kandung :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txIbu" id="txIbu" size="20" maxlength="30" value="<?php echo$P->ibu?>" onKeyPress="return focusNext('txTmpLahir', event)"/>
	</td>
</tr>


<tr>
	<td align="right" valign="top">Jenjang Pendidikan :</td>
   <td width="*" align="left" valign="top">
	<span id="suku_info">
    <select name="cbJenjang" id="cbJenjang" onKeyPress="return focusNext('cbJenjang', event)">
<?php		$sql = "SELECT * FROM jenjang";
		$res = QueryDb($sql);
	    while ($row = @mysql_fetch_row($res))
		  echo "<option value='$row[0]' " . StringIsSelected($row[0], $DP->jenjang) . " >$row[1]</option>";
?>
    </select>&nbsp;

    </span>
	</td>
</tr>

<tr>
	<td align="right" valign="top">Golongan :</td>
   <td width="*" align="left" valign="top">
	<span id="suku_info">
    <select name="cbGol" id="cbGol" onKeyPress="return focusNext('cbGol', event)">
<?php		$sql = "SELECT * FROM golongan";
		$res = QueryDb($sql);
	    while ($row = @mysql_fetch_row($res))
		  echo "<option value='$row[0]' " . StringIsSelected($row[0], $DP->jenjang) . " >$row[1]</option>";
?>
    </select>&nbsp;

    </span>
	</td>
</tr>

<tr>
	<td align="right" valign="top"><strong>Tempat, Tgl Lahir </strong>:</td>
    <td width="*" align="left" valign="top">
    <input type="text" name="txTmpLahir" id="txTmpLahir" size="20" maxlength="25" value="<?php echo$P->tmplahir?>" onKeyPress="return focusNext('cbTglLahir', event)"/>, 
    <select id="cbTglLahir" name="cbTglLahir" onKeyPress="return focusNext('cbBlnLahir', event)">
<?php	for ($i = 1; $i <= 31; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->tgllahir)?>><?php echo$i?></option>	
<?php	} ?>    
	</select>
    <select id="cbBlnLahir" name="cbBlnLahir" onKeyPress="return focusNext('txThnLahir', event)">
<?php	for ($i = 1; $i <= 12; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->blnlahir)?>><?php echo NamaBulan($i)?></option>	
<?php	} ?>    
	</select>
    <input type="text" name="txThnLahir" id="txThnLahir" size="4" maxlength="4" value="<?php echo$P->thnlahir?>" onKeyPress="return focusNext('txAlamat', event)"/>    </td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Agama :</strong>    </td>
    <td width="*" align="left" valign="top">
	<span id="agama_info">
    <select name="cbAgama" id="cbAgama" onKeyPress="return focusNext('cbSuku', event)">
<?php	$sql = "SELECT agama FROM $g_db_akademik.agama ORDER BY urutan";
	$result = QueryDb($sql);
	while ($row = mysql_fetch_row($result)) { ?>    
    	<option value="<?php echo$row[0]?>" <?php echo StringIsSelected($row[0], $DP->agama)?> ><?php echo$row[0]?></option>
<?php	} ?>    
    </select>&nbsp;
<?php 	if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
    <img src="../images/ico/tambah.png" border="0" onClick="tambah_agama();"">
<?php 	} ?>
	</span>
	</td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Suku :</strong>    </td>
    <td width="*" align="left" valign="top">
	<span id="suku_info">
    <select name="cbSuku" id="cbSuku" onKeyPress="return focusNext('cbNikah', event)">
<?php		$sql = "SELECT suku FROM $g_db_akademik.suku";
		$res = QueryDb($sql);
	    while ($row = @mysql_fetch_row($res))
		  echo "<option value='$row[0]' " . StringIsSelected($row[0], $DP->suku) . " >$row[0]</option>";
?>
    </select>&nbsp;
<?php 	if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
		<img src="../images/ico/tambah.png" onClick="tambah_suku();" />
<?php 	} ?>
    </span>
	</td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Status Menikah :</strong>    </td>
    <td width="*" align="left" valign="top">
    <select name="cbNikah" id="cbNikah" onKeyPress="return focusNext('cbKelamin', event)">
		 <option value="menikah" <?php echo StringIsSelected("menikah", $DP->nikah)?> >Menikah</option>
		 <option value="belum" <?php echo StringIsSelected("belum", $DP->nikah)?> >Belum</option>
		 <option value="tak_ada" <?php echo StringIsSelected("tak_ada", $DP->nikah)?> >Tidak Ada Data</option>
    </select>&nbsp;    </td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Jenis Kelamin :</strong>    </td>
		<td width="*" align="left" valign="top">
		<select name="cbKelamin" id="cbKelamin" onKeyPress="return focusNext('txAlamat', event)">
			<option value="l" <?php echo StringIsSelected("l", $DP->kelamin)?>>Laki-laki</option>
			<option value="p" <?php echo StringIsSelected("p", $DP->kelamin)?>>Perempuan</option>
	   </select>&nbsp;
	</td>
</tr>
<tr>
	<td align="right" valign="top">Alamat :</td>
    <td width="*" align="left" valign="top"><input type="text" name="txAlamat" id="txAlamat" size="70" onKeyPress="return focusNext('txHP', event)" maxlength="255" value="<?php echo$P->alamat?>"/></td>
</tr>

<tr>
	<td align="right" valign="top">Kode Pos :</td>
    <td width="*" align="left" valign="top"><input type="text" name="txKodepos" id="txKodepos" size="40" onKeyPress="return focusNext('txHP', event)" maxlength="255" value="<?php echo$P->kodepos?>"/></td>
</tr>

<tr>
	<td align="right" valign="top"><strong>Status :</strong></td>
    <td width="*" align="left" valign="top">
<?php	$aktifchecked = "";
	if ($DP->aktif == 1) 
		$aktifchecked = "checked"; 
		
	$nonchecked = "";
	if ($DP->aktif == 0) 
		$nonchecked = "checked"; ?>
    <input type="radio" name="rbAktif" value="1" id="rbAktif" <?php echo$aktifchecked?> />&nbsp;Aktif
    <input type="radio" name="rbAktif" value="0" id="rbAktif" <?php echo$nonchecked?>  >&nbsp;Non Aktif&nbsp;
    <input type="text" name="txKetNonAktif" id="txKetNonAktif" value="<?php echo$P->ketnonaktif?>" size="25" maxlength="255"  onKeyPress="return focusNext('cbTglMulai', event)"/>
</td>
</tr>
<tr>
	<td align="right" valign="top"><strong>TMT SK CPNS :</strong></td>
    <td width="*" align="left" valign="top">
    <select id="cbTglMulai" name="cbTglMulai" onKeyPress="return focusNext('cbBlnMulai', event)">
<?php	for ($i = 1; $i <= 31; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->tglmulai)?>><?php echo$i?></option>	
<?php	} ?>    
	</select>
    <select id="cbBlnMulai" name="cbBlnMulai" onKeyPress="return focusNext('txThnMulai', event)">
<?php	$M = date("m");
	for ($i = 1; $i <= 12; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->blnmulai)?>><?php echo NamaBulan($i)?></option>	
<?php	} ?>    
	</select>
    <input type="text" name="txThnMulai" id="txThnMulai" onKeyPress="return focusNext('cbTglPensiun', event)" size="4" maxlength="4" value="<?php echo$P->thnmulai?>"/>    </td>
</tr>

<tr>
	<td align="right" valign="top"><strong>TMT SK Awal :</strong></td>
    <td width="*" align="left" valign="top">
    <select id="cbTglawal" name="cbTglawal" onKeyPress="return focusNext('cbBlnAwal', event)">
<?php	for ($i = 1; $i <= 31; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->tglawal)?>><?php echo$i?></option>	
<?php	} ?>    
	</select>
    <select id="cbBlnAwal" name="cbBlnAwal" onKeyPress="return focusNext('txThnAwal', event)">
<?php	$M = date("m");
	for ($i = 1; $i <= 12; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->blnawal)?>><?php echo NamaBulan($i)?></option>	
<?php	} ?>    
	</select>
    <input type="text" name="txThnAwal" id="txThnAwal" onKeyPress="return focusNext('cbTglPensiun', event)" size="4" maxlength="4" value="<?php echo$P->thnawal?>"/>    </td>
</tr>


<tr>
	<td align="right" valign="top"><strong>TMT SK Akhir :</strong></td>
    <td width="*" align="left" valign="top">
    <select id="cbTglAkhir" name="cbTglAkhir" onKeyPress="return focusNext('cbBlnAkhir', event)">
<?php	for ($i = 1; $i <= 31; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->tglakhir)?>><?php echo$i?></option>	
<?php	} ?>    
	</select>
    <select id="cbBlnAkhir" name="cbBlnAkhir" onKeyPress="return focusNext('txThnAkhir', event)">
<?php	$M = date("m");
	for ($i = 1; $i <= 12; $i++) { ?>    
    	<option value="<?php echo$i?>" <?php echo IntIsSelected($i, $DP->blnakhir)?>><?php echo NamaBulan($i)?></option>	
<?php	} ?>    
	</select>
    <input type="text" name="txThnAkhir" id="txThnAkhir" onKeyPress="return focusNext('cbTglPensiun', event)" size="4" maxlength="4" value="<?php echo$P->thnakhir?>"/>    </td>
</tr>

<tr>
	<td align="right" valign="top">Mata Pelajaran :</td>
   <td width="*" align="left" valign="top">
	<span id="mapel_info">
    <select name="cbMapel" id="cbMapel" onKeyPress="return focusNext('cbMapel', event)">
<?php		$sql = "SELECT * FROM mapel";
		$res = QueryDb($sql);
	    while ($row = @mysql_fetch_row($res))
		  echo "<option value='$row[0]' " . StringIsSelected($row[0], $P->mapelampu) . " >$row[1]</option>";
?>
    </select>&nbsp;

    </span>
	</td>
</tr>

<tr>
	<td align="right" valign="top">Total Jam :</td>
    <td width="*" align="left" valign="top"><input type="text" name="txJam" id="txJam" size="15" maxlength="15" value="<?php echo$P->totaljam?>" onKeyPress="return focusNext('txHP', event)"/>
     </td>
</tr>

<tr>
	<td align="right" valign="top">Gaji :</td>
    <td width="*" align="left" valign="top">Rp  <input type="text" name="txGaji" id="txGaji" size="15" maxlength="15" value="<?php echo$P->gaji?>" onKeyPress="return focusNext('txHP', event)"/>
     </td>
</tr>



<tr>
	<td align="right" valign="top">HP :</td>
    <td width="*" align="left" valign="top"><input type="text" name="txHP" id="txHP" size="15" maxlength="15" value="<?php echo$P->hp?>" onKeyPress="return focusNext('txTelpon', event)"/>
    Telpon: <input type="text" name="txTelpon" id="txTelpon" size="15" maxlength="15" value="<?php echo$P->telpon?>" onKeyPress="return focusNext('txEmail', event)"/>    </td>
</tr>
<tr>
	<td align="right" valign="top">Email :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txEmail" id="txEmail" size="45" maxlength="255" value="<?php echo$P->email?>" onKeyPress="return focusNext('txFacebook', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top">Facebook :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txFacebook" id="txFacebook" size="45" maxlength="255" value="<?php echo$P->facebook?>" onKeyPress="return focusNext('txTwitter', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top">Twitter :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txTwitter" id="txTwitter" size="45" maxlength="255" value="<?php echo$P->twitter?>" onKeyPress="return focusNext('txWebsite', event)"/>
	</td>
</tr>
<tr>
	<td align="right" valign="top">Website :</td>
    <td width="*" align="left" valign="top">
		<input type="text" name="txWebsite" id="txWebsite" size="45" maxlength="255" value="<?php echo$P->website?>" onKeyPress="return focusNext('foto', event)"/>
	</td>
</tr>


<tr>
	<td align="right" valign="top">Keterangan :</td>
    <td width="*" align="left" valign="top">
    <textarea id="txKeterangan" name="txKeterangan" rows="3" cols="60" onKeyPress="return focusNext('txAlasan', event)"><?php echo$P->keterangan?></textarea>    </td>
</tr>
<tr>
	<td align="center" valign="top" colspan="2" bgcolor="#CCCCCC">
    <input type="submit" value="Simpan" name="btSubmit" id="btSubmit" class="but" />    </td>
</tr>
</table>
<?php
CloseDb();
?>    
</td></tr>
<tr><td align="center">
<img src="../images/border.jpg">
<br><br>
</td></tr>
</table>
</form>

</body>
</html>