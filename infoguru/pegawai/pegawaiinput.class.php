<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once('../include/imageresizer.php');
require_once('../include/fileinfo.php');

class PegawaiInput
{
    public $ERRMSG;
    
    public $nama = "";
    public $panggilan = "";
    public $gelarawal = "";
    public $gelarakhir = "";
    public $nip  = "";
    public $nuptk  = "";
    public $nrp  = "";
    public $tmplahir = "";
    public $tgllahir = 1;
    public $blnlahir = 1;
    public $thnlahir = "";
    public $agama = "";
    public $suku = "";
    public $nikah = "";
    public $kelamin = "";
    public $alamat = "";
    public $hp = "";
    public $telpon = "";
    public $email = "";
    public $facebook = "";
    public $twitter = "";
    public $website = "";
    public $tglmulai = 1;
    public $blnmulai = 1;
    public $thnmulai = "";
    public $keterangan = "";
    public $pns = 1;
    public $bagian = "Akademik";
	
	public $tglawal= 1;
	public $blnawal= 1;
   public $thnawal= "";		
		public $tglakhir= 1;
        public $blnakhir= 1;
        public $thnakhir= "";		 	  
		public $nik= "";    		
		public $ibu= ""; 
		public $jenjang= ""; 
		public $gol= "";		 
		public $gaji= "";		 
		public $mapelampu= "";		 
		public $totaljam= "";		 
		public $kodepos= "";
		
		
    
    public function __construct()
    {
        $this->InitVariables();
                
        if (isset($_REQUEST['btSubmit']))
            $this->SaveData();
    }
    
    private function InitVariables()
    {
        $this->nama = CQ($_REQUEST['txNama']);
        $this->panggilan = CQ($_REQUEST['txPanggilan']);
        $this->gelarawal = $_REQUEST['txGelarAwal'];
        $this->gelarakhir = $_REQUEST['txGelarAkhir'];
        $this->nip  = $_REQUEST['txNIP'];
        $this->nuptk  = $_REQUEST['txNUPTK'];
        $this->nrp  = $_REQUEST['txNRP'];
        $this->tmplahir = $_REQUEST['txTmpLahir'];
        $this->tgllahir = $_REQUEST['cbTglLahir'];
        $this->blnlahir = $_REQUEST['cbBlnLahir'];
        $this->thnlahir = $_REQUEST['txThnLahir'];
        $this->agama = $_REQUEST['cbAgama'];
        $this->suku = $_REQUEST['cbSuku'];
        $this->nikah = $_REQUEST['cbNikah'];
        $this->kelamin = $_REQUEST['cbKelamin'];
        $this->alamat = CQ($_REQUEST['txAlamat']);
        $this->hp = $_REQUEST['txHP'];
        $this->telpon = $_REQUEST['txTelpon'];
        $this->email = $_REQUEST['txEmail'];
        $this->facebook = $_REQUEST['txFacebook'];
        $this->twitter = $_REQUEST['txTwitter'];
        $this->website = $_REQUEST['txWebsite'];
        $this->foto = $_FILES['foto'];
        $this->tglmulai = $_REQUEST['cbTglMulai'];
        $this->blnmulai = $_REQUEST['cbBlnMulai'];
        $this->thnmulai = $_REQUEST['txThnMulai'];
        $this->keterangan = CQ($_REQUEST['txKeterangan']);
        $this->pns = $_REQUEST['rbPNS'];
        $this->bagian = $_REQUEST['rbBagian'];
		
		$this->tglawal =$_REQUEST['cbTglawal'];
        $this->blnawal =$_REQUEST['cbBlnAwal'];
        $this->thnawal = $_REQUEST['txThnAwal'];
		
		$this->tglakhir = $_REQUEST['cbTglAkhir'];
        $this->blnakhir = $_REQUEST['cbBlnAkhir'];
        $this->thnakhir = $_REQUEST['txThnAkhir'];
		
	  
		$this->nik = $_REQUEST['txNIK'];     		
		$this->ibu = $_REQUEST['txIbu'];  
		$this->jenjang = $_REQUEST['cbJenjang']; 
		$this->gol = $_REQUEST['cbGol']; 		 
	 
		$this->gaji = $_REQUEST['txGaji']; 		 
		$this->mapelampu = $_REQUEST['cbMapel']; 		 
		$this->totaljam = $_REQUEST['txJam']; 		 
		$this->kodepos = $_REQUEST['txKodepos']; 	
		
		
        
        if (!isset($_REQUEST['rbPNS']))
            $this->pns = "PNS";
    }
    
    private function SaveData()
    {
    	$sql = "SELECT replid FROM $g_db_pegawai.pegawai WHERE nip='$this->nip'";
    	$result = QueryDb($sql);
    	if (mysql_num_rows($result) > 0)
        {
    		$this->ERRMSG = "Telah ada pegawai dengan NIP $this->nip";
            return;        
    	}
        
        $bday = "$this->thnlahir-$this->blnlahir-$this->tgllahir";
        $sday = "$this->thnmulai-$this->blnmulai-$this->tglmulai";
		 $awal = "$this->thnawal-$this->blnawal-$this->tglawal";
		$akhir = "$this->thnakhir-$this->blnakhir-$this->tglakhir";
		
        if (strlen($this->foto['tmp_name']) != 0)
        {
            $output = "../temp/img.tmp";
            ResizeImage($this->foto, 320, 240, 75, $output);
            
            $foto_data = addslashes(fread(fopen($output,"r"), filesize($output)));
            $foto = ", foto='$foto_data'";
            
            unlink($output);
        }
        else
        {
            $foto = "";
        }
        
        $success = true;
        BeginTrans();
        
        $pin = random(5);    
        $sql = "INSERT INTO $g_db_pegawai.pegawai SET 
        			nip='$this->nip',
                    nuptk='$this->nuptk',
                    nrg='$this->nrp',
					nama='$this->nama',
					panggilan='$this->panggilan',
					tmplahir='$this->tmplahir',
					tgllahir='$bday',
					agama='$this->agama',
					suku='$this->suku',
					alamat='$this->alamat',
					telpon='$this->telpon',
					handphone='$this->hp',
					email='$this->email',
                    facebook='$this->facebook',
                    twitter='$this->twitter',
                    website='$this->website',
					bagian='$this->bagian',
					pinpegawai='$pin',
					nikah='$this->nikah',
					keterangan='$this->keterangan',
					kelamin='$this->kelamin',
                    tmtskcpns='$sday',
					tmtskawal='$awal', tmtskakhir='$akhir',gaji= '$this->gaji', nik='$this->nik', ibu='$this->ibu', jenjang='$this->jenjang', golongan='$this->gol', mapelampu='$this->mapelampu', totaljam='$this->totaljam', kodepos='$this->kodepos',
					gelarawal='$this->gelarawal',
					gelarakhir='$this->gelarakhir',
					statuspeg='$this->pns'
                    $foto";
        QueryDbTrans($sql, $success);
        
        if ($success)
        {
            $sql = "INSERT INTO $g_db_pegawai.peglastdata SET nip='$this->nip'";
            QueryDbTrans($sql, $success);
		}
        
        if ($success)
        {
			CommitTrans(); 
			CloseDb(); ?>
            <script language="javascript">
                alert("Data telah tersimpan")
				document.location.href = "pegawaiinput.php";
	        </script>
<?php			exit();	          
		}
        else
        {
			$this->ERRMSG = "Gagal menyimpan data!";
			RollbackTrans();
            CloseDb();
		}
    }
}
?>