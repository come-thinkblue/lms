<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once('../include/imageresizer.php');
require_once('../include/fileinfo.php');

class DaftarPribadi
{
    public $ERRMSG;    
    public $nip;
    public $pegawai;  // nama pegawai
    public $replid;   // replid pegawai	
	 public $bagian;    
	 public $nuptk;
	 public $nrp;
    public $nama;
	 public $panggilan;
    public $gelarawal;
    public $gelarakhir;
    public $newnip;
    public $tmplahir;
    public $tgllahir;
    public $blnlahir;
    public $thnlahir;
    public $agama;
	 public $suku;
    public $nikah;
    public $kelamin;
    public $alamat;
    public $hp;
    public $telpon;
    public $email;
	 public $facebook;
	 public $twitter;
	 public $website;
    public $foto;
    public $status;
    public $tglmulai;
    public $blnmulai;
    public $thnmulai;
    public $keterangan;
    public $alasan;
    public $aktif;
    public $ketnonaktif;
    public $pns;
	public $tglawal;
	public $blnawal;
   public $thnawal;		
		public $tglakhir;
        public $blnakhir;
        public $thnakhir;		 	  
		public $nik;    		
		public $ibu; 
		public $jenjang; 
		public $gol;		 
		public $gaji;		 
		public $mapelampu;		 
		public $totaljam;		 
		public $kodepos;
    
    public function __construct()
    {
        $this->nip = $_REQUEST["nip"];
        $this->GetPegawaiInfo();
		
		  $op = $_REQUEST['op'];
		  if ($op == "cn984214713289476137246xb78461237841")
				$this->DeleteData();
		  elseif (isset($_REQUEST['btSubmit']))
            $this->SaveData();
        else
            $this->LoadData();
    }
	
	private function DeleteData()
	{
		$success = true;
		BeginTrans();
	
		if ($success)
		{
			$sql = "DELETE FROM $g_db_pegawai.pegawai WHERE nip='$this->nip'";
			QueryDbTrans($sql, $success);
		}
		
		if ($success)
		{
			CommitTrans();
			CloseDb(); ?>
			<script language="javascript">
				parent.daftarhasil.Refresh();
				document.location.href = "blank.php";
			</script>
<?php			exit();	
		}
		else
		{
			RollbackTrans();
			CloseDb(); ?>
			<script language="javascript">
				parent.daftarhasil.Refresh();
				document.location.href = "blank.php?message='Tidak dapat menghapus data pegawai!'";
			</script>
<?php			exit();	
		}
	}
    
    private function GetPegawaiInfo()
    {
        $sql = "SELECT TRIM(CONCAT(IFNULL(gelarawal,''), ' ' , nama, ' ', IFNULL(gelarakhir,''))) AS nama, replid
                  FROM pegawai p
                 WHERE p.nip='$this->nip'";
        $result = QueryDb($sql);
        $row = mysql_fetch_row($result);
        $this->pegawai = $row[0];
        $this->replid = $row[1];
    }
    
    private function SaveData()
    {
		  $this->bagian = $_REQUEST['rbBagian'];
		  $this->nuptk = $_REQUEST['txNUPTK'];
		  $this->nrp = $_REQUEST['txNRP'];
        $this->nama = CQ($_REQUEST['txNama']);
		  $this->panggilan = CQ($_REQUEST['txPanggilan']);  
        $this->gelarawal = $_REQUEST['txGelarAwal'];
        $this->gelarakhir = $_REQUEST['txGelarAkhir'];
        $this->newnip  = $_REQUEST['txNIP'];
        $this->tmplahir = $_REQUEST['txTmpLahir'];
        $this->tgllahir = $_REQUEST['cbTglLahir'];
        $this->blnlahir = $_REQUEST['cbBlnLahir'];
        $this->thnlahir = $_REQUEST['txThnLahir'];
        $this->agama = $_REQUEST['cbAgama'];
		  $this->suku = $_REQUEST['cbSuku'];
        $this->nikah = $_REQUEST['cbNikah'];
        $this->kelamin = strtolower($_REQUEST['cbKelamin']);
        $this->alamat = CQ($_REQUEST['txAlamat']);
        $this->hp = $_REQUEST['txHP'];
        $this->telpon = $_REQUEST['txTelpon'];
        $this->email = $_REQUEST['txEmail'];
		  $this->facebook = $_REQUEST['txFacebook'];
		  $this->twitter = $_REQUEST['txTwitter'];
		  $this->website = $_REQUEST['txWebsite'];
        $this->foto = $_FILES['foto'];
        $this->status = $_REQUEST['cbStatus'];
        $this->tglmulai = $_REQUEST['cbTglMulai'];
        $this->blnmulai = $_REQUEST['cbBlnMulai'];
        $this->thnmulai = $_REQUEST['txThnMulai'];
        $this->keterangan = CQ($_REQUEST['txKeterangan']);
        $this->alasan = CQ($_REQUEST['txAlasan']);
        $this->aktif = $_REQUEST['rbAktif'];
        $this->ketnonaktif = CQ($_REQUEST['txKetNonAktif']);
        $this->pns = $_REQUEST['rbPNS'];	
				
		$this->tglawal = $_REQUEST['cbTglawal'];
        $this->blnawal = $_REQUEST['cbBlnAwal'];
        $this->thnawal = $_REQUEST['txThnAwal'];
		
		$this->tglakhir = $_REQUEST['cbTglAkhir'];
        $this->blnakhir = $_REQUEST['cbBlnAkhir'];
        $this->thnakhir = $_REQUEST['txThnAkhir'];
		
	    	  
		$this->nik = $_REQUEST['txNIK'];     		
		$this->ibu = $_REQUEST['txIbu'];  
		$this->jenjang = $_REQUEST['cbJenjang']; 
		$this->gol = $_REQUEST['cbGol']; 		 
	 
		$this->gaji = $_REQUEST['txGaji']; 		 
		$this->mapelampu = $_REQUEST['cbMapel']; 		 
		$this->totaljam = $_REQUEST['txJam']; 		 
		$this->kodepos = $_REQUEST['txKodepos']; 		 
	
		
		
	
        $sql = "SELECT replid FROM $g_db_pegawai.pegawai WHERE nip = '$this->newnip' AND nip <> '$this->nip'";
		  $result = QueryDb($sql);
        if (mysql_num_rows($result) > 0)
        {
    		$this->ERRMSG = "Telah ada pegawai dengan NIP $nip";
        }
        else
        {
            $bday = "$this->thnlahir-$this->blnlahir-$this->tgllahir";
            $sday = "$this->thnmulai-$this->blnmulai-$this->tglmulai";
            $awal = "$this->thnawal-$this->blnawal-$this->tglawal";
			$akhir = "$this->thnakhir-$this->blnakhir-$this->tglakhir";
			
            
			$gantifoto = "";
			if (strlen($this->foto['tmp_name']) != 0)
			{
				$output = "../temp/img.tmp";
				ResizeImage($this->foto, 320, 240, 70, $output);
				
				$foto_data = addslashes(fread(fopen($output,"r"), filesize($output)));
				$gantifoto = ", foto='$foto_data'";
				
				unlink($output);
			}
            
			$success = true;
			BeginTrans();
            
			$sql = "UPDATE $g_db_pegawai.pegawai
					   SET nama='$this->nama', panggilan='$this->panggilan', gelarawal='$this->gelarawal', gelarakhir='$this->gelarakhir', nip='$this->newnip',
						   nuptk='$this->nuptk', nrg='$this->nrp', tmplahir='$this->tmplahir',
						   tgllahir='$bday', alamat='$this->alamat', handphone='$this->hp', telpon='$this->telpon', email='$this->email', tmtskcpns='$sday', tmtskawal='$awal', tmtskakhir='$akhir',gaji= '$this->gaji', nik='$this->nik', ibu='$this->ibu', jenjang='$this->jenjang', golongan='$this->gol', mapelampu='$this->mapelampu', totaljam='$this->totaljam', kodepos='$this->kodepos', keterangan='$this->keterangan', nikah='$this->nikah', agama='$this->agama', suku='$this->suku',  kelamin='$this->kelamin', aktif=$this->aktif, bagian='$this->bagian', facebook='$this->facebook', twitter='$this->twitter', website='$this->website', ketnonaktif='$this->ketnonaktif', doaudit = 1, statuspeg='$this->pns' $gantifoto WHERE nip='$this->nip'";
			QueryDbTrans($sql, $success);
						
			if ($success)
			{
				CommitTrans();
				CloseDb(); ?>
				<script language="javascript">
					var r = Math.floor((Math.random()*1000000)+1); 
	                document.location.href = "daftarpribadi.php?r="+r+"&nip=<?php echo$this->newnip?>&replid=<?php echo$this->replid?>";
					parent.daftarhasil.location.reload();
					parent.daftarmenu.UpdateNip("<?php echo$this->newnip?>");
	            </script>
<?php				exit();
			}
			else
			{
				$this->ERRMSG = "Gagal menyimpan data!";
				RollbackTrans();
			}
	    }
    }
    
    private function LoadData()
    {
        $sql = "SELECT * FROM pegawai WHERE nip='$this->nip'";
        $result = QueryDb($sql);
        $row = mysql_fetch_array($result);
		  $this->bagian = $row['bagian'];
		  $this->nuptk = $row['nuptk'];
		  $this->nrp = $row['nrp'];
        $this->nama = $row['nama']; //
		  $this->panggilan = $row['panggilan']; //
        $this->tmplahir = $row['tmplahir']; //
        $this->tgllahir = GetDatePart($row['tgllahir'], "d"); //
        $this->blnlahir = GetDatePart($row['tgllahir'], "m"); //
        $this->thnlahir = GetDatePart($row['tgllahir'], "y"); //
        $this->nikah = $row['nikah']; //
        $this->agama = $row['agama']; //
		  $this->suku = $row['suku']; //
        $this->kelamin = strtolower($row['kelamin']); //
        $this->alamat = $row['alamat']; //
        $this->hp = $row['handphone']; //
        $this->telpon = $row['telpon']; //
        $this->email = $row['email']; //
		  $this->facebook = $row['facebook']; //
		  $this->twitter = $row['twitter']; //
		  $this->website = $row['website']; //
        $this->foto = $row['foto']; //
        $this->aktif = $row['aktif']; //
        $this->keterangan = $row['keterangan']; //
		  $this->gelarawal = $row['gelarawal'];
        $this->gelarakhir = $row['gelarakhir'];
		  $this->tglmulai = GetDatePart($row['tmtskcpns'], "d");
        $this->blnmulai = GetDatePart($row['tmtskcpns'], "m");
        $this->thnmulai = GetDatePart($row['tmtskcpns'], "y");
		
		$this->tglawal = GetDatePart($row['tmtskawal'], "d");
        $this->blnawal = GetDatePart($row['tmtskawal'], "m");
        $this->thnawal = GetDatePart($row['tmtskawal'], "y");
		
		$this->tglakhir = GetDatePart($row['tmtskakhir'], "d");
        $this->blnakhir = GetDatePart($row['tmtskakhir'], "m");
        $this->thnakhir = GetDatePart($row['tmtskakhir'], "y");
		
		$this->pns = $row['statuspeg'];
		$this->ketnonaktif = $row['ketnonaktif'];     
		$this->ketnonaktif = $row['ketnonaktif'];     
		$this->nrg = $row['nrg'];  
		$this->nik = $row['nik'];     		
		$this->ibu = $row['ibu'];  
		$this->jenjang = $row['jenjang']; 
		$this->gol = $row['golongan']; 		 
			 
		$this->gaji = $row['gaji']; 		 
		$this->mapelampu = $row['mapelampu']; 		 
		$this->totaljam = $row['totaljam']; 		 
		$this->kodepos = $row['kodepos']; 		 
		$this->jarak = $row['jarak']; 		 
		$this->trans = $row['trans']; 
		$this->kodepos = $row['kodepos']; 		
    }
}
?>