<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/sessionchecker.php");
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once("../include/sessioninfo.php");

$stat = $_REQUEST['stat'];
$ref = $_REQUEST['ref'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="../style/style<?php echo GetThemeDir2()?>.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar Kepegawaian</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function DetailPegawai(nip) {
	var addr = "detailpegawai.php?nip="+nip;
	newWindow(addr, 'DetailPegawai','680','630','resizable=1,scrollbars=1,status=0,toolbar=0');
}
function cetak() {
	var addr = "statdetail_cetak.php?stat=<?php echo$stat?>&ref=<?php echo$ref?>";
	newWindow(addr, 'CetakDetail','790','650','resizable=1,scrollbars=1,status=0,toolbar=0');
}
</script>
</head>

<body style="background-color:#F5F5F5">
<?php
if ($stat == 1) 
{
	$info = "Satuan Kerja";
	$sql = "SELECT p.nip, TRIM(CONCAT(IFNULL(p.gelarawal,''), ' ', p.nama, ' ', IFNULL(p.gelarakhir,''))) AS fnama 
	        FROM pegawai p, peglastdata pl, pegjab pj, jabatan j
			WHERE p.aktif = 1 AND p.nip = pl.nip AND pl.idpegjab = pj.replid AND pj.idjabatan = j.replid AND j.satker='$ref' ORDER BY p.nama";	
} 
elseif ($stat == 2)
{
	$info = "Tingkat Pendidikan";
	$sql = "SELECT p.nip, TRIM(CONCAT(IFNULL(p.gelarawal,''), ' ', p.nama, ' ', IFNULL(p.gelarakhir,''))) AS fnama 
	        FROM pegawai p, peglastdata pl, pegsekolah ps
			WHERE p.aktif = 1 AND  p.nip = pl.nip AND pl.idpegsekolah = ps.replid AND ps.tingkat = '$ref' ORDER BY p.nama";
}
elseif ($stat == 3)
{
	$info = "Golongan";
	$sql = "SELECT p.nip, TRIM(CONCAT(IFNULL(p.gelarawal,''), ' ', p.nama, ' ', IFNULL(p.gelarakhir,''))) AS fnama 
	        FROM pegawai p, peglastdata pl, peggol pg
			WHERE p.aktif = 1 AND  p.nip = pl.nip AND pl.idpeggol = pg.replid AND pg.golongan = '$ref' ORDER BY p.nama";
}
elseif ($stat == 4)
{
	$info = "Usia";
	$sql = "SELECT nip, fnama FROM (
	          SELECT nip, fnama, IF(usia < 24, '<24',
                          IF(usia >= 24 AND usia <= 29, '24-29',
                          IF(usia >= 30 AND usia <= 34, '30-34',
                          IF(usia >= 35 AND usia <= 39, '35-39',
                          IF(usia >= 40 AND usia <= 44, '40-44',
                          IF(usia >= 45 AND usia <= 49, '45-49',
                          IF(usia >= 50 AND usia <= 55, '50-55', '>56'))))))) AS G FROM
                (SELECT nip, TRIM(CONCAT(IFNULL(p.gelarawal,''), ' ', p.nama, ' ', IFNULL(p.gelarakhir,''))) AS fnama, 
				        FLOOR(DATEDIFF(NOW(), tgllahir) / 365) AS usia FROM pegawai p WHERE aktif = 1) AS X) AS XX 
			WHERE G = '$ref'";
}

?>
<table width="100%" border="0">
  <tr>
    <td width="84%"><strong><?php echo$info?> : <?php echo$ref?></strong></td>
    <td width="16%" align="right"><a href="#" onclick="cetak()"><img src="../images/ico/print.png" width="16" height="16" border="0" />&nbsp;Cetak</a>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
	<table id="table" class="tab" border="1" cellpadding="2" cellspacing="0" width="100%">
		<tr height="35">
			<td class="header" align="center" width="7%">No</td>
			<td class="header" align="center" width="40%">NIP</td>
			<td class="header" align="center" width="40%">Nama</td>
			<td class="header" align="center" width="10%">&nbsp;</td>
		</tr>
		<?php
		OpenDb();
		$result = QueryDb($sql);
		$cnt = 0;
		while ($row = mysql_fetch_row($result)) {
		?>
		<tr height="20">
			<td align="center" valign="top"><?php echo++$cnt?></td>
			<td align="center" valign="top"><?php echo$row[0]?></b></td>
			<td align="left" valign="top"><?php echo$row[1] ?></td>
			<td align="center" valign="top">
				<a href="JavaScript:DetailPegawai('<?php echo$row[0]?>')" title="Lihat Detail Pegawai"><img src="../images/ico/lihat.png" border="0" /></a>
			</td>
		</tr>
		<?php
		}
		CloseDb();
		?>
		</table>
		<script language='JavaScript'>
		   Tables('table', 1, 0);
		</script>
	</td>
  </tr>
</table>



</body>
</html>