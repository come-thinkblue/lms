<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/sessionchecker.php");
require_once("../include/config.php");
require_once("../include/db_functions.php");
require_once("../include/common.php");
require_once('../include/theme.php');

OpenDb();

$nip = $_REQUEST['nip'];

$sql = "SELECT * FROM pegawai WHERE nip='$nip'";

$result = QueryDb($sql);
$row = mysql_fetch_array($result);

$namapeg = $row['nama'];
$gelarawal = $row['gelarawal'];
$gelarakhir = $row['gelarakhir'];
$pegawai = $namapeg;
$nip = $row['nip'];
$nuptk = $row['nuptk'];
$nrp = $row['nrp'];
$tmplahir = $row['tmplahir'];
$tgllahir = GetDatePart($row['tgllahir'], "d");
$blnlahir = GetDatePart($row['tgllahir'], "m");
$thnlahir = GetDatePart($row['tgllahir'], "y");
$agama = $row['agama'];
$suku = $row['suku'];
$nikah = $row['nikah'];
$jk = $row['kelamin'];
$alamat = $row['alamat'];
$hp = $row['handphone'];
$telpon = $row['telpon'];
$email = $row['email'];
$facebook = $row['facebook'];
$twitter = $row['twitter'];
$website = $row['website'];
$foto = $row['foto'];
$status = $row['status'];
$bagian = $row['bagian'];
$tglmulai = GetDatePart($row['mulaikerja'], "d");
$blnmulai = GetDatePart($row['mulaikerja'], "m");
$thnmulai = GetDatePart($row['mulaikerja'], "y");
$keterangan = $row['keterangan'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style<?php echo GetThemeDir2()?>.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar Kepegawaian</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top"><?php include("../include/headercetak.php") ?>
  <center><font size="4"><strong>DATA PRIBADI</strong></font><br /> </center><br /><br />
<br />

<table border="0" cellpadding="5" cellspacing="0" width="95%">
<tr>
	<td align="right" valign="top"><strong>Status :</strong></td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$status?></td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Bagian :</strong></td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$bagian?></td>
</tr>
<tr>
	<td width="140" align="right" valign="top"><strong>Nama </strong>:</td>
    <td width="0" align="left" valign="top"><?php echo$gelarawal . " " . $namapeg . " " . $gelarakhir?></td>
    <td width="113" rowspan="5" align="center" valign="top"><img src="../include/gambar.php?nip=<?php echo$nip?>&table=pegawai&field=foto" height="120" alt="Foto" /></td>
</tr>
<tr>
	<td align="right" valign="top"><strong>NIP </strong>:</td>
    <td width="0" align="left" valign="top"><?php echo$nip?></td>
</tr>
<tr>
	<td align="right" valign="top">NUPTK :</td>
    <td width="0" align="left" valign="top"><?php echo$nuptk?></td>
</tr>
<tr>
	<td align="right" valign="top">NRP :</td>
    <td width="0" align="left" valign="top"><?php echo$nrp?></td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Tempat, Tgl Lahir </strong>:</td>
    <td width="0" align="left" valign="top">
    <?php echo$tmplahir?>, <?php echo$tgllahir?> <?php echo NamaBulan($blnlahir)?> <?php echo$thnlahir?>    </td>
    </tr>
<tr>
	<td align="right" valign="top"><strong>Agama :</strong></td>
    <td width="0" align="left" valign="top"><?php echo$agama?></td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Suku :</strong></td>
    <td width="0" align="left" valign="top"><?php echo$suku?></td>
</tr>
<tr>
	<td align="right" valign="top"><strong>Pernikahan :</strong></td>
    <td width="0" align="left" valign="top"><?php echo$nikah?></td>
    </tr>
<tr>
	<td align="right" valign="top"><strong>Jenis Kelamin :</strong></td>
    <td width="*" colspan="2" align="left" valign="top"><?php if ($jk == "l") echo "Laki-Laki"; else echo "Perempuan"; ?></td>
</tr>
<tr>
	<td align="right" valign="top">Alamat :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$alamat?></td>
</tr>
<tr>
	<td align="right" valign="top">HP :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$hp?></td>
</tr>
<tr>
	<td align="right" valign="top">Telpon :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$telpon?></td>
</tr>
<tr>
	<td align="right" valign="top">Email :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$email?></td>
</tr>
<tr>
	<td align="right" valign="top">Facebook :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$facebook?></td>
</tr>
<tr>
	<td align="right" valign="top">Twitter :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$twitter?></td>
</tr>
<tr>
	<td align="right" valign="top">Website :</td>
    <td width="*" colspan="2" align="left" valign="top"><?php echo$website?></td>
</tr>
<tr>
	<td align="right" valign="top">Keterangan :</td>
    <td width="*" colspan="2" align="left" valign="top">
	<?php echo$keterangan?>    </td>
</tr>
</table>
	

</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>