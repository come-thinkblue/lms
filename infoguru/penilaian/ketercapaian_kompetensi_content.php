<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');
	require_once('../include/exceldata.php');
	require_once('../cek.php');

	$departemen = $_REQUEST['departemen'];
	$tahunajaran = $_REQUEST['tahunajaran'];
	$tingkat = $_REQUEST['tingkat'];
	$kelas = $_REQUEST['kelas'];
	$semester = $_REQUEST['semester'];
	$pelajaran = $_REQUEST['pelajaran'];
	
	$varbaris = 20;
	if (isset($_REQUEST['varbaris']))
		$varbaris = $_REQUEST['varbaris'];

	$page = 0;
	if (isset($_REQUEST['page']))
		$page = $_REQUEST['page'];

	$hal = 0;
	if (isset($_REQUEST['hal']))
		$hal = $_REQUEST['hal'];

	$urut = "kelas";
	if (isset($_REQUEST['urut']))
		$urut = $_REQUEST['urut'];
	$urutan = "ASC";
	if (isset($_REQUEST['urutan']))
		$urutan = $_REQUEST['urutan'];

	OpenDb();
	
	$op = $_REQUEST['op'];
	$aspek = $_REQUEST['aspek_penilaian'];
	$predikat = $_REQUEST['predikat'];
	
	if ($op == "hapus"){
		$sql = "DELETE FROM ketercapaian_kompetensi WHERE semester='$semester' AND pelajaran='$pelajaran' AND tahunajaran='$tahunajaran' 
					AND aspek_penilaian='$aspek' AND predikat='$predikat'";
		//echo"$sql";
		QueryDb($sql);
		?>
			<script language="javascript">
				refresh();
			</script>
		<?php
	} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Ketercapaian Kompetensi</title>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript">
			function refresh() {
				var departemen = document.getElementById('departemen').value;
				var tingkat = document.getElementById('tingkat').value;
				var tahun = document.getElementById('tahunajaran').value;
				var semester = document.getElementById('semester').value;
				var kelas = document.getElementById('kelas').value;
				var pelajaran = document.getElementById('pelajaran').value;
					
				parent.footer.location.href = "ketercapaian_kompetensi_content.php?departemen="+departemen+"&tingkat="+tingkat+"&semester="+semester+"&kelas="+kelas+"&pelajaran="+pelajaran+"&tahunajaran="+tahun;
			}

			function tambah() {				
				newWindow('ketercapaian_kompetensi_add.php?departemen=<?php echo$departemen?>&tahunajaran=<?php echo$tahunajaran?>&tingkat=<?php echo$tingkat?>&semester=<?php echo$semester?>&kelas=<?php echo$kelas?>&pelajaran=<?php echo$pelajaran?>','Tambahketercapaian_kompetensi','600','400','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function edit(id,grade) {
				newWindow('ketercapaian_kompetensi_edit.php?departemen=<?php echo$departemen?>&tahunajaran=<?php echo$tahunajaran?>&tingkat=<?php echo$tingkat?>&semester=<?php echo$semester?>&kelas=<?php echo$kelas?>&pelajaran=<?php echo$pelajaran?>&aspek_penilaian='+id+'&predikat='+grade, 'Ubahketercapaian_kompetensi','600','420','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function hapus(id,grade) {
				var departemen = document.getElementById('departemen').value;
				var tingkat = document.getElementById('tingkat').value;
				var tahun = document.getElementById('tahunajaran').value;
				var semester = document.getElementById('semester').value;
				var kelas = document.getElementById('kelas').value;
				var pelajaran = document.getElementById('pelajaran').value;
				
				//alert("ketercapaian_kompetensi_content.php?departemen="+departemen+"&tingkat="+tingkat+"&semester="+semester+"&kelas="+kelas+"&pelajaran="+pelajaran+"&tahunajaran="+tahun+"&op=hapus&aspek_penilaian="+id+"&predikat="+grade);
				
				if (confirm('Apakah anda yakin akan menghapus ketercapaian kompetensi ini?'))
					document.location.href = "ketercapaian_kompetensi_content.php?departemen="+departemen+"&tingkat="+tingkat+"&semester="+semester+"&kelas="+kelas+"&pelajaran="+pelajaran+"&tahunajaran="+tahun+"&op=hapus&aspek_penilaian="+id+"&predikat="+grade;
			}

			function cetak() {
				var departemen = document.getElementById('departemen').value;
				var tingkat = document.getElementById('tingkat').value;
				var tahun = document.getElementById('tahunajaran').value;
				var semester = document.getElementById('semester').value;
				var kelas = document.getElementById('kelas').value;
				var pelajaran = document.getElementById('pelajaran').value;
				var aspek_penilaian = document.getElementById('aspek_penilaian').value;
				var predikat = document.getElementById('predikat').value;
				
				newWindow("ketercapaian_kompetensi_cetak.php?departemen="+departemen+"&tingkat="+tingkat+"&semester="+semester+"&kelas="+kelas+"&pelajaran="+pelajaran+"&tahunajaran="+tahun+"&aspek_penilaian="+aspek_penilaian+"&predikat="+predikat, 'CetakSiswa','850','700','resizable=1,scrollbars=1,status=0,toolbar=0')
			}
		</script>
	</head>
	<body topmargin="0" leftmargin="0">
		<input type="hidden" name="urut" id="urut" value="<?php echo $urut ?>" />
		<input type="hidden" name="urutan" id="urutan" value="<?php echo $urutan ?>" />
		
		<input type="hidden" id="departemen" value="<?php echo$departemen?>">
		<input type="hidden" id="tahunajaran" value="<?php echo$tahunajaran?>">
		<input type="hidden" id="tingkat" value="<?php echo$tingkat?>">
		<input type="hidden" id="kelas" value="<?php echo$kelas?>">
		<input type="hidden" id="semester" value="<?php echo$semester?>">
		<input type="hidden" id="pelajaran" value="<?php echo$pelajaran?>">
		
		<?php
		$sel_matpel = "SELECT pelajaran FROM ketercapaian_kompetensi WHERE pelajaran=$pelajaran";
		$rs_matpel = QueryDb($sel_matpel);
		$row_matpel = @mysql_fetch_row($rs_matpel);
		//echo "$row_matpel[0]";
		?>
		
		<table border="0" width="90%" align="center">
			<!-- TABLE CENTER -->
			<tr>
				<td align="right">
					<input type="hidden" name="total" id="total" value="<?php echo $total ?>"/>
					<table width="100%" border="0" align="center">
						<tr>
							<td align="right">
								<a href="#" onClick="refresh()">
									<img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh
								</a>&nbsp;&nbsp;
								<a href="JavaScript:cetak()">
									<img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak
								</a>&nbsp;&nbsp; 
							</td>
						</tr>    
					</table>
					<br />       
					<table border="1" width="100%" id="table" class="tab" align="center" style="border-collapse:collapse" bordercolor="#000000" >
						<tr class="header" height="30" align="center">		
							<td width="4%">No</td>
							<td width="20%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('aspek_penilaian','<?php echo $urutan ?>')">Aspek Penilaian <?php echo change_urut('aspek_penilaian', $urut, $urutan) ?></td>
							<td width="20%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('predikat','<?php echo $urutan ?>')">Predikat <?php echo change_urut('predikat', $urut, $urutan) ?></td>
							<td width="*"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('deskripsi','<?php echo $urutan ?>')">Ketrcapaian Kompetensi<?php echo change_urut('deskripsi', $urut, $urutan) ?></td>
							<td width="10%">&nbsp;</td>
						</tr>
						<?php
						CloseDb();
						if ($page == 0) {
							$cnt = 1;
						} else {
							$cnt = (int) $page * (int) $varbaris + 1;
						}
						
						
						OpenDb();							
						//cek syncron
						$sql_sel = "SELECT DISTINCT a.dasarpenilaian, b.keterangan 
									FROM aturannhb a, dasarpenilaian b 
									WHERE a.dasarpenilaian=b.dasarpenilaian AND a.idpelajaran='$pelajaran'";
						$rs_sel = QueryDb($sql_sel);
						CloseDb();
						while($row_sel = @mysql_fetch_row($rs_sel)){
							?>
							<tr>
								<td height="25" align="center" rowspan="6"><?php echo $cnt ?></td>
								<td height="25" align="center" rowspan="6"><?php echo $row_sel[1] ?></td>
								<?php
								OpenDb();
								$sql_pred = "SELECT id_gradenilai,predikat FROM gradenilai ORDER BY huruf";
								$rs_pred = QueryDb($sql_pred);
								CloseDb();
								while($row_pred = @mysql_fetch_row($rs_pred)){
									?>
									<tr>
										<td height="30" align="center"><?php echo$row_pred[1]?></td>
										<?php
										OpenDb();
										$sql_desc = "SELECT deskripsi FROM ketercapaian_kompetensi 
													 WHERE tingkat='$tingkat' AND semester='$semester' AND pelajaran='$pelajaran' 
													 AND tahunajaran='$tahunajaran' AND aspek_penilaian='$row_sel[0]' AND predikat=$row_pred[0]";
										//echo"$sql_desc";
										$rs_desc = QueryDb($sql_desc);
										$row_desc = @mysql_fetch_row($rs_desc);
										CloseDb();
										?>
										<td height="30" align="left"><?php echo$row_desc[0]?></td>
										<td height="30" align="center">
											<a href="JavaScript:edit('<?php echo$row_sel[0]?>','<?php echo$row_pred[0]?>')" /><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Ektra Kulikuler!', this, event, '80px')"/></a>&nbsp;
											<a href="JavaScript:hapus('<?php echo$row_sel[0] ?>','<?php echo$row_pred[0]?>')"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Data Ekstra Kurikuler!', this, event, '80px')"/></a>
											<input type="hidden" name="aspek_penilaian" id="aspek_penilaian" value="<?php echo$row_sel[0] ?>"/>
											<input type="hidden" name="predikat" id="predikat" value="<?php echo$row_pred[0]?>"/>
										</td>
									</tr>
									<?php
								}
								?>
							</tr>
							<?php
							$cnt++;
						}
						?>
						<!-- END TABLE CONTENT -->
					</table>

					<script language='JavaScript'>
						Tables('table', 1, 0);
					</script>
				</td>
			</tr> 
		</table>  
		<?php
		CloseDb();
		?>
	</body>
</html>