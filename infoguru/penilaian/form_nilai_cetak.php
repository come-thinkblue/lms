<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
require_once("../include/sessionchecker.php");

$departemen = $_REQUEST['departemen'];
$tahunajaran = $_REQUEST['tahunajaran'];
$semester = $_REQUEST['semester'];
$kelas = $_REQUEST['kelas'];
$pelajaran = $_REQUEST['pelajaran'];
$nip = $_REQUEST['nip'];

OpenDb();
$sql = "SELECT k.kelas, s.semester, a.tahunajaran, t.tingkat, l.nama, p.nama AS guru FROM kelas k, semester s, tahunajaran a, tingkat t, pelajaran l, $g_db_pegawai.pegawai p WHERE k.replid = '$kelas' AND s.replid = '$semester' AND a.replid = '$tahunajaran' AND k.idtahunajaran = a.replid AND k.idtingkat = t.replid AND l.replid = '$pelajaran' AND p.nip = '$nip'";
$result = QueryDb($sql);
CloseDb();
$row = mysql_fetch_array($result);
$namakelas = $row['kelas'];
$namasemester = $row['semester'];
$namatahunajaran = $row['tahunajaran'];
$namatingkat = $row['tingkat'];
$namapelajaran = $row['nama'];
$namaguru = $row['guru'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MAN Kota Blitar INFOGURU [Cetak Form Pengisian Nilai Siswa]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">
<?php echo GetHeader($departemen)?>
<center>
  <font size="4"><strong>FORM PENGISIAN NILAI SISWA </strong></font><br />
 </center><br /><br />

<br />

<table>
<tr>
  	<td><strong>Departemen</strong></td>
  	<td width="30%"><strong>:&nbsp;<?php echo$departemen ?></strong></td>
    <td rowspan="2" valign="top"><strong>Tanggal</strong></td>  	
  	<td rowspan="2" valign="top"><strong>:&nbsp;_________________</strong></td>
</tr>
<tr>
    <td><strong>Tahun Ajaran</strong></td>
  	<td><strong>:&nbsp;<?php echo$namatahunajaran?></strong>
    </td>
</tr>
<tr>
    <td><strong>Semester</strong></td>
  	<td><strong>:&nbsp;<?php echo$namasemester?></strong></td>
    <td rowspan="2" valign="top"><strong>Jenis Pengujian</strong></td>  
  	<td rowspan="2" valign="top"><strong>:&nbsp;_______________________________________</strong></td>
</tr>
<tr>
    <td><strong>Kelas</strong></td>
  	<td><strong>:&nbsp;<?php echo$namatingkat ?>&nbsp;-&nbsp;<?php echo$namakelas?></strong></td>
</tr>
<tr>
    <td><strong>Pelajaran</strong></td>
  	<td><strong>:&nbsp;<?php echo$namapelajaran?></strong></td>
    <td valign="top" rowspan="2"><strong>Keterangan</strong></td>
   	<td valign="top" rowspan="2"><strong>:&nbsp;_______________________________________</strong></td>		
</tr>
<!--
<tr height="25">
    <td><strong>Tanggal</strong></td>  	
  	<td><strong>:&nbsp;_________________</strong></td>
</tr>
<tr height="25">
  	<td><strong>Jenis Pengujian</strong></td>  
  	<td><strong>:&nbsp;_______________________________________</strong></td>
</tr>
<tr height="25">
    <td valign="top"><strong>Keterangan</strong></td>
   	<td valign="top"><strong>:&nbsp;___________________________________________________________________________
     <p> &nbsp; ___________________________________________________________________________
     </strong></td>
</tr>-->
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left">
        <tr>
            <td width="5%" height="30" align="center" class="header">No</td>
            <td width="10%" height="30" align="center" class="header">N I S</td>
            <td width="30%" height="30" align="center" class="header">Nama</td>
            <td width="10%" height="30" align="center" class="header">Nilai</td>	
            <td width="55%" height="30" align="center" class="header">Keterangan</td>
        </tr>
        
        <?php
        OpenDb();
        
        $sql = "SELECT nis, nama FROM siswa WHERE idkelas = '$kelas' ORDER BY nama";
        $result = QueryDb($sql);
        CloseDb();
        
        while($row = @mysql_fetch_array($result)){
        
        ?>
        <tr>
            <td height="25" align="center"><?php echo++$i ?></td>
            <td height="25" align="center"><?php echo$row['nis'] ?></td>
            <td height="25"><?php echo$row['nama'] ?></td>
            <td height="25"></td>
            <td height="25"></td>
        </tr>
        <?php } ?>
    </table>
    </td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0">
        <tr>
            <td width="80%" align="left"></td>
            <td width="20%" align="center"><br><br>Guru</td>
        </tr>
        <tr>
            <td colspan="2" align="right">&nbsp;<br /><br /><br /><br /><br /></td>
        </tr>
        <tr>		
            <td></td>
            <td valign="bottom" align="center"><strong><?php echo$namaguru?></strong>
            <br /><hr />
            <strong>NIP. <?php echo$nip?></strong>
        </tr>
    </table>
    </td>
  </tr>
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>