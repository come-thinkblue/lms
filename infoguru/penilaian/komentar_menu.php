<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/dpupdate.php');

$urut = $_REQUEST['urut'];
if ($urut=="")
	$urut="nama";
else 
	$urut = $_REQUEST['urut'];

$urutan = $_REQUEST['urutan'];
if ($urutan=="")
	$urutan="asc";
else 
	$urutan = $_REQUEST['urutan'];

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat2 = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/aTR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Komentar Nilai Rapor</title>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function refresh() 
{	
	document.location.reload();
}

function change_dep() 
{	
	parent.header.change_dep();
}

function tampil(nis) 
{	
	parent.komentar_content.location.href="komentar_content.php?departemen=<?php echo$departemen?>&semester=<?php echo$semester?>&tingkat=<?php echo$tingkat2?>&tahunajaran=<?php echo$tahunajaran?>&pelajaran=<?php echo$pelajaran?>&kelas=<?php echo$kelas?>&nis="+nis;
}

function change_urut(urut,urutan) 
{
//alert ('WOWOR');
	if (urutan=="asc"){
	urutan="desc"
	} else {
	urutan="asc"
	}
	//if (confirm("Apakah anda yakin akan menghapus angkatan ini?"))
	document.location.href="komentar_menu.php?departemen=<?php echo$departemen?>&semester=<?php echo$semester?>&tingkat=<?php echo$tingkat2?>&tahunajaran=<?php echo$tahunajaran?>&pelajaran=<?php echo$pelajaran?>&kelas=<?php echo$kelas?>&urut="+urut+"&urutan="+urutan;
	
}
</script>
<style type="text/css">
<!--
.style1 {
	font-size: 12px;
	font-weight: bold;
}
.style2 {color: #FFFF00}
.style3 {color: #FFFFFF}
-->
</style>
</head>

<body topmargin="0" leftmargin="0">
<form name="main" method="post" action="komentar_footer.php" enctype="multipart/form-data">

<input type="hidden" name="departemen" id="departemen" value="<?php echo$departemen ?>" />
<input type="hidden" name="semester" id="semester" value="<?php echo$semester ?>" />
<input type="hidden" name="tingkat" id="tingkat" value="<?php echo$tingkat2 ?>" />
<input type="hidden" name="tahunajaran" id="tahunajaran" value="<?php echo$tahunajaran ?>" />
<input type="hidden" name="pelajaran" id="pelajaran" value="<?php echo$pelajaran ?>" />
<input type="hidden" name="kelas" id="kelas" value="<?php echo$kelas ?>" />
<?php 
	OpenDb();		
	$sql = "SELECT DISTINCT k.replid, s.nis, s.nama, k.komentar 
			  FROM siswa s, komennap k, infonap i 
			 WHERE s.idkelas = '$kelas' AND k.nis = s.nis AND k.idinfo = i.replid 
			   AND i.idkelas = '$kelas' AND i.idpelajaran = '$pelajaran' AND i.idsemester= '$semester' ORDER BY $urut $urutan";
	$sql = "SELECT nis, nama
			  FROM siswa
			 WHERE idkelas = '$kelas' AND aktif = 1
		  ORDER BY $urut $urutan";			   
	$result = QueryDb($sql);		
	$cnt = 1;
	$jum = @mysql_num_rows($result);
	if ($jum > 0) 
	{ ?>	

<table border="0" width="100%" align="center">
<!-- TABLE CENTER -->
<tr>
    <td align="left" valign="top" colspan="2">       
	<table border="1" width="100%" id="table" class="tab" bordercolor="#000000" >
	<tr>		
		<td width="3%" height="30" align="center" class="header">No</td>
		<td height="30" onMouseOver="background='../style/formbg2agreen.gif';height=30;" 
        	onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" 
            onClick="change_urut('nama','<?php echo$urutan?>')">
            <div align="center" class="style1"><strong><span class="style3">Siswa</span></strong></div>
        </td>
	</tr>
<?php 	while ($row = @mysql_fetch_array($result)) 
	{	?>
    <tr>        			
		<td height="25" align="center" onclick="tampil('<?php echo$row[nis]?>')" style="cursor:pointer" title="Klik untuk menampilkan komentar rapor siswa ini">
		<?php echo$cnt?>
        </td>
  		<td height="25" onclick="tampil('<?php echo$row[nis]?>')" style="cursor:pointer" title="Klik untuk menampilkan komentar rapor <?php echo$row['nama']?>">
		<?php echo$row['nis']?><br /><?php echo$row['nama']?>
        </td>
    </tr>
<?php	$cnt++;
	} 
	CloseDb();	?>
	</table>   	 
    <!-- END TABLE CONTENT -->
    <script language='JavaScript'>
	   	Tables('table', 1, 0);
    </script> 
   	</td>
</tr>
<!-- END TABLE CENTER -->    
</table>
</form>
<?php  } 
?>
</body>
</html>