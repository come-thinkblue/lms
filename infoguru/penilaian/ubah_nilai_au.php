<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');

if(isset($_POST["departemen"])){
	$departemen = $_POST["departemen"];
}elseif(isset($_GET["departemen"])){
	$departemen = $_GET["departemen"];
}
?>

<html>
<head>
<title>MAN Kota Blitar INFOGURU [Ubah Nilai Akhir Ujian]</title>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="JavaScript">
    function cek_form() {

      var nilai = document.ubah_nilai_au.kode.value;

        if(nilai.length == 0) {
            alert("Nilai tidak boleh kosong");
            document.ubah_nilai_au.nilai.value = "";
            document.ubah_nilai_au.nilai.focus();
            return false;
        }
        return true;
    }
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style=" background-image:url(../images/bgpop.jpg); background-repeat:repeat-x">


<?php
OpenDb();

if(isset($_POST[ubah])) {

    $query_up = "UPDATE $g_db_akademik.nau SET nilaiAU = '$_POST[nilai]' WHERE replid = '$_POST[id]'";
	$result_up = QueryDb($query_up) or die (mysql_error());
		
	  if(mysql_affected_rows> 0){
            ?>
            <script language="JavaScript">
				opener.document.location.href = "tampil_nilai_pelajaran.php?pelajaran=<?php echo$_POST[pelajaran] ?>&kelas=<?php echo$_POST[kelas] ?>&semester=<?php echo$_POST[semester] ?>&jenis_penilaian=<?php echo$_POST[jenis_penilaian] ?>&departemen=<?php echo$_POST[departemen] ?>&tahun=<?php echo$_POST[tahun] ?>&tingkat=<?php echo$_POST[tingkat] ?>";
				window.close();
            </script>
            <?php
        }
        elseif(mysql_affected_rows($conni) == 0){ 
            ?>
            <script language="JavaScript">
               // alert("Gagal mengubah data.");
				opener.document.location.href = "tampil_nilai_pelajaran.php?pelajaran=<?php echo$_POST[pelajaran] ?>&kelas=<?php echo$_POST[kelas] ?>&semester=<?php echo$_POST[semester] ?>&jenis_penilaian=<?php echo$_POST[jenis_penilaian] ?>&departemen=<?php echo$_POST[departemen] ?>&tahun=<?php echo$_POST[tahun] ?>&tingkat=<?php echo$_POST[tingkat] ?>";
				window.close();
            </script>
            <?php
        }

}
$query = "SELECT * FROM $g_db_akademik.nau WHERE replid = '$_GET[id]'";
$result = QueryDb($query);
$row = @mysql_fetch_array($result);
 
?>

    <form action="ubah_nilai_au.php" method="post" name="ubah_nilai_au" onSubmit="return cek_form()">
	
    <input type="hidden" name="id" value="<?php echo$row[replid] ?>">
	<input type="hidden" name="idujian" value="<?php echo$row[idujian] ?>">
	<input type="hidden" name="jenis_penilaian" value="<?php echo$_GET[jenis_penilaian] ?>">
	<input type="hidden" name="pelajaran" value="<?php echo$_GET[pelajaran] ?>">
	<input type="hidden" name="kelas" value="<?php echo$_GET[kelas] ?>">
	<input type="hidden" name="semester" value="<?php echo$_GET[semester] ?>">
	<input type="hidden" name="tahun" value="<?php echo$_GET[tahun] ?>">
	<input type="hidden" name="tingkat" value="<?php echo$_GET[tingkat] ?>">
	<input type="hidden" name="departemen" value="<?php echo$departemen ?>">
	<table border="0" align="center" width="100%">
        <tr>
            <td class="header">Ubah Nilai Akhir Ujian</td>
        </tr>
		<tr>
			<td>
			<fieldset><legend><b>Data Nilai Ujian</b></legend>
			<table width="100%">
        <tr>
            <td width="19%">NIS</td>
            <td width="81%">
            <input type="hidden" name="u_nis" value="<?php echo$row[nis]; ?>">
            <input type="text" size="45" name="nis" value="<?php echo$row[nis]; ?>" readonly></td>
        </tr>
        <tr>
            <td>Nama</td><td>
			<?php
			$query_nm = "SELECT * FROM $g_db_akademik.siswa WHERE siswa.nis = '$row[nis]'";
			$result_nm = QueryDb($query_nm);
			$row_nm = @mysql_fetch_array($result_nm);
			?>
			<input type="text" size="45" name="nama" value="<?php echo$row_nm[nama];?>" readonly></td>
        </tr>
       
		<tr>
			<td>Nilai Akhir</td>
			<td><input type="text" name="nilai" size="5" value="<?php echo$row[nilaiAU] ?>" maxlength="8"></td>
		</tr>
		</table>
		</fieldset>		</td>
		</tr>
        <tr>
            <td>
              <div align="center">
                <input type="button" value="Batal" name="batal" class="but" onClick="window.close();">
                <input type="submit" value="Simpan" name="ubah" class="but">			
              </div></td>
          </tr>
    </table>
    </form>

<?php
CloseDb();
?>
</body>
</html>