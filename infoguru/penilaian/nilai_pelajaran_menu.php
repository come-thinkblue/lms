<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../sessionchecker.php');
require_once('../library/dpupdate.php');

if(isset($_REQUEST["departemen"]))
	$departemen = $_REQUEST["departemen"];
if(isset($_REQUEST["semester"]))
	$semester = $_REQUEST["semester"];
if(isset($_REQUEST["kelas"]))
	$kelas = $_REQUEST["kelas"];
if(isset($_REQUEST["nip"]))
	$nip = $_REQUEST["nip"];
	
$warna = array('fcf5ca','d5fcca','cafcf3','cae6fc','facafc');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../style/style.css">
<title>Menu</title>
<script language="JavaScript" src="../script/tables.js"></script>
<script language="JavaScript">

function klik(kelas,semester,idaturan,idpelajaran)
{	
	parent.nilai_pelajaran_content.location.href="nilai_pelajaran_content.php?&kelas="+kelas+"&semester="+semester+"&idaturan="+idaturan+"&idpelajaran="+idpelajaran+"&nip=<?php echo $nip?>";
}

</script>
</head>
<body style="margin-left:0px; margin-top:0px; margin-right:1px">
<?php 
OpenDb();
$sql = "SELECT idtingkat FROM kelas WHERE replid='$kelas'";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);	
$tingkat2 = $row['idtingkat'];

$query_aturan = "SELECT DISTINCT g.idpelajaran, p.nama
				       FROM $g_db_akademik.aturannhb a, $g_db_akademik.pelajaran p, $g_db_akademik.guru g
						WHERE g.nip = '$nip'
						  AND g.idpelajaran = p.replid
						  AND p.departemen = 'MAN Kota Blitar'
						  AND a.idtingkat = '$tingkat2'
						  AND a.aktif = 1
					   ORDER BY p.nama";

$result_aturan = QueryDb($query_aturan);
if (!mysql_num_rows($result_aturan) == 0)
{
?>
<table width="100%" border="0">
<?php	$i = 0;
	$cnt = 0;
	while ($row_aturan = @mysql_fetch_array($result_aturan))
	{
		if ($i >= 5)
			$i = 0; ?>
<tr>
	<td>
    <fieldset style="background-color:#<?php echo$warna[$i]?>">
    <legend><strong><font size="2" face="verdana">
		<?php echo$row_aturan['nama']; ?></font></strong>
    </legend> 
    <table width="100%" cellspacing="2" >
<?php	$query_ap = "SELECT DISTINCT a.dasarpenilaian, dp.keterangan 
				   FROM $g_db_akademik.aturannhb a, dasarpenilaian dp
				  WHERE a.dasarpenilaian = dp.dasarpenilaian 
				    AND idtingkat = '$tingkat2' ORDER BY keterangan";
	$result_ap = QueryDb($query_ap);
	while($row_ap = @mysql_fetch_array($result_ap))
	{
		$cnt++;	?> 
	   	<tr>
   		<td>  
        	<table class="tab" id="table<?php echo$cnt?>" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
        	<tr height="30" class="header" align="center">
            	<td><?php echo$row_ap['keterangan']?></td>
	        </tr>
    <?php 	$query_jp = "SELECT a.idjenisujian, j.jenisujian, j.replid, a.replid 
					   FROM $g_db_akademik.aturannhb a, $g_db_akademik.jenisujian j 
					  WHERE a.dasarpenilaian='$row_ap[dasarpenilaian]' 
					    AND a.idjenisujian=j.replid AND a.idtingkat='$tingkat2'  ORDER BY j.jenisujian";
		$result_jp = QueryDb($query_jp);
		while($row_jp = @mysql_fetch_row($result_jp))
		{	?>
        	<tr>
            <td height="25" style="cursor:pointer" onclick="klik('<?php echo$kelas?>','<?php echo$semester?>','<?php echo$row_jp[3]?>','<?php echo $row_aturan['idpelajaran']; ?>')" align="center">
            	<u><strong><?php echo$row_jp[1]?></strong></u>
            </td>
        	</tr>
	<?php	} ?>
  			</table>
	   		<script language='JavaScript'>Tables('table<?php echo$cnt?>', 1, 0);</script>
    	</td>
    </tr>	
<?php	} ?>
	</table> 
    </fieldset>
    </td>
</tr>
<?php
  $i++;
  } 
?>
</table>  
<?php 
} else { 
?>
<table width="100%" border="0" align="center">          
<tr>
    <td align="center" valign="middle" height="300">
    <font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br /><br />Tambah aturan perhitungan nilai rapor yang akan diajar oleh guru <?php echo$_REQUEST['nama']?> di menu Aturan Perhitungan Nilai Rapor pada bagian Guru & Pelajaran. </b></font>
    </td>
</tr>
</table>
<?php } ?>
</body>
<?php CloseDb(); ?>
</html>