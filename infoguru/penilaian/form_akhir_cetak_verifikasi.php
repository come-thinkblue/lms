<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php 
require_once("../include/theme.php"); 
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../library/dpupdate.php');
require_once("../include/sessionchecker.php");

if (isset($_REQUEST['nip']))
	$nip = $_REQUEST['nip'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['pelajaran']))
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['tingkat']))
	$tingkat2 = $_REQUEST['tingkat'];
	
$aspek = "";
if (isset($_REQUEST['aspek']))
	$aspek = $_REQUEST['aspek'];
$aturan = "";
if (isset($_REQUEST['aturan']))
	$aturan = $_REQUEST['aturan'];
$fokus = "aspek";
if (isset($_REQUEST['fokus']))
	$fokus = $_REQUEST['fokus'];	

$ERROR_MSG = "<br><br>";

OpenDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Form Nilai Akhir]</title>
<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/ajax.js"></script>
<script language="javascript">

function validate(){	
	var aspek=document.getElementById("aspek").value;
	var aturan=document.getElementById("aturan").value;
	var semester=document.getElementById("semester").value;
	var pelajaran=document.getElementById("pelajaran").value;
	var kelas=document.getElementById("kelas").value;
	var tingkat=document.getElementById("tingkat").value;
	var nip=document.getElementById("nip").value;
	newWindow('form_akhir_cetak.php?idaturan='+aturan+'&semester='+semester+'&kelas='+kelas+'&nip='+nip, 'CetakFormPengisianNilaiAkhirSiswa',790,850,'resizable=1,scrollbars=1,status=0,toolbar=0');
	window.close();	
	//document.location.href = "form_akhir_cetak_verifikasi.php?aspek="+aspek+"&kelas="+kelas+"&semester="+semester+"&pelajaran="+pelajaran+"&tingkat="+tingkat+"&nip="+nip+"&aturan="+aturan+"&cetak=1";	
}

function change_aspek(){
	var aspek=document.getElementById("aspek").value;
	var semester=document.getElementById("semester").value;
	var pelajaran=document.getElementById("pelajaran").value;
	var kelas=document.getElementById("kelas").value;
	var tingkat=document.getElementById("tingkat").value;
	var nip=document.getElementById("nip").value;
	
	document.location.href = "form_akhir_cetak_verifikasi.php?aspek="+aspek+"&kelas="+kelas+"&semester="+semester+"&pelajaran="+pelajaran+"&tingkat="+tingkat+"&nip="+nip+'&fokus=aspek';
	
}

function change_aturan(){
	var aspek=document.getElementById("aspek").value;
	var semester=document.getElementById("semester").value;
	var pelajaran=document.getElementById("pelajaran").value;
	var kelas=document.getElementById("kelas").value;
	var tingkat=document.getElementById("tingkat").value;
	var nip=document.getElementById("nip").value;
	var aturan=document.getElementById("aturan").value;
	
	document.location.href = "form_akhir_cetak_verifikasi.php?aspek="+aspek+"&kelas="+kelas+"&semester="+semester+"&pelajaran="+pelajaran+"&tingkat="+tingkat+"&nip="+nip+'&aturan='+aturan+'&fokus=aturan';
	
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
		return false;
    }
    return true;
}


</script>
<style type="text/css">
<!--
.style2 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#fff" onLoad="document.getElementById('<?php echo$fokus?>').focus();">
    <!-- CONTENT GOES HERE //--->
    <form name="main" method="post">  
    <input type="hidden" name="semester" id="semester" value="<?php echo$semester?>">
    <input type="hidden" name="tingkat" id="tingkat" value="<?php echo$tingkat2?>">
    <input type="hidden" name="kelas" id="kelas" value="<?php echo$kelas?>">
    <input type="hidden" name="pelajaran" id="pelajaran" value="<?php echo$pelajaran?>">
	<input type="hidden" name="nip" id="nip" value="<?php echo$nip?>">
	<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
	<!-- TABLE CONTENT -->
	<tr height="25">
        <td colspan="2" class="header" align="center">Cetak Form Nilai Akhir Berdasarkan Jenis Pengujian</td>
    </tr>
    <tr>
    	<td><strong>Aspek Penilaian</strong></td>
    	<td><select name="aspek" id="aspek" onChange="change_aspek()" style="width:150px" onKeyPress="return focusNext('aturan', event)">
			<?php 
			$sql_daspen="SELECT DISTINCT a.dasarpenilaian, dp.keterangan
						   FROM $g_db_akademik.aturannhb a, dasarpenilaian dp 
						  WHERE a.dasarpenilaian = dp.dasarpenilaian AND a.idtingkat = '$tingkat2' 
						    AND a.idpelajaran = '$pelajaran' AND a.aktif = 1 
							AND a.nipguru = '$nip' 
					   ORDER BY	keterangan";
			$result_daspen = QueryDb($sql_daspen);
			$num_daspen = @mysql_num_rows($result_daspen);
			while ($row_daspen=@mysql_fetch_array($result_daspen))
			{
				if ($aspek=="")
					$aspek=$row_daspen['dasarpenilaian']; ?>	
				<option value="<?php echo$row_daspen['dasarpenilaian']?>" <?php echo StringIsSelected($row_daspen['dasarpenilaian'], $aspek) ?>><?php echo$row_daspen['keterangan']?></option>
		<?php } ?>
  		    </select>
  		</td>
  	</tr>
<?php	if ($num_daspen>0)
	{	?>
  	<tr>
    	<td><strong>Jenis Pengujian</strong></td>
    	<td>
        	<select name="aturan" id="aturan" style="width:150px" onKeyPress="return focusNext('cetak', event)" onChange="change_aturan()">
			<?php
			$sql_jenispengujian = 
				"SELECT a.replid, j.jenisujian 
				   FROM $g_db_akademik.aturannhb a, $g_db_akademik.jenisujian j 
				  WHERE a.idtingkat='$tingkat2' AND a.idpelajaran='$pelajaran' AND a.aktif=1 
				    AND a.dasarpenilaian='$aspek' AND nipguru='$nip' AND j.replid = a.idjenisujian
 		       ORDER BY jenisujian";
			$result_jenispengujian=QueryDb($sql_jenispengujian);
			$num_jenispengujian=@mysql_num_rows($result_jenispengujian);
			while ($row_jenispengujian=@mysql_fetch_array($result_jenispengujian))
			{
				if ($aturan=="")
					$aturan=$row_jenispengujian['replid'];	?>
    		    <option value="<?php echo urlencode($row_jenispengujian['replid'])?>" <?php echo IntIsSelected($row_jenispengujian['replid'], $aturan) ?>><?php echo$row_jenispengujian['jenisujian']?>
   		        </option>
		<?php  } ?>
  		    </select>
 		</td>
	</tr>
	<?php 	if ($num_jenispengujian!=0)
        { 
            $sql = "SELECT * FROM ujian 
                    WHERE idkelas = '$kelas' AND idaturan = '$aturan' AND idsemester = '$semester'";
            $result = QueryDb($sql);
                
            if (mysql_num_rows($result) > 0) 
            {	
                $but = "<input class=\"but\" type=\"button\" name=\"cetak\" id=\"cetak\" value=\"Cetak\" onClick=\"validate();\"  />";
            } else {
                $ERROR_MSG = "Belum ada data ujian pada aspek penilaian dan jenis pengujian ini!";		
            }
        } 
		else 
		{
            $ERROR_MSG = "Belum ada aturan penilaian yang tersimpan!";
        }
	} 
	else 
	{ 
		$ERROR_MSG = "Belum ada aspek penilaian yang tersimpan!"; ?>
<?php 	} ?>
  <tr><td colspan="2" align="center">
  <?php echo$but?>&nbsp;&nbsp;<input class="but" type="button" name="tutup" value="Tutup" onClick="window.close()" />
  </td></tr>
<?php if (strlen($ERROR_MSG) > 0) 
	{ ?>
    <tr>
	<td colspan="2" align="center">
    	<span class="style2"><?php echo$ERROR_MSG?></span>    
    </td>
	</tr>
<?php  } ?>

</table>
	</form>
</body>
</html>
<script language="javascript">
var spryselect1 = new Spry.Widget.ValidationSelect("aspek");
var spryselect2 = new Spry.Widget.ValidationSelect("jenis");
</script>
<?php
CloseDb();
?>