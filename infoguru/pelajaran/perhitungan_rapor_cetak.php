<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php'); 
require_once('../library/dpupdate.php');
require_once("../include/sessionchecker.php");

if(isset($_REQUEST["id_pelajaran"]))
	$id_pelajaran = $_REQUEST["id_pelajaran"];

OpenDb();
$sql = "SELECT j.departemen, j.nama, p.nip, p.nama 
			 FROM guru g, $g_db_pegawai.pegawai p, pelajaran j 
			WHERE g.nip=p.nip AND g.idpelajaran = j.replid AND j.replid = '$id_pelajaran' AND g.nip = '".SI_USER_ID()."'"; 
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$departemen = $row[0];
$pelajaran = $row[1];
$guru = $row[2].' - '.$row[3];
?>
<html>
<head>
<title></title>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<title>Cetak Perhitungan Nilai Rapor</title>
</head>
<body>
<center>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<!-- TABLE CENTER -->
<tr>
    <td align="left" valign="top">
  	 <?php echo GetHeader($departemen)?>
	 <center><font size="4"><strong>ATURAN PERHITUNGAN NILAI RAPOR</strong></font><br /></center><br /><br /><br />
<table>
    <tr>
		<td><strong>Departemen</strong>	</td>
    	<td><strong>: <?php echo$departemen ?></strong></td>
    </tr>
    <tr>
        <td><strong>Pelajaran</strong></td>
    	<td><strong>: <?php echo$pelajaran ?></strong></td>
   	</tr>
    <tr>
        <td><strong>Guru</strong></td>  	
        <td><strong>: <?php echo$guru ?></strong></td>       
	</tr>
    </table>

 <?php
	$sql = "SELECT tingkat,replid FROM tingkat WHERE departemen = '$departemen' ORDER BY urutan";
	$result = QueryDb($sql);
   while ($row_tkt = @mysql_fetch_array($result)) 
	{
		$query_at = "SELECT a.dasarpenilaian, dp.keterangan
		               FROM aturannhb a, tingkat t, dasarpenilaian dp
		 		  	 	  WHERE a.idtingkat='$row_tkt[replid]' AND a.idpelajaran = '$id_pelajaran' AND t.departemen='$departemen' 
	  				       AND t.replid = a.idtingkat AND a.nipguru = '".SI_USER_ID()."'
							 AND a.dasarpenilaian = dp.dasarpenilaian
					  GROUP BY a.dasarpenilaian";
		$result_at = QueryDb($query_at);
      if (@mysql_num_rows($result_at)>0)
		{ ?>
  <br>
  <b>Tingkat <?php echo$row_tkt['tingkat'] ?></b><br /><br />
  <table border="1" width="100%" id="table" class="tab" bordercolor="#000000">
  	<tr>
		<td height="30" align="center" class="header">No</td>
		<td height="30" align="center" class="header">Aspek Penilaian</td>
		<td height="30" align="center" class="header">Bobot Perhitungan Nilai Rapor </td>
	</tr>
	<?php	
	$i=1;
	while($row_at = mysql_fetch_row($result_at))
	{	?>
	<tr>
		<td height="25" align="center"><?php echo$i ?></td>
		<td height="25"><?php echo$row_at[1] ?></td>
		<td height="25">
		<?php
		$query_ju = "SELECT j.jenisujian, a.bobot, a.aktif, a.replid 
		               FROM aturannhb a, tingkat t, jenisujian j 
				 		  WHERE a.idtingkat = '$row_tkt[replid]' AND a.idpelajaran = '$id_pelajaran' AND j.replid = a.idjenisujian 
							 AND t.departemen = '$departemen' AND a.dasarpenilaian = '$row_at[0]' AND t.replid = a.idtingkat 
							 AND a.nipguru = '".SI_USER_ID()."'";
		$result_ju = QueryDb($query_ju);
		while($row_ju = mysql_fetch_row($result_ju))
		{
					echo $row_ju[0]." = ".$row_ju[1]."<br>";
		} ?>		
      </td>
</tr>
<?php		$i++;
	}	?>
	
  </table>
<?php }
}
?>

</td>
</tr>
</table>
<script language="javascript">
window.print();
</script>
</center>
</body>
</html>
<?php
CloseDb();
?>