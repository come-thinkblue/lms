<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/dpupdate.php');
require_once("../include/sessionchecker.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aturan Perhitungan Nilai Rapor[Menu]</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function tampil(id,nip) {
	parent.perhitungan_rapor_content.location.href="perhitungan_rapor_content.php?id_pelajaran="+id+"&nip_guru="+nip;
}

</script>
</head>
<body>
<table border="0" width="100%" align="center">
<!-- TABLE CENTER -->

<?php
OpenDb();
$sql="SELECT DISTINCT pel.departemen FROM pelajaran pel, guru g, departemen  d WHERE g.nip='".SI_USER_ID()."' AND pel.replid=g.idpelajaran AND pel.departemen = d.departemen ORDER BY d.urutan";
$result = QueryDb($sql);
$cnt = 0;
if ((@mysql_num_rows($result))>0){
?>
		
	<?php
	while ($row = @mysql_fetch_array($result)) {
		$departemen=$row[0];
		
	?>
       <tr><td> 
        <table class="tab" id="table" border="1" style="border-collapse:collapse"  width="100%" align="left" bordercolor="#000000">

        <tr>
        	<td class="header" align="center" height="30"><?php echo$departemen?></td></tr>
   	<?php 
		$sql2="SELECT pel.nama,pel.departemen,pel.replid FROM pelajaran pel, guru g WHERE g.nip='".SI_USER_ID()."' AND pel.replid=g.idpelajaran AND pel.departemen='$departemen' GROUP BY pel.nama";
		$result2 = QueryDb($sql2);
		$cnt2 = 0;
		while ($row2 = @mysql_fetch_array($result2)) {
			$nama_pelajaran=$row2[0];
		?>
		<tr>
        	<td align="center" height="25" onclick="tampil('<?php echo$row2[2]?>','<?php echo sI_USER_ID()?>','<?php echo$departemen?>')" style="cursor:pointer"><b><?php echo$nama_pelajaran?></b>           
            </td>
        </tr>
		<?php
			$cnt2++;
        }
		?></table>
		<script language='JavaScript'>
	    Tables('table', 1, 0);
		</script>
		<?php
 	}
	CloseDb();
} else { 
?>
	<table width="100%" border="0" align="center">          
	<tr>
		<td align="center" valign="middle" height="200">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data <br /><br />Tambah data pelajaran yang diajar oleh guru <?php echo$_REQUEST['nama']?> di menu pendataan guru </b></font>
		</td>
	</tr>
	</table> 
<?php } ?> 

<!-- END TABLE CENTER -->    
</table> 
        
</body>
</html>