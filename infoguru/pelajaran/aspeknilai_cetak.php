<?php
/**[N]**
 * LMS SMA Negeri 1 Malang
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2013 SMA Negeri1 Malang
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
require_once('../library/dpupdate.php');
require_once("../include/sessionchecker.php");

OpenDb();
$departemen = "yayasan";
$kurikulum = $_REQUEST['kurikulum'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS SMAN 1 MALANG[Cetak Aspek Penilaian]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center><font size="4"><strong>DATA ASPEK PENILAIAN</strong></font><br /> </center><br /><br />

<br />

	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="15%" class="header" align="center">Kode</td>
        <td width="*" class="header" align="center">Aspek Penilaian</td>
    </tr>
<?php 	OpenDb();
	$sql = "SELECT dasarpenilaian, keterangan FROM dasarpenilaian WHERE idkurikulum='$kurikulum'";    
	$result = QueryDB($sql);
	$cnt = 0;
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">
    	<td align="center"><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row['dasarpenilaian'] ?></td>
        <td><?php echo $row['keterangan'] ?></td>
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>

</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>