<?php
/**[N]**
 * LMS MAN Kota Blitar
 * @version: 1.0 (January 09, 2013)
 * Copyright (C)2016

 * **[N]**/ ?>
<?php

include('../charts/FusionCharts.php');	

require_once('include/errorhandler.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('include/common.php');
require_once('include/config.php');


OpenDb();

include('menu.php');




?>
<html>
<head>
<title>Untitled-1</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="script/tooltips.js"></script>
<script type="text/javascript">
function get_fresh(){
	document.location.reload();
}
function change_theme(theme){
	parent.topcenter.location.href="topcenter.php?theme="+theme;
	parent.topleft.location.href="topleft.php?theme="+theme;
	parent.topright.location.href="topright.php?theme="+theme;
	parent.midleft.location.href="midleft.php?theme="+theme;
	get_fresh();
	parent.midright.location.href="midright.php?theme="+theme;
	parent.bottomleft.location.href="bottomleft.php?theme="+theme;
	parent.bottomcenter.location.href="bottomcenter.php?theme="+theme;
	parent.bottomright.location.href="bottomright.php?theme="+theme;
}
</script>
</head>
<body >
<!-- ImageReady Slices (Untitled-1) -->
<div id="content"> 
<div class="wrapper">

<?php 
$que1="SELECT Count(siswa.nis) as siswa FROM siswa";
$result1=mysql_query($que1);
$row1 = mysql_fetch_array($result1);

$que2="SELECT bagian, Count(pegawai.nip) as jumlah FROM $g_db_pegawai.pegawai GROUP BY pegawai.bagian";
$result2=mysql_query($que2);
$jumrows1=mysql_num_rows($result2);


$data ="<chart showvalues='1' caption='' numberprefix='$' canvasBorderColor='1D8BD1' canvasBorderAlpha='60'
 showlegend='1' enablesmartlabels='1' showlabels='0' showpercentvalues='1' logoURL='kecil.png'>";

$data .= "<set label='Jumlah Siswa' value='" . $row1['siswa'] . "' />";

if ($jumrows1>0) {
				$no1=0;
while ($row2 = mysql_fetch_array($result2)) {
					$data .= "<set label='" . $row2['bagian']."' value='" . $row2['jumlah'] . "' />";
					$no1++;
				}
				$data.="</chart>";
              }
?>
!-- 4 + 4 + 4 -->
        <div class="fluid">
            <div class="widget gridWelcome">
              
                <div class="body">
				 <?php $user=$nama;?>

                <h1 style="color:#128f97; font-weight:800;">Selamat Datang </h1>
				
                <h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;"><?php echo $user;?> </br></h1>
                <p>Sebelum Melakukan otomasi data, anda perlu melakukan setting terhadap sistem agar kinerja sistem dapat berjalan dengan baik. Anda dapat memng-klik satu-persatu tombol dibawah ini untuk menyetting data awal sistem.</p></div>
                
        <ul class="middleNavA">
            <li><a href="referensi/pegawai.php" title="Setting Pegawai"><img src="css/icon bawah/pegawai.png" alt="" /><span>RPP</span></a></li>
            <li><a href="referensi/tahunajaran.php" title="Setting Tahun Ajaran"><img src="css/icon bawah/tahunAjaran.png" alt="" /><p>Aspek</p><p>Penilaian</p></a></li>
            <li><a href="referensi/tingkat.php" title="Setting Tingkat"><img src="css/icon bawah/tingkat.png" alt="" /><p>Jenis</p><p>Pengujian</p></a></li>
            <li><a href="referensi/angkatan.php" title="Setting Angkatan"><img src="css/icon bawah/angkatan.png" alt="" /><p>Pemberian</p><p>Grading</p></a></li>
            <li><a href="referensi/kelas.php" title="Setting Kelas"><img src="css/icon bawah/kelas.png" alt="" /><p>Perhitungan</p><p>Nilai Rapor</p></a></li>
           
        </ul>
            </div>
            <div class="widget grid5">
                <div class="whead"><h6>Grafik Data Perbandingan Jumlah Civitas Akademika</h6><div class="clear"></div></div>
                <div class="body">
                <?php echo renderChartHTML("../charts/Pie3D.swf", "", $data, "tes", "100%", "300", false);?>
                </div>
            </div>
            
    <?php
function jumlahData($count, $from){
$quePeg="SELECT Count(".$count.") as jumlah FROM ".$from."";
$resultPeg=mysql_query($quePeg);
$jumPeg=mysql_fetch_array($resultPeg);
$jumlah= $jumPeg['jumlah'];
return $jumlah;
}
	?>        
            
            <div class="widget grid3">
                <div class="whead"><h6>Tabel Data</h6><div class="clear"></div></div>
                <div class="body">
                <table cellpadding="0" cellspacing="0" width="100%" class="tAlt wGeneral">
                        <thead>
                            <tr>
                                <td width="60%">Jenis </td>
                                <td>Jumlah Data</td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td ><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/pegawai.png" width="40" height="40"alt="" /><span>Pegawai</span></a></td>
                               <?php $jumlahPegawai = jumlahData("pegawai.nip","$g_db_pegawai.pegawai"); ?>  <td align="center"><strong class="income"><?php echo $jumlahPegawai;?></strong></td>
                            </tr>
                            <tr>
                              <?php $jumlahTahunAjaran = jumlahData("tahunajaran","tahunajaran"); ?>    <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/tahunAjaran.png" width="40" height="40"alt="" /><span>Tahun Ajaran</span></a></td>
                                <td align="center"><strong class="income"><?php echo $jumlahTahunAjaran;?></strong></td>
                                
                            </tr>
                            <tr>
<?php $jumlah = jumlahData("tingkat","tingkat"); ?> <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/tingkat.png" width="40" height="40"alt="" /><span>Tingkat</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?> </strong></td>
                               
                            </tr>
                            <tr>
<?php $jumlah = jumlahData("angkatan","angkatan"); ?> <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/angkatan.png" width="40" height="40"alt="" /><span>Angkatan</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?></strong></td>
                               
                            </tr>
                            
                             <tr>
<?php $jumlah = jumlahData("kelas","kelas"); ?><td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/kelas.png" width="40" height="40"alt="" /><span>Kelas</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?></strong></td>
                               
                            </tr>
                            
                             <tr>
<?php $jumlah = jumlahData("semester","semester"); ?> <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/kalender.png" width="40" height="40"alt="" /><span>Semester</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?></strong></td>
                               
                            </tr>
                        </tbody>
                    </table>            
                
                </div>
            </div>
        </div>


</div>


</div>
<!-- End ImageReady Slices -->
</body>
</html>