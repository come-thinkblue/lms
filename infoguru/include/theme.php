<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('sessioninfo.php');
function GetThemeDir() {
	// Change this variable with user's SESSION theme
	//$theme = SI_USER_THEME();
	$theme = 2;
	if ($theme == 1) {
		return "images/theme/red/";
	} elseif ($theme == 2) {
		return "images/theme/blue/";
	} elseif ($theme == 3) {
		return "images/theme/black/";
	} elseif ($theme == 4) {
		return "images/theme/green/";
	} elseif ($theme == 5) {
		return "images/theme/lavender/";
	} elseif ($theme == 6) {
		return "images/theme/blue_sea/";
	} elseif ($theme == 7) {
		return "images/theme/greenpadi/";
	} elseif ($theme == 8) {
		return "images/theme/reddot/";
	} elseif ($theme == 9) {
		return "images/theme/sunset/";
	} elseif ($theme == 10) {
		return "images/theme/orange/";
	} elseif ($theme == 11) {
		return "images/theme/dirt/";
	} elseif ($theme == 12) {
		return "images/theme/purple/";
	}
	
}

function GetThemeDir2() 
{
	return "";	
}
?>