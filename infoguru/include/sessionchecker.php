<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ 
session_name("guru");
if(!isset($_SESSION)){ session_start();}
if (!isset($_SESSION['login']))
{
	if (file_exists("index.php")) 
	   $addr = "index.php";
	else if (file_exists("../index.php")) 
	   $addr = "../index.php";
	elseif(file_exists("../../index.php")) 
	   $addr = "../../index.php";
	else	
	   $addr = "../../../index.php"; ?>
	   
	<script language="javascript">
		if (self != self.top)
		{
			top.window.location.href='<?php echo$addr?>';
		}
		else if (self.name != "")
		{
			window.close();
			opener.top.window.location.href='<?php echo$addr?>';
		}
		else
		{
			window.location.href='<?php echo$addr?>';	
		}
	</script>
<?php	exit();
}  ?>