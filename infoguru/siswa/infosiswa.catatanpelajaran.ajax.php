<?php
/**[N]**
MAN Kota Blitar
 **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$nis="";
$pelajaran="";
$nis=$_REQUEST[nis];
$pelajaran=$_REQUEST[pelajaran];

$bulan_pjg = array(1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

OpenDb();
$sql_pp="SELECT pel.nama as namapelajaran, ppsiswa.statushadir as statushadir, pp.tanggal as tanggal, pp.jam as jam, pp.gurupelajaran as guru,ppsiswa.catatan as catatan FROM $g_db_akademik.pelajaran pel, $g_db_akademik.presensipelajaran pp, $g_db_akademik.ppsiswa ppsiswa WHERE ppsiswa.nis='$nis' AND ppsiswa.idpp=pp.replid AND pel.replid=pp.idpelajaran AND ppsiswa.catatan<>'' AND pp.idpelajaran='$pelajaran'";
$res_pp=QueryDb($sql_pp);
?>
<table width="100%" border="1" cellspacing="0" class="tab">
<tr class="header" height="30">
    <td width="4%" align="center">No.</td>
    <td width="5%" align="center">Status</td>
    <td width="25%" align="center">Tanggal-Jam</td>
    <td width="38%" align="center">Guru</td>
</tr>
<?php
if (@mysql_num_rows($res_pp)>0)
{
    $cnt=1;
	while ($row_pp=@mysql_fetch_array($res_pp))
    {
    	$a="";
    	if ($cnt%2==0)
    		$a="style='background-color:#FFFFCC'"; ?>
  <tr height="25" <?php echo$a?> >
    <td align="center" rowspan="2"><?php echo$cnt?></td>
    <td align="center">
	<?php
	switch ($row_pp[statushadir]){
	case 0:
		echo "Hadir";
		break;
	case 1:
		echo "Sakit";
		break;
	case 2:
		echo "Ijin";
		break;
	case 3:
		echo "Alpa";
		break;
	case 4:
		echo "Cuti";
		break;
	}
	?>
	</td>
    <td><?php echo ShortDateFormat($row_pp[tanggal])."-".$row_pp[jam]?></td>
    <td>[<?php echo$row_pp[guru]?>]&nbsp;
	<?php
	$res_gr=QueryDb("SELECT nama FROM $g_db_pegawai.pegawai WHERE nip='$row_pp[guru]'");
	$row_gr=@mysql_fetch_array($res_gr);
	echo $row_gr[nama];
	?>
	</td>
    </tr>
    <tr <?php echo$a?>>
    <td colspan="3"><?php echo$row_pp[catatan]?></td>
  </tr>
  <?php
  $cnt++;
  } } else { ?>
  ?>
  <tr>
    <td align="center" colspan="5">Tidak ada Catatan</td>
  </tr>
  <?php
  } ?>
</table>
<?php
CloseDb();
?>