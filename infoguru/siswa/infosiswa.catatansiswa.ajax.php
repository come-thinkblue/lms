<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/sessionchecker.php');
require_once('../include/errorhandler.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');

$nis = "";
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
    
$bulan = "";
if (isset($_REQUEST['bulan']))
	$bulan = $_REQUEST['bulan'];
    
$tahun = "";
if (isset($_REQUEST['tahun']))
	$tahun = $_REQUEST['tahun'];
    
OpenDb();
$sql = "SELECT c.replid as replid,c.judul as judul, c.catatan as catatan, c.nip as nip, p.nama as nama, c.tanggal as tanggal 
          FROM $g_db_umum.catatansiswa c, $g_db_pegawai.pegawai p 
	     WHERE c.nis='$nis' AND MONTH(c.tanggal)='$bulan' AND YEAR(c.tanggal)='$tahun' AND p.nip=c.nip ";
$result=QueryDb($sql);
$num=@mysql_num_rows($result);
?>
<table width="100%" border="1" cellspacing="0" class="tab">
<tr>
    <td height="30" width="43" class="header"><div align="center">No</div></td>
    <td height="30" width="900" class="header">Tanggal/Guru</td>
    </tr>
<?php
if ($num>0)
{
  	$cnt=1;
	while ($row=@mysql_fetch_array($result))
    {
    	$a="";
    	if ($cnt%2==0)
    		$a="style='background-color:#FFFFCC'";  ?>
            
        <tr <?php echo$a?>>
            <td height="25" valign="top" rowspan="2">
                <div align="center"><?php echo$cnt?></div>
            </td>
            <td height="25" valign="top">
                <?php echo ShortDateFormat($row[tanggal])?><br /><?php echo$row[nip]?>-<?php echo$row[nama]?>
            </td>
        </tr>
        <tr <?php echo$a?>>
            <td height="25">
                <font face="Verdana, Arial, Helvetica, sans-serif" color="#999999">
                    [<?php echo$row[judul]?>]
                </font><br /><?php echo$row[catatan]?>
            </td>
<?php      $cnt++;
  	}
}
else
{ ?>
<tr>
    <td height="25" colspan="4">
        <div align="center"><em>Tidak ada catatan Kejadian Siswa untuk NIS : <?php echo$nis?></em></div>
    </td>
</tr>
<?php
}
?>
</table>
<?php
CloseDb();
?>