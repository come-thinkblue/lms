<?php
	require_once('../include/sessionchecker.php');
	require_once('../include/config.php');
	require_once('../include/getheader.php');
	require_once('../include/db_functions.php');
	require_once('../include/common.php');

	$nis = $_SESSION["infosiswa.nis"];
	if (isset($_REQUEST['nis']))
		$nis = $_REQUEST['nis'];
		
	OpenDb();

	$res_nm_sis=QueryDb("SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'");
	$row_nm_sis=@mysql_fetch_array($res_nm_sis);
	
	$sql_ekskul = QueryDb("SELECT * FROM ekskul_pengurus WHERE nis='$nis'");
?>
	<style type="text/css">
	<!--
	.style1 {
		color: #666666;
		font-weight: bold;
	}
	-->
	</style>
	<table width="100%" border="0" cellspacing="0">
		<tr>
			<td>
				<fieldset>
					<legend class="style1">Ekstra Kurikuler</legend>
					<table width="100%" border="0" cellspacing="0">
						<tr>
							<td width="11%"><strong>NIS</strong></td>
							<td width="1%"><strong>:</strong></td>
							<td width="88%"><?php echo$nis?></td>
						</tr>
						<tr>
							<td><strong>Nama Siswa</strong></td>
							<td><strong>:</strong></td>
							<td><?php echo$row_nm_sis[nama]?></td>
						</tr>
					</table>	
				</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="1" cellspacing="0" class="tab">
					<tr class="header" height="30">
						<td width="3%" align="center">No.</td>
						<td width="20%" >Nama Ekskul</td>
						<td width="15%" >Nama Jabatan</td>
						<td width="15%" >Awal Kepengurusan</td>
						<td width="15%" >Periode</td>
						<td width="*" >Keterangan</td>
					</tr>
					<?php
					if (@mysql_num_rows($sql_ekskul)>0){
						$cnt=1;
						while ($row=@mysql_fetch_array($sql_ekskul)){
							$a="";
							if ($cnt%2==0)
								$a="style='background-color:#FFFFCC'";
							?>
							<?php
							$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul = '$row[2]'";
							$rs_eks = QueryDb($sql_eks);
							$row_eks = mysql_fetch_row($rs_eks);
							
							$sql_jab = "SELECT nama_jabatan FROM ekskul_struktur WHERE id_jabatan = '$row[3]'";
							$rs_jab = QueryDb($sql_jab);
							$row_jab = mysql_fetch_row($rs_jab);
							?>
							<tr height="25" <?php echo$a?>>
								<td align="center"><?php echo$cnt?></td>
								<td><?php echo$row_eks[0]?></td>
								<td><?php echo$row_jab[0]?></td>
								<td><?php echo$row[4]?></td>
								<td><?php echo$row[5]?></td>
								<td><?php echo$row[6]?></td>
							</tr>
							<?php
							$cnt++;
						}
					} 
					else {
						?>
						<tr height="25">
							<td align="center" colspan="6">Tidak ada keterangan ekstra kurikuler untuk siswa tsb.</td>
						</tr>
						<?php 
					} 
					?>
				</table>
			</td>
		</tr>
	</table>
<?php CloseDb() ?>