<?php
	require_once('../include/sessionchecker.php');
	require_once('../include/config.php');
	require_once('../include/getheader.php');
	require_once('../include/db_functions.php');
	require_once('../include/common.php');

	$nis = $_SESSION["infosiswa.nis"];
	if (isset($_REQUEST['nis']))
		$nis = $_REQUEST['nis'];
		
	OpenDb();

	$res_nm_sis=QueryDb("SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'");
	$row_nm_sis=@mysql_fetch_array($res_nm_sis);
	
	$sql_ekskul = QueryDb("SELECT * FROM penghargaan WHERE nis='$nis'");
	
	$tglawal = "";
	if (isset($_REQUEST['tglawal'])){
		$tglawl = explode('-',$_REQUEST['tglawal']);
		$tglawal = $tglawl[2]."-".$tglawl[1]."-".$tglawl[0];
		
	}
	$tglakhir = "";
	if (isset($_REQUEST['tglakhir'])){
		$tglakhr =  explode('-',$_REQUEST['tglakhir']);
		$tglakhir = $tglakhr[2]."-".$tglakhr[1]."-".$tglakhr[0];
		
	}
	$sql="SELECT ph.tanggal1,ph.tanggal2,phsiswa.keterangan FROM $g_db_akademik.presensiharian ph, $g_db_akademik.phsiswa phsiswa WHERE phsiswa.nis='$nis' AND phsiswa.idpresensi=ph.replid AND ph.tanggal1>='$tglawal' AND ph.tanggal2<='$tglakhir' AND phsiswa.keterangan<>''";
	$result=QueryDb($sql);
?>
	<style type="text/css">
	<!--
	.style1 {
		color: #666666;
		font-weight: bold;
	}
	-->
	</style>
	<table width="100%" border="0" cellspacing="0">
		<tr>
			<td>
				<fieldset>
					<legend class="style1">Penghargaan</legend>
					<table width="100%" border="0" cellspacing="0">
						<tr>
							<td width="11%"><strong>NIS</strong></td>
							<td width="1%"><strong>:</strong></td>
							<td width="88%"><?php echo$nis?></td>
						</tr>
						<tr>
							<td><strong>Nama Siswa</strong></td>
							<td><strong>:</strong></td>
							<td><?php echo$row_nm_sis[nama]?></td>
						</tr>
					</table>	
				</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="1" cellspacing="0" class="tab">
					<tr class="header" height="30">
						<td width="3%" align="center">No.</td>
						<td width="*" >Nama Penghargaan</td>
						<td width="*" >Bentuk Penghargaan</td>
						<td width="*" >Keterangan Penghargaan</td>
						<td width="*" >Kategori</td>
						<td width="*" >Tahun Ajaran</td>
						<td width="*" >Semester</td>
						<td width="*" >Waktu</td>
						<td width="*" >Keterangan</td>
					</tr>
					<?php
					if (@mysql_num_rows($sql_ekskul)>0){
						$cnt=1;
						while ($row=@mysql_fetch_array($sql_ekskul)){
							$a="";
							if ($cnt%2==0)
								$a="style='background-color:#FFFFCC'";
							?>
							<?php
							$sql_kat = "SELECT nama_kategori FROM kategori_penghargaan WHERE id_kategori_penghargaan = '$row[5]'";
							$rs_kat = QueryDb($sql_kat);
							$row_kat = mysql_fetch_row($rs_kat);
							?>
							<tr height="25" <?php echo$a?>>
								<td align="center"><?php echo$cnt?></td>
								<td><?php echo$row[2]?></td>
								<td><?php echo$row[3]?></td>
								<td><?php echo$row[4]?></td>
								<td><?php echo$row_kat[0]?></td>
								<td><?php echo$row[6]?></td>
								<td><?php echo$row[7]?></td>
								<td><?php echo$row[8]?></td>
								<td><?php echo$row[9]?></td>
							</tr>
							<?php
							$cnt++;
						}
					} 
					else {
						?>
						<tr height="25">
							<td align="center" colspan="9">Tidak ada keterangan penghargaan untuk siswa tsb.</td>
						</tr>
						<?php 
					} 
					?>
				</table>
			</td>
		</tr>
	</table>
<?php CloseDb() ?>