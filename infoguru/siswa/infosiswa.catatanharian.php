<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/sessionchecker.php');
	require_once('../include/config.php');
	require_once('../include/getheader.php');
	require_once('../include/db_functions.php');
	require_once('../include/common.php');

	$nis = $_SESSION["infosiswa.nis"];
	$bulan_pjg = array(1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

	OpenDb();
	$sql_thn = "SELECT ph.tanggal1,ph.tanggal2,phsiswa.keterangan
				  FROM $g_db_akademik.phsiswa phsiswa, $g_db_akademik.presensiharian ph
				 WHERE phsiswa.nis='$nis' AND phsiswa.idpresensi=ph.replid
				 GROUP BY YEAR(tanggal1)";
	$res_thn = QueryDb($sql_thn);

	$s = "SELECT DATE(now())";
	$re = QueryDb($s);
	$r = @mysql_fetch_row($re);
	$d = explode("-", $r[0]);
	$now = $d[2]."-".$d[1]."-".$d[0];
	if ($d[1]==1)
		$y=12;
	else
		$y=$d[1]-1;
	if (strlen($y)==1)
		$y="0".$y;
	$ytd = $d[2]."-".$y."-".$d[0];
	CloseDb();
?>
	<form name="panel5">
		<table width="100%" border="0" cellspacing="5">
			<tr>
				<td valign="top">
					<fieldset><legend>Periode</legend>
						<table width="100%" border="0" cellspacing="0">
							<tr>
								<td width="50%">
									<input type="hidden" id="niscthar" name="niscthar" value="<?php echo$nis?>">  
									<input title="Klik untuk membuka kalendar!" type="text" size="10" id="tglawal" readonly onClick="showCal('CalendarISCATPHAR1')"  value="<?php echo$ytd?>"/>
									&nbsp;<img src="../images/ico/calendar.png" name="btnawal" id="btnawal" title="Klik untuk membuka kalendar!" onClick="showCal('CalendarISCATPHAR1')"/>
									&nbsp;s.d.&nbsp;
									<input title="Klik untuk membuka kalendar!" type="text" size="10" id="tglakhir" readonly onclick="showCal('CalendarISCATPHAR2')" value="<?php echo$now?>"/>
									&nbsp;<img src="../images/ico/calendar.png" id="btnakhir" onClick="showCal('CalendarISCATPHAR2')" title="Klik untuk membuka kalendar!"/>
								</td>
								<td width="50%">
									<img title="Klik untuk menampilkan Presensi Harian" style="cursor:pointer;" src="../images/view.png" width="32" height="32" onclick="ShowCatatanHarian()" />
								</td>
							</tr>
							<tr>

							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td  valign="top"><div id="contentph"></div></td>
			</tr>
		</table>
	</form>