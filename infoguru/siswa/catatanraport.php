<?php
	require_once('../include/sessionchecker.php');
	require_once('../include/config.php');
	require_once('../include/getheader.php');
	require_once('../include/db_functions.php');
	require_once('../include/common.php');
	include('../menu.php');
	//include('menu.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Catatan Raport</title>
		<link href="../style/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="../style/calendar-win2k-1.css">
		<script type="text/javascript" src="../script/calendar.js"></script>
		<script type="text/javascript" src="../script/lang/calendar-en.js"></script>
		<script type="text/javascript" src="../script/calendar-setup.js"></script>
		<script language="javascript" type="text/javascript" src="../script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
		<script src="../script/SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
		<link href="../script/SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
		<script language="javascript" type="text/javascript">
			tinyMCE.init({
					mode : "textareas",
					theme : "simple"
				});

			function validate(){
				var judul=document.getElementById('judul').value;
				var kategori=document.getElementById('kategori').value;
				var catatan=tinyMCE.get('catatan').getContent();
				if (judul.length==0){
					alert ('Anda harus mengisikan data untuk Judul Catatan');
					document.getElementById('judul').focus();
					return false;
				}
				if (kategori.length==0){
					alert ('Anda harus mengisikan data untuk Kategori Catatan');
					document.getElementById('kategori').focus();
					return false;
				}
				if (catatan.length==0){
					alert ('Anda harus mengisikan data untuk Catatan');
					document.getElementById('catatan').focus();
					return false;
				}
				return true;
			}
		</script>
	</head>
	<body>
		<?php
		$jumlah = "";
		
		$nip =  $_SESSION['login'];
		OpenDb();
		$sql_get = "SELECT a.replid, b.nis, b.nama FROM kelas a, siswa b WHERE a.replid=b.idkelas AND nipwali=$nip";
		$rs_get = QueryDb($sql_get);
		//echo "$sql_get";
		CloseDb();
		?>
		</br></br></br></br>
		<table width='100%' align='center' cellspacing='0' border='0'>
		<tr>
			<td width='40%'>
				<table width='95%' align='center' cellspacing='0' border='1' class='tab'>
					<tr>
						<td width='10' height='25' align='center' class='header'>No.</td>
						<td width='10' height='25' align='center' class='header'>NIS</td>
						<td width='10' height='25' align='center' class='header'>NAMA</td>
						<td width='10' height='25' align='center' class='header'> &nbsp; </td>
					</tr>
					<tr>
						<?php
						if(mysql_num_rows($rs_get) > 0){
							$no = 1;
							while($row_get = mysql_fetch_row($rs_get)){
								?>
								<td><?php echo$no?></td>
								<td><?php echo$row_get[1]?></td>
								<td><?php echo$row_get[2]?></td>
								<td align='center'><input type='button' name='cek' value=' > ' /></td>
								<?php
								$no++
								
					?>
					</tr>
					<?php
							}
							$jumlah = $no;
						}
					?>
				</table>
			</td>
			<td align='top'>
				<table width='90%' cellspacing="5" border='0' class='tab'>
					<tr>
						<td><strong>Tanggal</strong></td>
						<td><input title="Klik untuk membuka kalender !" type="text" name="tanggal" id="tanggal" size="25" readonly="readonly" class="disabled" /><img title="Klik untuk membuka kalender !" src="../images/ico/calendar_1.png" name="btntanggal" width="16" height="16" border="0" id="btntanggal"/></td>
					</tr>
					<tr>
						<td><strong>Judul </strong></td>
						<td><input name="judul" type="text" id="judul" size="35" maxlength="254" /></td>
					</tr>
					<tr>
						<td colspan="3" align="left">
							<strong>Catatan </strong>
							<div align="center"><br />
								<textarea name="catatan" rows="25" id="catatan" style="width:100%"></textarea>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
</html>
<script type="text/javascript">
	Calendar.setup({
		inputField  : "tanggal",         
		ifFormat    : "%d-%m-%Y",  
		button      : "btntanggal"    
	});
	Calendar.setup({
		inputField  : "tanggal",      
		ifFormat    : "%d-%m-%Y",   
		button      : "tanggal"     
	});

</script>
<script language="javascript">
	var sprytextfield = new Spry.Widget.ValidationTextField("judul");
	var spryselect = new Spry.Widget.ValidationSelect("catatan");
</script>