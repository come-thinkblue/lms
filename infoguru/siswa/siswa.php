<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/config.php');
require_once('../include/getheader.php');
require_once('../include/common.php');
require_once('../include/sessionchecker.php');
require_once('../include/db_functions.php');
require_once('siswa.class.php');

OpenDb();
include('../menu.php');
//include('menu.php');
$S = new CSiswa();
$S->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Info Siswa</title>
	<script src="../script/ajax.js" type="text/javascript"></script>
	<script src="siswaui.js" type="text/javascript"></script>
	<script src="infosiswa.js" type="text/javascript"></script>
	<script src="../script/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
	<link href="../script/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">
	<script src="../script/SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
	<link href="../script/SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
	<script src="../script/SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
	<link href="../script/SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
	<link href="../style/style.css" rel="stylesheet" type="text/css" />
	<script language="javascript" src="../script/cal2.js"></script>
	<script language="javascript" src="../script/cal_conf3.js"></script>
	<link rel="stylesheet" type="text/css" href="../style/calendar-win2k-1.css">
	<script type="text/javascript" src="../script/calendar.js"></script>
	<script type="text/javascript" src="../script/lang/calendar-en.js"></script>
	<script type="text/javascript" src="../script/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="../script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
			tinyMCE.init({
					mode : "textareas",
					theme : "simple"
				});

			function validate(){
				var judul=document.getElementById('judul').value;
				var kategori=document.getElementById('kategori').value;
				var catatan=tinyMCE.get('catatan').getContent();
				if (judul.length==0){
					alert ('Anda harus mengisikan data untuk Judul Catatan');
					document.getElementById('judul').focus();
					return false;
				}
				if (kategori.length==0){
					alert ('Anda harus mengisikan data untuk Kategori Catatan');
					document.getElementById('kategori').focus();
					return false;
				}
				if (catatan.length==0){
					alert ('Anda harus mengisikan data untuk Catatan');
					document.getElementById('catatan').focus();
					return false;
				}
				return true;
			}
		</script>
</head> 
<body>
<div id="waitBox" style="position:absolute; visibility:hidden;">
	<img src="../img/loading2.gif" border="0" />&nbsp;<span class="tab2">Please&nbsp;wait...</span>
</div>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="250" align="left" valign="top"><div id="list" style=" width:350px">
		<?$S->ShowStudentList();?>
	</td>
    <td width="*" align="left" valign="top">
		<div id="content"></div>
	</td>
  </tr>
</table>
</body>
</html>