<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/sessionchecker.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
require_once('../include/db_functions.php');
require_once('../include/rupiah.php');

$nis = $_SESSION["infosiswa.nis"];

$tahun = 0;
if (isset($_REQUEST['tahun']))
  $tahun = $_REQUEST['tahun'];
?>

<form name='panelperpus'>
<?php  
OpenDb();

$sql = "SELECT DISTINCT YEAR(p.tglpinjam)
	      FROM $g_db_perpustakaan.pinjam p
		 WHERE p.idanggota = '$nis'
		 ORDER BY YEAR(p.tglpinjam) DESC";
$result = QueryDb($sql);
$ntahun = mysql_num_rows($result);

if ($ntahun == 0)
{
  CloseDb();
  echo "Belum ada data peminjaman<br>";
  exit();
}

echo "Tahun: <select name='tahun' class='cmbfrm' id='tahun' style='width:150px' onChange=\"ChangePerpusOption('tahun')\">";
while($row = mysql_fetch_row($result))
{
  if ($tahun == 0)
	$tahun = $row[0];
  echo "<option value='$row[0]' " . IntIsSelected($tahun, $row[0]) . " > " . $row[0] . "</option>";
} 
echo "</select>";

$sql = "SELECT pu.judul, DATE_FORMAT(p.tglpinjam, '%d-%b-%Y'), pu.replid,
			   DATE_FORMAT(p.tglkembali, '%d-%b-%Y'), p.status,
			   DATE_FORMAT(p.tglditerima, '%d-%b-%Y')
	      FROM $g_db_perpustakaan.pinjam p, $g_db_perpustakaan.daftarpustaka d, $g_db_perpustakaan.pustaka pu
		 WHERE p.idanggota = '$nis'
		   AND YEAR(p.tglpinjam) = '$tahun'
		   AND p.kodepustaka=d.kodepustaka
		   AND d.pustaka=pu.replid
		   AND p.status <> '0'
		 ORDER BY p.tglpinjam DESC";
$result = QueryDb($sql);
$cnt=1;
?>
<div style="height:15px">&nbsp;</div>
<table width="100%" border="1" cellspacing="0" cellpadding="0" class="tab">
  <tr>
    <td height="25" align="center" class="header">No</td>
    <td height="25" width='350' align="center" class="header">Judul Pustaka</td>
    <td height="25" align="center" class="header">Tgl Pinjam</td>
    <td height="25" align="center" class="header">Tgl Kembali</td>
    <td align="center" class="header">Status</td>
  </tr>
  <?php if (@mysql_num_rows($result)>0) { ?>
  <?php while ($row = @mysql_fetch_row($result)) { ?>
  <tr>
    <td height="20" align="center"><?php echo$cnt?></td>
    <td height="20"><div style="padding-left:5px;"><?php echo$row[0]?></div></td>
    <td height="20" align="center"><?php echo$row[1]?></td>
    <td height="20" align="center"><?php echo$row[3]?></td>
    <td align="center">
	<?php
    if ($row[4]=='1')
		echo "Belum dikembalikan";
	elseif ($row[4]=='2')
		echo "Sudah dikembalikan pada<br>".$row[5];		
	?>
    </td>
  </tr>
  <?php $cnt++; ?>
  <?php } ?>
  <?php } else { ?>
  <tr>
    <td colspan="5" height="20" align="center">Belum ada data peminjaman</td>
  </tr>
  <?php } ?>
</table>
<?php
CloseDb();
?>
</form>