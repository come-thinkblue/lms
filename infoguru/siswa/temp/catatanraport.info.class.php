<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
class CCatRepInfo
{
	private $nis;
	private $nama;
	private $reporttype;
    
    public function __construct()
    {
		if (isset($_REQUEST["nis"]))
		{
			$_SESSION["infocatatan.nis"] = $_REQUEST["nis"];
			$_SESSION["infocatatan.name"] = $this->getSiswaName($_REQUEST["nis"]);	
		}
		
		if (isset($_REQUEST['reporttype']))
		{
			$_SESSION['infocatatan.reporttype'] = $_REQUEST['reporttype'];
		}
		else
		{
			if (!isset($_SESSION['infocatatan.reporttype']))
				$_SESSION['infocatatan.reporttype'] = "PROFIL";
		}
		
		$this->nis = $_SESSION['infocatatan.nis'];
        $this->nama = $_SESSION['infocatatan.name'];
		$this->reporttype = $_SESSION['infocatatan.reporttype'];
    }
	
	public function ShowIdentity()
	{
		echo "<font style='font-size:17px; font-weight:bold; color:#666;'>";
        echo $this->nis;
        echo " - ";
        echo $this->nama;
        echo "</font>";
	}
    
    public function ShowReportComboBox()
    {
        echo "<br><br>";
        echo "Laporan : ";
        echo "<select id='reporttype' name='reporttype' onchange='GetReportContent()'>";
        echo "<option value='CATRAP' " . StringIsSelected($this->reporttype, "CATRAP") . ">Catatan Raport</option>";
        /*echo "<option value='PRESENSIHARIAN' " . StringIsSelected($this->reporttype, "PRESENSIHARIAN") . ">Presensi Harian</option>";
		echo "<option value='PRESENSIPELAJARAN' " . StringIsSelected($this->reporttype, "PRESENSIPELAJARAN") . ">Presensi Pelajaran</option>";
        echo "<option value='NILAI' " . StringIsSelected($this->reporttype, "NILAI") . ">Nilai</option>";
		echo "<option value='RAPOR' " . StringIsSelected($this->reporttype, "RAPOR") . ">Rapor</option>";
		echo "<option value='PERPUSTAKAAN' " . StringIsSelected($this->reporttype, "PERPUSTAKAAN") . ">Perpustakaan</option>";
		echo "<option value='CATSISWA' " . StringIsSelected($this->reporttype, "CATSISWA") . ">Catatan Siswa</option>";
		echo "<option value='CATPPEL' " . StringIsSelected($this->reporttype, "CATPPEL") . ">Catatan Presensi Pelajaran</option>";
		echo "<option value='CATPHAR' " . StringIsSelected($this->reporttype, "CATPHAR") . ">Catatan Presensi Harian</option>";
		echo "<option value='CATPEL' " . StringIsSelected($this->reporttype, "CATPEL") . ">Catatan Pelanggaran</option>";
		echo "<option value='EKSKUL' " . StringIsSelected($this->reporttype, "EKSKUL") . ">Ekstra Kurikuler</option>";
		echo "<option value='CATPRES' " . StringIsSelected($this->reporttype, "CATPRES") . ">Catatan Prestasi</option>";
		echo "<option value='CATPENG' " . StringIsSelected($this->reporttype, "CATPENG") . ">Catatan Penghargaan</option>"; */
        echo "</select>";
    }
    
    private function getSiswaName($nis)
	{
		$sql = "SELECT nama FROM siswa WHERE nis = '$nis'";
		$result = QueryDb($sql);
		$row = @mysql_fetch_array($result);
		return $row['nama'];
	}
	
	public function ShowReportContent()
	{
		if ($this->reporttype == "CATRAP")
			require_once("catatanraport.value.php");
		/* elseif ($this->reporttype == "PRESENSIHARIAN")
			require_once("infosiswa.presensiharian.php");
		elseif ($this->reporttype == "PRESENSIPELAJARAN")
			require_once("infosiswa.presensipelajaran.php");
		elseif ($this->reporttype == "NILAI")
			require_once("infosiswa.nilai.php");
		elseif ($this->reporttype == "RAPOR")
			require_once("infosiswa.rapor.php");
		elseif ($this->reporttype == "PERPUSTAKAAN")
			require_once("infosiswa.perpustakaan.php");
		elseif ($this->reporttype == "CATSISWA")
			require_once("infosiswa.catatansiswa.php");
		elseif ($this->reporttype == "CATPPEL")
			require_once("infosiswa.catatanpelajaran.php");
		elseif ($this->reporttype == "CATPHAR")
			require_once("infosiswa.catatanharian.php");
		elseif ($this->reporttype == "CATPEL")
			require_once("infosiswa.catatanpelanggaran.php");
		elseif ($this->reporttype == "EKSKUL")
			require_once("infosiswa.ekskul.php");
		elseif ($this->reporttype == "CATPRES")
			require_once("infosiswa.catatanprestasi.php");
		elseif ($this->reporttype == "CATPENG")
			require_once("infosiswa.catatanpenghargaan.php");*/
	}
}