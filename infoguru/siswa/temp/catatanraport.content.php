<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/sessionchecker.php');
require_once('../include/config.php');
require_once('../include/common.php');
require_once('../include/db_functions.php');
require_once('catatanraport.info.class.php');

OpenDb();
$S = new CCatRepInfo();
?>
<table border='0' width='100%' cellpadding='2'>
<tr>
    <td align='left' valign='top'>
		<?$S->ShowIdentity(); ?>            
		<?$S->ShowReportComboBox(); ?>
        <br><br>
        <table border="0" cellpadding="2" width="680">
			<tr>
				<td align="left" valign="top">
					<div id="catatanraport.content">
						<?$S->ShowReportContent(); ?>
					</div>
				</td>
			</tr>
		</table>
    </td>
</tr>
</table>
<?php
CloseDb();
?>