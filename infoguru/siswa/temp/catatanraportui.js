function show_wait(areaId)
{
	var x = document.getElementById("waitBox").innerHTML;
	document.getElementById(areaId).innerHTML = x;
}

function getTabContent(){
	show_wait('pilihsiswa');
	sendRequestText('catatanraport.pilihsiswa.php',showTabPilih,'');
}
function showTabPilih(x){
	document.getElementById('pilihsiswa').innerHTML = x;
}
function showTabCari(x){
	document.getElementById('carisiswa').innerHTML = x;
}

function showsisInfo(x){
	document.getElementById('catrepinfo').innerHTML = x;
}

function load_first_dep(){
	//alert ('Asup');
	//setTimeout(chg_dep(),3000);
}

function pilihsiswa(nis)
{
	show_wait('content');
	sendRequestText('catatanraport.content.php', showInfo, 'nis='+nis);
	
	/*
	setTimeout(function (){
		sendRequestText('infosiswa.content.php', showInfo, 'nis='+nis);
		setTimeout(function() {getInfoTabContent(nis)}, 700);
	});
	*/
}

function showInfo(x)
{
	document.getElementById('content').innerHTML = x;
    //var TabbedPanels2 = new Spry.Widget.TabbedPanels("TabbedPanels2");
}

function GetReportContent()
{
	var reporttype = document.getElementById('reporttype').value;
	
	show_wait('content');
	sendRequestText('catatanraport.content.php', showInfo, 'reporttype='+reporttype);
}

function ShowReportContent(html)
{
	document.location.href = "catatanraport.value.php";
}


function CariSiswa(){
	var nis = document.frmCari.nis.value;
	var nisn = document.frmCari.nisn.value;
	var nama = document.frmCari.nama.value;
	var next = false;
	if (nis.length>=3 || nisn.length>=3 || nama.length>=3){
		next = true;
		
	}
	if (!next){
		alert ('NIS,NISN atau Nama harus diisi dan tidak boleh kurang dari 3 karakter!');
		document.frmCari.nis.focus();
		return false;
	}
	var addr2 = "";
	if (nis.length>=3)
		addr2 += 'nis='+nis;
	if (nama.length>=3)
		addr2 += (addr2!="")?'&nama='+nama:'nama='+nama;
	if (nisn.length>=3)
		addr2 += (addr2!="")?'&nisn='+nisn:'nisn='+nisn;
	
	show_wait('sisInfoCari');
	sendRequestText('getsiscari.php',showsisInfoCari,addr2);
	
}
function showsisInfoCari(x){
	document.getElementById('sisInfoCari').innerHTML = x;
}
