<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$nis=$_REQUEST[nis];
$tahun=$_REQUEST[tahun];

OpenDb();
?>
<table width="100%" border="1" cellspacing="0" class="tab">
    <tr class="header" height="30">
        <td width="50"><div align="center">Bulan</div></td>
        <td width="35%"><div align="center">#</div></td>
    </tr>
<?php  $sql = "SELECT MONTH(tanggal) AS bulan
              FROM $g_db_umum.catatansiswa
             WHERE YEAR(tanggal)='$tahun' AND nis='$nis'
             GROUP BY MONTH(tanggal)";
    $result=QueryDb($sql);
    if (@mysql_num_rows($result)>0)
    {
       	while ($row=@mysql_fetch_array($result))
        {
    	  	$sql_cnt="SELECT COUNT(*) FROM $g_db_umum.catatansiswa WHERE nis='$nis' AND MONTH(tanggal)='$row[bulan]' AND YEAR(tanggal)='$tahun'";
		  	$res_cnt=QueryDb($sql_cnt);
			$row_cnt=@mysql_fetch_row($res_cnt); ?>
            <tr onClick="ShowCatatanSiswa('<?php echo$nis?>','<?php echo$row[bulan]?>','<?php echo$tahun?>')" style="cursor:pointer;" title="Klik untuk menampilkan daftar Catatan Siswa">
                <td width="50"><?php echo$bulan_pjg[$row[bulan]]?></td>
                <td><div align="center"><?php echo$row_cnt[0]?></div></td>
            </tr>
<?php	  	}
	}
    else
    {  ?>
        <tr>
            <td colspan="2">Tidak ada Data</td>
        </tr>
<?php  } ?>
</table>
<?php
CloseDb();
?>