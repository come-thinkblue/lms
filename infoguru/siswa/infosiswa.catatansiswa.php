<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/sessionchecker.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
require_once('../include/db_functions.php');
require_once('../include/common.php');

$nis = $_SESSION["infosiswa.nis"];

OpenDb();

$sql_year = "SELECT DISTINCT YEAR(tanggal) as tahun
               FROM $g_db_umum.catatansiswa
              WHERE nis='$nis'";
$res_year = QueryDb($sql_year);

$tahun = "";
$bulan_pjg = array(1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
?>
<table width="100%" border="0" cellspacing="5">
<tr>
    <td width="150" height="30" valign="top">
    <div id="thn_catatan">Tahun
    <select name="tahun" id="tahun" onChange="ChangeTahunCatatanSiswa('<?php echo$nis?>')">
<?php	if (@mysql_num_rows($res_year) > 0)
    {
        while ($row_year=@mysql_fetch_array($res_year))
        {
            if ($tahun == "")
                $tahun=$row_year[tahun]; ?>
            <option value="<?php echo$row_year[tahun]?>"><?php echo$row_year[tahun]?></option>
<?php  	}
	}
    else
    {	?>
        <option value="">Tidak ada data</option>
<?php	}	?>
    </select>
    </div>
    <br />
	<div id="tabel_ck">
    <table width="100%" border="1" cellspacing="0" class="tab">
    <tr class="header" height="30">
        <td width="50"><div align="center">Bulan</div></td>
        <td width="35%"><div align="center">#</div></td>
    </tr>
<?php  $sql = "SELECT MONTH(tanggal) AS bulan
              FROM $g_db_umum.catatansiswa
             WHERE YEAR(tanggal)='$tahun' AND nis='$nis'
             GROUP BY MONTH(tanggal)";
    $result=QueryDb($sql);
    if (@mysql_num_rows($result)>0)
    {
       	while ($row=@mysql_fetch_array($result))
        {
		  	$sql_cnt = "SELECT COUNT(*)
                          FROM $g_db_umum.catatansiswa
                         WHERE nis='$nis' AND MONTH(tanggal)='$row[bulan]' AND YEAR(tanggal)='$tahun'";
		  	$res_cnt = QueryDb($sql_cnt);
			$row_cnt = @mysql_fetch_row($res_cnt); ?>
            <tr onClick="ShowCatatanSiswa('<?php echo$nis?>', '<?php echo$row[bulan]?>', '<?php echo$tahun?>')" style="cursor:pointer;" title="Klik untuk menampilkan daftar Catatan Siswa">
                <td width="50"><?php echo$bulan_pjg[$row[bulan]]?></td>
                <td><div align="center"><?php echo$row_cnt[0]?></div></td>
            </tr>
<?php   	}
	}
    else
    {  ?>
            <tr>
                <td colspan="2" align="center">Tidak ada Data</td>
            </tr>
<?php  } ?>
    </table>
    </div>
    </td>
    <td rowspan="2"><div id="contentcatatan">&nbsp;</div></td>
</tr>
<tr>
    <td valign="top"></td>
</tr>
</table>
<?php
CloseDb();
?>