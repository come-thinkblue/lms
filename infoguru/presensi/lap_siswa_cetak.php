<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');

$nis = $_REQUEST['nis'];
$tglawal = $_REQUEST['tglawal'];
$tglakhir = $_REQUEST['tglakhir'];
$urut = $_REQUEST['urut'];	
$urutan = $_REQUEST['urutan'];
$urut1 = $_REQUEST['urut1'];	
$urutan1 = $_REQUEST['urutan1'];

OpenDb();
$sql = "SELECT departemen FROM tahunajaran t, kelas k, siswa s WHERE s.nis='$nis' AND s.idkelas=k.replid AND k.idtahunajaran=t.replid";
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$departemen = $row[0];

$sql = "SELECT nama FROM siswa WHERE nis='$nis'";   
$result = QueryDB($sql);	
$row = mysql_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MAN Kota Blitar INFOGURU [Cetak Laporan Presensi Siswa]</title>
</head>

<body>

<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
		<?php echo GetHeader($departemen)?>
		<center>
			<font size="4"><strong>LAPORAN PRESENSI PELAJARAN SISWA</strong></font><br />
		</center>
		<br /><br />
	</td>
</tr>	
<tr>
		<td><strong>Siswa</strong></td>
    <td><strong>: <?php echo$nis.' - '.$row['nama']?></strong></td>
</tr>
<tr>
		<td><strong>Periode Presensi</strong></td>
    <td><strong>: <?php echo format_tgl($tglawal).' s/d '. format_tgl($tglakhir) ?></strong></td>
</tr>
</table>

<br />
<?php 		
	OpenDb();

	$sql = "SELECT k.kelas, DAY(p.tanggal), MONTH(p.tanggal), YEAR(p.tanggal), p.jam, pp.catatan, l.nama, g.nama, p.materi, pp.replid
					  FROM presensipelajaran p, ppsiswa pp, $g_db_pegawai.pegawai g, kelas k, pelajaran l
					 WHERE pp.idpp = p.replid AND p.idkelas = k.replid AND p.idpelajaran = l.replid
					   AND p.gurupelajaran = g.nip AND pp.nis = '$nis'
						 AND p.tanggal BETWEEN '$tglawal' AND '$tglakhir' AND pp.statushadir = 0 $filter
					 ORDER BY $urut $urutan" ;
	$result = QueryDb($sql);			 
	$jum_hadir = mysql_num_rows($result);
	
	$sql1 = "SELECT k.kelas, DAY(p.tanggal), MONTH(p.tanggal), YEAR(p.tanggal), p.jam, pp.catatan, l.nama, g.nama, p.materi, pp.replid
						 FROM presensipelajaran p, ppsiswa pp, $g_db_pegawai.pegawai g, kelas k, pelajaran l
						WHERE pp.idpp = p.replid AND p.idkelas = k.replid AND p.idpelajaran = l.replid AND p.gurupelajaran = g.nip
						  AND pp.nis = '$nis' AND p.tanggal BETWEEN '$tglawal' AND '$tglakhir' AND pp.statushadir <> 0 $filter
						ORDER BY $urut1 $urutan1" ;
	$result1 = QueryDb($sql1);			 
	$jum_absen = mysql_num_rows($result1);

	if ($jum_hadir > 0) { 
	?>
	
    <fieldset>
        <legend><strong>Data Kehadiran</strong></legend>
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">
   	<tr>		
    	<td width="5%" height="30" align="center" class="header">No</td>      	
      	<td width="5%" height="30" align="center" class="header">Tanggal</td>            
      	<td width="5%" height="30" align="center" class="header">Jam</td>        
        <td width="5%" height="30" align="center" class="header">Kelas</td>
      	<td width="*" height="30" align="center" class="header">Catatan</td>
      	<td width="15%" height="30" align="center" class="header">Pelajaran</td>
      	<td width="15%" height="30" align="center" class="header">Guru</td>
      	<td width="25%" height="30" align="center" class="header">Materi</td>       
    </tr>
	<?php 
    $cnt = 1;
    while ($row = @mysql_fetch_row($result)) {					
    ?>	
    <tr>        			
        <td height="25" align="center"><?php echo$cnt?></td>      	
      	<td height="25" align="center"><?php echo ShortDateFormat($row[3].'-'.$row[2].'-'.$row[1])?></td>
      	<td height="25" align="center"><?php echo substr($row[4],0,5)?></td>
        <td height="25" align="center"><?php echo$row[0]?></td>        
      	<td height="25"><?php echo$row[5]?></td>
      	<td height="25"><?php echo$row[6]?></td>
      	<td height="25"><?php echo$row[7]?></td>
      	<td height="25"><?php echo$row[8]?></td>    
    </tr>
<?php		$cnt++;
    } 
    CloseDb();	?>
    </table>
    <script language='JavaScript'>
        Tables('table', 1, 0);
    </script>	
    </fieldset>
    
<?php 	} 
	if ($jum_absen > 0) { 
	?>
   	<br />
    <fieldset>
        <legend><strong>Data Ketidakhadiran</strong></legend>
    
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center">
    <tr>		
		<td width="5%" height="30" align="center" class="header">No</td>
      	<td width="5%" height="30" align="center" class="header">Tanggal</td>            
      	<td width="5%" height="30" align="center" class="header">Jam</td>
        <td width="5%" height="30" align="center" class="header">Kelas</td>
      	<td width="*" height="30" align="center" class="header">Catatan</td>
      	<td width="15%" height="30" align="center" class="header">Pelajaran</td>
      	<td width="15%" height="30" align="center" class="header">Guru</td>
      	<td width="25%" height="30" align="center" class="header">Materi</td>      	
    </tr>
	<?php 
    $cnt = 1;
    while ($row1 = @mysql_fetch_row($result1)) {					
    ?>	
    <tr>        			
        <td height="25" align="center"><?php echo$cnt?></td>        
        <td height="25" align="center"><?php echo ShortDateFormat($row[3].'-'.$row[2].'-'.$row[1])?></td>
        <td height="25" align="center"><?php echo substr($row1[4],0,5)?></td>
        <td height="25" align="center"><?php echo$row1[0]?></td>
        <td height="25"><?php echo$row1[5]?></td>
        <td height="25"><?php echo$row1[6]?></td>
        <td height="25"><?php echo$row1[7]?></td>
        <td height="25"><?php echo$row1[8]?></td>        
    </tr>
<?php		$cnt++;
    } 
    CloseDb();	?>
	  </table>	 

	</fieldset>
<?php 	} ?>    
	<br />
    <table width="100%" border="0" align="center">
    <tr>
        <td width="21%"><b>Jumlah Kehadiran</b></td>
        <td><b>: <?php echo$jum_hadir ?></b></td>
    </tr>
    <tr>
        <td><b>Jumlah Ketidakhadiran</b></td>
        <td><b>: <?php echo$jum_absen ?></b></td>
    </tr>
    <tr>
        <td><b>Jumlah Seharusnya</b></td>
        <td><b>: <?php $total = $jum_hadir+$jum_absen;
                echo $total ?></b></td>
    </tr>
    <tr>
        <td><b>Presentase Kehadiran</b></td>
        <td><b>: <?php 	if ($total == 0) 
                    $total = 1;
                $prs = (( $jum_hadir/$total)*100) ;
                echo (int)$prs ?>%</b></td>
    </tr>
	</table>

</td></tr>
</table>    
</body>
<?php if ($_REQUEST['lihat'] == 1) { ?>
<script language="javascript">
window.print();
</script>
<?php } ?> 
</html>