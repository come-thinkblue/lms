<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Laporan_Harian_Data_Siswa_Tidak_Hadir.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$departemen = $_REQUEST['departemen'];
$semester = $_REQUEST['semester'];
$kelas = $_REQUEST['kelas'];
$tingkat = $_REQUEST['tingkat'];
$tglawal = $_REQUEST['tglawal'];
$tglakhir = $_REQUEST['tglakhir'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

$filter1 = "AND t.departemen = '$departemen'";
if ($tingkat <> -1) 
	$filter1 = "AND k.idtingkat = '$tingkat'";

$filter2 = "";
if ($kelas <> -1) 
	$filter2 = "AND k.replid = '$kelas'";
	
OpenDb();
$sql = "SELECT t.departemen, a.tahunajaran, s.semester, k.kelas, t.tingkat FROM tahunajaran a, kelas k, tingkat t, semester s, presensiharian p WHERE p.idkelas = k.replid AND k.idtingkat = t.replid AND k.idtahunajaran = a.replid AND p.idsemester = s.replid AND s.replid = '$semester' $filter1 $filter2";  

$result = QueryDB($sql);	
$row = mysql_fetch_array($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Laporan Harian Data Siswa yang Tidak Hadir]</title>
<style type="text/css">
<!--
.style1 {
	font-size: 16px;
	font-family: 'Droid Sans', sans-serif;
}
.style4 {font-family: 'Droid Sans', sans-serif; font-weight: bold; font-size: 12px; }
.style5 {font-family: Verdana}
.style6 {font-size: 12px}
.style7 {font-family: 'Droid Sans', sans-serif; font-size: 12px; }
-->
</style>
</head>

<body>

<table width="100%" border="0" cellspacing="0">
  <tr>
    <th scope="row" colspan="10"><span class="style1">Laporan Harian Data Siswa yang Tidak Hadir</span></th>
  </tr>
</table>
<br />
<table width="27%">
<tr>
	<td width="43%"><span class="style4">Departemen</span></td>
    <td width="57%" colspan="9"><span class="style4">: 
      <?php echo$row['departemen']?>
    </span></td>
</tr>
<tr>
	<td><span class="style4">Tahun Ajaran</span></td>
    <td colspan="9"><span class="style4">: 
      <?php echo$row['tahunajaran']?>
    </span></td>
</tr>
<tr>
	<td><span class="style4">Semester</span></td>
    <td colspan="9"><span class="style4">: 
      <?php echo$row['semester']?>
    </span></td>
</tr>
<tr>
	<td><span class="style4">Tingkat</span></td>
    <td colspan="9"><span class="style4">: 
      <?php if ($tingkat == -1) echo "Semua Tingkat"; else echo $row['tingkat']; ?>
    </span></td>
</tr>
<tr>
	<td><span class="style4">Kelas</span></td>
    <td colspan="9"><span class="style4">: 
      <?php if ($kelas == -1) echo "Semua Kelas"; else echo $row['kelas']; ?>
    </span></td>
</tr>
<tr>
	<td><span class="style4">Periode Presensi</span></td>
    <td colspan="9"><span class="style4">: <?php echo format_tgl($tglawal).' s/d '. format_tgl($tglakhir) ?></span></td>
</tr>
</table>
<br />
<?php 	OpenDb();
	$sql = "SELECT s.nis, s.nama, SUM(ph.hadir), SUM(ph.ijin) AS ijin, SUM(ph.sakit) AS sakit, SUM(ph.alpa) AS alpa, SUM(ph.cuti) AS cuti, k.kelas, s.hportu, s.emailayah, s.alamatortu, s.telponortu, s.hpsiswa, s.emailsiswa, s.aktif, s.emailibu FROM siswa s LEFT JOIN (phsiswa ph INNER JOIN presensiharian p ON p.replid = ph.idpresensi) ON ph.nis = s.nis, kelas k, tingkat t WHERE k.replid = s.idkelas AND k.idtingkat = t.replid $filter1 $filter2 AND p.idsemester = '$semester' AND (((p.tanggal1 BETWEEN '$tglawal' AND '$tglakhir') OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) OR (('$tglawal' BETWEEN p.tanggal1 AND p.tanggal2) OR ('$tglakhir' BETWEEN p.tanggal1 AND p.tanggal2))) GROUP BY s.nis HAVING ijin>0 OR sakit>0 OR cuti>0 OR alpa>0 ORDER BY $urut $urutan";
	
	$result = QueryDb($sql);
	$jum = mysql_num_rows($result);
	if ($jum > 0) { 
?>
	<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left">
   	<tr height="30" align="center">
    	<td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>No</strong></td>
		<td width="10%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>N I S</strong></td>
        <td width="10%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Nama</strong></td>
   		<td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Kelas</strong></td>
        <td width="*" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Ortu</strong></td>
   		<td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Hadir</strong></td>
		<td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Ijin</strong></td>            
		<td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Sakit</strong></td>
        <td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Alpa</strong></td>
        <td width="5%" bgcolor="#CCCCCC" class="style6 style5 header"><strong>Cuti</strong></td>
        
    </tr>
<?php		
	$cnt = 0;
	while ($row = mysql_fetch_row($result)) { ?>
    <tr height="25" valign="middle">    	
    	<td align="center" valign="middle" style="text-align:center"><span class="style7">
   	    <?php echo++$cnt?>
    	</span></td>
		<td align="center" valign="middle"><span class="style7">
	    <?php echo$row[0]?>
		</span></td>
        <td valign="middle"><span class="style7">
        <?php echo$row[1]?>
        </span></td>
        <td align="center" valign="middle"><span class="style7">
        <?php echo$row[7]?>
        </span></td>
        <!--<td valign="middle"><span class="style7">HP: 
        <?php echo$row[8]?>
        <br />
       	  Email: 
       	  <?php echo$row[9]?>
       	  <br />
          Alamat: 
          <?php echo$row[10]?>
          <br />
		  Telp: 
		  <?php echo$row[11]?>
		  <br />
          HP Siswa: 
          <?php echo$row[12]?>
          <br />
          Email Siswa: 
          <?php echo$row[13]?>      	
          </span></td>
  		-->
        <td valign="middle"><span class="style7">
         <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="27%" >Handphone</td>
                <td>:&nbsp;</td>
                <td width="90%" ><?php echo$row[8]?> </td>  
            </tr>                
            <tr>
                <td>Email</td>
                <td>:&nbsp;</td>
              	<td>
				<?php 	if ($row[9] <> "" && $row[15] <> "")
						echo $row[9].", ".$row[15];
				 	elseif ($row[15] == "")
						echo $row[9];
					else 
						echo $row[15];
				?>
                </td>
            </tr>
            <tr>
                <td valign="top">Alamat</strong></td>
                <td valign="top">:&nbsp; </td>
              	<td><?php echo$row[10]?></td>
            </tr>
            <tr>
                <td>Telepon</strong></td>
              	<td>:&nbsp; </td>  
                <td><?php echo$row[11]?></td>
            </tr>
            <tr>
                <td>HP Siswa</strong></td>
              	<td>:&nbsp; </td>   
                <td><?php echo$row[12]?></td>
            </tr>
            <tr>
                <td>Email Siswa</strong></td>
              	<td>:&nbsp; </td>  
                <td><?php echo$row[13]?></td>
            </tr>
            </table> 
        </span></td> 
        <td align="center" valign="middle"><span class="style7"><font size="4"><b>
	    <?php echo$row[2]?>
	    </br>
  		</span></td>
        <td align="center" valign="middle"><span class="style7"><font size="4"><b>
        <?php echo$row[3]?>
        </br>
        </span></td>    
        <td align="center" valign="middle"><span class="style7"><font size="4"><b>
        <?php echo$row[4]?>
        </br>
        </span></td>
        <td align="center" valign="middle"><span class="style7"><font size="4"><b>
        <?php echo$row[5]?>
        </br>
        </span></td>    
        <td align="center" valign="middle"><span class="style7"><font size="4"><b>
        <?php echo$row[6]?>
        </br>
        </span></td>
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
</table>	
<?php 	} ?>

</body>
<script language="javascript">
window.print();
</script>

</html>