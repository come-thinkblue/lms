<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$semester = $_REQUEST['semester'];
$tingkat = $_REQUEST['tingkat'];
$pelajaran = $_REQUEST['pelajaran'];
$tglawal = $_REQUEST['tglawal'];
$tglakhir = $_REQUEST['tglakhir'];

OpenDb();
$sql = "SELECT t.departemen, a.tahunajaran, s.semester, t.tingkat, l.nama FROM tahunajaran a, kelas k, tingkat t, semester s, presensipelajaran p, pelajaran l WHERE p.idkelas = k.replid AND k.idtingkat = t.replid AND k.idtahunajaran = a.replid AND p.idsemester = s.replid AND s.replid = '$semester' AND t.replid = '$tingkat' AND p.idpelajaran = l.replid AND l.replid = '$pelajaran' ";  

$result = QueryDB($sql);	
$row = mysql_fetch_array($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MAN Kota Blitar INFOGURU [Cetak Statistik Kehadiran Setiap Kelas]</title>
</head>

<body>

<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
<?php echo GetHeader($row[departemen])?>
	
<center>
  <font size="4"><strong>STATISTIK KEHADIRAN SETIAP KELAS</strong></font><br />
 </center><br /><br />
<table>
<tr>
	<td width="25%"><strong>Departemen</strong></td>
    <td><strong>: <?php echo$row['departemen']?></strong></td>
</tr>
<tr>
	<td><strong>Tahun Ajaran</strong></td>
    <td><strong>: <?php echo$row['tahunajaran']?></strong></td>
</tr>
<tr>
	<td><strong>Semester</strong></td>
    <td><strong>: <?php echo$row['semester']?></strong></td>
</tr>
<tr>
	<td><strong>Tingkat</strong></td>
    <td><strong>: <?php echo$row['tingkat'] ?></strong></td>
</tr>
<tr>
	<td><strong>Pelajaran</strong></td>
    <td><strong>: <?php echo$row['nama'] ?></strong></td>
</tr>
<tr>
	<td><strong>Periode Presensi</strong></td>
    <td><strong>: <?php echo format_tgl($tglawal).' s/d '. format_tgl($tglakhir) ?></strong></td>
</tr>
</table>
<br />
<?php 	OpenDb();
	$sql = "SELECT DISTINCT k.kelas, k.replid FROM presensipelajaran p, kelas k WHERE p.idkelas = k.replid AND k.idtingkat = '$tingkat' AND p.idsemester = '$semester' AND p.idpelajaran = '$pelajaran' AND p.tanggal BETWEEN '$tglawal' AND '$tglakhir' ORDER BY k.kelas, p.tanggal ";	
	
	$result = QueryDb($sql);
	$jum = mysql_num_rows($result);
	if ($jum > 0) { 
?>
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
   	<tr height="30" align="center">
    	<td width="5%" class="header">No</td>
		<td width="10%" class="header">Kelas</td>
        <td width="*" class="header"></td>
    </tr>
<?php		
	$cnt = 0;
	while ($row = mysql_fetch_row($result)) { ?>
    <tr height="25">    	
    	<td align="center"><?php echo++$cnt?></td>
        <td align="center"><?php echo$row[0]?></td>
         <td align="center"><img src="statistik_batang.php?semester=<?php echo$semester?>&kelas=<?php echo$row[1]?>&pelajaran=<?php echo$pelajaran?>&tglakhir=<?php echo$tglakhir?>&tglawal=<?php echo$tglawal?>" />
        </td>
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>	
<?php 	} ?>
	</td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>

</html>