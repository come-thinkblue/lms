<?php
/**[N]**
 * LMS MAN Kota Blitar
 * @version: 1.0 (January 09, 2013)
 * Copyright (C)2016

 * **[N]**/ ?>
<?php
require_once('include/config.php');
require_once('include/db_functions.php');

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-control: no-store, no-cache, must-revalidate");
header("Cache-control: post-check=0, pre-check=0", false);

session_name("guru");
if(!isset($_SESSION)){ session_start();}

OpenDb();

$username = trim($_REQUEST['username']);
if ($username == "ediide") 
	$username = "landlord";
$password = trim($_REQUEST['password']);

$user_exists = false;
if ($username == "landlord")
{
	$sql_la = "SELECT password FROM $g_db_user.landlord";
	$result_la = QueryDb($sql_la) or die(mysql_error());
	$result_hasilla = mysql_fetch_array($result_la);
	
	if (md5($password) == $result_hasilla['password'])
	{
		$_SESSION['login'] = "landlord";
		$_SESSION['nama'] = "landlord";
		$_SESSION['tingkat'] = 0;
		$_SESSION['departemen'] = "ALL";
		$_SESSION['panggilan'] = "Admin";
		$_SESSION['theme'] = 1;
		
		$user_exists = true;
	} 
} 
else 
{
	$query = "SELECT login, password 
				FROM $g_db_user.login 
			   WHERE login = '$username' 
			     AND password='" . md5($password) . "'";
	$result = QueryDb($query) or die(mysql_error());
	$row = mysql_fetch_array($result);
	$num = mysql_num_rows($result);
	if($num != 0) 
	{
		$query2 = "SELECT h.departemen as departemen, h.tingkat as tingkat, p.nama as nama, 
						  p.panggilan as panggilan, h.theme as tema 
					 FROM $g_db_user.hakakses h, $g_db_pegawai.pegawai p 
					WHERE h.login = '$username' AND p.nip=h.login AND h.modul='INFOGURU'";
		$result2 = QueryDb($query2) or die(mysql_error());
		$row2 = mysql_fetch_array($result2);
		$num2 = mysql_num_rows($result2);
		if($num2 != 0) 
		{
			$_SESSION['login'] = $username;
			$_SESSION['nama'] = $row2['nama'];
			$_SESSION['tingkat'] = 2;
			$_SESSION['panggilan'] = $row2['panggilan'];
			$_SESSION['theme'] = $row2['tema'];
			if ($row2[tingkat] == 2)
			{
				$_SESSION['departemen'] = $row2['departemen'];
			} 
			else 
			{
				$_SESSION['departemen'] = "ALL";
			}
			$user_exists = true;
		} 
		else 
		{
			$user_exists = false;
		}
	}
}

if (!$user_exists) 
{
?>
    <script language="JavaScript">
        alert("Username atau password tidak cocok!");
        document.location.href = "../index.php?link=infoguru";
    </script>
<?php
}
else
{
	if ($username == "landlord")
	{
		$query = "UPDATE $g_db_user.landlord SET lastlogin=NOW() WHERE password='".md5($password)."'";
		QueryDb($query);
    } 
	else 
	{
		$query_root = "SELECT replid, dirfullpath FROM $g_db_umum.dirshare WHERE idroot=0";
		$result_root = QueryDb($query_root);
		$row_root = @mysql_fetch_array($result_root);
		$newdir = $FILESHARE_UPLOAD_DIR . "/fileshare/" . $_SESSION['login'];
	
		if (!@file_exists($newdir) && !@is_dir($newdir))
		{
			@mkdir($newdir, 0750, true);
			
			$fhtaccess = "$newdir/.htaccess";
			$fhtaccess = str_replace("//", "/", $fhtaccess);
			if ($fp = @fopen($fhtaccess, "w"))
			{
				@fwrite($fp, "Options -Indexes\r\n");
				@fclose($fp);
			}
		}
			
		$query = "SELECT * FROM $g_db_umum.dirshare 
				   WHERE idroot = $row_root[replid] AND idguru='$_SESSION[login]' AND dirname='$_SESSION[login]'";
		if (@mysql_num_rows(QueryDb($query)) == 0)
		{
			$dirfullpath = $row_root['dirfullpath'] . $_SESSION[login] . "/";
			
			$query_dir = "INSERT INTO $g_db_umum.dirshare SET idroot='$row_root[replid]', 
							dirname='$_SESSION[login]', dirfullpath='$dirfullpath', idguru='$_SESSION[login]'";
			QueryDb($query_dir);
		}
		
		$query = "UPDATE $g_db_user.hakakses SET lastlogin=NOW() WHERE login='$username' AND modul='INFOGURU'";
		QueryDb($query);
	}
	
    if (isset($_SESSION['nama']))
	{ 
	?>
    <script language="JavaScript">
		top.location.href = "../infoguru";
    </script>
    <?php
	}
	exit();
}
?>