<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];	
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];	
if (isset($_REQUEST['tgl1']))
	$tgl1 = $_REQUEST['tgl1'];	
if (isset($_REQUEST['bln1']))
	$bln1 = $_REQUEST['bln1'];	
if (isset($_REQUEST['th1']))
	$th1 = $_REQUEST['th1'];	
if (isset($_REQUEST['tgl2']))
	$tgl2 = $_REQUEST['tgl2'];		
if (isset($_REQUEST['bln2']))
	$bln2 = $_REQUEST['bln2'];	
if (isset($_REQUEST['th2']))
	$th2 = $_REQUEST['th2'];	
$urut = "s.nama";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	
$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];
		
$tglawal = "$th1-$bln1-$tgl1";
if (isset($_REQUEST['tglawal']))
	$tglawal = $_REQUEST['tglawal'];	
$tglakhir = "$th2-$bln2-$tgl2";
if (isset($_REQUEST['tglakhir']))
	$tglakhir = $_REQUEST['tglakhir'];	

$filter1 = "AND t.departemen = '$departemen'";
if ($tingkat <> -1) 
	$filter1 = "AND k.idtingkat = '$tingkat'";

$filter2 = "";
if ($kelas <> -1) 
	$filter2 = "AND k.replid = '$kelas'";


$op = $_REQUEST['op'];

if ($op == "xm8r389xemx23xb2378e23") {
	OpenDb();
	$sql = "DELETE FROM presensipelajaran WHERE replid = '$_REQUEST[replid]'";
	QueryDb($sql);
	CloseDb();	
}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Data Siswa Tidak Hadir</title>
<script language="javascript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript">

function cetak() {
	var tglawal = document.getElementById('tglawal').value;
	var tglakhir = document.getElementById('tglakhir').value;	
	var semester = document.getElementById('semester').value;
	var departemen = document.getElementById('departemen').value;
	var kelas = document.getElementById('kelas').value;
	var tingkat = document.getElementById('tingkat').value;
	
	newWindow('lap_absen_cetak.php?tglawal='+tglawal+'&tglakhir='+tglakhir+'&semester='+semester+'&kelas='+kelas+'&tingkat='+tingkat+'&departemen='+departemen+'&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>', 'CetakLaporanSiswayangTidakHadir','800','650','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function excel() {
	var tglawal = document.getElementById('tglawal').value;
	var tglakhir = document.getElementById('tglakhir').value;	
	var semester = document.getElementById('semester').value;
	var kelas = document.getElementById('kelas').value;
	var tingkat = document.getElementById('tingkat').value;
	var departemen = document.getElementById('departemen').value;
	
	newWindow('lap_absen_excel.php?tglawal='+tglawal+'&tglakhir='+tglakhir+'&semester='+semester+'&kelas='+kelas+'&tingkat='+tingkat+'&departemen='+departemen+'&urut=<?php echo$urut?>&urutan=<?php echo$urutan?>', 'CetakLaporanSiswayangTidakHadir','800','650','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function change_urut(urut,urutan) {		
	var semester = document.getElementById('semester').value;
	var kelas = document.getElementById('kelas').value;
	var tingkat = document.getElementById('tingkat').value;
	var departemen = document.getElementById('departemen').value;
	var tglawal = document.getElementById('tglawal').value;
	var tglakhir = document.getElementById('tglakhir').value;
		
	if (urutan =="ASC"){
		urutan="DESC"
	} else {
		urutan="ASC"
	}
	
	document.location.href = "lap_absen_footer.php?semester="+semester+"&kelas="+kelas+"&tingkat="+tingkat+"&departemen="+departemen+"&tglawal="+tglawal+"&tglakhir="+tglakhir+"&urut="+urut+"&urutan="+urutan;
	
}
</script>
</head>

<body>
<input type="hidden" name="tglawal" id="tglawal" value="<?php echo$tglawal?>">
<input type="hidden" name="tglakhir" id="tglakhir" value="<?php echo$tglakhir?>">
<input type="hidden" name="semester" id="semester" value="<?php echo$semester?>">
<input type="hidden" name="kelas" id="kelas" value="<?php echo$kelas?>">
<input type="hidden" name="tingkat" id="tingkat" value="<?php echo$tingkat?>">
<input type="hidden" name="departemen" id="departemen" value="<?php echo$departemen?>">
<input type="hidden" name="urut" id="urut" value="<?php echo$urut?>">
<input type="hidden" name="urutan" id="urutan" value="<?php echo$urutan?>">

<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background-repeat:no-repeat; background-attachment:fixed">
<!-- TABLE UTAMA -->
<tr>
	<td>

    <?php 		
	OpenDb();
	$sql = "SELECT DISTINCT s.nis, s.nama, l.nama, DAY(p.tanggal), MONTH(p.tanggal), YEAR(p.tanggal), pp.statushadir, pp.catatan, s.telponsiswa, s.hpsiswa, s.namaayah, s.telponortu, s.hportu, k.kelas FROM siswa s, presensipelajaran p, ppsiswa pp, pelajaran l, kelas k, tingkat t WHERE pp.idpp = p.replid AND s.idkelas = k.replid AND p.idsemester = '$semester' AND pp.nis = s.nis AND k.idtingkat = t.replid $filter1 $filter2 AND p.tanggal BETWEEN '$tglawal' AND '$tglakhir' AND p.idpelajaran = l.replid AND pp.statushadir <> 0 ORDER BY $urut $urutan";
	//echo $sql;
	$result = QueryDb($sql);			 
	$jum_hadir = mysql_num_rows($result);
	if ($jum_hadir > 0) { 
	?>  
     	<table width="100%" border="0" align="center">
        <!-- TABLE LINK -->
        <tr>
            <td align="right"> 	
            <a href="#" onClick="document.location.reload()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
            <a href="JavaScript:excel()"><img src="../images/ico/excel.png" border="0" onmouseover="showhint('Cetak dalam format Excel!', this, event, '80px')"/>&nbsp;Cetak Excel</a>&nbsp;&nbsp;
            <a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onmouseover="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp;
            
            </td>
        </tr>
        </table>
        <br />
      	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">
       	<tr height="30" align="center" class="header">		
			<td width="5%">No</td>
			<td width="8%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('s.nis','<?php echo$urutan?>')">N I S <?php echo change_urut('s.nis',$urut,$urutan)?></td>
			<td width="15%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('s.nama','<?php echo$urutan?>')">Nama <?php echo change_urut('s.nama',$urut,$urutan)?></td>            
			<?php if ($kelas == -1) { ?>
        	<td width="9%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('k.kelas','<?php echo$urutan?>')">Kelas <?php echo change_urut('k.kelas',$urut,$urutan)?></td>
			<?php } ?>
            <td width="12%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('l.nama','<?php echo$urutan?>')">Pelajaran<?php echo change_urut('l.nama',$urut,$urutan)?></td>
            <td width="6%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('p.tanggal','<?php echo$urutan?>')">Tgl <?php echo change_urut('p.tanggal',$urut,$urutan)?></td>
            <td width="8%">Presensi</td>
            <td width="10%">Keterangan</td>            
            <td width="7%">Tlp Siswa</td>
            <td width="10%">HP Siswa</td>
            <td width="15%">Orang Tua</td>
            <td width="7%">Tlp Ortu</td>
            <td width="10%">HP Ortu</td>
		</tr>
		<?php 
		$cnt = 0;
		while ($row = @mysql_fetch_row($result)) {	
			switch ($row[6]){
				case 1 : $st="Ijin";
				break;
				case 2 : $st="Sakit";
				break;	
				case 3 : $st="Alpha";
				break;
				case 4 : $st="Cuti";
				break;
			}		
							
		?>	
        <tr height="25">        			
			<td align="center"><?php echo++$cnt?></td>
			<td align="center"><?php echo$row[0]?></td>
            <td><?php echo$row[1]?></td>
            <?php if ($kelas == -1) { ?>
            <td align="center"><?php echo$row[13]?></td>
            <?php } ?>
            <td><?php echo$row[2]?></td>
            <td align="center"><?php echo$row[3].'-'.$row[4].'-'.substr($row[5],2,2)?></td>
           	<td align="center"><?php echo$st ?></td>
            <td><?php echo$row[7] ?></td>
            <td align="center"><?php echo$row[8]?></td>
            <td align="center"><?php echo$row[9]?></td>
            <td><?php echo$row[10]?></td>
            <td align="center"><?php echo$row[11]?></td>
            <td align="center"><?php echo$row[12]?></td>            
    	</tr>
 	<?php		
		} 
		CloseDb();	?>
		</table>
        <script language='JavaScript'>
   			Tables('table', 1, 0);
		</script>       
		</td>
    </tr>
<?php 	} else { ?>

	 <table width="100%" border="0" align="center">          
	<tr>
		<td align="center" valign="middle" height="250">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br />Tambah data presensi kelas di menu Presensi Pelajaran pada bagian Presensi.</b></font>
		</td>
	</tr>
	</table>
<?php	} ?> 
	</td>
</tr>    
<!-- END OF TABLE UTAMA -->
</table>
</body>
</html>