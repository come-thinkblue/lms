<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$flag = 0;
if (isset($_REQUEST['flag']))
	$flag = (int)$_REQUEST['flag'];
$nama = $_REQUEST['nama'];
$nip = $_REQUEST['nip'];

?>

<table border="0" width="100%" cellpadding="2" cellspacing="2" align="center">
<tr>
	<td>
	<input type="hidden" name="flag" id="flag" value="<?php echo$flag ?>" />
	</td>
</tr>
<tr>
	<td>
    	<form name="main">       
		<font color="#000000"><b>N I P</b></font>
    	<input type="text" name="nip" id="nip" value="<?php echo$_REQUEST['nip'] ?>" size="20" onKeyPress="return focusNext('submit', event)" />&nbsp;&nbsp; 
        <font color="#000000"><b>Nama</b></font>
    	<input type="text" name="nama" id="nama" value="<?php echo$_REQUEST['nama'] ?>" size="20" onKeyPress="return focusNext('submit', event)" />&nbsp;
		<input type="button" class="but" name="submit" id="submit" value="Cari" onclick="carilah()" style="width:80px;"/>&nbsp;	
	</form>
    </td>
</tr>

<tr>
	<td align="center">
    <div id = "caritabel">
<?php 
if (isset($_REQUEST['submit']) || $_REQUEST['submit'] == 1) { 
	OpenDb();
		
	if ((strlen($nama) > 0) && (strlen($nip) > 0))
		$sql = "SELECT p.nip, p.nama, pel.nama, pel.departemen FROM $g_db_pegawai.pegawai p, dbakademik.guru g, dbakademik.pelajaran pel, dbakademik.departemen d WHERE p.nama LIKE '%$nama%' AND p.nip LIKE '%$nip%' AND pel.replid=g.idpelajaran AND g.nip=p.nip AND pel.departemen = d.departemen GROUP BY p.nip ORDER BY d.urutan, p.nama"; 
	else if (strlen($nama) > 0)
		$sql = "SELECT p.nip, p.nama, pel.nama, pel.departemen FROM $g_db_pegawai.pegawai p, dbakademik.guru g, dbakademik.pelajaran pel, dbakademik.departemen d WHERE p.nama LIKE '%$nama%' AND pel.replid=g.idpelajaran AND g.nip=p.nip AND pel.departemen = d.departemen GROUP BY p.nip ORDER BY d.urutan, p.nama"; 
	else if (strlen($nip) > 0)
		$sql = "SELECT p.nip, p.nama, pel.nama, pel.departemen FROM $g_db_pegawai.pegawai p, dbakademik.guru g, dbakademik.pelajaran pel, dbakademik.departemen d WHERE p.nip LIKE '%$nip%' AND pel.replid=g.idpelajaran AND g.nip=p.nip AND pel.departemen = d.departemen GROUP BY p.nip ORDER BY p.nama"; 
	//echo $sql;	
	//else if ((strlen($nama) == 0) || (strlen($nip) == 0))
	//	$sql = "SELECT p.nip, p.nama, pel.nama, pel.departemen FROM $g_db_pegawai.pegawai p, dbakademik.guru g, dbakademik.pelajaran pel, dbakademik.departemen d AND pel.replid=g.idpelajaran AND g.nip=p.nip ORDER BY p.nama";	
	$result = QueryDb($sql);
	if (@mysql_num_rows($result)>0){
?>
    <br />
    <table width="100%" class="tab" align="center" cellpadding="2" cellspacing="0" id="table1" border="1" bordercolor="#000000">
    <tr height="30">
        <td class="header" width="7%" align="center">No</td>
        <td class="header" width="15%" align="center">N I P</td>
        <td class="header" align="center" >Nama</td>
        <td class="header" align="center" >Departemen</td>
        <!--<td class="header" align="center" >Pelajaran</td>-->
        <td class="header" width="10%">&nbsp;</td>
    </tr>
<?php
	$cnt = 0;
		while($row = mysql_fetch_row($result)) { ?>
	<tr height="25" onClick="pilih('<?php echo$row[0]?>','<?php echo$row[1]?>','<?php echo$row[3]?>','<?php echo$row[2]?>')" style="cursor:pointer">
		<td align="center"><?php echo++$cnt ?></td>
		<td align="center"><?php echo$row[0] ?></td>
		<td align="left"><?php echo$row[1] ?></td>
        <td align="center"><?php echo$row[3] ?></td>
		<!--<td align="center"><?php echo$row[2] ?></td>-->
		<td align="center">
		<input type="button" name="pilih" class="but" id="pilih" value="Pilih" onclick="pilih('<?php echo$row[0]?>', '<?php echo$row[1]?>', '<?php echo$row[3]?>', '<?php echo$row[2]?>')" />
		</td>
	</tr>
	<?php } CloseDb(); ?>
 	</table>
<?php } else { ?>    		
	<table width="100%" align="center" cellpadding="2" cellspacing="0" border="0" id="table1">
	<tr height="30" align="center">
		<td>   
   
	<br /><br />	
	<font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br /><br />            
		Tambah data guru di menu Pendataan Guru pada bagian Guru & Pelajaran. </b></font>	
	<br /><br />
   		</td>
    </tr>
    </table>
<?php 	} 
}?>	
	</div>
    </td>    
</tr>
<tr>
	<td align="center" >
	<input type="button" class="but" name="tutup" id="tutup" value="Tutup" onclick="window.close()" style="width:80px;"/>
	</td>
</tr>
</table>