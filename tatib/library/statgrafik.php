<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
$kode = $_REQUEST['kode'];
$departemen = $_REQUEST['key'];
$key = $_REQUEST['key'];
$keyword = $_REQUEST['keyword'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Grafik Statistik</title>
<script language="javascript" src="script/tools.js"></script>
<script  language="javascript">
function CetakWord() {
	var addr = "cetakgrafik.php?key=<?php echo$key?>&keyword=<?php echo$keyword?>&departemen=<?php echo$departemen?>&kode=<?php echo$kode?>";
	newWindow(addr, 'StatWord','790','630','resizable=1,scrollbars=1,status=0,toolbar=0');
}
</script>

</head>

<body topmargin="0" leftmargin="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td align="center">
<a href="JavaScript:CetakWord()"><img src="../images/ico/print.png" border="0" />&nbsp;Cetak</a>
</td></tr>
</table>
<br />
<table width="100%" border="0">
<tr><td>

	<div id="grafik" align="center">
	<table width="100%" border="0" align="center">
    <tr><td>
    	<div align="center">
            <img src="<?php echo"statimage.php?type=bar&key=$key&keyword=$keyword&departemen=$departemen&kode=$kode"?>" />
        </div>
    </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>
    	<div align="center">
              <img src="<?php echo"statimage.php?type=pie&key=$key&keyword=$keyword&departemen=$departemen&kode=$kode"?>" />
        </div>
    </td></tr>
	</table>
	</div>
    
</td></tr>
</table>

</body>
</html>