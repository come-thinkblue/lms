<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php 
require_once("include/theme.php"); 
require_once("include/errorhandler.php");
require_once("include/sessioninfo.php");
require_once("include/common.php");
require_once("include/config.php");
require_once("include/db_functions.php");
require_once("include/sessioninfo.php");

$menu="";
if (isset($_REQUEST['menu']))
	$menu=$_REQUEST['menu'];
$content="";
if (isset($_REQUEST['content']))
	$content=$_REQUEST['content'];	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" language="JavaScript1.2" src="design/dhtml/stmenu.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="script/ajax.js"></script>
<script type="text/javascript" language="JavaScript1.2" src="script/tools.js"></script>
<script type="text/javascript" language="JavaScript1.2">
function get_fresh(){
	document.location.reload();
}
function chating_euy(){
	newWindow('buletin/chat/chat.php','ChattingYuk',626,565,'resizable=0,scrollbars=0,status=0,toolbar=0');
}
function home(){
	document.location.reload();
	parent.framecenter.location.href="home.php";
}
function akademik(){
	sendRequestText("get_content.php", show_content, "menu=tatib");
	parent.framecenter.location.href="home.php";
}
function buletin(){
	sendRequestText("get_content.php", show_content, "menu=buletin");
	parent.framecenter.location.href="home.php";
}
function pengaturan(){
	sendRequestText("get_content.php", show_content, "menu=pengaturan");
	parent.framecenter.location.href="home.php";
}
function dotnet(){
	sendRequestText("get_content.php", show_content, "menu=dotnet");
	parent.framecenter.location.href="home.php";
}
function logout() {
    if (confirm("Anda yakin akan menutup Aplikasi Manajemen Akademik ini?"))
		document.location.href="logout.php";
}
function show_content(x) {
	document.getElementById("vscroll0").innerHTML = x;
}
function show_wait(areaId) {
	var x = document.getElementById("waitBox").innerHTML;
	document.getElementById(areaId).innerHTML = x;
}
function ganti() {
	var login=document.getElementById('login').value;
	var addr="pengaturan/ganti_password2.php";
	if (login=="LANDLORD" || login=="landlord"){
		alert ('Maaf, Administrator tidak dapat mengganti password !');
		parent.framecenter.location.href="center.php";
	} else {
		newWindow(addr,'GantiPasswordUser','419','200','resizeable=0,scrollbars=0,status=0,toolbar=0');
	}
}
function show_info(){
	document.getElementById('menu').style.display='none';
	document.getElementById('tentang').style.display='';
//	parent.content.location.href="jibasinfo.php";
}
function hide_info(){
	document.getElementById('menu').style.display='';
	document.getElementById('tentang').style.display='none';
	parent.content.location.href="referensi.php";
}

</script>

<style>

html
{
padding:0;
margin:0;
}

body
{
padding:0;
background-image:url(images/header.png);
background-repeat:repeat-x;
}

.header
{
width:100%;
height:100px;
margin-top:20px;
}

.logoHead
{
/*background-image:url(css/logo.png);
background-repeat:no-repeat;*/
width:50%;
height:100px;
margin-left:2%;
float:left;
margin-top:-20px;

}

.logoHead img
{

}

[class*="icon-"] {
  font-family: 'fontello';
  font-style: normal;
  font-size: 3em;
  speak: none;
}
.icon-home:after { content: "\2602"; } 
.icon-cog:after { content: "\2699"; } 
.icon-cw:after { content: "\27f3"; } 
.icon-location:after { content: "\2629"; } 

* { 
  -webkit-box-sizing: border-box; 
  -moz-box-sizing:    border-box; 
  box-sizing:         border-box; 
  margin: 0;
  padding: 0;
}



a {
  text-decoration: none;
  color: #DD6C4F;
}

a:hover {
  text-decoration:underline;
}

a:focus { 
  outline: none;
}

.nav {
  list-style: none;
  text-align: center;
  width:50%;
  position:relative;
  height:100px;
  margin-top:30px;
  margin-left:auto;
 
  
}

.nav li {
  position: relative;
  display: inline-block;  
  z-index:5; /* See: http://css-tricks.com/fighting-the-space-between-inline-block-elements/ */
  margin-left:3.8%;
  float:left;
}



.nav a {
  display: block;
  background-color: #f7f7f7;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#c7c5c5));
  background-image: -webkit-linear-gradient(top, #fff, #c7c5c5); 
  background-image: -moz-linear-gradient(top, #fff, #c7c5c5); 
  background-image: -ms-linear-gradient(top, #fff, #c7c5c5); 
  background-image: -o-linear-gradient(top, #fff, #c7c5c5); 
  color: #a7a7a7;
  padding:10px;
  position: relative;
  text-align: center;
  line-height: 40px;
  border-radius: 50%;
  box-shadow: 0px 3px 8px #3e969e, inset 0px 5px 8px #fff;
  z-index:5;
  border:#cde9ea solid 3px;
  
}

.nav a img
{
width:45px;
height:45px;
}


.nav a:hover {
  text-decoration: none;
  color: #555;
  background: #f5f5f5;
   box-shadow: 0px 3px 8px #144447, inset 0px 5px 8px #fff;
  
}


.tool-tip{
	color: #fff;
	margin-left:-20px;
	background-color: rgba( 0, 0, 0, .4);
	text-shadow: none;
	font-size: 11px;
	visibility: hidden;
	-webkit-border-radius: 7px; 
	-moz-border-radius: 7px; 
	-o-border-radius: 7px; 
	border-radius: 7px;	
	text-align: center;	
	opacity: 0;
	z-index: 999;
	padding:5px 10px 2px 10px;	
	position: absolute;
	cursor: default;
	-webkit-transition: all 200ms ease-in-out;
	-moz-transition: all 200ms ease-in-out;
	-ms-transition: all 200ms ease-in-out;
	-o-transition: all 200ms ease-in-out;
	transition: all 200ms ease-in-out;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	
}

.tool-tip,
.tool-tip.top{
	top: auto;
	bottom: 114%;
	left: 50%;		
}

.tool-tip.top:after,
.tool-tip:after{
	position: absolute;
	bottom: -12px;
	left: 50%;
	margin-left: -7px;
	content: ' ';
	height: 0px;
	width: 0px;
	border: 6px solid transparent;
    border-top-color: rgba( 0, 0, 0, .4);		
}

/* default heights, width and margin w/o Javscript */

.tool-tip,
.tool-tip.top{
	width:120px;;
	height: 25px;
	margin-left: -63px;
	
}
/* on hover of element containing tooltip default*/

*:not(.on-focus):hover > .tool-tip,
.on-focus input:focus + .tool-tip{
	visibility: visible;
	opacity: 1;
	-webkit-transition: all 200ms ease-in-out;
	-moz-transition: all 200ms ease-in-out;
	-ms-transition: all 200ms ease-in-out;
	-o-transition: all 200ms ease-in-out;
	transition: all 200ms ease-in-out;		
}


/* tool tip slide out */

*:not(.on-focus) > .tool-tip.slideIn,
.on-focus > .tool-tip{
	display: block;
}

.on-focus > .tool-tip.slideIn{
	z-index: -1;
}

.on-focus > input:focus + .tool-tip.slideIn{
	z-index: 1;
}

/* top slideIn */

*:not(.on-focus) > .tool-tip.slideIn,
*:not(.on-focus) > .tool-tip.slideIn.top,
.on-focus > .tool-tip.slideIn,
.on-focus > .tool-tip.slideIn.top{
	bottom: 50%;
}

*:not(.on-focus):hover > .tool-tip.slideIn,
*:not(.on-focus):hover > .tool-tip.slideIn.top,
.on-focus > input:focus + .tool-tip.slideIn,
.on-focus > input:focus + .tool-tip.slideIn.top{
	bottom: 85%;
}	



</style>


</head>
<body>  
<div class="header">

<div class="logoHead"><img src="css/logo.png" alt="a" /></div>
			<ul class="nav">
				<li><div class="tool-tip slideIn top">Setting</div><a href="referensi.php" target="content" > <img src="css/icon atas/setting.png" alt="s" /></a></li>
				<li><div class="tool-tip slideIn top">Guru & Pelajaran</div><a href="guru.php" target="content"><img src="css/icon atas/gp.png" alt="s" /></a></li>
                <li><div class="tool-tip slideIn top">Kesiswaan</div><a href="siswa.php" target="content"><img src="css/icon atas/elementary_school.png" alt="s" /></a></li>
				<li><div class="tool-tip slideIn top">Penilaian</div><a href="penilaian.php" target="content" ><img src="css/icon atas/penilaian.png" alt="s" /></a></li>
                <li><div class="tool-tip slideIn top">Logout</div><a href="javascript:logout();"><img src="css/icon atas/logout.png" alt="s" /></a></li>
			</ul>

</div>
</body>
