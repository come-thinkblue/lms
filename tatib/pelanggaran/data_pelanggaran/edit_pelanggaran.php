<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

$replid = $_REQUEST['replid'];

$idkategori = "";
if (isset($_REQUEST['idkategori']))
	$idkategori = $_REQUEST['idkategori'];
	
$jenis = "";
if (isset($_REQUEST['jenis']))
	$jenis = $_REQUEST['jenis'];
	
$keterangan = "";
if (isset($_REQUEST['keterangan']))
	$keterangan = $_REQUEST['keterangan'];

$tgl = date(d)."-".date(m)."-".date(Y);
if (isset($_REQUEST['tanggal']))
	$tgl= $_REQUEST['tanggal'];
	
$ERROR_MSG = "";
if (isset($_REQUEST['Simpan'])) {
$kategori = $_REQUEST['kategori'];
$jenis = $_REQUEST['jenis'];
$poin = $_REQUEST['poin'];
$tgl = $_REQUEST['tanggal'];
	OpenDb();
		$nama ="update data_pelanggaran set id_jenis='$jenis', tanggal='$tgl'  where id_pelanggaran='$replid'";
		$result = QueryDb($nama);
		CloseDb();
	
		if ($result) { ?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
<?php		}		
	
}

OpenDb();

$no = 1;
$nama =mysql_query("SELECT b.nama, c.nama, a.keterangan, c.id_kategori , b.id_jenis FROM data_pelanggaran a, jenis_pelanggaran b, kategori_pelanggaran c where a.id_pelanggaran='$replid' AND a.id_jenis=b.id_jenis AND b.id_kategori=c.id_kategori");
$hasil=mysql_fetch_array($nama);
//$jenis=$hasil[0];
//$kategori=$hasil[1];
$keterangan=$hasil[2];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar [Ubah Kategori Pelanggaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/cal2.js"></script>
<script language="javascript" src="../script/cal_conf2.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

function validate() {
	return validateEmptyText('kategori', 'kategori');
}

function change_jenis() {
	var idkategori = document.getElementById('idkategori').value;
	var replid = document.getElementById('replid').value;
	document.location.href = "edit_pelanggaran.php?idkategori="+idkategori+"&replid="+replid;
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
		.: Ubah Data Pelanggaran :.
	</div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
<form name="main" onSubmit="return validate()" action="edit_pelanggaran.php">
<input type="hidden" name="replid" id="replid" value="<?php echo$replid ?>" />
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
	<input type="hidden" name="replid" id="replid" value="<?php echo$replid?>" readonly/>
			<td><strong>Kategori</strong></td>
			<td>
				<select name="idkategori" id="idkategori" onChange="change_jenis()" style="width:200px" onKeyPress="return focusNext('departemen', event)">
<?php					$sql = "SELECT id_kategori, nama FROM kategori_pelanggaran ORDER BY id_kategori asc";
					OpenDb();
					$result = QueryDb($sql);
					while ($row = mysql_fetch_row($result)) {
						if ($idkategori == "")
							$idkategori = $row[0];	?>
							<option value="<?php echo$row[0]?>" <?php echo StringIsSelected($idkategori, $row[0])?> >
							<?php echo$row[1]?>
							</option>
				<?php	} ?>
				</select>
			</td>  
			
		</tr>
		<tr>
			<td><strong>Jenis Pelanggaran</strong></td>
			<td>
				<select name="jenis" id="jenis">
				<?php
				$sql=mysql_query("select * from jenis_pelanggaran where id_kategori='$idkategori'");
				while($data=mysql_fetch_array($sql)){
				?>
					<option value="<?php echo$data[0]?>"><?php echo$data[2]?></option>
				<?php
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td><strong>Tanggal</strong></td>
			<td><input type="text" onClick="showCal('Calendar2');" style="width:140px;" name="tanggal" id ="tanggal" readonly class="disabled" />
				<a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
			</td>
		</tr>
<tr>
	<td colspan="2" align="center"><br/><br/>
    <input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" />&nbsp;
    <input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
    </td>
</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>
<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo$ERROR_MSG?>');
</script>
<?php } ?>

<!-- Pilih inputan pertama -->

</body>
</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("kategori");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("jenis");
var sprytextfield1 = new Spry.Widget.ValidationTextField("poin");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("keterangan");
</script>