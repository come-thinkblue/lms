<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$nis_awal = $_REQUEST['nis_awal'];

$departemen = 0;
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
//echo 'departemen '.$departemen;

		
OpenDb();

$sql = "SELECT replid, departemen, nislama FROM riwayatdeptsiswa WHERE nis = '$nis_awal'";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
$dep[0] = array($row['departemen'], $nis_awal);
//$no[1] = $row['nislama'];
if ($row['nislama'] <> "") {
	$sql1 = "SELECT replid, departemen, nislama FROM riwayatdeptsiswa WHERE nis = '$row[nislama]'";
	$result1 = QueryDb($sql1);
	$row1 = @mysql_fetch_array($result1);	
	$dep[1] = array($row1['departemen'], $row['nislama']);
	//$no[2] = $row1['nislama'];	
	if ($row1['nislama'] <> "") {				
		$sql2 = "SELECT replid, departemen, nislama FROM riwayatdeptsiswa WHERE nis = '$row1[nislama]'";
		$result2 = QueryDb($sql2);
		$row2 = @mysql_fetch_array($result2);					
		$dep[2] = array($row2['departemen'],$row1['nislama']) ;
	}	
}		

$nis = $dep[$departemen][1];

$sql_ajaran = "SELECT DISTINCT(t.replid), t.tahunajaran FROM riwayatkelassiswa r, kelas k, tahunajaran t WHERE r.nis = '$nis' AND r.idkelas = k.replid AND k.idtahunajaran = t.replid ORDER BY t.aktif DESC";

$result_ajaran = QueryDb($sql_ajaran);
$k = 0;
while ($row_ajaran = @mysql_fetch_array($result_ajaran)) {
	$ajaran[$k] = array($row_ajaran['replid'],$row_ajaran['tahunajaran']);
	$k++;
}

$tahunajaran = $ajaran[0][0];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];

$sql_kls = "SELECT DISTINCT(r.idkelas), k.kelas, t.tingkat, k.idtahunajaran FROM riwayatkelassiswa r, kelas k, tingkat t WHERE r.nis = '$nis' AND r.idkelas = k.replid AND k.idtingkat = t.replid";

$result_kls = QueryDb($sql_kls);
$j = 0;
while ($row_kls = @mysql_fetch_array($result_kls)) {
	$kls[$j] = array($row_kls['idkelas'],$row_kls['kelas'],$row_kls['tingkat'],$row_kls['idtahunajaran']);
	if ($row_kls['idtahunajaran']==$tahunajaran)
		$kelas = $row_kls['idkelas'];
	$j++;
}

$page=0;
if (isset($_REQUEST['page']))
	$page = $_REQUEST['page'];
	
$hal=0;
if (isset($_REQUEST['hal']))
	$hal = $_REQUEST['hal'];
	


$varbaris=30;
if (isset($_REQUEST['varbaris']))
	$varbaris = $_REQUEST['varbaris'];	

$op = $_REQUEST['op'];
if ($op == "hapus") {
	OpenDb();
	$sql = "DELETE FROM data_pelanggaran WHERE id_pelanggaran = '$_REQUEST[replid]'";
	//QueryDb
	$result = QueryDb($sql);
	CloseDb();
	$page=0;
	$hal=0;

}	

//echo "<pre>";
//print_r($kls);
//echo "</pre>";



//$kelas = $kls[0][0];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Penilaian Pelajaran</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function change_dep() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;		
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}

function tambah(nis) {
	newWindow('tambah_pelanggaran.php?nis='+nis,'TambahPelanggaran','500','237','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function edit(replid) {
	newWindow('edit_pelanggaran.php?replid='+replid, 'UbahAngkatan','500','236','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function hapus(replid,nis) {

	if (confirm("Apakah anda yakin akan menghapus kategori ini?"))
		document.location.href = "lap_pelanggaran_menu.php?op=hapus&replid="+replid+"&nis_awal="+nis+"&page=<?php echo$page?>&hal=<?php echo$hal?>&varbaris=<?php echo$varbaris?>";
		//document.location.href = "lap_pelanggaran_menu.php?op=hapus&replid="+replid+"&nis_awal="+nis;
	}

function cetak() {
	var departemen = document.getElementById('departemen').value;
	//var total=document.getElementById("total").value;
	
	newWindow('cetak_data.php?departemen='+departemen+'&nis=<?php echo$nis?>&varbaris=<?php echo$varbaris?>&page=<?php echo$page?>' , 'CetakAngkatan','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
}
/*
function change() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	var kelas = document.getElementById("kelas").value;
	
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&kelas="+kelas+"&tahunajaran="+tahunajaran+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}
*/
function change_kls() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	var kelas = document.getElementById("kelas").value;
	
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&kelas="+kelas+"&tahunajaran="+tahunajaran+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}

function change_ta() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	//var kelas = document.getElementById("kelas").value;
	
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&tahunajaran="+tahunajaran+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}

function refresh(nis) {	
	document.location.reload();
	
}

function tampil(pelajaran,kelas,nis,departemen) {	
	parent.isi.location.href="lap_pelajaran_content.php?pelajaran="+pelajaran+"&kelas="+kelas+"&nis="+nis+"&departemen="+departemen;
}

</script>
</head>

<body>
<input type="hidden" name="nis" id="nis" value="<?php echo$nis?>">
<input type="hidden" name="nis_awal" id="nis_awal" value="<?php echo$nis_awal?>">
<table border="0" width="100%" align="center" >
<!-- TABLE CENTER -->
<tr>	
	<td width="38%"><strong>Departemen</strong></td>
    <td width="*"> 
    	<select name="departemen" id="departemen" onChange="change_dep()" style="width:120px">
		<?php for ($i=0;$i<sizeof($dep);$i++) {	?>        	
            <option value="<?php echo$dep[$i][0] ?>" <?php echo IntIsSelected($i, $departemen) ?> > <?php echo$dep[$i][0] ?> </option>
		<?php } ?>
		</select>
    </td>
</tr>

<tr>
	<td><strong>Tahun Ajaran</strong></td>
   	<td><select name="tahunajaran" id="tahunajaran" onchange="change_ta()" style="width:100px">
   		<?php for($k=0;$k<sizeof($ajaran);$k++) {?>
			<option value="<?php echo$ajaran[$k][0] ?>" <?php echo IntIsSelected($ajaran[$k][0], $tahunajaran) ?> > 
			<?php echo$ajaran[$k][1]?> </option>
		<?php } ?>
    	</select>    
	</td>
</tr>
<tr>
	<td><strong>Kelas </strong></td>
   	<td><select name="kelas" id="kelas" onchange="change_kls()" style="width:100px">
   		<?php for ($j=0;$j<sizeof($kls);$j++) {
				if ($kls[$j][3] == $tahunajaran) {
		?>
			<option value="<?php echo$kls[$j][0] ?>" <?php echo IntIsSelected($kls[$j][0], $kelas) ?> ><?php echo$kls[$j][2]." - ".$kls[$j][1] ?> </option>
		<?php 		}
			} ?>
    	</select>    
	</td>
</tr>   
<tr>
	<td colspan="2"><br />
	<?php 	OpenDb();		
		//$sql3 = "SELECT a.id_pelanggaran, a.id_jenis, a.keterangan, b.nama, b.poin, c.nama from data_pelanggaran a, jenis_pelanggaran b, kategori_pelanggaran c where a.nis='$nis' AND a.id_jenis=b.id_jenis AND b.id_kategori=c.id_kategori order by c.id_kategori asc";
		//echo '<br> sql '.$sql;
		$awal=mysql_query("select DISTINCT (a.id_kategori) from kategori_pelanggaran a, jenis_pelanggaran b, data_pelanggaran c where c.nis='$nis' AND b.id_jenis=c.id_jenis AND a.id_kategori=b.id_kategori order by a.id_kategori asc");
		$jumlah=mysql_num_rows($awal);
		if($jumlah>0){
	?>
		<table  border="0" cellpadding="2"  cellspacing="2" width="100%" >
			<tr>
				<td align="right" colspan="6">
					<a href="#" onClick="document.location.reload()"><img src="../images/ico/refresh.png" border="0" onmouseover="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
					<a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onmouseover="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp;
<?php					if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
					<a href="JavaScript:tambah(<?php echo$nis?>)"><img src="../images/ico/tambah.png" border="0" onmouseover="showhint('Tambah!', this, event, '50px')"/>&nbsp;Tambah Data</a>
<?php			} ?>    
				</td>
			</tr>
		</table>
	<?php
	while($hasil=mysql_fetch_array($awal)){
	    //echo"$hasil[0]";
	    $poin=0;
		$a=mysql_query("select nama from kategori_pelanggaran where id_kategori='$hasil[0]'");
		$b=mysql_fetch_array($a);
		
		?>
		
		<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
			<tr>
				<td colspan="6"><b><?php echo$b[0]?></b></td>
			</tr>
			<tr height="30"> 
						
				<td width="5%" class="header" align="center">No</td>
				<td width="30%" class="header" align="center">Jenis Pelanggaran</td>
				<td width="15%" class="header" align="center">Kategori</td>
				<td width="10%" class="header" align="center">Poin</td>
				<td width="20%" class="header" align="center">Tanggal</td>
				<td width="20%" class="header" align="center">Aksi</td>
			</tr>
		<?php
		$sql3 = mysql_query("SELECT a.id_pelanggaran, a.id_jenis, a.tanggal, b.nama, b.poin, c.nama from data_pelanggaran a, jenis_pelanggaran b, kategori_pelanggaran c where a.nis='$nis' AND a.id_jenis=b.id_jenis AND b.id_kategori=c.id_kategori AND c.id_kategori='$hasil[0]'");
		$no=1;
		while($data=mysql_fetch_array($sql3)){
		$poin2=$data[4];
		$poin=$poin+$poin2;
		?> 
			<tr height="25">   	
				<td align="center"><?php echo$no ?></td>
				<td align="center"><?php echo$data[3]?></td>       
				<td align="center"><?php echo$data[5]?></td>
				<td align="center"><?php echo$data[4]?></td>
				<td align="center"><?php echo$data[2]?></td>
				<td align="center">
					<a href="JavaScript:edit(<?php echo$data[0] ?>)"><img src="../images/ico/ubah.png" border="0" onmouseover="showhint('Ubah Angkatan!', this, event, '80px')"/></a>&nbsp;
					<a href="JavaScript:hapus(<?php echo$data[0] ?>,<?php echo$nis?>)"><img src="../images/ico/hapus.png" border="0" onmouseover="showhint('Hapus Kategori!', this, event, '80px')"/></a>
				</td>
<?php		
			$no++?>
			</tr>
		
		<?php
		}
		
		if($b[0]=="Tanggung Jawab"){
			if(($poin>=0)&&($poin<=10)){
			$predikat="Amat Baik";
			}else if(($poin>=11)&&($poin<=20)){
			$predikat="Baik";
			}else if(($poin>=21)&&($poin<=30)){
			$predikat="Cukup";
			}else if(($poin>=31)&&($poin<=75)){
			$predikat="Kurang";
			}else if(($poin>=76)&&($poin<=100)){
			$predikat="Sangat Kurang";
			}
		} else if($b[0]=="Disiplin"){
			if(($poin>=0)&&($poin<=10)){
			$predikat="Amat Baik";
			}else if(($poin>=11)&&($poin<=30)){
			$predikat="Baik";
			}else if(($poin>=31)&&($poin<=50)){
			$predikat="Cukup";
			}else if(($poin>=51)&&($poin<=75)){
			$predikat="Kurang";
			}else if(($poin>=76)&&($poin<=100)){
			$predikat="Sangat Kurang";
			}
		} else if($b[0]=="Kebersihan"){
			if(($poin>=0)&&($poin<=15)){
			$predikat="Amat Baik";
			}else if(($poin>=16)&&($poin<=30)){
			$predikat="Baik";
			}else if(($poin>=31)&&($poin<=50)){
			$predikat="Cukup";
			}else if(($poin>=51)&&($poin<=75)){
			$predikat="Kurang";
			}else if(($poin>=76)&&($poin<=100)){
			$predikat="Sangat Kurang";
			}
		}
		?>
			<tr>
				<td colspan="4" align="right"><b>Total Poin Pelanggaran :<?php echo$poin?></b></td><td colspan="2" align="right"><b>Predikat :<?php echo$predikat?></b></td>
			</tr>
			
		</table>
		
		<?php
	}
	 	OpenDb();			
	
	?>
	
	  <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>
	<?}else{
	?>
	<br/><br/>
	<div align="center">
	<b>Data Pelanggaran Masih Kosong</b><br/>
	Silahkan <a href="JavaScript:tambah(<?php echo$nis?>)">Tambah Data</a
	</div>
	<?php
	}?>
	</td>	
</tr>
<!-- END TABLE CENTER -->    
</table> 
   

</body>
</html>