<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$departemen = $_REQUEST['departemen'];
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];
$nis=$_REQUEST['nis'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Jenis Pelanggaran]</title>
</head>

<body>

<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>

<center>
  <font size="4"><strong>DATA PELANGGARAN</strong></font><br />
 </center><br /><br />

<br />
	<strong>Departemen <?php echo$departemen?></strong></font>
<br /><br />
		<?php
		$awal=mysql_query("select DISTINCT (a.id_kategori) from kategori_pelanggaran a, jenis_pelanggaran b, data_pelanggaran c where c.nis='$nis' AND b.id_jenis=c.id_jenis AND a.id_kategori=b.id_kategori order by a.id_kategori asc");
		$jumlah=mysql_num_rows($awal);
		?>
		
		<?php
	while($hasil=mysql_fetch_array($awal)){
	    //echo"$hasil[0]";
	    $poin=0;
		$a=mysql_query("select nama from kategori_pelanggaran where id_kategori='$hasil[0]'");
		$b=mysql_fetch_array($a);
		
		?>
		
		<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
			<tr>
				<td colspan="6"><b><?php echo$b[0]?></b></td>
			</tr>
			<tr height="30"> 
						
				<td width="5%" class="header" align="center">No</td>
				<td width="30%" class="header" align="center">Jenis Pelanggaran</td>
				<td width="15%" class="header" align="center">Kategori</td>
				<td width="10%" class="header" align="center">Poin</td>
				<td width="20%" class="header" align="center">Tanggal</td>
				<td width="20%" class="header" align="center">Aksi</td>
			</tr>
		<?php
		$sql3 = mysql_query("SELECT a.id_pelanggaran, a.id_jenis, a.tanggal, b.nama, b.poin, c.nama from data_pelanggaran a, jenis_pelanggaran b, kategori_pelanggaran c where a.nis='$nis' AND a.id_jenis=b.id_jenis AND b.id_kategori=c.id_kategori AND c.id_kategori='$hasil[0]'");
		$no=1;
		while($data=mysql_fetch_array($sql3)){
		$poin2=$data[4];
		$poin=$poin+$poin2;
		?> 
			<tr height="25">   	
				<td align="center"><?php echo$no ?></td>
				<td align="center"><?php echo$data[3]?></td>       
				<td align="center"><?php echo$data[5]?></td>
				<td align="center"><?php echo$data[4]?></td>
				<td align="center"><?php echo$data[2]?></td>
				<td align="center">
					<a href="JavaScript:edit(<?php echo$data[0] ?>)"><img src="../images/ico/ubah.png" border="0" onmouseover="showhint('Ubah Angkatan!', this, event, '80px')"/></a>&nbsp;
					<a href="JavaScript:hapus(<?php echo$data[0] ?>,<?php echo$nis?>)"><img src="../images/ico/hapus.png" border="0" onmouseover="showhint('Hapus Kategori!', this, event, '80px')"/></a>
				</td>
<?php		
			$no++?>
			</tr>
		
		<?php
		}
		
		if($b[0]=="Tanggung Jawab"){
			if(($poin>=0)&&($poin<=10)){
			$predikat="Amat Baik";
			}else if(($poin>=11)&&($poin<=20)){
			$predikat="Baik";
			}else if(($poin>=21)&&($poin<=30)){
			$predikat="Cukup";
			}else if(($poin>=31)&&($poin<=75)){
			$predikat="Kurang";
			}else if(($poin>=76)&&($poin<=100)){
			$predikat="Sangat Kurang";
			}
		} else if($b[0]=="Disiplin"){
			if(($poin>=0)&&($poin<=10)){
			$predikat="Amat Baik";
			}else if(($poin>=11)&&($poin<=30)){
			$predikat="Baik";
			}else if(($poin>=31)&&($poin<=50)){
			$predikat="Cukup";
			}else if(($poin>=51)&&($poin<=75)){
			$predikat="Kurang";
			}else if(($poin>=76)&&($poin<=100)){
			$predikat="Sangat Kurang";
			}
		} else if($b[0]=="Kebersihan"){
			if(($poin>=0)&&($poin<=15)){
			$predikat="Amat Baik";
			}else if(($poin>=16)&&($poin<=30)){
			$predikat="Baik";
			}else if(($poin>=31)&&($poin<=50)){
			$predikat="Cukup";
			}else if(($poin>=51)&&($poin<=75)){
			$predikat="Kurang";
			}else if(($poin>=76)&&($poin<=100)){
			$predikat="Sangat Kurang";
			}
		}
		?>
			<tr>
				<td colspan="4" align="right"><b>Total Poin Pelanggaran :<?php echo$poin?></b></td><td colspan="2" align="right"><b>Predikat :<?php echo$predikat?></b></td>
			</tr>
			
		</table>
		
		<?php
	}
	 	OpenDb();			
	
	?>
		
	</td>
</tr>
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>