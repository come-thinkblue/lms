<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

$replid = $_REQUEST['replid'];
$ERROR_MSG = "";
if (isset($_REQUEST['Simpan'])) {
$kategori = $_REQUEST['kategori'];
$jenis = $_REQUEST['jenis'];
$poin = $_REQUEST['poin'];
$keterangan = $_REQUEST['keterangan'];
	OpenDb();
		$nama ="update jenis_pelanggaran set id_kategori='$kategori', nama='$jenis', poin='$poin' where id_jenis='$replid'";
		$result = QueryDb($nama);
		CloseDb();
	
		if ($result) { ?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
<?php		}		
	
}

OpenDb();

$no = 1;
$nama =mysql_query("SELECT * FROM jenis_pelanggaran where id_jenis='$replid'");
$hasil=mysql_fetch_array($nama);
$id_kategori=$hasil['id_kategori'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar [Ubah Kategori Pelanggaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

function validate() {
	return validateEmptyText('kategori', 'kategori');
}
function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4"  onload="document.getElementById('kategori').focus();">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
		.: Ubah Jenis Pelanggaran :.
	</div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
<form name="main" onSubmit="return validate()" action="edit_jenis.php">
<input type="hidden" name="replid" id="replid" value="<?php echo$replid ?>" />
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
	<td><strong>Kategori</strong></td>
	<td>
	<select name="kategori" class="disabled" id="kategori">
	<?php
	$query2=mysql_query("select nama from kategori_pelanggaran where id_kategori='$id_kategori'");
	$hasil2=mysql_fetch_array($query2);
	?>
	<option value="<?php echo$id_kategori?>"><?php echo$hasil2['nama']?></option>
	<?php
	$query =mysql_query("SELECT id_kategori, nama FROM kategori_pelanggaran order by id_kategori asc ");
	while($hasil3=mysql_fetch_array($query)){
	?>
	
	<option value="<?php echo$hasil3['id_kategori']?>"><?php echo$hasil3['nama']?></option>
	<?php
	}
	?>
	</select>
	</td>
<tr>
		<tr>
			<td valign="top"><strong>Jenis Pelanggaran</strong></td>
			<td><textarea name="jenis"  id="jenis" rows="3" class="disabled" cols="30"><?php echo$hasil['nama']?></textarea></td>  
		</tr>
		<tr>
			<td><strong>Poin</strong></td>
			<td><input type="text" name="poin" id="poin" class="disabled" value=<?php echo$hasil['poin']?>></td>  
		</tr>
<tr>
	<td colspan="2" align="center">
    <input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" />&nbsp;
    <input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
    </td>
</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>
<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo$ERROR_MSG?>');
</script>
<?php } ?>

<!-- Pilih inputan pertama -->

</body>
</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("kategori");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("jenis");
var sprytextfield1 = new Spry.Widget.ValidationTextField("poin");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("keterangan");
</script>