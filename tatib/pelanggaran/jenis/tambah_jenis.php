<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');


$kategori = "";
if (isset($_REQUEST['kategori']))
	$kategori = CQ($_REQUEST['kategori']);
	
if (isset($_REQUEST['jenis']))
	$jenis = CQ($_REQUEST['jenis']);

if (isset($_REQUEST['poin']))
	$poin = CQ($_REQUEST['poin']);
	

$ERROR_MSG = "";
if (isset($_REQUEST['Simpan'])) {
	OpenDb();
		
		$sql = "INSERT INTO jenis_pelanggaran values ('','$kategori','$jenis','$poin')";
		$result = QueryDb($sql);
		CloseDb();			
		if ($result) { 	
		
		?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
<?php		}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar [Tambah Jenis Pelanggaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

function validate() {
	return validateEmptyText('kategori', 'kategori') && validateEmptyText('jenis', 'jenis')
			&& validateEmptyText('poin', 'poin')&& validateEmptyText('keterangan', 'keterangan')&&
		   validateMaxText('keterangan', 255, 'Keterangan')&&
		   validateMaxText('jenis', 255, 'jenis');			
}
function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('kategori').focus();">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
    .: Tambah Jenis Pelanggaran :.
    </div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
<form name="main" onSubmit="return validate()">
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
	<?php
		
        $nama =mysql_query("SELECT id_jenis FROM jenis_pelanggaran ");
		$jumlah=mysql_num_rows($nama);
	?>
	<td width="120"><strong>No</strong></td>
    <td><input type="text" class="disabled" size="5" value="<?php echo$jumlah+1 ?>" readonly/></td>
</tr>
<tr>
	<td><strong>Kategori</strong></td>
	<td>
	<select name="kategori" id="kategori" class="disabled" >
	<?php
	$query =mysql_query("SELECT id_kategori, nama FROM kategori_pelanggaran order by id_kategori asc ");
		while($hasil=mysql_fetch_array($query)){
	?>
	<option value="<?php echo$hasil['id_kategori']?>"><?php echo$hasil['nama']?></option>
	<?php
		}
	?>
	</select>
    </td>
</tr>
<tr>
			<td valign="top"><strong>Jenis Pelanggaran</strong></td>
			<td><textarea name="jenis" id="jenis" class="disabled" rows="3" cols="30"></textarea></td>  
		</tr>
		<tr>
			<td><strong>Poin</strong></td>
			<td><input type="text" id="poin" class="disabled" name="poin"></td>  
		</tr>
<tr>
	<td colspan="2" align="center">
    <input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" />&nbsp;
    <input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
    </td>
</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>

<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo$ERROR_MSG?>');
</script>
<?php } ?>

<!-- Pilih inputan pertama -->

</body>
</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("kategori");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("keterangan");
var sprytextfield1 = new Spry.Widget.ValidationTextField("poin");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("jenis");
</script>