-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2016 at 05:07 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `kurikulum`
--

CREATE TABLE IF NOT EXISTS `kurikulum` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kurikulum` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_kurikulum_departemen` (`departemen`),
  KEY `IX_kurikulum_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kurikulum`
--

INSERT INTO `kurikulum` (`replid`, `kurikulum`, `departemen`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'KTSP', 'SMAN 1 Malang', '', NULL, NULL, NULL, '2016-03-20 19:49:25', 0, 0),
(2, 'K13', 'SMAN 1 Malang', '', NULL, NULL, NULL, '2016-03-21 19:49:35', 0, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD CONSTRAINT `kurikulum_ibfk_1` FOREIGN KEY (`departemen`) REFERENCES `departemen` (`departemen`) ON DELETE CASCADE ON UPDATE CASCADE;
