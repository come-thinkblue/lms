-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2016 at 05:06 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `sifat_ktsp` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sifat_k13` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_daftarpelajaran_kode` (`kode`),
  KEY `FK_pelajaran_departemen` (`departemen`),
  KEY `IX_pelajaran_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`replid`, `kode`, `nama`, `departemen`, `sifat_ktsp`, `sifat_k13`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(45, 'FIS', 'FISIKA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-04-22 19:49:30', 52115, 0),
(46, 'MAT', 'Matematika', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-02 11:28:24', 33723, 0),
(47, 'BIND', 'BAHASA DAN SASTRA INDONESIA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:26:13', 2364, 0),
(48, 'OR', 'PENDIDIKAN JASMANI DAN KESEHATAN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:06:39', 49741, 0),
(49, 'SEN', 'SENI BUDAYA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:09:33', 18143, 0),
(50, 'BIG', 'BAHASA INGGRIS', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:25:53', 61297, 0),
(51, 'TIK', 'Teknik Informatika', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-02 11:29:39', 6305, 0),
(52, 'PKN', 'PKN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:04:07', 7666, 0),
(53, 'PAI', 'PAI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:03:45', 28139, 0),
(54, 'PAKR', 'PENDIDIKAN AGAMA KRISTEN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:04:39', 49479, 0),
(55, 'PAKA', 'PENDIDIKAN AGAMA KATOLIK', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:05:05', 10473, 0),
(56, 'SEJ', 'SEJARAH', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:06:02', 21886, 0),
(57, 'KIM', 'KIMIA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:02', 56879, 0),
(58, 'BIO', 'BIOLOGI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:17', 9893, 0),
(59, 'AKUN', 'AKUNTANSI', 'SMAN 1 Malang', 1, 0, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:29', 20225, 0),
(60, 'GEO', 'GEOGRAFI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:43', 41278, 0),
(61, 'ASTR', 'ASTRONOMI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:08:08', 2227, 0),
(62, 'ANTR', 'ANTROPOLOGI', 'SMAN 1 Malang', 1, 0, 1, 'test', NULL, NULL, NULL, '2013-02-03 22:08:26', 9366, 0),
(63, 'JEP', 'BAHASA JEPANG', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:08:44', 24135, 0),
(64, 'PRC', 'BAHASA PERANCIS', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:09:03', 5797, 0),
(65, 'MDR', 'BAHASA MANDARIN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:09:17', 33173, 0),
(66, 'JER', 'BAHASA JERMAN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:10:14', 25682, 0),
(67, 'ARB', 'BAHASA ARAB', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:10:32', 35580, 0),
(68, 'SOS', 'SOSIOLOGI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:11:12', 29486, 0),
(70, 'EKO', 'EKONOMI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:40:32', 64095, 0),
(71, 'BK', 'Bimbingan Konseling', 'SMAN 1 Malang', 0, 1, 1, '', NULL, NULL, NULL, '2013-02-13 18:58:22', 40584, 0),
(72, 'KW', 'Kewirausahaan', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-13 19:18:49', 47795, 0),
(73, 'NEW', 'New2', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2016-03-21 15:32:55', 0, 0),
(74, 'Coba', 'Coba Campuran', 'SMAN 1 Malang', 0, 1, 1, '', NULL, NULL, NULL, '2016-03-22 15:43:23', 0, 0);
