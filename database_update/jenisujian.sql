-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2016 at 09:21 AM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenisujian`
--

CREATE TABLE IF NOT EXISTS `jenisujian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenisujian` varchar(50) NOT NULL,
  `idkurikulum` int(1) unsigned NOT NULL DEFAULT '1',
  `iddasarpenilaian` varchar(10) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned DEFAULT '0',
  `issync` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jenisujian_pelajaran` (`idkurikulum`),
  KEY `IX_jenisujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `jenisujian`
--

INSERT INTO `jenisujian` (`replid`, `jenisujian`, `idkurikulum`, `iddasarpenilaian`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'Ujian AKhir Sekolah', 1, 'KOG', '', 'UAS', NULL, NULL, '2016-04-03 17:52:40', 0, 0),
(2, 'Ujian Tengah Semester', 1, 'KOG', 'Tesing', 'UTS', NULL, NULL, '2016-04-03 17:56:50', 0, 0),
(3, 'Ujian Akhir Sekolah', 2, 'KI1', NULL, 'UAS', NULL, NULL, '2016-04-03 18:13:01', 0, 0),
(5, 'Ujian Tengah Semester', 2, 'KI1', 'tes', 'UTS', NULL, NULL, '2016-04-03 18:25:09', 0, 0),
(8, 'Tugas', 2, 'KI1', 'tugas siswa', 'TUG', NULL, NULL, '2016-04-03 18:43:05', 0, 0),
(9, 'Sikap', 1, 'AF', '1', 'SI', NULL, NULL, '2016-04-03 18:44:26', 0, 0);
