/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : 0_man_dbuser

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-04-27 20:34:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `adminsiswa`
-- ----------------------------
DROP TABLE IF EXISTS `adminsiswa`;
CREATE TABLE `adminsiswa` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isnew` varchar(45) NOT NULL DEFAULT '1',
  `haschange` varchar(45) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '',
  `lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adminsiswa
-- ----------------------------

-- ----------------------------
-- Table structure for `hakakses`
-- ----------------------------
DROP TABLE IF EXISTS `hakakses`;
CREATE TABLE `hakakses` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `modul` varchar(100) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `departemen` varchar(50) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `theme` tinyint(2) unsigned DEFAULT '1',
  `lastlogin` datetime DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`),
  KEY `FK_hakakses_login` (`login`),
  CONSTRAINT `FK_hakakses_login` FOREIGN KEY (`login`) REFERENCES `login` (`login`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hakakses
-- ----------------------------
INSERT INTO `hakakses` VALUES (null, null, null, '23', '101', 'INFOGURU', '1', null, null, '1', '2010-04-16 10:01:22', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '25', '1', 'SIMAKA', '2', 'MAN Kota Blitar', 'wew', '1', '2016-04-21 12:56:55', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '26', '40', 'INFOGURU', '1', 'SMA', null, '1', '2016-01-31 21:53:32', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '27', '40', 'SIMAKA', '1', null, '', '1', '2016-03-25 19:06:44', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '28', '31', 'SIMAKA', '2', 'SMA', 'Staff', '1', null, '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '29', '123456', 'SIMTAKA', '1', '', '', '1', null, '1', '-1', null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '30', '2', 'SIMTAKA', '2', 'sma', '', '1', null, '1', '1', null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '31', '1', 'KEUANGAN', '1', null, '', '1', '2016-04-13 21:41:43', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '32', '123456789', 'SIMPEG', '1', null, '', '1', null, '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '34', '68', 'INFOGURU', '1', null, null, '1', '2013-03-21 11:43:36', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '36', '68', 'SIMAKA', '1', null, '', '1', '2013-04-19 21:40:17', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '38', '17', 'INFOGURU', '1', null, null, '1', '2013-02-15 00:18:55', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '39', '5', 'EMA', '1', null, '', '1', '2016-03-25 19:00:04', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '40', '68', 'SIMTAKA', '2', 'SMA Negeri 1 Malang', '', '1', '2013-02-23 18:03:42', '1', '1', null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '41', '68', 'KEUANGAN', '2', 'SMAN 1 Malang', '', '1', '2016-01-26 18:37:23', '1', null, null, null);
INSERT INTO `hakakses` VALUES (null, null, null, '42', '2', 'SIMAKA', '2', 'SMAN 1 Malang', '', '1', null, '1', null, null, null);

-- ----------------------------
-- Table structure for `hakaksesinfosiswa`
-- ----------------------------
DROP TABLE IF EXISTS `hakaksesinfosiswa`;
CREATE TABLE `hakaksesinfosiswa` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `theme` tinyint(2) unsigned DEFAULT '1',
  `lastlogin` datetime DEFAULT NULL,
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`),
  KEY `FK_hakaksesinfosiswa_nis` (`nis`),
  CONSTRAINT `FK_hakaksesinfosiswa_nis` FOREIGN KEY (`nis`) REFERENCES `jbsakad`.`siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hakaksesinfosiswa
-- ----------------------------

-- ----------------------------
-- Table structure for `hakakseskeuangan`
-- ----------------------------
DROP TABLE IF EXISTS `hakakseskeuangan`;
CREATE TABLE `hakakseskeuangan` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `modul` varchar(100) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hakakseskeuangan
-- ----------------------------

-- ----------------------------
-- Table structure for `hakaksessimaka`
-- ----------------------------
DROP TABLE IF EXISTS `hakaksessimaka`;
CREATE TABLE `hakaksessimaka` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `modul` varchar(100) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`),
  KEY `FK_hakakses_nip` (`login`),
  KEY `FK_hakakses_modul` (`modul`),
  CONSTRAINT `FK_hakakses_modul` FOREIGN KEY (`modul`) REFERENCES `modul` (`modul`) ON UPDATE CASCADE,
  CONSTRAINT `FK_hakakses_nip` FOREIGN KEY (`login`) REFERENCES `jbssdm`.`pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hakaksessimaka
-- ----------------------------

-- ----------------------------
-- Table structure for `landlord`
-- ----------------------------
DROP TABLE IF EXISTS `landlord`;
CREATE TABLE `landlord` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isnew` varchar(45) NOT NULL DEFAULT '1',
  `haschange` varchar(45) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of landlord
-- ----------------------------
INSERT INTO `landlord` VALUES ('GMS2', 'BD', 'AT', '1', '1', '0', '0c4a442cf14867d5946e911c0b819f96', '2016-04-23 15:02:50', null, null, null);

-- ----------------------------
-- Table structure for `login`
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`),
  KEY `FK_login_nip` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES (null, null, null, '7', '101', '5f4dcc3b5aa765d61d8327deb882cf99', '', '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '8', '1', 'c4ca4238a0b923820dcc509a6f75849b', null, '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '9', '40', '0c4a442cf14867d5946e911c0b819f96', '', '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '10', '31', '1253208465b1efa876f982d8a9e73eef', null, '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '11', '123456', '6a73f78adfb05448583c7e876439d896', null, '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '12', '2', '0c4a442cf14867d5946e911c0b819f96', '', '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '13', '1', '0c4a442cf14867d5946e911c0b819f96', null, '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '14', '123456789', '827ccb0eea8a706c4c34a16891f84e7b', null, '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '15', '68', 'fc3f318fba8b3c1502bece62a27712df', '', '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '16', '5', '8561863b55faf85b9ad67c52b3b851ac', null, '1', null, null, null);
INSERT INTO `login` VALUES (null, null, null, '17', '17', '379563d4cc020b27338863c063b9368d', '', '1', null, null, null);

-- ----------------------------
-- Table structure for `loginsiswa`
-- ----------------------------
DROP TABLE IF EXISTS `loginsiswa`;
CREATE TABLE `loginsiswa` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `theme` tinyint(2) NOT NULL DEFAULT '1',
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`),
  KEY `FK_loginsiswa_nis` (`login`),
  CONSTRAINT `FK_loginsiswa_nis` FOREIGN KEY (`login`) REFERENCES `jbsakad`.`siswa` (`nis`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of loginsiswa
-- ----------------------------

-- ----------------------------
-- Table structure for `lokasi`
-- ----------------------------
DROP TABLE IF EXISTS `lokasi`;
CREATE TABLE `lokasi` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isnew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `haschange` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lokasi` varchar(45) NOT NULL DEFAULT '',
  `singkatan` varchar(3) NOT NULL DEFAULT '',
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`replid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lokasi
-- ----------------------------

-- ----------------------------
-- Table structure for `modul`
-- ----------------------------
DROP TABLE IF EXISTS `modul`;
CREATE TABLE `modul` (
  `clientid` varchar(5) DEFAULT NULL,
  `region` varchar(5) DEFAULT NULL,
  `location` varchar(5) DEFAULT NULL,
  `isnew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `haschange` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `modul` varchar(100) NOT NULL,
  `info1` varchar(50) DEFAULT NULL,
  `info2` varchar(50) DEFAULT NULL,
  `info3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`modul`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modul
-- ----------------------------
