/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : 0_man_dbpegawai

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-04-27 20:34:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `agama`
-- ----------------------------
DROP TABLE IF EXISTS `agama`;
CREATE TABLE `agama` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agama` varchar(20) NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`agama`),
  UNIQUE KEY `UX_agama` (`replid`),
  KEY `IX_agama_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agama
-- ----------------------------
INSERT INTO `agama` VALUES ('2', 'Budha', '2', null, null, null, '2013-04-11 11:03:51', '0', '0');
INSERT INTO `agama` VALUES ('1', 'Islam', '1', null, null, null, '2013-04-11 11:03:39', '0', '0');

-- ----------------------------
-- Table structure for `bagianpegawai`
-- ----------------------------
DROP TABLE IF EXISTS `bagianpegawai`;
CREATE TABLE `bagianpegawai` (
  `bagian` varchar(50) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bagian`),
  KEY `UXBagianPegawai` (`replid`),
  KEY `IX_bagianpegawai_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bagianpegawai
-- ----------------------------
INSERT INTO `bagianpegawai` VALUES ('Akademik', '1', '1', null, null, null, '2010-03-02 10:08:32', '53202', '0');
INSERT INTO `bagianpegawai` VALUES ('Non Akademik', '2', '2', null, null, null, '2010-03-02 10:08:32', '19723', '0');

-- ----------------------------
-- Table structure for `diklat`
-- ----------------------------
DROP TABLE IF EXISTS `diklat`;
CREATE TABLE `diklat` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(10) unsigned NOT NULL,
  `allowselect` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `diklat` varchar(45) NOT NULL,
  `tingkat` tinyint(3) unsigned NOT NULL,
  `jenis` varchar(1) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_diklat_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of diklat
-- ----------------------------
INSERT INTO `diklat` VALUES ('11', '0', '1', 'DIKLAT STRUKTURAL', '0', 'S', null, null, null, '2012-06-19 07:00:00', '23097', '0');
INSERT INTO `diklat` VALUES ('12', '0', '0', 'DIKLAT FUNGSIONAL', '0', 'F', null, null, null, '2012-06-19 07:00:00', '29220', '0');
INSERT INTO `diklat` VALUES ('26', '11', '1', 'Diklat Kepemimpinan', '2', '', null, null, null, '2012-06-19 07:00:00', '11276', '0');
INSERT INTO `diklat` VALUES ('28', '12', '1', 'Diklat Pengajaran', '2', '', null, null, null, '2012-06-19 07:00:00', '34253', '0');

-- ----------------------------
-- Table structure for `eselon`
-- ----------------------------
DROP TABLE IF EXISTS `eselon`;
CREATE TABLE `eselon` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eselon` varchar(15) NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`eselon`),
  UNIQUE KEY `UX_eselon` (`replid`),
  KEY `IX_eselon_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eselon
-- ----------------------------
INSERT INTO `eselon` VALUES ('5', '(Tidak Ada)', '1', '1', null, null, null, '2012-06-19 07:00:00', '6374', '0');
INSERT INTO `eselon` VALUES ('1', 'Eselon I', '2', '0', null, null, null, '2012-06-19 07:00:00', '60172', '0');
INSERT INTO `eselon` VALUES ('2', 'Eselon II', '3', '0', null, null, null, '2012-06-19 07:00:00', '19617', '0');
INSERT INTO `eselon` VALUES ('3', 'Eselon III', '4', '0', null, null, null, '2012-06-19 07:00:00', '48627', '0');
INSERT INTO `eselon` VALUES ('4', 'Eselon IV', '5', '0', null, null, null, '2012-06-19 07:00:00', '53226', '0');

-- ----------------------------
-- Table structure for `golongan`
-- ----------------------------
DROP TABLE IF EXISTS `golongan`;
CREATE TABLE `golongan` (
  `replid` char(10) NOT NULL,
  `golongan` varchar(14) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL,
  `nama` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`golongan`),
  UNIQUE KEY `UX_golongan` (`replid`),
  KEY `IX_golongan_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of golongan
-- ----------------------------
INSERT INTO `golongan` VALUES ('18', '(Tidak Ada)', '1', '1', 'Tidak Ada Golongan', null, null, null, '2012-06-19 07:00:00', '54715', '0');
INSERT INTO `golongan` VALUES ('00', 'Gol 1', '1', '0', 'Golongan 1', null, null, null, '2016-03-25 21:31:37', '0', '0');
INSERT INTO `golongan` VALUES ('01', 'IIA', '2', '2', 'Pengatur Muda', null, null, null, '2012-06-19 07:00:00', '61109', '0');
INSERT INTO `golongan` VALUES ('02', 'IIB', '2', '3', 'Pengatur Muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '40431', '0');
INSERT INTO `golongan` VALUES ('03', 'IIC', '2', '4', 'Pengatur', null, null, null, '2012-06-19 07:00:00', '18827', '0');
INSERT INTO `golongan` VALUES ('04', 'IID', '2', '5', 'Pengatur Tingkat 1', null, null, null, '2012-06-19 07:00:00', '38373', '0');
INSERT INTO `golongan` VALUES ('05', 'IIIA', '3', '6', 'Penata Muda', null, null, null, '2012-06-19 07:00:00', '4324', '0');
INSERT INTO `golongan` VALUES ('06', 'IIIB', '3', '7', 'Penata muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '37558', '0');
INSERT INTO `golongan` VALUES ('07', 'IIIC', '3', '8', 'Penata', null, null, null, '2012-06-19 07:00:00', '43759', '0');
INSERT INTO `golongan` VALUES ('08', 'IIID', '3', '9', 'Penata Tingkat 1', null, null, null, '2012-06-19 07:00:00', '40589', '0');
INSERT INTO `golongan` VALUES ('09', 'IVA', '4', '10', 'Pembina', null, null, null, '2012-06-19 07:00:00', '6140', '0');
INSERT INTO `golongan` VALUES ('10', 'IVB', '4', '11', 'Pembina Tingkat 1', null, null, null, '2012-06-19 07:00:00', '39991', '0');
INSERT INTO `golongan` VALUES ('11', 'IVC', '4', '12', 'Pembina Utama Muda', null, null, null, '2012-06-19 07:00:00', '50477', '0');
INSERT INTO `golongan` VALUES ('12', 'IVD', '4', '13', 'Pembina Utama Madya', null, null, null, '2012-06-19 07:00:00', '1348', '0');
INSERT INTO `golongan` VALUES ('13', 'IVE', '4', '14', 'Pembuna Utama', null, null, null, '2012-06-19 07:00:00', '51900', '0');

-- ----------------------------
-- Table structure for `golongan_copy`
-- ----------------------------
DROP TABLE IF EXISTS `golongan_copy`;
CREATE TABLE `golongan_copy` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `golongan` varchar(14) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL,
  `nama` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`golongan`),
  UNIQUE KEY `UX_golongan` (`replid`),
  KEY `IX_golongan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of golongan_copy
-- ----------------------------
INSERT INTO `golongan_copy` VALUES ('18', '(Tidak Ada)', '1', '1', 'Tidak Ada Golongan', null, null, null, '2012-06-19 07:00:00', '54715', '0');
INSERT INTO `golongan_copy` VALUES ('1', 'IA', '1', '2', 'Juru Muda', null, null, null, '2012-06-19 07:00:00', '48370', '0');
INSERT INTO `golongan_copy` VALUES ('2', 'IB', '1', '3', 'Juru Muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '12175', '0');
INSERT INTO `golongan_copy` VALUES ('3', 'IC', '1', '4', 'Juru', null, null, null, '2012-06-19 07:00:00', '46822', '0');
INSERT INTO `golongan_copy` VALUES ('4', 'ID', '1', '5', 'Juru Tingkat 1', null, null, null, '2012-06-19 07:00:00', '997', '0');
INSERT INTO `golongan_copy` VALUES ('5', 'IIA', '2', '6', 'Pengatur Muda', null, null, null, '2012-06-19 07:00:00', '61109', '0');
INSERT INTO `golongan_copy` VALUES ('6', 'IIB', '2', '7', 'Pengatur Muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '40431', '0');
INSERT INTO `golongan_copy` VALUES ('7', 'IIC', '2', '8', 'Pengatur', null, null, null, '2012-06-19 07:00:00', '18827', '0');
INSERT INTO `golongan_copy` VALUES ('8', 'IID', '2', '9', 'Pengatur Tingkat 1', null, null, null, '2012-06-19 07:00:00', '38373', '0');
INSERT INTO `golongan_copy` VALUES ('9', 'IIIA', '3', '10', 'Penata Muda', null, null, null, '2012-06-19 07:00:00', '4324', '0');
INSERT INTO `golongan_copy` VALUES ('10', 'IIIB', '3', '11', 'Penata muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '37558', '0');
INSERT INTO `golongan_copy` VALUES ('11', 'IIIC', '3', '12', 'Penata', null, null, null, '2012-06-19 07:00:00', '43759', '0');
INSERT INTO `golongan_copy` VALUES ('12', 'IIID', '3', '13', 'Penata Tingkat 1', null, null, null, '2012-06-19 07:00:00', '40589', '0');
INSERT INTO `golongan_copy` VALUES ('13', 'IVA', '4', '14', 'Pembina', null, null, null, '2012-06-19 07:00:00', '6140', '0');
INSERT INTO `golongan_copy` VALUES ('14', 'IVB', '4', '15', 'Pembina Tingkat 1', null, null, null, '2012-06-19 07:00:00', '39991', '0');
INSERT INTO `golongan_copy` VALUES ('15', 'IVC', '4', '16', 'Pembina Utama Muda', null, null, null, '2012-06-19 07:00:00', '50477', '0');
INSERT INTO `golongan_copy` VALUES ('16', 'IVD', '4', '17', 'Pembina Utama Madya', null, null, null, '2012-06-19 07:00:00', '1348', '0');
INSERT INTO `golongan_copy` VALUES ('17', 'IVE', '4', '18', 'Pembuna Utama', null, null, null, '2012-06-19 07:00:00', '51900', '0');

-- ----------------------------
-- Table structure for `jabatan`
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(10) unsigned NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `singkatan` varchar(255) NOT NULL,
  `satker` varchar(255) DEFAULT NULL,
  `eselon` varchar(15) DEFAULT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_jabatan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('8', '0', 'NA', 'STRUKTUR ORGANISASI SEKOLAH', null, '(Tidak Ada)', '1', null, null, null, '2012-06-19 07:00:00', '58865', '0');
INSERT INTO `jabatan` VALUES ('27', '8', 'PENGURUS SEKOLAH', 'SEKOLAH', '(Tidak Ada)', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '7566', '0');
INSERT INTO `jabatan` VALUES ('28', '27', 'Kepala Sekolah', 'KEPSEK', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '57825', '0');
INSERT INTO `jabatan` VALUES ('29', '28', 'Kepala TU', 'KEPTU', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '4305', '0');
INSERT INTO `jabatan` VALUES ('30', '27', 'Ketua Komite Sekolah', 'K.KOMITE', '(Tidak Ada)', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '44641', '0');
INSERT INTO `jabatan` VALUES ('31', '28', 'Wakil Kepala Sekolah Bidang Kurikulum', 'WK.KURIKULUM', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '13697', '0');
INSERT INTO `jabatan` VALUES ('32', '28', 'Wakil Kepala Sekolah Bidang Kesiswaan', 'WK.KESISWAAN', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '95', '0');
INSERT INTO `jabatan` VALUES ('33', '28', 'Wakil Kepala Sekolah Bidang Humas', 'WK.HUMAS', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '24911', '0');
INSERT INTO `jabatan` VALUES ('34', '32', 'Pembina OSIS', 'PB.OSIS', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '58740', '0');
INSERT INTO `jabatan` VALUES ('35', '32', 'Pembina Ekstrakulikuler', 'PB.EKSTRA', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '22378', '0');
INSERT INTO `jabatan` VALUES ('36', '28', 'Wakil Kepala Sekolah Bidang Sarana Prasaran', 'WK.SARANA', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '1198', '0');
INSERT INTO `jabatan` VALUES ('37', '33', 'Kordinator Rumah Tangga', 'KO.RT', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '4384', '0');
INSERT INTO `jabatan` VALUES ('38', '28', 'Koordinator BP', 'KO.BP', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '18328', '0');
INSERT INTO `jabatan` VALUES ('39', '28', 'Koordinator IT', 'KO.IT', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '12955', '0');
INSERT INTO `jabatan` VALUES ('40', '28', 'Koordinator Guru', 'KO.GURU', 'GURU', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '9791', '0');
INSERT INTO `jabatan` VALUES ('41', '40', 'Guru', 'GURU', 'GURU', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '10089', '0');
INSERT INTO `jabatan` VALUES ('42', '8', 'Komite Sekolah', 'KOMSEK', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:09:52', '45480', '0');
INSERT INTO `jabatan` VALUES ('43', '31', 'Staff Kurikulum', 'S. KUR', '(Tidak Ada)', '(Tidak Ada)', '0', null, null, null, '2013-02-13 21:51:19', '58199', '0');

-- ----------------------------
-- Table structure for `jadwal`
-- ----------------------------
DROP TABLE IF EXISTS `jadwal`;
CREATE TABLE `jadwal` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `jenis` varchar(45) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `exec` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jadwal_pegawai` (`nip`),
  KEY `FK_jadwal_jenisagenda` (`jenis`),
  KEY `IX_jadwal_ts` (`ts`,`issync`),
  CONSTRAINT `FK_jadwal_jenisagenda` FOREIGN KEY (`jenis`) REFERENCES `jenisagenda` (`agenda`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jadwal_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jadwal
-- ----------------------------
INSERT INTO `jadwal` VALUES ('1', '1', 'jabatan', '2016-03-04', 'Jadi Presiden', '1', '0', null, null, null, '2016-03-11 18:35:00', '0', '0');

-- ----------------------------
-- Table structure for `jarak`
-- ----------------------------
DROP TABLE IF EXISTS `jarak`;
CREATE TABLE `jarak` (
  `id` int(1) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jarak
-- ----------------------------
INSERT INTO `jarak` VALUES ('1', '< 5 Km');
INSERT INTO `jarak` VALUES ('2', '5 - 10 Km');
INSERT INTO `jarak` VALUES ('3', '11 - 20 Km');
INSERT INTO `jarak` VALUES ('4', '21 - 30 Km');
INSERT INTO `jarak` VALUES ('5', '> 30 Km');

-- ----------------------------
-- Table structure for `jenisagenda`
-- ----------------------------
DROP TABLE IF EXISTS `jenisagenda`;
CREATE TABLE `jenisagenda` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agenda` varchar(45) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `urutan` tinyint(1) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`agenda`),
  UNIQUE KEY `UX_agenda` (`replid`),
  KEY `IX_jenisagenda_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jenisagenda
-- ----------------------------
INSERT INTO `jenisagenda` VALUES ('5', 'cpns', 'Pengangkatan CPNS', '6', null, null, null, '2012-06-19 07:00:00', '17386', '0');
INSERT INTO `jenisagenda` VALUES ('8', 'gaji', 'Penyesuaian Gaji', '4', null, null, null, '2012-06-19 07:00:00', '6748', '0');
INSERT INTO `jenisagenda` VALUES ('2', 'golongan', 'Kenaikan Golongan', '2', null, null, null, '2012-06-19 07:00:00', '47111', '0');
INSERT INTO `jenisagenda` VALUES ('3', 'jabatan', 'Kenaikan Jabatan', '1', null, null, null, '2012-06-19 07:00:00', '18723', '0');
INSERT INTO `jenisagenda` VALUES ('7', 'lainnya', 'Lainnya', '9', null, null, null, '2012-06-19 07:00:00', '17810', '0');
INSERT INTO `jenisagenda` VALUES ('1', 'pangkat', 'Kenaikan Pangkat', '3', null, null, null, '2012-06-19 07:00:00', '32879', '0');
INSERT INTO `jenisagenda` VALUES ('4', 'pensiun', 'Pensiun', '8', null, null, null, '2012-06-19 07:00:00', '45437', '0');
INSERT INTO `jenisagenda` VALUES ('6', 'pns', 'Pengangkatan PNS', '7', null, null, null, '2012-06-19 07:00:00', '63018', '0');

-- ----------------------------
-- Table structure for `jenisjabatan`
-- ----------------------------
DROP TABLE IF EXISTS `jenisjabatan`;
CREATE TABLE `jenisjabatan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `urutan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `jabatan` varchar(2) NOT NULL DEFAULT 'F',
  `isdefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`jenis`),
  UNIQUE KEY `Index_replid` (`replid`),
  KEY `IX_jenisjabatan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jenisjabatan
-- ----------------------------
INSERT INTO `jenisjabatan` VALUES ('8', 'KEPALA', '', '2', 'F', '0', null, null, null, '2012-06-19 07:00:00', '47718', '0');
INSERT INTO `jenisjabatan` VALUES ('16', 'KOORDINATOR', '', '4', 'F', '0', null, null, null, '2012-06-19 07:00:00', '49537', '0');
INSERT INTO `jenisjabatan` VALUES ('18', 'Pegawai', '', '6', 'F', '0', null, null, null, '2016-03-26 17:52:33', '0', '0');
INSERT INTO `jenisjabatan` VALUES ('17', 'STAF', '', '5', 'F', '0', null, null, null, '2012-06-19 07:00:00', '38997', '0');
INSERT INTO `jenisjabatan` VALUES ('11', 'WAKIL KEPALA', '', '3', 'F', '0', null, null, null, '2012-06-19 07:00:00', '46376', '0');

-- ----------------------------
-- Table structure for `jenjang`
-- ----------------------------
DROP TABLE IF EXISTS `jenjang`;
CREATE TABLE `jenjang` (
  `id` char(1) NOT NULL,
  `jenjang` char(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenjang
-- ----------------------------
INSERT INTO `jenjang` VALUES ('0', 'Tidak be');
INSERT INTO `jenjang` VALUES ('1', '<= SLTP');
INSERT INTO `jenjang` VALUES ('2', 'SLTA');
INSERT INTO `jenjang` VALUES ('3', 'D1');
INSERT INTO `jenjang` VALUES ('4', 'D2');
INSERT INTO `jenjang` VALUES ('5', 'D3');
INSERT INTO `jenjang` VALUES ('6', 'D4');
INSERT INTO `jenjang` VALUES ('7', 'S1');
INSERT INTO `jenjang` VALUES ('8', 'S2');
INSERT INTO `jenjang` VALUES ('9', 'S3');

-- ----------------------------
-- Table structure for `mapel`
-- ----------------------------
DROP TABLE IF EXISTS `mapel`;
CREATE TABLE `mapel` (
  `kode` char(2) NOT NULL,
  `mapel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mapel
-- ----------------------------
INSERT INTO `mapel` VALUES ('0', null);
INSERT INTO `mapel` VALUES ('01', 'Pendidikan Agama Islam (PAI)');
INSERT INTO `mapel` VALUES ('02', 'Al Qur\'an Hadist');
INSERT INTO `mapel` VALUES ('03', 'Aqidah Akhlak');
INSERT INTO `mapel` VALUES ('04', 'Fiqih');
INSERT INTO `mapel` VALUES ('05', 'Sejarah Kebudayaan Islam (SKI)');
INSERT INTO `mapel` VALUES ('06', 'Pendidikan Kewarganegaraan (PKn)');
INSERT INTO `mapel` VALUES ('07', 'Bahasa Indonesia');
INSERT INTO `mapel` VALUES ('08', 'Bahasa Arab');
INSERT INTO `mapel` VALUES ('09', 'Bahasa Inggris');
INSERT INTO `mapel` VALUES ('10', 'Bahasa Asing Lainnya');
INSERT INTO `mapel` VALUES ('11', 'Matematika');
INSERT INTO `mapel` VALUES ('12', 'IPA');
INSERT INTO `mapel` VALUES ('13', 'Fisika');
INSERT INTO `mapel` VALUES ('14', 'Biologi');
INSERT INTO `mapel` VALUES ('15', 'Kimia');
INSERT INTO `mapel` VALUES ('16', 'IPS');
INSERT INTO `mapel` VALUES ('17', 'Sejarah / Sejarah Nasional dan Umum');
INSERT INTO `mapel` VALUES ('18', 'Geografi');
INSERT INTO `mapel` VALUES ('19', 'Ekonomi / Akuntansi');
INSERT INTO `mapel` VALUES ('20', 'Sosiologi Antropologi');
INSERT INTO `mapel` VALUES ('21', 'Sosiologi');
INSERT INTO `mapel` VALUES ('22', 'Antropologi');
INSERT INTO `mapel` VALUES ('23', 'Tata Negara');
INSERT INTO `mapel` VALUES ('24', 'Seni Budaya dan Keterampilan / Kerajinan Tangan dan Kesenian');
INSERT INTO `mapel` VALUES ('25', 'Seni Budaya');
INSERT INTO `mapel` VALUES ('26', 'Keterampilan');
INSERT INTO `mapel` VALUES ('27', 'Pendidikan Jasmani, Olahraga dan Kesehatan');
INSERT INTO `mapel` VALUES ('28', 'Teknologi Informasi dan Komunikasi (TIK)');
INSERT INTO `mapel` VALUES ('29', 'Muatan Lokal Umum');
INSERT INTO `mapel` VALUES ('30', 'Muatan Lokal Agama');
INSERT INTO `mapel` VALUES ('31', 'Bimbingan Konseling / Bimbingan Penyuluhan');
INSERT INTO `mapel` VALUES ('32', 'Guru Kelas');
INSERT INTO `mapel` VALUES ('33', 'Lainnya');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Setting', '1', '0', '1', '#', null, '1', '1');
INSERT INTO `menu` VALUES ('2', 'Struktur Jabatan', '1', '1', '2', 'referensi/jabatan.php', null, '1', '0');
INSERT INTO `menu` VALUES ('3', 'Jenis Jabatan', '1', '1', '3', 'referensi/jenjab.php', null, '1', '0');
INSERT INTO `menu` VALUES ('4', 'Diklat', '1', '1', '4', 'referensi/diklat.php', null, '1', '0');
INSERT INTO `menu` VALUES ('5', 'Satuan Kerja', '1', '1', '5', 'referensi/satker.php', null, '1', '0');
INSERT INTO `menu` VALUES ('6', 'Data Pegawai', '2', '0', '1', '#', null, '1', '1');
INSERT INTO `menu` VALUES ('7', 'Cari Pegawai', '2', '1', '2', 'pegawai/daftar.php', null, '1', '0');
INSERT INTO `menu` VALUES ('8', 'Statistik Pegawai', '2', '1', '3', 'pegawai/statistik.php', null, '1', '0');
INSERT INTO `menu` VALUES ('9', 'Lain-lain', '3', '0', null, null, null, '1', '1');
INSERT INTO `menu` VALUES ('10', 'Jadwal Kepegawaian', '3', '1', '1', 'pegawai/jadwal.php', null, '1', '0');
INSERT INTO `menu` VALUES ('11', 'Struktur Organisasi', '3', '1', '2', 'pegawai/struktur.php', null, '1', '0');
INSERT INTO `menu` VALUES ('12', 'Daftar Urut Kepangkatan', '3', '1', '3', 'pegawai/dukpangkat.php', null, '1', '0');
INSERT INTO `menu` VALUES ('13', 'Pengaturan', '4', '0', '1', '#', null, '1', '1');
INSERT INTO `menu` VALUES ('14', 'Tambah User', '4', '1', '2', 'pengaturan/user.php', null, '1', '0');
INSERT INTO `menu` VALUES ('15', 'Ganti Password', '4', '1', '3', 'pengaturan/pengaturan.php#', null, '1', '0');
INSERT INTO `menu` VALUES ('16', 'Keluar', '5', '0', '1', 'logout.php', null, '1', '0');
INSERT INTO `menu` VALUES ('17', 'Tambah Pegawai', '2', '1', '1', 'pegawai/pegawaiinput.php', null, '1', '0');

-- ----------------------------
-- Table structure for `menuguru`
-- ----------------------------
DROP TABLE IF EXISTS `menuguru`;
CREATE TABLE `menuguru` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menuguru
-- ----------------------------
INSERT INTO `menuguru` VALUES ('27', 'Notifikasi', '6', '1', '3', 'home.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('16', 'Berita Sekolah', '5', '1', '2', 'buletin/rubriksekolah/beritasekolah.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('28', 'Berita Guru', '5', '1', '3', 'buletin/beritaguru/beritaguru.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('14', 'Berita Siswa', '5', '1', '4', 'buletin/beritasiswa/beritasiswa.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('17', 'Pesan', '6', '1', '2', 'buletin/pesan/pesan.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('19', 'Agenda', '6', '1', '4', 'buletin/agendaguru/agenda.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('20', 'Galeri Photo', '6', '1', '5', 'buletin/galerifoto/galerifoto.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('21', 'File Sharing', '6', '1', '6', 'buletin/filesharing/main.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('1', 'Akademik', '1', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('2', 'Kalender Pendidikan', '1', '1', '4', 'jadwal/kalender_main.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('3', 'Jadwal', '1', '1', '2', 'jadwal.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('4', 'Pelajaran', '1', '1', '3', 'pelajaran.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('5', 'Penilaian', '1', '1', '1', 'penilaian.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('10', 'Presensi', '3', '1', '2', 'presensi.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('12', 'Siswa', '3', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('11', 'Kejadian Siswa', '3', '1', '3', 'catatankejadian.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('13', 'Perpustakaan', '4', '0', '1', 'perpustakaan/perpustakaan.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('6', 'Kepegawaian', '2', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('7', 'Data Pegawai', '2', '1', '2', 'pegawai/data.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('8', 'Struktur Jabatan', '2', '1', '3', 'pegawai/struktur.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('9', 'Daftar Urut Kepangkatan', '2', '1', '4', 'pegawai/dukpangkat.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('24', 'Ganti Password', '6', '1', '7', 'frametop.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('25', 'Keluar', '7', '0', '1', 'logout.php', null, '1', '1');
INSERT INTO `menuguru` VALUES ('26', 'Cari Siswa', '3', '1', '2', 'siswa/siswa.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('15', 'Berita', '5', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('18', 'Lainya', '6', '0', '1', null, null, '1', '1');

-- ----------------------------
-- Table structure for `menusiswa`
-- ----------------------------
DROP TABLE IF EXISTS `menusiswa`;
CREATE TABLE `menusiswa` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menusiswa
-- ----------------------------
INSERT INTO `menusiswa` VALUES ('1', 'Akademik', '1', '0', '1', null, null, '1', '1');
INSERT INTO `menusiswa` VALUES ('2', 'Info Akademik', '1', '1', '2', 'infosiswa/infosiswa_content.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('3', 'Penilaian', '1', '1', '3', 'akademik/penilaian/lap_pelajaran_main.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('4', 'Jadwal Pelajaran', '1', '1', '4', 'akademik/jadwal/jadwal_kelas_footer.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('5', 'Kalender Pendidikan', '1', '1', '5', 'akademik/jadwal/kalender_footer.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('6', 'Perpustakaan', '2', '0', '1', 'perpustakaan/perpustakaan.php', null, '1', '1');
INSERT INTO `menusiswa` VALUES ('7', 'Berita', '3', '0', '1', null, null, '1', '1');
INSERT INTO `menusiswa` VALUES ('8', 'Berita Sekolah', '3', '1', '2', 'buletin/rubriksekolah/beritasekolah.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('9', 'Beritas Siswa', '3', '1', '3', 'buletin/beritasiswa/beritasiswa.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('10', 'Lainya', '4', '0', '1', null, null, '1', '1');
INSERT INTO `menusiswa` VALUES ('11', 'Pesan', '4', '1', '2', 'buletin/pesan/pesan.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('12', 'Notifikasi', '4', '1', '3', 'framecenter.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('13', 'Agenda', '4', '1', '4', 'buletin/agendasiswa/agenda.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('14', 'Galery Fhoto', '4', '1', '5', 'buletin/galerifoto/galerifoto.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('15', 'File Sharing', '4', '1', '6', 'buletin/filesharing/main.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('16', 'Keluar', '5', '0', '1', 'infosiswa/logout.php', null, '1', '1');

-- ----------------------------
-- Table structure for `pegawai`
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `nrp` varchar(30) DEFAULT NULL,
  `nuptk` varchar(30) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(25) DEFAULT NULL,
  `tmplahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `kelamin` varchar(1) NOT NULL,
  `ibu` varchar(255) NOT NULL,
  `jenjang` char(2) DEFAULT NULL,
  `statuspeg` char(1) DEFAULT NULL,
  `golongan` char(2) DEFAULT NULL,
  `panggilan` varchar(50) DEFAULT NULL,
  `gelarawal` varchar(45) DEFAULT NULL,
  `gelarakhir` varchar(45) DEFAULT NULL,
  `gelar` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `suku` varchar(50) DEFAULT NULL,
  `noid` varchar(50) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telpon` varchar(20) DEFAULT NULL,
  `handphone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `foto` blob,
  `bagian` varchar(50) NOT NULL,
  `nikah` varchar(10) NOT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `aktif` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `pinpegawai` varchar(25) DEFAULT NULL,
  `mulaikerja` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `ketnonaktif` varchar(45) DEFAULT NULL,
  `pensiun` date DEFAULT NULL,
  `doaudit` tinyint(1) DEFAULT '0',
  `info1` varchar(20) DEFAULT NULL,
  `info2` varchar(20) DEFAULT NULL,
  `info3` varchar(20) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tmtskcpns` date DEFAULT NULL,
  `tmtskawal` date DEFAULT NULL,
  `tmtskakhir` date DEFAULT NULL,
  `gaji` varchar(8) DEFAULT NULL,
  `mapelampu` char(2) DEFAULT NULL,
  `totaljam` char(2) DEFAULT NULL,
  `statusser` char(1) DEFAULT NULL,
  `statuslulus` char(1) DEFAULT NULL,
  `mapelser` char(2) DEFAULT NULL,
  `nrg` varchar(255) NOT NULL,
  `nosknrg` varchar(255) DEFAULT NULL,
  `tglsknrg` varchar(255) DEFAULT NULL,
  `statustpg` char(1) DEFAULT NULL,
  `tahunmulaitpg` char(4) DEFAULT NULL,
  `besartpg` varchar(255) DEFAULT NULL,
  `kodepos` char(7) DEFAULT NULL,
  `jarak` char(1) DEFAULT NULL,
  `trans` char(1) DEFAULT NULL,
  PRIMARY KEY (`nip`),
  UNIQUE KEY `UX_pegawai_replid` (`replid`),
  KEY `FK_pegawai_agama` (`agama`),
  KEY `FK_pegawai_suku` (`suku`),
  KEY `FK_pegawai_bagian` (`bagian`),
  KEY `IX_pegawai_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES ('24', '1', '', '', 'robait usman', null, 'blitar', '1988-03-13', 'l', '', null, null, null, 'bet', '', 's,Kom', null, 'Islam', 'Jawa', '2', 'landungsari', '', '085646742211', 'come.thinkblue@gmail.com', '', '', '', null, 'Non Akademik', 'belum', '', '1', '78462', '2012-01-01', '', '', null, '1', null, null, null, '2013-02-06 15:15:24', '9926', '0', null, null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null);
INSERT INTO `pegawai` VALUES ('75', '131135720001070002', '', '4742745647300022', 'LILIK SRI WAHYUNI, S.Pd.', '3572025004670003', 'JOMBANG', '1965-05-06', 'P', 'MULASTRI', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Raras Wuyung No.6 Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2003-07-01', '2003-07-01', '2013-01-07', '850000', '07', '24', '1', '1', '07', '091736902038', '1/11/2009', '10/11/2009', '1', '2008', '1500000', '66120', '1', '3');
INSERT INTO `pegawai` VALUES ('78', '131135720001070005', '', '', 'NUROBIKAH, S.Pd.', '0000000000000000', 'BLITAR', '1963-09-06', 'P', 'SITI MAEMONAH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Dsn. Banjar Mlati Tunjung Udanawu Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-07-01', '2005-07-01', '2013-01-07', '850000', '07', '24', '0', '', '', '', '', '', '', '', '', '66154', '4', '3');
INSERT INTO `pegawai` VALUES ('84', '131135720001070011', '', '', 'DESIANA EKA MUFIDA, S.Pd.', '0000000000000000', 'BLITAR', '1965-09-11', 'P', 'MARYATI', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Bendo Rt.6 Rw.3 Ponggok Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2008-07-01', '2008-07-01', '2013-01-07', '850000', '07', '24', '0', '', '', '', '', '', '', '', '', '66153', '2', '3');
INSERT INTO `pegawai` VALUES ('77', '131135720001080004', '122392132021', '8835745648200012', 'ABDULLAH QOMAR, S.Ag.', '3505070305670002', 'BLITAR', '1965-11-30', 'l', 'SITI ROHMAH', '7', '2', '07', '', '', '', '', 'Islam', 'Jawa', '2', 'Centong Rt.2 Rw.4 Purworejo Sanankulon Blitar', '', '123456', '', '', '', '', 0x2F0039006A002F00340041004100510053006B005A004A005200670041004200410051004100410041005100410042004100410044002F002F00670041003700510031004A004600510056005200500055006A006F0067005A0032005100740061006E0042006C005A007900420032004D005300340077004900430068003100630032006C0075005A00790042004A0053006B006300670053006C004200460052007900420032004E006A00490070004C004300420078006400570046007300610058005200350049004400300067004E007A0041004B002F003900730041005100770041004B0042007700630049004200770059004B004300410067004900430077006F004B0043007700340059004500410034004E0044005100340064004600520059005200470043004D0066004A0053005100690048007900490068004A006900730033004C007900590070004E0043006B00680049006A00420042004D005400510035004F007A0034002B005000690055007500520045006C004400500045006700330050005400340037002F003900730041005100770045004B004300770073004F00440051003400630045004200410063004F007900670069004B004400730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037004F007A00730037002F003800410041004500510067004100380041004300300041007700450069004100410049005200410051004D005200410066002F0045004100420038004100410041004500460041005100450042004100510045004200410041004100410041004100410041004100410041004200410067004D0045004200510059004800430041006B004B0043002F002F00450041004C00550051004100410049004200410077004D004300420041004D004600420051005100450041004100410042006600510045004300410077004100450045005100550053004900540046004200420068004E0052005900510063006900630052005100790067005A0047006800430043004E004300730063004500560055007400480077004A0044004E00690063006F0049004A004300680059005800470042006B0061004A00530059006E004B0043006B0071004E004400550032004E007A00670035004F006B004E004500520055005A004800530045006C004B00550031005200560056006C00640059005700560070006A005A00470056006D005A0032006800700061006E004E003000640058005A003300650048006C003600670034005300460068006F006500490069005900710053006B003500530056006C007000650059006D005A00710069006F00360053006C007000710065006F0071006100710079007300370053003100740072006500340075006200720043007700380054004600780073006600490079006300720053003000390054005600310074006600590032006400720068003400750050006B003500650062006E0036004F006E007100380066004C007A00390050005800320039002F006A0035002B0076002F0045004100420038004200410041004D00420041005100450042004100510045004200410051004500410041004100410041004100410041004200410067004D0045004200510059004800430041006B004B0043002F002F00450041004C0055005200410041004900420041006700510045004100770051004800420051005100450041004100450043006400770041004200410067004D00520042004100550068004D005100590053005100560045004800590058004500540049006A004B00420043004200520043006B00610047007800770051006B006A004D0031004C007700460057004A007900300051006F0057004A004400540068004A00660045005800470042006B0061004A00690063006F004B0053006F0031004E006A00630034004F0054007000440052004500560047005200300068004A0053006C004E005500560056005A005800570046006C0061005900320052006C005A006D0064006F006100570070007A006400480056003200640033006800350065006F004B00440068004900570047006800340069004A00690070004B0054006C004A00570057006C00350069005A006D0071004B006A0070004B0057006D007000360069007000710072004B007A0074004C00570032007400370069003500750073004C00440078004D0058004700780038006A004A00790074004C00540031004E0058005700310039006A005A00320075004C006A0035004F0058006D0035002B006A007000360076004C007A00390050005800320039002F006A0035002B0076002F00610041004100770044004100510041004300450051004D005200410044003800410039006B00790061004D006D006B006F006F00410058004E004700540053005500550041004C006B0030005A004E004A00520051004100750061004D006D006B00700047005A0055005500730078004100550044004A004A006F00410064006B0031005400750074005700730037004B0065004F00470034006E00560048006B0036005A004E006300500034007A002B0049003900760070006F002B007A006100580049004A007000510033007A006E002B0048004600650054006100720034006B00750039005400750057006E006C006C0049005900390067007800770050007A006F0041003900750031006A00340069003600620070004500370051004D0050004F006300640047006A004F0052002B004E0054006100660038005100640049003100430038006A0074006F0035004E00720079007900420045007A0078006B004500410067002F007200690076006E0056003700320052006900510057004F004B00660044006600500047003600730072006B00460054006B00450048007000510042003900570072004D006A00530047004E0058004200590044004A0041003500780054006C0063004D004100560049004900500063005600340048006F003300780046007600620047004E00680075004C007600300058004A002B00550066005500640036003900410038004B00660045006100780076005500530030007600790049004A00530063004200770050006B002F00480030006F004100370033004E00470061005200570056006C0044004B0051005100650051005200330070006100410044004E00470061004B004B00410044004E00470061004B004B00410044004E00470061004B004B00410044004E004600460046004100430055005500550055004100460046004A005300300041004600460046004A00510041004D0077005600530053006300410064006100380072002B004B00480069007A0059005200700074006E0064003400580042004D0068006A0050004F00660053007500340038005900360077006D0069002B0047007200750036004D0067005200390068005700500031004C00480067005600380032005800310031004C006400330044007500370046006D00640069005300540033004E004100450055003900300030006A004500730078004E005600440049006100300049004E004B006E006D0078006C0053004B0073007600340066006B004300350048004E00410047004B0057006F0056006A0057006900640047006E004400590032006D0074004B00310038004E0075007900670073004B0041004D0043004F005100710065006100760057006C003800380062006700710078004200460062004400650047004D003800690071006C007800340064006E00690055006D0050006E004600410048007000330077002F007700440048002F004D0047006C006100670035004D005A002B0053004F0054005000540030007A002F004B007600560041006300690076006C004F0030006B006D007400720067004100350056006C004E00660052002F006700750034006500360038004C00570055007A007A00650061007A004A007900320037005000340055004100620031004600460046004100420052005200520051004100550055005500550041004600460046004600410043005500550055006C004100420052005200520051004100550031006D00780053004D00320042005500440079005500410065006200660047006D0035004B0036006400700038005100630067007600490035003200350034004F0041004F006600310072007A006200520039004D0044004B004C006D0059005A007A0039003000560031005800780062007600320075002F0045006C0076005A00620073007000420045004F00500064006A007A002F00490056006C00520045004A00430069004C00300041006F0041006D006A0052004600340041007100390044004300720044007000560053004D0062006D00460061006C007400480077004B004100470070005A004A006E004F00300056006100570031005800670041005600490071005A0071005A004600780051004200410062005A0051004F006C005200470032005500350079004B00750075007000370030007900670044006C005000450047006A00440079007A00640077007200670072003900370033004600640033003800480039005A00610057003100750064004B006C004F005400470066004D006A005000730065004300500038002B0074005A005600780045004A004C006400300059005A004200420072004D002B0047007400310039006A0038006200770078005A007700730075002B0049002F0069004F005000310041006F004100390030006F006F006F006F0041004B004B004B004B0041004300690069006900670041006F006F006F006F00410053006B007000610051003000410049005400540057006200690068006A0069006F006E00610067004200720076005600650052002B0074004F0064007100710079007900630047006700440078005400780034003700580066006A006D0056004F0050006C00320071004D00660054005000390061006100460032004B004D006E00410048007200550033006900560042004A00380051004A0079004F007700420050002F0066004E005A005700720078007A007900790071006B0054006B004C0033004600410047007200420049006E0056005700420072005400740062006F00440041004A0072006A00590039004F0031004B004A0064003800540046007500350047006100730057007500700033004500620037004A003100490049006F004100390042006A0065004D006F0043004B006D00690065004D006400540058004D00320065006F0046003100470044005200650036006E004A004500760079003900610041004F006E00650053004D006A0067006900710054003300560073006A00370057006C00550045002B003900630069007400350071006C002B002F006C0072004D004900770054003600310063005400510072007200470058006E0044004D00610041004F006F004B00680034002B004F003900630070006F0036007600590065004F004C0054006A006B0058004B003900660072005800530061005000620054005100570035006A006C006B004C00670064004D0039005200570058005000610073006600480065006E0071006D0051007A00790052006B0059002F007700420036006700440033004F006C007000420053003000410046004600460046004100420052005200520051004100550055005500550041004E0070004300610057006D004D0061004100470075006100670063003400710052006A005500450068006F00410068006C00610075004D00380054002B004C00570030006E0055004500740049006B0042004A00580063007A004E007900420037005600310073007A0059004600650061002B004F00370046006A0066007200640064005600630041006600510069006700440041006E007600420071006E006900530065003900320042005300590067004D004400310071007000710044005300780045006D004E004D0073006100580053002F006B0076004A00460050003800410064002F0072005700760035004B0054006A00470042005100420079004C007A00360073007200710059003500480077005400790071003100660057007A0075006900690066006100440076004C004C006B0074006A006C00540036006500390062007600380041005A00320077003500410071004F005A0050004C00470047003700550041004A006F0046006E007600660061003900570039005700300077006E002F0056006A006E00480046004C00700043007300300070004B006A00690074007600410059003700580036003000410065006400330057006E0036006B004D0047004100750048004200350043006E0048004600610075006D0057002B0072007700520078007600350030006A004F0053006400790053006300340048006200420072007000700039004C007A0049004A0049002B002F00550056006300730037004D006E004700340064004B004100460030002F007A004700690042006B005800420078007A0056005300370067006B0054007800460061003300630054004600470069006900590068006800310042003600630065002F004E00620058006C0068004F004200560047002B006700750048006E005300570048004700490078006C0038002B006C004100470033003400590076004C006C0066004500430077004D0078003800710053004D0035004800710065007500610037006D0075004E0038004B0057006200530036006800390071004900770049006B0036002B00350072007300710041004300690069006900670041006F006F006F006F0041004B004B004B004B004100470031004700350071005100310047003100410045004C004700710030007000710065005100310057006B004E00410046004F006400730043007500620031002B00300046002F005A0079005200480037003300560054003600470075006700750044007700610078004C00780038005A006F0041003800300051006500580065006E006700670038007100770050005500470074004B0031006600350068007A00530036007A006200370062002F007A0056006A004A00440048004A0049004700610072005200730056006200720069006700440066006A00410064004B0078003900530043005000500073003300680051004F0053006100740077005800590056004F006600530073006E005500720063003300540046006C006600470065006F00780031006F0041003100390044003100430030006900660044004500450064004B003100330065004B00340059006D0047005100620068007A006A004E00630054005A0036005500300066007A00650059005600470065006700720056006800300064006C00750055006E0069006D005A0047004800550039007A00510042003100460074004C006C006400720064006100750070006700560051006800770045005500350035004800550031005900530053006700430064006A00560052002F004E00610034004900330066007500380059004900480056006A005600700066006D007200520030002F0052006200750035006400580038006A0062004500780034006B004C00440047005000700031006F0041003600540052004C004D0032006D006D0078006800680069005300510062006D00390076006100740047006B0055006200560041003900420069006C006F0041004B004B004B004B0041004300690069006900670041006F006F006F006F004100610061006A0063005600490061006900630030004100560035004B00720053006E006900720055006800710070004E0033006F0041007A0037006B003800470073004F0039003700310073005800520049007A0057004600650073006500610041004D004F002B004700630031007A003800780032005300470074003600360062004A004E0059004E002B004E0070003300550041004D006B006E005A00550034004E005A007A00610078006800790071006A006300520056006C004A0051003600340050004E005100540051004B00700033004B006C004100440072006600560062006700740077007300680048007300740061007400740072004E007900760079007400620079004F00760066004B0031006C0057002B007000790032003500770073005A004A003900680057007400610033003800740032003400450073005200550065003400780051004200660074007400610055007A0043004900710036004500390041003400780057002F006200760035006900670031006D00430043004F00570045004100710042006A006B0063005600640074003300450061006800610041004E004B00460053007A007100460047005300540067004300760051004C0057004C0079004C0061004B004C002B0034006F00460063006A00340062007400500074005600330039006F0059006600750034006A0078003700740058005A00430067004200610057006B007000610041004300690069006900670041006F006F006F006F0041004B004B004B004B0041004900790031005100750061004300310052004F0031004100440058006100710073006800340071005700520071007200530074007800510042005200750052006B00470073004F00390058007200570031006300500077006100780062003100680067003000410063002F006500440042004E0063003900710038007900780051006800530066006D0063003400550065007600650074004C005600390063003000360030006B0061004F0057003500540065004F0071006A006B002F0070005800490076007200430033002F006900430030004A0058004600750073006D007A00420036006B004E00770054002B007400410044006F003700760061003300570074004B00470065004B00520050006D004E005A006D00710036005A004E007000740034003000550067004F004F0071004E002F00650048007200560052005A004800580076005100420076007800540078004C004C006E006A004900720061007300620075004300660068007300410069007500480044007900450035004200360031006100740070007200680048007900720047006700440030004A005A0034006B0054004A005900560058006200550041007A006200590036003500790033006B0075005A00380042006D004F004B0032006200570044007900560044007300430057005000430071004F0070004E00410048006100650045004E0062006B007400640055002F0073004B002B005600450061006100490058004E006D0034002F00350061004B006600760041002B00340049005000340041002B006C0064003800680079004B00380056002B004B004500560078006F0064003100340063006C00520032006A0075004900720055006A0065006800770051007900730044007700660059006D007500700038004E006600460066005200620071003000670068003100650063003200640035007400780049007A00490066004C0059002B006F00490036005A0039003800550041006500680030005600550073006400550073004E0053006A00380079007800760059004C006C00660057004B0051004E002F004B007200640041004300300055005500550041004600460046004600410042005200520052005100420052005A007300560045003700630055007800350051004F00390063003300720033006A00620052007400440033004A0063003300510065005900660038007300590076006D0062003800660054003800610041004E0032005700510044007600560047003600760049006F00490079003800730069006F0067003600730078007700420058006C004F00740066004600620055004C0070006D005400540059005500740059002B007A00740038007A006E002B00670072006900390051003100760055004E00540062006400650058006300730035002F00320033004A0041002F0043006700440031004C0078004200380052004E004C0073004300300056006F0033003200320058002F0070006D0066006B0048002F004100760038004B0038002B00310058007800760071002B006F004D0032004A007800620078006E002B0043004C006A00390065007400630038007A002B00390052006B0035004E00410041007A00730037004600690053005300650063006D0070004C00640039006B00790050003300560067006100690070007900480044004300670044003200730061004C0062002B004A00740044006A004400590045006D003300640047002F00630047007600500037002F0053004C006A005400370074003700610035006A004B005300490066007700500075005000610076005200660068006E0063004A0066006100500046004500530066004D005100370061003700480078004E0034004E00730074006200300034006C003200570043003400690058004B00540048006F005000720037005500410065004100720062004E006E00700057006A005A0032005A0059006A00690074006100380038004E00360068007000370062007000720064006A004500660075007A0049004D006F003300300062007000540059003000450049006F0041006E00740034004500680058004A0078007800580063006500430076004400440058006B00300065007300580036004500510078006E006400620078006E002B004D002F0033006A0037004400740056004C0077006A00340050006E003100570056004C002F00410046004A00440048005A004B00630072004700650044004B006600660032007200300035005600560046004300710041007100670059004100480051005500410065004F006600480061006400540063003600560045004F0071007200490054002B004F004B0038006B005A00790056003600310036005A003800630058002F00770043004A002F005A0078002B006B004A005000360031003500650054007800510042005000620058003800390074004D007300730045007A00780053004B006500470052006900430050007800460064006E007000480078005900380054006100640074005600370030005800630059002F0068007500560033005A002F00480067002F007200580041004500340066004E0050004400550041006500350036005A003800620062004F0055004B0075006F0036005A004A0047006500370077004F00470048003500480048003800360037005800530050004700570067006100340041004C004C00550059006A00490066002B00570055006800320050002B00520036002F0068005800790032007300680048006500720045005600300038005A00420044005900780051004200390063005500560038002B0065004800660069006400720075006A00370049006D0075005000740056007500760048006C0054002F00410044006300650078003600690076005800500043002F006A007A005300660045007900720045006A002F005A00370077006A006D0043005100390066003900300039002F0035003000410064005000520052005200510042003800380065004B005000690058007100570071005300530032002B006E0079004E00610057005A004A0041004B00380053004F00500055006E007400390042005800440053005400730037004500730078004A0050006300310045007A003100470057007A00510042004900580071004D00730054005300550055004100470061004B004B004B0041004300670063004500550055005500410065006C00660043007200560078005A00330030006B0062007400680056004900610074006600340069002B004F0035007400520031006A002B007700340048004E007200700030004C0062005A00350054006E00390036002F003400630037005200360064002F00790072006700760042006A0075004E0056005A0046007A0068006F007A006E0048003400560036006C00720058006800530033003800530061006200610058004B004C003500560078004B0043007000630044006F00340037002F0041004A00300041005A002B006D0065004F00310073004E004600510051005100490034004C004300450070006300330041005500670034002B003900740050004A005500440032007A0057004E004A007100570070005200360070006500520051004C00420061007400350066006E004B00590049004700640034003900790037006C0077005700580067004800490078006A0047004D0031007A0045006200580065006700360068004E0061007400350073004D006B0063006F0042004500520077003200410066006D004200500048004200480034005600310039002F004D004A006400560069006D006A00420049006E00740046006D005100530033007A0045006B0075004D0042004D0064004D00720031004700420032006F00410075002B00410076004600560035006F0074003100620072006500330066006E0032004E00370049005200500035006B00750035006F007000430066007600410048004200340079004E0032004D006A006E005000460065003000390052006D00760044004E00410038004D00580058006900570031005700560035006E002B007A00770045006F0043005300430058006C0059006A004A004400410038006A004100460065007400360046004E0064005700790044005300640052006200640063005100490050004C006B002F0035003600700030007A003900520033006F004100380059002B004E006C0079004A0066004700610078004100350038006D0032005100480036006B006B002F003400560035007A006E006900750073002B004A00740030006200720078003700710070004A00790045006C0043004400380041004200580049005A0077006300550041004E00590035004E004B0044007800540054003100700063006300550041004F00440063003100490047007100410064006100660075006F00410073004B002B004B0030004C004B0039006C00740035006B006B0069006B005A0048005500350056006C004F004300440057005500720056004E00470032004B004100500065006600440048007800510073006E00300057004E0064005A006B00620037005800470064006800630044003700340034007700330031002F00770041004B004B0038005700740037006A00620048006A005000650069006700440044004A007A005200520052005100410055005500550055004100460046004600460041004200520052005200510042003000580067005400550045003000330078005A005900540053003400380070007000420048004900440030004B00740078007A002B006400660053004E0078007000300046006E0062004B00620063004600550038007800570043006400680039004B002B005500490058004B0053004100670034004900720036006B003800500036006E002F006200660067006A005400720035006A006C0035005900550033002F0041004F0038004F0044002B006F006F0041003400540034006F002B0047006200620062004E0071006F004B0078006B00440035007A00360035003600560078004F006A00530052005200610078005900420064005400690065004A00540075006B006500470033004700460049005500740030004B0067006B0034004700500078007200750050006A004800710055006900430047007700680043006B0059003300790038006A006A0073004D006A00300036002F007000580043002B004700580076006C0031004F004400790059004C00540065003800350032006D0052004D007900590048007A0045004B00650067002B00570067004400330076007700740070004E0074007000760068007A005400340049004300720049007100430051004D0042006A00630053004D00350035002B007400570039005A0069002F0030004D0033006300620042004A003700580039003500470035002F00550048003200490034007100700034004F007600570076006600440046006D00300070002F006600520072003500630071006B0067006C0057004800470044002B0047004B0069003800640033006A0057004800670033005500700030004F0047005700460067004400370034006F0041002B005A00740061007600570031004400570062007900380063002F004E0050004F0037006E003800530054005700630078007900610065003500350071004F006700410070005300610053006900670041006F006F006F006F0041006300680071005A005400550043006E00420071005400640078005100420061006A00630068004B004B006900520073004C0052005100420056006F006F006F006F0041004B004B004B004B0041004300690069006900670041006F006F006F006F00410041006300470076006500760067007A007100660032007A00770068004A00700037004E006C00720057003600340039006C0062006E002B00650061003800460072007600760068005A007200330039006B00580047007300520076004A007400560037004200350046007A002F0066005500630066007A004E004100450058006A003700550049006400570038005400580056003000300055007200480035006A00750054006B0071007500370061006E0058006A00470030005A002F0034004600560044005100460073006D006D00680053005300470038006B0058007A0079004D005200710050004B007700780043007400750062004F005200380074005400610034004A007800630058006B004400360073005A002F0049006B0045004B0072006300760038002F00420055006E0061004F002F0049007800550076006800300058007200330039006E0035005600370062004C004A0039006F006B005600580064006B0079004700590043004D00660075007A00310035004700610041004F0071002B0048002F0069006900300030004800570076007300510065005A004C004B0034006E006100490071006500590055002B00630068005700550035004A004A0036005A0039006A00580062002F004600710035004500480067004300390035002F0031006800560042002B004A00720079004A003500640052006C00640070007A0064003200620046006D006C0048006D00650058004700430063004100640049002B0078002B0051003800340072006400380062002B004C004700310066003400640057006B0045007A006F0062006B00540071006B006F00520077003200630041006B0048006A00310047004B00410050004B00330050004E004E006F004A007A00520051004100550055005500550041004600460046004600410041004B0058005000510055006C0041003600300041005400710065004B004B0061004400780052005100420046005200520052005100410055005500550055004100460046004600460041004200520052005200510041005600660030004B0064007200660057007200560031006A00450075005A00410070006A005A0074006F0063004500340032006B003900670061006F00550041006B004500450048004200480065006700440072006400530065004F006100370062002F006900550069004F005300610037007A0035006300550070006B003300630048004F00320051003500360074006A00380071004E004500610030004700710032006A004C0059005400500049007200790075004900300062004D003200510046004B005900620047004D0037007300390071006A00730057006C006A00730039004D00590061006F0079005200740075006B004D0063007A006C0059005200730059006400660071005300610076006500480070004C007900320076004C0042004600310061004800790059006D00640039006A004F007100780045004900510054003800350036005A007A00510042004200500039006700690045004C004A0059003300530053004E004E004B00410070006300470066006200740047004F006300590036006C007500310063002F0071006A007800410051007800320036006C00450061004E0057006400530065006400320053004F006600660048003800360036005A006A0065003200310072005A007700740071006C007300300053007900750076006C00680031004D00570056002B0059006E007A0042002F0076005600790065006F00500049005A006C0069006B006E00570062007900560032004B0055004F005600410048006F00650034006F004100710030005500550055004100460046004600540032006C006C004E0065007900460049005100760079006A004C004D007A004200560041003900790065004B00410049004B004B006C00750062005700610030006E006100470065004E006F003500460036007100770071004B00670041006F006F006F006F0041006500700034006F00700067004E004600410042005200520052005100410055005500550055004100460046004600460041004200520052005200510041005500550055005500410064004A006F004D003800450074007500690053006100610073006800740030006B0033007A00370079002B00560077005300410059002B006E0033007300630031007000610045006C006D007300310068003900740030007900590051007300680038007900560035004D0078004600530050006D002B005400620078007900420033007200480038004A007500340075004C00300052003300730039006F00360032006A00790069005300460073004D0064006700330059002F004800460064004A005A00700063004E00630057004C0057002B00750032003600540047003300640050004D00440037007000670058004A006B004F003500440037006300550041005A006B004D006C0070004C006100610065007A0036005A004C007300380077007300370068007800350050004A007700330079005900360034004100370031007900630078007A004B0078003200680065006500670047004B00360036005300350075005A004C00610079007500570031006500490079004E004B003700370031006300430059006200770046002B0036005000700058004900530075007A007900750037007300570059006B006B007300650070006F0041006200520052005200510041002B0043004300530035006E00570047004D005A005A006A0067005600720046006F00720052004400620032007200620068006E006C00380063007500520033002B006E0074005300510051002F0059004E004C0045006A0044004500740030004D00350078006E004300640076007A0050003800410049005600640030007600540032006C006A0045007800770056004A003500790063006300650033002B00650034006F00410067005700370069006E006900530031007600590066004F00690041002B0051002F0064006500500031003200740032004800730063006A003200370031006E0036006A007000720057006200430053004E0076004E0074006E00500037007500550044004700660059006A007300520033004800380078007A0058005400580075006C005200540057007A0047004D0042004800560063006E004A0036006E004800490072004C0073007000510062006500340067006B00770079004E0045002B003500570035004700510070004900500031004200460041004800500030005500480072005200510041005500550055005500410046004600460046004100420052005200520051004100550055005500550041004600460046004600410042005200520052005100420050005A0054004C00420064006F00370078002B0059006E0052006B0033006C00640077005000420047005200300072007200340035006F0044004E006100420064004B0064007000560073006D005A006C0068004F00790051004E007600490048007A00340035002B00540046006300540058005A0032003100330063004C0039006A0033003600770033006C006D007A004D00690078003300550068004500530035003300520034004200350078003600300041005A0065006F0053003200730057006E00320033006C0032005A006A004F002B00540068007A006C00770075004200740079007700480072006D007300430074007200560070004C006D0050005300620053004500360067003000380049004C00520069004E0047007A0047004E0075004F0052003600390061007800610041004300690069006900670044005700690064007200760052006F0034002B00530030004500680054002F0067004A00350048003600370071003600480053004700520062004200490069006F0079006A005A0062004A0077004700340036005A0072006E007600440038003000590075005A0062005300550044002F00410045006C0051004900790065007A0067003500580038002B0052002B004E0054007800360068004A0042004B006300720067004500640044002F004B00670044006F004A0032002B007A00510048004500680032004B0050006B005A0051004F0076006F007700720043006C00560049004C00430036006C00540041004C005200370063002B00350049004800380073003100460063005800380031003200670069004200430072002F0064003900760072005500570071004600720065007700670074006D004A004C0053004D005A00530053004D0066004C00300058002F00770042006D006F004100790061004B004B004B0041004300690069006900670044002F002F00320051003D003D0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003673000004D1510138F5540104D1510104D1510100000000E00200000000000050BB950980F1AE00504D990906000000000000000201000000000000DC939F09000000000000000050BB950980F1AE00504D99090700000000000000070000000000000000000000C044DC400000000050BB950980F1AE00504D9909080000000000000002010000000000005C2B9E09000000000000000050BB950980F1AE00504D990909000000000000000201000000000000BC6A9F09000000000000000050BB950980F1AE00504D99090A0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00504D99090B0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00504D99090C00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99090D00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99090E00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99090F00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091000000000000000000000000000000000000000000000000000000050BB950980F1AE00504D990911000000000000000201000000000000FC929F09000000000000000050BB950980F1AE00504D9909120000000000000002010000000000005C949F09000000000000000050BB950980F1AE00504D9909130000000000000002010000000000002C469E09000000000000000050BB950980F1AE00504D9909140000000000000002010000000000005C093204000000000000000050BB950980F1AE00504D99091500000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091600000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091700000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091800000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091900000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091A00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091B00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091C0000000000000002010000000000008C0F9709000000000000000050BB950980F1AE00504D99091D00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091E00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99091F00000000000000020100000000000004089E09000000000000000050BB950980F1AE00504D990920000000000000000201000000000000DC959F09000000000000000050BB950980F1AE00504D99092100000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092200000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092300000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092400000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092500000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092600000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092700000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99092800000000000000000000000000000000000000000000000000000050BB950980F1AE00504D990929000000000000000201000000000000EC12A109000000000000000050BB950980F1AE00504D99092A00000000000000020100000000000044259E09000000000000000050BB950980F1AE00504D99092B00000000000000020100000000000044259E09000000000000000050BB950980F1AE00504D99092C000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00504D99092D000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00504D99092E000000000000000201000000000000AC721B0A000000000000000050BB950980F1AE00504D99092F0000000000000002010000000000007C969F09000000000000000050BB950980F1AE00504D99093000000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093100000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093200000000000000020100000000000044259E09000000000000000050BB950980F1AE00504D99093300000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093400000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093500000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093600000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093700000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093800000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093900000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093A00000000000000000000000000000000000000000000000000000050BB950980F1AE00504D99093B000000000000000201000000000000DC709F09000000000000000050BB950980F1AE00504D99093C00000000000000020100000000000004089E09000000000000000050BB950980F1AE00504D99093D0000000000000002010000000000007CFF9D09000000000000000050BB950964454A0068903C043E000000A40152010000000000000000C023A009000000000000000050BB950980F1AE00704D9909000000000000000002010000000000005C499E09000000000000000050BB950980F1AE00704D9909010000000000000002010000000000003C29A109000000000000000050BB950980F1AE00704D99090200000000000000000000000000000000000000000000000000000050BB950980F1AE00704D990903000000000000000201000000000000B427A109000000000000000050BB950980F1AE00704D9909040000000000000002010000000000001C0D3204000000000000000050BB950980F1AE00704D9909050000000000000002010000000000000429A109000000000000000050BB950980F1AE00704D990906000000000000000201000000000000DC939F09000000000000000050BB950980F1AE00704D9909070000000000000007000000000000000000000080C7DC400000000050BB950980F1AE00704D9909080000000000000002010000000000005C2B9E09000000000000000050BB950980F1AE00704D990909000000000000000201000000000000CC28A109000000000000000050BB950980F1AE00704D99090A0000000000000002010000000000003C129E09000000000000000050BB950980F1AE00704D99090B0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00704D99090C00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99090D00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99090E00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99090F00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091000000000000000000000000000000000000000000000000000000050BB950980F1AE00704D990911000000000000000201000000000000FC929F09000000000000000050BB950980F1AE00704D9909120000000000000002010000000000005C949F09000000000000000050BB950980F1AE00704D9909130000000000000002010000000000002C469E09000000000000000050BB950980F1AE00704D9909140000000000000002010000000000009CD29E09000000000000000050BB950980F1AE00704D99091500000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091600000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091700000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091800000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091900000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091A00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091B00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091C0000000000000002010000000000008C0F9709000000000000000050BB950980F1AE00704D99091D00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091E00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99091F00000000000000020100000000000004089E09000000000000000050BB950980F1AE00704D990920000000000000000201000000000000DC959F09000000000000000050BB950980F1AE00704D99092100000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092200000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092300000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092400000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092500000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092600000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092700000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99092800000000000000000000000000000000000000000000000000000050BB950980F1AE00704D990929000000000000000201000000000000EC12A109000000000000000050BB950980F1AE00704D99092A00000000000000020100000000000044259E09000000000000000050BB950980F1AE00704D99092B00000000000000020100000000000044259E09000000000000000050BB950980F1AE00704D99092C000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00704D99092D000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00704D99092E000000000000000201000000000000AC721B0A000000000000000050BB950980F1AE00704D99092F0000000000000002010000000000007C969F09000000000000000050BB950980F1AE00704D99093000000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093100000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093200000000000000020100000000000044259E09000000000000000050BB950980F1AE00704D99093300000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093400000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093500000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093600000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093700000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093800000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093900000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093A00000000000000000000000000000000000000000000000000000050BB950980F1AE00704D99093B000000000000000201000000000000DC929F09000000000000000050BB950980F1AE00704D99093C00000000000000020100000000000004089E09000000000000000050BB950980F1AE00704D99093D0000000000000002010000000000007CFF9D09000000000000000050BB950964454A00088F3C043E000000A40152010000000000000000601FA009000000000000000050BB950980F1AE00904D990900000000000000000201000000000000B4039E09000000000000000050BB950980F1AE00904D9909010000000000000002010000000000002428A109000000000000000050BB950980F1AE00904D99090200000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99090300000000000000000000000000000000000000000000000000000050BB950980F1AE00904D9909040000000000000002010000000000005C28A109000000000000000050BB950980F1AE00904D9909050000000000000002010000000000007414A109000000000000000050BB950980F1AE00904D990906000000000000000201000000000000DC939F09000000000000000050BB950980F1AE00904D99090700000000000000070000000000000000000000403DD9400000000050BB950980F1AE00904D9909080000000000000002010000000000004C509E09000000000000000050BB950980F1AE00904D9909090000000000000002010000000000005490CA03000000000000000050BB950980F1AE00904D99090A0000000000000002010000000000002C229E09000000000000000050BB950980F1AE00904D99090B0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00904D99090C00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99090D00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99090E00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99090F00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091000000000000000000000000000000000000000000000000000000050BB950980F1AE00904D990911000000000000000201000000000000FC929F09000000000000000050BB950980F1AE00904D9909120000000000000002010000000000005C949F09000000000000000050BB950980F1AE00904D9909130000000000000002010000000000002C469E09000000000000000050BB950980F1AE00904D9909140000000000000002010000000000000CD39E09000000000000000050BB950980F1AE00904D99091500000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091600000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091700000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091800000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091900000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091A00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091B00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091C0000000000000002010000000000008C0F9709000000000000000050BB950980F1AE00904D99091D00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091E00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99091F00000000000000020100000000000004089E09000000000000000050BB950980F1AE00904D990920000000000000000201000000000000DC959F09000000000000000050BB950980F1AE00904D99092100000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092200000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092300000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092400000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092500000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092600000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092700000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99092800000000000000000000000000000000000000000000000000000050BB950980F1AE00904D990929000000000000000201000000000000EC12A109000000000000000050BB950980F1AE00904D99092A00000000000000020100000000000044259E09000000000000000050BB950980F1AE00904D99092B00000000000000020100000000000044259E09000000000000000050BB950980F1AE00904D99092C00000000000000020100000000000054479709000000000000000050BB950980F1AE00904D99092D00000000000000020100000000000054479709000000000000000050BB950980F1AE00904D99092E000000000000000201000000000000AC721B0A000000000000000050BB950980F1AE00904D99092F0000000000000002010000000000007C969F09000000000000000050BB950980F1AE00904D99093000000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093100000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093200000000000000020100000000000044259E09000000000000000050BB950980F1AE00904D99093300000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093400000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093500000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093600000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093700000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093800000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093900000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093A00000000000000000000000000000000000000000000000000000050BB950980F1AE00904D99093B000000000000000201000000000000BC959F09000000000000000050BB950980F1AE00904D99093C00000000000000020100000000000004089E09000000000000000050BB950980F1AE00904D99093D0000000000000002010000000000007CFF9D09000000000000000050BB950964454A00A88D3C043E000000A40152010000000000000000B074A009000000000000000050BB950980F1AE00B04D990900000000000000000201000000000000CCFD9D09000000000000000050BB950980F1AE00B04D990901000000000000000201000000000000EC27A109000000000000000050BB950980F1AE00B04D99090200000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99090300000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D9909040000000000000002010000000000005C6C9F09000000000000000050BB950980F1AE00B04D990905000000000000000201000000000000E429A109000000000000000050BB950980F1AE00B04D9909060000000000000002010000000000001C6C9F09000000000000000050BB950980F1AE00B04D990907000000000000000700000000000000000000008099D7400000000050BB950980F1AE00B04D9909080000000000000002010000000000004C509E09000000000000000050BB950980F1AE00B04D9909090000000000000002010000000000009C609F09000000000000000050BB950980F1AE00B04D99090A0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00B04D99090B0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00B04D99090C00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99090D00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99090E00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99090F00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091000000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D990911000000000000000201000000000000FC929F09000000000000000050BB950980F1AE00B04D9909120000000000000002010000000000005C949F09000000000000000050BB950980F1AE00B04D9909130000000000000002010000000000002C469E09000000000000000050BB950980F1AE00B04D9909140000000000000002010000000000006C439B09000000000000000050BB950980F1AE00B04D99091500000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091600000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091700000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091800000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091900000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091A00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091B00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091C0000000000000002010000000000008C0F9709000000000000000050BB950980F1AE00B04D99091D00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091E00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99091F00000000000000020100000000000004089E09000000000000000050BB950980F1AE00B04D990920000000000000000201000000000000DC959F09000000000000000050BB950980F1AE00B04D99092100000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092200000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092300000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092400000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092500000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092600000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092700000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99092800000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D990929000000000000000201000000000000EC12A109000000000000000050BB950980F1AE00B04D99092A00000000000000020100000000000044259E09000000000000000050BB950980F1AE00B04D99092B00000000000000020100000000000044259E09000000000000000050BB950980F1AE00B04D99092C000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00B04D99092D000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00B04D99092E000000000000000201000000000000AC721B0A000000000000000050BB950980F1AE00B04D99092F0000000000000002010000000000009C6E9F09000000000000000050BB950980F1AE00B04D99093000000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093100000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093200000000000000020100000000000044259E09000000000000000050BB950980F1AE00B04D99093300000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093400000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093500000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093600000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093700000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093800000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093900000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093A00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04D99093B000000000000000201000000000000BC959F09000000000000000050BB950980F1AE00B04D99093C00000000000000020100000000000004089E09000000000000000050BB950980F1AE00B04D99093D0000000000000002010000000000007CFF9D09000000000000000050BB950964454A00E88A3C043E000000A40152010000000000000000A074A009000000000000000050BB950980F1AE00B04C990900000000000000000201000000000000FC039E09000000000000000050BB950980F1AE00B04C9909010000000000000002010000000000007429A109000000000000000050BB950980F1AE00B04C99090200000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99090300000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C9909040000000000000002010000000000009428A109000000000000000050BB950980F1AE00B04C9909050000000000000002010000000000007414A109000000000000000050BB950980F1AE00B04C990906000000000000000201000000000000DC939F09000000000000000050BB950980F1AE00B04C99090700000000000000070000000000000000000000C013D8400000000050BB950980F1AE00B04C9909080000000000000002010000000000004C509E09000000000000000050BB950980F1AE00B04C9909090000000000000002010000000000003C6D3404000000000000000050BB950980F1AE00B04C99090A0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00B04C99090B0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00B04C99090C00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99090D00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99090E00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99090F00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091000000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C990911000000000000000201000000000000FC929F09000000000000000050BB950980F1AE00B04C9909120000000000000002010000000000005C949F09000000000000000050BB950980F1AE00B04C9909130000000000000002010000000000002C469E09000000000000000050BB950980F1AE00B04C9909140000000000000002010000000000002C0C3204000000000000000050BB950980F1AE00B04C99091500000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091600000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091700000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091800000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091900000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091A00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091B00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091C0000000000000002010000000000008C0F9709000000000000000050BB950980F1AE00B04C99091D00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091E00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99091F00000000000000020100000000000004089E09000000000000000050BB950980F1AE00B04C990920000000000000000201000000000000DC959F09000000000000000050BB950980F1AE00B04C99092100000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092200000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092300000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092400000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092500000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092600000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092700000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99092800000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C990929000000000000000201000000000000EC12A109000000000000000050BB950980F1AE00B04C99092A00000000000000020100000000000044259E09000000000000000050BB950980F1AE00B04C99092B00000000000000020100000000000044259E09000000000000000050BB950980F1AE00B04C99092C000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00B04C99092D000000000000000201000000000000EC731B0A000000000000000050BB950980F1AE00B04C99092E000000000000000201000000000000AC721B0A000000000000000050BB950980F1AE00B04C99092F0000000000000002010000000000007C969F09000000000000000050BB950980F1AE00B04C99093000000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093100000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093200000000000000020100000000000044259E09000000000000000050BB950980F1AE00B04C99093300000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093400000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093500000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093600000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093700000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093800000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093900000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093A00000000000000000000000000000000000000000000000000000050BB950980F1AE00B04C99093B000000000000000201000000000000DC709F09000000000000000050BB950980F1AE00B04C99093C00000000000000020100000000000004089E09000000000000000050BB950980F1AE00B04C99093D0000000000000002010000000000007CFF9D09000000000000000050BB950964454A0088893C043E000000A401520100000000000000009074A009000000000000000050BB950980F1AE00904C99090000000000000000020100000000000084399E09000000000000000050BB950980F1AE00904C9909010000000000000002010000000000001C2AA109000000000000000050BB950980F1AE00904C99090200000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99090300000000000000000000000000000000000000000000000000000050BB950980F1AE00904C9909040000000000000002010000000000002C27C903000000000000000050BB950980F1AE00904C9909050000000000000002010000000000007414A109000000000000000050BB950980F1AE00904C990906000000000000000201000000000000DC939F09000000000000000050BB950980F1AE00904C990907000000000000000700000000000000000000000099D8400000000050BB950980F1AE00904C9909080000000000000002010000000000004C509E09000000000000000050BB950980F1AE00904C9909090000000000000002010000000000002C27C903000000000000000050BB950980F1AE00904C99090A0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00904C99090B0000000000000002010000000000002C469E09000000000000000050BB950980F1AE00904C99090C00000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99090D00000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99090E00000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99090F00000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091000000000000000000000000000000000000000000000000000000050BB950980F1AE00904C990911000000000000000201000000000000FC929F09000000000000000050BB950980F1AE00904C9909120000000000000002010000000000005C949F09000000000000000050BB950980F1AE00904C9909130000000000000002010000000000002C469E09000000000000000050BB950980F1AE00904C9909140000000000000002010000000000003CE93204000000000000000050BB950980F1AE00904C99091500000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091600000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091700000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091800000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091900000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091A00000000000000000000000000000000000000000000000000000050BB950980F1AE00904C99091B00000000000000000000000000000000000000, 'Akademik', 'menikah', '', '1', '8782', '2016-01-01', '', '', null, '1', '', '', '', '0000-00-00 00:00:00', '0', '0', '2004-09-01', '0000-00-00', '2013-01-07', '850000', '08', '16', '1', '1', '08', '122392132021', '1/11/2009', '10/11/2009', '1', '2012', '1500000', '66151', '1', '3');
INSERT INTO `pegawai` VALUES ('83', '131135720001090010', '', '', 'FACHRIZAL ACHBAR, S.Pd.', '3572031809840004', 'BLITAR', '1968-07-07', 'L', 'LILIK SUHARSIH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Nias No.75 Sananwetan Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2008-07-01', '2008-07-01', '2013-01-07', '850000', '09', '24', '0', '', '', '', '', '', '', '', '', '', '1', '3');
INSERT INTO `pegawai` VALUES ('86', '131135720001110013', '', '0761763664210132', 'ENNY NAZARROHMAH, S.Si.', '3505136904350001', 'BLITAR', '1965-02-28', 'P', 'SITI MAHMUDAH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Pahlawan 35 Margomulyo Panggungrejo Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2008-07-01', '2008-07-01', '2013-01-07', '850000', '11', '24', '0', '', '', '', '', '', '', '', '', '', '4', '3');
INSERT INTO `pegawai` VALUES ('92', '131135720001110019', '', '', 'NAILA ZAHRIL MUNA, S.Pd.', '0000000000000000', 'BLITAR', '1965-06-19', 'P', 'NIKMATUL', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Ds. Sanankulon Kec. Sanankulon Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2013-01-07', '2013-01-07', '2013-01-07', '850000', '11', '20', '0', '', '', '', '', '', '', '', '', '66151', '1', '3');
INSERT INTO `pegawai` VALUES ('82', '131135720001180009', '', '', 'ISYROFIL HUDA, S.Pd.', '3572021805830002', 'BLITAR', '1959-09-18', 'L', 'SHOFIYAH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Kaliporong Pakunden 01 Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2008-07-01', '2008-07-01', '2013-01-07', '850000', '18', '10', '0', '', '', '', '', '', '', '', '', '66122', '1', '3');
INSERT INTO `pegawai` VALUES ('76', '131135720001190003', '', '9537746649200012', 'ENDRO GUNAWAN, SE.', '3505190205690002', 'BLITAR', '1968-07-28', 'L', 'OEMI KALSOEM', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Sangrahan Jugo Rt.1 Rw.1 Kesamben Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2003-07-01', '2003-07-01', '2013-01-07', '850000', '19', '39', '1', '1', '19', '091337412018', '1/11/2009', '01/11/2009', '1', '2009', '1500000', '', '4', '6');
INSERT INTO `pegawai` VALUES ('89', '131135720001240016', '', '', 'BENI MUSTAJIB, S.Pd.', '0000000000000000', 'BLITAR', '1962-12-10', 'L', 'KARTINI WIDIASTUTI', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Maron Rt.1 Rw.3 Kademangan Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2009-07-01', '2009-07-01', '2013-01-07', '850000', '24', '24', '0', '', '', '', '', '', '', '', '', '66161', '4', '3');
INSERT INTO `pegawai` VALUES ('90', '131135720001240017', '', '', 'SUYANTO, S.Pd.', '3572032207720002', 'BLITAR', '1967-07-27', 'L', 'ASMINI', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Veteran Plosokerep Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2009-07-01', '2009-07-01', '2013-01-07', '850000', '24', '20', '0', '', '', '', '', '', '', '', '', '', '1', '3');
INSERT INTO `pegawai` VALUES ('91', '131135720001240018', '', '6350758660200033', 'SAPTONI, SE', '3572011810800002', 'BLITAR', '1968-03-13', 'L', 'SUFIATIM', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Ir. Sukarno No.279 Rt.3 Rw.5 Sentul Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2011-07-01', '2011-07-01', '2013-01-07', '850000', '24', '20', '0', '', '', '', '', '', '', '', '', '', '1', '3');
INSERT INTO `pegawai` VALUES ('80', '131135720001270007', '', '', 'BOGI ARIYANTO, S.Pd', '0000000000000000', 'LUMAJANG', '1973-11-13', 'l', 'DASMINAH', '7', '2', '07', '', '', '', '', 'Islam', 'Jawa', '2', 'Kunir Rt.2 Rw.5 Wonodadi Blitar', '', '', '', '', '', '', '', 'Akademik', 'menikah', '', '1', '8782', null, '', '', null, '1', '', '', '', '0000-00-00 00:00:00', '0', '0', '2010-01-11', '0000-00-00', '2013-01-07', '850000', '27', '25', '0', '', '', '', '', '', '', '', '', '66155', '4', '3');
INSERT INTO `pegawai` VALUES ('88', '131135720001270015', '', '', 'DHYDIET SETIA BUDI, S.Pd.', '0000000000000000', 'BLITAR', '1974-02-04', 'L', 'SULASTRI', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Gadungan Rt.3 Rw.1 Gandusari Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2009-07-01', '2009-07-01', '2013-01-07', '850000', '27', '24', '0', '', '', '', '', '', '', '', '', '', '4', '3');
INSERT INTO `pegawai` VALUES ('79', '131135720001280006', '', '6655747649200012', 'MART HADI PRASETIYA, S.Sos.', '3572032303690004', 'PONOROGO', '1973-06-19', 'L', 'HARTINI', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Kemuning Gg.II Rt.1 Rw.3 Plosokerep Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2006-07-01', '2006-07-01', '2013-01-07', '850000', '28', '16', '0', '', '', '', '', '', '', '', '', '', '1', '3');
INSERT INTO `pegawai` VALUES ('87', '131135720001280014', '', '', 'ANA HAKIM SETIAWAN, ST.', '0000000000000000', 'BLITAR', '1969-10-07', 'L', 'UMI SALAMAH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Banggle I Rt.2 Rw.3 Kanigoro Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '850000', '28', '24', '0', '', '', '', '', '', '', '', '', '66171', '4', '3');
INSERT INTO `pegawai` VALUES ('74', '131135720001310001', '', '', 'H. MARYADI ABDUR ROHIM, S.Pd.', '0000000000000000', 'BLITAR', '1972-10-11', 'L', 'ALFIAH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Bendo Rt.6 Rw.3 Ponggok Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '1000000', '31', '24', '0', '', '', '', '', '', '', '', '1000000', '66153', '3', '3');
INSERT INTO `pegawai` VALUES ('81', '131135720001310008', '', '', 'ENDANG SRI ZUNTARI, S.Psi.', '0000000000000000', 'BLITAR', '1968-09-05', 'P', 'SUFIATIM', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Krenceng Rt.9 Rw.3 Nglegok Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2008-07-01', '2008-07-01', '2013-01-07', '850000', '31', '24', '0', '', '', '', '', '', '', '', '', '66181', '2', '3');
INSERT INTO `pegawai` VALUES ('85', '131135720001330012', '', '', 'LUTFIANA WAHYUNI, SP, M.Sos.', '3505106005780011', 'BLITAR', '1969-12-10', 'P', 'Hj. UMI SUNARTI', '8', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Irian 01 Jajar Rt.3 Rw.5 Kanigoro Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2008-07-08', '2008-07-08', '2013-01-07', '850000', '21', '24', '0', '', '', '', '', '', '', '', '', '66171', '3', '3');
INSERT INTO `pegawai` VALUES ('96', '131135720001330020', '', '8347747649300053', 'SITI MAHMUDAH', '3572025510690005', 'BLITAR', '1970-10-22', 'P', 'MIATUN', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Durian No.10 Tlumpu Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '900000', '', '', '0', '', '', '', '', '', '', '', '', '66126', '1', '3');
INSERT INTO `pegawai` VALUES ('97', '131135720001330021', '', '9333744646200023', 'SUPADI, S.Pd.I', '3572021001660002', 'BLITAR', '1971-12-08', 'L', 'BOISAH', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Karangbendo 02 Ponggok Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '900000', '', '', '0', '', '', '', '', '', '', '', '', '66153', '3', '3');
INSERT INTO `pegawai` VALUES ('98', '131135720001330022', '', '4137758660200023', 'M. RIDHODIN, ST.', '3572010508810003', 'BLITAR', '1976-01-23', 'L', 'KARYATUN', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Turi Selatan No.41 Jati Sukorejo Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2002-01-01', '2002-01-01', '2013-01-07', '900000', '', '', '0', '', '', '', '', '', '', '', '', '66120', '1', '3');
INSERT INTO `pegawai` VALUES ('99', '131135720001330023', '', '7557759662200003', 'KHOMARUDIN, S.Sos.', '3505102512810001', 'BLITAR', '1977-11-01', 'L', 'PARTIYEM', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Tegalrejo Rt.1 Rw.12 Sawentar Kanigoro Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '900000', '', '', '0', '', '', '', '', '', '', '', '', '66171', '3', '3');
INSERT INTO `pegawai` VALUES ('100', '131135720001330024', '', '0863760662300012', 'IDA MAYASARI', '3572027105820002', 'BLITAR', '1979-04-02', 'P', 'SUSIANI', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Jati Gg.X Balapan Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '850000', '', '', '0', '', '', '', '', '', '', '', '', '66121', '1', '3');
INSERT INTO `pegawai` VALUES ('101', '131135720001330025', '', '1856752653300052', 'ANIS NURQOLBIATI RASJIDA, SE.', '3572026405730001', 'BLITAR', '1980-09-06', 'P', 'MUTHMAINNAH ROSYID', '7', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Kalimas Patihan Rt.2 Rw.6 Pakunden Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '850000', '', '', '0', '', '', '', '', '', '', '', '', '66122', '1', '3');
INSERT INTO `pegawai` VALUES ('102', '131135720001330026', '', '', 'FUAD HABIBI, S.Pd.', '0000000000000000', 'BLITAR', '1970-10-04', 'L', 'UMI SALAMAH', '8', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Centong Rt.1 Rw.4 Purworejo Sanankulon Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2009-11-02', '2009-11-02', '2013-01-07', '850000', '', '', '0', '', '', '', '', '', '', '', '', '66151', '1', '3');
INSERT INTO `pegawai` VALUES ('103', '131135720001330027', '', '', 'SUNYOTO', '3505071304610001', 'MALANG', '1966-02-28', 'L', 'KARTIN ', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Pangkru Bendowulung Sanankulon Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '900000', '', '', '0', '', '', '', '', '', '', '', '', '66151', '1', '3');
INSERT INTO `pegawai` VALUES ('104', '131135720001330028', '', '', 'SANJAYA IWAN UTOMO', '0000000000000000', 'BLITAR', '1967-07-02', 'L', 'MARYATI', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Jati Gg.XI Balapan Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '850000', '', '', '0', '', '', '', '', '', '', '', '', '66121', '1', '3');
INSERT INTO `pegawai` VALUES ('105', '131135720001330029', '', '', 'JUARI', '0000000000000000', 'BLITAR', '1968-12-16', 'L', 'JUARI', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Jati Sukorejo Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2014-01-01', '2014-01-01', '2013-01-07', '750000', '', '', '0', '', '', '', '', '', '', '', '', '66121', '1', '1');
INSERT INTO `pegawai` VALUES ('106', '131135720001330030', '', '', 'SUJOTO', '0000000000000000', 'BLITAR', '1963-01-21', 'L', 'KULSUM', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Ds. Sanankulon Kec. Sanankulon Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2013-01-07', '1000000', '', '', '0', '', '', '', '', '', '', '', '', '66151', '1', '3');
INSERT INTO `pegawai` VALUES ('107', '131135720001330031', '', '', 'MOHAMAD SHOLIHIN', '0000000000000000', 'BLITAR', '1956-08-28', 'L', 'RUKAYAH', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Jati Gg.X Balapan Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2009-07-01', '2009-07-01', '2013-01-07', '800000', '', '', '0', '', '', '', '', '', '', '', '', '66121', '1', '3');
INSERT INTO `pegawai` VALUES ('108', '131135720001330032', '', '', 'HARIONO', '0000000000000000', 'BLITAR', '1975-09-12', 'L', 'KAMDIYAH', '2', '2', '', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Jati Sukorejo Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2013-01-03', '2013-01-03', '2013-01-07', '800000', '', '', '0', '', '', '', '', '', '', '', '', '66121', '1', '1');
INSERT INTO `pegawai` VALUES ('57', '195608281979032008', '', '', 'Hj. YUSTITIK, S.Ag.', '3505116808560003', 'BLITAR', '1957-01-10', 'P', 'MASLICHAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Gajah Mada No.18 Ngebrak Rt.3 Rw.2 Tawangsari Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2007-01-10', '4093300', '01', '24', '1', '1', '01', '076016043001', '1/11/2009', '10/11/2009', '1', '2007', '4093300', '66182', '3', '3');
INSERT INTO `pegawai` VALUES ('59', '195710011985031020', '', '0333735636200013', 'Drs. KOMARI', '3505100110570001', 'NGAWI', '1963-05-07', 'L', 'ISMIRAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Gogodeso Rt.1 Rw.2 Kanigoro Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1985-01-01', '1985-01-01', '1999-01-04', '4093300', '13', '24', '1', '1', '13', '091289852024', '1/11/2009', '10/11/2009', '1', '2009', '4093300', '66171', '2', '3');
INSERT INTO `pegawai` VALUES ('34', '195909281985031002', '', '5073763920000023', 'Drs. ALI AHMAT', '3504131809590002', 'TULUNGAGUNG', '1967-05-28', 'L', 'AMILAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Sumberagung Rt.2 Rw.5 Rejotangan Tulungagung', '08123303870', '08123303870', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1985-01-03', '1985-01-03', '2007-01-04', '4093300', '02', '28', '1', '1', '02', '093129362030', '1/11/2009', '01/11/2009', '1', '2007', '4093300', '', '3', '4');
INSERT INTO `pegawai` VALUES ('94', '196012081987032001', '', '1540738641300013', 'WARJIYAH, A.Ma', '3505024812600001', 'NGANJUK', '1968-02-02', 'P', 'DASMINAH', '4', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Bendorejo Rt.1 Rw.2 Udanawu Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1987-03-01', '1987-03-01', '2007-01-04', '3397500', '', '', '0', '', '', '', '', '', '', '', '', '66154', '4', '3');
INSERT INTO `pegawai` VALUES ('36', '196210121987032002', '', '4344740641300013', 'SITI ASROFIN, S.Pd.', '3505015210620002', 'BLITAR', '1966-01-07', 'P', 'SITI MAEMONAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Pikatan Wonodadi Blitar', '081332050497', '081332050497', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1987-01-03', '1987-01-03', '2009-01-04', '3968300', '11', '24', '1', '1', '11', '091839877046', '1/11/2009', '01/11/2009', '1', '2009', '3968300', '66155', '3', '3');
INSERT INTO `pegawai` VALUES ('56', '196301212006042006', '', '7453741644300002', 'Dra. ENDAH TRIASIH', '3572036101630002', 'BLITAR', '1977-07-26', 'P', 'OEMI KALSOEM', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Brau No.27 Bendogerit Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2006-01-04', '2006-01-04', '0000-00-00', '3652800', '21', '24', '1', '1', '21', '092285382021', '1/11/2009', '01/11/2009', '1', '2009', '3652800', '', '2', '3');
INSERT INTO `pegawai` VALUES ('93', '196304101986031001', '', '', 'BAROKAH, S.Sos.', '0000000000000000', 'BLITAR', '1973-06-19', 'L', 'UMI SALAMAH', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Wonorejo Srengat Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2014-01-01', '2014-01-01', '2013-01-07', '3541300', '', '', '0', '', '', '', '', '', '', '', '', '66152', '3', '4');
INSERT INTO `pegawai` VALUES ('28', '196306091992031002', '', '9941741643200002', 'Drs. ASHARI', '3504130906630004', 'BLITAR', '1977-07-24', 'L', 'ALFIJAH', '8', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Raya Rejotangan RT.2 Rw.3 Tulunggagung', '08113643982', '08113643982', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1992-01-03', '1992-01-03', '2005-01-04', '3847200', '09', '24', '1', '1', '09', '071981110989', '1/11/2009', '01/11/2009', '1', '2008', '3847200', '', '3', '4');
INSERT INTO `pegawai` VALUES ('60', '196307051985121004', '', '6037741643200033', 'SUMEIDIN, S.Pd', '3505010507630003', 'TULUNGAGUNG', '1975-03-28', 'L', 'MUJIAH', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Kunir wonodadi Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1985-01-01', '1985-01-01', '2001-01-10', '3363200', '19', '24', '0', '', '', '', '', '', '', '', '3363200', '66155', '4', '3');
INSERT INTO `pegawai` VALUES ('70', '196412082007011018', '', '3540742645200003', 'USMUNI, S.Pd.', '3505060812640002', 'BANGKALAN', '1978-07-15', 'L', 'MU`AFA', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Prambutan, Kawedusan Rt.2 Rw.3 Ponggok Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-01', '2007-01-01', '2011-01-02', '2820700', '06', '30', '1', '1', '06', '105021182221', '1/11/2009', '01/11/2009', '1', '2009', '2820700', '66153', '3', '3');
INSERT INTO `pegawai` VALUES ('32', '196502281993032001', '', '7560743644300032', 'Dra. FATHUL MUNIFAH', '3505226802650001', 'BLITAR', '1973-03-06', 'P', 'SUPANGATIN', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Gading Rt.1 Rw.7 Selopuro Blitar', '08113643683', '08113643683', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1993-01-03', '1993-01-03', '2006-01-04', '3729700', '02', '24', '1', '1', '02', '092635892023', '1/11/2009', '01/11/2009', '1', '2008', '3729700', '', '4', '3');
INSERT INTO `pegawai` VALUES ('27', '196506051991032003', '', '8937745644300072', 'Dra. ISNA MARWIYAH', '3504114506650005', 'TULUNGAGUNG', '1964-08-12', 'P', 'HASTUTI', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Tanen Rejotangan', '087756360165', '087756360165', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1991-01-03', '1991-01-03', '2002-01-04', '3847200', '11', '24', '1', '1', '11', '118021390006', '1/11/2011', '10/11/2011', '1', '2011', '3847200', '', '4', '3');
INSERT INTO `pegawai` VALUES ('33', '196506191994031003', '', '1951743644200022', 'Drs. MUSTOFA', '3505111906650001', 'BLITAR', '1986-03-06', 'L', 'MARDJIATUN', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Ronggowasito Kranggan Pojok Garum', '082332299123', '082332299123', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1994-01-03', '1994-01-03', '2007-01-04', '3729700', '27', '12', '1', '1', '27', '092187892017', '1/11/2009', '10/11/2009', '1', '2009', '3729700', '66182', '2', '4');
INSERT INTO `pegawai` VALUES ('29', '196511091993032001', '', '8441743644300003', 'Dra, SITI NURHIDAYAH', '3505114911650002', 'BLITAR', '1972-07-07', 'P', 'SOEMASTRI', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Ronggowasito Kranggan Pojok Garum', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1993-09-02', '1993-09-02', '2005-01-04', '3729700', '14', '24', '1', '1', '14', '021789392010', '1/11/2009', '10/11/2009', '1', '2009', '3729700', '66182', '2', '3');
INSERT INTO `pegawai` VALUES ('30', '196511301994031002', '', '3462743644200003', 'Drs. H. M. MAKMUN, M.Pd.', '3505033011650002', 'BLITAR', '1972-02-07', 'L', 'MUSLIMAH', '8', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Wilis Dermojayan Rt.4 Rw.1 Srengat Blitar', '08125200023', '08125200023', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1994-01-03', '1994-01-03', '2005-01-04', '3729700', '13', '24', '1', '1', '13', '092740392019', '1/11/2009', '01/11/2009', '1', '2009', '3729700', '66152', '3', '4');
INSERT INTO `pegawai` VALUES ('26', '196602011992031002', '', '3533744645200002', 'Drs. H. KHUSNUL KHULUK, M.Pd', '3572010102660002', 'BLITAR', '1954-01-02', 'L', 'Hj. SITI RUKAYAH', '8', '1', '11', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Ciliwung 106 Bendo Kepanjenkidul Kota Blitar', '081234109000', '081234109000', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2011-01-04', '4009900', '11', '24', '1', '1', '11', '021285897010', '1/11/2009', '01/11/2009', '1', '2009', '4009900', '66100', '1', '4');
INSERT INTO `pegawai` VALUES ('53', '196602281999032002', '', '8560744647300012', 'KUMIATIN, S.Pd.', '3505126802660002', 'CILACAP', '1967-10-04', 'P', 'KASIYAH', '7', '1', '09', '', '', '', '', 'islam', 'jawa', '2', 'Sumberjo Rt.1 Rw.1 Sutojayan Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1999-01-03', '1999-01-03', '2012-04-01', '3160900', '31', '24', '1', '1', '31', '092635897024', '1/11/2009', '10/11/2009', '1', '2009', '3160900', '66172', '3', '4');
INSERT INTO `pegawai` VALUES ('63', '196607011995122001', '', '1033744646300023', 'Dra. YULIASTUTI', '3505104107660037', 'BLITAR', '1969-02-05', 'P', 'KATMINEM', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jatisari Jatinom Kanigoro Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1995-01-01', '1995-01-01', '2009-01-10', '3520300', '15', '24', '1', '1', '15', '091288397022', '1/11/2009', '10/11/2009', '1', '2009', '3520300', '66171', '2', '3');
INSERT INTO `pegawai` VALUES ('54', '196702072006042008', '', '3539745647300042', 'MUALLIMAH, S.Pd.', '3572024702670001', 'BLITAR', '1967-03-05', 'P', 'MASLIKAH', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Pakunden RT.4 Rw.10 Sukorejo Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2006-01-04', '2006-01-04', '0000-00-00', '3128200', '30', '24', '1', '1', '30', '091585902016', '1/11/2009', '01/11/2009', '1', '2011', '3128200', '66122', '1', '3');
INSERT INTO `pegawai` VALUES ('61', '196705281992021002', '', '2860745646200002', 'SYAIKHUL ANAM, S.Pd.', '3505032805670002', 'BLITAR', '1977-06-25', 'L', 'MURTIYEM', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Bagelenan Rt.1 Rw.2 srengat Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1992-01-01', '1992-01-01', '2009-01-10', '3520300', '15', '24', '1', '1', '15', '092637402015', '1/11/2009', '10/11/2009', '1', '2009', '3520300', '66152', '3', '3');
INSERT INTO `pegawai` VALUES ('37', '196707271994032001', '', '7059745647300063', 'Dra. Hj. FARIDATUL HASANAH', '3572036707670004', 'BLITAR', '1969-03-23', 'P', 'NUR CHOTFI', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Riau No.2 Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2009-01-10', '3729700', '21', '24', '1', '1', '21', '092588402041', '1/11/2009', '10/11/2009', '1', '2009', '3729700', '66132', '1', '3');
INSERT INTO `pegawai` VALUES ('62', '196802021994122003', '', '6534746647300012', 'Dra. ENDAR SUGESTI', '3572014202680004', 'BLITAR', '1981-04-21', 'P', 'SRIASIH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Melati No.77 Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1994-01-01', '1994-01-01', '2007-01-10', '3520300', '13', '24', '1', '1', '13', '092285382020', '1/11/2009', '01/11/2009', '1', '2009', '3520300', '66112', '1', '3');
INSERT INTO `pegawai` VALUES ('38', '196803131997032002', '', '5645746647300002', 'ANIK NURCHATIMAH, S.Pd.', '3505145303680002', 'BLITAR', '1981-10-01', 'P', 'LATIFAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Tumpang Rt.3 Rw.1 Talun Blitar', '085856140052', '085856140052', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1997-01-03', '1997-01-03', '2010-01-04', '3505400', '11', '24', '1', '1', '11', '021886407009', '1/11/2009', '01/11/2009', '1', '2009', '3505400', '', '3', '3');
INSERT INTO `pegawai` VALUES ('44', '196805092005012001', '', '8841746648300012', 'ANDRIASTUTI JS, S.Pd.', '3572024905680003', 'BLITAR', '1983-05-18', 'P', 'KARTINI WIDIASTUTI', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Rayung Wulan Barat Rt.1 Rw.2 Kel. Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '3032600', '06', '24', '1', '1', '06', '091687407023', '1/11/2009', '01/11/2009', '1', '2009', '3032600', '66121', '1', '3');
INSERT INTO `pegawai` VALUES ('31', '196807071994031006', '', '6039746648200023', 'Drs. SULTONI', '3505220707680001', 'BLITAR', '1984-09-18', 'L', 'MARKAPAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jambewangi Rt.2 Rw.2 Selopuro Blitar', '08113643705', '08113643705', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1994-01-03', '1994-01-03', '2005-01-04', '3729700', '14', '24', '1', '1', '14', '091588407020', '1/11/2009', '10/11/2009', '1', '2009', '3729700', '', '4', '3');
INSERT INTO `pegawai` VALUES ('35', '196807281994032005', '', '6060746648300033', 'Dra. MUSLIMATUL AQOBAH', '3572026807680003', 'BLITAR', '1984-03-23', 'P', 'MUSYIROTUN', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Griya Kalimas Indah Blok.F-2 Pakunden Blitar', '08113643689', '08113643689', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1994-01-03', '1994-01-03', '2006-01-10', '3729700', '09', '24', '1', '1', '09', '092638407022', '1/11/2009', '10/11/2009', '1', '2009', '3729700', '66122', '1', '3');
INSERT INTO `pegawai` VALUES ('55', '196812162006042011', '', '0548746648300033', 'Dra. SRI ENDAHWORO', '3572035612800005', 'PASURUAN', '1978-05-20', 'P', 'SARTIWI', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Enggano I/20 Sananwetan Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2006-01-04', '2006-01-04', '0000-00-00', '3128200', '07', '24', '1', '1', '07', '111562138001', '1/11/2009', '10/11/2009', '1', '2011', '3128200', '', '2', '3');
INSERT INTO `pegawai` VALUES ('42', '196907101999032001', '', '7042747649300023', 'TITISARI, S.Pd.', '3572035007690004', 'BLITAR', '1985-04-29', 'P', 'MOEDJIANAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Dadap No.10 Rembang Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1999-01-07', '1999-01-07', '2011-01-10', '3398400', '26', '24', '1', '1', '26', '091738412015', '1/11/2009', '01/11/2009', '1', '2009', '3398400', '', '1', '3');
INSERT INTO `pegawai` VALUES ('45', '196910122005012003', '', '2344747649300063', 'SITI MASRUROH, S.Pd.', '3505065210690001', 'BLITAR', '1974-11-21', 'P', 'SUPARMI', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Dsn. Pupus Rt.7 Rw.4 Bacem Ponggok Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '3328300', '14', '24', '1', '1', '14', '091839912016', '1/11/2009', '10/11/2009', '1', '2010', '3328300', '66153', '3', '3');
INSERT INTO `pegawai` VALUES ('52', '197004101998032001', '', '0742748650300022', 'ISTIQOMAH, S.Pd.', '3572035004700001', 'BLITAR', '1980-02-21', 'P', 'MARKAPAH', '7', '1', '09', '', '', '', '', 'islam', 'jawa', '2', 'Klampok Sananwetan Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2010-01-04', '3363200', '07', '24', '0', '', '', '', '', '', '', '', '3363200', '', '2', '3');
INSERT INTO `pegawai` VALUES ('46', '197010222005011001', '', '5354748650200013', 'SOEGENG RUPIANTO, SS.', '3505132210700002', 'SURABAYA', '1968-02-19', 'L', 'SOEMINAH', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Margomulyo Rt.4 Rw.5 Panggungrejo Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '2850300', '07', '24', '1', '1', '07', '092339917007', '1/11/2009', '01/11/2009', '1', '2009', '2850300', '', '3', '3');
INSERT INTO `pegawai` VALUES ('47', '197108122005012004', '', '5144749651300063', 'RINI SATYARI, S.Pd.', '3572065208710003', 'BLITAR', '1969-07-22', 'P', 'NUR RAHAYU', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Halir No.30 Bendogerit Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '2850300', '15', '24', '1', '1', '15', '091838922012', '1/11/2009', '10/11/2009', '1', '2010', '2850300', '', '2', '3');
INSERT INTO `pegawai` VALUES ('73', '197207022007011019', '', '2053750652300023', 'UMMU ROISYAH, S.Ag', '3572036107720001', 'BLITAR', '1980-10-18', 'P', 'CHUSUSIYAH', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Sulawesi No.50 Sawahan Klampok Sananwetan Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2012-01-10', '2734600', '03', '24', '1', '1', '03', '075315064001', '1/11/2009', '10/11/2009', '1', '2007', '2734600', '', '1', '3');
INSERT INTO `pegawai` VALUES ('72', '197207071996032002', '', '1039750651300023', 'ANIQOTUZ ZUHROH, S.Ag', '3504134707720002', 'TULUNGAGUNG', '1990-01-07', 'P', 'ISTI`ANAH', '8', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Rowoubeng Tanen Rejotangan Tulungagung', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '0000-00-00', '0000-00-00', '2012-01-04', '3729300', '04', '24', '1', '1', '04', '095021182158', '1/11/2009', '01/11/2009', '1', '2009', '3729300', '', '3', '3');
INSERT INTO `pegawai` VALUES ('43', '197211102001121004', '', '1442750652200043', 'M. NUR ROHMAN, S.Pd.', '3572021011720002', 'BLITAR', '1963-10-04', 'L', 'SITI ASIYAH', '7', '1', '09', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Cemara Gg.XII No.67 Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2001-01-12', '2001-01-12', '2012-01-10', '3160900', '09', '24', '1', '1', '09', '091740427009', '1/11/2009', '10/11/2009', '1', '2010', '3160900', '66125', '1', '3');
INSERT INTO `pegawai` VALUES ('69', '197303062007102001', '', '9638751652300002', 'UNSAROYANI, S.Kom.', '3505114603730001', 'BLITAR', '1960-08-12', 'P', 'SITI RO`AZAH', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Dusun Sugihan Ds. Pojok Kec. Garum Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2010-01-11', '2734600', '28', '24', '0', '', '', '', '', '', '', '', '2734600', '66182', '3', '3');
INSERT INTO `pegawai` VALUES ('41', '197306191999032002', '', '5951751653300012', 'PUJIASTUTI, S.Pd.', '3572035906730001', 'BLITAR', '1979-03-20', 'P', 'MARIATUN', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Dadap No.7 Rembang Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1999-01-03', '1999-01-03', '2011-01-10', '3398400', '26', '24', '1', '1', '26', '092187932009', '1/11/2009', '10/11/2009', '1', '2010', '3398400', '', '1', '3');
INSERT INTO `pegawai` VALUES ('65', '197306192007101001', '', '5951751654200002', 'M. JAUHAR FATHONI, S.Ag.', '3505031906730001', 'BLITAR', '1969-10-15', 'L', 'MUNTIYAH', '8', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Rambutan Wonorejo Rt.3 Rw.1 Srengat Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2010-01-11', '2734600', '04', '25', '1', '1', '04', '122372169018', '1/11/2009', '10/11/2009', '1', '2012', '2734600', '66152', '3', '4');
INSERT INTO `pegawai` VALUES ('39', '197311131998032001', '', '1445751653300023', 'LUFFI SANDERIANA, S.Pd.', '3572025311730006', 'LUMAJANG', '1966-01-10', 'P', 'RINI SUGIARTI', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. C.II No.10 Perumnas Pakunden Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1998-01-03', '1998-01-03', '2011-01-10', '3505400', '26', '24', '1', '1', '26', '112272142003', '1/11/2009', '10/11/2009', '1', '2011', '3505400', '66122', '1', '3');
INSERT INTO `pegawai` VALUES ('40', '197404021998032002', '', '8734752653300002', 'DANIK MARYATI, S.Pd.', '3504104204740004', 'TULUNGAGUNG', '1980-08-05', 'P', 'PAINAH', '7', '1', '10', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Raya Jabalsari Rt.1 Rw.1 Sumbergempol Tulungagung', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '1998-01-03', '1998-01-03', '2011-01-10', '3505400', '26', '24', '1', '1', '26', '112272177002', '1/11/2009', '01/11/2009', '1', '2011', '3505400', '', '4', '3');
INSERT INTO `pegawai` VALUES ('58', '197509122006041027', '', '', 'MOH. ZAINUL FAJRI, M.Ag.', '0000000000000000', 'BLITAR', '1981-12-25', 'L', 'SITI CHOFSOH', '8', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'desa sumber sanankulon blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2006-01-04', '2006-01-04', '0000-00-00', '2820700', '01', '24', '0', '', '', '', '', '', '', '', '2820700', '66151', '2', '3');
INSERT INTO `pegawai` VALUES ('48', '197601232005012003', '', '0455754655300012', 'DWI LESTARI, S.Pd.', '3572026301760001', 'BLITAR', '1982-05-31', 'P', 'SRI WILUJENG', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Cemara No.147 Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '2850300', '18', '24', '1', '1', '18', '112072156001', '1/11/2009', '01/11/2009', '1', '2011', '2850300', '66125', '1', '3');
INSERT INTO `pegawai` VALUES ('49', '197701112005011003', '', '3443755656200012', 'CIPTO, S.Ag.', '3505071101770005', 'BLITAR', '1973-05-24', 'L', 'RUMPIKANI', '8', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Sumberjo Rt.1 Rw.1 Sanankulon Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2013-01-10', '2850300', '08', '24', '1', '1', '08', '091286337031', '1/11/2009', '10/11/2009', '1', '2007', '2850300', '66151', '2', '3');
INSERT INTO `pegawai` VALUES ('66', '197707242007102004', '', '2056755657300013', 'NUR BADRIYAH, S.Pd.', '3572016407770002', 'BLITAR', '1986-03-23', 'P', 'SHOFIYATUN', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Cikaso Tanggung Rt.2 Rw.4 Kepanjenkidul Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2010-01-11', '2734600', '08', '12', '1', '1', '08', '092438452003', '1/11/2009', '01/11/2009', '1', '2009', '2734600', '66132', '2', '3');
INSERT INTO `pegawai` VALUES ('64', '197707262007101001', '', '3058755656200003', 'ACHMAD BISRI, SE.', '3505072607770003', 'BLITAR', '1963-04-13', 'L', 'ALFIAH', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Kalibrantas No.101 Dawuhan Kauman Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2011-01-08', '2820700', '19', '24', '1', '1', '19', '092538452004', '1/11/2009', '01/11/2009', '1', '2009', '2820700', '66100', '1', '3');
INSERT INTO `pegawai` VALUES ('68', '197807152007101004', '', '0047756658300013', 'NANANG ZAINAL ARIFIN, S.Pd.', '3504131507780003', 'BLITAR', '1984-03-12', 'L', 'SUTJIATI', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Jeding Utara, Aryojeding Rt.1 Rw.9 Rejotangan Tulungagung', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2011-01-08', '2734600', '14', '24', '1', '1', '14', '091988457007', '1/11/2009', '01/11/2009', '1', '2010', '2734600', '', '3', '3');
INSERT INTO `pegawai` VALUES ('50', '197902042005011002', '', '8536757658200002', 'BASTOMI, S.Pd.', '3505110402790001', 'BLITAR', '1968-01-07', 'L', 'SITI MARIYAM', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Lingk. Dadapan Rt.3 Rw.2 Sumberdiren Garum Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '2850300', '17', '24', '1', '1', '17', '112042152001', '1/11/2009', '01/11/2009', '1', '2011', '2850300', '66182', '3', '3');
INSERT INTO `pegawai` VALUES ('95', '197903202007101002', '', '', 'SAHAB', '3504032003790001', 'TULUNGAGUNG', '1950-01-07', 'L', 'MARSUTI', '2', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Jl. Belimbing Karangsari Blitar', '', '', '', '', '', '', '', 'Non Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2014-01-04', '2054400', '', '', '0', '', '', '', '', '', '', '', '', '66125', '1', '3');
INSERT INTO `pegawai` VALUES ('67', '197903282007101001', '', '3660757658200012', 'NUR ANDI ISADRAIZEED, ST.', '3505092803790002', 'BLITAR', '1983-03-27', 'L', 'SUDARMI', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Krenceng Rt.2 Rw.1 Nglegok Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2007-01-10', '2007-01-10', '2010-01-11', '2734600', '28', '12', '1', '1', '28', '092636462005', '1/11/2009', '10/11/2009', '1', '2010', '2734600', '66181', '2', '3');
INSERT INTO `pegawai` VALUES ('51', '198006092005011001', '', '1941758658200002', 'M. MANSUR, S.Pd.', '3505070906800003', 'BLITAR', '1985-01-07', 'L', 'MUHANTINI', '7', '1', '08', '', '', '', '', 'islam', 'jawa', '2', 'Dsn. Nglaran Rt.2 Rw.1 Purworejo Sanankulon Blitar', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2005-01-01', '2005-01-01', '2011-01-10', '2850300', '09', '24', '1', '1', '09', '111572196001', '1/11/2009', '10/11/2009', '1', '2011', '2850300', '66151', '1', '3');
INSERT INTO `pegawai` VALUES ('71', '198606032009122001', '', '', 'IMROATUL MUFIDAH, S.Pd.', '3517114306860009', 'JOMBANG', null, 'P', 'ISTIQOMAH', '7', '1', '07', '', '', '', '', 'islam', 'jawa', '2', 'Dsn. Trimulyo Rt.1 Rw.6 Ngujang Kedungwaru Tulungagung', '', '', '', '', '', '', '', 'Akademik', '', '', '1', '8782', null, '', '', null, null, '', '', '', '0000-00-00 00:00:00', '0', '0', '2010-07-01', '2010-07-01', '2010-01-09', '2390600', '13', '10', '0', '', '', '', '', '', '', '', '2390600', '', '4', '3');
INSERT INTO `pegawai` VALUES ('25', '2', null, null, 'Ahasun Naseh', null, 'Tulunguagung', '1988-01-05', 'l', '', null, null, null, '', '', 's,Kom', null, 'Islam', 'Jawa', '2', 'Malang', '', '085646742211', 'come.thinkblue@gmail.com', null, null, null, null, 'Akademik', 'menikah', '', '1', '08782', null, null, null, null, '0', null, null, null, '2013-02-02 11:21:58', '23366', '0', null, null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `pegdiklat`
-- ----------------------------
DROP TABLE IF EXISTS `pegdiklat`;
CREATE TABLE `pegdiklat` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `iddiklat` int(10) unsigned NOT NULL,
  `tahun` int(10) unsigned NOT NULL,
  `sk` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pegdiklat_pegawai` (`nip`),
  KEY `FK_pegdiklat_diklat` (`iddiklat`),
  KEY `IX_pegdiklat_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pegdiklat_diklat` FOREIGN KEY (`iddiklat`) REFERENCES `diklat` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pegdiklat_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegdiklat
-- ----------------------------

-- ----------------------------
-- Table structure for `peggaji`
-- ----------------------------
DROP TABLE IF EXISTS `peggaji`;
CREATE TABLE `peggaji` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `gaji` varchar(15) NOT NULL,
  `sk` varchar(45) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `judulsk` varchar(255) DEFAULT NULL,
  `tanggalsk` varchar(255) DEFAULT NULL,
  `dok` text,
  `doaudit` tinyint(1) DEFAULT '1',
  `terakhir` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peggaji_pegawai` (`nip`),
  KEY `IX_peggaji_ts` (`ts`,`issync`),
  CONSTRAINT `FK_peggaji_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of peggaji
-- ----------------------------

-- ----------------------------
-- Table structure for `peggol`
-- ----------------------------
DROP TABLE IF EXISTS `peggol`;
CREATE TABLE `peggol` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `golongan` varchar(14) NOT NULL,
  `tmt` date NOT NULL,
  `sk` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `judulsk` varchar(255) DEFAULT NULL,
  `tanggalsk` varchar(45) DEFAULT NULL,
  `dok` text,
  `petugas` varchar(45) DEFAULT NULL,
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peggol_pegawai` (`nip`),
  KEY `FK_peggol_golongan` (`golongan`),
  KEY `IX_peggol_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of peggol
-- ----------------------------
INSERT INTO `peggol` VALUES ('1', '196602011992031002', '11', '2011-10-13', '13/10/2011', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:25', '0', '0');
INSERT INTO `peggol` VALUES ('2', '196506051991032003', '10', '0000-00-00', '13/10/2011', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:26', '0', '0');
INSERT INTO `peggol` VALUES ('3', '196306091992031002', '10', '1991-01-03', '01/03/1991', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:27', '0', '0');
INSERT INTO `peggol` VALUES ('4', '196511091993032001', '10', '1992-01-03', '01/03/1992', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:28', '0', '0');
INSERT INTO `peggol` VALUES ('5', '196511301994031002', '10', '1993-09-02', '09/02/1993', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:29', '0', '0');
INSERT INTO `peggol` VALUES ('6', '196807071994031006', '10', '1994-01-03', '01/03/1994', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:30', '0', '0');
INSERT INTO `peggol` VALUES ('7', '196502281993032001', '10', '1994-01-03', '01/03/1994', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:31', '0', '0');
INSERT INTO `peggol` VALUES ('8', '196506191994031003', '10', '1993-01-03', '01/03/1993', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:32', '0', '0');
INSERT INTO `peggol` VALUES ('9', '195909281985031002', '10', '1994-01-03', '01/03/1994', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:33', '0', '0');
INSERT INTO `peggol` VALUES ('10', '196807281994032005', '10', '1985-01-03', '01/03/1985', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:34', '0', '0');
INSERT INTO `peggol` VALUES ('11', '196210121987032002', '10', '1994-01-03', '01/03/1994', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:35', '0', '0');
INSERT INTO `peggol` VALUES ('12', '196707271994032001', '10', '1987-01-03', '01/03/1987', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:36', '0', '0');
INSERT INTO `peggol` VALUES ('13', '196803131997032002', '10', '0000-00-00', '21/11/2000', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:37', '0', '0');
INSERT INTO `peggol` VALUES ('14', '197311131998032001', '10', '1997-01-03', '01/03/1997', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:38', '0', '0');
INSERT INTO `peggol` VALUES ('15', '197404021998032002', '10', '1998-01-03', '01/03/1998', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:39', '0', '0');
INSERT INTO `peggol` VALUES ('16', '197306191999032002', '10', '1998-01-03', '01/03/1998', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:40', '0', '0');
INSERT INTO `peggol` VALUES ('17', '196907101999032001', '10', '1999-01-03', '01/03/1999', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:41', '0', '0');
INSERT INTO `peggol` VALUES ('18', '197211102001121004', '09', '1999-01-07', '01/07/1999', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:42', '0', '0');
INSERT INTO `peggol` VALUES ('19', '196805092005012001', '08', '2001-01-12', '01/12/2001', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:43', '0', '0');
INSERT INTO `peggol` VALUES ('20', '196910122005012003', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:44', '0', '0');
INSERT INTO `peggol` VALUES ('21', '197010222005011001', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:45', '0', '0');
INSERT INTO `peggol` VALUES ('22', '197108122005012004', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:46', '0', '0');
INSERT INTO `peggol` VALUES ('23', '197601232005012003', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:47', '0', '0');
INSERT INTO `peggol` VALUES ('24', '197701112005011003', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:48', '0', '0');
INSERT INTO `peggol` VALUES ('25', '197902042005011002', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:49', '0', '0');
INSERT INTO `peggol` VALUES ('26', '198006092005011001', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:50', '0', '0');
INSERT INTO `peggol` VALUES ('27', '197004101998032001', '09', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:51', '0', '0');
INSERT INTO `peggol` VALUES ('28', '196602281999032002', '09', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:52', '0', '0');
INSERT INTO `peggol` VALUES ('29', '196702072006042008', '08', '1999-01-03', '01/03/1999', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:53', '0', '0');
INSERT INTO `peggol` VALUES ('30', '196812162006042011', '08', '2006-01-04', '01/04/2006', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:54', '0', '0');
INSERT INTO `peggol` VALUES ('31', '196301212006042006', '08', '2006-01-04', '01/04/2006', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:55', '0', '0');
INSERT INTO `peggol` VALUES ('32', '195608281979032008', '10', '2006-01-04', '01/04/2006', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:56', '0', '0');
INSERT INTO `peggol` VALUES ('33', '197509122006041027', '08', '2005-01-01', '01/01/2005', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:57', '0', '0');
INSERT INTO `peggol` VALUES ('34', '195710011985031020', '10', '2006-01-04', '01/04/2006', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:58', '0', '0');
INSERT INTO `peggol` VALUES ('35', '196307051985121004', '08', '1985-01-01', '01/01/1985', '', '1', '', '', '', '', '1', '', '', '', '2016-03-26 19:35:59', '0', '0');
INSERT INTO `peggol` VALUES ('36', '196705281992021002', '10', '1985-01-01', '01/01/1985', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('37', '196802021994122003', '10', '1992-01-01', '01/01/1992', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('38', '196607011995122001', '10', '1994-01-01', '01/01/1994', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('39', '197707262007101001', '07', '1995-01-01', '01/01/1995', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('40', '197306192007101001', '07', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('41', '197707242007102004', '07', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('42', '197903282007101001', '07', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('43', '197807152007101004', '07', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('44', '197303062007102001', '07', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('45', '196412082007011018', '07', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('46', '198606032009122001', '07', '2007-01-01', '01/01/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('47', '197207071996032002', '10', '2010-07-01', '07/01/2010', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('48', '197207022007011019', '07', '0000-00-00', '27/08/2012', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('49', '131135720001310001', '', '0000-00-00', '27/08/2012', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('50', '131135720001070002', '', '0000-00-00', '30/06/1992', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('51', '131135720001190003', '', '2003-07-01', '07/01/2003', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('52', '131135720001080004', '', '2003-07-01', '07/01/2003', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('53', '131135720001070005', '', '2004-09-01', '09/01/2004', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('54', '131135720001280006', '', '2005-07-01', '07/01/2005', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('55', '131135720001270007', '', '2006-07-01', '07/01/2006', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('56', '131135720001310008', '', '2010-01-11', '01/11/2010', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('57', '131135720001180009', '', '2008-07-01', '07/01/2008', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('58', '131135720001090010', '', '2008-07-01', '07/01/2008', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('59', '131135720001070011', '', '2008-07-01', '07/01/2008', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('60', '131135720001330012', '', '2008-07-01', '07/01/2008', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('61', '131135720001110013', '', '2008-07-08', '07/08/2008', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('62', '131135720001280014', '', '2008-07-01', '07/01/2008', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('63', '131135720001270015', '', '0000-00-00', '13/03/2009', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('64', '131135720001240016', '', '2009-07-01', '07/01/2009', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('65', '131135720001240017', '', '2009-07-01', '07/01/2009', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('66', '131135720001240018', '', '2009-07-01', '07/01/2009', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('67', '131135720001110019', '', '2011-07-01', '07/01/2011', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('68', '196304101986031001', '08', '2013-01-07', '01/07/2013', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('69', '196012081987032001', '08', '2014-01-01', '01/01/2014', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('70', '197903202007101002', '08', '1987-03-01', '03/01/1987', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('71', '131135720001330020', '', '2007-01-10', '01/10/2007', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('72', '131135720001330021', '', '0000-00-00', '17/07/1991', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('73', '131135720001330022', '', '0000-00-00', '17/07/1999', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('74', '131135720001330023', '', '2002-01-01', '01/01/2002', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('75', '131135720001330024', '', '0000-00-00', '17/07/2002', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('76', '131135720001330025', '', '0000-00-00', '18/07/2005', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('77', '131135720001330026', '', '0000-00-00', '18/07/2005', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('78', '131135720001330027', '', '2009-11-02', '11/02/2009', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('79', '131135720001330028', '', '0000-00-00', '17/07/1985', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('80', '131135720001330029', '', '0000-00-00', '18/07/2005', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('81', '131135720001330030', '', '2014-01-01', '01/01/2014', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('82', '131135720001330031', '', '0000-00-00', '17/07/1980', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('83', '131135720001330032', '', '2009-07-01', '07/01/2009', '', '1', '', '', '', '', '1', '', '', '', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `peggol` VALUES ('84', '', '', '0000-00-00', '', '', '0', '', '', '', '', '0', '', '', '', '0000-00-00 00:00:00', '0', '0');

-- ----------------------------
-- Table structure for `pegjab`
-- ----------------------------
DROP TABLE IF EXISTS `pegjab`;
CREATE TABLE `pegjab` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `idjabatan` int(10) unsigned DEFAULT NULL,
  `tmt` date NOT NULL,
  `sk` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `jenis` varchar(50) NOT NULL,
  `namajab` varchar(255) DEFAULT NULL,
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `fk_pegjab_jabatan` (`idjabatan`),
  KEY `fk_pegjab_pegawai` (`nip`),
  KEY `IX_pegjab_ts` (`ts`,`issync`),
  CONSTRAINT `fk_pegjab_jabatan` FOREIGN KEY (`idjabatan`) REFERENCES `jabatan` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_pegjab_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegjab
-- ----------------------------

-- ----------------------------
-- Table structure for `pegkeluarga`
-- ----------------------------
DROP TABLE IF EXISTS `pegkeluarga`;
CREATE TABLE `pegkeluarga` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hubungan` varchar(50) DEFAULT NULL,
  `tgllahir` varchar(50) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pegkeluarga_pegawai` (`nip`),
  KEY `IX_pegkeluarga_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegkeluarga
-- ----------------------------
INSERT INTO `pegkeluarga` VALUES ('1', '41', 'Abida', '0', 'Anak', '13 Maret 2015', '', '', 'anak pertama', null, null, null, '2016-03-11 18:33:00', '0', '0');

-- ----------------------------
-- Table structure for `pegkerja`
-- ----------------------------
DROP TABLE IF EXISTS `pegkerja`;
CREATE TABLE `pegkerja` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `thnawal` varchar(4) NOT NULL,
  `thnakhir` varchar(4) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `fk_pegkerja_pegawai` (`nip`),
  KEY `IX_pegkerja_ts` (`ts`,`issync`),
  CONSTRAINT `fk_pegkerja_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegkerja
-- ----------------------------
INSERT INTO `pegkerja` VALUES ('1', '132 456 001', 'Pikiran Rakyan', '1996', '1999', 'Wartawan', '', '1', null, null, null, '2012-06-19 07:00:01', '52383', '0');
INSERT INTO `pegkerja` VALUES ('2', '131 924 825', 'SMA Negeri 3 Durian', '1992', '1995', 'Guru', '', '1', null, null, null, '2012-06-19 07:00:01', '43673', '0');

-- ----------------------------
-- Table structure for `peglastdata`
-- ----------------------------
DROP TABLE IF EXISTS `peglastdata`;
CREATE TABLE `peglastdata` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `idpeggol` int(10) unsigned DEFAULT NULL,
  `idpegjab` int(10) unsigned DEFAULT NULL,
  `idpegdiklat` int(10) unsigned DEFAULT NULL,
  `idpegsekolah` int(10) unsigned DEFAULT NULL,
  `idpeggaji` int(10) unsigned DEFAULT NULL,
  `idpegserti` int(10) unsigned DEFAULT NULL,
  `idpegkerja` int(10) unsigned DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peglastdata_pegawai` (`nip`),
  KEY `IX_peglastdata_ts` (`ts`,`issync`),
  CONSTRAINT `FK_peglastdata_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of peglastdata
-- ----------------------------

-- ----------------------------
-- Table structure for `pegsekolah`
-- ----------------------------
DROP TABLE IF EXISTS `pegsekolah`;
CREATE TABLE `pegsekolah` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tingkat` varchar(20) NOT NULL,
  `sekolah` varchar(255) NOT NULL,
  `lulus` int(10) unsigned NOT NULL,
  `sk` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pegsekolah_pegawai` (`nip`),
  KEY `IX_pegsekolah_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pegsekolah_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegsekolah
-- ----------------------------

-- ----------------------------
-- Table structure for `pegserti`
-- ----------------------------
DROP TABLE IF EXISTS `pegserti`;
CREATE TABLE `pegserti` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `sertifikat` varchar(255) NOT NULL,
  `lembaga` varchar(255) NOT NULL,
  `tahun` smallint(6) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `fk_pegserti_pegawai` (`nip`),
  KEY `IX_pegserti_ts` (`ts`,`issync`),
  CONSTRAINT `fk_pegserti_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegserti
-- ----------------------------

-- ----------------------------
-- Table structure for `satker`
-- ----------------------------
DROP TABLE IF EXISTS `satker`;
CREATE TABLE `satker` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `satker` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`satker`),
  UNIQUE KEY `UX_satker` (`replid`),
  KEY `IX_satker_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of satker
-- ----------------------------
INSERT INTO `satker` VALUES ('19', '(Tidak Ada)', 'Tidak Ada Satuan Kerja', '1', null, null, null, '2012-06-19 07:00:01', '17795', '0');
INSERT INTO `satker` VALUES ('21', 'GURU', 'Pengajar', '0', null, null, null, '2012-06-19 07:00:01', '51312', '0');
INSERT INTO `satker` VALUES ('23', 'KOPERASI', 'Koperasi', '0', null, null, null, '2012-06-19 07:00:01', '6583', '0');
INSERT INTO `satker` VALUES ('24', 'MANAJEMEN', 'Manajemen Sekolah', '0', null, null, null, '2012-06-19 07:00:01', '10040', '0');
INSERT INTO `satker` VALUES ('22', 'TATA USAHA', 'Tata Usaha Sekolah', '0', null, null, null, '2012-06-19 07:00:01', '30450', '0');
INSERT INTO `satker` VALUES ('20', 'YAYASAN', 'Pengurus Yayasan', '0', null, null, null, '2012-06-19 07:00:01', '56600', '0');

-- ----------------------------
-- Table structure for `suku`
-- ----------------------------
DROP TABLE IF EXISTS `suku`;
CREATE TABLE `suku` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `suku` varchar(20) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`suku`),
  UNIQUE KEY `UX_suku` (`replid`),
  KEY `IX_suku_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of suku
-- ----------------------------
INSERT INTO `suku` VALUES ('38', 'Belum Ada Data', '0', null, null, null, '2013-02-13 18:01:32', '37365', '0');
INSERT INTO `suku` VALUES ('32', 'Jawa', '0', null, null, null, '2010-03-02 10:07:22', '36536', '0');
INSERT INTO `suku` VALUES ('34', 'Minang', '0', null, null, null, '2012-07-19 14:27:05', '23157', '0');
INSERT INTO `suku` VALUES ('33', 'Sunda', '0', null, null, null, '2010-03-02 10:07:22', '6177', '0');

-- ----------------------------
-- Table structure for `trans`
-- ----------------------------
DROP TABLE IF EXISTS `trans`;
CREATE TABLE `trans` (
  `id` int(1) NOT NULL,
  `trans` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trans
-- ----------------------------
INSERT INTO `trans` VALUES ('1', 'Jalan Kaki');
INSERT INTO `trans` VALUES ('2', 'Sepeda');
INSERT INTO `trans` VALUES ('3', 'Sepeda Motor');
INSERT INTO `trans` VALUES ('4', 'Mobil Pribadi');
INSERT INTO `trans` VALUES ('5', 'Kendaraan Antar Jemput Sekolah');
INSERT INTO `trans` VALUES ('6', 'Angkutan Umum');
INSERT INTO `trans` VALUES ('7', 'Perahu/Sampan');
INSERT INTO `trans` VALUES ('8', 'Lainnya');
