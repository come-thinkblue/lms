/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : 0_man_dbperpustakaan

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-04-27 20:34:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `aktivitas`
-- ----------------------------
DROP TABLE IF EXISTS `aktivitas`;
CREATE TABLE `aktivitas` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `aktivitas` text,
  `perpustakaan` int(10) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_aktivitas_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aktivitas
-- ----------------------------

-- ----------------------------
-- Table structure for `anggota`
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noregistrasi` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL DEFAULT '',
  `alamat` varchar(255) NOT NULL,
  `kodepos` varchar(6) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telpon` varchar(100) DEFAULT NULL,
  `HP` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `institusi` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `tgldaftar` date NOT NULL DEFAULT '0000-00-00',
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `foto` blob,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `Index_nopen` (`noregistrasi`),
  KEY `IX_anggota_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 2020352 kB; InnoDB free: 2020352 kB; (`NoRegist';

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('1', 'ANG20130223180151', 'Ahsanun Naseh Khudori', 'Rejosari', '66281', 'ahsanunnaseh@yahoo.com', '085649847221', '', '', '', '', '2013-02-23', '1', null, null, null, null, '2013-02-23 18:02:21', '0', '0');

-- ----------------------------
-- Table structure for `daftarpustaka`
-- ----------------------------
DROP TABLE IF EXISTS `daftarpustaka`;
CREATE TABLE `daftarpustaka` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pustaka` int(10) unsigned NOT NULL,
  `perpustakaan` int(10) unsigned NOT NULL,
  `kodepustaka` varchar(45) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0 dipinjam, 1 tersedia',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `UX_daftarpustaka` (`kodepustaka`),
  KEY `FK_daftarpustaka_perpus` (`perpustakaan`),
  KEY `FK_daftarpustaka_pustaka` (`pustaka`),
  KEY `IX_daftarpustaka_ts` (`ts`,`issync`),
  CONSTRAINT `FK_daftarpustaka_perpus` FOREIGN KEY (`perpustakaan`) REFERENCES `perpustakaan` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_daftarpustaka_pustaka` FOREIGN KEY (`pustaka`) REFERENCES `pustaka` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of daftarpustaka
-- ----------------------------
INSERT INTO `daftarpustaka` VALUES ('1', '1', '1', '050/001/T/00001/BU', '0', null, null, null, '2013-02-23 23:23:59', '63167', '0');
INSERT INTO `daftarpustaka` VALUES ('2', '1', '1', '050/001/T/00002/BU', '0', null, null, null, '2013-02-24 10:54:59', '34713', '0');
INSERT INTO `daftarpustaka` VALUES ('3', '1', '1', '050/001/T/00003/BU', '0', null, null, null, '2013-03-13 11:08:07', '49594', '0');
INSERT INTO `daftarpustaka` VALUES ('4', '1', '1', '050/001/T/00004/BU', '0', null, null, null, '2013-01-01 12:18:40', '12771', '0');
INSERT INTO `daftarpustaka` VALUES ('5', '1', '1', '050/001/T/00005/BU', '1', null, null, null, '2013-02-23 23:21:57', '46130', '0');
INSERT INTO `daftarpustaka` VALUES ('6', '1', '1', '050/001/T/00006/BU', '1', null, null, null, '2013-02-23 23:21:57', '61280', '0');
INSERT INTO `daftarpustaka` VALUES ('7', '1', '1', '050/001/T/00007/BU', '1', null, null, null, '2013-02-23 23:21:57', '36948', '0');
INSERT INTO `daftarpustaka` VALUES ('8', '1', '1', '050/001/T/00008/BU', '1', null, null, null, '2013-02-23 23:21:57', '900', '0');
INSERT INTO `daftarpustaka` VALUES ('9', '1', '1', '050/001/T/00009/BU', '1', null, null, null, '2013-02-23 23:21:57', '24714', '0');
INSERT INTO `daftarpustaka` VALUES ('10', '1', '1', '050/001/T/00010/BU', '1', null, null, null, '2013-02-23 23:21:57', '55340', '0');

-- ----------------------------
-- Table structure for `denda`
-- ----------------------------
DROP TABLE IF EXISTS `denda`;
CREATE TABLE `denda` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpinjam` int(10) unsigned NOT NULL,
  `denda` int(10) DEFAULT '0',
  `telat` int(10) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_denda_idpeminjaman` (`idpinjam`),
  KEY `IX_denda_ts` (`ts`,`issync`),
  CONSTRAINT `FK_denda_idpeminjaman` FOREIGN KEY (`idpinjam`) REFERENCES `pinjam` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of denda
-- ----------------------------

-- ----------------------------
-- Table structure for `format`
-- ----------------------------
DROP TABLE IF EXISTS `format`;
CREATE TABLE `format` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(3) NOT NULL DEFAULT '',
  `nama` varchar(100) NOT NULL DEFAULT '',
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `Kode_U` (`kode`),
  KEY `IX_format_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of format
-- ----------------------------
INSERT INTO `format` VALUES ('1', 'BU', 'Buku', '', null, null, null, '2010-03-08 08:40:53', '0', '0');

-- ----------------------------
-- Table structure for `katalog`
-- ----------------------------
DROP TABLE IF EXISTS `katalog`;
CREATE TABLE `katalog` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(15) NOT NULL,
  `nama` varchar(100) NOT NULL DEFAULT '',
  `rak` int(10) unsigned NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `counter` int(10) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `REPL_IN` (`kode`),
  KEY `FK_katalog_rak` (`rak`),
  KEY `IX_katalog_ts` (`ts`,`issync`),
  CONSTRAINT `FK_katalog_rak` FOREIGN KEY (`rak`) REFERENCES `rak` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of katalog
-- ----------------------------
INSERT INTO `katalog` VALUES ('1', '000', 'KARYA UMUM', '1', 'Klasifikasi pustaka dalam kelompok karya umum ', '0', null, null, null, '2010-03-07 17:28:08', '48886', '0');
INSERT INTO `katalog` VALUES ('2', '010', 'Bibliografi', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '34247', '0');
INSERT INTO `katalog` VALUES ('3', '020', 'Perpustakaan dan Informasi Sains', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '24576', '0');
INSERT INTO `katalog` VALUES ('4', '030', 'Ensiklopedia dan Buku Pintar', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '20137', '0');
INSERT INTO `katalog` VALUES ('5', '040', '(   ...   )', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '26959', '0');
INSERT INTO `katalog` VALUES ('6', '050', 'Majalah, Jurnal dan Terbitan Berkala', '1', '', '10', null, null, null, '2013-02-23 23:21:57', '8852', '0');
INSERT INTO `katalog` VALUES ('7', '060', 'Asosiasi, Organisasi dan Museum', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '28911', '0');
INSERT INTO `katalog` VALUES ('8', '070', 'Media Berita, Jurnalisme dan Publikasi', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '52469', '0');
INSERT INTO `katalog` VALUES ('9', '080', 'Kutipan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '44550', '0');
INSERT INTO `katalog` VALUES ('10', '090', 'manuskrip dan buku-buku langka', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '65344', '0');
INSERT INTO `katalog` VALUES ('11', '100', 'FILSAFAT', '1', 'Klasifikasi pustaka yang tergolong dalam ilmu filsafat', '0', null, null, null, '2010-03-07 17:28:08', '62009', '0');
INSERT INTO `katalog` VALUES ('12', '110', 'Metafisika', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '48483', '0');
INSERT INTO `katalog` VALUES ('13', '120', 'Epistemologi', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '56389', '0');
INSERT INTO `katalog` VALUES ('14', '130', 'Parapsikologi dan Perdukunan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '5433', '0');
INSERT INTO `katalog` VALUES ('15', '140', 'Filsafat Aliran Pemikiran', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '54588', '0');
INSERT INTO `katalog` VALUES ('16', '150', 'Psikologi', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '60049', '0');
INSERT INTO `katalog` VALUES ('17', '160', 'Logika', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '5421', '0');
INSERT INTO `katalog` VALUES ('18', '170', 'Etika', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '43547', '0');
INSERT INTO `katalog` VALUES ('19', '180', 'Filsafat kuno, pertengahan dan lanjutan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '4883', '0');
INSERT INTO `katalog` VALUES ('20', '190', 'Filsafat Barat Modern', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '24833', '0');
INSERT INTO `katalog` VALUES ('21', '200', 'AGAMA', '1', 'Klasifikasi pustaka yang berkaitan dengan keagamaan', '0', null, null, null, '2010-03-07 17:28:08', '43986', '0');
INSERT INTO `katalog` VALUES ('22', '210', 'Filsafat dan Teori Agama', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '14372', '0');
INSERT INTO `katalog` VALUES ('23', '220', 'Injil', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '5431', '0');
INSERT INTO `katalog` VALUES ('24', '300', 'ILMU SOSIAL, SOSIOLOGI dan ANTROPOLOGI', '1', 'Pengelompokan Pustaka dalam kategori ilmu sosial', '0', null, null, null, '2010-03-07 17:28:08', '49567', '0');
INSERT INTO `katalog` VALUES ('25', '310', 'Statistika Umum', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '34952', '0');
INSERT INTO `katalog` VALUES ('26', '320', 'Ilmu Politik', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '26057', '0');
INSERT INTO `katalog` VALUES ('27', '330', 'Ekonomi', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '25429', '0');
INSERT INTO `katalog` VALUES ('28', '340', 'Hukum', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '48975', '0');
INSERT INTO `katalog` VALUES ('29', '350', 'Administrasi Umum dan Ilmu Militer', '1', '', '2', null, null, null, '2010-03-08 08:44:43', '37528', '0');
INSERT INTO `katalog` VALUES ('30', '360', 'Permasalahan Sosial dan Pelayanan Sosial', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '40716', '0');
INSERT INTO `katalog` VALUES ('31', '370', 'Pendidikan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '25463', '0');
INSERT INTO `katalog` VALUES ('32', '380', 'Perdagangan, Komunikasi dan Perhubungan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '5165', '0');
INSERT INTO `katalog` VALUES ('33', '390', 'Kewarganegaraan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '14967', '0');
INSERT INTO `katalog` VALUES ('34', '400', 'BAHASA', '1', 'Klasifikasi pustaka dalam kelompok bahasa', '0', null, null, null, '2010-03-07 17:28:08', '59340', '0');
INSERT INTO `katalog` VALUES ('35', '410', 'Linguistik', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '55207', '0');
INSERT INTO `katalog` VALUES ('36', '420', 'BAHASA INGGRIS', '1', 'pengelompokan pustaka (bahasa) dalam kelompok bahasa inggris', '0', null, null, null, '2010-03-07 17:28:08', '32485', '0');
INSERT INTO `katalog` VALUES ('37', '430', 'Bahasa Arab', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '62332', '0');
INSERT INTO `katalog` VALUES ('38', '500', 'ILMU MURNI', '1', 'klasifikasi pustaka dalam kelompok ilmu murni', '0', null, null, null, '2010-03-07 17:28:08', '17616', '0');
INSERT INTO `katalog` VALUES ('39', '510', 'Matematika', '1', 'pengelompokan pustaka (ilmu murni) dalam kelompok matematika', '0', null, null, null, '2010-03-07 17:28:08', '32144', '0');
INSERT INTO `katalog` VALUES ('40', '530', 'Fisika', '1', 'pengelompokan pustaka ilmu murni berdasarkan klas fisika ', '0', null, null, null, '2010-03-07 17:28:08', '42340', '0');
INSERT INTO `katalog` VALUES ('41', '540', 'Kimia', '1', 'klasifikasi pustaka ilmu murni dalam kelompok ilmu kimia', '0', null, null, null, '2010-03-07 17:28:08', '49740', '0');
INSERT INTO `katalog` VALUES ('42', '570', 'Biologi', '1', 'pengelompokan pustaka ilmu murni kedalam klas biologi', '0', null, null, null, '2010-03-07 17:28:08', '56148', '0');
INSERT INTO `katalog` VALUES ('43', '600', 'ILMU TERAPAN', '1', 'pengelompokan pustaka kedalam kategori ilmu terapan', '0', null, null, null, '2010-03-07 17:28:08', '458', '0');
INSERT INTO `katalog` VALUES ('44', '700', 'KESENIAN', '1', 'Pengelompokan pustaka kedalam kategori kesenian', '0', null, null, null, '2010-03-07 17:28:08', '30437', '0');
INSERT INTO `katalog` VALUES ('45', '710', 'Kreasi dan Ketrampilan', '1', '', '0', null, null, null, '2010-03-07 17:28:08', '19752', '0');
INSERT INTO `katalog` VALUES ('46', '800', 'KESUSASTERAAN', '1', 'klasifikasi pustaka berdasarkan kelompok sastra', '0', null, null, null, '2010-03-07 17:28:08', '7445', '0');
INSERT INTO `katalog` VALUES ('47', '900', 'GEOGRAFI dan SEJARAH ', '1', 'klasifikasi pustaka kedalam kelompok ilmu geografi dan sejarah', '0', null, null, null, '2010-03-07 17:28:08', '43502', '0');

-- ----------------------------
-- Table structure for `konfigurasi`
-- ----------------------------
DROP TABLE IF EXISTS `konfigurasi`;
CREATE TABLE `konfigurasi` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siswa` int(10) unsigned DEFAULT NULL,
  `pegawai` int(10) unsigned DEFAULT NULL,
  `other` int(10) unsigned DEFAULT NULL,
  `denda` int(10) unsigned DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_konfigurasi_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfigurasi
-- ----------------------------
INSERT INTO `konfigurasi` VALUES ('1', '3', '4', '0', '0', null, null, null, '2010-03-25 13:24:56', '0', '0');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `idmenu` varchar(255) DEFAULT NULL,
  `idParent` varchar(255) DEFAULT NULL,
  `idUrut` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `isVisible` varchar(255) DEFAULT NULL,
  `child` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Master', '1', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('2', 'Perpustakaan', '1', '1', '2', 'http://localhost/jibas/simtaka/ref/pustaka.php', null, '1', '0');
INSERT INTO `menu` VALUES ('3', 'Format', '1', '1', '3', 'http://localhost/jibas/simtaka/ref/format.php', null, '1', '0');
INSERT INTO `menu` VALUES ('4', 'Rak', '1', '1', '4', 'http://localhost/jibas/simtaka/ref/rak.php', null, '1', '0');
INSERT INTO `menu` VALUES ('5', 'Katalog', '1', '1', '5', 'http://localhost/jibas/simtaka/ref/katalog.php', null, '1', '0');
INSERT INTO `menu` VALUES ('6', 'Penerbit', '1', '1', '6', 'http://localhost/jibas/simtaka/ref/penerbit.php', null, '1', '0');
INSERT INTO `menu` VALUES ('7', 'Penulis', '1', '1', '7', 'http://localhost/jibas/simtaka/ref/penulis.php', null, '1', '0');
INSERT INTO `menu` VALUES ('8', 'Master Pustaka', '2', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('9', 'Cari Pustaka', '2', '1', '2', 'http://localhost/jibas/simtaka/pus/pustaka.cari.php', null, '1', '0');
INSERT INTO `menu` VALUES ('10', 'Daftar Pustaka', '2', '1', '3', 'http://localhost/jibas/simtaka/pus/pustaka.daftar.php', null, '1', '0');
INSERT INTO `menu` VALUES ('11', 'Pustaka Baru', '2', '1', '4', 'http://localhost/jibas/simtaka/pus/pustaka.baru.php', null, '1', '0');
INSERT INTO `menu` VALUES ('12', 'Peminjaman', '3', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('13', 'Peminjaman', '3', '1', '2', 'http://localhost/jibas/simtaka/pjm/pinjam.php', null, '1', '0');
INSERT INTO `menu` VALUES ('14', 'Daftar Peminjaman', '3', '1', '3', 'http://localhost/jibas/simtaka/pjm/daftar.pinjam.php', null, '1', '0');
INSERT INTO `menu` VALUES ('15', 'peminjaman Terlambat', '3', '1', '4', 'http://localhost/jibas/simtaka/pjm/daftar.pinjam.telat.php', null, '1', '0');
INSERT INTO `menu` VALUES ('16', 'Pengembalian', '4', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('17', 'Pengembalian', '4', '1', '2', 'http://localhost/jibas/simtaka/kbl/kembali.php', null, '1', '0');
INSERT INTO `menu` VALUES ('18', 'Daftar Pengembalian', '4', '1', '3', 'http://localhost/jibas/simtaka/kbl/daftar.kembali.php', null, '1', '0');
INSERT INTO `menu` VALUES ('19', 'Daftar Penerimaan Denda', '4', '1', '4', 'http://localhost/jibas/simtaka/kbl/daftar.denda.php', null, '1', '0');
INSERT INTO `menu` VALUES ('20', 'Statistik', '5', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('21', 'Statistik Peminjam', '5', '1', '2', 'http://localhost/jibas/simtaka/pus/stat.pinjam.php', null, '1', '0');
INSERT INTO `menu` VALUES ('22', 'Statistik Pustaka', '5', '1', '3', 'http://localhost/jibas/simtaka/pus/stat.pustaka.php', null, '1', '0');
INSERT INTO `menu` VALUES ('23', 'Statistik Peminjam Pustaka', '5', '1', '4', 'http://localhost/jibas/simtaka/pus/stat.all.php', null, '1', '0');
INSERT INTO `menu` VALUES ('24', 'User', '6', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('25', 'Daftar Anggota', '6', '1', '2', 'http://localhost/jibas/simtaka/atr/anggota.php', null, '1', '0');
INSERT INTO `menu` VALUES ('26', 'Daftar Pengguna', '6', '1', '3', 'http://localhost/jibas/simtaka/atr/pengguna.php', null, '1', '0');
INSERT INTO `menu` VALUES ('27', 'Ganti Password', '6', '1', '4', 'http://localhost/jibas/simtaka/atr/user_ganti.php', null, '1', '0');
INSERT INTO `menu` VALUES ('28', 'Setting', '7', '0', '1', null, null, '1', '1');
INSERT INTO `menu` VALUES ('29', 'Konfigurasi', '7', '1', '2', 'http://localhost/jibas/simtaka/atr/konfigurasi.php', null, '1', '0');
INSERT INTO `menu` VALUES ('30', 'Header', '7', '1', '3', 'http://localhost/jibas/simtaka/atr/Header.php', null, '1', '0');

-- ----------------------------
-- Table structure for `penerbit`
-- ----------------------------
DROP TABLE IF EXISTS `penerbit`;
CREATE TABLE `penerbit` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(3) NOT NULL DEFAULT '',
  `nama` varchar(100) NOT NULL DEFAULT '',
  `alamat` varchar(255) DEFAULT NULL,
  `telpon` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `kontak` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `REPL_ID` (`kode`),
  KEY `IX_penerbit_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of penerbit
-- ----------------------------
INSERT INTO `penerbit` VALUES ('1', '001', 'Ediide Press', 'Landungsari Malang', '0341-335221', 'ahsanunnaseh@yahoo.com', '0341-335221', 'www.ediide.com', '', '', null, null, null, '2013-02-23 23:19:34', '44006', '0');

-- ----------------------------
-- Table structure for `penulis`
-- ----------------------------
DROP TABLE IF EXISTS `penulis`;
CREATE TABLE `penulis` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(3) NOT NULL DEFAULT '',
  `nama` varchar(100) NOT NULL DEFAULT '',
  `kontak` varchar(255) DEFAULT NULL,
  `biografi` text,
  `keterangan` varchar(255) DEFAULT NULL,
  `gelardepan` varchar(45) DEFAULT NULL,
  `gelarbelakang` varchar(45) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `REPL_ID` (`kode`),
  KEY `IX_penulis_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of penulis
-- ----------------------------
INSERT INTO `penulis` VALUES ('1', '001', 'Ahsanun Naseh Khudori', '085 649 847 2221', '', '', '', 'S.Kom', null, null, null, '2013-02-23 23:20:17', '13902', '0');

-- ----------------------------
-- Table structure for `perpustakaan`
-- ----------------------------
DROP TABLE IF EXISTS `perpustakaan`;
CREATE TABLE `perpustakaan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL DEFAULT '',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `Nama` (`nama`),
  KEY `IX_perpustakaan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of perpustakaan
-- ----------------------------
INSERT INTO `perpustakaan` VALUES ('1', 'SMA Negeri 1 Malang', '', null, null, null, '2010-03-08 08:40:44', '0', '0');

-- ----------------------------
-- Table structure for `pinjam`
-- ----------------------------
DROP TABLE IF EXISTS `pinjam`;
CREATE TABLE `pinjam` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kodepustaka` varchar(45) NOT NULL,
  `tglpinjam` date NOT NULL DEFAULT '0000-00-00',
  `tglkembali` date NOT NULL DEFAULT '0000-00-00',
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `idanggota` varchar(45) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 belum di acc, 1 udah di acc, 2 udah dikembalikan',
  `tglditerima` date NOT NULL DEFAULT '0000-00-00',
  `petugaspinjam` varchar(50) DEFAULT NULL,
  `petugaskembali` varchar(50) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`,`tglkembali`),
  KEY `IX_pinjam_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of pinjam
-- ----------------------------
INSERT INTO `pinjam` VALUES ('1', '050/001/T/00001/BU', '2013-02-23', '2013-02-28', 'okey', '12345678', '1', '0000-00-00', null, null, 'siswa', null, null, '2013-02-23 23:23:59', '24150', '0');
INSERT INTO `pinjam` VALUES ('2', '050/001/T/00002/BU', '2013-02-23', '2013-02-11', '', '12345678', '2', '2013-02-23', null, null, 'siswa', null, null, '2013-02-23 23:28:08', '55490', '0');
INSERT INTO `pinjam` VALUES ('3', '050/001/T/00002/BU', '2013-02-24', '2013-02-24', '', '15180', '1', '0000-00-00', null, null, 'siswa', null, null, '2013-02-24 10:54:59', '18166', '0');
INSERT INTO `pinjam` VALUES ('4', '050/001/T/00003/BU', '2013-03-13', '2013-03-18', '', '12345678', '1', '0000-00-00', null, null, 'siswa', null, null, '2013-03-13 11:08:07', '185', '0');
INSERT INTO `pinjam` VALUES ('5', '050/001/T/00004/BU', '2013-01-01', '2013-01-01', '', '15810', '1', '0000-00-00', null, null, 'siswa', null, null, '2013-01-01 12:18:40', '64768', '0');

-- ----------------------------
-- Table structure for `pustaka`
-- ----------------------------
DROP TABLE IF EXISTS `pustaka`;
CREATE TABLE `pustaka` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL DEFAULT '',
  `abstraksi` text NOT NULL,
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `tahun` int(4) unsigned NOT NULL DEFAULT '0',
  `keteranganfisik` varchar(255) NOT NULL DEFAULT '',
  `penulis` int(10) unsigned NOT NULL,
  `penerbit` int(10) unsigned NOT NULL,
  `format` int(10) unsigned NOT NULL,
  `katalog` int(10) unsigned NOT NULL,
  `cover` blob,
  `keterangan` varchar(255) DEFAULT NULL,
  `harga` int(10) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pustaka_penulis` (`penulis`),
  KEY `FK_pustaka_format` (`format`),
  KEY `FK_pustaka_katalog` (`katalog`),
  KEY `FK_pustaka_penerbit` (`penerbit`),
  KEY `IX_pustaka_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pustaka_format` FOREIGN KEY (`format`) REFERENCES `format` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pustaka_katalog` FOREIGN KEY (`katalog`) REFERENCES `katalog` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pustaka_penerbit` FOREIGN KEY (`penerbit`) REFERENCES `penerbit` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pustaka_penulis` FOREIGN KEY (`penulis`) REFERENCES `penulis` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of pustaka
-- ----------------------------
INSERT INTO `pustaka` VALUES ('1', 'Tutorial LMS SMAN 1 Malang', '', 'LMS Ediide', '2012', 'Masih bagus', '1', '1', '1', '6', null, '', '55000', null, null, null, '2013-02-23 23:21:57', '6334', '0');

-- ----------------------------
-- Table structure for `rak`
-- ----------------------------
DROP TABLE IF EXISTS `rak`;
CREATE TABLE `rak` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rak` varchar(25) NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `Kode_U` (`rak`),
  KEY `IX_rak_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rak
-- ----------------------------
INSERT INTO `rak` VALUES ('1', 'Rak', 'contoh data', null, null, null, '2010-03-07 17:25:32', '0', '0');
