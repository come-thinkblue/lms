/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : 0_man_simaset

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-04-27 20:34:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------

-- ----------------------------
-- Table structure for `r_kodebrg1`
-- ----------------------------
DROP TABLE IF EXISTS `r_kodebrg1`;
CREATE TABLE `r_kodebrg1` (
  `kode1` varchar(10) NOT NULL,
  `nama` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_kodebrg1
-- ----------------------------
INSERT INTO `r_kodebrg1` VALUES ('PLN', 'Peralatan Laboratorium Nautika Kapal Niaga');
INSERT INTO `r_kodebrg1` VALUES ('PLT', 'Peralatan Laboratorium Teknika Kapal Niaga');
INSERT INTO `r_kodebrg1` VALUES ('APP', 'Alat Pendukung Pembelajaran');
INSERT INTO `r_kodebrg1` VALUES ('APE', 'Alat Penunjang Pendidikan');
INSERT INTO `r_kodebrg1` VALUES ('APR', 'Alat Praktek');

-- ----------------------------
-- Table structure for `r_kodebrg2`
-- ----------------------------
DROP TABLE IF EXISTS `r_kodebrg2`;
CREATE TABLE `r_kodebrg2` (
  `kode1` varchar(10) NOT NULL,
  `kode2` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_kodebrg2
-- ----------------------------
INSERT INTO `r_kodebrg2` VALUES ('APE', 'NAU', 'Jurusan Nautika');
INSERT INTO `r_kodebrg2` VALUES ('APE', 'TEK', 'Jurusan Teknika');
INSERT INTO `r_kodebrg2` VALUES ('APP', 'NAU', 'Jurusan Nautika');
INSERT INTO `r_kodebrg2` VALUES ('APP', 'TEK', 'Jurusan Teknika');
INSERT INTO `r_kodebrg2` VALUES ('APR', 'IPA', 'Alat Praktek IPA');
INSERT INTO `r_kodebrg2` VALUES ('APR', 'IPS', 'Alat Praktek IPS');
INSERT INTO `r_kodebrg2` VALUES ('APR', 'AGM', 'Alat Praktek Agama');
INSERT INTO `r_kodebrg2` VALUES ('APR', 'NAV', 'Alat Praktek Navigasi');

-- ----------------------------
-- Table structure for `r_ruang`
-- ----------------------------
DROP TABLE IF EXISTS `r_ruang`;
CREATE TABLE `r_ruang` (
  `id_ruang` int(3) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(150) NOT NULL,
  `kondisi` int(3) NOT NULL,
  `luas` float NOT NULL,
  `tgg_jwb` varchar(100) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_ruang
-- ----------------------------
INSERT INTO `r_ruang` VALUES ('1', 'Ruang Kepala Sekolah', '1', '16', 'Drs. Sugiatno');
INSERT INTO `r_ruang` VALUES ('2', 'Ruang Kantin', '1', '16', 'Drs. Sugiatno');
INSERT INTO `r_ruang` VALUES ('3', 'Ruang Laboratorium', '1', '56', 'Drs. Sugiatno');
INSERT INTO `r_ruang` VALUES ('4', 'Ruang Perpustakaan', '1', '30', 'Drs. Kuswadi');

-- ----------------------------
-- Table structure for `t_barang`
-- ----------------------------
DROP TABLE IF EXISTS `t_barang`;
CREATE TABLE `t_barang` (
  `id_barang` int(5) NOT NULL AUTO_INCREMENT,
  `kode_brg1` varchar(10) NOT NULL,
  `kode_brg2` varchar(10) NOT NULL,
  `kode_brg3` varchar(10) NOT NULL,
  `kode_brg4` varchar(4) NOT NULL,
  `kode_gbg` varchar(20) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `merk` varchar(200) NOT NULL,
  `nilai_aset` float NOT NULL,
  `letak` int(2) NOT NULL,
  `kondisi` int(1) NOT NULL,
  `asal_perolehan` varchar(100) NOT NULL,
  `thn_dapat` varchar(4) NOT NULL,
  `tgl_buku` datetime NOT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_barang
-- ----------------------------
INSERT INTO `t_barang` VALUES ('1', 'APR', 'IPA', '1', '1', 'APRIPA11', 'Gelas Ukur', 'Hitachi', '10000', '1', '1', 'P', '2012', '2012-06-11 19:55:28');
INSERT INTO `t_barang` VALUES ('2', 'APR', 'IPA', '2', '1', 'APRIPA21', 'Bejana Ukur', 'Glass', '20000', '1', '2', 'Pembelian', '2012', '2012-06-11 19:56:07');
INSERT INTO `t_barang` VALUES ('3', 'APR', 'IPA', '2', '2', 'APRIPA22', 'Bejana Ukur', 'Glass', '20000', '1', '2', 'Pembelian', '2012', '2012-06-11 19:56:07');
INSERT INTO `t_barang` VALUES ('4', 'APR', 'IPS', '1', '1', 'APRIPS11', 'Globe', 'SInar Dunia', '30000', '1', '1', 'Pembelian', '2012', '2012-06-11 19:58:58');
INSERT INTO `t_barang` VALUES ('5', 'APR', 'IPS', '1', '2', 'APRIPS12', 'Globe', 'SInar Dunia', '30000', '1', '1', 'Pembelian', '2012', '2012-06-11 19:58:58');
INSERT INTO `t_barang` VALUES ('6', 'APR', 'IPA', '3', '1', 'APRIPA31', 'Termometer Celcius', 'Fahrenheit', '90000', '1', '1', 'Pembelian', '2012', '2012-06-11 20:01:25');
INSERT INTO `t_barang` VALUES ('7', 'APR', 'IPA', '3', '2', 'APRIPA32', 'Termometer Celcius', 'Fahrenheit', '90000', '1', '1', 'Pembelian', '2012', '2012-06-11 20:01:26');
INSERT INTO `t_barang` VALUES ('8', 'APR', 'IPA', '3', '3', 'APRIPA33', 'Termometer Celcius', 'Fahrenheit', '90000', '1', '1', 'Pembelian', '2012', '2012-06-11 20:01:26');
INSERT INTO `t_barang` VALUES ('9', 'APR', 'IPA', '3', '4', 'APRIPA34', 'Termometer Celcius', 'Fahrenheit', '90000', '1', '1', 'Pembelian', '2012', '2012-06-11 20:01:26');
INSERT INTO `t_barang` VALUES ('10', 'APR', 'IPA', '3', '5', 'APRIPA35', 'Termometer Celcius', 'Fahrenheit', '90000', '1', '1', 'Pembelian', '2012', '2012-06-11 20:01:26');
INSERT INTO `t_barang` VALUES ('11', 'APP', 'NAU', '1', '1', 'APPNAU11', 'Kapal Tankers', 'Germany', '100000', '1', '1', 'Pembelian', '2012', '2012-06-11 20:05:10');
INSERT INTO `t_barang` VALUES ('12', 'APR', 'IPA', '4', '1', 'APRIPA41', 'Solder Listrik', 'Sakura', '10000', '3', '1', 'Pembelian', '2012', '2012-06-12 18:29:48');
INSERT INTO `t_barang` VALUES ('13', 'APR', 'IPA', '4', '2', 'APRIPA42', 'Solder Listrik', 'Sakura', '10000', '3', '1', 'Pembelian', '2012', '2012-06-12 18:29:48');
INSERT INTO `t_barang` VALUES ('14', 'APR', 'IPA', '4', '3', 'APRIPA43', 'Solder Listrik', 'Sakura', '10000', '3', '1', 'Pembelian', '2012', '2012-06-12 18:29:48');
