-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2016 at 07:38 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE IF NOT EXISTS `agama` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agama` varchar(20) NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`agama`),
  UNIQUE KEY `UX_agama` (`replid`),
  KEY `IX_agama_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`replid`, `agama`, `urutan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(2, 'Budha', 2, NULL, NULL, NULL, '2013-04-11 11:03:51', 0, 0),
(1, 'Islam', 1, NULL, NULL, NULL, '2013-04-11 11:03:39', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `aktivitaskalender`
--

CREATE TABLE IF NOT EXISTS `aktivitaskalender` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkalender` int(10) unsigned NOT NULL DEFAULT '0',
  `tanggalawal` date NOT NULL DEFAULT '0000-00-00',
  `tanggalakhir` date NOT NULL DEFAULT '0000-00-00',
  `kegiatan` varchar(50) NOT NULL,
  `keterangan` text,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_aktivitaskalender_kalenderakademik` (`idkalender`),
  KEY `IX_aktivitaskalender_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `aktivitaskalender`
--


-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(45) NOT NULL,
  `klsakhir` int(10) unsigned NOT NULL,
  `tktakhir` int(10) unsigned NOT NULL,
  `tgllulus` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_alumni_siswa` (`nis`),
  KEY `FK_alumni_tingkat` (`tktakhir`),
  KEY `FK_alumni_kelas` (`klsakhir`),
  KEY `FK_alumni_departemen` (`departemen`),
  KEY `IX_alumni_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `alumni`
--


-- --------------------------------------------------------

--
-- Table structure for table `angkatan`
--

CREATE TABLE IF NOT EXISTS `angkatan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `angkatan` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_angkatan_departemen` (`departemen`),
  KEY `IX_angkatan_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `angkatan`
--

INSERT INTO `angkatan` (`replid`, `angkatan`, `departemen`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(20, '2015', 'SMAN 1 Malang', 1, '', NULL, NULL, NULL, '2016-01-22 11:08:55', 0, 0),
(21, '2014', 'SMAN 1 Malang', 1, '', NULL, NULL, NULL, '2016-01-22 11:09:02', 0, 0),
(22, '2013', 'SMAN 1 Malang', 1, '', NULL, NULL, NULL, '2016-01-22 11:09:08', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `asalsekolah`
--

CREATE TABLE IF NOT EXISTS `asalsekolah` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departemen` varchar(50) NOT NULL,
  `sekolah` varchar(100) NOT NULL,
  `urutan` tinyint(2) unsigned DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sekolah`),
  UNIQUE KEY `UX_asalsekolah` (`replid`),
  KEY `FK_asalsekolah_departemen` (`departemen`),
  KEY `IX_asalsekolah_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `asalsekolah`
--


-- --------------------------------------------------------

--
-- Table structure for table `aturangrading`
--

CREATE TABLE IF NOT EXISTS `aturangrading` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idtingkat` int(10) unsigned NOT NULL,
  `idkurikulum` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dasarpenilaian` varchar(50) NOT NULL,
  `nmin` decimal(6,1) NOT NULL,
  `nmax` decimal(6,1) NOT NULL,
  `grade` varchar(2) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_aturangrading_tingkat` (`idtingkat`),
  KEY `FK_aturangrading_dasarpenilaian` (`dasarpenilaian`),
  KEY `IX_aturangrading_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=135 ;

--
-- Dumping data for table `aturangrading`
--

INSERT INTO `aturangrading` (`replid`, `idtingkat`, `idkurikulum`, `dasarpenilaian`, `nmin`, `nmax`, `grade`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(21, 1, 1, 'AF', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(22, 1, 1, 'AF', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(23, 1, 1, 'AF', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(24, 1, 1, 'AF', '55.0', '64.0', 'D', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(25, 1, 1, 'KOG', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(26, 1, 1, 'KOG', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(27, 1, 1, 'KOG', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(31, 2, 1, 'AF', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(32, 2, 1, 'AF', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(33, 2, 1, 'AF', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(34, 2, 1, 'AF', '55.0', '64.0', 'D', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(35, 2, 1, 'KOG', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(36, 2, 1, 'KOG', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(37, 2, 1, 'KOG', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(38, 2, 1, 'PSI', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(39, 2, 1, 'PSI', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(40, 2, 1, 'PSI', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(54, 1, 1, 'PSI', '81.0', '90.0', 'A', NULL, NULL, NULL, '2016-04-03 23:53:00', 0, 0),
(55, 1, 1, 'PSI', '71.0', '80.0', 'B', NULL, NULL, NULL, '2016-04-03 23:53:00', 0, 0),
(61, 1, 1, 'PSI', '61.0', '70.0', 'C', NULL, NULL, NULL, '2016-04-04 00:53:41', 0, 0),
(62, 1, 1, 'PSI', '51.0', '60.0', 'D', NULL, NULL, NULL, '2016-04-04 00:53:41', 0, 0),
(63, 2, 2, 'KI1', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(64, 2, 2, 'KI1', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(65, 2, 2, 'KI1', '55.0', '64.0', 'D', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(66, 2, 2, 'KI1', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(67, 2, 2, 'KI2', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(68, 2, 2, 'KI2', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(69, 2, 2, 'KI2', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(70, 2, 2, 'KI3', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(71, 2, 2, 'KI3', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(72, 2, 2, 'KI3', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(73, 2, 2, 'KI4', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(74, 2, 2, 'KI4', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(75, 2, 2, 'KI4', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-05 07:58:17', 0, 0),
(103, 3, 2, 'KI1', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(104, 3, 2, 'KI1', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(105, 3, 2, 'KI1', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(106, 3, 2, 'KI1', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(107, 3, 2, 'KI2', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(108, 3, 2, 'KI2', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(109, 3, 2, 'KI2', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(110, 3, 2, 'KI2', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(111, 3, 2, 'KI3', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(112, 3, 2, 'KI3', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(113, 3, 2, 'KI3', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(114, 3, 2, 'KI3', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(115, 3, 2, 'KI4', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(116, 3, 2, 'KI4', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(117, 3, 2, 'KI4', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(118, 3, 2, 'KI4', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-21 07:56:35', 0, 0),
(119, 1, 2, 'KI1', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(120, 1, 2, 'KI1', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(121, 1, 2, 'KI1', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(122, 1, 2, 'KI1', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(123, 1, 2, 'KI2', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(124, 1, 2, 'KI2', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(125, 1, 2, 'KI2', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(126, 1, 2, 'KI2', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(127, 1, 2, 'KI3', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(128, 1, 2, 'KI3', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(129, 1, 2, 'KI3', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(130, 1, 2, 'KI3', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(131, 1, 2, 'KI4', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(132, 1, 2, 'KI4', '70.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(133, 1, 2, 'KI4', '55.0', '69.0', 'C', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0),
(134, 1, 2, 'KI4', '0.0', '54.0', 'D', NULL, NULL, NULL, '2016-04-22 12:36:58', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `aturannhb`
--

CREATE TABLE IF NOT EXISTS `aturannhb` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idtingkat` int(10) unsigned NOT NULL,
  `idkurikulum` int(1) unsigned NOT NULL DEFAULT '1',
  `dasarpenilaian` varchar(50) NOT NULL,
  `idjenisujian` int(10) unsigned NOT NULL,
  `bobot` tinyint(3) unsigned NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_aturannhb_jenisujian` (`idjenisujian`),
  KEY `FK_aturannhb_dasarpenilaian` (`dasarpenilaian`),
  KEY `FK_aturannhb_pelajaran` (`idkurikulum`),
  KEY `FK_aturannhb_tingkat` (`idtingkat`),
  KEY `IX_aturannhb_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `aturannhb`
--

INSERT INTO `aturannhb` (`replid`, `idtingkat`, `idkurikulum`, `dasarpenilaian`, `idjenisujian`, `bobot`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(2, 1, 1, 'KOG', 1, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:17', 0, 0),
(7, 2, 1, 'AF', 9, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:53', 0, 0),
(9, 1, 1, 'KOG', 2, 20, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:18:19', 0, 0),
(12, 2, 2, 'KI3', 10, 30, 1, NULL, NULL, NULL, NULL, '2016-04-20 14:20:55', 0, 0),
(13, 2, 2, 'KI3', 11, 30, 1, NULL, NULL, NULL, NULL, '2016-04-20 14:20:55', 0, 0),
(14, 2, 2, 'KI3', 12, 40, 1, NULL, NULL, NULL, NULL, '2016-04-20 14:20:55', 0, 0),
(15, 1, 2, 'KI3', 10, 30, 1, NULL, NULL, NULL, NULL, '2016-04-20 14:22:28', 0, 0),
(16, 1, 2, 'KI3', 11, 30, 1, NULL, NULL, NULL, NULL, '2016-04-20 14:22:28', 0, 0),
(17, 1, 2, 'KI3', 12, 40, 1, NULL, NULL, NULL, NULL, '2016-04-20 14:22:28', 0, 0),
(18, 1, 2, 'KI4', 13, 100, 1, NULL, NULL, NULL, NULL, '2016-04-22 12:31:40', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `auditbesarjtt`
--

CREATE TABLE IF NOT EXISTS `auditbesarjtt` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `nis` varchar(20) NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditbesarjtt_auditinfo` (`idaudit`),
  KEY `IX_auditbesarjtt_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditbesarjtt`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditbesarjttcalon`
--

CREATE TABLE IF NOT EXISTS `auditbesarjttcalon` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idcalon` int(10) unsigned NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditbesarjttcalon_auditinfo` (`idaudit`),
  KEY `IX_auditbesarjttcalon_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditbesarjttcalon`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditinfo`
--

CREATE TABLE IF NOT EXISTS `auditinfo` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sumber` varchar(100) NOT NULL,
  `idsumber` int(10) unsigned NOT NULL,
  `tanggal` datetime NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_auditinfo_departemen` (`departemen`),
  KEY `IX_auditinfo_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `auditinfo`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditjurnal`
--

CREATE TABLE IF NOT EXISTS `auditjurnal` (
  `status` tinyint(1) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `transaksi` varchar(255) NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `nokas` varchar(100) NOT NULL,
  `idtahunbuku` int(10) unsigned NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `sumber` varchar(40) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditjurnal_auditinfo` (`idaudit`),
  KEY `IX_auditjurnal_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `auditjurnal`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditjurnaldetail`
--

CREATE TABLE IF NOT EXISTS `auditjurnaldetail` (
  `status` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idjurnal` int(10) unsigned NOT NULL,
  `koderek` varchar(15) NOT NULL,
  `debet` decimal(15,0) NOT NULL DEFAULT '0',
  `kredit` decimal(15,0) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_auditjurnaldetail_auditinfo` (`idaudit`),
  KEY `IX_auditjurnaldetail_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `auditjurnaldetail`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditnilai`
--

CREATE TABLE IF NOT EXISTS `auditnilai` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenisnilai` varchar(45) NOT NULL,
  `idnilai` int(10) unsigned NOT NULL,
  `nasli` decimal(10,2) NOT NULL,
  `nubah` decimal(10,2) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alasan` varchar(255) NOT NULL,
  `pengguna` varchar(100) NOT NULL,
  `informasi` varchar(255) NOT NULL,
  PRIMARY KEY (`replid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `auditnilai`
--

INSERT INTO `auditnilai` (`replid`, `jenisnilai`, `idnilai`, `nasli`, `nubah`, `tanggal`, `alasan`, `pengguna`, `informasi`) VALUES
(1, 'nilaiujian', 6, '0.00', '50.00', '2016-04-22 09:06:52', 'testing', 'landlord - landlord', 'Nilai Ujian Nilai Harian FISIKA tanggal 22-04-2016 siswa 15887 DINNIA CITRA  NANDYA'),
(2, 'nilaiujian', 41, '0.00', '90.00', '2016-04-22 09:57:17', 'Testing2', 'landlord - landlord', 'Nilai Ujian Ujian Tengah Semester FISIKA tanggal 22-04-2016 siswa 15887 DINNIA CITRA  NANDYA'),
(3, 'nilaiujian', 246, '50.00', '90.00', '2016-04-22 11:39:30', 'testing3', 'landlord - landlord', 'Nilai Ujian Ujian Tengah Semester BAHASA DAN SASTRA INDONESIA tanggal 22-04-2016 siswa 15819 AISYAH SUGIASTU PUTRI');

-- --------------------------------------------------------

--
-- Table structure for table `auditpenerimaaniuran`
--

CREATE TABLE IF NOT EXISTS `auditpenerimaaniuran` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `nis` varchar(20) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaaniuran_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaaniuran_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditpenerimaaniuran`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditpenerimaaniurancalon`
--

CREATE TABLE IF NOT EXISTS `auditpenerimaaniurancalon` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `idcalon` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaaniurancalon_audit` (`idaudit`),
  KEY `IX_auditpenerimaaniurancalon_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditpenerimaaniurancalon`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditpenerimaanjtt`
--

CREATE TABLE IF NOT EXISTS `auditpenerimaanjtt` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idbesarjtt` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaanjtt_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaanjtt_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditpenerimaanjtt`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditpenerimaanjttcalon`
--

CREATE TABLE IF NOT EXISTS `auditpenerimaanjttcalon` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idbesarjttcalon` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaanjttcalon_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaanjttcalon_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditpenerimaanjttcalon`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditpenerimaanlain`
--

CREATE TABLE IF NOT EXISTS `auditpenerimaanlain` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `sumber` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaanlain_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaanlain_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditpenerimaanlain`
--


-- --------------------------------------------------------

--
-- Table structure for table `auditpengeluaran`
--

CREATE TABLE IF NOT EXISTS `auditpengeluaran` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idpengeluaran` int(10) unsigned NOT NULL,
  `keperluan` varchar(255) NOT NULL,
  `jenispemohon` tinyint(1) unsigned NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `pemohonlain` int(10) unsigned DEFAULT NULL,
  `penerima` varchar(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `tanggalkeluar` datetime NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `petugas` varchar(45) DEFAULT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `keterangan` text,
  `namapemohon` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpengeluaran_auditinfo` (`idaudit`),
  KEY `IX_auditpengeluaran_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditpengeluaran`
--


-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkelompok` int(10) unsigned NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `tglperolehan` date NOT NULL DEFAULT '0000-00-00',
  `foto` blob,
  `keterangan` varchar(255) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT 'unit',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_barang_kelompok` (`idkelompok`),
  KEY `IX_barang_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

--
-- Dumping data for table `barang`
--


-- --------------------------------------------------------

--
-- Table structure for table `besarjtt`
--

CREATE TABLE IF NOT EXISTS `besarjtt` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `cicilan` decimal(15,0) NOT NULL DEFAULT '0',
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pembayaranjtt_siswa` (`nis`),
  KEY `FK_pembayaranjtt_penerimaan` (`idpenerimaan`),
  KEY `IX_besarjtt_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `besarjtt`
--


-- --------------------------------------------------------

--
-- Table structure for table `besarjttcalon`
--

CREATE TABLE IF NOT EXISTS `besarjttcalon` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcalon` int(10) unsigned NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `cicilan` decimal(15,0) NOT NULL DEFAULT '0',
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_besarjttcalon_penerimaan` (`idpenerimaan`),
  KEY `FK_besarjttcalon_calonsiswa` (`idcalon`),
  KEY `IX_besarjttcalon_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `besarjttcalon`
--


-- --------------------------------------------------------

--
-- Table structure for table `bobotnau`
--

CREATE TABLE IF NOT EXISTS `bobotnau` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idinfo` int(10) unsigned DEFAULT NULL,
  `idujian` int(10) unsigned NOT NULL,
  `bobot` int(10) unsigned NOT NULL DEFAULT '0',
  `idaturan` int(10) unsigned DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_bobotnau_infobobot` (`idinfo`),
  KEY `FK_bobotnau_ujian` (`idujian`),
  KEY `IX_bobotnau_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `bobotnau`
--

INSERT INTO `bobotnau` (`replid`, `idinfo`, `idujian`, `bobot`, `idaturan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, NULL, 1, 50, 15, NULL, NULL, NULL, '2016-04-22 08:52:22', 0, 0),
(2, NULL, 2, 100, 16, NULL, NULL, NULL, '2016-04-22 08:52:50', 0, 0),
(3, NULL, 4, 100, 17, NULL, NULL, NULL, '2016-04-22 09:10:25', 0, 0),
(4, NULL, 5, 100, 15, NULL, NULL, NULL, '2016-04-22 11:35:40', 0, 0),
(5, NULL, 6, 90, 16, NULL, NULL, NULL, '2016-04-22 11:36:51', 0, 0),
(6, NULL, 8, 90, 16, NULL, NULL, NULL, '2016-04-22 11:36:51', 0, 0),
(7, NULL, 9, 10, 16, NULL, NULL, NULL, '2016-04-22 11:47:30', 0, 0),
(8, NULL, 7, 100, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(9, NULL, 10, 50, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(10, NULL, 11, 50, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(11, NULL, 12, 50, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `calonsiswa`
--

CREATE TABLE IF NOT EXISTS `calonsiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nopendaftaran` varchar(20) NOT NULL,
  `nisn` varchar(50) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `panggilan` varchar(30) DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `tahunmasuk` int(10) unsigned NOT NULL,
  `idproses` int(10) unsigned NOT NULL,
  `idkelompok` int(10) unsigned NOT NULL,
  `suku` varchar(20) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `status` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `kelamin` varchar(1) NOT NULL,
  `tmplahir` varchar(50) NOT NULL,
  `tgllahir` date NOT NULL,
  `warga` varchar(5) DEFAULT NULL,
  `anakke` tinyint(2) unsigned DEFAULT '0',
  `jsaudara` tinyint(2) unsigned DEFAULT '0',
  `bahasa` varchar(30) DEFAULT NULL,
  `berat` decimal(4,1) unsigned DEFAULT '0.0',
  `tinggi` decimal(4,1) unsigned DEFAULT '0.0',
  `darah` varchar(2) DEFAULT NULL,
  `foto` blob,
  `alamatsiswa` varchar(255) DEFAULT NULL,
  `kodepossiswa` varchar(8) DEFAULT NULL,
  `telponsiswa` varchar(20) DEFAULT NULL,
  `hpsiswa` varchar(20) DEFAULT NULL,
  `emailsiswa` varchar(100) DEFAULT NULL,
  `kesehatan` varchar(150) DEFAULT NULL,
  `asalsekolah` varchar(100) DEFAULT NULL,
  `ketsekolah` varchar(100) DEFAULT NULL,
  `namaayah` varchar(60) DEFAULT NULL,
  `namaibu` varchar(60) DEFAULT NULL,
  `almayah` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `almibu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pendidikanayah` varchar(20) DEFAULT NULL,
  `pendidikanibu` varchar(20) DEFAULT NULL,
  `pekerjaanayah` varchar(60) DEFAULT NULL,
  `pekerjaanibu` varchar(60) DEFAULT NULL,
  `wali` varchar(60) DEFAULT NULL,
  `penghasilanayah` int(10) unsigned DEFAULT '0',
  `penghasilanibu` int(10) unsigned DEFAULT '0',
  `alamatortu` varchar(100) DEFAULT NULL,
  `telponortu` varchar(20) DEFAULT NULL,
  `hportu` varchar(20) DEFAULT NULL,
  `emailayah` varchar(100) DEFAULT NULL,
  `alamatsurat` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `replidsiswa` int(10) unsigned DEFAULT NULL,
  `emailibu` varchar(100) DEFAULT NULL,
  `sum1` decimal(10,0) NOT NULL DEFAULT '0',
  `sum2` decimal(10,0) NOT NULL DEFAULT '0',
  `ujian1` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ujian2` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ujian3` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ujian4` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ujian5` decimal(5,2) NOT NULL DEFAULT '0.00',
  `info1` varchar(20) DEFAULT NULL,
  `info2` varchar(20) DEFAULT NULL,
  `info3` varchar(20) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`nopendaftaran`),
  UNIQUE KEY `UX_calonsiswa_replid` (`replid`),
  KEY `FK_calonsiswa_prosespenerimaansiswa` (`idproses`),
  KEY `FK_calonsiswa_kelompokcalonsiswa` (`idkelompok`),
  KEY `FK_calonsiswa_suku` (`suku`),
  KEY `FK_calonsiswa_agama` (`agama`),
  KEY `FK_calonsiswa_statusiswa` (`status`),
  KEY `FK_calonsiswa_asalsekolah` (`asalsekolah`),
  KEY `FK_calonsiswa_tingkatpendidikan` (`pendidikanayah`),
  KEY `FK_calonsiswa_jenispekerjaan` (`pekerjaanayah`),
  KEY `FK_calonsiswa_tingkatpendidikanibu` (`pendidikanibu`),
  KEY `FK_calonsiswa_jenispekerjaanibu` (`pekerjaanibu`),
  KEY `FK_calonsiswa_kondisisiswa` (`kondisi`),
  KEY `IX_calonsiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 196608 kB; (`pendidikanibu`) REFER `jbsumum/tin' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `calonsiswa`
--


-- --------------------------------------------------------

--
-- Table structure for table `dasarpenilaian`
--

CREATE TABLE IF NOT EXISTS `dasarpenilaian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dasarpenilaian` varchar(50) NOT NULL,
  `idkurikulum` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dasarpenilaian`),
  UNIQUE KEY `UX_dasarpenilaian_replid` (`replid`),
  KEY `IX_dasarpenilaian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dasarpenilaian`
--

INSERT INTO `dasarpenilaian` (`replid`, `dasarpenilaian`, `idkurikulum`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(3, 'AF', 1, 'AFEKTIF', NULL, NULL, NULL, '2016-01-22 11:32:59', 0, 0),
(4, 'KI1', 2, 'Sikap Spiritual', NULL, NULL, NULL, '2016-03-23 09:30:00', 0, 0),
(5, 'KI2', 2, 'Sikap Sosial', NULL, NULL, NULL, '2016-03-23 09:30:00', 0, 0),
(6, 'KI3', 2, 'Pengetahuan', NULL, NULL, NULL, '2016-03-23 09:30:42', 0, 0),
(7, 'KI4', 2, 'Ketrampilan', NULL, NULL, NULL, '2016-03-23 09:50:59', 0, 0),
(1, 'KOG', 1, 'KOGNITIF', NULL, NULL, NULL, '2016-01-22 11:32:28', 0, 0),
(2, 'PSI', 1, 'PSIKOMOTORIK', NULL, NULL, NULL, '2016-01-22 11:32:46', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `datadsp`
--

CREATE TABLE IF NOT EXISTS `datadsp` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `dsp` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `operator` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_datadsp_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `datadsp`
--


-- --------------------------------------------------------

--
-- Table structure for table `data_pelanggaran`
--

CREATE TABLE IF NOT EXISTS `data_pelanggaran` (
  `id_pelanggaran` int(5) NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `id_jenis` int(5) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  PRIMARY KEY (`id_pelanggaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `data_pelanggaran`
--

INSERT INTO `data_pelanggaran` (`id_pelanggaran`, `nis`, `id_jenis`, `tanggal`) VALUES
(13, '15188', 10, '18-04-2013'),
(5, '15188', 11, '01-03-2012'),
(38, '15188', 10, '17-04-2013'),
(39, '15188', 12, '01-02-2013'),
(37, '15188', 11, '12-02-2012'),
(36, '15195', 10, '19-02-2012'),
(58, '15188', 12, '11-04-2013'),
(59, '15188', 11, '11-04-2013');

-- --------------------------------------------------------

--
-- Table structure for table `deleteddata`
--

CREATE TABLE IF NOT EXISTS `deleteddata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tablename` varchar(100) NOT NULL,
  `rowid` bigint(20) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IX_deleteddata_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `deleteddata`
--


-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE IF NOT EXISTS `departemen` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departemen` varchar(50) NOT NULL,
  `nipkepsek` varchar(30) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`departemen`),
  UNIQUE KEY `UX_departemen_replid` (`replid`),
  KEY `FK_departemen_pegawai` (`nipkepsek`),
  KEY `IX_departemen_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`replid`, `departemen`, `nipkepsek`, `urutan`, `keterangan`, `aktif`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(11, 'SMAN 1 Malang', '5', 1, '', 1, NULL, NULL, NULL, '2013-02-14 21:29:59', 34486, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `idpelajaran` int(10) unsigned NOT NULL,
  `statusguru` varchar(50) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_guru_pegawai` (`nip`),
  KEY `FK_guru_pelajaran` (`idpelajaran`),
  KEY `FK_guru_statusguru` (`statusguru`),
  KEY `IX_guru_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=119 ;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`replid`, `nip`, `idpelajaran`, `statusguru`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(46, '2', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-02 15:38:50', 34283, 0),
(47, '4', 46, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-02 15:39:05', 57593, 0),
(51, '5', 52, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:13:32', 62950, 0),
(52, '6', 53, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:13:56', 4517, 0),
(53, '7', 53, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:14:39', 33594, 0),
(54, '8', 53, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:14:58', 4508, 0),
(55, '9', 54, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:15:19', 26618, 0),
(56, '10', 55, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:16:37', 4707, 0),
(57, '11', 52, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:17:08', 12183, 0),
(58, '12', 52, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:17:19', 52685, 0),
(59, '13', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:20:39', 20384, 0),
(60, '14', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:20:55', 14411, 0),
(61, '15', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:21:08', 35857, 0),
(62, '16', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:26:59', 20528, 0),
(63, '17', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:27:15', 54057, 0),
(64, '18', 47, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:27:28', 37960, 0),
(65, '19', 56, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:27:56', 38932, 0),
(66, '21', 56, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:28:14', 53637, 0),
(67, '20', 68, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:28:36', 51592, 0),
(68, '22', 50, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:29:02', 19731, 0),
(69, '23', 50, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:29:15', 13692, 0),
(70, '24', 50, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:29:38', 16953, 0),
(71, '25', 50, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:29:55', 38916, 0),
(72, '26', 50, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:30:15', 24384, 0),
(73, '27', 48, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:30:48', 23849, 0),
(74, '28', 48, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:30:58', 2925, 0),
(75, '29', 48, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:31:10', 738, 0),
(76, '30', 51, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:31:39', 18148, 0),
(77, '31', 51, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:31:47', 26107, 0),
(78, '32', 46, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:33:21', 37502, 0),
(79, '33', 46, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:33:31', 37456, 0),
(80, '34', 46, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:33:45', 25653, 0),
(81, '35', 46, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:33:59', 54482, 0),
(82, '36', 46, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:34:11', 31550, 0),
(83, '37', 45, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:35:28', 22138, 0),
(84, '38', 45, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:35:42', 25745, 0),
(85, '39', 45, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:35:58', 49902, 0),
(86, '40', 45, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:36:12', 14679, 0),
(87, '41', 45, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:36:31', 52591, 0),
(88, '42', 57, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:37:58', 5106, 0),
(89, '43', 57, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:38:12', 51767, 0),
(90, '44', 57, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:38:47', 24148, 0),
(91, '45', 57, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:38:57', 5523, 0),
(92, '46', 58, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:39:26', 40457, 0),
(93, '47', 58, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:39:34', 49386, 0),
(94, '48', 58, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:39:41', 19019, 0),
(95, '49', 58, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:39:57', 60440, 0),
(96, '50', 70, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:41:03', 30518, 0),
(97, '52', 70, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:41:33', 3569, 0),
(98, '51', 59, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:42:07', 13009, 0),
(99, '53', 60, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:42:57', 6957, 0),
(100, '54', 61, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:43:26', 35083, 0),
(101, '55', 62, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:43:53', 30966, 0),
(102, '56', 63, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:44:14', 43317, 0),
(103, '57', 64, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:44:31', 36731, 0),
(104, '58', 65, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:44:52', 51446, 0),
(105, '59', 49, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:45:25', 60194, 0),
(106, '60', 49, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:45:45', 8423, 0),
(107, '61', 49, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:46:05', 25274, 0),
(108, '67', 66, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:46:33', 27764, 0),
(109, '68', 67, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:46:48', 54264, 0),
(110, '69', 66, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-03 22:47:26', 7806, 0),
(111, '50', 59, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 18:49:31', 16204, 0),
(112, '62', 71, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 18:58:59', 36428, 0),
(113, '63', 71, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 18:59:17', 4543, 0),
(114, '64', 71, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 18:59:38', 6006, 0),
(115, '65', 71, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 18:59:56', 15836, 0),
(116, '66', 71, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 19:00:12', 46337, 0),
(117, '11', 72, 'Guru Pelajaran', 1, '', NULL, NULL, NULL, '2013-02-13 19:19:12', 45959, 0),
(118, '2', 51, 'Guru Pelajaran', 1, 'Test', NULL, NULL, NULL, '2016-04-21 06:34:29', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE IF NOT EXISTS `identitas` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(250) DEFAULT NULL,
  `situs` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `alamat1` varchar(255) DEFAULT NULL,
  `alamat2` varchar(255) DEFAULT NULL,
  `alamat3` varchar(255) DEFAULT NULL,
  `alamat4` varchar(255) DEFAULT NULL,
  `telp1` varchar(20) DEFAULT NULL,
  `telp2` varchar(20) DEFAULT NULL,
  `telp3` varchar(20) DEFAULT NULL,
  `telp4` varchar(20) DEFAULT NULL,
  `fax1` varchar(20) DEFAULT NULL,
  `fax2` varchar(20) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `foto` blob,
  `departemen` varchar(50) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `perpustakaan` varchar(45) DEFAULT NULL,
  `info1` varchar(20) DEFAULT NULL,
  `info2` varchar(20) DEFAULT NULL,
  `info3` varchar(20) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_identitas_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`replid`, `nama`, `situs`, `email`, `alamat1`, `alamat2`, `alamat3`, `alamat4`, `telp1`, `telp2`, `telp3`, `telp4`, `fax1`, `fax2`, `keterangan`, `foto`, `departemen`, `status`, `perpustakaan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(4, 'MAN Kota Blitar', 'www.sman1-mlg.net', 'info@sman1-mlg.net', 'Jl. Tugu Utara No. 01 Malang', '', NULL, NULL, '(0341) 123456', '(0341) 123456', '', '', '(0341) 123456', '', NULL, 0xffd8ffe000104a46494600010100000100010000fffe003b43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d2038300affdb00430006040506050406060506070706080a100a0a09090a140e0f0c1017141818171416161a1d251f1a1b231c1616202c20232627292a29191f2d302d283025282928ffdb0043010707070a080a130a0a13281a161a2828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828282828ffc0001108005a002c03012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00faa69199554962001d49ae4bc5de37b4f0f69b25d1b5bdba0bc016d6ed2b13ecaa3f9e07bd7cbbf117f682f11ea52c96ba45b3e8f0e480f70999b1eca46d53f99f7a00fa97c57e3bd07c336e65d4afe18ffba0b72c7d15464b1fa035e65a8fc46d77c4a5e2d0e3b7d22cdb205e6a20ee23d561539ffbecfe15f23b78835296f1af27b9927bb6e5a7958bb9fc4f356478b3570c0fda0d311f697813c78f1ddc3a1788e6b7fb7118867873e55c81dd33d1bd50f3dc6474f5289d5e356460ca46411c835f9b7378af539942cb296008607241523a107b11eb5f5bfc1af146afad7c3cd32f6fef1dee5b7a3b80017dac5413ef8033486767fbf85dda28637573bb2e99e718eb505db4774863bed3a09d0f055c6e1f91cd713aa6b36363e24d4e2bd7b849bcc46478ae1e3217cb5e30081d73daacc1e254207d9f5ab9c7f76e238e61f9e01fd6be771190e3bda4aa61b1364db766b457d77ff807b70a72e48b70be889352f03f83751c9bbf0d59ab1eac9084ff00d0715cb6a3f05bc0b79931437f64c7fe785c1c0fc1c3576b6fe21999806934bb95f70f0b7e9b855e5d5e06c7da34db81fed5bca92afea41fd2b0fa9711d2d60e33f9ff009d8e3af5f07465cb5a2e37f53c5750fd9f34c6c9d2fc4b3a7a2dcdbabfeaa47f2af51f86fe1b93c25e10b4d226ba8aeda2691bcd894aa9cb93d0f35b7f6cd1dffd64f241ff005de0641f9e315757c830426d668a688af0f1b641e4d7665d88cd655bd9e3a87246dbf4bfaa7630a92c24e37a13bbec79c6b9a5697a878bbc4736af0dc489696d04ea2190213ca291c8ef9a8aefc2fe1f81eea4b68eefcbb417311492e3fd63c2f1fcd9038043918f6acdf1ddd5ec1e36d42dec1887bd8a2b774c03bc155c0e7a1c81cd49a8a6b363772d96a1e23d1219e77904ea4e4c6cca37efc47f2e76af4ee01afa9b3b2d4fb18616a3a54a4aa5af14ed77b595f44bb9d16b5a1e91a8db6bb2da94d32e60bc91229966263554f2d4029e8c58f23a56a69d616821d7f4d8e511fd862849b88e5134b36ddc5c85fe163b7eefb8ae565b5d7a1b4d52f2ef5cd351555275902031dc2cf84661f274217d3ef2f6eb5aba326bd2c91c8baf695243223486f2d9461cc0b90ae4a024e0f39cf19ad70ea57694bfa5a9f2fc418754f0b19ca4a693b24af7d577697e64914f37fc22b7b76d1456f736d6f1dc47b2e19e4756700175c61720e7ad6df82a66b9f0e5bcf27df91a473f52ed5c85d5deb33787ae655d5b4ebcb210ac73dac2bf3468586d2c368e41c0072715d778087fc52965ff0003ff00d0dabaf1d4f970edbfe6fd3ccf8bcb6aa9e2e3157b72bdedaebbe9a1e69f12c85f1edc13234407947cc5eabf2af23dc56a3f8af4a5160979777faacd0bc8df6e92d5125886c2aa0649df8273c9ed55fc696f6f77f1416def9996d6578564218290a50773c0adbb5f05784e44803eaca2532b2caa2ed0855cb804123d96b834b4533f62a95f0b4f0943eb37d209e8976efbfe36ee66ea5e24d33574dceb7f223436c9224ec18b18a566604f00e55bae3835a573e2ad364863891f512b199d019369f9648ca8c0180003dbd299a6f873407d16da596fc0ba641e6c62651b0ef507ff001d24d5993c33e1f51386bd0088a47422e508dc31b5738f426bd5a70c2525697336bc8fc8b34cc31b8daafd8f2c697d94decb4dfcf45728c9e25d39bc3175a5c4b79199ad6340a42f96b223027007277104ee35d7780bfe454b1ff81ffe86d5e6badd958db40cd6729775bb9a1c170d945dbb5b8f5c9e7dabd2fc03ff0022a58ffc0fff00436a79953847097a77d65d7d0e0ca2ad49e3796adaea36d3d533cffc63f61ff85a69fdac40d3f743e7e738d9e58cf4ad59ed7c0cf6f75fd97736e971b4f9458bf1fbbc0cf18fbd8359ff0018b40beb4d75f5478b759cca88597fe59b85030de99c641ef5c2583849c03d1862b87091529c3deb7a1faee6d8678ac89d6a126dc69fc29e8edab4d77dcf5c68bc0a263b6684e66f973bf684f2fbf7fbe33f8d70f2dac5f69658a27952779c40c8768554008383c9ce4002b3d14b3055e493802bca3c59ab4d79f10a5f26e25fb3dbdda5bc4039002a10991f5da4fe35ea63272cbe29c64e4df77dbfcf63f1bc0538e6b36a505151becbabd17ddb9eb1fba496d62b869564ba98dbc21632d97db9e7d07f8d7b0f80863c29659ff6ff00f436af9bbe2778bf55d17c66b67a65df9105a1598a08d0e6470db8924127e57c60f15f4bf83b6ffc23d6fb00085a42b8f42ec47e95c398635e22338745256fc4f4b2bcbd61674ea75945dfef4d7e07a2eb3a5c1a9d9cb0dc4692248a5195c6430f43fe78af9afe21781ee7c357ad35a24b2e9aeff2b632d11feeb7f43debea36fb86b0fc444a79257e5272091c6457934ea383d0fb6caf34ab97d4f7758bdd773e62d31dda48a568e4ca9f9b09c8f7af18bcd0750b0f184b68d05c4fe45f7966610b00f893ef7e35f75ef703866edde97ed1373fbe93a9fe235d58cc6bc5461ccb55f89f3f4b054b0d88ab2a0ad093ba8f6f2bf55d8f8cbe33e9371ff09c5cdcdbc1732ade46263f216da43326d040e9f267f1afaa3c021d7c19a379a183fd9632c1860e768cd74627980e2593bff11aad7723bc8a59d98ed1d4e6b9a734dcacb766b4e9b5185ddda47fffd9, 'SMA', 0, NULL, NULL, NULL, NULL, '2013-02-13 19:05:35', 63030, 0),
(6, 'Perpustakaan Umum', 'www.sman1-mlg.sch.id', 'info@sman1-mlg.sch.id', 'Jl. Tugu Utara No. 01 Malang', NULL, NULL, NULL, '(0341) 123456', '(0341) 123456', NULL, NULL, '(0341) 123456', NULL, NULL, 0xffd8ffe000104a46494600010100000100010000fffe003b43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d2037300affdb0043000a07070807060a0808080b0a0a0b0e18100e0d0d0e1d15161118231f2524221f2221262b372f26293429212230413134393b3e3e3e252e4449433c48373d3e3bffdb0043010a0b0b0e0d0e1c10101c3b2822283b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3bffc00011080050002703012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f65a8e6b986d90b4d22a0033cd73faeeb5ad25a31d174e5b99738f9e4d817ea7fcfd6bc6fc4f07c44d59dcea56776d093feaad46e8ff0010a4e7f1cd4a945ecc7667a96adf12b4f8646b6d297edf38e0f96c0229f773c0fc33f4ac68fc4babadc8d4a4d52d642bd6c238f008f6909ceefc00fa75af1995754b1f9678ae6df1d9d0ae3f31510d46ec74b87fceac47d4da16bf65aed9adc5ac809e8cbd0ab0ea08ec47a515e19f0b350bb7f17342d70fe5cb6cc5d73c3118c13ee28a407acbdf5b2cf22f9b34651807611b6d0700fde031dc5491dd2cfc43a84337b09158fe55cadfddeaf16bd7f1e9d6d34d1aba96f2c1382507f853adee758bcbafb35c697971f78ce8303e5ddc93ed5e6d5c9a94e4e719ca2deba3efe47aea94d413bad97e4755225c15c3c68ea7b15e2b2eef41d22f33f6ad0ec6427bf90a0fe6066b3adef6e21bb86d7ec1716d2ce9e646b13b2065eb9e0e2b53ed9791dc241f68919d915f122ab0e4671920738f7acd64f8c8abd1c47de8f3ebe2a3467cb529fcd1534af0ae89a5ea7f6eb0d37ecd3842bb9646db838cf049a2b46db54964be3653470f99b4ee2aa559718ed93457761e8e268c39712d39797630f6b4eafbd4d591cf5deaf0e97e21d4bcd2e0c935ac8360ea13048fc8d578b5fb21691c4f2ca5953616f2f3fc32ae7aff00b62a3d59de1f1d3491c0970c922158a4202b7c83824f4ad88ae7cbd505ccdaabc2c523325bc9789f282e782ca30c3d873f357a0d2b23ea250a71a506d5db8a7bf95bb32a7fc24568d671c336f58e485a305a0dc0e55467ae7ef26d2070454f73a9e9b736d662e0ca52de1f2a3dd06e4660aa376d27048391cfb5392e64177115d452de1b68dd4225c040479a78ce0f0060e3b8c54d35d66e5123be481216b86511ce101cb020671c7073f415d14236d5afebee3e4f38a949c946974dfaf5f91169b7305f78b64bab727cb7838057046028231f876a2a4b231378cee1e02ad1bc5b9594e41c85e7f3a2b3c65b9a36ec8e5c037c92bff3331758d3cea7e36b8b30fb0be086dbbba460f4fc29f3782a582dbed06f11d42a31554fef1031d7b668d4d2ca4f1d4eba84cd0dbe06e756c107ca18e7eb8ab9158f8703a797a9662dff00bcccc7eefcdedfee8a74f5925fa5cfadcc3195b0d838ba52b7bb1e9a6be607c1f22398c5da1da33f2a64751ef504fe1a680407ed6a44d2ac63e4e84e7dfdbf5a7df5be911d813657af25c6d5f97cc3c9cf200ff3d0564c91b96509e6e4c8d192c7ab2b15c8fcabd4a72a92b5e7f81f9bd68d2577c97f3e6bee6a7848635b61e9137f31451e0f68e4d504b0b978da27018a91c8600fea28af371f252aa9aec7ad964250a2e325677633c6fa25ddaea726a0577412ed048fe02140c1fae3835cedab72cbebc8af73bcb386f2068a6457561b48619047a1af2fd6bc137f65a813a6c5e7424e429750c9ec724647bd4e1abf24937d0fab955a798602582aad45db47d34d8e7353b9367a3de5c2b157588aa107905be51fcf3f8560f866f66b5d0359bb590f9910468d9b9dac430079f735d86ade0dd6751d16e2d96c8f9a76bc63cf45cb03d0e4e31827f4accd1be1f7886df43d5acef34e50f728be5017319cb28623a371f36deb5b62eaf357bc5f4fd19f2382a0e9e1dc26b5bebf7afc0d0f857a9dd6a53dc7dae632bc45b0cc06406e71f9827f1a2ad7c36f09eb5e1db9bb9755b4581240bb4f9c8f9ebfdd27d68ae09cae97a7eacf4a11b3979bfd11ffd9, 'P_alls', 1, 'alls', NULL, NULL, NULL, '2013-02-13 22:47:44', 22308, 0),
(7, 'Sekolah Menengah Atas Negeri 1 Malang', 'www.sman1-mlg.sch.id', 'info@sman1-mlg.sch.id', 'Jl. Tugu Utara No. 01', '', NULL, NULL, '0341-123456', '0341-123456', '', '', '0341-1234560341-1234', '', NULL, NULL, 'SMAN 1 Malang', 0, '1', NULL, NULL, NULL, '2013-02-23 14:25:00', 13447, 0);

-- --------------------------------------------------------

--
-- Table structure for table `infobobotnau`
--

CREATE TABLE IF NOT EXISTS `infobobotnau` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idujian` int(10) unsigned NOT NULL,
  `idaturan` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_infobobotnau_ujian` (`idujian`),
  KEY `IX_infobobotnau_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `infobobotnau`
--


-- --------------------------------------------------------

--
-- Table structure for table `infobobotujian`
--

CREATE TABLE IF NOT EXISTS `infobobotujian` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `idkelas` int(10) unsigned NOT NULL DEFAULT '0',
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idjenisujian` int(10) unsigned NOT NULL DEFAULT '0',
  `pilihan` tinyint(4) NOT NULL DEFAULT '0',
  `info` varchar(100) NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_infobobotujian_idpelajaran` (`idpelajaran`),
  KEY `FK_infobobotujian_idsemester` (`idsemester`),
  KEY `FK_infobobotujian_idjenis` (`idjenisujian`),
  KEY `FK_infobobotujian_kelas` (`idkelas`),
  KEY `IX_infobobotujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `infobobotujian`
--


-- --------------------------------------------------------

--
-- Table structure for table `infojadwal`
--

CREATE TABLE IF NOT EXISTS `infojadwal` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(100) NOT NULL DEFAULT '',
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `terlihat` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `idtahunajaran` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_infojadwal_tahunajaran` (`idtahunajaran`),
  KEY `IX_infojadwal_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `infojadwal`
--

INSERT INTO `infojadwal` (`replid`, `deskripsi`, `aktif`, `terlihat`, `keterangan`, `idtahunajaran`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'Semester Genap', 1, 1, NULL, 21, NULL, NULL, NULL, '2013-02-03 00:15:55', 23179, 0),
(2, 'jadwal th 2012/2013', 1, 1, NULL, 22, NULL, NULL, NULL, '2013-02-14 22:34:49', 63570, 0),
(3, 'Jadwal Semeseter Genap 2015', 1, 1, NULL, 2, NULL, NULL, NULL, '2016-01-22 11:46:26', 0, 0),
(4, 'Baru', 1, 1, NULL, 3, NULL, NULL, NULL, '2016-04-04 10:22:13', 0, 0),
(5, 'Ganjil', 0, 1, NULL, 2, NULL, NULL, NULL, '2016-04-04 10:23:09', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `infonap`
--

CREATE TABLE IF NOT EXISTS `infonap` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idkelas` int(10) unsigned NOT NULL DEFAULT '0',
  `nilaimin` decimal(5,2) NOT NULL DEFAULT '0.00',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_infonap_pelajaran` (`idpelajaran`),
  KEY `FK_infonap_semester` (`idsemester`),
  KEY `FK_infonap_kelas` (`idkelas`),
  KEY `IX_infonap_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `infonap`
--

INSERT INTO `infonap` (`replid`, `idpelajaran`, `idsemester`, `idkelas`, `nilaimin`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 45, 1, 50, '80.00', NULL, NULL, NULL, '2016-04-22 09:11:07', 0, 0),
(4, 47, 1, 50, '50.00', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkelas` int(10) unsigned NOT NULL DEFAULT '0',
  `nipguru` varchar(30) NOT NULL,
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `departemen` varchar(50) NOT NULL DEFAULT '',
  `infojadwal` int(10) unsigned NOT NULL DEFAULT '0',
  `hari` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `jamke` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `njam` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sifat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `jam1` time NOT NULL DEFAULT '00:00:00',
  `jam2` time NOT NULL DEFAULT '00:00:00',
  `idjam1` int(10) unsigned NOT NULL DEFAULT '0',
  `idjam2` int(10) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jadwal_departemen` (`departemen`),
  KEY `FK_jadwal_pegawai` (`nipguru`),
  KEY `FK_jadwal_jam1` (`idjam1`),
  KEY `FK_jadwal_jam2` (`idjam2`),
  KEY `FK_jadwal_infojadwal` (`infojadwal`),
  KEY `FK_jadwal_pelajaran` (`idpelajaran`),
  KEY `FK_jadwal_kelas` (`idkelas`),
  KEY `IX_jadwal_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`replid`, `idkelas`, `nipguru`, `idpelajaran`, `departemen`, `infojadwal`, `hari`, `jamke`, `njam`, `sifat`, `status`, `keterangan`, `jam1`, `jam2`, `idjam1`, `idjam2`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(49, 50, '40', 45, 'SMAN 1 Malang', 3, 1, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 04:49:09', 0, 0),
(50, 50, '51', 59, 'SMAN 1 Malang', 3, 2, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 04:49:26', 0, 0),
(51, 50, '2', 47, 'SMAN 1 Malang', 3, 3, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 04:49:41', 0, 0),
(52, 50, '24', 50, 'SMAN 1 Malang', 3, 4, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 04:49:59', 0, 0),
(53, 50, '31', 51, 'SMAN 1 Malang', 3, 5, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 04:50:21', 0, 0),
(54, 50, '27', 48, 'SMAN 1 Malang', 3, 6, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 04:50:44', 0, 0),
(55, 50, '53', 60, 'SMAN 1 Malang', 3, 1, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 04:51:09', 0, 0),
(56, 50, '7', 53, 'SMAN 1 Malang', 3, 2, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 05:01:41', 0, 0),
(57, 50, '28', 48, 'SMAN 1 Malang', 3, 3, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 05:02:14', 0, 0),
(58, 50, '11', 72, 'SMAN 1 Malang', 3, 4, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 05:02:35', 0, 0),
(59, 50, '55', 62, 'SMAN 1 Malang', 3, 6, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 05:02:55', 0, 0),
(60, 50, '68', 67, 'SMAN 1 Malang', 3, 1, 5, 2, 1, 0, '', '10:15:00', '11:45:00', 38, 39, NULL, NULL, NULL, '2016-01-23 05:03:15', 0, 0),
(61, 50, '44', 57, 'SMAN 1 Malang', 3, 2, 5, 2, 1, 0, '', '10:15:00', '11:45:00', 38, 39, NULL, NULL, NULL, '2016-01-23 05:03:40', 0, 0),
(62, 50, '56', 63, 'SMAN 1 Malang', 3, 3, 5, 2, 1, 0, '', '10:15:00', '11:45:00', 38, 39, NULL, NULL, NULL, '2016-01-23 05:03:52', 0, 0),
(63, 50, '16', 47, 'SMAN 1 Malang', 3, 4, 5, 2, 1, 0, '', '10:15:00', '11:45:00', 38, 39, NULL, NULL, NULL, '2016-01-23 05:04:06', 0, 0),
(64, 50, '43', 57, 'SMAN 1 Malang', 3, 5, 5, 2, 1, 0, '', '10:15:00', '11:45:00', 38, 39, NULL, NULL, NULL, '2016-01-23 05:04:30', 0, 0),
(65, 50, '36', 46, 'SMAN 1 Malang', 3, 6, 5, 2, 1, 0, '', '10:15:00', '11:45:00', 38, 39, NULL, NULL, NULL, '2016-01-23 05:04:45', 0, 0),
(66, 50, '11', 52, 'SMAN 1 Malang', 3, 1, 7, 2, 1, 0, '', '11:50:00', '13:20:00', 40, 41, NULL, NULL, NULL, '2016-01-23 05:05:09', 0, 0),
(67, 50, '20', 68, 'SMAN 1 Malang', 3, 2, 7, 2, 1, 0, '', '11:50:00', '13:20:00', 40, 41, NULL, NULL, NULL, '2016-01-23 05:05:27', 0, 0),
(68, 50, '59', 49, 'SMAN 1 Malang', 3, 3, 7, 3, 1, 0, '', '11:50:00', '14:30:00', 40, 42, NULL, NULL, NULL, '2016-01-23 05:05:47', 0, 0),
(69, 50, '40', 45, 'SMAN 1 Malang', 3, 4, 7, 3, 1, 0, '', '11:50:00', '14:30:00', 40, 42, NULL, NULL, NULL, '2016-01-23 05:06:11', 0, 0),
(70, 50, '51', 59, 'SMAN 1 Malang', 3, 5, 7, 2, 1, 0, '', '11:50:00', '13:20:00', 40, 41, NULL, NULL, NULL, '2016-01-23 05:06:49', 0, 0),
(71, 50, '28', 48, 'SMAN 1 Malang', 3, 6, 7, 3, 1, 0, '', '11:50:00', '14:30:00', 40, 42, NULL, NULL, NULL, '2016-01-23 05:07:13', 0, 0),
(72, 50, '51', 59, 'SMAN 1 Malang', 3, 1, 9, 2, 1, 0, '', '13:45:00', '15:15:00', 42, 43, NULL, NULL, NULL, '2016-01-23 05:08:01', 0, 0),
(73, 50, '53', 60, 'SMAN 1 Malang', 3, 2, 9, 2, 1, 0, '', '13:45:00', '15:15:00', 42, 43, NULL, NULL, NULL, '2016-01-23 05:08:26', 0, 0),
(74, 51, '40', 45, 'SMAN 1 Malang', 3, 3, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 05:08:47', 0, 0),
(75, 52, '40', 45, 'SMAN 1 Malang', 3, 4, 1, 4, 1, 0, '', '06:45:00', '09:50:00', 34, 37, NULL, NULL, NULL, '2016-01-23 05:09:03', 0, 0),
(76, 53, '40', 45, 'SMAN 1 Malang', 3, 5, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-23 05:09:28', 0, 0),
(77, 54, '40', 45, 'SMAN 1 Malang', 3, 2, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 05:10:13', 0, 0),
(78, 50, '63', 71, 'SMAN 1 Malang', 3, 5, 3, 2, 1, 0, '', '08:20:00', '09:50:00', 36, 37, NULL, NULL, NULL, '2016-01-23 05:10:47', 0, 0),
(79, 51, '51', 59, 'SMAN 1 Malang', 3, 1, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-30 09:14:33', 0, 0),
(80, 51, '40', 45, 'SMAN 1 Malang', 3, 2, 1, 2, 1, 0, '', '06:45:00', '08:15:00', 34, 35, NULL, NULL, NULL, '2016-01-30 09:15:58', 0, 0),
(81, 61, '50', 59, 'SMAN 1 Malang', 3, 1, 1, 3, 1, 0, 'Coba', '06:45:00', '09:05:00', 34, 36, NULL, NULL, NULL, '2016-04-20 12:20:47', 0, 0),
(82, 50, '2', 51, 'SMAN 1 Malang', 3, 5, 9, 2, 1, 0, '', '13:45:00', '15:15:00', 42, 43, NULL, NULL, NULL, '2016-04-21 06:35:11', 0, 0),
(83, 50, '2', 47, 'SMAN 1 Malang', 5, 1, 1, 3, 1, 0, '', '06:45:00', '09:05:00', 34, 36, NULL, NULL, NULL, '2016-04-21 08:56:59', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE IF NOT EXISTS `jam` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jamke` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `departemen` varchar(50) NOT NULL DEFAULT '',
  `jam1` time NOT NULL DEFAULT '00:00:00',
  `jam2` time NOT NULL DEFAULT '00:00:00',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jam_departemen` (`departemen`),
  KEY `IX_jam_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`replid`, `jamke`, `departemen`, `jam1`, `jam2`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(34, 1, 'SMAN 1 Malang', '06:45:00', '07:30:00', NULL, NULL, NULL, '2013-02-13 19:01:12', 52283, 0),
(35, 2, 'SMAN 1 Malang', '07:30:00', '08:15:00', NULL, NULL, NULL, '2013-02-13 19:01:45', 40376, 0),
(36, 3, 'SMAN 1 Malang', '08:20:00', '09:05:00', NULL, NULL, NULL, '2013-02-13 19:02:28', 45031, 0),
(37, 4, 'SMAN 1 Malang', '09:05:00', '09:50:00', NULL, NULL, NULL, '2013-02-13 19:02:56', 38498, 0),
(38, 5, 'SMAN 1 Malang', '10:15:00', '11:00:00', NULL, NULL, NULL, '2013-02-13 19:03:25', 57397, 0),
(39, 6, 'SMAN 1 Malang', '11:00:00', '11:45:00', NULL, NULL, NULL, '2013-02-13 19:03:40', 40428, 0),
(40, 7, 'SMAN 1 Malang', '11:50:00', '12:35:00', NULL, NULL, NULL, '2013-02-13 19:04:02', 34367, 0),
(41, 8, 'SMAN 1 Malang', '12:35:00', '13:20:00', NULL, NULL, NULL, '2013-02-13 19:04:23', 3153, 0),
(42, 9, 'SMAN 1 Malang', '13:45:00', '14:30:00', NULL, NULL, NULL, '2013-02-13 19:04:42', 30350, 0),
(43, 10, 'SMAN 1 Malang', '14:30:00', '15:15:00', NULL, NULL, NULL, '2013-02-13 19:04:58', 34977, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenismutasi`
--

CREATE TABLE IF NOT EXISTS `jenismutasi` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenismutasi` varchar(45) NOT NULL DEFAULT '',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_jenismutasi_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jenismutasi`
--

INSERT INTO `jenismutasi` (`replid`, `jenismutasi`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(2, 'Pindah Sekolah', '', NULL, NULL, NULL, '2010-03-02 10:06:17', 29948, 0),
(3, 'Dikeluarkan', 'Sekedar contoh. Menu ini mengatur jenis-jenis mutasi siswa. Ubah atau tambahkan data ini sesuai dengan jenis-jenis mutasi siswa yang terjadi.', NULL, NULL, NULL, '2010-03-02 10:06:17', 28455, 0),
(4, 'Mengundurkan Diri', '', NULL, NULL, NULL, '2010-03-02 10:06:17', 52431, 0),
(5, 'Meninggal Dunia', '', NULL, NULL, NULL, '2010-03-02 10:06:17', 45730, 0),
(6, 'Pindah Tempat Tinggal', '', NULL, NULL, NULL, '2010-03-02 10:06:17', 5828, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenispekerjaan`
--

CREATE TABLE IF NOT EXISTS `jenispekerjaan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pekerjaan`),
  UNIQUE KEY `UX_jenispekerjaan` (`replid`),
  KEY `IX_jenispekerjaan_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jenispekerjaan`
--

INSERT INTO `jenispekerjaan` (`replid`, `pekerjaan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(14, 'PNS', NULL, NULL, NULL, '2010-03-02 10:07:22', 10914, 0),
(15, 'Wiraswasta', NULL, NULL, NULL, '2010-03-02 10:07:22', 53173, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenisujian`
--

CREATE TABLE IF NOT EXISTS `jenisujian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenisujian` varchar(50) NOT NULL,
  `idkurikulum` int(1) unsigned NOT NULL DEFAULT '1',
  `iddasarpenilaian` varchar(10) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned DEFAULT '0',
  `issync` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jenisujian_pelajaran` (`idkurikulum`),
  KEY `IX_jenisujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `jenisujian`
--

INSERT INTO `jenisujian` (`replid`, `jenisujian`, `idkurikulum`, `iddasarpenilaian`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'Ujian AKhir Sekolah', 1, 'KOG', '', 'UAS', NULL, NULL, '2016-04-03 17:52:40', 0, 0),
(2, 'Ujian Tengah Semester', 1, 'KOG', 'Tesing2', 'UTS', NULL, NULL, '2016-04-03 17:56:50', 0, 0),
(10, 'Nilai Harian', 2, 'KI3', 'testing keterangan2', 'NH', NULL, NULL, '2016-04-20 14:18:39', 0, 0),
(11, 'Ujian Tengah Semester', 2, 'KI3', '', 'UTS', NULL, NULL, '2016-04-20 14:19:24', 0, 0),
(12, 'Ujian Akhir Sekolah', 2, 'KI3', '', 'UAS', NULL, NULL, '2016-04-20 14:19:40', 0, 0),
(13, 'Ketrampilan', 2, 'KI4', '', 'KT', NULL, NULL, '2016-04-22 12:31:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pelanggaran`
--

CREATE TABLE IF NOT EXISTS `jenis_pelanggaran` (
  `id_jenis` int(5) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `poin` varchar(10) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `jenis_pelanggaran`
--

INSERT INTO `jenis_pelanggaran` (`id_jenis`, `id_kategori`, `nama`, `poin`) VALUES
(12, 17, 'Mencuri', '25'),
(10, 18, 'Telat', '20'),
(11, 19, 'Lupa', '5'),
(13, 18, 'Membawa Senjata Tajam', '20');

-- --------------------------------------------------------

--
-- Table structure for table `kalenderakademik`
--

CREATE TABLE IF NOT EXISTS `kalenderakademik` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kalender` varchar(50) NOT NULL DEFAULT '',
  `aktif` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `terlihat` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `idtahunajaran` int(10) unsigned NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_kalenderakademik_tahunajaran` (`idtahunajaran`),
  KEY `FK_kalenderakademik_departemen` (`departemen`),
  KEY `IX_kalenderakademik_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kalenderakademik`
--

INSERT INTO `kalenderakademik` (`replid`, `kalender`, `aktif`, `terlihat`, `idtahunajaran`, `departemen`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'Semester Genap', 1, 1, 21, 'SMAN 1 Malang', NULL, NULL, NULL, '2013-02-03 07:41:55', 14943, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_pelanggaran`
--

CREATE TABLE IF NOT EXISTS `kategori_pelanggaran` (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `kategori_pelanggaran`
--

INSERT INTO `kategori_pelanggaran` (`id_kategori`, `nama`) VALUES
(17, 'Tanggung Jawab'),
(18, 'Disiplin'),
(19, 'Kebersihan');

-- --------------------------------------------------------

--
-- Table structure for table `kejadianpenting`
--

CREATE TABLE IF NOT EXISTS `kejadianpenting` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkriteria` int(10) unsigned NOT NULL DEFAULT '0',
  `nis` varchar(15) NOT NULL DEFAULT '',
  `nip` varchar(15) NOT NULL DEFAULT '',
  `tanggal` date NOT NULL DEFAULT '0000-00-00',
  `kejadian` text NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_kejadianpenting_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kejadianpenting`
--


-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kelas` varchar(50) NOT NULL,
  `idtahunajaran` int(10) unsigned NOT NULL,
  `kapasitas` int(10) unsigned NOT NULL,
  `nipwali` varchar(30) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `idtingkat` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_kelas_pegawai` (`nipwali`),
  KEY `FK_kelas_tahunajaran` (`idtahunajaran`),
  KEY `FK_kelas_tingkat` (`idtingkat`),
  KEY `IX_kelas_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`replid`, `kelas`, `idtahunajaran`, `kapasitas`, `nipwali`, `aktif`, `keterangan`, `idtingkat`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(50, 'X-1', 2, 40, '11', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:11:49', 23500, 0),
(51, 'X-2', 2, 40, '12', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:12:31', 44410, 0),
(52, 'X-3', 2, 40, '13', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:12:47', 11733, 0),
(53, 'X-4', 2, 40, '14', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:13:18', 23924, 0),
(54, 'X-5', 2, 40, '15', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:13:49', 20733, 0),
(55, 'X-6', 2, 40, '16', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:14:26', 56257, 0),
(56, 'X-7', 2, 40, '17', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:14:51', 8487, 0),
(57, 'X-8', 2, 40, '18', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:15:13', 15999, 0),
(58, 'X-AKSEL', 2, 40, '19', 1, '', 1, NULL, NULL, NULL, '2013-02-03 21:15:50', 50078, 0),
(61, 'XI-IPA-1', 2, 40, '20', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:18:37', 36533, 0),
(62, 'XI-IPA-2', 2, 40, '21', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:22:03', 23677, 0),
(63, 'XI-IPA-3', 2, 40, '22', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:44:35', 35902, 0),
(64, 'XI-IPA-4', 2, 40, '23', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:44:55', 44510, 0),
(65, 'XI-IPA-5', 2, 40, '24', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:46:00', 5008, 0),
(66, 'XI-IPA-6', 2, 40, '25', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:46:25', 7526, 0),
(67, 'XI-IPA-AKSELERASI', 2, 40, '26', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:46:45', 40155, 0),
(68, 'XI-IPS-1', 2, 40, '27', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:47:27', 36223, 0),
(69, 'XI-IPS-2', 2, 40, '28', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:48:03', 18603, 0),
(70, 'XI-BHS', 2, 40, '29', 1, '', 2, NULL, NULL, NULL, '2013-02-03 21:48:38', 54925, 0),
(71, 'XII-BHS', 2, 40, '29', 1, '', 3, NULL, NULL, NULL, '2013-02-03 21:50:21', 46688, 0),
(80, 'XII-IPA4', 2, 40, '37', 1, '', 3, NULL, NULL, NULL, '2013-02-13 18:27:27', 27438, 0),
(96, 'XII-IPS2', 2, 40, '13', 1, '', 3, NULL, NULL, NULL, '2013-02-13 18:36:16', 60487, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kelompokcalonsiswa`
--

CREATE TABLE IF NOT EXISTS `kelompokcalonsiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kelompok` varchar(100) NOT NULL,
  `idproses` int(10) unsigned NOT NULL,
  `kapasitas` int(10) unsigned NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_kelompokcalonsiswa_prosespenerimaansiswa` (`idproses`),
  KEY `IX_kelompokcalonsiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kelompokcalonsiswa`
--

INSERT INTO `kelompokcalonsiswa` (`replid`, `kelompok`, `idproses`, `kapasitas`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'Akselerasi', 1, 40, '', NULL, NULL, NULL, '2013-03-23 22:22:26', 37738, 0),
(2, 'Reguler', 1, 40, '', NULL, NULL, NULL, '2013-03-23 22:22:39', 55249, 0);

-- --------------------------------------------------------

--
-- Table structure for table `komennap`
--

CREATE TABLE IF NOT EXISTS `komennap` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `idinfo` int(10) unsigned NOT NULL DEFAULT '0',
  `komentar` text NOT NULL,
  `predikat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_komennap_infonap` (`idinfo`),
  KEY `FK_komennap_siswa` (`nis`),
  KEY `IX_komennap_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=491 ;

--
-- Dumping data for table `komennap`
--

INSERT INTO `komennap` (`replid`, `nis`, `idinfo`, `komentar`, `predikat`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, '15819', 2, '<p>Sudah Mampu memahami bahasa indonesia</p>', 4, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(2, '15836', 2, '<p>Sudah Mampu Memahami</p>', 4, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(3, '15855', 2, '', 4, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(4, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(5, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(6, '15887', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(7, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(8, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(9, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(10, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(11, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(12, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(13, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(14, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(15, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(16, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(17, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(18, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(19, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(20, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(21, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(22, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(23, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(24, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(25, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(26, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(27, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(28, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(29, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(30, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(31, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(32, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(33, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(34, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(35, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:26', 0, 0),
(36, '15819', 2, '', 1, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(37, '15836', 2, '', 2, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(38, '15855', 2, '', 2, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(39, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(40, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(41, '15887', 2, '', 1, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(42, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(43, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(44, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(45, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(46, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(47, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(48, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(49, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(50, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(51, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(52, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(53, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(54, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(55, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(56, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(57, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(58, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(59, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(60, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(61, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(62, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(63, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(64, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(65, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(66, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(67, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(68, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(69, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(70, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:33', 0, 0),
(71, '15819', 1, '', 4, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(72, '15836', 1, '', 3, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(73, '15855', 1, '', 3, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(74, '15877', 1, '', 2, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(75, '15885', 1, '', 2, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(76, '15887', 1, '', 2, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(77, '15892', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(78, '15908', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(79, '15922', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(80, '15926', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(81, '15929', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(82, '15937', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(83, '15939', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(84, '15941', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(85, '15943', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(86, '15952', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(87, '15965', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(88, '15966', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(89, '15976', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(90, '15978', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(91, '16007', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(92, '16018', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(93, '16025', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(94, '16032', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(95, '16040', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(96, '16041', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(97, '16043', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(98, '16045', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(99, '16057', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(100, '16063', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(101, '16067', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(102, '16081', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(103, '16085', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(104, '16100', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(105, '16103', 1, '', 0, NULL, NULL, NULL, '2016-04-22 23:26:50', 0, 0),
(106, '15819', 2, '', 4, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(107, '15836', 2, '', 4, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(108, '15855', 2, '', 4, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(109, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(110, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(111, '15887', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(112, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(113, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(114, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(115, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(116, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(117, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(118, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(119, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(120, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(121, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(122, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(123, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(124, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(125, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(126, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(127, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(128, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(129, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(130, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(131, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(132, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(133, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(134, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(135, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(136, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(137, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(138, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(139, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(140, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:35', 0, 0),
(141, '15819', 2, '', 1, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(142, '15836', 2, '', 2, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(143, '15855', 2, '', 2, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(144, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(145, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(146, '15887', 2, '', 1, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(147, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(148, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(149, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(150, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(151, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(152, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(153, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(154, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(155, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(156, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(157, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(158, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(159, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(160, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(161, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(162, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(163, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(164, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(165, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(166, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(167, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(168, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(169, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(170, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(171, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(172, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(173, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(174, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(175, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-23 15:11:41', 0, 0),
(176, '15819', 1, '', 4, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(177, '15836', 1, '', 3, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(178, '15855', 1, '', 3, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(179, '15877', 1, '', 2, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(180, '15885', 1, '', 2, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(181, '15887', 1, '', 2, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(182, '15892', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(183, '15908', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(184, '15922', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(185, '15926', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(186, '15929', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(187, '15937', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(188, '15939', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(189, '15941', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(190, '15943', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(191, '15952', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(192, '15965', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(193, '15966', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(194, '15976', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(195, '15978', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(196, '16007', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(197, '16018', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(198, '16025', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(199, '16032', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(200, '16040', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(201, '16041', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(202, '16043', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(203, '16045', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(204, '16057', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(205, '16063', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(206, '16067', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(207, '16081', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(208, '16085', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(209, '16100', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(210, '16103', 1, '', 0, NULL, NULL, NULL, '2016-04-23 15:13:01', 0, 0),
(211, '15819', 2, '', 1, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(212, '15836', 2, '', 2, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(213, '15855', 2, '', 2, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(214, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(215, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(216, '15887', 2, '', 1, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(217, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(218, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(219, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(220, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(221, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(222, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(223, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(224, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(225, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(226, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(227, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(228, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(229, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(230, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(231, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(232, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(233, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(234, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(235, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(236, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(237, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(238, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(239, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(240, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(241, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(242, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(243, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(244, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(245, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:02:25', 0, 0),
(246, '15819', 2, '', 4, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(247, '15836', 2, '', 4, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(248, '15855', 2, '', 4, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(249, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(250, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(251, '15887', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(252, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(253, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(254, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(255, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(256, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(257, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(258, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(259, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(260, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(261, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(262, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(263, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(264, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(265, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(266, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(267, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(268, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(269, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(270, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(271, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(272, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(273, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(274, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(275, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(276, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(277, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(278, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(279, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(280, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:23', 0, 0),
(281, '15819', 2, '', 1, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(282, '15836', 2, '', 2, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(283, '15855', 2, '', 2, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(284, '15877', 2, '', 3, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(285, '15885', 2, '', 4, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(286, '15887', 2, '', 1, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(287, '15892', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(288, '15908', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(289, '15922', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(290, '15926', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(291, '15929', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(292, '15937', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(293, '15939', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(294, '15941', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(295, '15943', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(296, '15952', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(297, '15965', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(298, '15966', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(299, '15976', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(300, '15978', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(301, '16007', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(302, '16018', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(303, '16025', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(304, '16032', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(305, '16040', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(306, '16041', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(307, '16043', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(308, '16045', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(309, '16057', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(310, '16063', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(311, '16067', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(312, '16081', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(313, '16085', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(314, '16100', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(315, '16103', 2, '', 0, NULL, NULL, NULL, '2016-04-23 16:15:56', 0, 0),
(316, '15819', 1, '', 4, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(317, '15836', 1, '', 3, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(318, '15855', 1, '', 3, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(319, '15877', 1, '', 2, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(320, '15885', 1, '', 2, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(321, '15887', 1, '', 2, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(322, '15892', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(323, '15908', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(324, '15922', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(325, '15926', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(326, '15929', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(327, '15937', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(328, '15939', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(329, '15941', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(330, '15943', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(331, '15952', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(332, '15965', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(333, '15966', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(334, '15976', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(335, '15978', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(336, '16007', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(337, '16018', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(338, '16025', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(339, '16032', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(340, '16040', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(341, '16041', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(342, '16043', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(343, '16045', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(344, '16057', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(345, '16063', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(346, '16067', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(347, '16081', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(348, '16085', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(349, '16100', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(350, '16103', 1, '', 0, NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(351, '15819', 3, '', 1, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(352, '15836', 3, '', 2, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(353, '15855', 3, '', 2, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(354, '15877', 3, '', 3, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(355, '15885', 3, '', 4, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(356, '15887', 3, '', 1, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(357, '15892', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(358, '15908', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(359, '15922', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(360, '15926', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(361, '15929', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(362, '15937', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(363, '15939', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(364, '15941', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(365, '15943', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(366, '15952', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(367, '15965', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(368, '15966', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(369, '15976', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(370, '15978', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(371, '16007', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(372, '16018', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(373, '16025', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(374, '16032', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(375, '16040', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(376, '16041', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(377, '16043', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(378, '16045', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(379, '16057', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(380, '16063', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(381, '16067', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(382, '16081', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(383, '16085', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(384, '16100', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(385, '16103', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:41:11', 0, 0),
(386, '15819', 3, '', 4, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(387, '15836', 3, '', 4, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(388, '15855', 3, '', 4, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(389, '15877', 3, '', 3, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(390, '15885', 3, '', 4, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(391, '15887', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(392, '15892', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(393, '15908', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(394, '15922', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(395, '15926', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(396, '15929', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(397, '15937', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(398, '15939', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(399, '15941', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(400, '15943', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(401, '15952', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(402, '15965', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(403, '15966', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(404, '15976', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(405, '15978', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(406, '16007', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(407, '16018', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(408, '16025', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(409, '16032', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(410, '16040', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(411, '16041', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(412, '16043', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(413, '16045', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(414, '16057', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(415, '16063', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(416, '16067', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(417, '16081', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(418, '16085', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(419, '16100', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(420, '16103', 3, '', 0, NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(421, '15819', 4, '', 4, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(422, '15836', 4, '', 4, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(423, '15855', 4, '', 4, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(424, '15877', 4, '', 3, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(425, '15885', 4, '', 4, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(426, '15887', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(427, '15892', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(428, '15908', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(429, '15922', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(430, '15926', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(431, '15929', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(432, '15937', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(433, '15939', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(434, '15941', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(435, '15943', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(436, '15952', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(437, '15965', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(438, '15966', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(439, '15976', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(440, '15978', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(441, '16007', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(442, '16018', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(443, '16025', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(444, '16032', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(445, '16040', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(446, '16041', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(447, '16043', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(448, '16045', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(449, '16057', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(450, '16063', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(451, '16067', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(452, '16081', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(453, '16085', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(454, '16100', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(455, '16103', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(456, '15819', 4, '', 1, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(457, '15836', 4, '', 2, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(458, '15855', 4, '', 2, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(459, '15877', 4, '', 3, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(460, '15885', 4, '', 4, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(461, '15887', 4, '', 1, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(462, '15892', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(463, '15908', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(464, '15922', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(465, '15926', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(466, '15929', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(467, '15937', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(468, '15939', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(469, '15941', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(470, '15943', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(471, '15952', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(472, '15965', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(473, '15966', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(474, '15976', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(475, '15978', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(476, '16007', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(477, '16018', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(478, '16025', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(479, '16032', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(480, '16040', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(481, '16041', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(482, '16043', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(483, '16045', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(484, '16057', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(485, '16063', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(486, '16067', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(487, '16081', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(488, '16085', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(489, '16100', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(490, '16103', 4, '', 0, NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kondisisiswa`
--

CREATE TABLE IF NOT EXISTS `kondisisiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kondisi` varchar(100) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`kondisi`),
  UNIQUE KEY `UX_kondisisiswa` (`replid`),
  KEY `IX_kondisisiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kondisisiswa`
--

INSERT INTO `kondisisiswa` (`replid`, `kondisi`, `urutan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(8, 'Berkecukupan', 1, NULL, NULL, NULL, '2010-03-02 10:06:17', 30009, 0),
(9, 'Kurang Mampu', 2, NULL, NULL, NULL, '2010-03-02 10:06:17', 54640, 0),
(13, 'Reguler', 0, NULL, NULL, NULL, '2013-02-13 18:01:32', 20561, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kriteriakejadian`
--

CREATE TABLE IF NOT EXISTS `kriteriakejadian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kriteria` varchar(50) NOT NULL DEFAULT '',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_kriteriakejadian_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `kriteriakejadian`
--


-- --------------------------------------------------------

--
-- Table structure for table `kurikulum`
--

CREATE TABLE IF NOT EXISTS `kurikulum` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kurikulum` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_kurikulum_departemen` (`departemen`),
  KEY `IX_kurikulum_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kurikulum`
--

INSERT INTO `kurikulum` (`replid`, `kurikulum`, `departemen`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'KTSP', 'SMAN 1 Malang', '', NULL, NULL, NULL, '2016-03-20 19:49:25', 0, 0),
(2, 'K13', 'SMAN 1 Malang', '', NULL, NULL, NULL, '2016-03-21 19:49:35', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_sikap`
--

CREATE TABLE IF NOT EXISTS `master_sikap` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_sikap` varchar(50) NOT NULL,
  `idkurikulum` int(1) unsigned NOT NULL DEFAULT '1',
  `iddasarpenilaian` varchar(10) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned DEFAULT '0',
  `issync` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jenisujian_pelajaran` (`idkurikulum`),
  KEY `IX_jenisujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `master_sikap`
--

INSERT INTO `master_sikap` (`replid`, `nama_sikap`, `idkurikulum`, `iddasarpenilaian`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(14, 'Deskripsi Sosial 1', 2, 'KI2', 'Sudah menunjukkan kebiasaan berdoa sebelum dan sesudah melaksanakan kegiatan, menjalankan ibadah tepat waktu, memberi salam, bersyukur, menghormati orang lain dalam menjalankan ibadah, dan menjaga kebersihan lingkungan sekolah', '', '', '', '2016-04-23 16:31:04', 0, 0),
(15, 'Deskripsi Spiritual 1', 2, 'KI1', 'Sudah menunjukkan sikap jujur, disiplin, tanggung jawab, santun, peduli, dan percaya diri', '', '', '', '2016-04-23 16:31:04', 0, 0),
(16, 'Deskripsi Afektif', 1, 'AF', 'Isi Deskripsi', NULL, NULL, NULL, '2016-04-23 16:35:21', 0, 0),
(17, 'Deskripsi Spiritual 2', 2, 'KI1', 'Sudah menunjkkan', NULL, NULL, NULL, '2016-04-25 18:00:52', 0, 0),
(18, 'Deskripsi Spiritual 3', 2, 'KI1', 'Coba Deskripsi', NULL, NULL, NULL, '2016-04-25 18:57:55', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` varchar(3) NOT NULL DEFAULT '',
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('0', 'PENGATURAN', '1', '0', '1', '../_main/menu_referensi.php', '', '1', '1'),
('1', 'Header Dokumen', '1', '1', '1', 'referensi/identitas.php', '', '1', '0'),
('10', 'Pengajar Mapel', '3', '1', '3', 'guru/guru_main.php', '', '1', '0'),
('11', 'Data Guru/Pegawai', '2', '1', '2', 'referensi/pegawai.php', '', '1', '0'),
('12', 'Jadwal Guru', '2', '1', '3', 'jadwal/jadwal_guru_main.php', '', '1', '0'),
('13', 'Rekapitulasi Jadwal', '2', '1', '4', 'jadwal/rekap_jadwal_main.php', '', '1', '0'),
('14', 'Lap. Presensi Guru', '2', '1', '5', 'presensi/lap_pengajar_main.php', '', '1', '0'),
('15', 'Lap. Rekapitulasi Absensi Guru', '2', '1', '6', 'presensi/lap_refleksi_main.php', '', '1', '0'),
('16', 'PENGAJARAN', '3', '0', '1', '', '', '1', '1'),
('17', 'Jam belajar', '3', '1', '1', 'jadwal/definisi_jam.php', '', '1', '0'),
('18', 'Jadwal Mapel', '3', '1', '4', 'jadwal/jadwal_kelas_main.php', '', '1', '0'),
('19', 'Data Mapel', '3', '1', '2', 'guru/pelajaran.php', '', '1', '0'),
('2', 'Identitas Sekolah', '1', '1', '2', 'referensi/departemen.php', '', '1', '0'),
('20', 'RPP Guru', '3', '1', '5', 'guru/rpp_main.php', '', '1', '0'),
('21', 'RPP Guru Perkelas', '3', '1', '6', 'penilaian/ujian_rpp_siswa.php', '', '1', '0'),
('22', 'PRESENSI PELAJARAN', '4', '0', '1', '', '', '1', '1'),
('23', 'Presensi Pelajaran', '4', '1', '1', 'presensi/presensi_main.php', '', '1', '0'),
('24', 'Cetak Form Presensi Pelajaran', '4', '1', '2', 'presensi/formpresensi_pelajaran.php', '', '1', '0'),
('25', 'Lap. Absensi Perpelajaran Berdasarkan Kelas', '4', '1', '4', 'presensi/lap_kelas_main.php', '', '1', '0'),
('26', 'Lap. Absensi Perpelajaran Berdasarkan Tingkat', '4', '1', '3', 'presensi/statistik_kelas_main.php', '', '1', '0'),
('27', 'Statistik Kehadiran Siswa Per-Pelajaran', '4', '1', '5', 'presensi/statistik_hariansiswa_main.php', '', '1', '1'),
('28', 'PRESENSI HARIAN', '5', '0', '1', '', '', '1', '1'),
('29', 'Presensi Harian', '5', '1', '1', 'presensi/input_presensi_main.php', '', '1', '0'),
('3', 'Tahun Ajaran', '1', '1', '3', 'referensi/tahunajaran.php', '', '1', '0'),
('30', 'Cetak Form Presensi Harian', '5', '1', '2', 'presensi/formpresensi_harian.php', '', '1', '0'),
('31', 'Lap. Presensi Harian Siswa', '5', '1', '5', 'presensi/lap_hariansiswa_main.php', '', '1', '0'),
('32', 'Lap. Presensi Harian Siswa PerKelas', '5', '1', '3', 'presensi/lap_hariankelas_main.php', '', '1', '0'),
('33', 'Lap. Harian Absensi Siswa Yang Tidak Hadir', '5', '1', '4', 'presensi/lap_harianabsen_main.php', '', '1', '0'),
('34', 'Statistik Kehadiran Siswa', '5', '1', '7', 'presensi/statistik_hariansiswa_main.php', '', '1', '0'),
('35', 'Statistik Kehadiran Siswa PerKelas', '5', '1', '6', 'presensi/statistik_hariankelas_main.php', '', '1', '0'),
('36', 'PENILAIAN', '6', '0', '1', '', '', '1', '1'),
('37', 'Aspek Penilaian', '6', '1', '1', 'guru/aspeknilai.php', '', '1', '0'),
('38', 'Jenis Evaluasi', '6', '1', '2', 'guru/jenis_pengujian.php', '', '1', '0'),
('39', 'Aturan Grading', '6', '1', '3', 'guru/aturannilai_main.php', '', '1', '0'),
('4', 'Semester', '1', '1', '4', 'referensi/semester.php', '', '1', '0'),
('40', 'Bobot Evaluasi', '6', '1', '4', 'guru/perhitungan_rapor.php', '', '1', '0'),
('41', 'Penilaian Pelajaran', '6', '1', '6', 'penilaian/lihat_nilai_pelajaran.php', '', '1', '0'),
('44', 'Cetak Form Penilaian', '6', '1', '7', 'penilaian/formpenilaian.php', '', '1', '0'),
('45', 'DAFTAR NILAI', '7', '0', '1', '', '', '1', '1'),
('46', 'Daftar Nilai Siswa Perpelajaran', '7', '1', '1', 'penilaian/lap_pelajaran_main.php', '', '1', '0'),
('47', 'Laporan Akhir Hasil Belajar Setiap Siswa', '7', '1', '3', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('48', 'Rata-rata Nilai Setiap', '7', '1', '4', 'penilaian/rataus.main.php', '', '1', '0'),
('49', 'Audit Perubahan Nilai', '7', '1', '5', 'referensi/auditnilai.php', '', '1', '0'),
('5', 'Tingkat', '1', '1', '5', 'referensi/tingkat.php', '', '1', '0'),
('50', 'Legger Nilai', '7', '1', '2', 'penilaian/lap_legger.php', '', '1', '0'),
('51', 'RAPOR', '8', '0', '1', '', '', '1', '1'),
('52', 'Penentuan Nilai Rapor', '8', '1', '2', 'penilaian/lihat_penentuan.php', '', '1', '0'),
('53', 'Komentar Rapor', '8', '1', '2', 'penilaian/komentar_main.php', '', '1', '0'),
('54', 'Nilai Rapor Setiap Siswa', '8', '1', '3', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('55', 'Laporan Rapor', '8', '1', '4', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('56', 'Pindah/Naik Kelas', '8', '1', '5', 'siswa/siswa_pindah_main.php', '', '1', '0'),
('57', 'USER', '9', '0', '1', 'usermenu.php', '', '1', '1'),
('58', 'Daftar Pengguna', '9', '1', '1', 'user/user.php', '', '1', '0'),
('59', 'Ganti Password', '9', '1', '2', 'user/user_ganti.php', '', '1', '0'),
('6', 'Angkatan', '1', '1', '6', 'referensi/angkatan.php', '', '1', '0'),
('60', 'Logout', '9', '1', '3', 'logout.php', '', '1', '0'),
('61', 'KENAIKAN & KELULUSAN', '8', '0', '1', 'kelulusan.php', '', '1', '0'),
('62', 'BUKU INDUK', '8', '0', '1', 'penilaian/lap_buku_induk_main.php', NULL, '1', '0'),
('63', 'Master Sikap', '6', '1', '5', 'guru/jenis_sikap.php', '', '1', '0'),
('64', 'Penetuan Sikap', '8', '1', '1', 'penilaian/lihat_penentuan_sikap.php', '', '1', '0'),
('7', 'Kelas', '1', '1', '7', 'referensi/kelas.php', '', '1', '0'),
('8', 'GURU/PEGAWAI', '2', '0', '1', '', '', '1', '1'),
('9', 'Status Guru', '2', '1', '1', 'guru/statusguru.php', '', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `menu2`
--

CREATE TABLE IF NOT EXISTS `menu2` (
  `idParent` varchar(20) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `level` varchar(2) DEFAULT NULL,
  `child` varchar(5) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu2`
--

INSERT INTO `menu2` (`idParent`, `category`, `nama`, `level`, `child`, `url`) VALUES
('1', '1', 'SISWA', '0', '1', 'siswa/siswa.php'),
('1', '2', 'Info Siswa', '1', '0', 'siswa/stat.php'),
('1', '3', 'Statistik Kesiswaan', '1', '0', 'siswa/stat.php'),
('1', '4', 'Statistik Nilai Rapor', '1', '0', 'siswa/statnilai.php'),
('1', '5', 'Rata-Rata Nilai Siswa', '1', '0', 'siswa/rataus.top.php'),
('2', '6', 'KEPEGAWAIAN', '0', '1', NULL),
('2', '7', 'Laporan Presensi Guru', '1', '0', 'pegawai/lappengajar.php'),
('2', '8', 'Statistik Kepegawaian', '1', '0', 'pegawai/statpegawai.php'),
('3', '9', 'KEUANGAN', '0', '1', NULL),
('3', '10', 'Penerimaan', '1', '1', NULL),
('3', '11', 'Laporan Pembayaran Setiap Kelas', '2', '0', 'keu/lapbayarsiswa_kelas.php'),
('3', '12', 'Laporan Pembayaran Setiap Siswa', '2', '0', 'keu/lapbayarsiswa_all.php'),
('3', '13', 'Laporan Tunggakan Siswa', '2', '0', 'keu/lapbayarsiswa_nunggak.php'),
('3', '14', 'Laporan Penerimaan Lain-lain', '2', '0', 'keu/lappenerimaanlain.php'),
('3', '15', 'Jurnal Penerimaan', '2', '0', 'keu/jurnalpenerimaan.php'),
('3', '16', 'Pengeluaran', '1', '1', NULL),
('3', '17', 'Laporan Pengeluaran', '2', '0', 'keu/lappengeluaran.php'),
('3', '18', 'Pencarian data Pengeluaran', '2', '0', 'keu/lappengeluaran_cari.php'),
('3', '19', 'Jurnal Pengeluaran', '2', '0', 'keu/jurnalpengeluaran.php'),
('3', '20', 'Laporan Keuangan', '1', '1', NULL),
('3', '21', 'Laporan Transaksi Keuangan', '2', '0', 'keu/laptransaksi.php'),
('3', '22', 'Laporan Audit Perubahan Data Keuangan', '2', '0', 'keu/lapaudit.php'),
('3', '23', 'Laporan Besar', '2', '0', 'keu/lapbukubesar.php'),
('3', '24', 'Laporan Neraca Percobaan', '2', '0', 'keu/lapneracaper.php'),
('3', '25', 'Laporan Rugi Laba', '2', '0', 'keu/laprugilaba.php'),
('3', '26', 'Laporan Neraca', '2', '0', 'keu/lapneraca.php'),
('3', '27', 'Laporan Perubahan Modal', '2', '0', 'keu/lapmodal.php'),
('3', '28', 'Laporan Arus Kas', '2', '1', 'keu/lapcashflow.php'),
('4', '29', 'Peminjaman', '1', '1', NULL),
('4', '30', 'Daftar Peminjaman', '2', '0', 'pustaka/pjm/daftar.pinjam.php'),
('4', '31', 'Daftar Terlambat', '2', '0', 'pustaka/pjm/daftar.pinjam.telat.php'),
('4', '1', 'PERPUSTAKAAN', '0', '1', NULL),
('4', '3', 'Pengembalian', '1', '1', NULL),
('4', '3', 'Daftar pengembalian', '2', '0', 'pustaka/kbl/daftar.kembali.php'),
('4', '3', 'Daftar Penerimaan Denda', '2', '0', 'pustaka/kbl/daftar.denda.php'),
('4', '4', 'Laporan', '1', '1', NULL),
('4', '4', 'Statistik Peminjaman', '2', '0', 'pustaka/pus/stat.pinjam.php'),
('4', '4', 'Statistik Buku', '2', '0', 'pustaka/pus/stat.pustaka.php'),
('4', '4', 'Statistik Peminjaman Pustaka', '2', '0', 'pustaka/pus/stat.all.php'),
('5', '1', 'PENGGUNA', '0', '1', NULL),
('5', '2', 'Daftar Pengguna', '1', '0', 'pengguna/pengguna.php'),
('5', '3', 'Ganti Password', '1', '0', 'pengguna/gantipassword.php');

-- --------------------------------------------------------

--
-- Table structure for table `menuguru`
--

CREATE TABLE IF NOT EXISTS `menuguru` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menuguru`
--

INSERT INTO `menuguru` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('2', 'Notifikasi', '6', '1', '3', 'http://localhost:4004/man/infoguru/home.php', NULL, '1', '0'),
('3', 'Berita Sekolah', '5', '1', '3', 'http://localhost:4004/man/infoguru/buletin/rubriksekolah/beritasekolah.php', NULL, '1', '0'),
('4', 'Berita Guru', '5', '1', '2', 'http://localhost:4004/man/infoguru/buletin/beritaguru/beritaguru.php', NULL, '1', '0'),
('5', 'Berita Siswa', '5', '1', '4', 'http://localhost:4004/man/infoguru/buletin/beritasiswa/beritasiswa.php', NULL, '1', '0'),
('6', 'Pesan', '6', '1', '2', 'http://localhost:4004/man/infoguru/buletin/pesan/pesan.php', NULL, '1', '0'),
('7', 'Agenda', '6', '1', '4', 'http://localhost:4004/man/infoguru/buletin/agendaguru/agenda.php', NULL, '1', '0'),
('8', 'Galeri Photo', '6', '1', '5', 'http://localhost:4004/man/infoguru/buletin/galerifoto/galerifoto.php', NULL, '1', '0'),
('9', 'File Sharing', '6', '1', '6', 'http://localhost:4004/man/infoguru/buletin/filesharing/main.php', NULL, '1', '0'),
('10', 'Akademik', '1', '0', '1', '', NULL, '1', '1'),
('11', 'Kalender Pendidikan', '1', '1', '5', 'http://localhost:4004/man/infoguru/jadwal/kalender_main.php', NULL, '1', '0'),
('12', 'Jadwal', '1', '1', '2', 'http://localhost:4004/man/infoguru/jadwal.php', NULL, '1', '0'),
('13', 'Pelajaran', '1', '1', '3', 'http://localhost:4004/man/infoguru/pelajaran.php', NULL, '1', '0'),
('14', 'Penilaian', '1', '1', '4', 'http://localhost:4004/man/infoguru/penilaian.php', NULL, '1', '0'),
('15', 'Presensi', '3', '1', '2', 'http://localhost:4004/man/infoguru/presensi.php', NULL, '1', '0'),
('16', 'Siswa', '3', '0', '1', '', NULL, '1', '1'),
('17', 'Kejadian Siswa', '3', '1', '4', 'http://localhost:4004/man/infoguru/catatankejadian.php', NULL, '1', '0'),
('18', 'Perpustakaan', '4', '0', '1', 'http://localhost:4004/man/infoguru/perpustakaan/perpustakaan.php', NULL, '1', '0'),
('19', 'Kepegawaian', '2', '0', '1', '', NULL, '1', '1'),
('20', 'Data Pegawai', '2', '1', '2', 'http://localhost:4004/man/infoguru/pegawai/data.php', NULL, '1', '0'),
('21', 'Struktur Jabatan', '2', '1', '3', 'http://localhost:4004/man/infoguru/pegawai/struktur.php', NULL, '1', '0'),
('22', 'Daftar Urut Kepangkatan', '2', '1', '4', 'http://localhost:4004/man/infoguru/pegawai/dukpangkat.php', NULL, '1', '0'),
('24', 'Ganti Password', '6', '1', '7', 'http://localhost:4004/man/infoguru/frametop.php', NULL, '1', '0'),
('25', 'Keluar', '7', '0', '1', 'http://localhost:4004/man/lms/logout.php', NULL, '1', '1'),
(NULL, 'Cari Siswa', '3', '1', '3', 'http://localhost:4004/man/infoguru/siswa/siswa.php', NULL, '1', '0'),
(NULL, 'Berita', '5', '0', '1', NULL, NULL, '1', '1'),
(NULL, 'Lainya', '6', '0', '1', NULL, NULL, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `menuhumas`
--

CREATE TABLE IF NOT EXISTS `menuhumas` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menuhumas`
--

INSERT INTO `menuhumas` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('32', 'ALUMNI', '6', '0', '1', '', NULL, '1', '1'),
('33', 'Pendataan Alumni', '6', '1', '2', 'siswa/alumni_main.php', NULL, '1', '0'),
('34', 'Daftar Alumni', '6', '1', '3', 'siswa/alumni.php', NULL, '1', '0'),
('35', 'Pencarian Alumni', '6', '1', '4', 'siswa/alumni_cari.php', NULL, '1', '0'),
('36', 'USER', '7', '0', '1', 'usermenu.php', NULL, '1', '1'),
('37', 'Daftar Pengguna', '7', '1', '2', 'user/user.php', NULL, '1', '0'),
('38', 'Ganti Password', '7', '1', '3', 'user/user_ganti.php', NULL, '1', '0'),
('39', 'Logout', '8', '0', '1', 'logout.php', NULL, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `menukesiswaan`
--

CREATE TABLE IF NOT EXISTS `menukesiswaan` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menukesiswaan`
--

INSERT INTO `menukesiswaan` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('0', 'Setting', '1', '0', '1', '../_main/menu_referensi.php', NULL, '1', '1'),
('1', 'Angkatan', '1', '1', '2', 'referensi/angkatan.php', NULL, '1', '0'),
('2', 'Tingkat', '1', '1', '3', 'referensi/tingkat.php', NULL, '1', '0'),
('3', 'Tahun Ajaran', '1', '1', '4', 'referensi/tahunajaran.php', NULL, '1', '0'),
('4', 'Semester', '1', '1', '5', 'referensi/semester.php', NULL, '1', '0'),
('5', 'Kelas', '1', '1', '6', 'referensi/kelas.php', NULL, '1', '0'),
('6', 'PPDB', '2', '0', '1', '', NULL, '1', '1'),
('7', 'Konfigurasi Nama Proses', '2', '1', '2', 'siswa_baru/proses.php', NULL, '1', '0'),
('8', 'Kelompok Proses', '2', '1', '3', 'siswa_baru/kelompok.php', NULL, '1', '0'),
('9', 'Konfigurasi Pendataan PSB', '2', '1', '4', 'siswa_baru/settingpsb_main.php', NULL, '1', '0'),
('10', 'Pendataan Calon Siswa', '2', '1', '5', 'siswa_baru/calon_main.php', NULL, '1', '0'),
('11', 'Cari Calon Siswa', '2', '1', '6', 'siswa_baru/cari_main.php', NULL, '1', '0'),
('12', 'Statistik Penerimaan Siswa', '2', '1', '2', 'siswa_baru/statistik_main.php', NULL, '2', '2'),
('13', 'Penempatan Siswa Baru', '2', '1', '8', 'siswa_baru/penempatan_main.php', NULL, '1', '0'),
('14', 'SISWA', '3', '0', '1', '', NULL, '1', '1'),
('15', 'Pendataan Siswa', '3', '1', '2', 'siswa/siswa_main.php', NULL, '1', '0'),
('16', 'Cari siswa', '3', '1', '3', 'siswa/siswa_cari_main.php', NULL, '1', '0'),
('17', 'Statistik Kesiswaan', '3', '1', '4', 'siswa/siswa_statistik_main.php', NULL, '1', '0'),
('18', 'PIN Siswa dan Orang Tua', '3', '1', '5', 'siswa/pin_main.php', NULL, '1', '0'),
('19', 'MUTASI', '4', '0', '6', '', NULL, '1', '1'),
('20', 'Jenis-Jenis Mutasi', '4', '1', '1', 'mutasi/jenis_mutasi_siswa.php', NULL, '1', '0'),
('21', 'Mutasi Siswa', '4', '1', '2', 'mutasi/mutasi_siswa.php', NULL, '1', '0'),
('22', 'Statistik Mutasi Siswa', '4', '1', '3', 'mutasi/statistik_mutasi_siswa.php', NULL, '1', '0'),
('23', 'Daftar Mutasi Siswa', '4', '1', '4', 'mutasi/daftar_mutasi.php', NULL, '1', '0'),
('24', 'PRESENSI HARIAN', '5', '0', '1', '', NULL, '1', '1'),
('25', 'Input Presensi Harian', '5', '1', '2', 'presensi/input_presensi_main.php', NULL, '1', '0'),
('26', 'Cetak Form Presensi Harian', '5', '1', '3', 'presensi/formpresensi_harian.php', NULL, '1', '0'),
('27', 'Lap. Presensi Harian Siswa', '5', '1', '4', 'presensi/lap_hariansiswa_main.php', NULL, '1', '0'),
('28', 'Lap. Presensi Harian Siswa PerKelas', '5', '1', '5', 'presensi/lap_hariankelas_main.php', NULL, '1', '0'),
('29', 'Lap. Harian Absensi Siswa Yang Tidak Hadir', '5', '1', '6', 'presensi/lap_harianabsen_main.php', NULL, '1', '0'),
('30', 'Statistik Kehadiran Siswa', '5', '1', '7', 'presensi/statistik_hariansiswa_main.php', NULL, '1', '0'),
('31', 'Statistik Kehadiran Siswa PerKelas', '5', '1', '8', 'presensi/statistik_hariankelas_main.php', NULL, '1', '0'),
('32', 'ALUMNI', '6', '0', '1', '', NULL, '1', '1'),
('33', 'Pendataan Alumni', '6', '1', '2', 'siswa/alumni_main.php', NULL, '1', '0'),
('34', 'Daftar Alumni', '6', '1', '3', 'siswa/alumni.php', NULL, '1', '0'),
('35', 'Pencarian Alumni', '6', '1', '4', 'siswa/alumni_cari.php', NULL, '1', '0'),
('36', 'USER', '7', '0', '1', 'usermenu.php', NULL, '1', '1'),
('37', 'Daftar Pengguna', '7', '1', '2', 'user/user.php', NULL, '1', '0'),
('38', 'Ganti Password', '7', '1', '3', 'user/user_ganti.php', NULL, '1', '0'),
('39', 'Logout', '8', '0', '1', 'logout.php', NULL, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `menusiswa`
--

CREATE TABLE IF NOT EXISTS `menusiswa` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenusiswa` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menusiswa`
--

INSERT INTO `menusiswa` (`id`, `nama`, `idmenusiswa`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('1', 'Akademik', NULL, '0', '1', NULL, NULL, '1', '1'),
('2', 'Info Akademik', NULL, '1', '2', 'http://192.168.1.9/lms/infosiswa/infosiswa/infosiswa_content.php', NULL, '1', '0'),
('3', 'Penilaian', NULL, '1', '3', 'http://192.168.1.9/lms/infosiswa/akademik/penilaian/lap_pelajaran_main.php', NULL, '1', '0'),
('4', 'Jadwal Pelajaran', NULL, '1', '4', 'http://192.168.1.9/lms/infosiswa/akademik/jadwal/jadwal_kelas_footer.php', NULL, '1', '0'),
('5', 'Kalender Pendidikan', NULL, '1', '5', 'http://192.168.1.9/lms/infosiswa/akademik/jadwal/kalender_footer.php', NULL, '1', '0'),
('6', 'Perpustakaan', NULL, '0', '1', 'http://192.168.1.9/lms/infosiswa/perpustakaan/perpustakaan.php', NULL, '1', '1'),
('7', 'Berita', NULL, '0', '1', NULL, NULL, '1', '1'),
('8', 'Berita Sekolah', NULL, '1', '2', 'http://192.168.1.9/lms/infosiswa/buletin/rubriksekolah/beritasekolah.php', NULL, '1', '0'),
('9', 'Beritas Siswa', NULL, '1', '3', 'http://192.168.1.9/lms/infosiswa/buletin/beritasiswa/beritasiswa.php', NULL, '1', '0'),
('10', 'Lainya', NULL, '0', '1', NULL, NULL, '1', '1'),
('11', 'Pesan', NULL, '1', '2', 'http://192.168.1.9/lms/infosiswa/buletin/pesan/pesan.php', NULL, '1', '0'),
('12', 'Notifikasi', NULL, '1', '3', 'http://192.168.1.9/lms/infosiswa/framecenter.php', NULL, '1', '0'),
('13', 'Agenda', NULL, '1', '4', 'http://192.168.1.9/lms/infosiswa/buletin/agendasiswa/agenda.php', NULL, '1', '0'),
('14', 'Galery Fhoto', NULL, '1', '5', 'http://192.168.1.9/lms/infosiswa/buletin/galerifoto/galerifoto.php', NULL, '1', '0'),
('15', 'File Sharing', NULL, '1', '6', 'http://192.168.1.9/lms/infosiswa/buletin/filesharing/main.php', NULL, '1', '0'),
('16', 'Keluar', NULL, '0', '1', 'http://localhost/lms/logout.php', NULL, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `menutatib`
--

CREATE TABLE IF NOT EXISTS `menutatib` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menutatib`
--

INSERT INTO `menutatib` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('1', 'PRESENSI HARIAN', '1', '0', '1', NULL, NULL, '1', '1'),
('2', 'Input Presensi Harian', '1', '1', '2', 'presensi/input_presensi_main.php', NULL, '1', '0'),
('3', 'Cetak Form Presensi Harian', '1', '1', '3', 'presensi/formpresensi_harian.php', NULL, '1', '0'),
('4', 'LAPORAN PRESENSI HARIAN', '2', '0', '1', NULL, NULL, '1', '1'),
('5', 'Lap. Presensi Harian Siswa', '2', '1', '2', 'presensi/lap_hariansiswa_main.php', NULL, '1', '0'),
('6', 'Lap. Presensi Harian Siswa PerKelas', '2', '1', '3', 'presensi/lap_hariankelas_main.php', NULL, '1', '0'),
('7', 'Lap. Harian Absensi Siswa Yang Tidak Hadir', '2', '1', '4', 'presensi/lap_harianabsen_main.php', NULL, '1', '0'),
('8', 'PRESENSI PELAJARAN', '3', '0', '1', NULL, NULL, '1', '1'),
('9', 'Input Presensi Pelajaran', '3', '1', '2', 'presensi/presensi_main.php', NULL, '1', '0'),
('10', 'Cetak Form Presensi Pelajaran', '3', '1', '3', 'presensi/formpresensi_pelajaran.php', NULL, '1', '0'),
('11', 'LAPORAN PRESENSI PELAJARAN', '4', '0', '1', NULL, NULL, '1', '1'),
('12', 'Laporan Absensi Perpelajaran Berdasarkan Kelas', '4', '1', '2', 'presensi/lap_kelas_main.php', NULL, '1', '0'),
('13', 'Laporan Absensi Perpelajaran Berdasarkan Tingkat', '4', '1', '3', 'presensi/statistik_kelas_main.php', NULL, '1', '0'),
('14', 'Laporan Presensi Pengajar', '4', '1', '4', 'presensi/lap_pengajar_main.php', NULL, '1', '0'),
('15', 'Laporan Rekapitulasi Absensi Pengajaran', '4', '1', '5', 'presensi/lap_refleksi_main.php', NULL, '1', '0'),
('16', 'STATISTIK ABSENSI', '5', '0', '1', NULL, NULL, '1', '1'),
('17', 'Statistik Kehadiran Siswa', '5', '1', '2', 'presensi/statistik_hariansiswa_main.php', NULL, '1', '0'),
('18', 'Statistik Kehadiran Siswa PerKelas', '5', '1', '3', 'presensi/statistik_hariankelas_main.php', NULL, '1', '0'),
('19', 'Statistik Kehadiran Siswa Per-Pelajaran', '5', '1', '4', 'presensi/statistik_hariansiswa_main.php', NULL, '1', '0'),
('20', 'LAN', '6', '0', '1', '', NULL, '1', '1'),
('21', 'Kategori Pelanggaran', '6', '1', '2', 'pelanggaran/kategori/kategori.php', NULL, '1', '0'),
('22', 'Jenis Pelanggaran', '6', '1', '3', 'pelanggaran/jenis/jenis.php', NULL, '1', '0'),
('23', 'Data Pelanggaran', '6', '1', '4', 'pelanggaran/data_pelanggaran/lap_pelanggaran_main.php', NULL, '1', '0'),
('24', 'User', '7', '0', '1', 'usermenu.php', NULL, '1', '1'),
('25', 'Daftar Pengguna', '7', '1', '2', 'user/user.php', NULL, '1', '0'),
('26', 'Logout', '8', '0', '1', 'logout.php', NULL, '1', '1'),
('26', 'Ganti Password', '7', '1', '3', 'user/user_ganti.php', NULL, '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `menu_copy`
--

CREATE TABLE IF NOT EXISTS `menu_copy` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_copy`
--

INSERT INTO `menu_copy` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('0', 'Setting', '1', '0', '1', '../_main/menu_referensi.php', '', '1', '1'),
('2', 'Pegawai', '1', '1', '3', 'pegawai.php', '', '1', '0'),
('3', 'Identitas', '1', '1', '4', 'identitas.php', '', '1', '0'),
('4', 'Departemen', '1', '1', '5', 'departemen.php', '', '1', '0'),
('5', 'Angkatan', '1', '1', '6', 'angkatan.php', '', '1', '0'),
('6', 'Tingkat', '1', '1', '7', 'tingkat.php', '', '1', '0'),
('7', 'Tahun Ajaran', '1', '1', '8', 'tahunajaran.php', '', '1', '0'),
('8', 'Semester', '1', '1', '9', 'semester.php', '', '1', '0'),
('9', 'Kelas', '1', '1', '10', 'kelas.php', '', '1', '0'),
('10', 'GURU', '2', '0', '1', '', '', '1', '1'),
('11', 'Pendataan Guru', '2', '1', '2', 'guru_main.php', '', '1', '0'),
('12', 'Status Guru', '2', '1', '3', 'statusguru.php', '', '1', '0'),
('13', 'PRESENSI HARIAN', '3', '0', '1', '', '', '1', '1'),
('14', 'Input Presensi Harian', '3', '1', '2', 'input_presensi_main.php', '', '1', '0'),
('15', 'Cetak Form Presensi Harian', '3', '1', '3', 'formpresensi_harian.php', '', '1', '0'),
('16', 'Lap. Presensi Harian Siswa', '3', '1', '4', 'lap_hariansiswa_main.php', '', '1', '0'),
('17', 'Lap. Presensi Harian Siswa PerKelas', '3', '1', '5', 'lap_hariankelas_main.php', '', '1', '0'),
('18', 'Lap. Harian Absensi Siswa Yang Tidak Hadir', '3', '1', '6', 'lap_harianabsen_main.php', '', '1', '0'),
('19', 'Statistik Kehadiran Siswa', '3', '1', '7', 'statistik_hariansiswa_main.php', '', '1', '0'),
('20', 'Statistik Kehadiran Siswa PerKelas', '3', '1', '8', 'statistik_hariankelas_main.php', '', '1', '0'),
('21', 'PRESENSI PELAJARAN', '3', '0', '1', '', '', '1', '1'),
('22', 'Input Presensi Pelajaran', '4', '1', '2', 'presensi_main.php', '', '1', '0'),
('23', 'Cetak Form Presensi Pelajaran', '4', '1', '3', 'formpresensi_pelajaran.php', '', '1', '0'),
('24', 'Laporan Absensi Perpelajaran Berdasarkan Kelas', '4', '1', '4', 'lap_kelas_main.php', '', '1', '0'),
('25', 'Laporan Absensi Perpelajaran Berdasarkan Tingkat', '4', '1', '5', 'statistik_kelas_main.php', '', '1', '0'),
('26', 'Statistik Kehadiran Siswa Per-Pelajaran', '4', '1', '6', 'statistik_hariansiswa_main.php', '', '1', '1'),
('27', 'Laporan Presensi Pengajar', '4', '1', '7', 'lap_pengajar_main.php', '', '1', '0'),
('28', 'Laporan Rekapitulasi Absensi Pengajaran', '4', '1', '8', 'lap_refleksi_main.php', '', '1', '0'),
('29', 'JADWAL', '5', '0', '1', '', '', '1', '1'),
('30', 'Jam belajar', '5', '1', '2', 'jadwal/definisi_jam.php', '', '1', '0'),
('31', 'Jadwal Guru', '5', '1', '3', 'jadwal/jadwal_guru_main.php', '', '1', '0'),
('32', 'Jadwal Kelas', '5', '1', '4', 'jadwal/jadwal_kelas_main.php', '', '1', '0'),
('33', 'Rekap Jadwal', '5', '1', '5', 'jadwal/rekap_jadwal_main.php', '', '1', '0'),
('34', 'PELAJARAN', '6', '0', '1', '', '', '1', '1'),
('35', 'Pendataan pelajaran', '6', '1', '2', 'guru/pelajaran.php', '', '1', '0'),
('36', 'RPP', '6', '1', '3', 'guru/rpp_main.php', '', '1', '0'),
('37', 'Daftar RPP Perkelas', '6', '1', '4', 'penilaian/ujian_rpp_siswa.php', '', '1', '0'),
('38', 'Daftar Nilai Siswa Perpelajaran', '6', '1', '5', 'penilaian/lap_pelajaran_main.php', '', '1', '0'),
('39', 'PENILAIAN', '7', '0', '1', '', '', '1', '1'),
('40', 'Aspek Penilaian', '7', '1', '2', 'guru/aspeknilai.php', '', '1', '0'),
('41', 'Jenis-Jenis Pengujian', '7', '1', '3', 'guru/jenis_pengujian.php', '', '1', '0'),
('42', 'Aturan Grading', '7', '1', '4', 'guru/aturannilai_main.php', '', '1', '0'),
('43', 'Penilaian Pelajaran', '7', '1', '5', 'penilaian/lihat_nilai_pelajaran.php', '', '1', '0'),
('44', 'Cetak Form Penilaian', '7', '1', '6', 'penilaian/formpenilaian.php', '', '1', '0'),
('45', 'Laporan Akhir Hasil Belajar Setiap Siswa', '7', '1', '7', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('46', 'Audit Perubahan Nilai', '7', '1', '8', 'referensi/auditnilai.php', '', '1', '0'),
('47', 'Rata-rata Nilai Setiap', '7', '1', '9', 'penilaian/rataus.main.php', '', '1', '0'),
('48', 'Legger Nilai', '7', '1', '10', 'penilaian/lap_legger.php', '', '1', '0'),
('49', 'RAPOR', '8', '0', '1', '', '', '1', '1'),
('50', 'Perhitungan Nilai Rapor', '8', '1', '2', 'guru/perhitungan_rapor.php', '', '1', '0'),
('51', 'Penentuan Nilai Rapor', '8', '1', '3', 'penilaian/lihat_penentuan.php', '', '1', '0'),
('52', 'Komentar Rapor', '8', '1', '4', 'penilaian/komentar_main.php', '', '1', '0'),
('53', 'Nilai Rapor Setiap Siswa', '8', '1', '5', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('54', 'Laporan Rapor', '8', '1', '6', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('68', 'USER', '15', '0', '1', 'usermenu.php', '', '1', '1'),
('69', 'Daftar Pengguna', '15', '1', '2', 'user/user.php', '', '1', '0'),
('70', 'Ganti Password', '15', '1', '3', 'user/user_ganti.php', '', '1', '0'),
('71', 'LAINYA', '16', '0', '1', '', '', '1', '1'),
('72', 'Pindah Kelas', '16', '1', '2', 'siswa/siswa_pindah_main.php', '', '1', '0'),
('73', 'Logout', '16', '1', '3', 'logout.php', '', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `mutasisiswa`
--

CREATE TABLE IF NOT EXISTS `mutasisiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(15) NOT NULL DEFAULT '',
  `jenismutasi` int(10) unsigned NOT NULL DEFAULT '0',
  `tglmutasi` date NOT NULL DEFAULT '0000-00-00',
  `keterangan` varchar(255) DEFAULT NULL,
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_mutasisiswa_jenismutasi` (`jenismutasi`),
  KEY `FK_mutasisiswa_siswa` (`nis`),
  KEY `FK_mutasisiswa_departemen` (`departemen`),
  KEY `IX_mutasisiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `mutasisiswa`
--


-- --------------------------------------------------------

--
-- Table structure for table `nap`
--

CREATE TABLE IF NOT EXISTS `nap` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL DEFAULT '',
  `idaturan` int(10) unsigned NOT NULL DEFAULT '0',
  `idinfo` int(10) unsigned NOT NULL DEFAULT '0',
  `nilaiangka` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nilaihuruf` varchar(2) NOT NULL DEFAULT '',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_nap_infonap` (`idinfo`),
  KEY `FK_nap_siswa` (`nis`),
  KEY `FK_nap_aturannhb` (`idaturan`),
  KEY `IX_nap_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=246 ;

--
-- Dumping data for table `nap`
--

INSERT INTO `nap` (`replid`, `nis`, `idaturan`, `idinfo`, `nilaiangka`, `nilaihuruf`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(71, '15819', 6, 1, '90.00', 'A', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(72, '15836', 6, 1, '81.50', 'B', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(73, '15855', 6, 1, '73.00', 'B', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(74, '15877', 6, 1, '64.50', 'C', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(75, '15885', 6, 1, '56.00', 'C', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(76, '15887', 6, 1, '68.00', 'C', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(77, '15892', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(78, '15908', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(79, '15922', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(80, '15926', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(81, '15929', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(82, '15937', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(83, '15939', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(84, '15941', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(85, '15943', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(86, '15952', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(87, '15965', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(88, '15966', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(89, '15976', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(90, '15978', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(91, '16007', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(92, '16018', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(93, '16025', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(94, '16032', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(95, '16040', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(96, '16041', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(97, '16043', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(98, '16045', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(99, '16057', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(100, '16063', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(101, '16067', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(102, '16081', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(103, '16085', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(104, '16100', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(105, '16103', 6, 1, '0.00', 'D', NULL, NULL, NULL, '2016-04-25 16:00:35', 0, 0),
(141, '15819', 7, 3, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(142, '15836', 7, 3, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(143, '15855', 7, 3, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(144, '15877', 7, 3, '75.00', 'B', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(145, '15885', 7, 3, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(146, '15887', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(147, '15892', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(148, '15908', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(149, '15922', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(150, '15926', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(151, '15929', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(152, '15937', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(153, '15939', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(154, '15941', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(155, '15943', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(156, '15952', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(157, '15965', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(158, '15966', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(159, '15976', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(160, '15978', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(161, '16007', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(162, '16018', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(163, '16025', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(164, '16032', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(165, '16040', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(166, '16041', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(167, '16043', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(168, '16045', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(169, '16057', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(170, '16063', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(171, '16067', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(172, '16081', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(173, '16085', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(174, '16100', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(175, '16103', 7, 3, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 07:43:06', 0, 0),
(176, '15819', 7, 4, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(177, '15836', 7, 4, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(178, '15855', 7, 4, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(179, '15877', 7, 4, '75.00', 'B', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(180, '15885', 7, 4, '85.00', 'A', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(181, '15887', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(182, '15892', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(183, '15908', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(184, '15922', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(185, '15926', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(186, '15929', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(187, '15937', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(188, '15939', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(189, '15941', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(190, '15943', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(191, '15952', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(192, '15965', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(193, '15966', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(194, '15976', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(195, '15978', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(196, '16007', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(197, '16018', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(198, '16025', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(199, '16032', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(200, '16040', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(201, '16041', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(202, '16043', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(203, '16045', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(204, '16057', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(205, '16063', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(206, '16067', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(207, '16081', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(208, '16085', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(209, '16100', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(210, '16103', 7, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:42', 0, 0),
(211, '15819', 6, 4, '51.20', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(212, '15836', 6, 4, '58.20', 'C', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(213, '15855', 6, 4, '67.90', 'C', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(214, '15877', 6, 4, '77.60', 'B', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(215, '15885', 6, 4, '87.30', 'A', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(216, '15887', 6, 4, '40.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(217, '15892', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(218, '15908', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(219, '15922', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(220, '15926', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(221, '15929', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(222, '15937', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(223, '15939', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(224, '15941', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(225, '15943', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(226, '15952', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(227, '15965', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(228, '15966', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(229, '15976', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(230, '15978', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(231, '16007', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(232, '16018', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(233, '16025', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(234, '16032', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(235, '16040', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(236, '16041', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(237, '16043', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(238, '16045', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(239, '16057', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(240, '16063', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(241, '16067', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(242, '16081', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(243, '16085', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(244, '16100', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0),
(245, '16103', 6, 4, '0.00', 'D', NULL, NULL, NULL, '2016-04-26 08:31:50', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `nau`
--

CREATE TABLE IF NOT EXISTS `nau` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `nis` varchar(20) NOT NULL,
  `idkelas` int(10) unsigned DEFAULT NULL,
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idjenis` int(10) unsigned NOT NULL DEFAULT '0',
  `nilaiAU` decimal(10,2) NOT NULL DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  `idaturan` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_nau_idpelajaran` (`idpelajaran`),
  KEY `FK_nau_nis` (`nis`),
  KEY `FK_nau_idsemester` (`idsemester`),
  KEY `FK_nau_idjenis` (`idjenis`),
  KEY `FK_nau_idaturan` (`idaturan`),
  KEY `FK_nau_kelas` (`idkelas`),
  KEY `IX_nau_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=456 ;

--
-- Dumping data for table `nau`
--

INSERT INTO `nau` (`replid`, `idpelajaran`, `nis`, `idkelas`, `idsemester`, `idjenis`, `nilaiAU`, `keterangan`, `idaturan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(141, 47, '15819', 50, 1, 11, '54.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(142, 47, '15836', 50, 1, 11, '54.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(143, 47, '15855', 50, 1, 11, '63.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(144, 47, '15877', 50, 1, 11, '72.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(145, 47, '15885', 50, 1, 11, '81.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(146, 47, '15887', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(147, 47, '15892', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(148, 47, '15908', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(149, 47, '15922', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(150, 47, '15926', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(151, 47, '15929', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(152, 47, '15937', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(153, 47, '15939', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(154, 47, '15941', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(155, 47, '15943', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(156, 47, '15952', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(157, 47, '15965', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(158, 47, '15966', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(159, 47, '15976', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(160, 47, '15978', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(161, 47, '16007', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(162, 47, '16018', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(163, 47, '16025', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(164, 47, '16032', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(165, 47, '16040', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(166, 47, '16041', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(167, 47, '16043', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(168, 47, '16045', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(169, 47, '16057', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(170, 47, '16063', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(171, 47, '16067', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(172, 47, '16081', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(173, 47, '16085', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(174, 47, '16100', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(175, 47, '16103', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:52:51', 0, 0),
(211, 47, '15819', 50, 1, 12, '50.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(212, 47, '15836', 50, 1, 12, '60.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(213, 47, '15855', 50, 1, 12, '70.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(214, 47, '15877', 50, 1, 12, '80.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(215, 47, '15885', 50, 1, 12, '90.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(216, 47, '15887', 50, 1, 12, '100.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(217, 47, '15892', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(218, 47, '15908', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(219, 47, '15922', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(220, 47, '15926', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(221, 47, '15929', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(222, 47, '15937', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(223, 47, '15939', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(224, 47, '15941', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(225, 47, '15943', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(226, 47, '15952', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(227, 47, '15965', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(228, 47, '15966', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(229, 47, '15976', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(230, 47, '15978', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(231, 47, '16007', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(232, 47, '16018', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(233, 47, '16025', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(234, 47, '16032', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(235, 47, '16040', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(236, 47, '16041', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(237, 47, '16043', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(238, 47, '16045', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(239, 47, '16057', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(240, 47, '16063', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(241, 47, '16067', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(242, 47, '16081', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(243, 47, '16085', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(244, 47, '16100', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(245, 47, '16103', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:54:14', 0, 0),
(281, 45, '15819', 50, 1, 10, '90.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(282, 45, '15836', 50, 1, 10, '85.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(283, 45, '15855', 50, 1, 10, '80.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(284, 45, '15877', 50, 1, 10, '75.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(285, 45, '15885', 50, 1, 10, '70.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(286, 45, '15887', 50, 1, 10, '70.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(287, 45, '15892', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(288, 45, '15908', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(289, 45, '15922', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(290, 45, '15926', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(291, 45, '15929', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(292, 45, '15937', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(293, 45, '15939', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(294, 45, '15941', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(295, 45, '15943', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(296, 45, '15952', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(297, 45, '15965', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(298, 45, '15966', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(299, 45, '15976', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(300, 45, '15978', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(301, 45, '16007', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(302, 45, '16018', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(303, 45, '16025', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(304, 45, '16032', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(305, 45, '16040', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(306, 45, '16041', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(307, 45, '16043', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(308, 45, '16045', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(309, 45, '16057', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(310, 45, '16063', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(311, 45, '16067', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(312, 45, '16081', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(313, 45, '16085', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(314, 45, '16100', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(315, 45, '16103', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 11:55:19', 0, 0),
(316, 45, '15819', 50, 1, 11, '90.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(317, 45, '15836', 50, 1, 11, '80.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(318, 45, '15855', 50, 1, 11, '70.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(319, 45, '15877', 50, 1, 11, '60.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(320, 45, '15885', 50, 1, 11, '50.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(321, 45, '15887', 50, 1, 11, '90.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(322, 45, '15892', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(323, 45, '15908', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(324, 45, '15922', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(325, 45, '15926', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(326, 45, '15929', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(327, 45, '15937', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(328, 45, '15939', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(329, 45, '15941', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(330, 45, '15943', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(331, 45, '15952', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(332, 45, '15965', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(333, 45, '15966', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(334, 45, '15976', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(335, 45, '15978', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(336, 45, '16007', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(337, 45, '16018', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(338, 45, '16025', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(339, 45, '16032', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(340, 45, '16040', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(341, 45, '16041', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(342, 45, '16043', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(343, 45, '16045', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(344, 45, '16057', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(345, 45, '16063', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(346, 45, '16067', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(347, 45, '16081', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(348, 45, '16085', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(349, 45, '16100', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(350, 45, '16103', 50, 1, 11, '0.00', NULL, 16, NULL, NULL, NULL, '2016-04-22 11:55:35', 0, 0),
(351, 45, '15819', 50, 1, 12, '90.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(352, 45, '15836', 50, 1, 12, '80.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(353, 45, '15855', 50, 1, 12, '70.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(354, 45, '15877', 50, 1, 12, '60.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(355, 45, '15885', 50, 1, 12, '50.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(356, 45, '15887', 50, 1, 12, '50.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(357, 45, '15892', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(358, 45, '15908', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(359, 45, '15922', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(360, 45, '15926', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(361, 45, '15929', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(362, 45, '15937', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(363, 45, '15939', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(364, 45, '15941', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(365, 45, '15943', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(366, 45, '15952', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(367, 45, '15965', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(368, 45, '15966', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(369, 45, '15976', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(370, 45, '15978', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(371, 45, '16007', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(372, 45, '16018', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(373, 45, '16025', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(374, 45, '16032', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(375, 45, '16040', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(376, 45, '16041', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(377, 45, '16043', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(378, 45, '16045', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(379, 45, '16057', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(380, 45, '16063', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(381, 45, '16067', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(382, 45, '16081', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(383, 45, '16085', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(384, 45, '16100', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(385, 45, '16103', 50, 1, 12, '0.00', NULL, 17, NULL, NULL, NULL, '2016-04-22 11:55:48', 0, 0),
(386, 47, '15819', 50, 1, 10, '50.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(387, 47, '15836', 50, 1, 10, '60.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(388, 47, '15855', 50, 1, 10, '70.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(389, 47, '15877', 50, 1, 10, '80.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(390, 47, '15885', 50, 1, 10, '90.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(391, 47, '15887', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(392, 47, '15892', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(393, 47, '15908', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(394, 47, '15922', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(395, 47, '15926', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(396, 47, '15929', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(397, 47, '15937', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(398, 47, '15939', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(399, 47, '15941', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(400, 47, '15943', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(401, 47, '15952', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(402, 47, '15965', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(403, 47, '15966', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(404, 47, '15976', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(405, 47, '15978', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(406, 47, '16007', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(407, 47, '16018', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(408, 47, '16025', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(409, 47, '16032', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(410, 47, '16040', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(411, 47, '16041', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(412, 47, '16043', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(413, 47, '16045', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(414, 47, '16057', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(415, 47, '16063', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(416, 47, '16067', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(417, 47, '16081', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(418, 47, '16085', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(419, 47, '16100', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(420, 47, '16103', 50, 1, 10, '0.00', NULL, 15, NULL, NULL, NULL, '2016-04-22 12:00:21', 0, 0),
(421, 47, '15819', 50, 1, 13, '85.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(422, 47, '15836', 50, 1, 13, '85.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(423, 47, '15855', 50, 1, 13, '85.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(424, 47, '15877', 50, 1, 13, '75.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(425, 47, '15885', 50, 1, 13, '85.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(426, 47, '15887', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(427, 47, '15892', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(428, 47, '15908', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(429, 47, '15922', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(430, 47, '15926', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(431, 47, '15929', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(432, 47, '15937', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(433, 47, '15939', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(434, 47, '15941', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(435, 47, '15943', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(436, 47, '15952', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(437, 47, '15965', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(438, 47, '15966', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(439, 47, '15976', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(440, 47, '15978', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(441, 47, '16007', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(442, 47, '16018', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(443, 47, '16025', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(444, 47, '16032', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(445, 47, '16040', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(446, 47, '16041', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(447, 47, '16043', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(448, 47, '16045', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(449, 47, '16057', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(450, 47, '16063', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(451, 47, '16067', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(452, 47, '16081', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(453, 47, '16085', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(454, 47, '16100', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0),
(455, 47, '16103', 50, 1, 13, '0.00', NULL, 18, NULL, NULL, NULL, '2016-04-22 12:32:46', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `nilaiujian`
--

CREATE TABLE IF NOT EXISTS `nilaiujian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idujian` int(10) unsigned NOT NULL DEFAULT '0',
  `nis` varchar(20) NOT NULL,
  `nilaiujian` decimal(10,2) NOT NULL DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_nilaiujian_idujian` (`idujian`),
  KEY `FK_nilaiujian_nis` (`nis`),
  KEY `IX_nilaiujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=421 ;

--
-- Dumping data for table `nilaiujian`
--

INSERT INTO `nilaiujian` (`replid`, `idujian`, `nis`, `nilaiujian`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 1, '15819', '90.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(2, 1, '15836', '80.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(3, 1, '15855', '70.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(4, 1, '15877', '60.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(5, 1, '15885', '50.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(6, 1, '15887', '50.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(7, 1, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(8, 1, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(9, 1, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(10, 1, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(11, 1, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(12, 1, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(13, 1, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(14, 1, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(15, 1, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(16, 1, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(17, 1, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(18, 1, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(19, 1, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(20, 1, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(21, 1, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(22, 1, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(23, 1, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(24, 1, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(25, 1, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(26, 1, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(27, 1, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(28, 1, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(29, 1, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(30, 1, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(31, 1, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(32, 1, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(33, 1, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(34, 1, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(35, 1, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(36, 2, '15819', '90.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(37, 2, '15836', '80.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(38, 2, '15855', '70.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(39, 2, '15877', '60.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(40, 2, '15885', '50.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(41, 2, '15887', '90.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(42, 2, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(43, 2, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(44, 2, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(45, 2, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(46, 2, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(47, 2, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(48, 2, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(49, 2, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(50, 2, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(51, 2, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(52, 2, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(53, 2, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(54, 2, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(55, 2, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(56, 2, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(57, 2, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(58, 2, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(59, 2, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(60, 2, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(61, 2, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(62, 2, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(63, 2, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(64, 2, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(65, 2, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(66, 2, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(67, 2, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(68, 2, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(69, 2, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(70, 2, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(106, 4, '15819', '90.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(107, 4, '15836', '80.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(108, 4, '15855', '70.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(109, 4, '15877', '60.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(110, 4, '15885', '50.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(111, 4, '15887', '50.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(112, 4, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(113, 4, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(114, 4, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(115, 4, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(116, 4, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(117, 4, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(118, 4, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(119, 4, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(120, 4, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(121, 4, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(122, 4, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(123, 4, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(124, 4, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(125, 4, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(126, 4, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(127, 4, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(128, 4, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(129, 4, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(130, 4, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(131, 4, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(132, 4, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(133, 4, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(134, 4, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(135, 4, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(136, 4, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(137, 4, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(138, 4, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(139, 4, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(140, 4, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(141, 5, '15819', '50.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(142, 5, '15836', '60.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(143, 5, '15855', '70.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(144, 5, '15877', '80.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(145, 5, '15885', '90.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(146, 5, '15887', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(147, 5, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(148, 5, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(149, 5, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(150, 5, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(151, 5, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(152, 5, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(153, 5, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(154, 5, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(155, 5, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(156, 5, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(157, 5, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(158, 5, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(159, 5, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(160, 5, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(161, 5, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(162, 5, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(163, 5, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(164, 5, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(165, 5, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(166, 5, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(167, 5, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(168, 5, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(169, 5, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(170, 5, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(171, 5, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(172, 5, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(173, 5, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(174, 5, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(175, 5, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(176, 6, '15819', '50.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(177, 6, '15836', '60.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(178, 6, '15855', '70.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(179, 6, '15877', '80.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(180, 6, '15885', '90.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(181, 6, '15887', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(182, 6, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(183, 6, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(184, 6, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(185, 6, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(186, 6, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(187, 6, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(188, 6, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(189, 6, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(190, 6, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(191, 6, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(192, 6, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(193, 6, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(194, 6, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(195, 6, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(196, 6, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(197, 6, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(198, 6, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(199, 6, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(200, 6, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(201, 6, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(202, 6, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(203, 6, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(204, 6, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(205, 6, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(206, 6, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(207, 6, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(208, 6, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(209, 6, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(210, 6, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(211, 7, '15819', '50.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(212, 7, '15836', '60.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(213, 7, '15855', '70.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(214, 7, '15877', '80.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(215, 7, '15885', '90.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(216, 7, '15887', '100.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(217, 7, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(218, 7, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(219, 7, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(220, 7, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(221, 7, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(222, 7, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(223, 7, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(224, 7, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(225, 7, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(226, 7, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(227, 7, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(228, 7, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(229, 7, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(230, 7, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(231, 7, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(232, 7, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(233, 7, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(234, 7, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(235, 7, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(236, 7, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(237, 7, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(238, 7, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(239, 7, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(240, 7, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(241, 7, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(242, 7, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(243, 7, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(244, 7, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(245, 7, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(281, 9, '15819', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(282, 9, '15836', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(283, 9, '15855', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(284, 9, '15877', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(285, 9, '15885', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(286, 9, '15887', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(287, 9, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(288, 9, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(289, 9, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(290, 9, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(291, 9, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(292, 9, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(293, 9, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(294, 9, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(295, 9, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(296, 9, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(297, 9, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(298, 9, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(299, 9, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(300, 9, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(301, 9, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(302, 9, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(303, 9, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(304, 9, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(305, 9, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(306, 9, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(307, 9, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(308, 9, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(309, 9, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(310, 9, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(311, 9, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(312, 9, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(313, 9, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(314, 9, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(315, 9, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(316, 10, '15819', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(317, 10, '15836', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(318, 10, '15855', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(319, 10, '15877', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(320, 10, '15885', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(321, 10, '15887', '90.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(322, 10, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(323, 10, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(324, 10, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(325, 10, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(326, 10, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(327, 10, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(328, 10, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(329, 10, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(330, 10, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(331, 10, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(332, 10, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(333, 10, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(334, 10, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(335, 10, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(336, 10, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(337, 10, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(338, 10, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(339, 10, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(340, 10, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(341, 10, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(342, 10, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(343, 10, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(344, 10, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(345, 10, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(346, 10, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(347, 10, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(348, 10, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(349, 10, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(350, 10, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(351, 11, '15819', '90.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(352, 11, '15836', '90.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(353, 11, '15855', '90.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(354, 11, '15877', '80.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(355, 11, '15885', '80.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(356, 11, '15887', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(357, 11, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(358, 11, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(359, 11, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(360, 11, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(361, 11, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(362, 11, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(363, 11, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(364, 11, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(365, 11, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(366, 11, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(367, 11, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(368, 11, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(369, 11, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(370, 11, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(371, 11, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(372, 11, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(373, 11, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(374, 11, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(375, 11, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(376, 11, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(377, 11, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(378, 11, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(379, 11, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(380, 11, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(381, 11, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(382, 11, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(383, 11, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(384, 11, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(385, 11, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(386, 12, '15819', '80.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(387, 12, '15836', '80.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(388, 12, '15855', '80.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(389, 12, '15877', '70.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(390, 12, '15885', '90.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(391, 12, '15887', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(392, 12, '15892', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(393, 12, '15908', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(394, 12, '15922', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(395, 12, '15926', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(396, 12, '15929', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(397, 12, '15937', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(398, 12, '15939', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(399, 12, '15941', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(400, 12, '15943', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(401, 12, '15952', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(402, 12, '15965', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(403, 12, '15966', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(404, 12, '15976', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(405, 12, '15978', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(406, 12, '16007', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(407, 12, '16018', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(408, 12, '16025', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(409, 12, '16032', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(410, 12, '16040', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(411, 12, '16041', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(412, 12, '16043', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(413, 12, '16045', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(414, 12, '16057', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(415, 12, '16063', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(416, 12, '16067', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(417, 12, '16081', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(418, 12, '16085', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(419, 12, '16100', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0),
(420, 12, '16103', '0.00', '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE IF NOT EXISTS `pelajaran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `sifat_ktsp` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sifat_k13` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_daftarpelajaran_kode` (`kode`),
  KEY `FK_pelajaran_departemen` (`departemen`),
  KEY `IX_pelajaran_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`replid`, `kode`, `nama`, `departemen`, `sifat_ktsp`, `sifat_k13`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(45, 'FIS', 'FISIKA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-04-22 19:49:30', 52115, 0),
(46, 'MAT', 'Matematika', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-02 11:28:24', 33723, 0),
(47, 'BIND', 'BAHASA DAN SASTRA INDONESIA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:26:13', 2364, 0),
(48, 'OR', 'PENDIDIKAN JASMANI DAN KESEHATAN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:06:39', 49741, 0),
(49, 'SEN', 'SENI BUDAYA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:09:33', 18143, 0),
(50, 'BIG', 'BAHASA INGGRIS', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:25:53', 61297, 0),
(51, 'TIK', 'Teknik Informatika', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-02 11:29:39', 6305, 0),
(52, 'PKN', 'PKN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:04:07', 7666, 0),
(53, 'PAI', 'PAI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:03:45', 28139, 0),
(54, 'PAKR', 'PENDIDIKAN AGAMA KRISTEN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:04:39', 49479, 0),
(55, 'PAKA', 'PENDIDIKAN AGAMA KATOLIK', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:05:05', 10473, 0),
(56, 'SEJ', 'SEJARAH', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:06:02', 21886, 0),
(57, 'KIM', 'KIMIA', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:02', 56879, 0),
(58, 'BIO', 'BIOLOGI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:17', 9893, 0),
(59, 'AKUN', 'AKUNTANSI', 'SMAN 1 Malang', 1, 0, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:29', 20225, 0),
(60, 'GEO', 'GEOGRAFI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:07:43', 41278, 0),
(61, 'ASTR', 'ASTRONOMI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:08:08', 2227, 0),
(62, 'ANTR', 'ANTROPOLOGI', 'SMAN 1 Malang', 1, 0, 1, 'test', NULL, NULL, NULL, '2013-02-03 22:08:26', 9366, 0),
(63, 'JEP', 'BAHASA JEPANG', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:08:44', 24135, 0),
(64, 'PRC', 'BAHASA PERANCIS', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:09:03', 5797, 0),
(65, 'MDR', 'BAHASA MANDARIN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:09:17', 33173, 0),
(66, 'JER', 'BAHASA JERMAN', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:10:14', 25682, 0),
(67, 'ARB', 'BAHASA ARAB', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:10:32', 35580, 0),
(68, 'SOS', 'SOSIOLOGI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:11:12', 29486, 0),
(70, 'EKO', 'EKONOMI', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-03 22:40:32', 64095, 0),
(71, 'BK', 'Bimbingan Konseling', 'SMAN 1 Malang', 0, 1, 1, '', NULL, NULL, NULL, '2013-02-13 18:58:22', 40584, 0),
(72, 'KW', 'Kewirausahaan', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2013-02-13 19:18:49', 47795, 0),
(73, 'NEW', 'New2', 'SMAN 1 Malang', 1, 1, 1, '', NULL, NULL, NULL, '2016-03-21 15:32:55', 0, 0),
(74, 'Coba', 'Coba Campuran', 'SMAN 1 Malang', 0, 1, 1, '', NULL, NULL, NULL, '2016-03-22 15:43:23', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `petasekolah`
--

CREATE TABLE IF NOT EXISTS `petasekolah` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idsekolah` int(10) unsigned NOT NULL,
  `idwilayah` int(10) unsigned DEFAULT NULL,
  `koordinat` varchar(100) DEFAULT NULL,
  `gambar` blob,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peta_wilayah` (`idwilayah`),
  KEY `FK_peta_sekolah` (`idsekolah`),
  KEY `IX_petasekolah_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `petasekolah`
--


-- --------------------------------------------------------

--
-- Table structure for table `phsiswa`
--

CREATE TABLE IF NOT EXISTS `phsiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpresensi` int(10) unsigned NOT NULL,
  `nis` varchar(20) NOT NULL,
  `hadir` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ijin` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sakit` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cuti` smallint(5) unsigned NOT NULL DEFAULT '0',
  `alpa` smallint(5) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_phsiswa_siswa` (`nis`),
  KEY `FK_phsiswa_presensiharian` (`idpresensi`),
  KEY `IX_phsiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=363 ;

--
-- Dumping data for table `phsiswa`
--

INSERT INTO `phsiswa` (`replid`, `idpresensi`, `nis`, `hadir`, `ijin`, `sakit`, `cuti`, `alpa`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 1, '15810', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 9124, 0),
(2, 1, '15819', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 58330, 0),
(3, 1, '15836', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 2160, 0),
(4, 1, '15855', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 32398, 0),
(5, 1, '15877', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 24449, 0),
(6, 1, '15885', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 25052, 0),
(7, 1, '15887', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 51911, 0),
(8, 1, '15892', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 53338, 0),
(9, 1, '15908', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 45429, 0),
(10, 1, '15922', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 1599, 0),
(11, 1, '15926', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 2767, 0),
(12, 1, '15929', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 9037, 0),
(13, 1, '15937', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 36882, 0),
(14, 1, '15939', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 26241, 0),
(15, 1, '15941', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 20558, 0),
(16, 1, '15943', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 24065, 0),
(17, 1, '15952', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 58651, 0),
(18, 1, '15965', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 24470, 0),
(19, 1, '15966', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 11926, 0),
(20, 1, '15976', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 51750, 0),
(21, 1, '15978', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 26380, 0),
(22, 1, '16007', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 42182, 0),
(23, 1, '16018', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 708, 0),
(24, 1, '16025', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 8056, 0),
(25, 1, '16032', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 38153, 0),
(26, 1, '16040', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 35536, 0),
(27, 1, '16041', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 63221, 0),
(28, 1, '16043', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 12907, 0),
(29, 1, '16045', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 5931, 0),
(30, 1, '16057', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 56463, 0),
(31, 1, '16063', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 2400, 0),
(32, 1, '16067', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 39203, 0),
(33, 1, '16081', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 57754, 0),
(34, 1, '16085', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 40099, 0),
(35, 1, '16100', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 27232, 0),
(36, 1, '16103', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-04 00:41:53', 15863, 0),
(37, 2, '15180', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 34768, 0),
(38, 2, '15188', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 53035, 0),
(39, 2, '15195', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 29811, 0),
(40, 2, '15203', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 55481, 0),
(41, 2, '15204', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 56909, 0),
(42, 2, '15212', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 52574, 0),
(43, 2, '15220', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 26612, 0),
(44, 2, '15226', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 40868, 0),
(45, 2, '15232', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 58972, 0),
(46, 2, '15242', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 41198, 0),
(47, 2, '15244', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 29071, 0),
(48, 2, '15258', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 21763, 0),
(49, 2, '15259', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 21602, 0),
(50, 2, '15264', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 42720, 0),
(51, 2, '15267', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 17732, 0),
(52, 2, '15277', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 26031, 0),
(53, 2, '15278', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 11427, 0),
(54, 2, '15287', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 44573, 0),
(55, 2, '15291', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 57523, 0),
(56, 2, '15294', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 22834, 0),
(57, 2, '15299', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 7131, 0),
(58, 2, '15301', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 32685, 0),
(59, 2, '15307', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 10969, 0),
(60, 2, '15312', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 22321, 0),
(61, 2, '15313', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 13169, 0),
(62, 2, '15320', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 64410, 0),
(63, 2, '15359', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 20423, 0),
(64, 2, '15367', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 39943, 0),
(65, 2, '15371', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 7385, 0),
(66, 2, '15403', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 48155, 0),
(67, 2, '15413', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 22031, 0),
(68, 2, '15491', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 31219, 0),
(69, 2, '15420', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 24472, 0),
(70, 2, '15423', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 28704, 0),
(71, 2, '15426', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 4571, 0),
(72, 2, '15447', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 2275, 0),
(73, 2, '15452', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 63188, 0),
(74, 2, '15458', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 46998, 0),
(75, 2, '15462', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2013-02-13 19:40:03', 45424, 0),
(115, 1, '15810', 17, 1, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(116, 1, '15819', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(117, 1, '15836', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(118, 1, '15855', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(119, 1, '15877', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(120, 1, '15885', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(121, 1, '15887', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(122, 1, '15892', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(123, 1, '15908', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(124, 1, '15922', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(125, 1, '15926', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(126, 1, '15929', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(127, 1, '15937', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(128, 1, '15939', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(129, 1, '15941', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(130, 1, '15943', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(131, 1, '15952', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(132, 1, '15965', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(133, 1, '15966', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(134, 1, '15976', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(135, 1, '15978', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(136, 1, '16007', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(137, 1, '16018', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(138, 1, '16025', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(139, 1, '16032', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(140, 1, '16040', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(141, 1, '16041', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(142, 1, '16043', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(143, 1, '16045', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(144, 1, '16057', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(145, 1, '16063', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(146, 1, '16067', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(147, 1, '16081', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(148, 1, '16085', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(149, 1, '16100', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(150, 1, '16103', 18, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(151, 2, '12345', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(152, 2, '15819', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(153, 2, '15836', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(154, 2, '15855', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(155, 2, '15877', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(156, 2, '15885', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(157, 2, '15887', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(158, 2, '15892', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(159, 2, '15908', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(160, 2, '15922', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(161, 2, '15926', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(162, 2, '15929', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(163, 2, '15937', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(164, 2, '15939', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(165, 2, '15941', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(166, 2, '15943', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(167, 2, '15952', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(168, 2, '15965', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(169, 2, '15966', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(170, 2, '15976', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(171, 2, '15978', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(172, 2, '16007', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(173, 2, '16018', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(174, 2, '16025', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(175, 2, '16032', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(176, 2, '16040', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(177, 2, '16041', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(178, 2, '16043', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(179, 2, '16045', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(180, 2, '16057', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(181, 2, '16063', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(182, 2, '16067', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(183, 2, '16081', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(184, 2, '16085', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(185, 2, '16100', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(186, 2, '16103', 1, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(222, 4, '15805', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(223, 4, '15816', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(224, 4, '15822', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(225, 4, '15841', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(226, 4, '15867', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(227, 4, '15883', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(228, 4, '15888', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(229, 4, '15890', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(230, 4, '15895', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(231, 4, '15897', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(232, 4, '15898', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(233, 4, '15906', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(234, 4, '15909', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(235, 4, '15913', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(236, 4, '15915', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(237, 4, '15931', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(238, 4, '15940', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(239, 4, '15950', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(240, 4, '15958', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(241, 4, '15968', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(242, 4, '15977', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(243, 4, '15982', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(244, 4, '15986', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(245, 4, '16001', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(246, 4, '16002', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(247, 4, '16026', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(248, 4, '16042', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(249, 4, '16046', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(250, 4, '16054', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(251, 4, '16069', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(252, 4, '16070', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(253, 4, '16073', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(254, 4, '16077', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(255, 4, '16083', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(256, 4, '16093', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(257, 4, '16105', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(258, 5, '15819', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(259, 5, '15836', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(260, 5, '15855', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(261, 5, '15877', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(262, 5, '15885', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(263, 5, '15887', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(264, 5, '15892', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(265, 5, '15908', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(266, 5, '15922', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(267, 5, '15926', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(268, 5, '15929', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(269, 5, '15937', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(270, 5, '15939', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(271, 5, '15941', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(272, 5, '15943', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(273, 5, '15952', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(274, 5, '15965', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(275, 5, '15966', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(276, 5, '15976', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(277, 5, '15978', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(278, 5, '16007', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(279, 5, '16018', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(280, 5, '16025', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(281, 5, '16032', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(282, 5, '16040', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(283, 5, '16041', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(284, 5, '16043', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(285, 5, '16045', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(286, 5, '16057', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(287, 5, '16063', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(288, 5, '16067', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(289, 5, '16081', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(290, 5, '16085', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(291, 5, '16100', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(292, 5, '16103', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(293, 6, '15819', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(294, 6, '15836', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(295, 6, '15855', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(296, 6, '15877', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(297, 6, '15885', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(298, 6, '15887', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(299, 6, '15892', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(300, 6, '15908', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(301, 6, '15922', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(302, 6, '15926', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(303, 6, '15929', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(304, 6, '15937', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(305, 6, '15939', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(306, 6, '15941', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(307, 6, '15943', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(308, 6, '15952', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(309, 6, '15965', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(310, 6, '15966', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(311, 6, '15976', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(312, 6, '15978', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(313, 6, '16007', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(314, 6, '16018', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(315, 6, '16025', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(316, 6, '16032', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(317, 6, '16040', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(318, 6, '16041', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(319, 6, '16043', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(320, 6, '16045', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(321, 6, '16057', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(322, 6, '16063', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(323, 6, '16067', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(324, 6, '16081', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(325, 6, '16085', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(326, 6, '16100', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(327, 6, '16103', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(328, 7, '15819', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(329, 7, '15836', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(330, 7, '15855', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(331, 7, '15877', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(332, 7, '15885', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(333, 7, '15887', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(334, 7, '15892', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(335, 7, '15908', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(336, 7, '15922', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(337, 7, '15926', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(338, 7, '15929', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(339, 7, '15937', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(340, 7, '15939', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(341, 7, '15941', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(342, 7, '15943', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(343, 7, '15952', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(344, 7, '15965', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(345, 7, '15966', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(346, 7, '15976', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(347, 7, '15978', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(348, 7, '16007', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(349, 7, '16018', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(350, 7, '16025', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(351, 7, '16032', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(352, 7, '16040', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(353, 7, '16041', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(354, 7, '16043', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(355, 7, '16045', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(356, 7, '16057', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(357, 7, '16063', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(358, 7, '16067', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(359, 7, '16081', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(360, 7, '16085', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(361, 7, '16100', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0),
(362, 7, '16103', 6, 0, 0, 0, 0, '', NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ppsiswa`
--

CREATE TABLE IF NOT EXISTS `ppsiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpp` int(10) unsigned NOT NULL DEFAULT '0',
  `nis` varchar(20) NOT NULL,
  `statushadir` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 Hadir; 1 Sakit; 2 Ijin; 3 Alpa; 4 Cuti',
  `catatan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_ppsiswa_presensipelajaran` (`idpp`),
  KEY `FK_ppsiswa_siswa` (`nis`),
  KEY `IX_ppsiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Presensi kehadiran siswa dalam pelajaran' AUTO_INCREMENT=73 ;

--
-- Dumping data for table `ppsiswa`
--

INSERT INTO `ppsiswa` (`replid`, `idpp`, `nis`, `statushadir`, `catatan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 1, '12345', 1, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(2, 1, '15819', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(3, 1, '15836', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(4, 1, '15855', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(5, 1, '15877', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(6, 1, '15885', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(7, 1, '15887', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(8, 1, '15892', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(9, 1, '15908', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(10, 1, '15922', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(11, 1, '15926', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(12, 1, '15929', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(13, 1, '15937', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(14, 1, '15939', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(15, 1, '15941', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(16, 1, '15943', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(17, 1, '15952', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(18, 1, '15965', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(19, 1, '15966', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(20, 1, '15976', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(21, 1, '15978', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(22, 1, '16007', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(23, 1, '16018', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(24, 1, '16025', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(25, 1, '16032', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(26, 1, '16040', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(27, 1, '16041', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(28, 1, '16043', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(29, 1, '16045', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(30, 1, '16057', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(31, 1, '16063', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(32, 1, '16067', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(33, 1, '16081', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(34, 1, '16085', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(35, 1, '16100', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(36, 1, '16103', 0, '', NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(37, 2, '12345', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(38, 2, '15819', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(39, 2, '15836', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(40, 2, '15855', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(41, 2, '15877', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(42, 2, '15885', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(43, 2, '15887', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(44, 2, '15892', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(45, 2, '15908', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(46, 2, '15922', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(47, 2, '15926', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(48, 2, '15929', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(49, 2, '15937', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(50, 2, '15939', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(51, 2, '15941', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(52, 2, '15943', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(53, 2, '15952', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(54, 2, '15965', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(55, 2, '15966', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(56, 2, '15976', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(57, 2, '15978', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(58, 2, '16007', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(59, 2, '16018', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(60, 2, '16025', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(61, 2, '16032', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(62, 2, '16040', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(63, 2, '16041', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(64, 2, '16043', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(65, 2, '16045', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(66, 2, '16057', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(67, 2, '16063', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(68, 2, '16067', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(69, 2, '16081', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(70, 2, '16085', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(71, 2, '16100', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0),
(72, 2, '16103', 0, '', NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ppsiswahadir`
--

CREATE TABLE IF NOT EXISTS `ppsiswahadir` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `idkelas` int(10) unsigned NOT NULL,
  `idsemester` int(10) unsigned NOT NULL,
  `idpelajaran` int(10) unsigned NOT NULL,
  `gurupelajaran` varchar(30) NOT NULL,
  `bulan` tinyint(1) unsigned NOT NULL,
  `tahun` smallint(5) unsigned NOT NULL,
  `hadir` smallint(5) unsigned NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  UNIQUE KEY `UX_ppsiswahadir` (`nis`,`idkelas`,`idsemester`,`idpelajaran`,`gurupelajaran`,`bulan`,`tahun`),
  KEY `FK_ppsiswahadir_siswa` (`nis`),
  KEY `FK_ppsiswahadir_kelas` (`idkelas`),
  KEY `FK_ppsiswahadir_semester` (`idsemester`),
  KEY `FK_ppsiswahadir_pelajaran` (`idpelajaran`),
  KEY `FK_ppsiswahadir_pegawai` (`gurupelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ppsiswahadir`
--


-- --------------------------------------------------------

--
-- Table structure for table `presensiharian`
--

CREATE TABLE IF NOT EXISTS `presensiharian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkelas` int(10) unsigned NOT NULL,
  `idsemester` int(10) unsigned NOT NULL,
  `tanggal1` date NOT NULL,
  `tanggal2` date NOT NULL,
  `hariaktif` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_presensiharian_semester` (`idsemester`),
  KEY `IX_presensiharian_tanggal1` (`tanggal1`),
  KEY `IX_presensiharian_tanggal2` (`tanggal2`),
  KEY `FK_presensiharian_kelas` (`idkelas`),
  KEY `IX_presensiharian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `presensiharian`
--

INSERT INTO `presensiharian` (`replid`, `idkelas`, `idsemester`, `tanggal1`, `tanggal2`, `hariaktif`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 50, 1, '2016-01-01', '2016-01-22', 18, NULL, NULL, NULL, '2016-01-22 21:16:16', 0, 0),
(2, 50, 1, '2016-01-26', '2016-01-26', 1, NULL, NULL, NULL, '2016-01-26 10:29:30', 0, 0),
(4, 51, 1, '2016-04-19', '2016-04-24', 6, NULL, NULL, NULL, '2016-04-19 11:49:25', 0, 0),
(5, 50, 1, '2016-05-19', '2016-05-24', 6, NULL, NULL, NULL, '2016-04-19 11:50:00', 0, 0),
(6, 50, 1, '2016-03-19', '2016-03-24', 6, NULL, NULL, NULL, '2016-04-19 11:51:56', 0, 0),
(7, 50, 1, '2016-04-19', '2016-04-24', 6, NULL, NULL, NULL, '2016-04-19 11:52:31', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `presensipelajaran`
--

CREATE TABLE IF NOT EXISTS `presensipelajaran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkelas` int(10) unsigned NOT NULL DEFAULT '0',
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `tanggal` date NOT NULL DEFAULT '0000-00-00',
  `jam` time NOT NULL DEFAULT '00:00:00',
  `gurupelajaran` varchar(30) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `materi` varchar(1000) NOT NULL DEFAULT '',
  `objektif` varchar(255) DEFAULT NULL,
  `refleksi` varchar(255) DEFAULT NULL,
  `rencana` varchar(255) DEFAULT NULL,
  `keterlambatan` tinyint(4) DEFAULT NULL,
  `jumlahjam` decimal(4,2) NOT NULL DEFAULT '0.00',
  `jenisguru` int(10) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(20) DEFAULT NULL,
  `info2` varchar(20) DEFAULT NULL,
  `info3` varchar(20) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_presensipelajaran_semester` (`idsemester`),
  KEY `FK_presensipelajaran_jenisguru` (`jenisguru`),
  KEY `FK_presensipelajaran_pelajaran` (`idpelajaran`),
  KEY `FK_presensipelajaran_pegawai` (`gurupelajaran`),
  KEY `FK_presensipelajaran_kelas` (`idkelas`),
  KEY `IX_presensipelajaran_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Informasi presensi kehadiran guru dan siswa per mata pelajar' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `presensipelajaran`
--

INSERT INTO `presensipelajaran` (`replid`, `idkelas`, `idsemester`, `idpelajaran`, `tanggal`, `jam`, `gurupelajaran`, `keterangan`, `materi`, `objektif`, `refleksi`, `rencana`, `keterlambatan`, `jumlahjam`, `jenisguru`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 50, 1, 45, '2016-01-22', '08:00:00', '40', 'Hadir', 'Gaya dan Tekanan', '', '', 'Hukum Ohm', 10, '4.00', 6, NULL, NULL, NULL, '2016-01-22 21:22:09', 0, 0),
(2, 50, 1, 45, '2016-01-26', '09:39:00', '40', 'tes', 'tes', '', '', 'TES', 10, '2.00', 6, NULL, NULL, NULL, '2016-01-26 10:27:53', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prosespenerimaansiswa`
--

CREATE TABLE IF NOT EXISTS `prosespenerimaansiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proses` varchar(100) NOT NULL,
  `kodeawalan` varchar(5) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_prosespenerimaansiswa_departemen` (`departemen`),
  KEY `IX_prosespenerimaansiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `prosespenerimaansiswa`
--

INSERT INTO `prosespenerimaansiswa` (`replid`, `proses`, `kodeawalan`, `departemen`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'SPMB', 'SP', 'SMAN 1 Malang', 1, '', NULL, NULL, NULL, '2013-03-23 22:21:40', 36926, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ratauk`
--

CREATE TABLE IF NOT EXISTS `ratauk` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkelas` int(10) unsigned NOT NULL DEFAULT '0',
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idujian` int(10) unsigned NOT NULL DEFAULT '0',
  `nilaiRK` decimal(10,2) NOT NULL DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_ratauk_idsemester` (`idsemester`),
  KEY `FK_ratauk_idujian` (`idujian`),
  KEY `FK_ratauk_kelas` (`idkelas`),
  KEY `IX_ratauk_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ratauk`
--

INSERT INTO `ratauk` (`replid`, `idkelas`, `idsemester`, `idujian`, `nilaiRK`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 50, 1, 1, '11.43', NULL, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(2, 50, 1, 2, '12.57', NULL, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(4, 50, 1, 4, '11.43', NULL, NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(5, 50, 1, 5, '10.00', NULL, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(6, 50, 1, 6, '10.00', NULL, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(7, 50, 1, 7, '12.86', NULL, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(9, 50, 1, 9, '2.57', NULL, NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(10, 50, 1, 10, '15.43', NULL, NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(11, 50, 1, 11, '12.29', NULL, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(12, 50, 1, 12, '11.43', NULL, NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rataus`
--

CREATE TABLE IF NOT EXISTS `rataus` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idkelas` int(10) unsigned DEFAULT NULL,
  `idjenis` int(10) unsigned NOT NULL DEFAULT '0',
  `rataUS` decimal(10,2) NOT NULL DEFAULT '0.00',
  `keterangan` varchar(255) DEFAULT NULL,
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `idaturan` int(10) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_rataus_nis` (`nis`),
  KEY `FK_rataus_idsemester` (`idsemester`),
  KEY `FK_rataus_jenis` (`idjenis`),
  KEY `FK_rataus_idpelajaran` (`idpelajaran`),
  KEY `FK_rataus_kelas` (`idkelas`),
  KEY `IX_rataus_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=281 ;

--
-- Dumping data for table `rataus`
--

INSERT INTO `rataus` (`replid`, `nis`, `idsemester`, `idkelas`, `idjenis`, `rataUS`, `keterangan`, `idpelajaran`, `idaturan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, '15819', 1, 50, 10, '90.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(2, '15836', 1, 50, 10, '85.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(3, '15855', 1, 50, 10, '80.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(4, '15877', 1, 50, 10, '75.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(5, '15885', 1, 50, 10, '70.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(6, '15887', 1, 50, 10, '70.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(7, '15892', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(8, '15908', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(9, '15922', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(10, '15926', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(11, '15929', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(12, '15937', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(13, '15939', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(14, '15941', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(15, '15943', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(16, '15952', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(17, '15965', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(18, '15966', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(19, '15976', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(20, '15978', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(21, '16007', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(22, '16018', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(23, '16025', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(24, '16032', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(25, '16040', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(26, '16041', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(27, '16043', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(28, '16045', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(29, '16057', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(30, '16063', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(31, '16067', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(32, '16081', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(33, '16085', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(34, '16100', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(35, '16103', 1, 50, 10, '0.00', NULL, 45, 15, NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(36, '15819', 1, 50, 11, '90.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(37, '15836', 1, 50, 11, '80.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(38, '15855', 1, 50, 11, '70.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(39, '15877', 1, 50, 11, '60.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(40, '15885', 1, 50, 11, '50.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(41, '15887', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(42, '15892', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(43, '15908', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(44, '15922', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(45, '15926', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(46, '15929', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(47, '15937', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(48, '15939', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(49, '15941', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(50, '15943', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(51, '15952', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(52, '15965', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(53, '15966', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(54, '15976', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(55, '15978', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(56, '16007', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(57, '16018', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(58, '16025', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(59, '16032', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(60, '16040', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(61, '16041', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(62, '16043', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(63, '16045', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(64, '16057', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(65, '16063', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(66, '16067', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(67, '16081', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(68, '16085', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(69, '16100', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(70, '16103', 1, 50, 11, '0.00', NULL, 45, 16, NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(71, '15819', 1, 50, 12, '90.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(72, '15836', 1, 50, 12, '80.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(73, '15855', 1, 50, 12, '70.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(74, '15877', 1, 50, 12, '60.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(75, '15885', 1, 50, 12, '50.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(76, '15887', 1, 50, 12, '50.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(77, '15892', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(78, '15908', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(79, '15922', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(80, '15926', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(81, '15929', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(82, '15937', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(83, '15939', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(84, '15941', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(85, '15943', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(86, '15952', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(87, '15965', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(88, '15966', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(89, '15976', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(90, '15978', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(91, '16007', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(92, '16018', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(93, '16025', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(94, '16032', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(95, '16040', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(96, '16041', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(97, '16043', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(98, '16045', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(99, '16057', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(100, '16063', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(101, '16067', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(102, '16081', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(103, '16085', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(104, '16100', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(105, '16103', 1, 50, 12, '0.00', NULL, 45, 17, NULL, NULL, NULL, '2016-04-22 08:53:15', 0, 0),
(106, '15819', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(107, '15836', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(108, '15855', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(109, '15877', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(110, '15885', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(111, '15887', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(112, '15892', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(113, '15908', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(114, '15922', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(115, '15926', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(116, '15929', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(117, '15937', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(118, '15939', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(119, '15941', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(120, '15943', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(121, '15952', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(122, '15965', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(123, '15966', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(124, '15976', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(125, '15978', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(126, '16007', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(127, '16018', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(128, '16025', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(129, '16032', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(130, '16040', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(131, '16041', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(132, '16043', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(133, '16045', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(134, '16057', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(135, '16063', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(136, '16067', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(137, '16081', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(138, '16085', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(139, '16100', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(140, '16103', 1, 50, 0, '0.00', NULL, 0, 17, NULL, NULL, NULL, '2016-04-22 09:09:48', 0, 0),
(141, '15819', 1, 50, 10, '70.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(142, '15836', 1, 50, 10, '70.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(143, '15855', 1, 50, 10, '70.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(144, '15877', 1, 50, 10, '70.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(145, '15885', 1, 50, 10, '70.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(146, '15887', 1, 50, 10, '25.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(147, '15892', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(148, '15908', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(149, '15922', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(150, '15926', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(151, '15929', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(152, '15937', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(153, '15939', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(154, '15941', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(155, '15943', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(156, '15952', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(157, '15965', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(158, '15966', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(159, '15976', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(160, '15978', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(161, '16007', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(162, '16018', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(163, '16025', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(164, '16032', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(165, '16040', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(166, '16041', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(167, '16043', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(168, '16045', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(169, '16057', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(170, '16063', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(171, '16067', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(172, '16081', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(173, '16085', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(174, '16100', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(175, '16103', 1, 50, 10, '0.00', NULL, 47, 15, NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(176, '15819', 1, 50, 11, '70.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(177, '15836', 1, 50, 11, '30.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(178, '15855', 1, 50, 11, '35.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(179, '15877', 1, 50, 11, '40.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(180, '15885', 1, 50, 11, '45.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(181, '15887', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(182, '15892', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(183, '15908', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(184, '15922', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(185, '15926', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(186, '15929', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(187, '15937', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(188, '15939', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(189, '15941', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(190, '15943', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(191, '15952', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(192, '15965', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(193, '15966', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(194, '15976', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(195, '15978', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(196, '16007', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(197, '16018', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(198, '16025', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(199, '16032', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(200, '16040', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(201, '16041', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(202, '16043', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(203, '16045', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(204, '16057', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(205, '16063', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(206, '16067', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(207, '16081', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(208, '16085', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(209, '16100', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(210, '16103', 1, 50, 11, '0.00', NULL, 47, 16, NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(211, '15819', 1, 50, 12, '50.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(212, '15836', 1, 50, 12, '60.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(213, '15855', 1, 50, 12, '70.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(214, '15877', 1, 50, 12, '80.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(215, '15885', 1, 50, 12, '90.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(216, '15887', 1, 50, 12, '100.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(217, '15892', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(218, '15908', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(219, '15922', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(220, '15926', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(221, '15929', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(222, '15937', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(223, '15939', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(224, '15941', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(225, '15943', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(226, '15952', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(227, '15965', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(228, '15966', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(229, '15976', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(230, '15978', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(231, '16007', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(232, '16018', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(233, '16025', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(234, '16032', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(235, '16040', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(236, '16041', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(237, '16043', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(238, '16045', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(239, '16057', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(240, '16063', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(241, '16067', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(242, '16081', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(243, '16085', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(244, '16100', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(245, '16103', 1, 50, 12, '0.00', NULL, 47, 17, NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(246, '15819', 1, 50, 13, '85.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(247, '15836', 1, 50, 13, '85.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(248, '15855', 1, 50, 13, '85.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(249, '15877', 1, 50, 13, '75.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(250, '15885', 1, 50, 13, '85.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(251, '15887', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(252, '15892', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(253, '15908', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(254, '15922', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(255, '15926', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(256, '15929', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(257, '15937', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(258, '15939', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(259, '15941', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(260, '15943', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(261, '15952', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(262, '15965', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(263, '15966', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(264, '15976', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(265, '15978', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(266, '16007', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(267, '16018', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(268, '16025', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(269, '16032', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(270, '16040', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(271, '16041', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(272, '16043', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(273, '16045', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(274, '16057', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(275, '16063', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(276, '16067', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(277, '16081', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(278, '16085', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(279, '16100', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(280, '16103', 1, 50, 13, '0.00', NULL, 47, 18, NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `riwayatdeptsiswa`
--

CREATE TABLE IF NOT EXISTS `riwayatdeptsiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `mulai` date NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '0 -> Baru, 1 -> Siswa Pindah Departemen',
  `keterangan` varchar(255) DEFAULT NULL,
  `nislama` varchar(20) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_riwayatdeptsiswa_departemen` (`departemen`),
  KEY `FK_riwayatdeptsiswa_siswa` (`nis`),
  KEY `IX_riwayatdeptsiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=771 ;

--
-- Dumping data for table `riwayatdeptsiswa`
--

INSERT INTO `riwayatdeptsiswa` (`replid`, `nis`, `departemen`, `mulai`, `aktif`, `status`, `keterangan`, `nislama`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(44, '15810', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 62210, 0),
(45, '15819', 'SMAN 1 Malang', '2016-01-26', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 55505, 0),
(46, '15836', 'SMAN 1 Malang', '2016-01-26', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 8461, 0),
(47, '15855', 'SMAN 1 Malang', '2016-01-26', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 30099, 0),
(48, '15877', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 16933, 0),
(49, '15885', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 17734, 0),
(50, '15887', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 44068, 0),
(51, '15892', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 32210, 0),
(52, '15908', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 10812, 0),
(53, '15922', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 60756, 0),
(54, '15926', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 46998, 0),
(55, '15929', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 22424, 0),
(56, '15937', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 720, 0),
(57, '15939', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 41918, 0),
(58, '15941', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 57443, 0),
(59, '15943', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 56079, 0),
(60, '15952', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 52001, 0),
(61, '15965', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 24711, 0),
(62, '15966', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 48578, 0),
(63, '15976', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 8056, 0),
(64, '15978', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 53674, 0),
(65, '16007', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 13517, 0),
(66, '16018', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 25331, 0),
(67, '16025', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 23255, 0),
(68, '16032', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 62393, 0),
(69, '16040', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 38590, 0),
(70, '16041', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 26385, 0),
(71, '16043', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 20341, 0),
(72, '16045', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 62949, 0),
(73, '16057', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 30643, 0),
(74, '16063', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 30976, 0),
(75, '16067', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 12449, 0),
(76, '16081', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 28517, 0),
(77, '16085', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 44862, 0),
(78, '16100', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 1154, 0),
(79, '16103', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 60568, 0),
(80, '15805', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 30564, 0),
(81, '15816', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 23376, 0),
(82, '15822', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 61848, 0),
(83, '15841', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 57216, 0),
(84, '15867', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 24070, 0),
(85, '15883', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 53051, 0),
(86, '15888', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 55586, 0),
(87, '15890', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 111, 0),
(88, '15895', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 15244, 0),
(89, '15897', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 36986, 0),
(90, '15898', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 57146, 0),
(91, '15906', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 34410, 0),
(92, '15909', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 30267, 0),
(93, '15913', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 50638, 0),
(94, '15915', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 22836, 0),
(95, '15931', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 901, 0),
(96, '15940', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 45256, 0),
(97, '15950', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 57512, 0),
(98, '15958', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 37008, 0),
(99, '15968', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 31620, 0),
(100, '15977', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 23192, 0),
(101, '15982', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 18669, 0),
(102, '15986', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 15411, 0),
(103, '16001', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 7997, 0),
(104, '16002', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 27002, 0),
(105, '16026', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 43881, 0),
(106, '16042', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 29126, 0),
(107, '16046', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 31267, 0),
(108, '16054', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 11130, 0),
(109, '16069', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 46147, 0),
(110, '16070', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 49203, 0),
(111, '16073', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 3514, 0),
(112, '16077', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 1096, 0),
(113, '16083', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 58333, 0),
(114, '16093', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 49900, 0),
(115, '16105', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 57917, 0),
(116, '15815', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 9533, 0),
(117, '15829', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 50709, 0),
(118, '15838', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 64103, 0),
(119, '15842', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 23899, 0),
(120, '15852', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 50072, 0),
(121, '15878', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 18435, 0),
(122, '15882', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 57318, 0),
(123, '15884', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 24817, 0),
(124, '15889', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 44560, 0),
(125, '15893', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 11369, 0),
(126, '15894', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 34038, 0),
(127, '15903', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 57006, 0),
(128, '15904', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 37288, 0),
(129, '15944', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 2156, 0),
(130, '15945', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 17571, 0),
(131, '15962', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 36809, 0),
(132, '15971', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 45689, 0),
(133, '15980', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 40073, 0),
(134, '15981', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 6338, 0),
(135, '15987', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 14789, 0),
(136, '15989', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 29029, 0),
(137, '15991', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 22584, 0),
(138, '15992', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 40004, 0),
(139, '15995', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 34832, 0),
(140, '16008', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 2621, 0),
(141, '16011', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 55545, 0),
(142, '16023', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 47804, 0),
(143, '16028', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 31025, 0),
(144, '16036', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 11728, 0),
(145, '16044', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 35027, 0),
(146, '16065', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 60876, 0),
(147, '16066', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 58028, 0),
(148, '16074', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 49757, 0),
(149, '16075', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 54714, 0),
(150, '16097', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 19255, 0),
(151, '16107', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 63127, 0),
(152, '15807', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 8664, 0),
(153, '15808', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 37165, 0),
(154, '15843', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 52536, 0),
(155, '15845', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 53980, 0),
(156, '15848', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 16558, 0),
(157, '15850', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 63786, 0),
(158, '15853', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 3177, 0),
(159, '15862', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 39115, 0),
(160, '15863', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 28992, 0),
(161, '15868', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 18184, 0),
(162, '15876', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 16632, 0),
(163, '15880', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 53214, 0),
(164, '15891', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 7318, 0),
(165, '15911', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 505, 0),
(166, '15924', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 39387, 0),
(167, '15925', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 57419, 0),
(168, '15956', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 56936, 0),
(169, '15961', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 55699, 0),
(170, '15972', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 35318, 0),
(171, '15975', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 10896, 0),
(172, '15988', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 49150, 0),
(173, '15996', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 33651, 0),
(174, '15997', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 54419, 0),
(175, '15998', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 37346, 0),
(176, '15999', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 11160, 0),
(177, '16000', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 15484, 0),
(178, '16012', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 19992, 0),
(179, '16021', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 1718, 0),
(180, '16030', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 56364, 0),
(181, '16031', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 6693, 0),
(182, '16052', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 62046, 0),
(183, '16059', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 64812, 0),
(184, '16064', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 36585, 0),
(185, '16068', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 62813, 0),
(186, '16071', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 39867, 0),
(187, '16089', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 51737, 0),
(188, '15811', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 48138, 0),
(189, '15828', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 29542, 0),
(190, '15834', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 15136, 0),
(191, '15840', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 20010, 0),
(192, '15859', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 12589, 0),
(193, '15865', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8071, 0),
(194, '15866', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 43681, 0),
(195, '15899', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 17, 0),
(196, '15901', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 1478, 0),
(197, '15910', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 52213, 0),
(198, '15912', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8749, 0),
(199, '15916', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 10947, 0),
(200, '15918', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 49731, 0),
(201, '15932', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 13248, 0),
(202, '15953', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 44714, 0),
(203, '15957', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8413, 0),
(204, '15963', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 55488, 0),
(205, '15969', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 17971, 0),
(206, '15973', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 5038, 0),
(207, '15983', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 48840, 0),
(208, '15993', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 35943, 0),
(209, '15994', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 49510, 0),
(210, '16004', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 41439, 0),
(211, '16005', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 12471, 0),
(212, '16006', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 9849, 0),
(213, '16019', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 58022, 0),
(214, '16024', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 50819, 0),
(215, '16039', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8769, 0),
(216, '16060', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 50221, 0),
(217, '16061', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 45642, 0),
(218, '16062', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 1862, 0),
(219, '16078', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 30622, 0),
(220, '16088', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 40347, 0),
(221, '16091', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 41904, 0),
(222, '16106', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 34901, 0),
(223, '16108', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 22428, 0),
(224, '15804', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 7, 0),
(225, '15812', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 50240, 0),
(226, '15814', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 21683, 0),
(227, '15817', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 50490, 0),
(228, '15837', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 46176, 0),
(229, '15847', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 37234, 0),
(230, '15857', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 28142, 0),
(231, '15864', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 995, 0),
(232, '15907', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 40557, 0),
(233, '15917', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 6725, 0),
(234, '15928', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 32721, 0),
(235, '15930', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 11463, 0),
(236, '15935', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 33522, 0),
(237, '15942', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 13139, 0),
(238, '15948', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 14912, 0),
(239, '15954', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 51806, 0),
(240, '15955', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 6669, 0),
(241, '15959', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 52138, 0),
(242, '15967', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 59121, 0),
(243, '15979', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 45434, 0),
(244, '15984', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 6991, 0),
(245, '15990', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 53348, 0),
(246, '16009', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 16201, 0),
(247, '16010', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 52236, 0),
(248, '16013', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 6238, 0),
(249, '16016', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 6078, 0),
(250, '16027', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 55644, 0),
(251, '16034', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 27918, 0),
(252, '16058', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 10213, 0),
(253, '16072', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 63211, 0),
(254, '16084', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 62931, 0),
(255, '16098', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 51296, 0),
(256, '16086', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 45386, 0),
(257, '16087', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 17885, 0),
(258, '16090', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 8741, 0),
(259, '16104', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 19787, 0),
(260, '15813', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 36362, 0),
(261, '15821', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 15589, 0),
(262, '15826', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 3197, 0),
(263, '15833', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 31419, 0),
(264, '15844', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 2598, 0),
(265, '15846', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 14823, 0),
(266, '15849', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 1685, 0),
(267, '15854', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 62229, 0),
(268, '15860', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 18044, 0),
(269, '15870', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 25411, 0),
(270, '15871', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 38520, 0),
(271, '15874', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 36407, 0),
(272, '15875', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 37656, 0),
(273, '15896', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 63574, 0),
(274, '15902', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 6310, 0),
(275, '15920', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 33311, 0),
(276, '15921', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 4391, 0),
(277, '15933', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 41627, 0),
(278, '15938', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 627, 0),
(279, '15946', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 40206, 0),
(280, '15951', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 54105, 0),
(281, '15974', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 31835, 0),
(282, '16022', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 37484, 0),
(283, '16029', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 42146, 0),
(284, '16035', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 514, 0),
(285, '16047', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 17200, 0),
(286, '16048', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 51479, 0),
(287, '16049', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 49751, 0),
(288, '16050', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 34476, 0),
(289, '16051', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 38632, 0),
(290, '16076', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 62742, 0),
(291, '16079', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 44480, 0),
(292, '16082', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 29524, 0),
(293, '16095', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 46890, 0),
(294, '16099', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 5162, 0),
(295, '16109', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 64305, 0),
(296, '15806', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 24742, 0),
(297, '15818', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 46477, 0),
(298, '15823', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 35766, 0),
(299, '15824', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 33664, 0),
(300, '15827', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 23651, 0),
(301, '15830', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 146, 0),
(302, '15831', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 28402, 0),
(303, '15832', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 40141, 0),
(304, '15839', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 19825, 0),
(305, '15851', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 43442, 0),
(306, '15856', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 56745, 0),
(307, '15858', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 24623, 0),
(308, '15869', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 44477, 0),
(309, '15873', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 10030, 0),
(310, '15881', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 60234, 0),
(311, '15900', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 26276, 0),
(312, '15905', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 17006, 0),
(313, '15914', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 61237, 0),
(314, '15919', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 49291, 0),
(315, '15923', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 61861, 0),
(316, '15927', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 13860, 0),
(317, '15934', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 28257, 0),
(318, '15936', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 51450, 0),
(319, '15947', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 11049, 0),
(320, '15960', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 19002, 0),
(321, '15970', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 42256, 0),
(322, '16015', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 49645, 0),
(323, '16038', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 12898, 0),
(324, '16053', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 19049, 0),
(325, '16055', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 61613, 0),
(326, '16056', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 24208, 0),
(327, '16080', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 10937, 0),
(328, '16094', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 24744, 0),
(329, '16096', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 45976, 0),
(330, '16101', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 61129, 0),
(331, '16102', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 44819, 0),
(332, '15809', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 35800, 0),
(333, '15820', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 29087, 0),
(334, '15825', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 49695, 0),
(335, '15835', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 44814, 0),
(336, '15861', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 15321, 0),
(337, '15872', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 15670, 0),
(338, '15879', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 53527, 0),
(339, '15886', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 58307, 0),
(340, '15949', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 8307, 0),
(341, '15964', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 7648, 0),
(342, '15985', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 59872, 0),
(343, '16003', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 61662, 0),
(344, '16014', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 39895, 0),
(345, '16017', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 19536, 0),
(346, '16020', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 27003, 0),
(347, '16033', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 60028, 0),
(348, '16037', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 10256, 0),
(349, '16092', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 51608, 0),
(350, '15609', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 50969, 0),
(351, '15650', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 62861, 0),
(352, '15698', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 48556, 0),
(353, '15747', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 24717, 0),
(354, '15757', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 11015, 0),
(355, '15769', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 17215, 0),
(356, '15498', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 31316, 0),
(357, '15504', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 8983, 0),
(358, '15506', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 4151, 0),
(359, '15513', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 23966, 0),
(360, '15526', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 35869, 0),
(361, '15528', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 59917, 0),
(362, '15532', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 24110, 0),
(363, '15551', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 48867, 0),
(364, '15556', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 47436, 0),
(365, '15567', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 50852, 0),
(366, '15581', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 35092, 0),
(367, '15582', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 58174, 0),
(368, '15583', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 36731, 0),
(369, '15593', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 57134, 0),
(370, '15604', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 40358, 0),
(371, '15638', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 47725, 0),
(372, '15655', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 41552, 0),
(373, '15656', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 4177, 0),
(374, '15678', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 64095, 0),
(375, '15667', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 34554, 0),
(376, '15673', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 50737, 0),
(377, '15674', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 46039, 0),
(378, '15684', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 19658, 0),
(379, '15685', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 1920, 0),
(380, '15696', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 15987, 0),
(381, '15697', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 47563, 0),
(382, '15701', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 31293, 0),
(383, '15703', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 39657, 0),
(384, '15706', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 34074, 0),
(385, '15711', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 16877, 0),
(386, '15740', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 36994, 0),
(387, '15741', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 13675, 0),
(388, '15763', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 29588, 0),
(389, '15768', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 31925, 0),
(390, '15774', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 51321, 0),
(391, '15783', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 32740, 0),
(392, '15511', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 53966, 0),
(393, '15516', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 25713, 0),
(394, '15525', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 10199, 0),
(395, '15529', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 56132, 0),
(396, '15530', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 21223, 0),
(397, '15534', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 51164, 0),
(398, '15535', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 46989, 0),
(399, '15546', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 18561, 0),
(400, '15547', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 19567, 0),
(401, '15559', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 15707, 0),
(402, '15562', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 7435, 0),
(403, '15574', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 39581, 0),
(404, '15584', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 16898, 0),
(405, '15597', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 21037, 0),
(406, '15602', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 47108, 0),
(407, '15616', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 55202, 0),
(408, '15625', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 64317, 0),
(409, '15646', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 50747, 0),
(410, '15648', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 29633, 0),
(411, '15651', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 17514, 0),
(412, '15658', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 11221, 0),
(413, '15665', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 31625, 0),
(414, '15668', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 64524, 0),
(415, '15669', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 48596, 0),
(416, '15682', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 48537, 0),
(417, '15687', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 15140, 0),
(418, '15710', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 31781, 0),
(419, '15715', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 36708, 0),
(420, '15719', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 47080, 0),
(421, '15720', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 22974, 0),
(422, '15721', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 42540, 0),
(423, '15727', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 30627, 0),
(424, '15733', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 56508, 0),
(425, '15749', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 23985, 0),
(426, '15753', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 65402, 0),
(427, '15765', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 62991, 0),
(428, '15792', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 9662, 0),
(429, '15509', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 50088, 0),
(430, '15524', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 10933, 0),
(431, '15563', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 46452, 0),
(432, '15793', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 13432, 0),
(433, '15576', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 16929, 0),
(434, '15577', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 8676, 0),
(435, '15585', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 40352, 0),
(436, '15591', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 45024, 0),
(437, '15611', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 22228, 0),
(438, '15617', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 38312, 0),
(439, '15620', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 40162, 0),
(440, '15621', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 15997, 0),
(441, '15622', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 64346, 0),
(442, '15627', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 63117, 0),
(443, '15628', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 35503, 0),
(444, '15630', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 22073, 0),
(445, '15637', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 20871, 0),
(446, '15641', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 25230, 0),
(447, '15785', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 13165, 0),
(448, '15653', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 44342, 0),
(449, '15666', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 46397, 0),
(450, '15681', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 24338, 0),
(451, '15692', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 39015, 0),
(452, '15695', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 39444, 0),
(453, '15702', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 5094, 0),
(454, '15709', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 63311, 0),
(455, '15726', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 12547, 0),
(456, '15728', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 15142, 0),
(457, '15731', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 20662, 0),
(458, '15796', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 64543, 0),
(459, '15744', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 18436, 0),
(460, '15745', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 59850, 0),
(461, '15746', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 30758, 0),
(462, '15752', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 58332, 0),
(463, '15761', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 35336, 0),
(464, '15500', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 6844, 0),
(465, '15789', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 52139, 0),
(466, '15508', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 54519, 0),
(467, '15518', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 4860, 0),
(468, '15536', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 30746, 0),
(469, '15540', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 34881, 0),
(470, '15544', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 59926, 0),
(471, '15545', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 51515, 0),
(472, '15548', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 13078, 0),
(473, '15554', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 48445, 0),
(474, '15557', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 49370, 0),
(475, '15565', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 20382, 0),
(476, '15568', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 35412, 0),
(477, '15572', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 54599, 0),
(478, '15578', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 4251, 0),
(479, '15579', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 45384, 0),
(480, '15586', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 42860, 0),
(481, '15605', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 40891, 0);
INSERT INTO `riwayatdeptsiswa` (`replid`, `nis`, `departemen`, `mulai`, `aktif`, `status`, `keterangan`, `nislama`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(482, '15629', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 17046, 0),
(483, '15635', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 62998, 0),
(484, '15636', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 58017, 0),
(485, '15642', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 57101, 0),
(486, '15644', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 52755, 0),
(487, '15784', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 57417, 0),
(488, '15657', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 23538, 0),
(489, '15670', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 5117, 0),
(490, '15676', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 35905, 0),
(491, '15680', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 47495, 0),
(492, '15693', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 12363, 0),
(493, '15791', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 34284, 0),
(494, '15705', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 49852, 0),
(495, '15714', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 48061, 0),
(496, '15723', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 8700, 0),
(497, '15736', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 53615, 0),
(498, '15742', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 56927, 0),
(499, '15759', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 26646, 0),
(500, '15515', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 4873, 0),
(501, '15520', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 63578, 0),
(502, '15523', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 39911, 0),
(503, '15533', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 34556, 0),
(504, '15538', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 48576, 0),
(505, '15552', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 4203, 0),
(506, '15553', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 7610, 0),
(507, '15560', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 36559, 0),
(508, '15566', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 32469, 0),
(509, '15571', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 37696, 0),
(510, '15594', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 42038, 0),
(511, '15601', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 51675, 0),
(512, '15603', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 50061, 0),
(513, '15606', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 53953, 0),
(514, '15612', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 15684, 0),
(515, '15613', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 60088, 0),
(516, '15614', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 58534, 0),
(517, '15619', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 45987, 0),
(518, '15631', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 1567, 0),
(519, '15640', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 63229, 0),
(520, '15649', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 35704, 0),
(521, '15662', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 35079, 0),
(522, '15797', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 7440, 0),
(523, '15671', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 41210, 0),
(524, '15694', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 16027, 0),
(525, '15700', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 38406, 0),
(526, '15708', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 18530, 0),
(527, '15712', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 52264, 0),
(528, '15718', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 11073, 0),
(529, '15734', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 64499, 0),
(530, '15735', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 11653, 0),
(531, '15738', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 42649, 0),
(532, '15750', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 17338, 0),
(533, '15754', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 38920, 0),
(534, '15762', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 24233, 0),
(535, '15776', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 35923, 0),
(536, '15501', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 57028, 0),
(537, '15801', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 53695, 0),
(538, '15507', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 3567, 0),
(539, '15514', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 15127, 0),
(540, '15531', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 65346, 0),
(541, '15537', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 35566, 0),
(542, '15541', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 32415, 0),
(543, '15558', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 60146, 0),
(544, '15564', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 4695, 0),
(545, '15589', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 62242, 0),
(546, '15595', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 3379, 0),
(547, '15598', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 31401, 0),
(548, '15600', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 61729, 0),
(549, '15786', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 27576, 0),
(550, '15607', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 15127, 0),
(551, '15608', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 6916, 0),
(552, '15615', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 13768, 0),
(553, '15632', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 62788, 0),
(554, '15633', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 64138, 0),
(555, '15639', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 28228, 0),
(556, '15675', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 2252, 0),
(557, '15677', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 7760, 0),
(558, '15690', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 35725, 0),
(559, '15707', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 27218, 0),
(560, '15716', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 33314, 0),
(561, '15722', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 29802, 0),
(562, '15724', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 42969, 0),
(563, '15737', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 11690, 0),
(564, '15739', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 37079, 0),
(565, '15743', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 29484, 0),
(566, '15751', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 46983, 0),
(567, '15756', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 13669, 0),
(568, '15758', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 21517, 0),
(569, '15767', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 41648, 0),
(570, '15770', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 64100, 0),
(571, '15779', 'SMAN 1 Malang', '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 6168, 0),
(591, '15519', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 22885, 0),
(592, '15575', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 5700, 0),
(593, '15580', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 34711, 0),
(594, '15592', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 1767, 0),
(595, '15610', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 55989, 0),
(596, '15645', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 40911, 0),
(597, '15647', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 57388, 0),
(598, '15659', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 13298, 0),
(599, '15664', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 38608, 0),
(600, '15672', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 42841, 0),
(601, '15679', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 25771, 0),
(602, '15686', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 53084, 0),
(603, '15699', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 12244, 0),
(604, '15732', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 4936, 0),
(605, '15760', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 64285, 0),
(606, '15772', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 29202, 0),
(607, '15773', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 10640, 0),
(608, '15777', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 62759, 0),
(609, '15782', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 15261, 0),
(640, '15503', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 1342, 0),
(641, '15505', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 19243, 0),
(642, '15510', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 61526, 0),
(643, '15517', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 11971, 0),
(644, '15521', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 17304, 0),
(645, '15522', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 12574, 0),
(646, '15527', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 14416, 0),
(647, '15542', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 27386, 0),
(648, '15549', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 32357, 0),
(649, '15561', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 14275, 0),
(650, '15587', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 6200, 0),
(651, '15588', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 45062, 0),
(652, '15590', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 29925, 0),
(653, '15596', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 63318, 0),
(654, '15799', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 63535, 0),
(655, '15788', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 31135, 0),
(656, '15623', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 54514, 0),
(657, '15624', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 47323, 0),
(658, '15626', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 20434, 0),
(659, '15643', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 29339, 0),
(660, '15652', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 26061, 0),
(661, '15654', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 47632, 0),
(662, '15660', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 26942, 0),
(663, '15730', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 17373, 0),
(664, '15748', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 7121, 0),
(665, '15766', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 35065, 0),
(666, '15775', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 57193, 0),
(667, '15778', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 24530, 0),
(668, '15780', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 24945, 0),
(669, '15781', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 22712, 0),
(670, '15502', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 29367, 0),
(671, '15512', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 32302, 0),
(672, '15539', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 21935, 0),
(673, '15543', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 30757, 0),
(674, '15550', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 33413, 0),
(675, '15555', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 7767, 0),
(676, '15569', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 46813, 0),
(677, '15570', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 62201, 0),
(678, '15573', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 42464, 0),
(679, '15802', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 13902, 0),
(680, '16115', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 31130, 0),
(681, '15618', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 18089, 0),
(682, '15795', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 16778, 0),
(683, '15634', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 1938, 0),
(684, '15661', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 29650, 0),
(685, '15663', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 26137, 0),
(686, '15683', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 45262, 0),
(687, '15688', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 31949, 0),
(688, '15691', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 23234, 0),
(689, '15713', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 13080, 0),
(690, '15725', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 25867, 0),
(691, '15729', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 12403, 0),
(692, '15790', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 31690, 0),
(693, '15794', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 37808, 0),
(694, '15755', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 6491, 0),
(695, '15764', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 22645, 0),
(696, '15787', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 63602, 0),
(697, '15771', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 20643, 0),
(698, '15192', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 65247, 0),
(699, '15193', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 9765, 0),
(700, '15260', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 2466, 0),
(701, '15272', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 64644, 0),
(702, '15298', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 59080, 0),
(703, '15328', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 32154, 0),
(704, '15336', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 59700, 0),
(705, '15340', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 41558, 0),
(706, '15453', 'SMAN 1 Malang', '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 8947, 0),
(707, '15180', 'SMAN 1 Malang', '2010-07-02', 0, 0, NULL, NULL, NULL, NULL, NULL, '2013-03-25 13:19:03', 54375, 0),
(708, '15188', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 27850, 0),
(709, '15195', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 57088, 0),
(710, '15203', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 14337, 0),
(711, '15204', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 64298, 0),
(712, '15212', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 38542, 0),
(713, '15220', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 36725, 0),
(714, '15226', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 62459, 0),
(715, '15232', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 7763, 0),
(716, '15242', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 48616, 0),
(717, '15244', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 9973, 0),
(718, '15258', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 9412, 0),
(719, '15259', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 24981, 0),
(720, '15264', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 40565, 0),
(721, '15267', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 14985, 0),
(722, '15277', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 38003, 0),
(723, '15278', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 14451, 0),
(724, '15287', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 64428, 0),
(725, '15291', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 45865, 0),
(726, '15294', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 29254, 0),
(727, '15299', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 53471, 0),
(728, '15301', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 14708, 0),
(729, '15307', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 60544, 0),
(730, '15312', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 55863, 0),
(731, '15313', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 16552, 0),
(732, '15320', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 12411, 0),
(733, '15359', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 21694, 0),
(734, '15367', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 24285, 0),
(735, '15371', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 46444, 0),
(736, '15403', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 45445, 0),
(737, '15413', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 22523, 0),
(738, '15491', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 50535, 0),
(739, '15420', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 27110, 0),
(740, '15423', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 17953, 0),
(741, '15426', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 48945, 0),
(742, '15447', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 23270, 0),
(743, '15452', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 15786, 0),
(744, '15458', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 44778, 0),
(745, '15462', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 10571, 0),
(746, '15179', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 50444, 0),
(747, '15494', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 3510, 0),
(748, '15198', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 32807, 0),
(749, '15224', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 39631, 0),
(750, '15485', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 56649, 0),
(751, '15236', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 54313, 0),
(752, '15240', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 63237, 0),
(753, '15252', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 53864, 0),
(754, '15253', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 46063, 0),
(755, '15284', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 2640, 0),
(756, '15286', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 15953, 0),
(757, '15297', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 25411, 0),
(758, '15302', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 29377, 0),
(759, '15317', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 25763, 0),
(760, '15323', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 22786, 0),
(761, '15330', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 13211, 0),
(762, '15335', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 48430, 0),
(763, '15343', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 44592, 0),
(764, '15375', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 31699, 0),
(765, '15396', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 21288, 0),
(766, '15431', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 60811, 0),
(767, '16111', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 30659, 0),
(768, '15478', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 24458, 0),
(769, '15495', 'SMAN 1 Malang', '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 14817, 0),
(770, '12345678', 'SMAN 1 Malang', '2013-02-14', 1, 0, NULL, NULL, NULL, NULL, NULL, '2013-02-14 22:04:53', 2371, 0);

-- --------------------------------------------------------

--
-- Table structure for table `riwayatkelassiswa`
--

CREATE TABLE IF NOT EXISTS `riwayatkelassiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `idkelas` int(10) unsigned NOT NULL,
  `mulai` date NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '0 -> Baru, 1 -> Naik, 2 -> Tidak Naik, 3-> Pindah',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_riwayatkelassiswa_siswa` (`nis`),
  KEY `FK_riwayatkelassiswa_kelas` (`idkelas`),
  KEY `IX_riwayatkelassiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=772 ;

--
-- Dumping data for table `riwayatkelassiswa`
--

INSERT INTO `riwayatkelassiswa` (`replid`, `nis`, `idkelas`, `mulai`, `aktif`, `status`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(44, '15810', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 6217, 0),
(45, '15819', 50, '2016-01-26', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 23336, 0),
(46, '15836', 50, '2016-01-26', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 60753, 0),
(47, '15855', 50, '2016-01-26', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 36185, 0),
(48, '15877', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 9373, 0),
(49, '15885', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 34945, 0),
(50, '15887', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 52361, 0),
(51, '15892', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 34367, 0),
(52, '15908', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 25026, 0),
(53, '15922', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 25689, 0),
(54, '15926', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 3277, 0),
(55, '15929', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 27237, 0),
(56, '15937', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 58991, 0),
(57, '15939', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 52042, 0),
(58, '15941', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 14907, 0),
(59, '15943', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 49535, 0),
(60, '15952', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 21685, 0),
(61, '15965', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 4163, 0),
(62, '15966', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 9659, 0),
(63, '15976', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 4954, 0),
(64, '15978', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 4445, 0),
(65, '16007', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 25261, 0),
(66, '16018', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 457, 0),
(67, '16025', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 9756, 0),
(68, '16032', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 47274, 0),
(69, '16040', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 45382, 0),
(70, '16041', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 60615, 0),
(71, '16043', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 19709, 0),
(72, '16045', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 5590, 0),
(73, '16057', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 46130, 0),
(74, '16063', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 836, 0),
(75, '16067', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 849, 0),
(76, '16081', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 45312, 0),
(77, '16085', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 63405, 0),
(78, '16100', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 48220, 0),
(79, '16103', 50, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:05:45', 48643, 0),
(80, '15805', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 33816, 0),
(81, '15816', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 15771, 0),
(82, '15822', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 20941, 0),
(83, '15841', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 4362, 0),
(84, '15867', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 45630, 0),
(85, '15883', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 59549, 0),
(86, '15888', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 58755, 0),
(87, '15890', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 12684, 0),
(88, '15895', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 18033, 0),
(89, '15897', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 51645, 0),
(90, '15898', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 40527, 0),
(91, '15906', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:34', 12923, 0),
(92, '15909', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 5067, 0),
(93, '15913', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 56415, 0),
(94, '15915', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 51355, 0),
(95, '15931', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 29479, 0),
(96, '15940', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 54321, 0),
(97, '15950', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 11109, 0),
(98, '15958', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 11777, 0),
(99, '15968', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 52400, 0),
(100, '15977', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 7726, 0),
(101, '15982', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 55130, 0),
(102, '15986', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 7893, 0),
(103, '16001', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 60295, 0),
(104, '16002', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 23384, 0),
(105, '16026', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 46136, 0),
(106, '16042', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 45109, 0),
(107, '16046', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 3952, 0),
(108, '16054', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 15643, 0),
(109, '16069', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 30722, 0),
(110, '16070', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 3921, 0),
(111, '16073', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 8403, 0),
(112, '16077', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 42121, 0),
(113, '16083', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 62891, 0),
(114, '16093', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 27720, 0),
(115, '16105', 51, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:09:35', 60773, 0),
(116, '15815', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 50505, 0),
(117, '15829', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 40467, 0),
(118, '15838', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 38836, 0),
(119, '15842', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 48358, 0),
(120, '15852', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 2213, 0),
(121, '15878', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 50919, 0),
(122, '15882', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 16380, 0),
(123, '15884', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 1074, 0),
(124, '15889', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 36856, 0),
(125, '15893', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 36107, 0),
(126, '15894', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 58564, 0),
(127, '15903', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 40563, 0),
(128, '15904', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 25522, 0),
(129, '15944', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 29076, 0),
(130, '15945', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 64290, 0),
(131, '15962', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 33131, 0),
(132, '15971', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 62760, 0),
(133, '15980', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 63355, 0),
(134, '15981', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 31801, 0),
(135, '15987', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 47147, 0),
(136, '15989', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 29755, 0),
(137, '15991', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 58918, 0),
(138, '15992', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 43752, 0),
(139, '15995', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 8975, 0),
(140, '16008', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 60906, 0),
(141, '16011', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 43023, 0),
(142, '16023', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 28047, 0),
(143, '16028', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 33601, 0),
(144, '16036', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 30439, 0),
(145, '16044', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 20692, 0),
(146, '16065', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 47123, 0),
(147, '16066', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 126, 0),
(148, '16074', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 48327, 0),
(149, '16075', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 62009, 0),
(150, '16097', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 51742, 0),
(151, '16107', 52, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:10:31', 40456, 0),
(152, '15807', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 4618, 0),
(153, '15808', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 63472, 0),
(154, '15843', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 38274, 0),
(155, '15845', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 37557, 0),
(156, '15848', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 5253, 0),
(157, '15850', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 61507, 0),
(158, '15853', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 60539, 0),
(159, '15862', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 36923, 0),
(160, '15863', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 8680, 0),
(161, '15868', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 25069, 0),
(162, '15876', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 1845, 0),
(163, '15880', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 60444, 0),
(164, '15891', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 2034, 0),
(165, '15911', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 37878, 0),
(166, '15924', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 26486, 0),
(167, '15925', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 47695, 0),
(168, '15956', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 20502, 0),
(169, '15961', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 24689, 0),
(170, '15972', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 45426, 0),
(171, '15975', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 18599, 0),
(172, '15988', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 64828, 0),
(173, '15996', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 31375, 0),
(174, '15997', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 38802, 0),
(175, '15998', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 28949, 0),
(176, '15999', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 23206, 0),
(177, '16000', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 26360, 0),
(178, '16012', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 40517, 0),
(179, '16021', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 39477, 0),
(180, '16030', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 32777, 0),
(181, '16031', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 49085, 0),
(182, '16052', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 27379, 0),
(183, '16059', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 13088, 0),
(184, '16064', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 45671, 0),
(185, '16068', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 23789, 0),
(186, '16071', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 14641, 0),
(187, '16089', 53, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:17:09', 4698, 0),
(188, '15811', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 46079, 0),
(189, '15828', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 20831, 0),
(190, '15834', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 29101, 0),
(191, '15840', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 61874, 0),
(192, '15859', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 35758, 0),
(193, '15865', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 10468, 0),
(194, '15866', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 2976, 0),
(195, '15899', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 20929, 0),
(196, '15901', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 21253, 0),
(197, '15910', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 21105, 0),
(198, '15912', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 506, 0),
(199, '15916', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 60361, 0),
(200, '15918', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 31532, 0),
(201, '15932', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 40852, 0),
(202, '15953', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 57677, 0),
(203, '15957', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 38045, 0),
(204, '15963', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 44607, 0),
(205, '15969', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 51205, 0),
(206, '15973', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8634, 0),
(207, '15983', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 28977, 0),
(208, '15993', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 53565, 0),
(209, '15994', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 29683, 0),
(210, '16004', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 10949, 0),
(211, '16005', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8951, 0),
(212, '16006', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 27225, 0),
(213, '16019', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 35913, 0),
(214, '16024', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 41005, 0),
(215, '16039', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 17198, 0),
(216, '16060', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 6530, 0),
(217, '16061', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 58012, 0),
(218, '16062', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 8619, 0),
(219, '16078', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 40584, 0),
(220, '16088', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 65158, 0),
(221, '16091', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 53987, 0),
(222, '16106', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 3966, 0),
(223, '16108', 54, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:18:03', 39122, 0),
(224, '15804', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 8246, 0),
(225, '15812', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 62048, 0),
(226, '15814', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 23036, 0),
(227, '15817', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 36524, 0),
(228, '15837', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 6368, 0),
(229, '15847', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 47516, 0),
(230, '15857', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 25191, 0),
(231, '15864', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 11458, 0),
(232, '15907', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 39873, 0),
(233, '15917', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 62667, 0),
(234, '15928', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 4959, 0),
(235, '15930', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 15297, 0),
(236, '15935', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 41325, 0),
(237, '15942', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 9636, 0),
(238, '15948', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 48266, 0),
(239, '15954', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 62434, 0),
(240, '15955', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 21810, 0),
(241, '15959', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 59086, 0),
(242, '15967', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 9610, 0),
(243, '15979', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 25338, 0),
(244, '15984', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 63786, 0),
(245, '15990', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 28174, 0),
(246, '16009', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 7463, 0),
(247, '16010', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 32921, 0),
(248, '16013', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 7508, 0),
(249, '16016', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 39438, 0),
(250, '16027', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 3464, 0),
(251, '16034', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 64179, 0),
(252, '16058', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 60473, 0),
(253, '16072', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 25108, 0),
(254, '16084', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 48218, 0),
(255, '16098', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 34059, 0),
(256, '16086', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 46652, 0),
(257, '16087', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 60250, 0),
(258, '16090', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 21753, 0),
(259, '16104', 55, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:19:11', 47909, 0),
(260, '15813', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 34437, 0),
(261, '15821', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 19710, 0),
(262, '15826', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 57223, 0),
(263, '15833', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 48348, 0),
(264, '15844', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 29253, 0),
(265, '15846', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 51885, 0),
(266, '15849', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 18871, 0),
(267, '15854', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 43249, 0),
(268, '15860', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 1543, 0),
(269, '15870', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 4184, 0),
(270, '15871', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:06', 30897, 0),
(271, '15874', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 65257, 0),
(272, '15875', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 61330, 0),
(273, '15896', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 64483, 0),
(274, '15902', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 29645, 0),
(275, '15920', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 40784, 0),
(276, '15921', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 37651, 0),
(277, '15933', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 10539, 0),
(278, '15938', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 23106, 0),
(279, '15946', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 56681, 0),
(280, '15951', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 44294, 0),
(281, '15974', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 47239, 0),
(282, '16022', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 27473, 0),
(283, '16029', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 4930, 0),
(284, '16035', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 45812, 0),
(285, '16047', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 58747, 0),
(286, '16048', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 55233, 0),
(287, '16049', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 14638, 0),
(288, '16050', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 7391, 0),
(289, '16051', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 64936, 0),
(290, '16076', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 15014, 0),
(291, '16079', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 37627, 0),
(292, '16082', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 49069, 0),
(293, '16095', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 26248, 0),
(294, '16099', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 54101, 0),
(295, '16109', 56, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:20:07', 15183, 0),
(296, '15806', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 16003, 0),
(297, '15818', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 18422, 0),
(298, '15823', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 58681, 0),
(299, '15824', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 3178, 0),
(300, '15827', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 45919, 0),
(301, '15830', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 49049, 0),
(302, '15831', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 62890, 0),
(303, '15832', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 37206, 0),
(304, '15839', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 33364, 0),
(305, '15851', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 26242, 0),
(306, '15856', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 18959, 0),
(307, '15858', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 21789, 0),
(308, '15869', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 51632, 0),
(309, '15873', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 3620, 0),
(310, '15881', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 9498, 0),
(311, '15900', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 6774, 0),
(312, '15905', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 23315, 0),
(313, '15914', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:43', 43993, 0),
(314, '15919', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 6633, 0),
(315, '15923', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 63659, 0),
(316, '15927', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 64350, 0),
(317, '15934', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 21605, 0),
(318, '15936', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 56433, 0),
(319, '15947', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 64992, 0),
(320, '15960', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 5941, 0),
(321, '15970', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 31068, 0),
(322, '16015', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 31457, 0),
(323, '16038', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 39447, 0),
(324, '16053', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 12810, 0),
(325, '16055', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 25238, 0),
(326, '16056', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 34875, 0),
(327, '16080', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 11561, 0),
(328, '16094', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 48748, 0),
(329, '16096', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 49019, 0),
(330, '16101', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 49674, 0),
(331, '16102', 57, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:21:44', 29152, 0),
(332, '15809', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 13817, 0),
(333, '15820', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 63792, 0),
(334, '15825', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 12091, 0),
(335, '15835', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 31252, 0),
(336, '15861', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 11159, 0),
(337, '15872', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 48861, 0),
(338, '15879', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 3402, 0),
(339, '15886', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 1410, 0),
(340, '15949', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 20905, 0),
(341, '15964', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 61551, 0),
(342, '15985', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 34689, 0),
(343, '16003', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 64706, 0),
(344, '16014', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 45961, 0),
(345, '16017', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 29441, 0),
(346, '16020', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 290, 0),
(347, '16033', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 14587, 0),
(348, '16037', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 45082, 0),
(349, '16092', 58, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:27:05', 1761, 0),
(350, '15609', 70, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 37609, 0),
(351, '15650', 70, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 12302, 0),
(352, '15698', 70, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 34230, 0),
(353, '15747', 70, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 47140, 0),
(354, '15757', 70, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 29143, 0),
(355, '15769', 70, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:28:21', 10183, 0),
(356, '15498', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 27717, 0),
(357, '15504', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 42057, 0),
(358, '15506', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 60514, 0),
(359, '15513', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 35843, 0),
(360, '15526', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 53987, 0),
(361, '15528', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 9183, 0),
(362, '15532', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 63339, 0),
(363, '15551', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 35475, 0),
(364, '15556', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 13804, 0),
(365, '15567', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 15417, 0),
(366, '15581', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 8699, 0),
(367, '15582', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 17505, 0),
(368, '15583', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 51470, 0),
(369, '15593', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 40790, 0),
(370, '15604', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 38622, 0),
(371, '15638', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 22514, 0),
(372, '15655', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 37457, 0),
(373, '15656', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 29591, 0),
(374, '15678', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 45272, 0),
(375, '15667', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 5015, 0),
(376, '15673', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 30729, 0),
(377, '15674', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 29305, 0),
(378, '15684', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 7533, 0),
(379, '15685', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 7993, 0),
(380, '15696', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 42849, 0),
(381, '15697', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 1087, 0),
(382, '15701', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 43989, 0),
(383, '15703', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 16673, 0),
(384, '15706', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 15063, 0),
(385, '15711', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 34039, 0),
(386, '15740', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 22866, 0),
(387, '15741', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 58327, 0),
(388, '15763', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 51412, 0),
(389, '15768', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 47929, 0),
(390, '15774', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 21587, 0),
(391, '15783', 61, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:30:22', 39669, 0),
(392, '15511', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 34985, 0),
(393, '15516', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 23953, 0),
(394, '15525', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 54185, 0),
(395, '15529', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 18386, 0),
(396, '15530', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 7860, 0),
(397, '15534', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 1277, 0),
(398, '15535', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 20967, 0),
(399, '15546', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 4609, 0),
(400, '15547', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 64692, 0),
(401, '15559', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 5090, 0),
(402, '15562', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 36655, 0),
(403, '15574', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 42649, 0),
(404, '15584', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 63115, 0),
(405, '15597', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 31378, 0),
(406, '15602', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 19735, 0),
(407, '15616', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 10775, 0),
(408, '15625', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 1480, 0),
(409, '15646', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 24033, 0),
(410, '15648', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 47808, 0),
(411, '15651', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 30323, 0),
(412, '15658', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 21003, 0),
(413, '15665', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 9603, 0),
(414, '15668', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 4502, 0),
(415, '15669', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 35340, 0),
(416, '15682', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:39', 18888, 0),
(417, '15687', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 32618, 0),
(418, '15710', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 2487, 0),
(419, '15715', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 39083, 0),
(420, '15719', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 45059, 0),
(421, '15720', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 59299, 0),
(422, '15721', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 54226, 0),
(423, '15727', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 50259, 0),
(424, '15733', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 896, 0),
(425, '15749', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 25284, 0),
(426, '15753', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 32563, 0),
(427, '15765', 62, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:31:40', 21956, 0),
(428, '15792', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 65458, 0),
(429, '15509', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 10819, 0),
(430, '15524', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 15523, 0),
(431, '15563', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 32283, 0),
(432, '15793', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 992, 0),
(433, '15576', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 59595, 0),
(434, '15577', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 22643, 0),
(435, '15585', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 5730, 0),
(436, '15591', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 43623, 0),
(437, '15611', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 58598, 0),
(438, '15617', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 36883, 0),
(439, '15620', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 57897, 0),
(440, '15621', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 31704, 0),
(441, '15622', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 55677, 0),
(442, '15627', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 59542, 0),
(443, '15628', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 49039, 0),
(444, '15630', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 21956, 0),
(445, '15637', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 39207, 0),
(446, '15641', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 53544, 0),
(447, '15785', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 13971, 0),
(448, '15653', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 65107, 0),
(449, '15666', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 47634, 0),
(450, '15681', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 21346, 0),
(451, '15692', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 28394, 0),
(452, '15695', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 56911, 0),
(453, '15702', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 51030, 0),
(454, '15709', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 55651, 0),
(455, '15726', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 59880, 0),
(456, '15728', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 11263, 0),
(457, '15731', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 5107, 0),
(458, '15796', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 38887, 0),
(459, '15744', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 24227, 0),
(460, '15745', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 36248, 0),
(461, '15746', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 17666, 0),
(462, '15752', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 41378, 0),
(463, '15761', 63, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:34:37', 15468, 0),
(464, '15500', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 54695, 0),
(465, '15789', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 26113, 0),
(466, '15508', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 22500, 0),
(467, '15518', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 46412, 0),
(468, '15536', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 25527, 0),
(469, '15540', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 2677, 0),
(470, '15544', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 49157, 0),
(471, '15545', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 59548, 0),
(472, '15548', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 28991, 0),
(473, '15554', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 56125, 0),
(474, '15557', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 37567, 0),
(475, '15565', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 48272, 0),
(476, '15568', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 29605, 0),
(477, '15572', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 16570, 0),
(478, '15578', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 1983, 0),
(479, '15579', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 38849, 0),
(480, '15586', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 40017, 0),
(481, '15605', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 55463, 0),
(482, '15629', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 14486, 0),
(483, '15635', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 54526, 0),
(484, '15636', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 39174, 0),
(485, '15642', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 23459, 0),
(486, '15644', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 32622, 0),
(487, '15784', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 10432, 0),
(488, '15657', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 19260, 0),
(489, '15670', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 14057, 0),
(490, '15676', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 14725, 0),
(491, '15680', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:20', 12095, 0),
(492, '15693', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 45731, 0),
(493, '15791', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 55428, 0),
(494, '15705', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 54046, 0),
(495, '15714', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 9335, 0),
(496, '15723', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 8369, 0),
(497, '15736', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 24250, 0),
(498, '15742', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 10239, 0),
(499, '15759', 64, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:36:21', 33283, 0),
(500, '15515', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 27063, 0),
(501, '15520', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 21334, 0),
(502, '15523', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 58569, 0),
(503, '15533', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 46619, 0),
(504, '15538', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 51183, 0),
(505, '15552', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 18105, 0),
(506, '15553', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 893, 0),
(507, '15560', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 41295, 0),
(508, '15566', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 3001, 0),
(509, '15571', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 42506, 0),
(510, '15594', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 42926, 0),
(511, '15601', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 58359, 0),
(512, '15603', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:17', 36585, 0),
(513, '15606', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 40477, 0),
(514, '15612', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 22361, 0),
(515, '15613', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 40651, 0),
(516, '15614', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 27107, 0),
(517, '15619', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 22490, 0),
(518, '15631', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 18836, 0),
(519, '15640', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 47712, 0),
(520, '15649', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 31895, 0),
(521, '15662', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 18298, 0),
(522, '15797', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 12903, 0),
(523, '15671', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 13934, 0),
(524, '15694', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 45438, 0),
(525, '15700', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 47879, 0),
(526, '15708', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 47775, 0),
(527, '15712', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 39116, 0),
(528, '15718', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 4525, 0),
(529, '15734', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 26634, 0),
(530, '15735', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 42649, 0),
(531, '15738', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 6038, 0),
(532, '15750', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 52313, 0),
(533, '15754', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 24659, 0),
(534, '15762', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 36028, 0),
(535, '15776', 65, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:37:18', 53893, 0),
(536, '15501', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 62174, 0),
(537, '15801', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 45709, 0),
(538, '15507', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 12056, 0),
(539, '15514', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 57964, 0),
(540, '15531', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 52128, 0),
(541, '15537', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 49552, 0),
(542, '15541', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 1006, 0),
(543, '15558', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 53137, 0),
(544, '15564', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 29860, 0),
(545, '15589', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 36634, 0),
(546, '15595', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 27560, 0),
(547, '15598', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 36130, 0),
(548, '15600', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 49298, 0),
(549, '15786', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 19489, 0),
(550, '15607', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 31479, 0),
(551, '15608', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 26192, 0),
(552, '15615', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 327, 0),
(553, '15632', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 39768, 0),
(554, '15633', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 27142, 0),
(555, '15639', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 49135, 0),
(556, '15675', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 52525, 0),
(557, '15677', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 57552, 0),
(558, '15690', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 40488, 0),
(559, '15707', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 46880, 0),
(560, '15716', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 35983, 0),
(561, '15722', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 40144, 0),
(562, '15724', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 11957, 0),
(563, '15737', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 3698, 0),
(564, '15739', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 38536, 0),
(565, '15743', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 34153, 0),
(566, '15751', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 53506, 0),
(567, '15756', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 16255, 0),
(568, '15758', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 52307, 0),
(569, '15767', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 10459, 0);
INSERT INTO `riwayatkelassiswa` (`replid`, `nis`, `idkelas`, `mulai`, `aktif`, `status`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(570, '15770', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 14199, 0),
(571, '15779', 66, '2013-02-03', 1, 0, NULL, NULL, NULL, NULL, '2013-02-03 23:41:21', 1581, 0),
(591, '15519', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 18841, 0),
(592, '15575', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 17379, 0),
(593, '15580', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 29705, 0),
(594, '15592', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 6728, 0),
(595, '15610', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 63863, 0),
(596, '15645', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 12631, 0),
(597, '15647', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 6968, 0),
(598, '15659', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 19758, 0),
(599, '15664', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 16357, 0),
(600, '15672', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 54211, 0),
(601, '15679', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 28920, 0),
(602, '15686', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 63558, 0),
(603, '15699', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 44316, 0),
(604, '15732', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 59903, 0),
(605, '15760', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 57073, 0),
(606, '15772', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 65074, 0),
(607, '15773', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 60744, 0),
(608, '15777', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 22634, 0),
(609, '15782', 67, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:01:57', 39513, 0),
(640, '15503', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 52427, 0),
(641, '15505', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 42725, 0),
(642, '15510', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 36536, 0),
(643, '15517', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 13, 0),
(644, '15521', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 63007, 0),
(645, '15522', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 59865, 0),
(646, '15527', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 8095, 0),
(647, '15542', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 14186, 0),
(648, '15549', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 64412, 0),
(649, '15561', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 51716, 0),
(650, '15587', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 39041, 0),
(651, '15588', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 23148, 0),
(652, '15590', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 11367, 0),
(653, '15596', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 22231, 0),
(654, '15799', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 29841, 0),
(655, '15788', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 17749, 0),
(656, '15623', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 24426, 0),
(657, '15624', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 33195, 0),
(658, '15626', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 30159, 0),
(659, '15643', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 9274, 0),
(660, '15652', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 58656, 0),
(661, '15654', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 51573, 0),
(662, '15660', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 51924, 0),
(663, '15730', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 9191, 0),
(664, '15748', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 54108, 0),
(665, '15766', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 17572, 0),
(666, '15775', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 10324, 0),
(667, '15778', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 23816, 0),
(668, '15780', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 53784, 0),
(669, '15781', 68, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:04:07', 55542, 0),
(670, '15502', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 42341, 0),
(671, '15512', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 52808, 0),
(672, '15539', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 1461, 0),
(673, '15543', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 1639, 0),
(674, '15550', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 26114, 0),
(675, '15555', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 13381, 0),
(676, '15569', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:37', 37724, 0),
(677, '15570', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 35404, 0),
(678, '15573', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 44478, 0),
(679, '15802', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 46637, 0),
(680, '16115', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 39919, 0),
(681, '15618', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 33946, 0),
(682, '15795', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 65143, 0),
(683, '15634', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 35434, 0),
(684, '15661', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 27362, 0),
(685, '15663', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 52643, 0),
(686, '15683', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 65078, 0),
(687, '15688', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 49648, 0),
(688, '15691', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 52179, 0),
(689, '15713', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 16051, 0),
(690, '15725', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 6286, 0),
(691, '15729', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 3940, 0),
(692, '15790', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 14390, 0),
(693, '15794', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 61817, 0),
(694, '15755', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 35241, 0),
(695, '15764', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 36203, 0),
(696, '15787', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 44303, 0),
(697, '15771', 69, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:05:38', 11084, 0),
(698, '15192', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 18123, 0),
(699, '15193', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 36557, 0),
(700, '15260', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 10577, 0),
(701, '15272', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 55697, 0),
(702, '15298', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 41744, 0),
(703, '15328', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 808, 0),
(704, '15336', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 51530, 0),
(705, '15340', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 37678, 0),
(706, '15453', 71, '2013-02-04', 1, 0, NULL, NULL, NULL, NULL, '2013-02-04 00:07:44', 50181, 0),
(707, '15180', 80, '2010-07-02', 0, 0, NULL, NULL, NULL, NULL, '2013-03-25 13:19:03', 31862, 0),
(708, '15188', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 24562, 0),
(709, '15195', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 36597, 0),
(710, '15203', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 36527, 0),
(711, '15204', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 33677, 0),
(712, '15212', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 4119, 0),
(713, '15220', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 10007, 0),
(714, '15226', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 34002, 0),
(715, '15232', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 25374, 0),
(716, '15242', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 63387, 0),
(717, '15244', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 60843, 0),
(718, '15258', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 10532, 0),
(719, '15259', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 51642, 0),
(720, '15264', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 46211, 0),
(721, '15267', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 8966, 0),
(722, '15277', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 59333, 0),
(723, '15278', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 48525, 0),
(724, '15287', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 51970, 0),
(725, '15291', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 29623, 0),
(726, '15294', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 49184, 0),
(727, '15299', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 54998, 0),
(728, '15301', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 57460, 0),
(729, '15307', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 31905, 0),
(730, '15312', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 17991, 0),
(731, '15313', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 53551, 0),
(732, '15320', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 63064, 0),
(733, '15359', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 60575, 0),
(734, '15367', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 63349, 0),
(735, '15371', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 24797, 0),
(736, '15403', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 11159, 0),
(737, '15413', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 26590, 0),
(738, '15491', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 56539, 0),
(739, '15420', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 4407, 0),
(740, '15423', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 5530, 0),
(741, '15426', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 61237, 0),
(742, '15447', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 31476, 0),
(743, '15452', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 12798, 0),
(744, '15458', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 42926, 0),
(745, '15462', 80, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:01:32', 8565, 0),
(746, '15179', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 31575, 0),
(747, '15494', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 63457, 0),
(748, '15198', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 30127, 0),
(749, '15224', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 41511, 0),
(750, '15485', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 17264, 0),
(751, '15236', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 63735, 0),
(752, '15240', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 45565, 0),
(753, '15252', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 23927, 0),
(754, '15253', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 28548, 0),
(755, '15284', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 65077, 0),
(756, '15286', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 44797, 0),
(757, '15297', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 57383, 0),
(758, '15302', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 39073, 0),
(759, '15317', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 3708, 0),
(760, '15323', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 28051, 0),
(761, '15330', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 46952, 0),
(762, '15335', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 49941, 0),
(763, '15343', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 40780, 0),
(764, '15375', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 13645, 0),
(765, '15396', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 56001, 0),
(766, '15431', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 48813, 0),
(767, '16111', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 33931, 0),
(768, '15478', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 20328, 0),
(769, '15495', 96, '2010-07-02', 1, 0, NULL, NULL, NULL, NULL, '2013-02-13 18:44:33', 54816, 0),
(770, '12345678', 81, '2013-02-14', 1, 0, NULL, NULL, NULL, NULL, '2013-04-23 16:24:23', 19972, 0),
(771, '12345', 53, '2016-01-26', 1, 3, NULL, NULL, NULL, NULL, '2016-01-26 15:24:30', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rpp`
--

CREATE TABLE IF NOT EXISTS `rpp` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idtingkat` int(10) unsigned NOT NULL,
  `idsemester` int(10) unsigned NOT NULL,
  `idpelajaran` int(10) unsigned NOT NULL,
  `koderpp` varchar(20) NOT NULL,
  `rpp` varchar(255) NOT NULL,
  `deskripsi` text,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_rpp_tingkat` (`idtingkat`),
  KEY `FK_rpp_semester` (`idsemester`),
  KEY `FK_rpp_pelajaran` (`idpelajaran`),
  KEY `IX_rpp_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rpp`
--


-- --------------------------------------------------------

--
-- Table structure for table `sas`
--

CREATE TABLE IF NOT EXISTS `sas` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL DEFAULT '',
  `idaturan` int(10) unsigned NOT NULL DEFAULT '0',
  `idsikap` int(3) unsigned NOT NULL DEFAULT '0',
  `idsemester` int(3) unsigned NOT NULL DEFAULT '0',
  `idkelas` int(4) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=213 ;

--
-- Dumping data for table `sas`
--

INSERT INTO `sas` (`replid`, `nis`, `idaturan`, `idsikap`, `idsemester`, `idkelas`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(143, '15819', 4, 18, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(144, '15836', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(145, '15855', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(146, '15877', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(147, '15885', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(148, '15887', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(149, '15892', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(150, '15908', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(151, '15922', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(152, '15926', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(153, '15929', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(154, '15937', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(155, '15939', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(156, '15941', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(157, '15943', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(158, '15952', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(159, '15965', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(160, '15966', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(161, '15976', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(162, '15978', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(163, '16007', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(164, '16018', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(165, '16025', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(166, '16032', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(167, '16040', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(168, '16041', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(169, '16043', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(170, '16045', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(171, '16057', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(172, '16063', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(173, '16067', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(174, '16081', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(175, '16085', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(176, '16100', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(177, '16103', 4, 15, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:20', 0, 0),
(178, '15819', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(179, '15836', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(180, '15855', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(181, '15877', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(182, '15885', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(183, '15887', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(184, '15892', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(185, '15908', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(186, '15922', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(187, '15926', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(188, '15929', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(189, '15937', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(190, '15939', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(191, '15941', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(192, '15943', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(193, '15952', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(194, '15965', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(195, '15966', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(196, '15976', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(197, '15978', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(198, '16007', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(199, '16018', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(200, '16025', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(201, '16032', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(202, '16040', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(203, '16041', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(204, '16043', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(205, '16045', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(206, '16057', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(207, '16063', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(208, '16067', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(209, '16081', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(210, '16085', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(211, '16100', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0),
(212, '16103', 5, 14, 1, 50, NULL, NULL, NULL, '2016-04-26 08:32:36', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE IF NOT EXISTS `sekolah` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namasekolah` varchar(45) NOT NULL,
  `gambar` blob,
  `koordinat` text,
  `wilayah` int(10) unsigned NOT NULL,
  `titikpusat` varchar(10) NOT NULL,
  `lokasi` varchar(10) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_sekolah_wilayah` (`wilayah`),
  KEY `IX_sekolah_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sekolah`
--


-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `semester` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_semester_departemen` (`departemen`),
  KEY `IX_semester_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`replid`, `semester`, `departemen`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'Genap', 'SMAN 1 Malang', 1, 'Semester Aktif', NULL, NULL, NULL, '2016-01-22 09:57:04', 0, 0),
(2, 'Ganjil', 'SMAN 1 Malang', 0, '', NULL, NULL, NULL, '2016-01-22 09:57:13', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settingpsb`
--

CREATE TABLE IF NOT EXISTS `settingpsb` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idproses` int(10) unsigned NOT NULL,
  `kdsum1` varchar(5) DEFAULT NULL,
  `nmsum1` varchar(100) DEFAULT NULL,
  `kdsum2` varchar(5) DEFAULT NULL,
  `nmsum2` varchar(100) DEFAULT NULL,
  `kdujian1` varchar(5) DEFAULT NULL,
  `nmujian1` varchar(100) DEFAULT NULL,
  `kdujian2` varchar(5) DEFAULT NULL,
  `nmujian2` varchar(100) DEFAULT NULL,
  `kdujian3` varchar(5) DEFAULT NULL,
  `nmujian3` varchar(100) DEFAULT NULL,
  `kdujian4` varchar(5) DEFAULT NULL,
  `nmujian4` varchar(100) DEFAULT NULL,
  `kdujian5` varchar(5) DEFAULT NULL,
  `nmujian5` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_settingpsb_prosespsb` (`idproses`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `settingpsb`
--


-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `nisn` varchar(50) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `panggilan` varchar(30) DEFAULT NULL,
  `aktif` tinyint(1) unsigned DEFAULT '1',
  `tahunmasuk` int(10) unsigned DEFAULT NULL,
  `idangkatan` int(10) unsigned DEFAULT NULL,
  `idkelas` int(10) unsigned DEFAULT NULL,
  `suku` varchar(20) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `kondisi` varchar(100) DEFAULT NULL,
  `kelamin` varchar(1) DEFAULT NULL,
  `tmplahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `warga` varchar(5) DEFAULT NULL,
  `anakke` tinyint(2) unsigned DEFAULT '0',
  `jsaudara` tinyint(2) unsigned DEFAULT '0',
  `bahasa` varchar(60) DEFAULT NULL,
  `berat` decimal(4,1) unsigned DEFAULT '0.0',
  `tinggi` decimal(4,1) unsigned DEFAULT '0.0',
  `darah` varchar(2) DEFAULT NULL,
  `foto` mediumblob,
  `alamatsiswa` varchar(255) DEFAULT NULL,
  `kodepossiswa` varchar(8) DEFAULT NULL,
  `telponsiswa` varchar(20) DEFAULT NULL,
  `hpsiswa` varchar(20) DEFAULT NULL,
  `emailsiswa` varchar(100) DEFAULT NULL,
  `kesehatan` varchar(150) DEFAULT NULL,
  `asalsekolah` varchar(100) DEFAULT NULL,
  `ketsekolah` varchar(100) DEFAULT NULL,
  `namaayah` varchar(60) DEFAULT NULL,
  `namaibu` varchar(60) DEFAULT NULL,
  `almayah` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `almibu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pendidikanayah` varchar(20) DEFAULT NULL,
  `pendidikanibu` varchar(20) DEFAULT NULL,
  `pekerjaanayah` varchar(60) DEFAULT NULL,
  `pekerjaanibu` varchar(60) DEFAULT NULL,
  `wali` varchar(60) DEFAULT NULL,
  `penghasilanayah` int(10) unsigned DEFAULT '0',
  `penghasilanibu` int(10) unsigned DEFAULT '0',
  `alamatortu` varchar(100) DEFAULT NULL,
  `telponortu` varchar(20) DEFAULT NULL,
  `hportu` varchar(20) DEFAULT NULL,
  `emailayah` varchar(100) DEFAULT NULL,
  `alamatsurat` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `frompsb` tinyint(1) unsigned DEFAULT '0',
  `ketpsb` varchar(100) DEFAULT NULL,
  `statusmutasi` int(10) unsigned DEFAULT NULL,
  `alumni` tinyint(1) unsigned DEFAULT '0' COMMENT '0 bukan alumni, 1 alumni',
  `pinsiswa` varchar(25) DEFAULT NULL,
  `pinortu` varchar(25) DEFAULT NULL,
  `pinortuibu` varchar(25) DEFAULT NULL,
  `emailibu` varchar(100) DEFAULT NULL,
  `info1` varchar(20) DEFAULT NULL,
  `info2` varchar(20) DEFAULT NULL,
  `info3` varchar(20) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`nis`),
  UNIQUE KEY `UX_siswa_replid` (`replid`),
  KEY `FK_siswa_angkatan` (`idangkatan`),
  KEY `FK_siswa_suku` (`suku`),
  KEY `FK_siswa_agama` (`agama`),
  KEY `FK_siswa_status` (`status`),
  KEY `FK_siswa_kondisi` (`kondisi`),
  KEY `FK_siswa_pendidikanayah` (`pendidikanayah`),
  KEY `FK_siswa_pendidikanibu` (`pendidikanibu`),
  KEY `FK_siswa_pekerjaanayah` (`pekerjaanayah`),
  KEY `FK_siswa_pekerjaanibu` (`pekerjaanibu`),
  KEY `FK_siswa_statusmutasi` (`statusmutasi`),
  KEY `FK_siswa_kelas` (`idkelas`),
  KEY `FK_siswa_asalsekolah` (`asalsekolah`),
  KEY `IX_siswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1052 ;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1051, '', NULL, '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-22 11:13:55', 0, 0),
(39, '12345', '15810', 'AFNAN', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12345', '84447', '47882', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 57925, 0),
(1050, '12345678', '987654321', 'Tes', 'tes', 1, 2014, 22, 81, 'Jawa', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'tes', '1993-02-03', 'WNI', 1, 3, 'Indonesia', '60.0', '160.0', 'A', '', 'Malang', NULL, '085649847221', '085649847221', 'ahsanunnaseh@yahoo.com', '', 'SMP EDIIDE', '', 'ayah tes', 'ibu tes', 1, 0, 'S1', 'SMA', 'PNS', 'Wiraswasta', '', 0, 0, 'Malang', '', '', '', 'Malang', '', 0, NULL, NULL, 0, '07825', '78798', '47867', '', NULL, NULL, NULL, '2013-04-23 16:24:23', 62823, 0),
(1026, '15179', '9958734647', 'ADITYA RAHADIYANSYAH', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Belum Ada Data', 'Reguler', 'l', 'MLG', '1994-12-30', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Berkecukupan', 'Jl. Dana', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78211', '94003', '71962', '', NULL, NULL, NULL, '2013-02-13 18:44:32', 29862, 0),
(987, '15180', '9958039192', 'ADLY MARDIKA', NULL, 0, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-04-09', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Griya Permata Alam FA-15, Karangploso', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUMARYONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 1, '73905', '75575', '60255', '', NULL, NULL, NULL, '2013-03-25 13:19:03', 36318, 0),
(988, '15188', '9950040403', 'AJENG AYU HAPSARI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', '', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-01-28', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Mulyodadi 3 Dau, Kab. Malang', '65151', '', '', '', NULL, 'Belum Ada Data', NULL, 'HARTOYO S', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27941', '65071', '17874', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 16385, 0),
(693, '15192', '15192', 'ALFIERA SAVITRI', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12218', '14116', '52542', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 59018, 0),
(694, '15193', '15193', 'ALIEFIA RIZKY DIWANDANA', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'p', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16385', '14639', '21026', '', NULL, NULL, NULL, '2013-02-08 13:53:00', 25933, 0),
(989, '15195', '9953745632', 'AMBHARI PARAMASTRYA PUTRI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'SLEMAN', '1995-04-28', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Bromo no.98-100 Kepanjen', '65163', '', '', '', NULL, 'Belum Ada Data', NULL, 'BOEDI PRIJATNO (ALM)', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25806', '85414', '24469', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 39261, 0),
(1028, '15198', '9958453874', 'ANASTASIA KARINA DEWI ASTUTI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Katolik', 'Reguler', 'Berkecukupan', 'l', 'UJG PDG', '1995-07-09', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Perum.Griya Permata Alam blok KB no.34 Malang', '65152', '', '', '', NULL, 'Belum Ada Data', NULL, 'BENYDIKTAS SIGIT SANTOSA,S.Pd', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '69889', '16248', '44799', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 44636, 0),
(990, '15203', '9942164933', 'ANGGITA ROSIANA PUTRI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1994-12-30', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Kauman 1 Ngebruk, Sumberpucung kab. Malang', '65165', '', '', '', NULL, 'Belum Ada Data', NULL, 'WIJI YULIANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29005', '28071', '62611', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 11720, 0),
(991, '15204', '9958453382', 'ANGGUN RENDRA ARVIANSYAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-08-18', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Kelud 11 (ASPOL) Malang', '65115', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUWARNO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32852', '62861', '33420', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 8564, 0),
(992, '15212', '9952141442', 'ARIO WASKITO', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'SEMARANG', '1995-11-21', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Perum Gadang Regency N-3 Malang', '65149', '', '', '', NULL, 'Belum Ada Data', NULL, 'WARSITO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '31042', '97083', '88057', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 41021, 0),
(993, '15220', '9958597472', 'AULIA AZA FUADIPUTRI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-04-04', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Kendalsari IV/31 Malang', '65141', '', '', '', NULL, 'Belum Ada Data', NULL, 'ACHMAD B BARRUL FUAD', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '37756', '70911', '41219', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 36029, 0),
(1029, '15224', '9958816522', 'AZUMA PRASTUTISARI MARIELA', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1996-11-09', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Perunggu Selatan No.11 Malang', '65122', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUPRAYITNO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '35570', '48600', '90761', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 52215, 0),
(994, '15226', '9958816479', 'BALQIS ISTIQOMAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-04-18', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Danau Tursiah C5A-6 Malang', '65138', '', '', '', NULL, 'Belum Ada Data', NULL, 'AGUS TAUFIQ', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98566', '49044', '51598', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 5390, 0),
(995, '15232', '9958596041', 'CAESARANI TRI HANDAYANI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-05-11', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Pondok Blimbing Indah N6 /11 MALANG', '65125', '', '', '', NULL, 'Belum Ada Data', NULL, 'Ir. Hariyadi', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67398', '11617', '23170', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 48166, 0),
(1031, '15236', '9958453324', 'CHALIF RAFI PRAYOGI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1996-12-19', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl S Supriyadi 4/2310', '65114', '', '', '', NULL, 'Belum Ada Data', NULL, 'SIGIT WASESO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91573', '94194', '91217', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 47433, 0),
(1032, '15240', '9958735291', 'CITRA NANDA ISLAMI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1996-01-22', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Band. Palmerah X K/18B Villa Gunung Buring Malang', '65138', '', '', '', NULL, 'Belum Ada Data', NULL, 'NUR MOHAMMAD ARIFIN', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58987', '82695', '87983', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 24677, 0),
(996, '15242', '9958735129', 'DANAR YAZID PERMANA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-10-15', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Danau Bratan Timur 1 H8a/20', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'SULASMONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14746', '11731', '64824', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 38054, 0),
(997, '15244', '9962280616', 'DARIN RAUSAN FIKRI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1996-03-05', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Perum Bumi Palapa D/11', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'CHOLID TRI TJAHJONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98319', '48449', '56832', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 40027, 0),
(1033, '15252', '9958734636', 'DEWANGGA KURNIAWAN ', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-07-21', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Haji Alwi tirtomoyo Pakis Kab. Malang', '65139', '', '', '', NULL, 'Belum Ada Data', NULL, 'SISWO SUDARMANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58363', '33430', '76677', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 38112, 0),
(1034, '15253', '9947958435', 'DHIMAS ARFIANSYAH', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'BLT', '1995-07-28', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jalan Ariesmunandar gang 05/576', '41158', '', '', '', NULL, 'Belum Ada Data', NULL, 'RAMLI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '61721', '76313', '76930', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 23569, 0),
(998, '15258', '9958734673', 'DIMAS SURYA DIRGANTARA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-08-24', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jln S. Supriadi 2C/15 Malang', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUJONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24282', '87083', '88747', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 12176, 0),
(999, '15259', '9958816481', 'DINDA MIFTAKHUL ROISYAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-04-21', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Arjuno I/ 1153 malang', '65119', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUGENG HARIYANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45511', '83812', '60432', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 24421, 0),
(695, '15260', '15260', 'DINDA NALURITA KRISTANTI', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60821', '84394', '79949', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 22427, 0),
(1000, '15264', '9958453795', 'DYAN EKA MAULANA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Katolik', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-02-18', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'JL KOL SUGIONO 3A NO 51', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'Sunarji Tri C', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76921', '65168', '93986', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 52204, 0),
(1001, '15267', '9958816538', 'EKA NUR YULIANA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-07-18', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Ikan Tombro Selatan No. 48, Malang', '65125', '', '', '', NULL, 'Belum Ada Data', NULL, 'TRI AGUS PRIYONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94176', '38307', '65314', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 43830, 0),
(696, '15272', '15272', 'ERA MARDHIKA', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67882', '42640', '13124', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 45487, 0),
(1002, '15277', '9952385129', 'FAHMI ZULFIKAR AHSAN SUSANTO', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-12-24', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Ikan Piranha Atas 220 kav. 9 Malang', '65142', '', '', '', NULL, 'Belum Ada Data', NULL, 'RADIUS EKO SUSANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38292', '21011', '70909', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 65404, 0),
(1003, '15278', '9958816525', 'FAIZAL ANUGRAH BHASWARA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-06-25', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'jl. Gotong royong no 35 pakisaji', '65162', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUBEJAN SUBIANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50620', '98175', '12134', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 51596, 0),
(1035, '15284', '9950922749', 'FARIZ ACHMAD', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'JKT', '1995-07-10', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Bantaran Indah G-45 Malang', '65141', '', '', '', NULL, 'Belum Ada Data', NULL, 'AHMAD FADILLAH', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44125', '98288', '24989', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 4551, 0),
(1036, '15286', '9958596703', 'FEBRIANO AKBAR RAMADHANI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-06-02', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Gajayana gang 5 no 608B malang', '65144', '', '', '', NULL, 'Belum Ada Data', NULL, 'Martono Dwi Atmadja', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74291', '75979', '86401', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 55343, 0),
(1004, '15287', '9952002367', 'FEMBI REKRISNA GRANDEA PUTRA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-11-04', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Pondok Kopi Estate No. 10 Malang', '65141', '', '', '', NULL, 'Belum Ada Data', NULL, 'EDY RUSWANTO, S.E.', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '30720', '13167', '38952', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 2683, 0),
(1005, '15291', '9950040509', 'FIRA WIJI UTAMI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-06-04', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Terusan Ikan Piranha Atas No.124 Malang', '65142', '', '', '', NULL, 'Belum Ada Data', NULL, 'OKE HERMADI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91521', '75338', '36531', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 1037, 0),
(1006, '15294', '9958597657', 'FISABELLA RILAMSARI PUTRI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-04-11', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Kendalsari 4 kav. 12 Malang', '65141', '', '', '', NULL, 'Belum Ada Data', NULL, 'Ir. IWAN GURITNO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '18657', '60542', '63453', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 10519, 0),
(1037, '15297', '9958453788', 'GAGAH RIZKI RAMADHANI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'DPS', '1995-08-05', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Ciliwung II A/3 Malang', '65122', '', '', '', NULL, 'Belum Ada Data', NULL, 'DHARMAWAN BOEDI RACHMANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70554', '94835', '46082', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 45067, 0),
(697, '15298', '15298', 'GARNIS YOGA P. N. P', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47766', '26545', '30334', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 19022, 0),
(1007, '15299', '9947958356', 'GERINDRA PANJI FIRMAN HERDINATA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-05-16', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Kenanga no. 73 A Sekarpuro Pakis kab. Malang', '65154', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUWARNO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94702', '21060', '83682', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 27099, 0),
(1008, '15301', '9957011218', 'GISKA KOESUMASARI PUTRI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-08-08', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Sarimun no.40 Batu', '65326', '', '', '', NULL, 'Belum Ada Data', NULL, 'Drs. KOESWARDIYONO, M.M. (alm)', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59643', '30684', '79620', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 49047, 0),
(1038, '15302', '9940660717', 'GRESELA YOUNIGA', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MDN', '1995-05-05', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Tirto Mulyo, Perum. Graha Sejahtera A/ 14', '65151', '', '', '', NULL, 'Belum Ada Data', NULL, 'Drs. SUGENG RUSMIWARI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76951', '25828', '72643', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 14093, 0),
(1009, '15307', '9962029959', 'HANIATUS ZULFATUL ALIYAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1996-01-17', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'jl. Bubutan no.4 malang', '65125', '', '', '', NULL, 'Belum Ada Data', NULL, 'HARIYANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15779', '18569', '75777', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 46585, 0),
(1010, '15312', '9968677800', 'HEALZA KURNIA HENDIASTUTJIK', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1996-01-25', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Telaga Warna A 21 Malang', '65144', '', '', '', NULL, 'Belum Ada Data', NULL, 'ACHMAD TUTJIK MOECHID', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44672', '85529', '23270', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 43422, 0),
(1011, '15313', '9958816517', 'HERLINA WULANDARI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-06-15', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'jalan selat sunda raya d1/27 malang', '65138', '', '', '', NULL, 'Belum Ada Data', NULL, 'HENDRARTO HADISURYO, S.H', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '37625', '60833', '15743', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 53423, 0),
(1039, '15317', '9962280594', 'ILMI NAVIANTI RUSYDI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-05-22', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Bukit Dieng Block T No 9 - 10 Malang', '65139', '', '', '', NULL, 'Belum Ada Data', NULL, 'SYAIFUL RUSDI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51468', '98523', '20751', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 41702, 0),
(1012, '15320', '9952149988', 'INDRAYADI DWI PRAKOSO', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'SAMARINDA', '1995-08-15', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Teluk Etna VII kav.98 / No.10', '65125', '', '', '', NULL, 'Belum Ada Data', NULL, 'TJIPTO RAHADI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47231', '14389', '20737', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 21507, 0),
(1040, '15323', '9958596855', 'INNEKE AMELIA BRAMESWARI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-09-05', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Plaosan timur no 121A/6', '65125', '', '', '', NULL, 'Belum Ada Data', NULL, 'EKO PRIYONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '99908', '78214', '28979', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 6782, 0),
(698, '15328', '15328', 'IZZATUL AULIA ', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66956', '40885', '18465', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 31477, 0),
(1041, '15330', '9952160008', 'JODDI ADITYA INDRAWAN', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-07-29', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Mondokaki No.27 Malang', '65139', '', '', '', NULL, 'Belum Ada Data', NULL, 'RUDY IRMAWAN', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85825', '89600', '84125', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 6367, 0),
(1042, '15335', '9956457622', 'KEKE GENIO', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-04-24', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Sukoanyar Plalar No.42 Pakis Malang', '65154', '', '', '', NULL, 'Belum Ada Data', NULL, 'HERMANTO HALIM', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '68513', '56091', '17177', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 64070, 0),
(699, '15336', '15336', 'KEN LAKSMI MUNINGGAR', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22336', '65477', '67573', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 38637, 0),
(700, '15340', '15340', 'KHOIRUNNISA`', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '87927', '58728', '32794', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 13018, 0),
(1043, '15343', '9945313175', 'LAKSMI KEN FITRI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'PSR', '1995-10-29', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Perum Alam Hijau Lestari Blok O no.4 Singosari', '65153', '', '', '', NULL, 'Belum Ada Data', NULL, 'ADI KUSNANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24788', '13465', '44821', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 38883, 0),
(1013, '15359', '9958597780', 'MIFTACH KARIMA HADI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'BALIKPAPAN', '1995-02-04', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Vila Bukit Tidar A3-713 Malang', '65145', '', '', '', NULL, 'Belum Ada Data', NULL, 'MUHADI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86283', '39105', '90605', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 15965, 0),
(1014, '15367', '', 'MOHAMMAD INGGIL TAUFANI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'PAPUA', '1995-04-17', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Joyo Utomo 504 Malang', '65144', '', '', '', NULL, 'Belum Ada Data', NULL, 'ACHNAN ROSYADI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52260', '64124', '13555', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 41202, 0),
(1015, '15371', '9950040512', 'MUHAMMAD ADIANSYAH ARIFAH PUTRA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'BANDUNG', '1995-06-06', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Perum. Pandanwangi Park Kav. 52 Malang', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'ABUBAKAR', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62689', '77598', '45282', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 47298, 0),
(1044, '15375', '9958596084', 'MUHAMMAD HAFID YUSUF', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-12-18', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Papa Biru 37 Malang', '65141', '', '', '', NULL, 'Belum Ada Data', NULL, 'YUSUF', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58178', '29848', '70573', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 4596, 0),
(1045, '15396', '9952160034', 'NISRINA HABIBATY', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'SBY', '1995-07-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Candi Sari No.17 Malang', '65142', '', '', '', NULL, 'Belum Ada Data', NULL, 'BAMBANG SAMIONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95283', '23193', '36124', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 38657, 0),
(1016, '15403', '9958735098', 'NURANI LATHIFAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Katolik', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-08-27', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Danau Bratan Timur IV/D29', '65138', '', '', '', NULL, 'Belum Ada Data', NULL, 'ABD. RABI`', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27880', '32760', '21745', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 50179, 0),
(1017, '15413', '9961887738', 'PUGUH ROHMANU RIZKA PRATAMA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Katolik', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1996-03-05', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'PERUMAHAN JOYO GRAND i1/11', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'WARSANA ADI SOETANTA', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62215', '36854', '33270', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 50518, 0),
(1019, '15420', '9953745569', 'RANNY ETNA MELATI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-02-09', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Kendalpayak no. 68 Pakisaji, Malang', '65162', '', '', '', NULL, 'Belum Ada Data', NULL, 'HARRY SANTOSA', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '20304', '44532', '17622', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 27, 0),
(1020, '15423', '9958735054', 'REFFANY DYAH SEPTATIWI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-09-05', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Wijaya Kusuma no.26 Lowokwaru Malang', '65141', '', '', '', NULL, 'Belum Ada Data', NULL, 'SAPTO EDHI SUMANTRI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85692', '32513', '12441', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 6235, 0),
(1021, '15426', '9958596965', 'REIZA ADI CAHYA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-10-17', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'jl. I.R. Rais 288C', '65116', '', '', '', NULL, 'Belum Ada Data', NULL, 'IZA RUDIANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95131', '15948', '22645', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 39319, 0),
(1046, '15431', '9945632754', 'REYSKA DENADA PUTRI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'PRB', '1995-05-25', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Danau Sentani Tengah IV H2 D12 Sawojajar Malang', '65139', '', '', '', NULL, 'Belum Ada Data', NULL, 'DEDI HARTONO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74192', '35764', '17959', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 19551, 0),
(1022, '15447', '9958453604', 'SABRINA ROSELINI', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-06-09', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'jl. I.R. Rais 14', '', '', '', '', NULL, 'Belum Ada Data', NULL, 'SUJALI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88862', '93093', '52332', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 28290, 0),
(1023, '15452', '9952384974', 'SHARFINA MAHDIYA ANISAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-03-26', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Pondok Bestari Indah C1/81', '65151', '', '', '', NULL, 'Belum Ada Data', NULL, 'ACHMAD SJAIFULLAH', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33208', '54289', '70948', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 22043, 0),
(701, '15453', '15453', 'SHARA AZZURAIDA', NULL, 1, 2013, 22, 71, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84099', '73084', '57764', '', NULL, NULL, NULL, '2013-02-04 00:07:44', 63714, 0),
(1024, '15458', '9958453986', 'SISKA PUTRI AYU NINGTIYAS', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1995-11-19', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'jl. Candi Telagawangi II A/25', '65142', '', '', '', NULL, 'Belum Ada Data', NULL, 'EKO BUDIANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25794', '32372', '31436', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 16635, 0),
(1025, '15462', '9958597754', 'TASYA ESTU HIDAYANA', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Islam', 'Berkecukupan', 'Reguler', 'l', 'LANGSA', '1995-03-25', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Raya Mondoroko 4 Blok C1 No.5', '65153', '', '', '', NULL, 'Belum Ada Data', NULL, 'TUKIMIN HADI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72847', '52441', '56935', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 14764, 0),
(1048, '15478', '9958735087', 'YULIA YASMIN', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'SBY', '1995-10-28', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl.Danau Kerinci Raya G6E/24 Sawojajar Malang', '65139', '', '', '', NULL, 'Belum Ada Data', NULL, 'MOH. IBRAHIM', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14360', '21335', '77190', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 12144, 0),
(1030, '15485', '9954721356', 'BOBBY AKBAR ADIYASA ', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '2695-01-21', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Brigjen. Katamso 12 Malang', '65139', '', '', '', NULL, 'Belum Ada Data', NULL, 'DRESTANTO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81889', '72027', '57155', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 23130, 0),
(1018, '15491', '9962280639', 'QONITAH MEER WAHIDAH', NULL, 1, 2014, 20, 80, 'Belum Ada Data', 'Katolik', 'Berkecukupan', 'Reguler', 'l', 'MALANG', '1996-03-30', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Pondok Blimbing Indah B2/18 Malang', '65126', '', '', '', NULL, 'Belum Ada Data', NULL, 'HERU LUKITO', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96977', '82218', '54107', '', NULL, NULL, NULL, '2013-02-13 18:01:32', 65379, 0),
(1027, '15494', '9947737908', 'AKHMAD SYAHRONI YUSUF', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1997-04-04', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jalan Danau Jempang I E2E No. 04 Malang', '65138', '', '', '', NULL, 'Belum Ada Data', NULL, 'BUDIONO (ALMARHUM)', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74915', '12439', '51124', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 6541, 0),
(1049, '15495', '9964784555', 'ZAINIYYATUN JAMILAH', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'p', 'PSR', '1995-04-02', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'J. Susukan Rejo no. 37 Pasuruan', '67171', '', '', '', NULL, 'Belum Ada Data', NULL, 'MUHAMMAD JAMIL', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21072', '24621', '78365', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 28267, 0),
(351, '15498', '15498', 'ABID SUGIASTU ISMIRZA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14789', '71606', '29178', '', NULL, NULL, NULL, '2013-02-03 23:30:21', 64797, 0),
(459, '15500', '15500', 'ADELINA KHOLIFAH PUTRI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '40195', '49907', '26674', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 36862, 0),
(531, '15501', '15501', 'ADHIKA NUGRAHA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79561', '89990', '15562', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 30636, 0),
(665, '15502', '15502', 'ADHINTA SALSABIILA', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72711', '26937', '99549', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 12987, 0),
(635, '15503', '15503', 'ADI PRAYOGO', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83760', '67377', '45499', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 28447, 0),
(352, '15504', '15504', 'ADINDA KUSUMANINGDIAH TANTRI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42062', '62177', '82429', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 44639, 0),
(636, '15505', '15505', 'AFAN JANNA AVISHA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '13842', '76120', '28290', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 61517, 0),
(353, '15506', '15506', 'AGHNIA HANINDITA CINDY FATIKHAH', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93632', '21471', '93573', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 52277, 0),
(533, '15507', '15507', 'AGUNG RISKY SETYAWAN', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '31309', '21283', '77874', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 1927, 0),
(461, '15508', '15508', 'AHMAD SYAUQI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25614', '34685', '25993', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 39678, 0),
(424, '15509', '15509', 'ALDY HANIF WIBISONO', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75261', '82647', '42444', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 36187, 0),
(637, '15510', '15510', 'ALDY PRADANA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28341', '63001', '87669', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 24834, 0),
(387, '15511', '15511', 'ALEXANDRA FEBRIANTO', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33619', '91787', '96360', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 34595, 0),
(666, '15512', '15512', 'ALFIAN WIDATMOKO', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '46968', '19320', '21828', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 58077, 0),
(354, '15513', '15513', 'ALICIA HERIERA ANDIKA ASTU GEMILANG', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78171', '33186', '55785', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 27995, 0),
(534, '15514', '15514', 'ALIFA AYU MIRANTI HARTONO', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63716', '60086', '71179', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 49577, 0),
(495, '15515', '15515', 'ALIFIA NURLITA', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '39029', '66821', '13960', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 20944, 0),
(388, '15516', '15516', 'ALLDO RAAFI` ILMAN', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44829', '80885', '11305', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 13028, 0),
(638, '15517', '15517', 'ALVIN MAHAMIDI', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67792', '51482', '55533', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 63634, 0),
(462, '15518', '15518', 'AMALIA INSANI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '89589', '11792', '85683', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 14472, 0),
(586, '15519', '15519', 'ANANDYA SARASWATI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '90673', '80799', '10200', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 10017, 0),
(496, '15520', '15520', 'ANANTHA YULLIAN SUKMADEWA', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '23498', '73004', '45396', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 55166, 0),
(639, '15521', '15521', 'ANDI ANDANG YEHUDA NATHANAEL P.', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67492', '41267', '86015', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 29681, 0),
(640, '15522', '15522', 'ANDREAN DWI ANDARU', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59851', '18223', '47776', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 1001, 0),
(497, '15523', '15523', 'ANDREAS BUDIYANTO SURANTA TARIGAN', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '49392', '76986', '73798', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 46995, 0),
(425, '15524', '15524', 'ANDREAS YACOBUS', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24801', '33517', '49958', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 34890, 0),
(389, '15525', '15525', 'ANGGITA PUSPITASARI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96102', '36154', '35271', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 42624, 0),
(355, '15526', '15526', 'ANGRAINI AYU WULANDARI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34584', '52438', '20638', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 41786, 0),
(641, '15527', '15527', 'ANINDYA ERINA WIDIANTY', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '10763', '23622', '82217', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 65014, 0),
(356, '15528', '15528', 'ANINDYA RIZKA FARINA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45959', '72525', '50453', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 31270, 0),
(390, '15529', '15529', 'ANISA CLAUDINA', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85474', '65278', '69696', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 43738, 0),
(391, '15530', '15530', 'ANNISA NOVIDYA UTAMI HARAHAP', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34011', '78207', '83518', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 54594, 0),
(535, '15531', '15531', 'ANYS KHOIRIYAH', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94701', '99818', '21324', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 47848, 0),
(357, '15532', '15532', 'ANZIL AZIZA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '35905', '87015', '13397', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 62756, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(498, '15533', '15533', 'APSARI KURNIAWATI UTAMI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59557', '93079', '25427', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 42053, 0),
(392, '15534', '15534', 'ARI VIANDRI WISMANANDA ', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78289', '18302', '38161', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 41161, 0),
(393, '15535', '15535', 'ARIEH MOUNTARA', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59129', '76391', '97990', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 49482, 0),
(463, '15536', '15536', 'ARIEL PRATAMA EFFENDI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '90911', '53018', '38504', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 20890, 0),
(536, '15537', '15537', 'ARIF RAHMAN', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33173', '73617', '58676', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 64602, 0),
(499, '15538', '15538', 'ARIFA NURIYANI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96970', '51085', '81812', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 63899, 0),
(667, '15539', '15539', 'ARINTYA YOGANTARI MULYOTO', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42947', '96789', '25301', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 36072, 0),
(464, '15540', '15540', 'ARISA RACHMAWATI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52794', '78386', '42755', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 35399, 0),
(537, '15541', '15541', 'ARLY SETYA LESTARI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80203', '72043', '53750', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 10002, 0),
(642, '15542', '15542', 'ARNOLD SABASTIAN JOSHUA SABDO P.', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25252', '61634', '50689', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 62759, 0),
(668, '15543', '15543', 'ARYA SYENA CAHYONO', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32269', '26630', '14895', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 7028, 0),
(465, '15544', '15544', 'ASITA ALMUFIDAH', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96805', '67876', '36150', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 39804, 0),
(466, '15545', '15545', 'ATIKAH KARIMAH', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32567', '67297', '20052', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 480, 0),
(394, '15546', '15546', 'AULIA RIZKHAN', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29386', '72254', '18107', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 29398, 0),
(395, '15547', '15547', 'AYU AMALIA RAFI`AH SOESILO', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94722', '21280', '40099', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 32891, 0),
(467, '15548', '15548', 'AYU RASTIKASARI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22755', '28093', '15022', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 12132, 0),
(643, '15549', '15549', 'AZMI KUSUMASTUTI UTOMO', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14098', '34166', '62936', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 54301, 0),
(669, '15550', '15550', 'BANOWO SETO DIMURTI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '49567', '98076', '40698', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 46984, 0),
(358, '15551', '15551', 'BARETTA ABRILIYA PRASTIKA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '17974', '20674', '16346', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 47777, 0),
(500, '15552', '15552', 'BAYU NADYA RISKY IRIANTI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41072', '22162', '41931', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 44656, 0),
(501, '15553', '15553', 'BELLA EKA SYAHPUTRI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71264', '77120', '43762', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 12385, 0),
(468, '15554', '15554', 'BIZATYA DIO SANTOSO', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '35153', '13274', '49785', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 40189, 0),
(670, '15555', '15555', 'BULAN RAHMA NINDITA', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '13474', '89757', '94139', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 30328, 0),
(359, '15556', '15556', 'CAHYO SUKARSONO ', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24872', '33411', '33265', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 30772, 0),
(469, '15557', '15557', 'CINTYA AYU PERMATASARI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75719', '70130', '65954', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 4231, 0),
(538, '15558', '15558', 'CITRA AWANIS GHAISANI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '65865', '26178', '41385', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 38845, 0),
(396, '15559', '15559', 'CITTA ARASTI RAMADHANI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21266', '88720', '87243', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 2639, 0),
(502, '15560', '15560', 'CLARA PUSPARANI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '53364', '19242', '21207', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 47166, 0),
(644, '15561', '15561', 'CLAUDIA NOVITA SISWARINI', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12593', '33363', '65613', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 28396, 0),
(397, '15562', '15562', 'CLORINDA FARICHA DEWI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '89380', '55509', '46692', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 43859, 0),
(426, '15563', '15563', 'CORNELIA SERTHA LORENZSA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45927', '53086', '95815', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 44816, 0),
(539, '15564', '15564', 'DADANG AQIL WICAHYA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25124', '27137', '33590', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 19715, 0),
(470, '15565', '15565', 'DEANOVA SABILA', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80950', '10521', '29157', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 39723, 0),
(503, '15566', '15566', 'DERIA ELNIZA KUSUMA PUTRI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86250', '57901', '94041', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 31271, 0),
(360, '15567', '15567', 'DESTYA DWI ARIYANTI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81483', '48384', '46586', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 57771, 0),
(471, '15568', '15568', 'DEVI NOFITA SARI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56389', '19908', '44194', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 49151, 0),
(671, '15569', '15569', 'DEVI NOVENTINA', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43352', '47738', '61935', '', NULL, NULL, NULL, '2013-02-04 00:05:37', 43603, 0),
(672, '15570', '15570', 'DHANYTA FITRIANI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80723', '73338', '28503', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 48181, 0),
(504, '15571', '15571', 'DHIYA ACFIRA TSANIATRI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38431', '87361', '48239', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 48658, 0),
(472, '15572', '15572', 'DIAN KARTIKA AMANDANI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78598', '27653', '92731', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 41789, 0),
(673, '15573', '15573', 'DIAN PERMATASARI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38199', '18348', '96809', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 55948, 0),
(398, '15574', '15574', 'DIAN SELLA RAHMASARI ', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91640', '95493', '83000', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 29909, 0),
(587, '15575', '15575', 'DIANI AKMALIA APSARI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83110', '99837', '37283', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 25551, 0),
(428, '15576', '15576', 'DICKY ISWAHYU PRATAMA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14389', '93767', '66831', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 30193, 0),
(429, '15577', '15577', 'DIMAS PRASETYA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74974', '55465', '15468', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 50598, 0),
(473, '15578', '15578', 'DIMAS CAMEEO WICAKSONO', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45094', '44612', '76645', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 50110, 0),
(474, '15579', '15579', 'DINA APRILLIA', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70360', '63031', '53352', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 62691, 0),
(588, '15580', '15580', 'DINDA KARTIKA VILAILI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79860', '49444', '48850', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 4264, 0),
(361, '15581', '15581', 'DODY RIZKYANTO', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32889', '31023', '93512', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 55586, 0),
(362, '15582', '15582', 'DUANTI RAHMA FARDILA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27647', '96570', '55739', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 3748, 0),
(363, '15583', '15583', 'DWI RATNA PARAMITHA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '99195', '25572', '33439', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 44061, 0),
(399, '15584', '15584', 'DWITA YULIANDARI WAHYUNINGTYAS', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43621', '89111', '55126', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 28969, 0),
(430, '15585', '15585', 'DYAH AYU PUSPAGARINI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24986', '70650', '77153', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 21656, 0),
(475, '15586', '15586', 'DYAH AYU PUSPITASARI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42415', '42969', '74170', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 58094, 0),
(645, '15587', '15587', 'EGA WAHYU PRATAMA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63737', '69767', '55847', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 19164, 0),
(646, '15588', '15588', 'EGA WIDYO PRATOMO', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51784', '87925', '58780', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 45543, 0),
(540, '15589', '15589', 'EKA PUTRI PERWITA SUCI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '40789', '85811', '24505', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 4152, 0),
(647, '15590', '15590', 'ESTY DWI KHOIRUN NISA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64148', '41032', '79795', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 46086, 0),
(431, '15591', '15591', 'FAHDYNIA KARNIRA GUNAWAN', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32358', '29599', '31540', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 38655, 0),
(589, '15592', '15592', 'FAHIMA BUDI IMANIAR', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79354', '89315', '17009', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 44388, 0),
(364, '15593', '15593', 'FAHMI FAJRUL HAQ', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '92190', '83860', '72008', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 16097, 0),
(505, '15594', '15594', 'FAHROZI FATHUR ROCHMAN', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '89124', '74355', '13066', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 33911, 0),
(541, '15595', '15595', 'FAIRUZA AROFAH IZDIHARINA KHODIJAH MUROD', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28312', '62526', '30939', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 61975, 0),
(648, '15596', '15596', 'FAISAL AKBAR PANJAITAN', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21992', '92735', '62279', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 32590, 0),
(400, '15597', '15597', 'FAIZ KAMAL HASAN', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56992', '67531', '48699', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 2758, 0),
(542, '15598', '15598', 'FARADILLA FAUZIYAH RISNAWATI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16970', '78868', '62464', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 62134, 0),
(543, '15600', '15600', 'FARHANA JIHAN AZIZAH', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55692', '79466', '13133', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 20918, 0),
(506, '15601', '15601', 'FATIKA MAULIDYAH YUWANTO', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88553', '79564', '69317', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 22985, 0),
(401, '15602', '15602', 'FAZA MUHAMMAD S.', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50739', '73616', '84117', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 28248, 0),
(507, '15603', '15603', 'FELIX YOSA HERDIMA SANJAYA', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '49216', '38835', '26550', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 5709, 0),
(365, '15604', '15604', 'FERNANDA YERISHA HARTINAH RIDWAN', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63663', '20323', '30101', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 32545, 0),
(476, '15605', '15605', 'FILAILY NURA RAMADHAN', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66857', '26530', '87320', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 5977, 0),
(508, '15606', '15606', 'FIRDA NIRMALA PUTRI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60344', '59698', '57314', '', NULL, NULL, NULL, '2013-02-03 23:37:17', 32742, 0),
(545, '15607', '15607', 'FITRATILLAH HILHANIF ', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72228', '25015', '12818', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 14719, 0),
(546, '15608', '15608', 'FITRIA NUR KHABIBA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93051', '14297', '48978', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 46483, 0),
(345, '15609', '15609', 'FITRIANI AYUNINGTYAS', NULL, 1, 2013, 22, 70, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '20724', '21648', '45305', '', NULL, NULL, NULL, '2013-02-03 23:28:21', 50568, 0),
(590, '15610', '15610', 'FUAD HIDAYAT', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21748', '80355', '16719', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 28341, 0),
(432, '15611', '15611', 'GAYATRI PUTRI M.', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83511', '84191', '69464', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 17513, 0),
(509, '15612', '15612', 'GINA GRISELDA', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42136', '78602', '21726', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 40529, 0),
(510, '15613', '15613', 'GRACE ANGELA WARDOYO', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88601', '38590', '22953', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 64752, 0),
(511, '15614', '15614', 'GRACE MARGARETHA RETNO WULAN', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59441', '16808', '74741', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 22992, 0),
(547, '15615', '15615', 'GUSTAFIAN FAWALLY AL-BARR', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '30153', '95530', '57671', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 44681, 0),
(402, '15616', '15616', 'HAFIDZ HASYMI RIZALDI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29377', '78228', '52037', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 22882, 0),
(433, '15617', '15617', 'HAKIM HABIBI HIDAYATULLAH USMAN', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '49206', '48483', '55262', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 29715, 0),
(676, '15618', '15618', 'HANDIRA LAKSMINITA', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '23716', '58463', '26692', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 40675, 0),
(512, '15619', '15619', 'HANIFAH MARGASARI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '48351', '14191', '56141', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 25462, 0),
(434, '15620', '15620', 'HASYA AGHNIA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28178', '48043', '70864', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 3950, 0),
(435, '15621', '15621', 'HELMI PANGGAGAS ADI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95144', '56683', '96705', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 37938, 0),
(436, '15622', '15622', 'HERVINDA AINI FITRI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '46117', '62646', '16784', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 44998, 0),
(651, '15623', '15623', 'HIBBAN RAZAN AFANI', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28502', '48904', '80209', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 60870, 0),
(652, '15624', '15624', 'HUSNA SHAFIYAH', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '68482', '94485', '75145', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 24121, 0),
(403, '15625', '15625', 'I KADEK ADIT PUTRA IGMAS', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79410', '94832', '38304', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 19329, 0),
(653, '15626', '15626', 'IDA AYU GERHANA SARASWATI ', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29044', '68508', '15743', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 24004, 0),
(437, '15627', '15627', 'IDA AYU SARITHAYANTI MAHAGUNA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27117', '55787', '89426', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 19818, 0),
(438, '15628', '15628', 'IDA BAGUS AGUNG SURYA WIKRAMA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33353', '80248', '60922', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 42826, 0),
(477, '15629', '15629', 'IDHSA ILHAMI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94374', '17628', '82609', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 23581, 0),
(439, '15630', '15630', 'IKA SUKMAWATI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79604', '54246', '34782', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 7626, 0),
(513, '15631', '15631', 'ILHAM AKBAR WICAKSONO', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '17805', '85664', '57578', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 40020, 0),
(548, '15632', '15632', 'ILHAM SETYA WICAKSONO', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62030', '89113', '49369', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 25860, 0),
(549, '15633', '15633', 'INASDIAH FARRAS FAUZIYYAH', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14461', '23695', '73811', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 10477, 0),
(678, '15634', '15634', 'INDRASWARA ARDI YUWONO', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64092', '38853', '99205', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 13262, 0),
(478, '15635', '15635', 'INTAN MAULANI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16621', '50235', '71674', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 21291, 0),
(479, '15636', '15636', 'IRZAL FARESA', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28954', '90460', '31411', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 18107, 0),
(440, '15637', '15637', 'ISMI NINGTYAS', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41336', '39992', '91069', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 43560, 0),
(366, '15638', '15638', 'ITA AYU MAYASARI MUHLISHATIN', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85498', '99379', '90271', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 6506, 0),
(550, '15639', '15639', 'IZHAR MIRZA HARDIAN', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74176', '35537', '67660', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 8825, 0),
(514, '15640', '15640', 'JEFF L GAOL', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '92751', '37246', '25103', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 23947, 0),
(441, '15641', '15641', 'JESSICA PRAHASUTI LUIGI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '90379', '11167', '23088', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 2358, 0),
(480, '15642', '15642', 'JIHAN ROFIDU ROKHIM', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76207', '23870', '41660', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 21819, 0),
(654, '15643', '15643', 'KALILA DESI JUJANE', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94808', '99742', '51954', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 23964, 0),
(481, '15644', '15644', 'KUSUMA ISLAMI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29729', '19961', '52496', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 11520, 0),
(591, '15645', '15645', 'LALA AYU KANTARI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21535', '43458', '62364', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 20288, 0),
(404, '15646', '15646', 'LEVRITA NINDYA POETRI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '13854', '29451', '94129', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 11038, 0),
(592, '15647', '15647', 'LINDA PERMATA', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52394', '27614', '92834', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 5950, 0),
(405, '15648', '15648', 'LIYA SANJAYA', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14289', '14232', '29533', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 33453, 0),
(515, '15649', '15649', 'LUCHA KAMALA PUTRI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51354', '20961', '89623', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 48874, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(346, '15650', '15650', 'LUSIANA DWI ANDINI', NULL, 1, 2013, 22, 70, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12697', '58044', '58817', '', NULL, NULL, NULL, '2013-02-03 23:28:21', 35138, 0),
(406, '15651', '15651', 'LUTFIANA HANIFAH', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88252', '50105', '69605', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 19082, 0),
(655, '15652', '15652', 'LUTHFI MAHDYA SUSANTI', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '65352', '25747', '39638', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 23883, 0),
(443, '15653', '15653', 'M. DWIKI AMIRULLAH', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '20903', '41288', '49324', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 30357, 0),
(656, '15654', '15654', 'M. NIZAR RAMADHAN', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32056', '14054', '81714', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 18508, 0),
(367, '15655', '15655', 'MAFIRA PUTRI RAMADHANI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '54742', '80772', '65160', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 34925, 0),
(368, '15656', '15656', 'MAHARDHIKA GALIH PRATAMA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27122', '69149', '51457', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 62628, 0),
(483, '15657', '15657', 'MALINDA SYIFA YUSHARANI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98559', '28206', '50222', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 10967, 0),
(407, '15658', '15658', 'MARGIANA BELINDA AMALIYA ', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71204', '84836', '47433', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 33544, 0),
(593, '15659', '15659', 'MARISA PRAMASHEILLA PUTRI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16431', '39515', '82641', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 59264, 0),
(657, '15660', '15660', 'MAYZA NASTITI RIANDINI', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '99684', '25708', '53341', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 49438, 0),
(679, '15661', '15661', 'MEGA NADILA SAWITRI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '17448', '94786', '38454', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 40297, 0),
(516, '15662', '15662', 'MEIDIANA PRASTIWI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '35559', '70431', '28724', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 52365, 0),
(680, '15663', '15663', 'MERLIN NURLITA PANDIANGAN', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '23069', '34961', '72747', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 47856, 0),
(594, '15664', '15664', 'MICHAEL IMMANUEL', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '10644', '12740', '86544', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 58895, 0),
(408, '15665', '15665', 'MIFTAHUL FIRDAUS', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '48026', '96093', '19769', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 5821, 0),
(444, '15666', '15666', 'MIFTAHUL FIRDAUS ISLAMI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80492', '96720', '14603', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 61450, 0),
(370, '15667', '15667', 'MOCH TAUFAN WARDANA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22832', '27190', '38766', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 34074, 0),
(409, '15668', '15668', 'MOCH. ISCHAK', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33004', '42370', '82712', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 18666, 0),
(410, '15669', '15669', 'MOCHAMMAD IQBAL RAVANELLI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '10518', '44668', '29190', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 25527, 0),
(484, '15670', '15670', 'MOH. CHALIFFILARDHY SYAIFUDDIN', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43417', '87472', '32684', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 25686, 0),
(518, '15671', '15671', 'MONICA DIAN KARTIKA DEWI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22494', '91855', '83326', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 42196, 0),
(595, '15672', '15672', 'MUCHAMMAD HAIDAR TEGAR R.', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83509', '42926', '77515', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 31488, 0),
(371, '15673', '15673', 'MUH. WILDAN MARETRA PUTRA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59163', '22148', '73085', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 52475, 0),
(372, '15674', '15674', 'MUHAMMAD AKBAR ELNANDA DZULFIKAR', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56986', '55282', '12162', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 1433, 0),
(551, '15675', '15675', 'MUHAMMAD AL FATIH ABIL FIDA`', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '53961', '90519', '35470', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 29932, 0),
(485, '15676', '15676', 'MUHAMMAD AMIR FAIZ', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43405', '52393', '65061', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 54933, 0),
(552, '15677', '15677', 'MUHAMMAD FAJAR ARIFIANTO', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63368', '45404', '97558', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 59279, 0),
(369, '15678', '15678', 'MUHAMMAD FARIS MAS`UD', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56349', '96813', '60931', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 4361, 0),
(596, '15679', '15679', 'MUHAMMAD NABIL FAROJ', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '68675', '62863', '12806', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 11469, 0),
(486, '15680', '15680', 'MULKI AULY POETRY', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '61669', '92736', '70303', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 31440, 0),
(445, '15681', '15681', 'MULLAH KALILIANDA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50552', '87200', '67740', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 33448, 0),
(411, '15682', '15682', 'MUSTHAFANI AKHYAR ABDILLAH', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83034', '12137', '18860', '', NULL, NULL, NULL, '2013-02-03 23:31:39', 30911, 0),
(681, '15683', '15683', 'MUSTIKA AYUNINGRUM', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55863', '60968', '68103', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 53744, 0),
(373, '15684', '15684', 'MUTIARA DAMAYANTI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62271', '16936', '55867', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 8409, 0),
(374, '15685', '15685', 'NABILA MU`TAZ DZAKIYYA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64897', '46762', '56953', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 44222, 0),
(597, '15686', '15686', 'NABILLA HEFIN RACHMAWATI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45052', '96582', '54866', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 1757, 0),
(412, '15687', '15687', 'NABILLA QHUSNA', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59291', '78645', '98637', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 14361, 0),
(682, '15688', '15688', 'NADYA FERINA N.', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66666', '98632', '77616', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 58543, 0),
(553, '15690', '15690', 'NADYARANI NIKMATULLAH ASRI IBRAHIM', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50239', '87730', '30950', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 2358, 0),
(683, '15691', '15691', 'NANDA RATNA ANJARSARI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38288', '91987', '53598', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 21329, 0),
(446, '15692', '15692', 'NAUFAL MUTTAQIN', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51708', '19243', '47084', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 33717, 0),
(487, '15693', '15693', 'NAUFAL NUGRAHANDITHA', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75613', '18861', '53600', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 49048, 0),
(519, '15694', '15694', 'NI LUH PUTU SATVIKA SAESARI DWI JATI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25380', '20920', '27421', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 11566, 0),
(447, '15695', '15695', 'NICO WAHYUDI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70125', '38857', '92817', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 24925, 0),
(375, '15696', '15696', 'NOURMA VIDYA PRIMANTIKA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27013', '43713', '13154', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 34205, 0),
(376, '15697', '15697', 'NOVINDA CAHYA DIYANTI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41388', '54309', '10351', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 35222, 0),
(347, '15698', '15698', 'NOVINDA PUTRI NAZLIA WIBOWO', NULL, 1, 2013, 22, 70, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '26011', '11902', '50090', '', NULL, NULL, NULL, '2013-02-03 23:28:21', 3987, 0),
(598, '15699', '15699', 'NUGROHO WICAKSONO', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '99899', '69888', '94603', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 27478, 0),
(520, '15700', '15700', 'NUKE KRISTRIYANTO PUTERI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86447', '21228', '40638', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 48050, 0),
(377, '15701', '15701', 'NUR INAS SAFITRI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86243', '39373', '34030', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 59335, 0),
(448, '15702', '15702', 'NUR LATIFAH KHOMSIATI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98682', '97868', '24863', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 35166, 0),
(378, '15703', '15703', 'NUR QUM IRFAN', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42993', '36741', '67611', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 60537, 0),
(489, '15705', '15705', 'NUR SENA MAGISTA A.', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '46547', '49767', '69672', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 43227, 0),
(379, '15706', '15706', 'NURUL HIKMAH', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81424', '15766', '61872', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 29925, 0),
(554, '15707', '15707', 'OKTAVIA ANISA RAWINDRA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58858', '77479', '94150', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 29737, 0),
(521, '15708', '15708', 'OKTAVIANUS SATRIA PUTRA', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '99479', '97647', '29311', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 58646, 0),
(449, '15709', '15709', 'OKTAVINA DWIE FITRANDINI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '87726', '99004', '49350', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 43280, 0),
(413, '15710', '15710', 'PARADIKA FARANDI ANGESTI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33989', '75345', '43051', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 52139, 0),
(380, '15711', '15711', 'PRAKARSA BRAMADITYA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52054', '71297', '99755', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 38626, 0),
(522, '15712', '15712', 'PRASETYAWAN', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81088', '78988', '80552', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 52224, 0),
(684, '15713', '15713', 'PRAYOGI KESDIK ADIRAHARJO', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81045', '18768', '10971', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 60136, 0),
(490, '15714', '15714', 'PRITHO AJENG MAHARANI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72519', '59643', '37191', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 55146, 0),
(414, '15715', '15715', 'PUTRI MUMPUNI ASWOKO ', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88060', '41854', '93886', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 48152, 0),
(555, '15716', '15716', 'PUTRI NUR CAHYANTI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55542', '65485', '66208', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 21685, 0),
(523, '15718', '15718', 'RACHMADWIPA NOVANDRI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '73497', '50772', '70163', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 38791, 0),
(415, '15719', '15719', 'RAHADIAN TRI A.S.', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58939', '85525', '36057', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 19760, 0),
(416, '15720', '15720', 'RAHMA DEWI HUTAMI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52561', '87350', '91260', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 18524, 0),
(417, '15721', '15721', 'RAHMAT FERMANSYAH P.', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63466', '55367', '49005', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 30982, 0),
(556, '15722', '15722', 'RAHMAT PRASETYADI ', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64945', '93635', '11162', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 14444, 0),
(491, '15723', '15723', 'RAIHAN NABIL ZAKIY', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71320', '68886', '30742', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 33553, 0),
(557, '15724', '15724', 'RAMA ARIGO NOVIAN', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38566', '32071', '22840', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 45787, 0),
(685, '15725', '15725', 'RAMADHONI PUTRA ELANDI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25018', '46863', '32819', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 41016, 0),
(450, '15726', '15726', 'RANI LAKSMI DEVINDASARI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70617', '13907', '39944', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 22794, 0),
(418, '15727', '15727', 'RATNA DEWI PRATIWI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85519', '85786', '82693', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 12449, 0),
(451, '15728', '15728', 'RATNA NUR OKTAVIA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44682', '17869', '18490', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 65168, 0),
(686, '15729', '15729', 'RATNA NUR SAFITRI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '37203', '97897', '16596', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 19358, 0),
(658, '15730', '15730', 'REHULINA GINTING', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '37234', '95701', '77189', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 47735, 0),
(452, '15731', '15731', 'REIZA NOVITA PUTRI', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64192', '37798', '46876', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 10890, 0),
(599, '15732', '15732', 'RENDY SURYA PUTRA', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16004', '80473', '56810', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 53789, 0),
(419, '15733', '15733', 'RETNO KARTIKA HAYATI', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '11266', '12713', '77521', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 28351, 0),
(524, '15734', '15734', 'RETNO WULANSARI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56924', '80747', '93899', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 54933, 0),
(525, '15735', '15735', 'RHEINADIA INDRASWARI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21738', '56463', '11588', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 5205, 0),
(492, '15736', '15736', 'RIEZKY FAUZAN RACHMAN', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44984', '50631', '53070', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 15744, 0),
(558, '15737', '15737', 'RIFDA ZULFA LUTHFIA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84610', '20635', '90280', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 61936, 0),
(526, '15738', '15738', 'RIFKI DWI NURAINI ASTUTI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '11446', '18989', '48737', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 47225, 0),
(559, '15739', '15739', 'RIKA NURLAILI DEWI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66833', '66280', '73411', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 48953, 0),
(381, '15740', '15740', 'RISKHA INDAH RESPATI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94807', '86363', '98381', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 54035, 0),
(382, '15741', '15741', 'RIZKY ALAMANDA FIRDAUS', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94672', '43026', '71712', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 3349, 0),
(493, '15742', '15742', 'RIZKY FENALDO MAULANA', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '10855', '33068', '46564', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 25936, 0),
(560, '15743', '15743', 'RONA ROFIDA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81568', '12508', '65172', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 15913, 0),
(454, '15744', '15744', 'ROSA DWI KIRANA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43657', '35213', '31357', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 808, 0),
(455, '15745', '15745', 'ROSIDA MAULIDINI ', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67459', '46624', '59065', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 294, 0),
(456, '15746', '15746', 'SALSABIILA KARIMAH', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84822', '82799', '93046', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 1688, 0),
(348, '15747', '15747', 'SALVIA ARROSYIDA', NULL, 1, 2013, 22, 70, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '73212', '32491', '77845', '', NULL, NULL, NULL, '2013-02-03 23:28:21', 25481, 0),
(659, '15748', '15748', 'SANTIKA SANAA DHIFA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67980', '88770', '30886', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 59363, 0),
(420, '15749', '15749', 'SATRIA LUTHFI HERMAWAN', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74347', '96371', '45857', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 31547, 0),
(527, '15750', '15750', 'SEKTIAN HADI BINORTA', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67689', '16576', '18171', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 33302, 0),
(561, '15751', '15751', 'SETINDA EKA ANDRIYANI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88652', '81514', '53929', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 16783, 0),
(457, '15752', '15752', 'SHABRINA AMALIA SHANY', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93181', '37895', '45003', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 61584, 0),
(421, '15753', '15753', 'SHEISA FITRIA SAVERINA', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '69323', '78892', '60164', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 54463, 0),
(528, '15754', '15754', 'STEFANUS DION FINNADI', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93001', '76240', '30173', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 12960, 0),
(689, '15755', '15755', 'SUKMA PARAMASTUTI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '20341', '79258', '18884', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 64600, 0),
(562, '15756', '15756', 'SYAHRI MAULANA RAMADHAN', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45471', '70755', '25314', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 61049, 0),
(349, '15757', '15757', 'TAZKI ADI PRASETYA', NULL, 1, 2013, 22, 70, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79941', '65394', '36259', '', NULL, NULL, NULL, '2013-02-03 23:28:21', 30487, 0),
(563, '15758', '15758', 'TETA LALVINA', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63334', '18859', '79610', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 40268, 0),
(494, '15759', '15759', 'THASIA ISABELITA', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15692', '68297', '98322', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 11473, 0),
(600, '15760', '15760', 'TIFANY CICILIA', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '40209', '52832', '52289', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 22587, 0),
(458, '15761', '15761', 'TIFFANY DEWI SETYANINGRUM', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '87696', '71210', '11135', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 31894, 0),
(529, '15762', '15762', 'TITUS RENDY P.', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51432', '46946', '24952', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 6536, 0),
(383, '15763', '15763', 'TRI PAMUNGKAS KARTIKA ADI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98922', '16677', '65039', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 54020, 0),
(690, '15764', '15764', 'TRIAS MARIYAH ULFA', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62729', '73275', '78550', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 25674, 0),
(422, '15765', '15765', 'ULFA GITA RUSMALA', NULL, 1, 2013, 21, 62, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '19114', '30819', '27314', '', NULL, NULL, NULL, '2013-02-03 23:31:40', 32136, 0),
(660, '15766', '15766', 'ULFA HIKMATUL FAUZIAH', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29937', '84045', '66768', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 52584, 0),
(564, '15767', '15767', 'VANIARTA SHINTYARANI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15258', '52811', '26565', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 396, 0),
(384, '15768', '15768', 'VANNY SEPTIANA LARASANTI', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27938', '33226', '92442', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 37232, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(350, '15769', '15769', 'VIKRI ROZAK ISKANDAR', NULL, 1, 2013, 22, 70, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16265', '71327', '62762', '', NULL, NULL, NULL, '2013-02-03 23:28:21', 47141, 0),
(565, '15770', '15770', 'VIRNANDA PUTRI PRADHINI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28027', '71294', '28050', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 58414, 0),
(692, '15771', '15771', 'VIVI PERMATA SARI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34061', '50219', '23001', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 30711, 0),
(601, '15772', '15772', 'WACHIDATUL AYU FITRI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '26979', '34321', '14680', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 26978, 0),
(602, '15773', '15773', 'WAHYU SINTA PUTRI HAMDANI', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '48714', '32573', '21407', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 41172, 0),
(385, '15774', '15774', 'WANDA ATHIRA LUQYANA', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44403', '89844', '44009', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 12810, 0),
(661, '15775', '15775', 'WIDYA CHANDRA ANDHESTI YUDHA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45124', '57243', '13735', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 48194, 0),
(530, '15776', '15776', 'WINONA RETA KINASIH', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91413', '32970', '93270', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 41907, 0),
(603, '15777', '15777', 'YANNA DEBBY RESTIFANNY Y.', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55439', '15428', '63519', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 9681, 0),
(662, '15778', '15778', 'YANUAR AKBAR R.', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34480', '84090', '84457', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 11100, 0),
(566, '15779', '15779', 'YULIA DWI PRATIWI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63520', '16487', '33628', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 9753, 0),
(663, '15780', '15780', 'ZUHRATULLAILI DZIKRI', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85403', '95254', '75281', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 45491, 0),
(664, '15781', '15781', 'ZULFA IZDIHAR', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80700', '32740', '29416', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 63025, 0),
(604, '15782', '15782', 'ZULFIATUL KHIFTIANNISA`', NULL, 1, 2013, 21, 67, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72966', '84241', '51284', '', NULL, NULL, NULL, '2013-02-04 00:01:57', 55951, 0),
(386, '15783', '15783', 'ZULFIKAR RIZKY AZHAR', NULL, 1, 2013, 21, 61, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '18207', '10109', '35435', '', NULL, NULL, NULL, '2013-02-03 23:30:22', 19500, 0),
(482, '15784', '15784', 'LUCIA PUTRI RACHMADANI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '19347', '96404', '68477', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 4845, 0),
(442, '15785', '15785', 'KEVIN BONANZA INDRAWAN', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98571', '52902', '63047', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 60972, 0),
(544, '15786', '15786', 'FIKA DESSY ARIANDI', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24717', '15834', '88804', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 61306, 0),
(691, '15787', '15787', 'ULFATHAN DANA RAFI', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '90047', '25129', '83386', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 47548, 0),
(650, '15788', '15788', 'HAYU MAULIDYA', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56853', '30430', '38455', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 24132, 0),
(460, '15789', '15789', 'AHMAD RIZKI UBAIDILLAH', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62990', '89768', '33977', '', NULL, NULL, NULL, '2013-02-03 23:36:20', 56351, 0),
(687, '15790', '15790', 'SATYA GUSTI BRAHMATYO', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83873', '35850', '49613', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 48020, 0),
(488, '15791', '15791', 'NOVITA RAHMANIA PUTRI', NULL, 1, 2013, 21, 64, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78632', '86575', '92892', '', NULL, NULL, NULL, '2013-02-03 23:36:21', 60508, 0),
(423, '15792', '15792', 'ABDUL HAFID', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '26153', '83739', '75726', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 37970, 0),
(427, '15793', '15793', 'DHIA NAQQIYA SALSABILA', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15380', '15784', '22574', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 22056, 0),
(688, '15794', '15794', 'SHINTYA FUADIAH D.', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24005', '76874', '66617', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 42407, 0),
(677, '15795', '15795', 'I GEDE SAKA BRAMANDALA', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '36599', '33140', '83797', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 49936, 0),
(453, '15796', '15796', 'RIZKI BAHRUDIN', NULL, 1, 2013, 21, 63, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57776', '96571', '13780', '', NULL, NULL, NULL, '2013-02-03 23:34:37', 29079, 0),
(517, '15797', '15797', 'MOCH. CHAFID RIZKI ALMA`ARIJ', NULL, 1, 2013, 21, 65, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22361', '18416', '71519', '', NULL, NULL, NULL, '2013-02-03 23:37:18', 51785, 0),
(649, '15799', '15799', 'FIQIH ARDIANSYAH', NULL, 1, 2013, 21, 68, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41206', '94865', '15564', '', NULL, NULL, NULL, '2013-02-04 00:04:07', 52257, 0),
(532, '15801', '15801', 'ADITYA HIMAWAN HOGANTARA R.L.', NULL, 1, 2013, 21, 66, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50661', '84798', '77551', '', NULL, NULL, NULL, '2013-02-03 23:41:21', 8726, 0),
(674, '15802', '15802', 'FIRLY ADHYATMA             ', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96082', '73969', '88096', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 29467, 0),
(219, '15804', '15804', 'ADANI ARDHANARESWARI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60862', '29857', '55910', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 62792, 0),
(75, '15805', '15805', 'ADE WILDAN RIZKY FACHRY', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '18311', '95892', '67187', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 61511, 0),
(291, '15806', '15806', 'ADELIA BAYU FEBRIAN', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81583', '56179', '56747', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 14058, 0),
(147, '15807', '15807', 'ADIN NUGROHO', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '99617', '61772', '48294', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 12901, 0),
(148, '15808', '15808', 'ADRIAN PUTRA VIERA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62916', '24269', '76615', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 62628, 0),
(327, '15809', '15809', 'AFIFAH SALSABILA', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83736', '63349', '68362', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 55061, 0),
(183, '15811', '15811', 'AGHNIA NURI FADHILLA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '30342', '63026', '47520', '', NULL, NULL, NULL, '2013-02-03 23:18:02', 64871, 0),
(220, '15812', '15812', 'AGRESSYA PUTRI DEWA ASMARA', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '13234', '63901', '94213', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 41207, 0),
(255, '15813', '15813', 'AGYA ARDA WIRADHIKA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12334', '94355', '75571', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 49124, 0),
(221, '15814', '15814', 'AHMAD ILHAM R.', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '31810', '17015', '45619', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 28460, 0),
(111, '15815', '15815', 'AHMAD NORCA KURNIA ZEN', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1988-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66117', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57151', '35270', '43260', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 64582, 0),
(76, '15816', '15816', 'AINUN NADIAH FITRI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '49343', '15859', '98723', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 11859, 0),
(222, '15817', '15817', 'AINUR ROSYIDAH', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '39157', '72190', '57870', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 50132, 0),
(292, '15818', '15818', 'AISYA NADYA PUTRI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78547', '14461', '86483', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 5792, 0),
(40, '15819', '15819', 'AISYAH SUGIASTU PUTRI', '', 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', 'WNI', 1, 2, '', '0.0', '0.0', 'O', 0xffd8ffe000104a46494600010100000100010000fffe003c43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d203130300affdb00430001010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffdb00430101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffc00011080078006503012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f2d7725b0a0704f3cf3c91ebd07b639a401c3120fe192063e83a7ebdbbd30b10c4fbe3db8ff3fa9a72bf62339e327a73dce3a63e87e95ff4c96692b25b24d5b5ff0086fcbe5a7f95ab6d756ecef64f5d35d3a2bfaef61cc3e5f989ebc9192704f5e4ff005ea78c9a6aaa7f78824139c71d703d71fd0f4eb53a807900633d871c83fa761d7a0c7bdbb683cc6c150718cf031cfb63d318e0e6a1cd462dbd3add74db4b5bab4b4ebb1314ad6d1f54f75aa57fbbb3b5fe4678881271bd8f620b75ce79e87d7fcf3560432118da71db713f98fcff00fd75d65a69026236c6474e80018e9ce3fce3b64d74d6fe1891c02d1803820e31c7ae4a91c9efd876c66bcdc466987a2ed3959f9b4ba2f96bdfcbb591564f7d7cf6bedf7decbf23cacdab9fe139faff00f5baff009ebcd1f66914608241e39381f873d3d7b9c1e2bd766f0ca451bcb2aa2468b9691dd1514100e4b300a06083c9e845615d68c833b0023191865208238208ea0839078cf51918359d1ceb0f59f2a975d5a77d6c9dafbab6fe9afa8b95bba4b4eb6b6af6e9fd5d5b73ce590803ae71c649fd3d0f19f4e7e9889bcc519e001d39c7e406071ce7f1adebeb23093f29233d81fc78fc7b8e9db06b9e9542b721f239c004f071818ec7dff975af5e95455229a6a49a4d3d1f6df5dd7f4c6ec96d74eda5be5b69fe642c4f2738279c9fd0e73fafb77a6063d77fa81c7b67dbbfe7c7738a716e0e15f6f3d4647f80ff0026a3191c60f231d09e9938e47a71e9debaa2b47a26eeb4d2fb2ebe9fe44c7d34f3defa6baf65b5bb3f245952c460638f5cf7fa77e393de8a8849b476c9eb81e9c7a83f9d159b84aff0bdfb47fbbd6fe5bfff00222bcd6892b7a2f2bfcaff008377ea0b29639c70738cf3e8dd3afa773ed538739fc403ce41ebdb233cf5c678ce6b2a39b9209c7391d3dfd3be4f4e9cfb55d8dc1c671db3c673d7b11edd476e47a56d3a5cbb76bdbbfa7f5f992deaedb76dfefdd3fc6e6945fc3f5eff008fe5dbbe075ed5d568f68d2c990990d80303e9eff5f53c77edcada9cbaf4033c0c7e59c76ce3f96715e8fa44f6ba6d8dc6a576c120b4b692e646c67e58a3693681c12cdb4041dd881c0e6bc3cceb3a3466a2af2968977bb4edf7fde553bb6d24db6d72a5d5ed64b4d5b695bd0f953f6b7fdafb4bfd96b4ed0f4bd174ef0f7883c79ad2497f2e91ae5edfdbdae8fa0aa5c4306af729a7c265b9fb4ea51259dbd90bab49ae152ee4859becf205fe7cbe347ed9bf1e7e38eaba84de27f8a1e234d2d4dcbc1e17f04dc5f7863c2b6b6d31622c974cb2b9857508e05730c575ad3ea9a8ba2a89aeee01da9f706aff0008f5efdb17e2378dbc7171a93dc5c6ad702e6ce174f32d74fd36ff0065a6816f6f1f976ac2cf46d16eeda4b7926812f2e2e2dd2eef41bb964c56f865ff0004eed6ee27d5ae3ec1e7d95a6b5aa78735126dde496d4b49736b14fb098e19a1927b1bf82268e46915f4aba65521a2cff877f480fa51714f15f17710e4b906758dcb784b018c783c260b035de1a58fa786ab2a52c763ab61e51af5d632a41d6a5879d6961a8d2f6508c255154ab53fd28f09fc0ac9f87f20ca333cc72ac2637887158755f178fc5d355d61eb548c2a3c360e35d3a74561554547dad3846b547cd5272e471843f2a340f14fc40d42ea54d3b5cf18480987cec789b5572d6f0cb1489f2a5c43f2c535b5bb28492268e4b580a3a4d1c4c3f4cbf65ffdb7be2c7c35d7e0f0c7c48b8d63c63e168af16c2e27d5f5cd4b51d474589a50f2ea22eae61bdb8d5ec608e5499609e51756f6b20d970f6cb1a450e81fb2bcff000d3e2c695a2ead04735a6a1a98823c47b172675b696d25087cdb696706dae6151b0c33813061024acdebbf1c7f65fb8f84fad417f35b7db7c31e20b5b730ea566889359c6e43695a85bb05015e0779b4cd42231086207ca45789a2317e79c07e3371cf00f11e5f9de55c478f5394e84aa60b158aab88caf35c0ca51954c1e330959ce8548548b972cd4635a8ca5ed70f569578c6a47ecf887c3ce1de29cab199766393e1274a31a90956a54614b1982ad6e58d7a35a9c554a5384a293926e9c947d9d58ce9ca507fb453cd61ace9965ace99325dd86a56b15dda5cc44324d0cc81d1d587182a73f88ae1ee9595b0723a8e09ee4fe3fe7debce7f64ed67fb5fe0bd8f85af155751f005cdc7874aee6db2e98890dee9535bab9dfe441677b158a47991605b68e313329466f59d4a058e4231ce39e09f5ea3dfbf6c63d2bfddbf0ef8af09c61c3392f116054a386ceb2dc26634a94e5cd2a1f59a319d4c34e7cb0e69e1aa39d09cd46319ce9b94572b47f989c4b90e278733ecd722c4be6ab96e36be179eca3ed69c2a3546ba57768d7a4e1562936e2a766d34edcbb1933c4800f43df3fe78f5ef55e4629c6ee9c8f6eb9c7af4c9e87eb5624da188da739f5c63a761fd4723d6a8b94ce01239cf7e98e99ebdcfd33c0c57e994a29adba2d2dd7f3bff005af4f05e8b55a5b6d6c926befb6fb6abb6b75494b6416e463a2fae7d71d7d07e3c934557180cc0ae4f19e78e323b7d307e9d3d4ad9d34ddd26d69dedb2f25fd7e10dabdeefa3dbc93fe65dbfab22259707839ec0f3d3a67dc71cf0793f4abd0cdd33cfa11fd7d3df8f61d8562073b88ce39c0c7e47f90e3f0e6adc321079e476eb9f43fcc7ff005f18adaad1bc5e8aff0087456f47b6dd112edd3cbd7cfee7e8761a7fcf20e87a0f4c8ff38fcb9aeafc4b606fbe1ff8c6c63689669fc31ac240d36e3179a6c6609e68492273196015cacd11099db2270ebcae900b3a91d49031827a91cfd33fd6bde7e1e695e13d73c61e13f0e78e6f6decfc37e21d774ad0b518ee2ee3b21a941abdf43a7cba55bcaf3db96bfbe82e65834bb485cdcea1aab58e9f6d0dccd7296b37e49e2871361783383b8a78b71b86c6e3703c2fc3d9bf1062f0797c23531f8ac364d81ad8fc450c242728539622ad3c3ca1454ea420a6d39ce114e4be9f82f21c4f14715f0df0e60f1386c1e2b3dcfb2aca70d8ac64dd3c261ebe3f1b430d4aae26718ce6a8c27514a7c909c9c6ea31949a47e56ffc13a7c3579e18f0ddd7c4ad56cee87c39d5748bbf072f88becf3df4367e2cd1628a1d3aee68904f713182eadb4d5934e8a292e6e20b949a1596082f9a2fbef45f8cde0cf87b378af53b5b091fc3be219eddb58d0750b1bf82f346d5add16096ff004c7bfb54b7bdd3af9a082e01d1ee2f6fec7528a6736eba35f4f258fe9a7ec35fb26eadfb397c2ff8a7f06fccf0e5e788b4bf8bbe2bf147866fb488b509f49f0c5b78a2d6c355d0ed2017c20b898e83697715ab925964780e66937b81f587c33f007c65f07fc5bf891aa78bbe224df107c0fe2593c0779e0cf0deb53c1a845e00b1d1fc3fa85a78ea0be9ee3c2964badea3e29f104fa6eaba7dc5a4da559e949a7cb6d169ad6f7d34507fcb3e378b72dc467fc5398d374d286253c161b1388af41e370b3bd6a2b0b1c3e0f190fac4297239ba95e8d16dc62a7ab93ff7370fc319953c9b87b2d8c26e3ecaa54c557a5428d4582aca508d578a9d6c5e1a6f0f294a6a31a342b576949f237ca8fe4a7f695f12f87fe20ea72788be135d5c6a9aed85c2dc0b1b0b2d6897970152d6661a79f2af632c92699a988ca3432dc69da9620f3ae8dab2f8fba778bbe196a7e17f8a1a3dd45e27d274c67d56c2f6d24b39adb5481d648ef534fbb61756ef776a236beb7b112c7184bbba85dad6eccd0ff6cde3cf1b687e27b197c2fe1db4d0344d51ade5104f1da59486dde4b548722d90c0d22ab2bb84f323092796edb96311b7e765affc133fe06ebbaaf893c63e2df0dda7c41d7355f8a9e2bf8916c7c4961797373e1ad235f8a0874ef871a66af3f88750bbbbf047868452cba259ea0ad736d34c668ee1257bb6bbf5701e21655530b5962bda4a182a71ab97469fb6a988ad88557f79469feea9528469a519bf6d557329be48ae5933c9c6701e751af46a61a34a8bc5ce54b1ee6e953a3428ba5174ead4b55ab56a3ad79538ba34e4a1ca9d49a728dff9acfd953e2ef873c4b2f8e5fc39677ba7dce8c349bfd4f4d92396409a7c69776b76eccb12c2624b25b401d96393cc486562d21007dc1abb891c3824865c82070d9e720807231ce738008e7a578afc7cfd9bb4efd9abf6b5f8ab67f0f2dbfb0bc2de3db05d7747f0bc76d2cc9676179069da5dfdadadf492f9e2ce0d721be8adad192e619a5bdd36379adffb36577f55bfbadb1c680315112052cad19caaedcb2b0c827018838619e47a7fbedf41ae2accb8cfc0be1fc7e332d582c3e0332cd72cca313d734cae9ce8635632a42f28d3a9431f8ec7e573a70938f365dcf7fde33fcb9fa4df0f613873c49c661e962a388c4e270943118fa6b5584c545cb0bec14deb2a7570f86c3e369b924ed8b6ada5de0dc30576e9d483c123f9e4e723f0ac69a704f5e73d7be477fc33fa73cf14fbcb862cdc8fbc4038e475cf53c0ebfaf6c018af31e7e63f4faff2e307ebd3b57f74e1683e55295b549af4ff0083f2ecd9fcdef56dff005aff005b74dba1a42e0739639f6ce07b76fafe345504906393f9807b9f627d3a9a2ba1d349b566fd13b74f2fbfe7ae9a3e46f5b2d75fcbfaf97a133705bbe7f4effe7dbd2a68492caa4743f87e20f7e3fce3985b19249f4cfb0fcfafaf4cfd314e86455719ce3af43f9f23dffcf61aba7a5f47fd7de7334d7cf66baff573d1fc3b87741c751d7079f7e7ae0e3d6bf16ffe0a8ff1d7c5967f113c3be08f0eeb3a968767f0faf744d62dd34fb992d2797c60f6b1ebd67ad3bc1202afa3db4da7a69326730dd2ea5246cac38fd9ff000ddd46268f9ce4afa0e38c7f207ebd72326bf17ffe0a27f0e2d75efda15a46b94b66d5f44f0ff886d8dc32476f2ae9960ba26a30e590869258a68248965912206de7695e38449343fc2df4e2cd737cabc19cda1963a94e96699be5394e69387327fd9b899d79d5a72926b969d5af430d4aa5df2cd4fd9c94a337197f407d1b30580c67891879e32119d4c0e538fc76014f95a86369cf0b49554a49de74e856aee1cbef45da71b4a375fda27fc128ff006cff00871fb62fc34f0e78e2cfc49a36a3f13e7f03784afbe2bf84ed34ed56c26f0debc2e35af0e5ceff00ed2b682cef2d2ff54f0f6a8fa7cba63dd88b4c8b4f7d4a696f6e7ed375fa09f1734cbd8ef40f0a5c4108ba971aa456d2dac17f2d8324827874c92f48b38ef77b4644972563450dc862b5fe76dff04dff00db6350fd83ff00698f857a9786f48d67c57e14f1c5e6a1f0f7e26f877c3adfdabadebba678f752f0cdbe992787b46b6b38a79bc51e08f14f87ecf50d1ad5a4bbbbf11e93ab6a9a35bae95777b2a47fdcaea6f73f187c41a6f8c7c3be3dd7b47822f0cc96ba6dbe9b71149a75d4faa5cd9de4f7b7d65342cf24ad6f6b043673a490bdba4d7414ee9594ff00ce4715644f24c6ca0f0c965f8ec47fb362e509ba787716a0f0cea538ce49aa3ec9a56bfb3945ebcb26bfd8fe12cfff00b6a9aad5315159861687b2c561eef92b464e128e2dd1f79b8b92aae5c917fbd8ca314938a3a4d7f4696f22d0d2dbc33a3e85aedb488d77a8d978da78754b1996099a3b896f1b4b9adb5955baf21b54b15b3d3a2badd21b6982471c337d4de01d42ce3f06451ea9a9e9d7fabc703dbea17f6455219e7881577f2d5d844ec54165c9c12c005af89354f845a8ea334af7bac78934f9251003a8c1ad6b08ea8857cc6b74fed25b532ce15c077b6628ae372b11b8f93fc4df8abe1efd977c15ad41a4de5cdfeb1e29d52f24d174ed5b54d4756bcbff0013be8779a94f1b5d5e4f3c915b269fa25feb1770472430c76f657496cab3491c4faf0f70ee333acfb87786b86a960735ce788735c264b97e0b0b0584a353199a6269e13050ab5f172a34a93957ad4e13af53929d383752b4a118c99eb71266980cab2acc734ccf30af472ccb32dab996371538579d5a34b0749d7afcb0f66ea554a9d392852a6a727256a6a6e5147e5f7edcbae69de22fdacb53d5b4f1744f85669b43bc965b88e5b3959f4dbc976dac4a88d1450dddbdacd2a33ca0dfc45d04655dabc66fef95f71041c8c0e4f618f518c7a67afe15e71fdb5adf897c4fe21f147893519354bcd56f2e255bc9228609ae4cba85c5edc4f746de38c4b38bd96e5639e40679207092c8e116b725bb5ce0740a00e4e71edfe7b57fd577803e1451f093c2ae0ce038429fb7c832ba74331a987ad5ab61f199b556ab66d8ea152bc69d57471b994f1589a30952a51a74aac29c29c29c6115fe1678b9c653e3de3acef89a53ab286618a9cb0d1ab0a74ea50c0c1f2e030b5214dca1ed30b8454a8549294dca7094a527372636e25c9247727df079233cfaf5fc2a896e793ee49fa91e9c76f41f8704691589248e7ebd47b63ea4f4e7f44232723afae7fcff008724907a57ef34e0a1157d34b7df6dff00adb73f3424463d949f6c608e4f5e0f5e68a154739cf618033ebcf00f073c7e38a2b29f2f33df7ecffbbd9afcbb795f44a765aa4bb5b5dd7fc1fc3cc92e67d81cfb9c71efc75fd07d4738c563c9aaa46796c761907b7e7cfe07fdef4afaade88509cff788c9272724f39238cfd39e39e853e14781effe33fc50f0afc3db5f12787bc1763af6b5a5e9fad78e3c5faad8689e12f07699a85fdbd849ad6b7aaea97761631859ae23b5d334d37b15ef88359b8d3f40d252e355d4ece096330c7e539064f8fcf73cc650cbf2acb30b5b1d8ec66225c9470f84c353752ad6a92d5d9462ed18a94e6da8423393516f0783c566589c3e0b0746a6231389ad4e861e8d35cd52ad5a92508c52d15db96adb4a2aede89b3a4d135d925bc86ded8497134d22430c3046ef34b2c8e238e28a2452f2492395444505999b006e2057937ed75fb1f7c5dfda3748f065f7807c097d1f8ce2be87c31657fe2788f84343bbb3d7ae27b0b24b8f136bb1596990a26a724f622d4dd99b5037771676505cdf2c56d27f4f5e06ff008278fc3ff87d63f0b13e1c5a685e2cd764d27c75a2fc445f1d6b73f84f588f58b0d6bc3915cf896c7c49692c85f548ad20d46dbc39a3d8db41a34301b7bed265b89135ed7f58fc4ff80bf14bc056bf1abe29c1f15757d6bc59a17c3eb4bbd4bc39e09bb8ae5e1bef13e870eaf3e8fafc3a2d858c7a5f8635bf0de9d6773adea328bad74dcfc42d4ad6c85e429a25a083fcc2f19fe949c1de2ae51c47c0b95f05cb17c3b98b581a59be6f9854c1e63edb0f8b8cf0b9a53caa1837f54f6589c2c717848d5c5e21d6a74d47174a929d4c3afea2e01f0a736e12ccf2ce279676e8e67816abcf0383c2c31187952af4dd1ad83ab8975d2a91ab46aba7524a953509c94a94dba6aa3f0cff008254ff00c121fc65fb3aff00c14f3e16e8ff00b4f45e09f891ade99f0f7e237c49d13c2be0fd5357d4a0f066ada0f846f2e7c29e36be9f59f0fe9b63aeead6dafbd8697a558a40fa6aeb172ba8cb7734d636c87f663f61ef114de3dfd987e04f8b7c3da88b3f1243f0d7c27a6eb2b7c5a5c6b7a4e8d6ba76b563abc2196432aea56b74c927c93e1c3676b62beccff8256fc3f6f117c60fda13f6b0f16fc5ef0678f3c71e27b5d0be177833c07a05e594bacfc2ef869a1c9617da53f8e6c25b9b9d6ecf52f103e9361268b25ec7629aa69d6b7daec56be77886e92dfe36f1cfc0df187ec5ff00b5af8c748f09cc5fe07fc6ad73c45e3af0c6891826c7c0bae4b776b7baf7832d10ccf2ad9e970eb1a4dfe96ed14226f0f6b1a1b496d6f7d16a16b6ff00e4778e9c3b4e9e5b87a991d2a9f50c831556a622339ba8eabc67b2a356ad4e67252829c285073e5e55254a2928b523fbbbc1ae2055b3fc761735a91962f36c1e1e9e1e5052a708bc13ad5dd3a7f0b84942b4ea69aca34ea4db6afcdf49df49f19a6be2973ff08c59da1e16ee1b8b9ba2a9d992d8e1830e48566233d7af3f9d9fb64e9f2ffc2e2fd823c10ef26bfa87c47fdb7fe0bf8675dd384e219353f08f8927d5fc29e3196f551d1ffb2a0d37c44639a0478731ca8b1b890e5ff4335cf1adc69fa3cba908ad1cac25f1204dc1b63300b9247619c0c0e9c57817c0bf0468167f15be20ff00c1427f6aad3e76f853fb2a7c3d6f137c2dd3350b7b430dc7c41d7f526d2f43d6b4ab5d4e7b7b11aae9f22c317876f759163a69d7b58d2759b2d4e03a04d7b61f8a786f4332cd78b327fecba156157038dc363275a8529d3747d8d584e12954845ba6f9d4634e52704ebba708be7944fd9fc44c5e072ae14cda58eaaa5f58c1d5c2d2a55aafb4756a56838469d28d49252726d7328a6d5353934a3191f965fb51fc1bbcf807f1bfe2ef80acb4ebc1e14f0478f2efc3da36a4566b8b58b4bbdb3b6f13f85eceeef58384bf9fc27ae68f77e55c389e54779143b24ac3e6a7d7141e1c03d33e99e38cf4ea3bf3c9c1eb5fa13e21fda93f664fdbc7f6f9d1be24f89bc05e24d57e1249a2f8974df1e7c3e8fc40cba6dcfc56d37c37e1ff0004787755b3d71350f0de91accfa8782ac6f56ce7b216377a24ba2deb47a8cd677906a15f5ef82bf61efd9b3f6a3f086b5e08f869e10d3bc07a8785fc6fe2193c2de38f086bd7379e248bc0be34d7bc53e31d20f88f4fd5f57d5ed3c610780ad6e20f0beac97372bafccb69a45a699ade93a6df4325bffd0f785df4dccbf0997e4592f897c338ba1530d84c26071dc5595d78e2a352b52a3428bc6637289c1578f334e78bad83c5e21ca7cf530f83b4d5087f91bc57e0a5675b138fc83308cdd793c4c32bc55154e34d5494dca950c6c2a3526a4bf770ab878da138a9d7938b9cbf0fadb55572a0c8189ea7238fc4600ebd3838e9e95bb05ca1e8c49e723a8e9ebcf43fe79af51f8fff00b0f7c6ff00d9d6f3c4ba9c7368ff0014bc07e16b875d5bc67e0237d72ba359acd730a5df89bc3f7b6d06b1a1c51cd677d6f79721350d26c6e2ce78a7d58811bcbf35e8daec7703ef0ce06411f4edef8faf5cfb7f79f0cf1570af1d6570ce784b3bc067597ced155f055b99d3a8e319ba388a3350c4616bc6328ca587c4d1a55a0a49ca0934dfe1799e4b9964d88785ccf095f075d2e650ad1b29c6fa4a9ce2dc2a41b4d29d394a2da7693b33d4126405831e323078e463f0feb4561c774a464f2081823bf5ce78ebc8ebce73457a92c37bcf47d175ed15d34ebfd5b5f3d4d3ff00836f2f3f3fc19c9f8a75158a1750d8e0839cf7007e3cf7e327afad7e85f86fc03e06f05fecd7fb27f8775af0d687a9789ff69ef8b1e2e9bc61aa78eb4bf11b7873c3563abc3e1df09fc2bd41a4d1fc2de20fb6ae85a6df6b9e2fd2eee008da75e6a3ab69525ed94bae86b6fcd3b5d3f50f1df8d3c27e06d1d5a7d5fc61e27d0bc29a5451aef79f52f10ead69a559468a59773c975770aaa965058e3701c8fd16fdbafe227877e10fed43e2ff000168fab6a9e0db4f82569e0fd33c1967169571afff00605d58787741bb87586df6771a6e8f733ddc5af584dad6a1aa7867475d397c3da5dd3c9a6c37ba8f877f853e9d3c675307c3bc27c0784c4d4a5fdb78cc5e759a468cdc5d4c0644b0d470d4310a2f9a542be3f32862a1149a75f2d849b4e9a4ff0074f02b23855ccb32ceead38cbea5429e0f0d2947992ad8c539549c74ba9c2951706d35fbbaf51753fa03f0bf84be1f7813e13e99e17fd9d758bdf89917c01f86fe0fd52d742d4fc516baff0088bc617cdaa6a3a34a747375a74fe218759f0459e9fe1b92f1259f4df0eb1f1168968a967069d66d1ff321e0ff00811f1a7f6aeff82927ed4de37f861e03d5fc27f0cbc35f103e1b7c4ff15ea7a4e8a9e0eb1b8d4fc587c41e267d4f4eb5d5bcad4ed3c497d05bdaea5a243a645ac29d5e18b51ba363e1dbdd4ae2d7f74be11699e13f107ece5f0c7f695f83ba4eb5af6a3f1a3e1d6a56fe3c8878b350bbd2349b8d3af2cd3e205df827589d67d734dbbb7d7f409355d3aceff52d45ade15b8b277b597cc92e62ff00826bfc70d43e20789bf6b0f815e286d1aeee7e0afed01a8eb3e0ef1edabc963e22f1dfc3cf16ebbe23d4be1fff00c24138945fead6fe14d234cd37c3a45d5f35bd934f178260d2ec3c39a16936337f99b85c24d53c16279de229c71f2719564a38951a745c2ae1ab578b6eac632551c5eae31953509497325fd233c5ca92c6b8c23cff0057853938b7c8e52af4a4e6a92b45bb725af657751d9a4a2be2bf82ff0000be3ffec95f177c47fb4b59fc25f10c1f1abe237827c46a7c012f8cf4f93c33750eb570c2cb57f1ccff000d3470f7761a6e8de17d36fad34ebd834abfb8d6b497d2fc49e2544f0bf81af67fa924d73e28eb7fb2df8c757fda56d7c69aefc4ef08f8db55f11af8d6cb48f106bbaff87756f116ae9a668105b6890dadd6b1ac7832eefa1d734658ecf47d2b47d3344d2a2934dbdbdb06f0dea76dfa4df118dfadbf8b2cb41d46ca1f129f1069c749d1af27b84fed6f0c6b5e0ff0c69d6d736d2daddda32e8f6face8be25f2633749651ea2b72cb019e7559fbdf07699e1bb11e2dd47c41aadaea90dbe87a0e99a86a5712f996d73e2337bae6ada9e9823bd9ae66bfb4d06c759d02013ccf7ab15d5d6a966663796d7d05bf2e3f0b96e3b0d8ac263f2ea788a78e5570b88e7752a7b7a1564d497245c1c534e32f6909a9c67184e0d4a1170edc0e699860b1586c7612b2a388c2ca8e2a9d685a3384a97f0e119c9c97227cd174dc651945b8c938b69fe20fec55e204fda06efc4daa6bd1de78864f079d25f4ef0d582dc5d694f7f25ede7daa7f175a25b0d4068968ba725bdfd8422d753dba82cad673456f74b0bbf69d8be3c2bfc62baf8bfe35974ff0081ff0016b4cf10f863fe11bd4e7ba4f0a68de28d227baf13f8734a8bc3da70bc874796c6efc2fa35d47ac586a6fa96a9e2583c35673dddd69bab3ccbfa992fc26d2fc27e16d3afbc13e1bf09784754f887addbe9dacead772ad9bdbf847c3b2f89b546b5d535a827d3f52d42583c5be37f18b092e2e191ad354b1d6bfb36e648c4177eadab780be1f788be0b587803e2368169f112df5cd413c4b75a6cd15ac1addf5c43e21b1b9f0ceb90c0a65974cd5f45d2ff00e11b7808984fa6ad845682e58c2a4fcaf07708e49c1f97fd530b4d62272ccf1d8878e853f6389af08e26b470b2c5463297b5fa9d2e5a3084e51bd45eda9fb2774be8b8a78c733e2dcd678dc6370a50c160b0d4b092aaeb51c2c951a32c44f08dc631a72c45682af2928b6d4bd9cdce31b9fc24ebb2e99aa7c57f08fc10f82fa4ce3c57e20b14f1ed9dfbe957b7f7f2f88757d3df4fd367d11f46d0f54f164fabdfa5ae9a9e1fb7b9bc67fed1bb5d36f6c925d3744b81fda8fc3ef0c7c46f871e1ef81363e3346ff859da37c19d2e1f1459ea1a868863bef8907c33e1d93c5971ae6b5a1de2c9ad6aa750fed283fb66dafafa2d5ae34f92e92eaf27d4127b8fc4fd3bf6c9ff008263fece9ff051ab6f15780fe11f8c35fd0bc3de1bb0f841e22fda074fd46f35ff00047823c69384d1ad1bc388d677d7f796163e199ad3c3d34dfdaf6d6f7f637115cf873429751d4bc3a7c75fb99fb5bdc8f0bdd7ecff00acdaf888ea569aef8da3d3a796f6f2d6ef57d5744f15f8a74b69b45d164bb592d2f754bf4ba5d0b42884b04496974db2e6ded6d7ed11fd6d2ad0af8dab4649ba588a189853bc545a6a87353969a251f67cced2fb4e099f318d8d454a83e49465154e494a4a6e52e65ceb4d559d44da6937150938aebc4fc4df84f7df133c1971e31d4eff00c47a75ddcc1e25b9f0878734ad33446d4acbc6171ad4be1f1a5db58ea97d2787f58baf13eaf2d9e917f6dadeb3a7cfa88b8caeb5e1b864babf87f9daff008289fec47aafec67e3ff000a789743d4adbc41f0a7e2dc17da8681aae9b0c10e9da178aac8c537897c270c7697177041a64525d26a1e15592e0ce7467934e91aeae744bdbc9bf753f6defda793e0f4df007e15684fa65978afc69e20bbf897e21f105f6bbac687e02f06e9ba578a62f1558457973f65867f14c5a978dc1d0f4fd36db51d2ae74f82d175bd7ee747d25bf7ff00297fc1427c29e35f1a7fc13e7c4fe2dd674ed7753bad27e27e87f152c6f350d620d7126d06eb52ff0084767f106926396f4db69cf69aeea5aa476716a1a92d9586ad7ee9ab6a3a6c16f343fd17f467f117883823c50e19c353c6d3a191f14e6385e1ecf32faf28c29e221899ac3e071979492a55f2fc5d7a538574af3a53af856fd9d594a1f9d7887c3f84ce386b1d52549cb1597e1eae3b015a0bde8ca92e7ad49593bc2b429ce0e0f6928d4f89439bf01f4cd4166b6562c01c2e72d8ea3ea7d33f53eb9a2bcef41d4e37b31972a404ce73dc1c7407fa7e3d8aff006a635a138c64acd492777e6a3fd7fc333f8e67424a524f74f5dbf57ebfd2d7e88ff827dd845e2ffdbe7f656d22e526b982dfe33f8475f7485646643e15d407892176311568e25b8d2a1f3260ca228fe724102bf6abfe0a9fff0004cef1e7c69f8e5f07fe31fc3bd4bc3ba1dd7887e30687e1af13695adf89fc35a3e9b6baaea53e91a0786b5fd1575bbfd3af3591ace9be1cd224d4f43b0b5bff001343af68573a968fa378aee35c69f42fcfdff820bf802c3c73fb6cf893c75ab59b5fe9ff0007fe13ebde20b185152561e2ad7f59d13c3da13ac4cca59e0b5bbd66ee2642ce9796f6db5588cd7ae7fc168be26fed03f15ff681f0a7c21f01ea3e3cf0d7817e1869717c51b3ff00845ee2db4ad4353f8957ba96a1a341acdc3eb7ace991789ed341b48f4cfec9f00e98ff00f099789c6b4fa4782ac61fedaf12a47fe38fd33388679bf8c184c0e1aaab641c2d81c1d6e67cca389cc3178dcc2a46cdab5f0b5f0125b35eedb667f5cf83997c709c332a95572fd771f88ab76afcd4a8d2a3423b6bfc48d656baba766da6cfd62f8dff0006347fd9d3f633f885f06be0fdcf89fc2de20f06683a97c47b7f1d6a016eae3c45aa8d3f51d5be296b1a9d85acd026a305df8434eb8d1e0d32c608acf46957c39a5f87e58ed745b59a0fcf8fd88fe22eb3aa7ecfde00f8bdf0dd3e187867fe162f85a16d346bfe09f1347e39f164de025d7f4c8f52bdf17f95e21d5af356d43c2fe10f11788ee6c74e97c46b67e28f10dfbc71ea179a9482f7ea0f845fb47fc54fda23fe09eba6f897c65a249adfed21f0d23d7348d6349d5a29b4db0f8b12f85a2d661d5ac74bbed4d54eb363e3ff000ce99af783e798bb6a5a9f89349d575c9b4f86cae2f26b2f907fe08b577e0bf8f11fc52f06f88be1ff0086be21f867e0b6f6f84baecd697da24da1be95afbd9681a0eb22d2f6d6dae66f1059c3677b6fa7dc4274cf0e6b3e13d626b4b58f5f9fc417337f2e5452581c1b52ab19c675e73a6a524a5520e8c14e94db51e57171955518b729d57293b591fa6cad18e2af1a7351ad4e1ccd295e3284df2c925d5c63ecdc9f2c7924a31e6b9fab687f69ff001bdf782ef355f821f04f5bbe7b0875bb9d52cbc67af585c43e17d5e5d1ae65d6ad6c241616fa8ea77114976e21d72db4a798daad9dba592eb1aa8d3fda3e317c4af0e5acbf0cfe1d691e1392cbc2bf101fc496da8ea3a8e996f158cde14d09a3b2f11697a5584705d358dfea1a86af6293df5fdae972ad935ec1a65d8d72f34d993e9df875a6d9f85b44d3acf59b5b0935c913fb2611146f39b38a77b9be87c3f0b96b8fb359da5988ee66b75296f1a9645536f6f6c0f85fc61f83167e3cd6fec57b0eb76fa778662d2f58f014fe13d72e7426d3b51d26eed7555f094b636ed358de69de21b9d32282facb52b59633005bab2b8b2bcfecf16bc146a375ed2e7b45bb49c9bbb6b556d5be6766f5d126ed748c273b52e64a318ecd416bd2c97336b4dbb5edd1bb7a1deffc2b8d57fb5fc5b79e1cbb48fc3d21fed3f0f6bba55cdd3e922ffec77168ba7f862e92f6caca6bd8a3b3b9bb1a45bc5732de2137f20b9b5b9b51f3d7c5cf8b1e12d5fe2e59784bc6f65a9fc3bf0a693e09d66efc47e23d45a4d2b59b1f11f8cef74af0ff0082756d2f57d25ae752d36cacfc39a37c4ed2f5b9ed27fece834af16e8fe2057bf874ef10af877eb1f0ee9ba25ca47e19d6c595f6b3e27b27bff165b24d183a85ed8c563a76a1a8b5ac0cb1c6b05d2da5bc93c11b2b4cd664b9574dbf077ed97f0bbc2ba37c24f8bff1d7c7fa1f8f2ebc77f08f6eb1e0dbcf0b78c354f0ee91e2579efad7c3fe0d5beb0d1da43a7f86bc2f7dac7db356b9d1ff00b1bc4961a55e788a2b6d45a3bebabad461f2519c22a352378a778b7cfcb36a093e6766b9bde7cebe171ba6aec746529c6514d3e6972d92524a5a36aeadcaf96c9356b36dae88fe5d3c61f08be0e7803f69ff0010fecbd65e19f02789e3d43c41ae785fc37e31bbf8856d717fab69de27f127862cb4bf1fe8fe30b0f06412f867c39a768be2f92efe26f83e1d0f56d2f5bf116a5e2cd7a4d56d6c34df055ad87f575a9ea9f043483e18f06f88efee35fbbf84d65a0d87c23d6e6d4edb5fb4d7ad74fd3bc2b6f792e9b2786e49e1fb67832caf6e349bdbcbfb2b3d63ecda6cda819279e3babd93f94dbcfdab756f885e3d7fda0a4d43c33a6f8b7e1f69714de1991ecbc512e8fa4da7d8258ac22f10d9e9b0dde8df6fd2346b9d60db41abeb769a9dfa5fe97368734fac27c395b5fe823f60ed36cfe3ff00c14f037c7a8fc4ef169bf16b42f13a699e04b9f07e91610f84eef4f6bff0078835ad23508a1b6827d2afeda2b086c2dee34b695e1d452daf59b52d3352d4353f6d60234e741ca551252e59f2c95e70946d3d6316a9c5d2528b7c9cbcad2569462dce3ab5554e356714b963cb7f7aeabbf7d2b3776da516adaab4fa592fe75be2bfed53e38f13dd45ae7ed31e13f177876efc33a743f0d3c0fa67c4af0d6ab0e9be074b59249becba7dc34be14d413c45e35b0b3b7d435bd2342d1fc5de20d4f50b18f40974ad226d2ecebfa38fd947e1378ebc65ff0004d5f017c28f8b320b4ff8593a0f8cac63b1beb52f7da7783fe20c5ae3e8e9a9cd78f2adcf94675d6fc3f71a44ba6431f87af7408b4f7b7fb390df1b7fc150be257ec21f0c7c41f0ebe2cfc4df823a8fc6ff008ebe24f16f8775ed1fc2371acf8caf6c34ef85bf07b506be93c61a9f82bc19e23b5b5bb6bff87de15bfd26c75dd6ecb5c8adbc3da65feade25b4f12cfe19d1bc11e24fd9df0cf8ebc11f163e0a786be267c34d4d3fe115f1ae93a278b3c3d3acb0c50dbda5d69f0d8db4491c724b65671e8fe53457b6f685acd1f4bbc851d9034f5d580ab5e9e674aa4672a588c262686229d585e1569e22338be783838c6f427aa71845f3a4d68ec9636519e0211545c29578d48b8bd61caa2a3ece317cd2b54bceee567522afddbfe0d755b1d57c09e23f14782b5e88db6b9e11f116b1e18d66d8330fb3eada0ea173a5ea3072a1b115e5acc837053819201268afadff00e0ab7e09d37e18fedd5f19e0d10c6da2f8da4f0dfc46b3688150d7fe2cf0fd8dc78a99d8fc9248fe3483c4923b44ceb97daede72caaa57fb83c15c69feb1f08f0d67d19c53cdf25cb730a90534bd957c4e1a8d5c45169bba950aeea52927b4a9c96b677fe35ce326782cd730c2723b61b175a8c5dd2e68427684f569fbf04a6b4ed6dd1f7e7fc1156cf5cf007ecb5fb5f7c7ed064d1b48d575df19781be16beb9afdcdc698d1f876d2dbedfabc5e1dd445b9816e8a789ee2f756d41af208fc3f63a38d6641249671254fff000519f1c4df14f4bfd977c6df0f7c5971e1cbff0009f89bc19e1eb61a2f8cbc411c1aaea3e0cd4daf3e1cfc49bbd1f4bf08cb378435cf0125f78c8aea6d269aba3ea7e34bad3e5d5a1953c33a844515fe4178d589ab9878bbc7d5b14d54a90cde861a32b59aa387cb72fa14a1bbd234e1156dbcb6b7f52f047ee320c8e34bdc5ec39ec927ad69d57517bc9e8debdd747b1f4f7c4ff8a77fa3fc05f857f01bc11ac787ad3c4df0df46f0bea7e1cf141d7fc77e20f1078af55f0a2dd1b8d735ebbd73c2f6ba558a6b373ae6b9e24f0f9bad4355f0cda7882dade08747bdf0ca695657ff005e7fc133fe0cf847e147c2ff0014fc40b0bad16f3c6ff19be2078aaf3c6d7fa0e9c74ad2eded742f12f8a2db46d0e0d345ecf0c7345777dac7882f3502916a5733788869d737177a5e87a07d94a2be270586a55f07275137ec6ab8d3577cab9e31e66d3ba7269daef5b24ba1ea627135614e4a3257a9529ba92b2e69eadd9c959d938decacaedf73affdabff006c8f89ff00b307897e1a784fe197ecc9f17bf68cb7babbd1757f881e2ff0c5cc9751e89e149b52b9d1755b5b68acaeef6fb55f88e96d39d6ec2c3c47a5786fc0d776522c63c496f74674d23eb4d4be21d9eb165e16d52c85e787f55f195bf85b4fd28cda459a6b5e1a7d7756b2bebbb6d42fd27d67443aadacc61866d37ccbeb27d634ff00223b8b94b94725146172cc2cea549ca32bd3a52ab0578a4a50a7292bfbbccd5d2ba726ded7d59854c4d48d0a2a2a29ce4a1297bedc9371bbf7a6e29d9daea2afd533e3ef1cfede179f083f6bff000afecfd69fb307c6ef14785b56b5d1740f187c75f0ce957773e11d1f5dd6ec2c4e91a85ad9c3612da78974a4115b45f107c4a75fd06ebc2971a6eacc9a66ab6fa4ea0f37d89f1674af08fc4af02fc44f845e2ad4a3d2741f8d1a1ebde13d52f74e10e81a908b57f0bff6735ce8da8df25edb6abaee9b6b64fa8d9a35acd6db232d756f3db59cf148515c51c0e1eaa539c5b6a306b54f96ca0d24da6ed1be976daef7d4d2789a949518d3e58f345ca4d735dcdb8b73d64d293bdbdd4968b4beaffcfefc5dfb13fc53f86bff000510f84dfb267c46d15f45d17c43f133c11a0eb9ae6910d9f881756f00f8b3c59a7d96a3e35f096bfa8d8eb13d9787fc45a66a0f3e856515dfd920d74ea5a4dde9ba7ebf06b1a458ff00653f18be28f813f678f01a7c0ed1f51d2bc00be22f06de7847e0b685a0437dabeb56b1e91e17b9b1f104d1c109b49acecbc35637da27d8efa59efa5d47c4dace9f13336a335bc3a91457b595538d4c362ea546ea38ac349464fdcbbc4d086d1e56bdd6f66b777d1d8eac6ce789c4e554ea49a8cb0b5ebc946caf5214a55936a4a49a728462d35ac2eb46ee7e32ff00c14c7e0769df0cfe2ce97f143c6127c41f1e68fe30f877f0d3c3ff000f34ff000369dacd80b01e03d43c35e12f18e89378c347429a3dc42d71a57c47877cf67a76bfae69fe1c5d7f4ad4b40d2752d3eebf4f7f62d4f0dfc1cfd913e06e917779ad6a5e04f897abebada0782bc5da4e93a76b7e17f01f8db56f195d78527b2f08697a96ad73a041ab49ff00084debe8d7da95edce8d6bab6a09358e9dad0bfb52515c187a7196655b9aedaaf5ddddaedfb493d74db45a5ada1bd69ca594e11b6ff79539249376e5a74e508595ec9a515795aedddb7ab3f3f7fe0b89f059fc73e2ff00d9a7c4bf0db4392f6ee1f879e2ef0deac6c6d8c5671687a76b9a46bfe17584ac5046bbe7f15f88e47b5550d6ecdba450d3ef90a28afed7f0b78f788327e03c872cc2cf0b3c36116654e8bc450756aaa6f37cc271839fb48de34d4b929ae55cb4e318fd9b9f906719165f8cccb1389ad0a9ed6afb173719f2a6e387a51bdb95ead4537aeb2bbd365fffd9, 'Malang', '66118', '', '', '', '', NULL, '', 'Jono', 'Jina', 0, 0, NULL, NULL, NULL, NULL, '', 1500000, 0, '', '', '', '', '', '', 0, NULL, NULL, 0, '29613', '32674', '73434', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 41043, 0),
(328, '15820', '15820', 'AJENG DWI BINTARI PUTRI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59548', '14238', '77061', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 27214, 0),
(256, '15821', '15821', 'AJI MUTHI`AH NUR AZIZAH', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60549', '36507', '71270', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 63098, 0),
(77, '15822', '15822', 'AKBAR SEPTIANSYAH', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67602', '56806', '21015', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 8726, 0),
(293, '15823', '15823', 'AKHMAD FISABILILLAH', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '73370', '43608', '37379', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 18206, 0),
(294, '15824', '15824', 'ALFAN ALIEF SEKTIANANDA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78757', '26000', '33174', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 55048, 0),
(329, '15825', '15825', 'ALFREDO FERNANDA', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58933', '81892', '46677', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 35108, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(257, '15826', '15826', 'ALIFA MARCELIA LAKSMI SRIVANI', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93054', '31679', '59391', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 51784, 0),
(295, '15827', '15827', 'ALIFAH PUTRI ADZHANI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '98443', '67261', '16139', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 45955, 0),
(184, '15828', '15828', 'ALIFIA KARTIKA RACHMAN', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42494', '47829', '83212', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 20450, 0),
(112, '15829', '15829', 'ALVA AFRIZA PUTRA PRATAMA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1989-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66118', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52713', '15783', '76313', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 27339, 0),
(296, '15830', '15830', 'ALVINNO DIAMONDI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83553', '76227', '52407', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 27581, 0),
(297, '15831', '15831', 'ALVINO DENY SETYAWAN', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '89522', '77825', '35550', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 48216, 0),
(298, '15832', '15832', 'AMALIA AIZZA CHOIRUNNISA`', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28025', '76449', '12944', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 32657, 0),
(258, '15833', '15833', 'AMALIA BALQIS', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86832', '54002', '25192', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 14406, 0),
(185, '15834', '15834', 'AMALIA PUTRI USWATUN HASANAH', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86486', '74866', '78885', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 15526, 0),
(330, '15835', '15835', 'AMANDA PUTRI ISMU CHUMAIROH', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96968', '15091', '79514', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 42430, 0),
(41, '15836', '15836', 'AMATULLOH ZAINA JANNATI', '', 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', 'WNI', 2, 4, '', '0.0', '0.0', '', 0xffd8ffe000104a46494600010100000100010000fffe003c43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d203130300affdb00430001010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffdb00430101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffc00011080078005403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00fe6cd509393ce464633fe7d7a7d00e054813aed1f5e7ebeb53818007a52f3d8679afe27febfab1ff00590a9da2b4ddaf9bb2b3f9db62058c8c6541c7ab639fc013fcaa4285be5e80ff0074004fe6401fd7f42fc7af14f0ad9c0e0ffbc3f519cfe3f8639a15f4b77d3d4d2118a5ef256df7b2f5724fefbed6d7722f2c2e01c9381d4febc679fc4fd7bd3803c051eb9e7a0e7d4fad51d4756d2f4848cea77f6f6ef20631c4f26eb8902ed1f2411ef99c1240caa1018819cb0acfb6f15e8774d1476f2de48f234a18c76176c22f2c16632831865518080152eee711c6e01af530b926738e87b5c26578fc4d3b5fda51c256a906add271838cb4ecdbee7e7fc45e2d7855c258c8e5fc4be217066458ee7f66f039a71265383c64649a4d54c256c542b52b36939548463aa6dd9b6741b48ebc7f9cf6c9a42318f7cfaff5029c8af2c31dcc64bdbcaa1a3941f91b78c8c720e400410541192080722907cc40240c0c0f73f9f53fd2bcd9c274e5285484a138b6a509c5c6516b46a516934d3d1a6934f47a9f7986af84c6d1a589c157a38bc362210a9431186ad0ad46b53a918ca9d4a75694a54ea42709294670938ce2d4a2da6af19552318e991dfbf5c7b7e62a368d3392001f56cf4e9dc7ff005aac1e09ceefae719fd3a1fc68ce0600f7cf0dfd31537d6fdadf81a3a69ab68f7f895faab74d36dffa548c49e9f5e071fa7a628ab5cf603f3c7f43452b7a7dde9e7e5f9763274a29fc09ff00dbbe9e5e5f3f9b1fb4e40edeb8c8e9d8f14846091e952e30474033c003be0f7cff004a08c93c03c0c67ea734cea515c8df54d6baf96dad96fd34e9dd0c09900f3d0fe79e3fce6b99f126b3fd9df60d3609766a5ac4d225b3e54982daca33777d74cac1b223823f2235c3eeb8b8801013791d4aaf078009ce7d38cf6cf4c76ae53c1df0bf5df8d1f1da1f0ae9b30b2b1d13c2e973a96a47698ec2dafa63187264023375713ceb1c51f25e38cc8cbb5091f45c2d87c055cda388cd6718e5b96d1a999631495d54a5867050a3ca9373f6b5ea5283824dd45274d2bc91fcdff4ace37e21e08f077379708a9d3e28e29cc303c1991e2e949c2797e233b862678bc7c2a2b7b1ab85cab079854c3e214a3f55c57b1c439274acf87b3d3b45f17f882ca68a7986a71ef96dc89370ba0a7cc92ca68d8ed94140de48c798b26d937128b8e86ca3d0f50bf369248fa44ef346f15ec1b83412322c52b3c417e6b4744569edd54bab45f688425e280bf6078f7f60bf15681e1896f7c1f75a67c46f14694d9b6d57e1df8974ad3f5db58a3329369e29f875aea69d7f7f337985575ff0c78a6e6fa00ac874799231037c2de32f067c4cf024114de3af0078ebc1ecb2492ac9ad7877548a0775da6796df50b9b4446b79d956468a499956e019a1d88d22cbfd1592f15e4d9c424b038dc1d34f968d0c34a74286261ca9f255a7879495654ea4541c21528d3a90e49295351716ff00c0ae24e0ce24c9eaaaf9b60b31c6e2272ab8bc6e62a38ac6e1eb4aa4a13af0c56314654feb14aa3a8ead78e26bd2abed63285694f9d2aafe3fd6bc21e3497c39e24b63243712451acc9324b1cd04b8fb16a16774411345246c98126e69a3923469b7ec7af73785a44263caba93b71c76c82c067a83d3078c938e83e62f12acff00103c0377e2187cabcbef01dc6996f3ddc483ce1a25f2dc5bda5ccaedb5bc9b39ada2b21187696da1dc8e5228d1c7bff842fcea5e19d075012348d71a55919647fbef32c091cc5ffdb13248ac4856241e057e61e27e5f8773c1e674a9429629d7ad80cc5d3568d4af0a54abe1a735a253a9424e49b5cd28382727c8affea2fece7e3fce71b438bfc37cd730c563b25cb72dcbb89b83e8e2aaf3d5c0603178ec4e5f9d6130f293753ea943307829c69d37ec28629e2aa46109e2e48dd43e7c793c4abc363f00460023923d3ffac281cf0491c11c63afff005bd7f4a91d14b798990c3ae30323200ea39393ea300f7c60ae73c9c82719c907b01c7ffaebf2369afbedf3b5ff00af43fd4192b38a7672b2574d5a6b44a4f4ba97477eeb5d6e420e382141f71cfe8307ebcd14f6620e063a7f8d142f56bfa5fd7c896acf58f6ea9ff2f97afdc8763a741839e3e847b7ad3b1ce3807be4e3afd69769f51c75fd7d474f7f5fc6a4dbc83df1cf009ce38fa63da8516f6fbdedd3fcff00ae9717a493575bab7c97975d7f51a06de739e0f438ed9fafd31f5f6afd21fd8cfe1ab6b47e277897c25a6d85878965f875f0f60b7b8963b8921bcbfb7b8f187daefa552f3cbb9cc56c268edcc51492408122843330fcdf6cf18ce7070700e0e0753d07f5fcabf433f60ff8c379e19f166a1e0bb88e35b3934bbdd56ceed1b171318af2c4cba6caad88e5487ed1757b6e49570b7176ac5d628bcbcb18eb43038bf62df3d4a3084e09be59c2189c3d794651da717ec55e2ee9daf64d23f9cfe935c3d99677c0583c4e5f0f68b87b3da79d6369b938a8616195e6981a95d2ba4e74658e859fc51a73a8e0f752fa47c2ff000e3e2a6ade30bf6d4754d1afb4fb4b8d30e9f15af86a6d235cb28dedacff00b45753d4e2bc92db5057d55aeffb3da3860b98ac521f39ae19e75b7f61fdacbf67df88563a05adde85268f30bad06247bed7f42bdd7ac22ba9602d6b16a10d8ab5ca584970562bb91037d9e39a4b90b7332436d71ec7e2bf8996965a7d9f89b42f0f4de229347692e350d0b4cb9b2b2bbd52510325b234d752dbc1e525c6c92e3fd22195a303f7814bb2fb45cfed05ae78cf48d2fc43a87c31d67c25a4e850d92de6a1ab5e69b3d8eb5632dbbdacd651698af26a96f2e987ecb2b5d5d5b436d2a2dc2a99637490f9b4714ab62659954c3e16954c3c7092861e8d354f0f51d1b46aca585a7ee3525c8e51f75544ea3bee8ff3b6b51af468e17054a588950acb1742bcf118de6c673e2f99c69d3c5d572abed1373fab4ad37494e92b38c74fe6a7c53fb1a7c65f831adfc44f1dc7e019fc5ff04fc7bf0fbc47e16f19ea3e10d2253078764d7b478e7d3bc5634197379a5d868fe2e834fbfbc9ed8cc62d161bc7731cd70e6bcdb5ef04e89f0e348f06781ac2c67b5d6741f0d8ff0084a2ea7bbb999b54d4755d5752d674ebc96c6e598e9372ba06a3a4dacb676d29b67820b5baf2e3bbb8bb2ffd307ed0bfb47f86fc0df02fc55e25b4d06db5bd321fec8b4bdf0fb5cc5696daea6a9aa58da49a5b4f14776d1c3770cf325c8114c7ecde68c1c92bfcd4f8f7c67aa7c44f1a789bc71acc36f6da9789b57bcd566b4b452b67611cef8b4d36cc39694da69966b6fa7da3c8ef2b416d1195da42ec7ec715c4998e7b82c3d2c5aa51850aaa739d183a6b133a54a54b0ae74e729b8bc3d0ab569f345c39e1ec2328da9294bfa1be869e17d4c9f8c78938e6861b1386c9e9e5d8ee1fa129e353e6c7e271194e3f1b8595284a352b5094614f1b3fac43d9431ad54c3cb9e55a34b8b23008da33fd0e318e3af7c64e38c76149b3a7bf51e831fe467dfa54a71c920f3db24720fb1ce07ea78edc25792d495da6975eeb5b5fa6aff23fd1c928c9a76495a36bdef6df5f27a3f3f5b321644cf3e9ea47afa29a2a42e01f5cf231ff00ebff001a2b3db4716f5f35d63fddf3efd7eed3963d629bd137ff0080ff009fe0228033d327a60ff2ce4f4009fc3d29ca42820f3923af27a76f7f4c67fa520505cf38e300fa71dba64e7e80673d6bdafe057ecebf1b7f699f1c5b7c37f813f0d7c51f12fc617301ba974df0e58f9d069760b2a4126abafead70f6fa3f877478e5962864d5f5dd434fd363b89a0b77ba134f146ddb85c355c4ce9d0c3d2ab5ebd692853a3469caad59cddad1a708294a7276d14536d3b743c7ccb34cb725c162b34cdb1f82caf2dc0519e271b98e6389a382c0e130f4f59d7c562f13529d0c3d182d6752ad48422b7678a023b7500e3923381d4800f3d802074e0d773f0e3c4dab783bc6be1ff10e8d6af7f7d69a8448ba644257935582effd12e34c45895e4325f5bcd2db40638de549e447891e445afe8e7f67ff00f83683e3cf88d2c358fda53e31f833e11d84b1c77571e10f01d9cbf11bc66f1b81bec6ef5579f43f0968f740e41bdd3af3c636a853e582757debfb6ff00bfe0945fb267eca105b4ff0f3e1b43e28f1c5a44cf27c50f892d69e2ef1b25c4299f3f46927d3adb46f0b3b1cc264f0a693a34ef0c8d1dc4f72cceeff00a8649e12713e68a35319469e518392739d4c638d4c438249b50c1d293a9ceacd72621e1d6e9be8ff0088bc5afa76780dc379766992e4b8aade276618bc3e272fad966434aad2c8aa43114e742ad2c6f10e32953c1cf0b529ce51956c9a19b4ed2b4617bca3fcb6f88347d4a0d6b52b6835fd6b4b8ac355d57c39e20d174cbbb25bbb2d7f45bb9f4dd77c3dabdf4706a8d63aa68d7f04d6330b42f6c66b7998fdb2092376effc3fa5789753f0aae896fe28f1ea5aabb216d5f568eea1ba8a28c4115bcf6b26976d6ab6db51a49e7110ba624bdb4f63023452f4dff050ff00d9ebc79fb2cfed73e33f18682f790fc30fda3756bff1ee94e2112e9961e38912d9bc69a1dcdbca935af997b772a7896c6795639666d6f528ad140b19e46f29f0beabafa58bdd6af7128b09144af04291dbdb4a006051d60581e4566c1689d8c4dfc418815f9ef1565b0e17ce730caa74e34e8d0ab25084f0d4655eae1ab72d4a33a35df2dbdad3945a9ca949d2727c9cdcb73f8fb84f892bf15f0ae1332a10c33799469569ca318cd61f15467c95a84655233af1587ab0945255ef28c1734a3294dbf3efdb2b46f15dd7c1bd0e2f0c46752f09783b5dd226f89d259c7713dc6937fe27b1d5a3f00df5d483701e1fd4e4d03c5b6ad76c8b045ac58e9f0dc4a1ef74d13fe51b2124938cfd08049ff6b91c63d7d457fa137fc1217f671d2b54f825e2bf8b1e32f0fc0d79f1c631636b6fa9d943736f7bf0d7c293dd59786edf52b0ba85edaef4ed6357b9f13eaad6770b2daea3a06ab6514c9b679436b7ed1bff0006fcfec11fb45b6a3e21f0df8535dfd9b7c7f7e1e7b8bdf833796b65e0c7d44e57cdb9f873aa595df856d6d198095ec7c251f849a5dd979f79777fd1ff00e21963b1790e4f9b602a61f0f8ac6e5d85c562327adcf49d08d7a6a74254abc9cf9dd6a0e9d4a90aca9ba75252e6abefc20bf4bf073e9c9c0be1acf30f0db8e322cce9e5d94e7799430dc6790d2a398471553118a75715fdb5957361b151960ebbad85a58ecbe7984eb612861292c0c1d2756a7f9e09573838c63904f1dc741939f6c1e7d3a65bf36d3d4e79048cf71d79e38e7fae6bfa5dfda4bfe0d92fdad7e1ad8eabaffc03f88de00fda1b49d3e2b9b98b4196daebe17fc44be821532ac163a2eb5a86b9e11bebbd80a843e3ab19aea4005b5a1925482bf9c7f17f83bc55e00f12eb9e0cf1af87f5af0978b3c35a95de8be21f0d788f4ebdd1b5dd0f56b099a0bcd3754d2efe282f6c6f2d65468e7b7b98639626186038af81cd721cd726946399e06b6194db8d3a92e59d1a8d6ad42bd294e94a496ae2a7cc96ad24f5ff0045bc38f197c32f16b095f17e1ef18e53c49f55853a98dc261a7570d9a60a9d576a5531b9466143079a6129d46a50a7531184a74ea548ca109ca5192399c21e49e70074e9ed9c73f5a29f965e0367dc38fd739e7e87f5cd15e1386bb2e9bb85fa778dfbf97c9b3f5257d1dfb3f863b7bbe9d13fbfd4b1a569b7facea961a46976b35f6a7aa5f5a69da758dba196e2f2fafa78ed6d2d2de31cbcf71712c70c2839691c01cb0aff0048bff8255fec65e16ff827c7ecdbe08f0ceada269cff00113e27c9a6eb5f18bc7717972de8f1bdfdac52697a17db86e493c23a04778de1fd0da0922b713a5d6af2402eb5ebf907f093ff0004d5f8772fc4ff00db8ff677d063d3e5d522d27c7d61e379edd2169a103c036f73e30b27bf451bbfb35f56d1f4fb7bf2bf30b69e403e664aff00469d3fc71a75ff0080759d17c48a27b5b19ee34b9efa24d86de10c23b0d4248623b85b794f0acf3460b5ace166dbb0971fd07e0bf0fd1af87ccb3aa94b9eb3aeb2dc34dd93a34a34a957c554a5771b49bab875ce9ddc2955a76719ce12ff0021ff00696f8a199d2c6706784b97632a50cb313977fae3c4987a32945e3e5531d88cb787f0f89716bda61f09572fccf16f0b2e684f112c16267153c2e1a4be84d7ec944af09cf9531631b2e41866cfcea841046725902e39dc070d8af997e237c4f4f85f04cb79e12d47c597178255b336b7f058a928a4c703bcd69308bcc2c4194871bca82a1466be84f00eb23c73e09d32f9e68ae356b5b64b6bf96375749b50d399ade79c1072d1dcc9034aa463893aaf38abe25f06e95e2cb029359a35ddb921e3650436324900e4e0138efc0eb9c57eef86aea11787ad2e49c26a129dfdd8c95b9253574dd3a8b4725a7577b9fe4dc25c925cc9ca29d9abb5b767baefa1fce1ea3fb4dfc17ff82887c5af8f7fb00fc7ef86fa6fecd9f1d3e1f6a7a0f8bbf66fb7f16f8c2df576f8b3663429b528b58f0f6af368ba05bc5af451cf7369aa7852c46a77375e1fbebeb9d1ae7549741d6db4cf25f845fb11cdf12fc503e1c5959db433e911dcdcf89e5bb69631a4e97a4de259ea4269238261f6f694b59d8c263ff48bf78c3bc7024d7117997fc1c03fb3a3fc3793e107ed81e04d02ca2f13fc30d526f0678975b90c2d25b786fc472c96be1859ac669233a841a6789b55bcb7d35a32f3e9b27896f19e396ce42d69f9e1f037fe0a41fb7a7ed36da6fec9bf0875df0dfc3ed77e34dde9fe19f1cfc5cf86fe0db8d0be2abf86a5d4366b3ae6afe31b5d42e974ab7d22c6f6f6e2e754f0ed8787f5d9b738b7d5a3d5aee2b87e2e26f0230be24e3b87f895e2f2fc2e1b2b552867b46b622b51a98cc2e15fb6a54f9a9d2ab1718bf79d46e359e1b153a77b52a4e3f59c35e3963fc3ccbf3be18a74f32af5f31a90c4645ec2842a61f0989c5a851a95a3295584e526af154945d2588c2d39ca57ab591fdac69bfb407c3cfd9caff40f815a1f80bc69e2ebdd1b40b4826d3fe1ee9da45ee99e09d26d23b7b0d1348d665bdd5f4c4d3ef2e6c2037b6d611f9935be9cb6d7374902dfd835d7d7ff000f7e26e9df1234efb768fa56b7a2c919c5d5a788aca1b5bcb707a6d5b4bbbeb7932785c5c03839280715f2b7ecc3fb3df863e117c35f0bf803c356578ba7689a369f652ea1ab0379e24f10de450aaea1e21f146ab701aef53d775abd371aa6ab7f7524b737da85ddcde5d4b2dc4f24adf6c787f42b3d0eccc36b0a44ce4991d1402d939009c01c139e87a904d766794329c32953a2a73c647d9c54e3270a568a515fb8bca34e9c29a518538dda6a3cd39352bfc9c6bcebfbf523cb52a37527294dd49ce727cd27293b26e4db6e5bbddddbb9a125fc692981cee2ce235e8093b54c84e7a052ea0e3bb00339afe62ffe0e13ff008274f807e2a7c18f127edb1e02d2adfc3ff1a7e15689a36a3f1116c2d76c7f133e1c473c5a55ccdac456ebb0f89fc05692c3aa59788264592ebc2561a9e8ba94d769a7787574cfe8e2eefcdc6ab75042ff00bc822923836e087b921ee5f2491c6e30479c8c6c3f36391e69f197c1da3fc5af04fc41f855e22892efc37e38f006bfe06d6212b959b4ff0010691a8e917e15994e3f717324713e082e04a0ed214fcde69c35473bcbf1196e26116b138295484a57bd0adc89e1b1107bc650ab2a73567ef25520ef194a27e95e10789b9e7849e22f0bf1ce4b8dc4e11e579ae0ff00b568e1a6d4335c82ae268ace728c5536fd9e230f8ec12a90e49c5fb2aea862a97b3c4e1e8d5a7fe4ecc8727038cf4cf4f63939cfe7f5a2ba3f19f85b55f0478bfc51e0cd762fb36b7e12f116b7e18d62020830eaba06a775a4ea1163e6c795776934646e382a41248cd15fc635294a9d49d39fbb384e509c649734671972ca2efadd495add36ebaffd4561b134b13428622854a756857a54eb51ab077855a356309d3a9069d9c27097345ad1c5dd1fb77ff06f67c399fc4dfb5d78e7c7f25b34b61f0d7e0feacab70632e906bbe30d7746d374b4129f9239a7d2ac7c46635c8778a1b8d990b26cfeaf3c53a85dd9c9a97d859a2bd8afa1709211e4dd585f5bcd6ba958dd472148a5b49cc76a1d1dc624f2ca618061f981ff000419f8091fc24fd8e87c51d534e68bc4df1efc597de2d96696068ee4f823c3dbbc37e0fb3909d8c2dde6b6f10f88ec997779b6de238e5cf94e95fa77f1bfc3905f5b5f5bed9469fade9b756ed28731949022cd1a160431d93470ca46fdae8ac920dacca7fb0fc37cae794706e54a71b6231329e63555acd2c76b42edabae5a3f57bdd73465cc9aba48ff009dafa63f1d61b8fbe90fc6b88c1d4f6997f0e4f09c1b83aa9a94672e1ba6b0f99ca2e378ba6f399e6bec5a6d4e9724d3bcac759fb287c515f0d49736a6ef7f87a3f17eaba2ddc6b389e0b55bc5b5d41255752ea3c8b9bb911fe6202c801c12057e9c3d9c371247a9e9b3432453a876313a98e547c306564ca904609072793f4afe6cfe0e7874da5978e3c2da66a11e806ebc49fdb8d2a5b4d7260d4a54d361be9f4f48afecd2d27bafec154b87db2a4e26bb49221e73495fa57f0abc353cba3d95b6b9e3093577b7b996ec3de787eca4925592de0b68ed6e592ee3df696eb034b1dbae17cd9e5794cc3604fa2942ae26b5a92e5a918f2cb583854a6dfbb0716e2d3868a2faa7bed6fe5cc6518c2a49ddb8f46d59bf753e6b26d6bb59df55ea72dff0568f82be14f895fb167ed1c3c59144ba3693f07fc7be28ba791635fb34fe14f0eea1e25b3be80ba3a4777a76a1a55adf59b942d1dd5b4322825769fe683fe0d71f02fc3cf15fc75fda265beb985fe31f833e1ff84afbc3a9a879135a7fc207ae6bf7f6be2c96cadcafda1a44f11697e1386fee978821bcd3a34f2cdc4cb37f401ff058ef8936ff000cbfe097dfb4c9826dedaaf8453c2907f67c22d2248be2478d34cf0d5dda595b492dd7d9ac2d6d3c49716f6f6c66904367145023ed50abfc437fc119bf68dd63f677ff00828dfc26f15daea771a768ff0010ae2d7e0ef8942308229745f1d5f45a3da7da19dd15e1d33c4d75a06beee58248fa5ed524e00fd5f863078fafc3198d0a557d962b112a985a3295e515ec70cabc13a7cd6fdf4952a2e5ade2ddd3499f9fe7588a54339cbaa55845c30f4e3566e4968ab57f60e5cceed2a30752a25aa8cb5566d33fd492d2086c6ddde584584caaa2650fbe0620e736f33e0946e484dc181cab201b49d0bbd461b5d3a5bb63b55627652c40e4292a719c60edce0f5ea2bc46cb5fd43ec305b6a971717f1c8488a6bb8edcc3399609d7ca903cd170ac51764d149e6062030206efcf4f887fb6cf89fe1adf78b6c17e127816fb43d37c6de28f0d7872daefe2af8a34ef176b83c2fe20d4b44beb84d034bf839e20d3ed20f374d7b8692ebc413e99656b7164352d52d25b9083f0ee24ccf22e14860f17c539cd0cb2866199d3cb30556a53c4568e3319530f89c5ac25154a156aaabf54c162abb7522e10a542a73d44e294ff0040cbb2ec766b5d6172cc1d6c6625c1ce342843da54708db9a6a2b5b4535cf6568a7cced14dafad7e23fc42d43c09e19f16f8b609e08a6d1ec8de5bb4968f778d42feee0b5b78a5856457990cb7091b40ad17ca4af2d815bfe01f1b5cf8f342d23c5773671e9d71a9e9fa6cf3d944eed0432bc24ceb1193e758642fe6448fb9a349155d9ce49fc8cf19fed77e20f8d7a0dee8579f0fb48f04e97797f05a6a56b178ff0058d575c86eeda78b50b24fecd9fe1e787b4ed52d2eee218506a9a6ebf73a7c6f0ddda22cd736b790c1fa97f08ad3ecfe08d2ed579105a5ac4a3a0220b78e256eff00f3c988c64fcc013d6becf25cf320e26c02cdb21c552c76094eae0562a8d3ad083ab8774d57847dbd1a2ea28cda5ed20a54deaa136f998b31cb319954d60f31c2d5c26369d45cf46bc792a429b84671bc6ed72cd494e32da5171947dd9272fe053fe0b43f048fc10ff828cfed07a75b5a1b4d13e22eb563f1974131dbb430dcdbfc4bb24d775d9e00c02ba47e346f1459bbc5fbaf3ed66450ac8c8a57f725f1dff61ffd9cff0069ff0016695e3ff8b5e0fb3f10789b49f0c59783ed6f65740f168da6ea9acead6d6a4952c44779aeea12024e71363a01457e079d78398fc7e6f996370998e0a861f198dc462a9519d2aee549622afb570f7538f2c6551a8d9e91b2e88ff5a3c34fda2dc11c2fe1f705f0cf12f0bf136639ef0ef0d64f91e658fc24f08e86371194e0a8603eb71756ac66e588861e156af3455eacea5b468f22f07e81a57c14f867e03f86de1ebaf2341f879e0bf0c7837479e38edd6e069fe1dd16cf47b2925b295822bc905ac52c9b4ca81da539240c79878f7e23789ae747974c17b71a8dacf968e07d22059e300952d1ea0b0a3c2b925085ccac8c577956cd7f253e1fff0082e67edb1a65a4367e2f6f853f12d618560375e2cf0349657b32ac423cc87c1fad78634ddd81b9a41a589012c4380483fa03e09ff828d7c70d7bc2f71aceb9f093c137b1390636d0b54f1268e63568c3175b7ba1e206080300409c29f552457d143c5ee08a7428d2a98cc665d4fd9c284235b0388972f2a4a30e5c22c4a6ac92ba728ab6b65a9fc81e20fd133c66e09cc218be21a3916775f39c563f131c6e579dd1af1c656a7568d6c5e22a7f6950cb2bd3954a98ca73fded384ea4a72b46f176fd00f06f8cb50f0efc6dd112eac653a4f8b5afb49d420993e4b7ba82ce6d42d35056246d659ad85a825482d7cea482403fa83f08bc552aeb8da5dcdbb88274636b74096843a80cb1c8c388d986402d85ce3e6e467f047f647f8ebe2dfda3fe347c49b4d5b44d0344b4f00f823c37a969167a70bfb8d4e5d53c41ad6a526a535eea37972d1cd15a5bf87f4e86d63b7d36d2487ed374d33ca92c4917ee6fc269430b4ba6017cd8e06da189dab820b3124ed20ae1c1c6d61f37cd5f6d9166785cd69e0731cbeb4ab60f17efd1ab3a72a6e74d4dd36e50a9185449f27bbcca325a3e5499fcf7c519363322c7e232acca9468e3b071a74b134a9d58568c273a71ad05ed29b9539b51a8949c2528a7169376b9f007fc17d35e66ff008276fc5fd1ecee199ae75bf85c2e369ddf2afc53f064c55406c8e63500e781927d47f04de13bdd4bc25e21d3bc4167732dbea36335a6a16577191e75bdd595d45716f2c2e4b88cc13dbc72264e77aee04f02bfbf9ff82da782a2d73fe09d3fb40eb9685fed7a358f82f55b60a72cd2d87c46f084f3c841cf11c11ccc31d8138f978ff3e06d4af43c4f7c25529b950b90cae339e0c790307aee19e4f3806bfa0f83aa428e0ea4a51973433172524af08c7d8617e2d6c9e9a27ba7657d9fe15c594a73c641271719606316a4ed27fbeaeda5d5a49df4bd9db6b1feab7f0b7c7b27c54f869f0ebe27e9eb730e99e39f04f857c53628d711cd08b4f11e8565aba4c2599a0c18a3b90618a321e64521c31015ff001d7e3b78cfc53e1ef1d59f8afc1fa14de3ef11e81e3af8e825d06d352bbd3efafb44ff0085c7ad5d5d2e9f7d69740aeb33c5616dae696b793369f7d2693636d79bede60e3e9aff00825a7c5f87e27ffc13cff6621a65b378875fd03e10685e163a7a5cd90bb91bc073def823cf57bd745927b7fec08a79103acf87de92955735f15fc58fd857c59e24f8bde31f1949f143c4be12f107886f659974a87c2babdd5cc56f34f25d2db9bab6f884a5670f9b869748874ad36e2e65bebf8f4a81f54d47cdfe5ff1c382b37e2badc2943259568ffab1c613cfa5528bcb9273c1e0f1b95c70b523997351953c4e1333c7c7da53a3889d1ad1c3cfd93836d7ec7c0d9c51cbab3c663255230c5657530f5614e359caa53c661d73d392c3d6c355f6351b8c2ba8e228ba987955846a424d4e3bbe12d6fc41e33d6bc13aff008b34887c33ad6adae6b7a86abe181a8dc6a77ba3d9f85edf46b6d234cd52ea52e973ab81aab36ac74e93fb262bd6b88f4b8bfb3e3b39a5fdd7f83df10bc2975e08d2e1fed9b66d482ac577a62acc6fada54c45b67468d000e46f12a6f8ca10c1bae3f9bcf8e3ad7817fe09dbe06f855aeeaba478dbe20cfe32f88dafda78b2eb4e3a2585fe95a8dd784f4b9aceeb4bf0fdcc8ccc9abe9be14105c5adff008a5019b485b9b64492eeefccc5b9ff0082b97876cf40b9d43e1a7c0ef89fa83dbe99777324be30d4743f03e9e5e2b5b895bf79a4c9e33bdb88f72c68f1c70d8b4a08c5cc65775564d9fe4dc07c3f97e4fc499a617099ad39e638ecc68d4519622be2f37cdb1b9a55ae960e82a556a579e2bda4d61e9ca309c9d3e69385dfd961f81f8a78f7110c6f0de4d8bc7e131189784c0d5f6d42961f0f468ca384c3e16ad6c5e2a4b0f470b42342852788af754a9c1b9ce2aeff00ac143381be07821866ccd1ac922ef2afcef3b0b2fcc4123079ebde8afe0defbfe0e06fdbaa3ba953c1f1fc2ef09787c31fb0689ff08e6b3aebdac793febb53d5fc4b2dc5ccc46d5768a3b4b5f90186ce125cb95cd2f19b82d49ae6cce5676e68e01b8bb38ea9caac2567adb9a3176dd2f791fd3347f676fd212ad2a7526f81b0f2a908ce542b712e21d6a2e49374ea3c3e535e839c2ed4bd956ab4db8be4a924d37f8372c81886ce383807827db1d393919072323eb5fd1afecbd69a3f89be1ee9b022ac9f6cd26ca78f6aa9de27b489c13b8e46e04374eb938c934515fc539feb86c34faac45976d613fc7dd563fd1dfa4846d96f0ed74daa90a999a4d5ad69d2cba6d356ef4616f272bdee9afa7ff637d120f861fb618898795a7fc4ef036aba04d19c79726b1e1dba8f59b13f3283e6369936b2e002062ddb39f9b1fbabe08b29fc2fe369e1bbb863e1ed4a133dbaf55b590482495977290a85de47ea33bd83162ab4515fd69e0ce2eb62f81f27a95a5cd3c263b1187a32574fd97b794b96767ef25ed1a5b7baa2b7573fc74f1ce852a3c6d8d9d38d9e3f2cc062b10b74eb3a2a0e705f65b5460deafde72e92b1f2f7fc15c7c6fe10d67f60cfda6fc23a76a7672dd9f855e20bcb7844f11925bdd1614d6a254f9c0f3165b040a09072c1860835fe7291995f025562eef92a593e5192a40fdebed27af0a3b3723068a2bfabb842a4ead2c529bf7562a8a515f0de74d733b6bafbb1f2d1687f29f154553ad8671de586949c9dafa556924edb2bb76efadcfeec3fe0863e05f1578aff00e09ebf0dbc65e04d60ff006ef83bc6ff0013bc357fa679c1678a6b4f175f6b76d0a90c4edb8d2b5db193cb7f91fcf23391cfed44905f7c40b482cb51f0fcda5fc4bd0c2dacb0dcefb017964e1825f2e76adf5bc122032450990b33f2563dcc0a2bc1cd69c68e6f8e54eea357135dca0dde0a57e6e68ae92bbdefb5bb5cf7b29ab3ab95e11cda72a785a4e33b5a4d2518f2c9af8a2d6f74ddfaeaeffcf1ff00c140fc1be2cf137ed59a67c17f125ec3ad68fe07d1342f89170163ceed67c6ababe95a7473a8ddb25d334ed2755f2e304096db5a85bcb8d52267f39f8ede08f0cf813e0cf882edeced2de4d3fc2faadccf22224631069d712b924a0230b83b43152548e4d1457f9ede2e626b623c49ce61566e50c1e330b85c3c1eb1a74a14e83495db6e4e5294e526ef2949bea7fa53e0e61a9e1b853c3dc2504e9d2cce381c6e35c2d19d6c4637114a55a4dc52564a7c94d72be48460b5e5bbfe5f81181c8e83b8ff0027ebde8a28af015df57bf7f38afd59fed2ad97a23fffd9, 'Malang', '66119', '', '', '', '', NULL, '', '', '', 0, 0, NULL, NULL, NULL, NULL, '', 0, 0, '', '', '', '', '', '', 0, NULL, NULL, 0, '92775', '91561', '81238', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 15694, 0),
(223, '15837', '15837', 'AMRI` TAQWIMBO ZAMZAMI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93776', '97290', '60187', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 31150, 0),
(113, '15838', '15838', 'AN NISA NUR LAILA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '82827', '24610', '96729', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 50207, 0),
(299, '15839', '15839', 'ANAK AGUNG GEDE P', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15745', '97679', '74898', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 77, 0),
(186, '15840', '15840', 'ANDHINI PURBOSARI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63417', '45187', '22508', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 34569, 0),
(78, '15841', '15841', 'ANDIENA ELSAFIRA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '18060', '31019', '16661', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 50220, 0),
(114, '15842', '15842', 'ANDINA PUTRI LESTARI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '46936', '51011', '23099', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 1870, 0),
(149, '15843', '15843', 'ANDITYA KAHYUNA CHOSUN', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1990-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66119', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67385', '84708', '34155', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 9272, 0),
(259, '15844', '15844', 'ANIETYA DEWI A.', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60256', '57170', '13107', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 16423, 0),
(150, '15845', '15845', 'ANINDYA WAHYU KURNIAWATI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66120', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33870', '64244', '92378', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 33760, 0),
(260, '15846', '15846', 'ANITA CHANISYA RAHMAH', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41266', '89777', '56347', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 7411, 0),
(224, '15847', '15847', 'ANNISA FIRDAUZI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94652', '61733', '13256', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 24374, 0),
(151, '15848', '15848', 'ANNISA NURIL FADHILA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67998', '84234', '62726', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 25846, 0),
(261, '15849', '15849', 'ANNISAA AHMADA ATUSTA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45640', '96373', '10911', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 18362, 0),
(152, '15850', '15850', 'ANUGRAH PAMMASE', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83829', '98325', '89036', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 42121, 0),
(300, '15851', '15851', 'APRILIA WIDIA ANDINI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '92350', '69450', '25641', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 41813, 0),
(115, '15852', '15852', 'ARIF TRI KURNIAWAN', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '90993', '50792', '69655', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 39029, 0),
(153, '15853', '15853', 'ARIQ NANDIWARDHANA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '87615', '56813', '71227', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 50646, 0),
(262, '15854', '15854', 'ARRAKHAF JULIANSYAH', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45650', '96803', '25694', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 23769, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(42, '15855', '15855', 'ARYA PRABA P', '', 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1991-03-14', 'WNI', 1, 1, '', '0.0', '0.0', '', 0xffd8ffe000104a46494600010100000100010000fffe003c43524541544f523a2067642d6a7065672076312e3020287573696e6720494a47204a50454720763632292c207175616c697479203d203130300affdb00430001010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffdb00430101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101ffc00011080078005a03012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00fe7d89e71fd091edfe7f51dce4f7f6e87ebea3b7ebdfb500e7d3f039f5ff000fc71ed487b723fefa23f975aff3bcff00b29b5d5dbbdb64fa6caebb6e2fe7cfd7bff2faf18f6a8a79e1b78659e796382186379a59a6711450c51a96925964721238e35059d9c8555059880334e27033d7e8e4fb57e77fed5ff192f6ef549be1af87af1e0d2f4ef2bfe1269ade4c1d475165f34696f229f9ecf4e1e534f1862b35f334732ffa1a06fa8e10e16c67176714b2bc2c951a4a2ebe371728f3c70b8584a319d4e4bc79ea4a528d3a54f9973d49c79a518294a3f83fd227c79e1bfa3bf86f8fe3bcfa84b32c64ebd3cab86b87e8d68e1f119f67d89a556a61b06abca15161b09468d0af8ccc31ae954fab60f0f51d2a588c54f0f86afe8ff127f6bcf0fe8134fa5f812c62f13dfc4d24526ad7cd35b687148876836f14662bdd493767f7aaf6303a859609a789813f31ea3fb567c66bdb8335b6bba7697112596d2c342d29a04ce7e50d7f6f7d72c3de4b8663dc9c035f389249c9393ef495fd6193f873c219361e146193e131d5525ed31599d1a78eaf565649c9aaf0952a57b7c14695382ecdddbff9f9f11be9a9f48ff11b37af98d7f1273ee12c0caab960b21e04c7e2f8532bc05152e68508cf2caf4b32c772e8dd6cd730c76226f4757914211faefc1ffb6078fb49966ff84b2d6c3c5b6d21531af976fa25d5bb2904f973d859bc1246c01dc935a3b863b925504a9fac3e167ed23e0ff8997c9a234171e1bf104c4fd8f4dbe9d2e60d40842cc9657d1c70ab5c2804fd9a686091c7fc7bf9e564d9f92956acaf6eb4ebbb6beb19e5b5bbb49e2b9b6b881da29a09e1712433452290c924722aba3290558020d79d9ff857c259c50c43c365f4b29c7d4a76a18bc073d1a54aa4636839e0a135849d36d2555468c2a4e2db8d48ced35f61e12fd3ebe909e1c66793d3cf38bf1be21709e1318a79a643c58b0f996638ec156abcd8b850e28af427c434717084a72c054c46638ac261eac6942a612b61632c3cbf7ad4e46739e9ebc7031df91d7ebf853b1ee7f3fae7afafe631c1af28f829e3cff858bf0ef43f10dc491b6ac124d375c48ff8355b06f2a57640aaa86f2036fa82a28da91ddaaa92149af57c8f51f98afe45ccb0189cab1f8ccb7190e4c4e0711570d5e2af6f694a6e0dc5b4b9a12b734256b4a0e325a347fd15f05f15e4bc79c25c37c69c3f88face4bc539265b9ee595a4942a3c266785a58aa50ad0539aa588a2aa3a389a2db951c442a519da74e4831ee47f9f7fff00577eb463dcfe9fd45191ea3f314647a8fcc5711f4a945f5b7c3d5795f7edf9fdc1dfb9e9d31efebfe7f5a3d3827f2fd791fa5211f4edfc39f5f4fafe1f8d1c71919c7fb27f4e2816cdf4f95baa7b2dbb9cd78cfc470f84bc29e21f12ce159744d22fb5148db859a782090dac07041fdfdcf950e0329264c03938afc3dd42f2ef56d4af2feee47babed46ee6bbb999b2d25c5d5d4ad2cd2b772f2cb2331f52d5fa7ff00b5ef891348f85e9a324816efc4dacd8daa479c31b3d398ea5772e01e6349a0b285f3c1fb42820e4e3f37ecefadbc3d0473c314771acbb5a5cdb49346935bd9a8fb6194344fb9269195ad1e3dd9453b8952cb81fd6de00f0e517916619c62eaac1d1c6e3796b63274dd471c1e0a0e9d2a74209c5d5ad5b133c5429d3528294a9fbf24a9c9c7fc12fda9fe22d6cebc5de13f0f3095bdbe0781b8569e3f1386a756d1871071557fac62162209351952c9703925484e4a73852c4d4708a557967c99461c10410718ef9ce318eb9cf18addb0f0af89354b69ef34ed0f54beb5b6655b89ad6ca79d2067457412f948c63de8caca58004329070c33d559f8f6ce0b668aebc21e1dbcba25645d41ada786e52e63548d27fdd5c0427686696265314b2c8d232026bbef02783ff690f8dba96a3a8fc22f007c50f88171a3259e99a88f86de0ef11788d746b7d49aea4d3ad7515f0ce99789630dc8b4b84b237822565b431c2db6dd553fa1e8e5bc2d4a309e2739cc3319d56ad83ca32e74abd18b8cb9a75eb63daa2e54e5c96a7878d65512a89d6a294672ff002d5cb33ad374f0d84a51925753ab5255233b38b6a14a82954b4a3cf79cdc5d37cafd9d44da5e2f1f853c4d2c0d751e83ab35ba3b46d30b0b911ef4547750c6300b22491bb85cec47466c075272ef2c2f34f9e4b6beb59ed27898a490dc46f1488c002432380c0e195b91d194f4619f4ff12df7c648adee752f12c7e36b1b4d3b559740d42f751b2d56c2dedf5e8c4d2dc693a8dc4d0431ff006ca471c9e7da5db9be1042892279504691f11278a75b9ed85a5e5d3dec1f6a6bc6176a2691ae0dbada16f3a4065c0b6558826ed88b8d8aa466b93154b85d73d3c256cfa9545674eae370d81709ae57753a146ba9d2bceca3355ab2518b6e2dcad0ba7fda1171fac53c2c6ff1c29ceba70bbd1c65529479ed1def0a777dadafd89fb15f8a258b57f16f83a5947d9af6c2df5fb48d89f96eec27834fbaf2c72034f6f7d0349d0b2d9a1e76d7e87f3fed76ebb475fc3f4ebd2bf24bf664d58587c67f0b1402286fe2d534c9d7e6fde9b9d22ebcb0719fbd73142e71801807201e9fad6a73d80e57a027ff00d5f5ebd7dabf8abc6acb69e038c9d7a5671ccf2dc2e2e538a718cea53956c14a514d27ac70b4e52babb936dead9ff453fb33f8bb11c47f46e864f8aad2ab538238d38838770ea73e79c32ec553cbf8930d1bbbb54e35f3dc652a71bb518d27185a2a292f3ea7a1eebd8e3d3f3f4f7a5e7fdaff00c7293b74ec7f83df8ffeb0fc4d2edff77fef9ffebd7e447fa14937b7f5adbf51d4d6e327affc088fd07b73f4e69d4d6e87f1f4f4ff003ef9f6a0a974f2f26b4d2dbdcfce7fdb33595b8f17f8574391c98b4cf0fdc6a0c10aff00aed5af648b66d39f9bcbd2a3ce71c119238af9d7e19fc34f197c65f889e1cf00f81741d4bc47e21d6afb4db0b4d374cb637170e92de5ad889366258d15a4b98106f0c925c4d0dbc6af2cf142fec1fb5dc0f1fc4f170db9927d07459621202caa8dfda703282318025b6964519c6f766c7241fd67ff00836cbc3de18f107fc142ae6e35db0b6bfbfd07e11f88b53d1d6e516e22b5b96d7fc2fa75cdda46e0a19c477b1db248cbb638eea51bb2cb9fef9f0eb09430fc03c2b83a32e48e270183c64e492f7eae2a756b554d36eee9e22ad651d168e6f6765ff2f3f4aaaf89e28fa59f8b91cd1ce9b5c758dca1394af2597e4986c365981e46eea2aae5d97e19d35af2a9a4eed59fd1df027fe0803f1baf3c63f0725f897f09746f127c2ef1b78b35cd1fe26dc5c45e21f0f6a9f0a7c0ba1dfadd69be2af126b1a0788ad66bed6fc55a4dd5eae8da77c3dd1bc55756979a65b68de27d3564beb2d6e2fed93fe09f7fb38fecf9fb26fc17b3f87ff04fc01a47c3eb1b7f0fe8fa96b325e68f6ba7f8d358960d3fecd0af8df5396c6c755d735eb00bf62babbd593ed2f340cf28f359c9fb1a3f054773a4c72dc8b25b186dc4f3482d6dcdd6c51b9809c5bef8c7192c87728188ca9f987c6de1bd0bc79e34d5bc75af699a56a967e1df3e7b2f0f0957ec06eb4db4674b6d4d6d9b064b4bbf29e681e5236c2ca9e5ac8093fa34a9e272da942ad1c3c6ad695174e72568fb470a716ea34b9945caee4d7573695a9c6305f1393e4393558e674f0d89a785847114ea3954f6525868d4aaa11c2d3c4ce953c4558cacda855ab5793914a366e6dfc07ff0522f03deeada069f75f0c7f611f867fb606af67f137419ae3c15e3bb110f85f4ad53c5761acc31f8e25d25bc31a9e93af69fa38fb3d878a6e6e2f2d174bb6f1045797332d869fabcd6dfc9a7fc1657f61af8a16ba95e7c5ff04fec77e01f82be18f86fff0009269ff11b51f813e0ebff000c7c32d5741d3aeb4e87c39e37b18759bed1efc5bdfe9eee4e9a7c03a1eaba68b5d4e6d577db5bc53bff00a287c10b21e2af0e2ddea1a2cba7f89342bd7d3358d3757b58d6559a3861960b886552498ae6da68658a5564756df04884461cfe5f7fc1773c43a1f807fe09e5fb505fea16b6703ea1f0afc45e1fb70d146a8da9f8a203e1cd2d460292e750d52dc27cdb8b6d19218a9e5c6e171588c07d76a55f79414e6aa253728d9f2c209dbd8a8c9abf2eb78f2d92948cb32cb32daf5f36cb7115656a383972f27d569bc1d4c24635dd784bd83ad59558d269aa95e74dc2acb93550947fcbe7e0fea0da57c4df015f60144f1668f0c832a1825cddc36b21218fddf2e76c9200201e7838fda94395fa1ffebe7f5afc2bd1af069b77a4ea31e12e2c75a86e838c6e65b792d258fdc149118f3804be547ca48fdd38983c6922f01d438fa30054fe231fd79afe50f1ff0008a962f85b177e675f018ea2dee92a153095a31f2b3c5cf47ae8decd1fea4fec97ce7dbf0cf8cfc3f27a65f9f709671083ef9be5f9ce0aa492b6b7592514efa2d2dd592607a7f93d7f3a28a2bf9e4ff5e95924fabe5d93f2bebb6be5f8b0a4ff001f4f6ff3cfe14b49fe3ebedfe78fc68265f2fbdbe8bbfe7d7e47e707eda96661f18785efd7205df869adc8190a4d86ab77267b862a351039e406ce40e2bed1ff008201fc4fd3be1a7fc148be1c45aa5ec5636de3ff0008f8cfc0b14b348628e4bd9edacbc4d636fbb705f32e6e3c2e9042ae184b33a42a03caaebf2b7edbb6a027c3fbd1c123c436acdd720368f322fa8ce6439e840af89bc15e31f127c3bf17f867c77e0fd56eb42f15783f5dd2fc49e1ed62cd825d69bac68d7b0dfe9f790960ca5a1b9823728ead1caa1a2951e37753fdb7e17e2e557817866b394a52c3d2c4d149bba8ac3e638b84524de9eec534b45abb5cff00999fa6ce1970d7d2f7c51aaa9fb8f3ce1fce1c124bda4734e16c8730c424f5d673c4d68ddb6d37ab6d1fed07a87c69d3bc1fe009b5fbdb37b8d26ced2d12e6e8473c9040b3ab7da2e6f9a185fecd656a8a1e7bd958dbdbc45e6b992dade0794f86685fb447c009ed354ba9fc45e1eb77be823b631c13decf0a595b4737956d2cb0ef8ae74cb769d922b57335ac1e76c4b7489dc8fc76ff008247ff00c15fbe1a7edd3f0bec3c01e24bad23c37f1cb48d16cf4ef881f0eef268a0fedb104715b5e789fc210dcc864d5340bf39b8b88e0f3eebc3f3dc2586a980f637da87eeddb7ecf3f0725d261974ff000ce9f6b1485ee45a5986b68e392650d29805ac91c68ce79f34ab10cc5f96191faee171b8cc5737255a6d413e5f6d4d6da2f77e172765fccaced74f53e0322adc17c9531399c73a7431d529d4a15b25a98794a725efce3898e2149d39c2a2b72ae577728cd45a4617c1cfda37c2be27d6effc3fe0bb27f1058422e0ea9e22d1ad6fa6b1b0bb86d2492dc6ab79340b1412dc4302416b03c8ac5446b6f19b78894fe71ffe0e9efda2bc3da07ec6fa7fc2b1a8c63c4df18bc75e1bd234fd28cabf6a7d1fc27a8dbf8c35fd4fc92448d67673e9ba358dc4ab954b9d5ac91fef807f773e2b7c4ff85bfb29f84f5efec7b08749d38e9f79aa9d2742b657d4b55bab7b66b9b9768be562eb1c61ee2f6ea48e1897325e5c451aee5ff2ebff00828efede7f11bfe0a09fb456bdf173c64b2e8de18d2daebc3ff0cfc10b746ead7c25e118aeda5892598044bbd7759942ea3e20d455144f74d0d9c18d3b4ed3e287cac46675ead3a9849d68d4492ba828da3692b47992bb568dd736b67f7fcf71862f29cae18fc465786c552fed7855c1e0678c929626587f670a789ab89708aa2a6a8d5e58c69ab29d4872dd466cf826224fdd5c900639c7258364fe4063b8cfad7eef689279ba36952e73e669d6526718cf996d13e48f7dd9fc6bf075491c0f50723af1f4ff003d6bf74fc1eed2784fc31237de7f0fe8ce4939c96d3ad989fc4935fce3e3e28cf03c2d34db953ad9a4257b6f38609a4b4bd946115abb745b1fe8d7ec95afcbc41e36e1ba55ca381abad5a57a18de27a7bf77f59b25d526747451457f351fed6f45b7d9fb4d76e9d3fa614d2c07538c9fe9fe3f8e7daaf69ba76a1acea363a46916377aa6aba95d4163a7e9d616f2dddededddcc82282dad6da0479a79e69195238a2467766000c9afde8fd88ff00e088fe35f8b52278a7f6aa1aefc35f0334562d0783342d62c60f891e2637d8616b6a2c17541e16b160f15adeeafa94906b4bff001318347d121dfa578ba0fbfe02f0d78a7c44c74f0d90e0d2c1e1a708e639be29ce965b97c669b8fb6ad18549d4ad28a6e186c3d3ad889af7bd9aa6a538fe05e3efd24fc2dfa3af0f433ae3fce26b31c6d2af3c8385b2b8d3c5f11e7f56872c66b0383752953a185a75251857cc71d5b0d80a526a9fb79579d3a33fe45bf6d8d534f9ec7c17a64372b2dfda5feb12dd41182ff6647b6d382c77122e5229d83c522dbb113792e933208de367fcff0055323aaf52cc178ebc9c74afe877fe0e45d27e177c13fdb4fc3bfb1cfc0bf09681e06f857fb3afc1ff008796ebe1dd0d2e659078d7c71a7df78e35ed5b5dd56f67b9bbd73c47a969de23d10eb3ae5ddc5cdf6a72416cf7f752dcc2cb17e02f82e16b8f17f85e1488ced2f883464102aee336751b6cc417726778f9705d01ce0b2e723fb2787783b0bc294328e12c1e6553368d0786a7571d2c2bc37b4c5e6152389c4c2961d55aad52a15f133a349baae55230536d393b7fcd578f3e33e2fc7bf13f8afc57c6e454786df104f091c364d47193c77d532ec972dc2e4f97aaf8d9d1a1f58c556c1e5f46b62eac30f4697b7a95152a30a6a28feb27fe0decfd917e1d5f7c47f8cf63f1174b94fc5bf057896df5df877aec3aa5cdb41ac7833c31e24f17fc39d57c4be124d3ee90df43a378e741d6b4dd726759c59aeabe1db89a102eb4b96e3fb92d2fc29a8697a1c4b71ad6b32c71db8c5c5b6ad7c172aa14e63566fde1036fcb1139385e0003f24ffe09bbfb10d8fc39f825e33f0bbd921f8e7fb3b7c71d67e28f85b5db65116bda6681f16bc27a2586b1e116ba819c5e68be38d3bc0d7bafdde937324d6f79ac5d593ba4788675fdd3d0ac63d6345b0beb4951d6f6d2299d4a8c66440cdb914e030276b6303823935f779ae48b2ccdb31c161b9aae1e8626bd2a738557ac6153dc86ad392e5b38bd79a328caf791e3f08e73471bc398094674a9ce9c5c2bd3f67671a93b54551a8eb79a934e4fed41a69348fc3fff008283f8c7e0efc34f809f177c51e25f1af85a1bf8fc17e24b8306a7aac1abeab7bf63b2b822c2689b568ef9ed9ae2448dad8cf6b6f3ccc2d9e6ccc237ff002dfbb94cf757131c6669a493e54118f9dcb71182c1073c202768e32715feab3ff0563f057c0cf0b7c17f889e33f167c16f86be2df12f87be1d78b7c5abafeb7f0dfc31e29f1036a3a0e91733685a6595d5e68b7fa9f9d7fa92da5b3c9148be55999ce5503b0ff2a299da4964918e59dd998e02e589c924280a0e739c0033d001c57cc62f06f0aa9b74952756551db76f95c5cb99f57792eafab7d0e5e34c5cb113cb62ea7b48468d6a905cce5a5474a3cc93b28a9ba4d2493bf2ddbd6d1e93c17e11f10f8f7c5de1bf05784b49bfd7fc4de2ad674fd0742d1b4bb59af350d4b54d4ee63b4b4b3b4b581249679a596555544527a938009afdd29fc3b7fe0d90785356d2b52d1351f0fc70e9575a46af6771a7ea7a7b59c31c2b6f7b6579141736d3a22aef8e6863704e4a80457cd7ff00042dd42d74ff00f82b3fec426f2cad7508351f8ba343f22ee04b88966d7fc2fe22d1adae5637575f3ed2e6f62b9b77da4c73448e36950ebfe9f3fb527fc12dff00654fdaff00c2bab69de34f879a7e8df10ec2092d349f88fe169eef43f17e8f2167f2a5b5b8b76b8d32f21694c8f2699ae689ade95206775b3b79dd2ea2f88e3ff0ca7c77c3982c760b388e0732cb7198fa786cbebd0e6c0e2d4a8e5cdcb138a8cfdb612717351a55a347154db9ca1521462dd55fd3bf428fa58f0d7d1978c38a30fc5bc298dcdf21e38a1c3d87ccb8832ac4d379af0f51caabe6ee9ce86575e11a399e16acf1f2a98da31c660f1118e1e94e83c44e0b0f3ff398a2bee9fdb8bfe09f9f1cbf614f1d4fa0fc46d264d4bc0fa9eaf7b63e09f887630aae95e21b78545cdb477b04735c9d175996c5d6e1f4cb99e449bcabd6d3ae6f63b1bc36ff000ae47a8fcc57f17e7791e6bc3b9962328ceb0557038fc2b8aab42af2bbc67153a55a95483952af42b53946a51af4673a55a9ca33a73945a67fd1670471d7097891c2f95718f03e7b82e23e1ace28aad80cd32fa8a74aa724fd9d6a3569cd42be171786ad1950c5e0f154e962b098884e8622953a909417f529ff0006f6fec29a0f8de7f13fed61f1234282f24b59354f0a7c1b8f55b41716b6b7564896fe25f185b432a8496e3cf9db40d36e864db2d9ebcab93323c7fd68e85f0dbc3ba3082f9ecd24d5a1fb45ca4aeaa0a5c195c19210aaac570d16d8d8bc658248177e0d7c21ff00049eff0084467fd8f3e1d683e12d3e3d12efe180bdf869e20d204d6f2dd59788fc2a2dd67bebf96d259ece6b8f16585cda78be69acee2e2d6e21f12c735b5c4d1b2cc7f4c0b99350b5567d934114e258783e6c370223e6a8e8e239618d1f19f2d9c838592377ff0043786f239701e4381e13c0d6f64f098584b389e1db4b1d9bd4a5ed31f5e7522a32ab4aa569aa145cde985a5420928c1457fcaa7d23fc66cc3c7af1738b7c44c656c4cf2cc763aa607853058a6dff006570a602ace964982851bba787a93c32faf6369d3b4659962f1955b93a8dbff2caff0083b3bc1ba6f847fe0ad1aa4ba6c4f1ff00c255fb3e7c29f14ea0ed8c4fa95d6ade3ad2e79542f0018748b7461b57e7463839dedf86ff00b1f7c3dd5be207ed0bf0834fb28c0d397e24f8365d66f1904e963a4d86b36fabeb3732dba491dc4b15b689a6ea5753a42d1efb78a4479e1566917fa67ff83cb7c0b75a67edf9f027c7ab1b9b1f137ecc5a068eeea14c2977a17c48f892e9bdb606f3ae21d41d0124ab4762510fee5c57e1e7fc1317c2779aefc57d3524d2f53b7b3f1278bfc15e1ad2bc696476e9de19d5e7f16787bc3f7916bd344b2cda669fa9daf8eac6da0d46588dabea8fa7d83b29ba696dfeb38772e963f8cf2aa58a95650a91c263e55a2af28c6182a75e9547251925055a34e9395ad16f96569276fc3a75d51c9e728a83715530ea0de8dfb69516ad74fe1bcf96f771daf74dff00ac87c02f85f61e18f8c9f133c4f15ede9d4be25fc1ef809ae6a700b95feca92ff4093e237871a6834f89bc99244d2ecf478e49a42ff348c6075134a16efc42f0c789fe1ddfdadcf87e388683a9decd122a3b8b7b0b8999e66b70bd608594bbdbc7b888d124843c8225965ed3f66dd5a5f13f84fe1a789e7557b8d4be09f818cd37ca5e512db41a8c0eec8cc843adf4b2868f0a0b3637751ee9f10f4093c4be0cf11e8f68f043a8dee93770e917138fdc5aeb46073a45ccbc1223875016ef22a82c62deb83bc8319b57af4b37c453af3526a74e8ce51567154a31a3170692692842365add68eecf4386b37964f5e9ce104f0b524a389a125cd09d3959f334d5f9a9a6a716bde7671776f4fe77ff00e0b29acdcf837fe0977fb6dfc65bcb9fb25cafc33bff00867a2ea2c504cb278befb4ef085cdc69acf1bf952cfad78934ad262bc871346835386278e4984d17f94d2a3bac8caaede5856720642a124166ee0676807a75cf6aff00538ff839874b7f0cff00c11dbc5bf08b49bc96eb54b9f177c0eb0f13de5bb3431cb6abf12fc37ac6a37ba8ca408d0eb7af584735a5930792495848d12dac0f2a7f9aa6a5f09f56f0bfc0ed4fc75abc7069cba9f8fae7c276319b88e7babc7d1ecb4cbcb98425b4c61104675195fed015c48d6d708a70015e8ff57732cf70d1c452a7569e5b9764f9ce6b3c54a93f676c12c44dc5ce5c9153c4ac2e1e9c39a576ea2708cdda12bccb3ea18bccb17899558355f1f87c0e168a7ad3a6e34a14a0a3ab4ed395494629ae794af65792fd0eff00837a7c27078cbfe0b1ff00b0d697738f22cbe24f887c46e0a87fde7857e1af8dbc476bf29e4e6f34cb71b80ca7dfcaeddc3fd7ebec6f6dab5c5c441717f6bc93c6dba842ae58f20abc6a1f182c0a3819c85aff00330ff834dbf64bd67e247edc51fed2179a55d43a67c06951f4bd426468a0b893c5de09f18e87ac476cee04734d043af78664645dcdf63d49e552830c7fd3b5a305416504a12cbe80e0807af1c67d9b91dc67c2f655b0580cbe351b4b1b87c4e29d26ace30ab897878b6b469cd6029544a4b58a8cb58c91e6622a42a62ea38bbba5eca8377d2e97b5b7afef6d7efa2ea7c7ff00b5cfecafe01fda7ff674f8a3f05fc6fa6dbea09e2dd12edf4ad6ae605b8d4740f17c51f9ba1f8a74b9e506482ff45be5b79edd55846f6f13d94cb25adc4f149fe635e29f0ceb1e0df13f88fc21afd84d67aef8535ed5fc37ad5a3a481ad756d0b50b8d2f52b6605721a0bcb59a220f20a9aff58cbc6ff45b86ff009e36b3ceeb8ce64951c46a32083805873bbaa7a62bf1b7c75ff04a3fd94bc77e37f1978dfc43e13926d7fc65e2af10f8ab5c992489525d63c43abde6afa9ca8be436d592f6f27755dc70081938cd7e63e21f8675fc468e5b8ac1e2f0b82ccb2e9d7a55f13898caf5f075d52951a0e4a326e386ab4e75292f861f59acd25cceff00e82fd093e98597fd1b70dc6f9171761b39ce386b88aae5599e5381cb9d3a90cb73ac2aaf86cc7114e956a908d3598e0a7818e2251d672cbf0edaf89afb67f62ffd977c1ffb347833c41a7f876f2ef56d5fc617fa1df78935ebb492197518fc29e19d23c11e13b536f35ddfce3fb1bc25a0693a6f9d737b76fba27b5d33fb23c3369e1ff0ce83f5bea0e23d4f460612fe64d768251c341bad8bfcf820bc12f97b590e54cc2ddb198d48ade1b84c1a6c1f29dc502bae0aed743b1c0001fe343c71918f5ae1be35784356f1af84ad743d135cbdd035397c4fe11bd8efb4fd6bc45e1db89adb44f1468dafdf68cdae785753d2b5dd2a0f115869373e1dbad42c67b836769aadc5ccda5eb10452e937dfa9d7946b66559bda53ab14e57935fbb9c632dd36e3a34aeaf64b45a1fe7cd9c25ca925cb4e2a29746a2aeb4bf6ff0023f977ff0083b4ff00635d63e3c7ecc5f083e33f83f49b5bef117c37f889e1df06ea77925b169ad344f1eeb51f8774aff4b8e22f0dab78935fb6372259042899986d649527fe4d7fe087573069be3bf8dfe0df146957cb6fa1ea9f09fc47e20c42f33e9961a2fc44b6b0d4ec2fecc24924104be28ff843a6d42f1e25b6b34d207db67853cb59bfd00bf6fafd93be287c46fd98fe36f87354bed6b54f0b47a4d878ba1d0a2f8c7f19fc673d9695e03bed2bc61a828d5bc59f13fc34b77ab5ec5a1ea6b6b7f75e1eb8874ab8beb27b7b1bbfecb2f7ff00c957c0afd8abe23fecebff00059ffdad7e0b2e8b3e8ba3fc52fd923e2c7c5292d5a59aeed6f2e7c45e0cf0beb5a9cf6d6f7be2bf1666c2dfe30eada87fc236975e2ff135fd849a7dadb3f88ef1ed2fee27fbde0aad4e9e6bc3599a8ba957015b1d93e3795f2c6a65d8dc3e2bd8559e8da950ad8b74a73fb317878af7b9547c3cc1b783ccf0774a35211cc297327a54c3d5c2caa53564f99548d394e31d5b6ea5d248fef67f649b3783e12f80f893658781fc2ba2dab4f2cb2cada7e99a5db5969cd33c859a4b86b2b489a67767632b1677672ec7eaab812342eb1322c876ec6705d01cf24aa952d81923905980078af0efd9ef4f1a7fc3ad1502b241736b6d75649200192c67811add3e46742aac2408170bb42ed0aa703de3f3ea3b0f4e7f1f53d31c0e6be1b3eaaaae7398d48fc3f5aa9c9a6d08cbdd8b4d744945ab59d99e8e1bfdde8eff00c286f7d7dd5df5d77d77b9f807ff000724e876937fc129ff00681d71c0b47f0bdd780bc4a977129129bfb4f1a691a6e9ac65465910fdbf57b588ce0830a48d21deaa40ff00378f14f81fc67f12be05fecede18f0ef87efb5bf18fc40f8b1e284f01f82b44d360ba1e28d4f5cf15d8783f497d4e3597ed4f79ad6a368748f0ed90925852cf4cf11dd6c58ef1a787fd22ffe0e50f1d781bc37ff0004a2fda3bc31e2cd7b49d3f56f1ee93a168be0ad12eaea26d5fc4fe24d33c57e1ff125b58e89a3464ea1aa1b41a235edfcb668d16936314daa5ec905b5a4d327cabff0449ff8245787fc33f073f652fdacbf683d1342bff887a77c1ef863e23f837e19b323541e0fb5d7fc0f0ea30f8bb5ed4aeadfca97c4ed69e24d5e5d0b45b0852c7c2d75e24f14ea37d7fe23d5751d05bc1ff7f91e794b03c159ae031f5250a198e0ebe1f0f1e5729d5a95330c23e6a4dd94a50a784af069be4a71bca49ca308be0ab45bc761ebd282a93a38c5524ef64a11c1d48a52777cabda56836d2726972ed276fd3dff00824fff00c13b3c35ff0004f7fd9b7c03f0ee3834e9bc797fa741e23f88dac69fba4b7bdf1b6b5a6db2eab63613cf147753689e1db1b0d23c2ba24f731433de697e1eb0bbba8a2b899e31fab733a205424fef49039071c166c9eca15493d467007246726e34bbd7921920d62f6d96218317d9ec1e27f99189606d164dc426cc89000aef81b882316f2c755b6bd8eee6d78dc4325efee34ffecf862c79c30f025c24ed2986384798cac8f9d8cecc0e193f3ac4e22ae675fdbd7c453e7f671a74e1cb56d0a5462a1468c1469b8251a6959a97228a77927ee9e95282a6f92319352a8e73937169ca6d3949de5ccd77b45bb2b24ce9a585a584ae4a898877c0e8171804739c85456e70407c9c8e7cb2eeda5fb5dce1703ed137186e3f78dc70a471ec4d7b02ab79614904edc673cf18c9c74cf1923f11585268d6d248eecaa59dd9d8eec6599892719e3249a9c1631615cf9939295ad649eb757ddadd23ae13ba71924ecdb56baf26eeb75b5baf7364c71c219634da396d806467827a70727af39039ac4d4a6927b04b948b2d677f677051801b6082f6133c83a8dc2d44b2a038c9c03b5b760a2b9e8eb2e67f129d3d6fafbd2e569faa324bdd72bbba7defd97519acd869be2cd0357d0aef171a66b7a5ea1a2ea1101cb5a6a3692d95dc6e0e31be09dd30d86e7bd7e25fed21fb31f882e7f6e2fd983f68fd1ade47d5bc57f083c59f04be256a90dbc263b1d02d6db53f1da4b3198064fb7eaacba392caad0dcdae9a817c8fb4c521457d7707e26a60b38ab4e9724a0e8632f0ab1538b70c0e26a45b5a2bc6a53a535fdea54dbbf2a3c9cd68c2a508b774f9a0ef17676f68a0d5fb4a139464baa7e96fda4f87564b61e0af0c5aac490adbe8ba6c091a1cac71456a8b14624601d82461577300782793827b29d2474db13f96fc61b1c76201041e0e395c1c0e78ce41457c8e266e789af37f14ab5593b777393eb73be8bfddc176a74ff1823f12bfe0b71f0bbc177bff0004f7fda7bc77ab69367a9f88adfc1ba6e8363adea16c6f35363e22f16687a15b68ba6ccf14f2e9fa549aa6a1632496d6a035fdec16cf70e05b45707f53ff0067ef0937803e017c14f05bc2b6d2783fe147c3ef0c9b6894451db3e83e10d2b4c6b78e34f9625805a98d5506c4081546d18a28afa1c7622bd7e19ca2356a4a71a79be6d469a93bf252a786caa50a717ba845d59b8c5b718f33e551bcafcf4a11a78ec446115152a54a72b759294e376b66da4aeed77d59fcdc69ff00b7affc148fc75f1f7e21fc09f857f14bc1aabe12f147886d750f11f8c3c19e057d3fc21a0278be7f0d685f6f9478645dea5a85f5cb476da55a4f3196e9a09e7d4351b2b382e2e17ea6ff008267f83bf6f5f11fedaff1fbe21fed8ffb516abf1dbc19e11f843e0ed1be13f85f49d374ef027c3ed0755f1b78af5a935cd5c7c3cf09dbe9be1797c596365e031649e20d42d2ff005eb4d2f5e9f4d5bbb782e6e20728af1f36cd7114788b1d9561e8e030d82a181c1d1e4a180c253ab5658bc1fd6ea57a98af64f131ad19c1d382a35a95174a735568d4a9cb523fb5716e0f2cc0e4d83a585ca72fa15f139061b34a98fa54aad3c67d6259ad3c14a09c2b470fec6541cb9a12c3ca6e7273f69750e5fe8298955247618c9e9d40edcf1d87553c1e0d38741c0e9fdc3fd28a2bcf3f217faafc5a3fffd9, 'Malang', '66120', '', '', '', '', NULL, '', '', '', 0, 0, NULL, NULL, NULL, NULL, '', 0, 0, '', '', '', '', '', '', 0, NULL, NULL, 0, '26903', '74143', '20827', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 16259, 0),
(301, '15856', '15856', 'ASHINTA ANGGRAELISABET HADI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '36754', '68605', '58051', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 882, 0),
(225, '15857', '15857', 'ASYRAF RIFQI GHALY FIRDAUS', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '49880', '28854', '29902', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 60350, 0),
(302, '15858', '15858', 'ATHAYA HAIDARANIS NADHIRA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '82760', '89489', '20737', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 55618, 0),
(187, '15859', '15859', 'AUDY DANIAL WIBAWANTO', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70887', '38347', '96342', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 52748, 0),
(263, '15860', '15860', 'AYU SEKAR PAMBAYUN', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96792', '88942', '94213', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 29558, 0),
(331, '15861', '15861', 'AZIMA AL RUSYDIYA KUSUMA PRATI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81636', '22414', '29585', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 21815, 0),
(154, '15862', '15862', 'BAGUS ARIE WICAKSONO', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76372', '53659', '67830', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 31040, 0),
(155, '15863', '15863', 'BALQISH SYAFIRA MAULIDYA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38281', '41886', '72133', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 1740, 0),
(226, '15864', '15864', 'BAYU ARIF RAMADHAN', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55426', '17527', '22299', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 41526, 0),
(188, '15865', '15865', 'BESTARI GILANG NOOR ABDILLAH', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50554', '46312', '26350', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 9963, 0),
(189, '15866', '15866', 'BIANCA ADINDANESTYA PUTRI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '65831', '18591', '85338', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 28123, 0),
(79, '15867', '15867', 'BRIAN ADITYA NUR MUCH', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29625', '77351', '11692', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 46749, 0),
(156, '15868', '15868', 'CINDY MEGARANI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16378', '66841', '73870', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 21951, 0),
(303, '15869', '15869', 'CINTYA KUSUMA MAHADHIKA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '26201', '49655', '74492', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 35075, 0),
(264, '15870', '15870', 'CINTYA PUSPITA MUSTOFA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14416', '71084', '49601', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 19114, 0),
(265, '15871', '15871', 'CORNELIA NINDYA SEPTIAN', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95816', '39091', '85912', '', NULL, NULL, NULL, '2013-02-03 23:20:06', 10214, 0),
(332, '15872', '15872', 'CRESENSIA RARA H.', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84932', '71633', '35800', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 9829, 0),
(304, '15873', '15873', 'DANDUNG SATRIA WIRAMBODO PUTRA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22101', '33260', '85245', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 59197, 0),
(266, '15874', '15874', 'DEANO DAMARIO PUTRAYURIN', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '85635', '88829', '93891', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 38926, 0),
(267, '15875', '15875', 'DEASY SARASWATI', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71912', '20452', '10173', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 20474, 0),
(157, '15876', '15876', 'DESSY AYU FENTY R', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32264', '15483', '77452', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 5262, 0),
(43, '15877', '15877', 'DEVI HASNA SALSABELA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1992-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66121', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52420', '91697', '62492', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 25098, 0),
(116, '15878', '15878', 'DEVINA YULIA PUTRI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57726', '70527', '89696', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 57439, 0),
(333, '15879', '15879', 'DHEA ANDHIA PUTRIE SUWANDI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22823', '50382', '48766', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 704, 0),
(158, '15880', '15880', 'DHIMAS RADITYA W.', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51343', '97143', '84516', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 24856, 0),
(305, '15881', '15881', 'DIANITA ROSAYANI PUTRI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '35630', '43001', '24014', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 53537, 0),
(117, '15882', '15882', 'DIAS NOVI NURHAYATI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57592', '52741', '82267', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 2697, 0),
(80, '15883', '15883', 'DIMITRI YOLA NALURITHA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86937', '78221', '41062', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 24883, 0),
(118, '15884', '15884', 'DINA PUJI LESTARI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '18246', '42186', '96133', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 41003, 0),
(44, '15885', '15885', 'DINDA AYU WANODYA SUPRIATINING', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1993-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66122', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70055', '26528', '74962', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 61595, 0),
(334, '15886', '15886', 'DINDA WIDYA HAVSARI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25726', '91906', '62352', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 53021, 0),
(45, '15887', '15887', 'DINNIA CITRA  NANDYA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15957', '45983', '93645', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 55993, 0),
(81, '15888', '15888', 'DITA WIRAYANTI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1994-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66123', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27364', '98445', '25837', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 7529, 0),
(119, '15889', '15889', 'DITZA SHARFINA ADANI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '36138', '19916', '35552', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 61981, 0),
(82, '15890', '15890', 'DWI FANNY AGUSTINI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83704', '90254', '82714', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 61488, 0),
(159, '15891', '15891', 'DWIKY AJI KURNIAWAN', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62799', '14101', '20790', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 11518, 0),
(46, '15892', '15892', 'DWIVANNY RETNANINGTYAS', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62589', '76807', '77732', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 64071, 0),
(120, '15893', '15893', 'DZAKY HUSAM', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60210', '69941', '18010', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 50599, 0),
(121, '15894', '15894', 'EGA SETIAWAN', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '90858', '67823', '77945', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 15366, 0),
(83, '15895', '15895', 'EGA YAMAWIDURA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79492', '10123', '44011', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 63083, 0),
(268, '15896', '15896', 'EKA YANIK OKTAVIA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88200', '95139', '27383', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 62619, 0),
(84, '15897', '15897', 'ELFIRDAUZIA AMALIA RIZKY', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14769', '14076', '48596', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 44429, 0),
(85, '15898', '15898', 'ELISYA JAUHAR MAHARDITA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51915', '80230', '22637', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 16204, 0),
(190, '15899', '15899', 'ELYA RAHMITA R', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1995-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66124', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '97359', '27096', '19175', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 14895, 0),
(306, '15900', '15900', 'EMILDA NOVI RACHMAWATI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '73697', '17279', '61830', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 63378, 0),
(191, '15901', '15901', 'ERIC GIAN ANGGARA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '82920', '58140', '96220', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 39065, 0),
(269, '15902', '15902', 'ERICK MAHENDRA CANTONA SALIM', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43300', '73105', '52739', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 634, 0),
(122, '15903', '15903', 'EVALDO WIYOKO WIBISONO', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '61980', '75604', '82366', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 59646, 0),
(123, '15904', '15904', 'EVITA NUR EKALIA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '79547', '96909', '42424', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 31795, 0),
(307, '15905', '15905', 'FACHREZA HADI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59862', '31238', '39158', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 20572, 0),
(86, '15906', '15906', 'FADHEL FRISMAJAYA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95115', '48190', '97457', '', NULL, NULL, NULL, '2013-02-03 23:09:34', 31199, 0),
(227, '15907', '15907', 'FADILA NUR AFIDA S', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55579', '54143', '79366', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 54304, 0),
(47, '15908', '15908', 'FADILLA NUR ISLAMIA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '53137', '42799', '94445', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 9677, 0),
(87, '15909', '15909', 'FAHMI SATRIA AJI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96484', '66406', '67974', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 26913, 0),
(192, '15910', '15910', 'FAJAR SATRIA WICAKSANA WAHONO', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22594', '20809', '19449', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 36300, 0),
(160, '15911', '15911', 'FANIA AYU WARDANI ', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33473', '41837', '28372', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 53745, 0),
(193, '15912', '15912', 'FANIA DEWI AMALIA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75169', '75238', '81056', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 14413, 0),
(88, '15913', '15913', 'FANNI RAHMATIKA SANI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '19627', '70002', '87529', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 62, 0),
(308, '15914', '15914', 'FANUEL BIMO SETIADI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16987', '38738', '77900', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 24, 0),
(89, '15915', '15915', 'FARAHDITA NOVIA LAMUSU', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '13226', '88928', '47883', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 64628, 0),
(194, '15916', '15916', 'FARHAN RAMADHAN', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74457', '41756', '29685', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 41811, 0),
(228, '15917', '15917', 'FARIH FIDDAROIN FADLI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58597', '76380', '44787', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 12163, 0),
(195, '15918', '15918', 'FARIZ GALI PUTRA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '40147', '21424', '76124', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 6845, 0),
(309, '15919', '15919', 'FATHIYA RACHMASARI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '74901', '63558', '79728', '', NULL, NULL, NULL, '2013-02-03 23:21:43', 36254, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(270, '15920', '15920', 'FATIMAH AZ ZAHRA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62231', '96831', '91302', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 63768, 0),
(271, '15921', '15921', 'FATKHI RAHMAN', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12116', '47126', '90029', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 38455, 0),
(48, '15922', '15922', 'FATWA LAHUM Y', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55436', '49865', '94861', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 27167, 0),
(310, '15923', '15923', 'FEBYOLA YOLANDA ROOSMARI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '53803', '72801', '16780', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 16352, 0),
(161, '15924', '15924', 'FIFI YULIANA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50840', '63906', '65966', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 56817, 0),
(162, '15925', '15925', 'FIRDAUSA ARVYANTI ANDRIANTO', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25237', '20702', '55841', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 14270, 0),
(49, '15926', '15926', 'FITRIA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93689', '40664', '17988', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 11707, 0),
(311, '15927', '15927', 'FORTUNATUS YOSANTONINO S.P.', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21585', '81018', '74156', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 1649, 0),
(229, '15928', '15928', 'FRISKA RETNANINGTYAS', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '36420', '73810', '42312', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 31038, 0),
(50, '15929', '15929', 'FUAD AZHAR MUSIRAN', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72191', '81965', '92549', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 6451, 0),
(230, '15930', '15930', 'FURQON MUHAMMAD AFIF', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '42848', '17371', '68676', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 57692, 0),
(90, '15931', '15931', 'GALIH CAKRAYUDA THOHARI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '20251', '35308', '81406', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 57205, 0),
(196, '15932', '15932', 'GALUH INDAH PURBOLARAS', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59720', '30940', '18288', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 8463, 0),
(272, '15933', '15933', 'GAMMA FITRIAN PERMADI', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '77040', '80340', '41614', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 44022, 0),
(312, '15934', '15934', 'GHAZI LABIB NAUFAL INSAN', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21738', '52002', '57394', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 18049, 0),
(231, '15935', '15935', 'GHINA ADILA HAFIZHAH', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64568', '13558', '55675', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 42094, 0),
(313, '15936', '15936', 'GIANINA ANDINTARA Y.', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75122', '25682', '77771', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 23253, 0),
(51, '15937', '15937', 'GIGIH SEPTIAN', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47177', '33456', '29752', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 3380, 0),
(273, '15938', '15938', 'HABRIDIO KURNIAWAN PUTRA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78016', '66682', '97550', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 58873, 0),
(52, '15939', '15939', 'HANA` PUTRI RAHMANSARI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71357', '75870', '23580', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 30672, 0),
(91, '15940', '15940', 'HANIAH TAUFIK ALKATIRI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34638', '80690', '59741', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 13633, 0),
(53, '15941', '15941', 'HANIF CALADI SAMUDJI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45634', '36722', '20390', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 3396, 0),
(232, '15942', '15942', 'HANIN NOVIA ANANDA', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '28139', '67609', '63495', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 40529, 0),
(54, '15943', '15943', 'HAQQI ALIF RAHMAWATI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29811', '21232', '33811', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 33266, 0),
(124, '15944', '15944', 'HELENA STEPHANIE B', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '48553', '53412', '36523', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 15744, 0),
(125, '15945', '15945', 'HENDITO K. PUTRA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '11654', '63721', '10299', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 7854, 0),
(274, '15946', '15946', 'HERLAMBANG DWI PRAYOGA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59877', '63756', '47564', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 48117, 0),
(314, '15947', '15947', 'HIBATULLAH IMANUNA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95400', '67351', '36571', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 62282, 0),
(233, '15948', '15948', 'HILAL ALAM FIRDAUSI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70597', '87290', '44217', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 8764, 0),
(335, '15949', '15949', 'HIMMAH ISLAURA MAS`UDI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1996-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66125', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '15245', '81815', '41378', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 28719, 0),
(92, '15950', '15950', 'HISRA MALIKHA ASSRI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '54422', '60990', '99756', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 4777, 0),
(275, '15951', '15951', 'HUSNI IHSUDHA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '77055', '88637', '60594', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 31723, 0),
(55, '15952', '15952', 'IBRAHIM IMAMUN HANIF', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75502', '61745', '63175', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 13909, 0),
(197, '15953', '15953', 'IGA ANDRIANINGSIH', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47548', '58433', '87785', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 33454, 0),
(234, '15954', '15954', 'IHYAK ULUMUDDIN', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91612', '79205', '48697', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 3, 0),
(235, '15955', '15955', 'IKA MAULINA PRATIWININGTYAS', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70713', '61049', '43096', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 25688, 0),
(163, '15956', '15956', 'IKA PUSPITASARI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55066', '51851', '71159', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 686, 0),
(198, '15957', '15957', 'IKA RIZKYAH KHOMZI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14529', '55706', '54443', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 23183, 0),
(93, '15958', '15958', 'IKKE ALMA ALUKA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95341', '34605', '15638', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 14067, 0),
(236, '15959', '15959', 'ILHAM', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '23039', '81097', '90353', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 23514, 0),
(315, '15960', '15960', 'IMMANUEL KAVISSON JUDIANTO', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '77914', '77420', '54564', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 29689, 0),
(164, '15961', '15961', 'INDRIANI DETTY P.', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45382', '93843', '42502', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 62759, 0),
(126, '15962', '15962', 'INGE MUSTIKA PRILIA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72991', '43185', '88507', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 6618, 0),
(199, '15963', '15963', 'INGE YULIANE SUSIANTO', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '16149', '62072', '30110', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 33925, 0),
(336, '15964', '15964', 'INSANUL FIRDAUS', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1997-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66126', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80715', '63146', '87935', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 14072, 0),
(56, '15965', '15965', 'IQBAL TAQWAKAL PUTRA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80197', '26743', '95107', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 17953, 0),
(57, '15966', '15966', 'IRFAN ARIQ ANSA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32013', '19994', '17640', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 12214, 0),
(237, '15967', '15967', 'IRFAN SETIAWAN', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '54081', '15435', '22330', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 7958, 0),
(94, '15968', '15968', 'JOHAN ADEPARAMA ARGA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84472', '69850', '15736', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 13390, 0),
(200, '15969', '15969', 'JUANITA TIFFANY PUTRI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14291', '65334', '97497', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 56571, 0),
(316, '15970', '15970', 'JULIAN CHRIST EXCELL RONDONUWU', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50304', '92393', '43447', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 38228, 0),
(127, '15971', '15971', 'JUNITA DIAN PRAMESWARI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '48501', '22822', '77861', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 55229, 0),
(165, '15972', '15972', 'KAMILA HAFIDA AR-ROZY', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '19441', '33244', '18112', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 21877, 0),
(201, '15973', '15973', 'KAMILIA KUSUMAWARDHANI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96914', '32981', '45930', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 5518, 0),
(276, '15974', '15974', 'KANIA AVIANDI SAVITRI', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '19790', '91015', '24911', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 59156, 0),
(166, '15975', '15975', 'KARTIKA WIDYASARI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80462', '34634', '82382', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 55647, 0),
(58, '15976', '15976', 'KARUNIA DIAN PUSPITASARI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70149', '76474', '34321', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 33618, 0),
(95, '15977', '15977', 'KHAIRINA FADIAH HIDAYATI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '64711', '62833', '52730', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 36079, 0),
(59, '15978', '15978', 'KHAIRINNISA S.', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78987', '47338', '51695', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 601, 0),
(238, '15979', '15979', 'KHOIRUNNISA', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '43610', '57654', '19758', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 1747, 0),
(128, '15980', '15980', 'KRESNA WISIKA PUTRA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34445', '80914', '24366', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 45671, 0),
(129, '15981', '15981', 'LATANIA NAUFA ARINUGRAHA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2006-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66135', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '33938', '48905', '93083', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 65492, 0),
(96, '15982', '15982', 'LINDA DWI AYUNINGTYAS', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72418', '66143', '50788', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 34581, 0),
(202, '15983', '15983', 'LINGGA LAZUARDI AL AZIZ', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38345', '59572', '89688', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 28054, 0),
(239, '15984', '15984', 'M. ALVIAN AZWAR ANAS', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24847', '73212', '40634', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 55919, 0),
(337, '15985', '15985', 'M. SHAHIB AL BARI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24912', '56436', '50671', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 22693, 0),
(97, '15986', '15986', 'MARIZA DINDA RIZKYNA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '94023', '34509', '39161', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 23055, 0),
(130, '15987', '15987', 'MARLINA MUTIARAHMI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2007-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66136', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47125', '26575', '95866', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 8933, 0),
(167, '15988', '15988', 'MASYITHOH NUR KHOIRUNNISA`', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14482', '61007', '27866', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 60308, 0),
(131, '15989', '15989', 'MATAHARI DYAH ARIANNE', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50761', '32617', '54668', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 60306, 0),
(240, '15990', '15990', 'MOCH. HANIF ILHAM', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '10831', '12737', '98378', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 35835, 0),
(132, '15991', '15991', 'MOCHAMAD GIFARI ILYASA RAMADHAN', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88719', '56077', '83187', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 61688, 0),
(133, '15992', '15992', 'MOCHAMAD HILMAN ADYF PRATOMO', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45152', '39013', '75186', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 30246, 0),
(203, '15993', '15993', 'MOCHAMAD RISNANDA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2008-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66137', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86990', '73892', '64057', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 63893, 0),
(204, '15994', '15994', 'MOHAMAD DZIKY PRAMUKTIA GUSTI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '69436', '88246', '69948', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 28935, 0),
(134, '15995', '15995', 'MOHAMAD JAYA SAPUTRA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '87287', '78178', '73616', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 33218, 0),
(168, '15996', '15996', 'MUCHAMMAD FIQRI FAHMI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '60283', '11605', '58760', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 45627, 0),
(169, '15997', '15997', 'MUCHAMMAD SHODIQ ALMUNSOWI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24926', '26655', '87090', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 55921, 0),
(170, '15998', '15998', 'MUHAMMAD ARIEF NUGRAHA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78209', '31030', '94223', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 30750, 0),
(171, '15999', '15999', 'MUHAMMAD BARIK NUGROHO', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78245', '64142', '24976', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 32708, 0),
(172, '16000', '16000', 'MUHAMMAD FIKRI UTOMO', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52855', '35531', '89751', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 17021, 0),
(98, '16001', '16001', 'MUHAMMAD HAFIDAVI FAHREZY', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14233', '90452', '21814', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 58759, 0),
(99, '16002', '16002', 'MUHAMMAD HAFIZH ZULKARNAEN', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '73087', '17736', '38442', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 15366, 0),
(338, '16003', '16003', 'MUHAMMAD HAMZAH FARRASAMULYA', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1999-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66128', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '36768', '63970', '50738', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 59358, 0),
(205, '16004', '16004', 'MUHAMMAD IHSAN', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41450', '96429', '96590', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 65416, 0),
(206, '16005', '16005', 'MUHAMMAD RAMZI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76657', '20407', '34887', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 61488, 0),
(207, '16006', '16006', 'MUHAMMAD TAUFIQUL NAUFAL', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '50225', '16111', '87650', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 7340, 0),
(60, '16007', '16007', 'MUHAMMAD YOGIE NUGROHO', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2009-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66138', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86375', '33212', '43479', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 57795, 0),
(135, '16008', '16008', 'MUHAMMAD ZAMRONI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57272', '19038', '99328', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 5909, 0),
(241, '16009', '16009', 'NABILA KHAIRA', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '25557', '60927', '89918', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 46357, 0),
(242, '16010', '16010', 'NABILLA EKA LARASATI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91509', '40084', '58174', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 54243, 0),
(136, '16011', '16011', 'NABILLAH YUNITASARI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55744', '16592', '78464', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 34547, 0),
(173, '16012', '16012', 'NADEA AYUNING ASTIAINANDHIYA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '11863', '23044', '31184', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 19814, 0),
(243, '16013', '16013', 'NADHIRA DYASTI P.', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57580', '31740', '61243', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 7894, 0),
(339, '16014', '16014', 'NADIA FATIN RAMADHIANI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2000-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66129', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '93654', '31858', '79120', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 7485, 0),
(317, '16015', '16015', 'NADIA PUTRI SARASWATI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88928', '42047', '90400', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 28570, 0),
(244, '16016', '16016', 'NADIA TAMARA A. ', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66405', '84828', '30235', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 18827, 0),
(340, '16017', '16017', 'NADILA AZTIZAR PUTRI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2001-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66130', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47252', '26013', '40115', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 44590, 0),
(61, '16018', '16018', 'NADYA LISFRIANA NOVITA IKASARI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12838', '80228', '19732', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 20223, 0),
(208, '16019', '16019', 'NAILA AGITAYANTO KIRANA SAKTI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '77050', '59810', '68661', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 41045, 0),
(341, '16020', '16020', 'NAMIRA', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2002-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66131', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71342', '35465', '31713', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 23065, 0),
(174, '16021', '16021', 'NARENDRA HARYO B.', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '26291', '61310', '24990', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 11548, 0),
(277, '16022', '16022', 'NEISYA INTAN CAHYANINGTYAS A', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2010-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66139', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '39279', '74620', '68340', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 9628, 0),
(137, '16023', '16023', 'NINDYA RIZQI ANJANI MARHENDRA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95833', '63146', '76729', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 48481, 0),
(209, '16024', '16024', 'NINDYASARI NASTITI', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '31388', '78391', '12433', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 5500, 0),
(62, '16025', '16025', 'NUDIA AMILIA RACHMAWATI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '69941', '32878', '93070', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 57349, 0),
(100, '16026', '16026', 'NUR `AINI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38307', '80478', '11326', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 35913, 0),
(245, '16027', '16027', 'NUR AISYAH ROOSYIDAH', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '78080', '63965', '74937', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 47898, 0),
(138, '16028', '16028', 'NUR AZIZATUN  NISA', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47043', '21708', '57119', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 62351, 0),
(278, '16029', '16029', 'NUR HABIBAH FAJR`INA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2011-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66140', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '19215', '69352', '79324', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 24913, 0),
(175, '16030', '16030', 'NURAFIFA DWI PUTRI INDAWAN', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12711', '86881', '21639', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 61171, 0),
(176, '16031', '16031', 'OLIVIA MEGA DYAH PRAMESTI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44949', '41902', '73898', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 60323, 0),
(63, '16032', '16032', 'PANDU ANUGRAH WIRA UTAMA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '55210', '44598', '70111', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 44544, 0),
(342, '16033', '16033', 'PARADISA EKSAKTA GHEOSA', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2003-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66132', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71003', '54014', '79328', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 51497, 0),
(246, '16034', '16034', 'PHEBY NOVIRA ADYANTI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '40668', '87882', '25330', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 46980, 0),
(279, '16035', '16035', 'PRADIGDA YOGA ADITYA DHARMA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2012-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66141', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56173', '57438', '11234', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 29273, 0);
INSERT INTO `siswa` (`replid`, `nis`, `nisn`, `nama`, `panggilan`, `aktif`, `tahunmasuk`, `idangkatan`, `idkelas`, `suku`, `agama`, `status`, `kondisi`, `kelamin`, `tmplahir`, `tgllahir`, `warga`, `anakke`, `jsaudara`, `bahasa`, `berat`, `tinggi`, `darah`, `foto`, `alamatsiswa`, `kodepossiswa`, `telponsiswa`, `hpsiswa`, `emailsiswa`, `kesehatan`, `asalsekolah`, `ketsekolah`, `namaayah`, `namaibu`, `almayah`, `almibu`, `pendidikanayah`, `pendidikanibu`, `pekerjaanayah`, `pekerjaanibu`, `wali`, `penghasilanayah`, `penghasilanibu`, `alamatortu`, `telponortu`, `hportu`, `emailayah`, `alamatsurat`, `keterangan`, `frompsb`, `ketpsb`, `statusmutasi`, `alumni`, `pinsiswa`, `pinortu`, `pinortuibu`, `emailibu`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(139, '16036', '16036', 'PRASETYO KURNIAWAN', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '40147', '66412', '86908', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 9400, 0),
(343, '16037', '16037', 'PUTRA FIRMAN ARDIANSYAH', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2004-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66133', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '75145', '35507', '70594', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 23909, 0),
(318, '16038', '16038', 'PUTRI  AULAWIYA ROSYIDA HALIM', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '23068', '42488', '48211', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 8348, 0),
(210, '16039', '16039', 'QISTHA YAMINICK', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51777', '16444', '58736', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 52569, 0),
(64, '16040', '16040', 'R. AJ. JULIETTA LARASATI RASDI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80065', '26955', '50084', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 49188, 0),
(65, '16041', '16041', 'RACHMADIANTI SUKMA HANIFA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67341', '33325', '31567', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 45612, 0),
(101, '16042', '16042', 'RADEN RORO SULISTYANTARI RETNO PALUPI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44831', '69240', '74813', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 33507, 0),
(66, '16043', '16043', 'RAFI FARAH SURYA MAHARANI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70634', '79933', '88118', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 27331, 0),
(140, '16044', '16044', 'RAFI NDARI ARDIANTO', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '63579', '38042', '36699', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 51481, 0),
(67, '16045', '16045', 'RAGA KHARISMA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41685', '23492', '23920', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 37521, 0),
(102, '16046', '16046', 'RAHADHIWARDAYA MUHAMMAD', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62604', '80129', '33384', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 7107, 0),
(280, '16047', '16047', 'RAHMA WIDJNA NING RISTITI', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2013-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66142', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83526', '12350', '33698', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 30927, 0),
(281, '16048', '16048', 'RAMADHANI DWI HIDAYATI', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2014-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66143', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '72954', '44560', '92110', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 45544, 0),
(282, '16049', '16049', 'RANGGA PANDU DEWANATA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2015-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66144', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12910', '12299', '33957', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 56196, 0),
(283, '16050', '16050', 'RATNA ARIANDA JUWITA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '59570', '28776', '58749', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 54996, 0),
(284, '16051', '16051', 'RATYA PRABASWARA RAMADHANA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '54298', '17618', '32146', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 64585, 0),
(177, '16052', '16052', 'REGIA ILMAHANI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76403', '82335', '79465', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 28754, 0),
(319, '16053', '16053', 'REGINA CLARABELLF', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86481', '29767', '39867', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 27478, 0),
(103, '16054', '16054', 'REINALDY FATURRAHMAN ROYANI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '88854', '97492', '29239', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 57022, 0),
(320, '16055', '16055', 'RENDHITYA DWIKI FEBRIANSYA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '80150', '17858', '35931', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 6903, 0),
(321, '16056', '16056', 'REYNA HARUM APRILIA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '41007', '72397', '76852', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 6878, 0),
(68, '16057', '16057', 'RHEZANDY GUSTI PRAMANA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22879', '50177', '71970', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 35695, 0),
(247, '16058', '16058', 'RIAN ACHMAD WILDAN ANISANTO', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '11690', '23380', '92712', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 40550, 0),
(178, '16059', '16059', 'RICKY DARMAWAN', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '38407', '28991', '80829', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 16284, 0),
(211, '16060', '16060', 'RIENADIYA RAHAYOE', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2016-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66145', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '89338', '70877', '19903', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 59681, 0),
(212, '16061', '16061', 'RIFKY AFRIZAL FAJAR KURNIAWAN', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83557', '77541', '18774', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 13046, 0),
(213, '16062', '16062', 'RISA AMALIA WULANINGTYAS', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '83754', '11801', '61851', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 22074, 0),
(69, '16063', '16063', 'RISKI ADIWIJAYA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84909', '77026', '56981', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 7661, 0),
(179, '16064', '16064', 'RISYDA NOOR MARWATI DZAKIRAH MUROD', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '10505', '90555', '62301', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 2064, 0),
(141, '16065', '16065', 'RIZKA RAMADHANI M.', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '89282', '42422', '80334', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 63909, 0),
(142, '16066', '16066', 'RIZKY BUANA PUTRA RAMADHAN', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '46556', '97791', '30795', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 52985, 0),
(70, '16067', '16067', 'RIZKY NANDA HENDRA PRAMESTI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95429', '92369', '38531', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 42309, 0),
(180, '16068', '16068', 'RIZQI VIJAYANTI AL HAQ', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '17206', '10310', '94045', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 53071, 0),
(104, '16069', '16069', 'RM. DHIMAS DWIAGUNG P.', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '22879', '99773', '23585', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 44828, 0),
(105, '16070', '16070', 'ROHMA WIDOWATI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '91819', '22011', '96555', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 15168, 0),
(181, '16071', '16071', 'ROSSA ARIANDA VADHANA', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '68750', '76856', '30821', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 61565, 0),
(248, '16072', '16072', 'ROUDHATUS ZAHRA', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2017-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66146', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '51958', '88766', '11936', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 9608, 0),
(106, '16073', '16073', 'ROYYAN AMIGO', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62237', '54915', '17169', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 3056, 0),
(143, '16074', '16074', 'RR YOFI KURNIA BAVARI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '81853', '22934', '17643', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 23133, 0),
(144, '16075', '16075', 'RR. DEA ANNISAYANTI PUTRI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27111', '37838', '14172', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 26833, 0),
(285, '16076', '16076', 'RYANMIZAR SATRIO D. N.', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '96339', '22035', '56695', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 12192, 0),
(107, '16077', '16077', 'SAFFANA QOLBY MAYANA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '44798', '40806', '32785', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 31473, 0),
(214, '16078', '16078', 'SALSABILA ANGEL LICENTA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '86434', '72728', '57046', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 37508, 0),
(286, '16079', '16079', 'SALSABILA GHINA ANORAGA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '62607', '21144', '16501', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 17905, 0),
(322, '16080', '16080', 'SARI NUGRAHANING G.', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '67711', '10516', '52241', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 36218, 0),
(71, '16081', '16081', 'SATRIA BAGUS  WAHYU RAMADHAN', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '71883', '59257', '13509', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 32425, 0),
(287, '16082', '16082', 'SATRIA GANA HANDIKA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66788', '33510', '82089', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 54693, 0),
(108, '16083', '16083', 'SATRIA PARAMARTA NUGRAHA', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '97093', '26226', '10799', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 10728, 0),
(249, '16084', '16084', 'SHABRINA PUTRI CHAIRANDY', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2018-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66147', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '56345', '30957', '55902', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 1439, 0),
(72, '16085', '16085', 'SHIMA KUNAZA FAZIRA', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76910', '26972', '41002', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 9948, 0),
(251, '16086', '16086', 'SOFWAN SULTHONI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32861', '69765', '84552', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 16407, 0),
(252, '16087', '16087', 'TAHANI BAMAZROE', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45838', '76054', '28277', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 31568, 0),
(215, '16088', '16088', 'TANIA PUSPITA LUKMAN', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '54230', '78111', '70186', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 45526, 0),
(182, '16089', '16089', 'TAUFIK JUANG DWIADI', NULL, 1, 2013, 22, 53, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '76934', '67136', '39701', '', NULL, NULL, NULL, '2013-02-03 23:17:09', 19132, 0),
(253, '16090', '16090', 'TEDDY KURNIAWAN BAHAR', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '58777', '79203', '30965', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 51004, 0),
(216, '16091', '16091', 'TIARA NUR ANNISA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '27837', '73077', '59275', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 8158, 0),
(344, '16092', '16092', 'TIYAS DIAH AYU RATNA PUTRI', NULL, 1, 2013, 22, 58, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2005-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66134', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '46932', '27474', '67714', '', NULL, NULL, NULL, '2013-02-03 23:27:05', 63583, 0),
(109, '16093', '16093', 'TRI APSARI CAHYANDINI', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '52245', '30405', '90237', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 8397, 0),
(323, '16094', '16094', 'TRIFANI ROSAYU SALSABILA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2020-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66149', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '21088', '23892', '90998', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 24991, 0),
(288, '16095', '16095', 'TRISKA PRAKASA WIKANANDA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '37828', '32952', '32210', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 25713, 0),
(324, '16096', '16096', 'UTRUJJAH RAHMADIAN', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2021-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66150', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '66619', '98573', '91596', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 38444, 0),
(145, '16097', '16097', 'VASTHI MAHDIAH', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '23321', '61212', '44642', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 14845, 0),
(250, '16098', '16098', 'VENA LUNA INDRI ARIFIANTI', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2019-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66148', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '45916', '13291', '94396', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 52296, 0),
(289, '16099', '16099', 'VETTY IZZA AYURAHMA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '32962', '92900', '77773', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 56099, 0),
(73, '16100', '16100', 'WAHYUDIN ACHMAT DIMYATI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '24079', '15560', '38867', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 51380, 0),
(325, '16101', '16101', 'WIKRAMA PRANANDA HAKIKI', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84057', '64461', '63886', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 41638, 0),
(326, '16102', '16102', 'WILDA MAULIDINA', NULL, 1, 2013, 22, 57, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '12586', '39072', '61316', '', NULL, NULL, NULL, '2013-02-03 23:21:44', 64980, 0),
(74, '16103', '16103', 'WINDA RIANDINI', NULL, 1, 2013, 22, 50, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '70104', '34987', '82143', '', NULL, NULL, NULL, '2013-02-03 23:05:45', 41045, 0),
(254, '16104', '16104', 'YASINTA SWASTIKA AYU', NULL, 1, 2013, 22, 55, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '84920', '65457', '59356', '', NULL, NULL, NULL, '2013-02-03 23:19:11', 17009, 0),
(110, '16105', '16105', 'YENIKO ARDHYA P.', NULL, 1, 2013, 22, 51, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '47926', '81334', '53253', '', NULL, NULL, NULL, '2013-02-03 23:09:35', 54427, 0),
(217, '16106', '16106', 'YOKO TIO HERMANSYAH', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2022-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66151', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '57851', '39491', '51665', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 13160, 0),
(146, '16107', '16107', 'YULIANA RACHMAWATI', NULL, 1, 2013, 22, 52, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '14245', '77171', '71875', '', NULL, NULL, NULL, '2013-02-03 23:10:31', 4353, 0),
(218, '16108', '16108', 'YUSUF ADE NARARYA', NULL, 1, 2013, 22, 54, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '95221', '64065', '59223', '', NULL, NULL, NULL, '2013-02-03 23:18:03', 46183, 0),
(290, '16109', '16109', 'ZURU FINATUL KUSNA', NULL, 1, 2013, 22, 56, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '2023-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66152', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '20704', '30866', '32902', '', NULL, NULL, NULL, '2013-02-03 23:20:07', 58427, 0),
(1047, '16111', '9945632733', 'RIZQY HABIB WAHYUDI', NULL, 1, 2014, 20, 96, 'Belum Ada Data', 'Islam', 'Reguler', 'Berkecukupan', 'l', 'MLG', '1995-07-28', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Jl. Teluk Cendrawasih no 36 Malang', '5139', '', '', '', NULL, 'Belum Ada Data', NULL, 'AGUS HARTADI', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '29810', '61278', '72619', '', NULL, NULL, NULL, '2013-02-13 18:44:33', 61632, 0),
(675, '16115', '16115', 'GERARD ADAM PONTOH', NULL, 1, 2013, 21, 69, 'Jawa', 'Islam', 'Reguler', 'Kurang Mampu', 'l', 'Malang', '1998-03-14', NULL, 0, 0, NULL, '0.0', '0.0', '', NULL, 'Malang', '66127', '', '', '', NULL, 'Belum Ada Data', NULL, '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '', 0, NULL, NULL, 0, '34371', '20034', '31021', '', NULL, NULL, NULL, '2013-02-04 00:05:38', 60421, 0);

-- --------------------------------------------------------

--
-- Table structure for table `statusguru`
--

CREATE TABLE IF NOT EXISTS `statusguru` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`status`),
  UNIQUE KEY `UX_statusguru_replid` (`replid`),
  KEY `IX_statusguru_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `statusguru`
--

INSERT INTO `statusguru` (`replid`, `status`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(8, 'Asisten', 'Sekedar contoh. Menu ini mengatur status setiap guru yang mengajar di sekolah. Ubah atau tambahkan data ini sesuai dengan status guru yang ada di sekolah.', NULL, NULL, NULL, '2010-03-02 10:06:18', 11638, 0),
(7, 'Guru Honorer', '', NULL, NULL, NULL, '2010-03-02 10:06:18', 52760, 0),
(6, 'Guru Pelajaran', '', NULL, NULL, NULL, '2010-03-02 10:06:18', 32293, 0);

-- --------------------------------------------------------

--
-- Table structure for table `statussiswa`
--

CREATE TABLE IF NOT EXISTS `statussiswa` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`status`),
  UNIQUE KEY `UX_statussiswa` (`replid`),
  KEY `IX_statussiswa_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `statussiswa`
--

INSERT INTO `statussiswa` (`replid`, `status`, `urutan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(12, 'Belum Ada Data', 0, NULL, NULL, NULL, '2013-02-13 18:44:32', 11112, 0),
(11, 'Berkecukupan', 0, NULL, NULL, NULL, '2013-02-13 18:01:32', 319, 0),
(7, 'Eksklusif', 2, NULL, NULL, NULL, '2010-03-02 10:06:18', 3185, 0),
(6, 'Reguler', 1, NULL, NULL, NULL, '2010-03-02 10:06:18', 50107, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suku`
--

CREATE TABLE IF NOT EXISTS `suku` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `suku` varchar(20) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`suku`),
  UNIQUE KEY `UX_suku` (`replid`),
  KEY `IX_suku_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `suku`
--

INSERT INTO `suku` (`replid`, `suku`, `urutan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(38, 'Belum Ada Data', 0, NULL, NULL, NULL, '2013-02-13 18:01:32', 37365, 0),
(32, 'Jawa', 0, NULL, NULL, NULL, '2010-03-02 10:07:22', 36536, 0),
(34, 'Minang', 0, NULL, NULL, NULL, '2012-07-19 14:27:05', 23157, 0),
(33, 'Sunda', 0, NULL, NULL, NULL, '2010-03-02 10:07:22', 6177, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabledep`
--

CREATE TABLE IF NOT EXISTS `tabledep` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(10) unsigned NOT NULL,
  `tname` varchar(100) NOT NULL,
  `colname` varchar(100) DEFAULT NULL,
  `deldep` varchar(2) DEFAULT NULL,
  `upddep` varchar(2) DEFAULT NULL,
  `tingkat` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `coltype` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IX_content` (`tname`,`deldep`,`upddep`),
  KEY `IX_tabledep_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tabledep`
--


-- --------------------------------------------------------

--
-- Table structure for table `tahunajaran`
--

CREATE TABLE IF NOT EXISTS `tahunajaran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tahunajaran` varchar(50) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `tglmulai` date NOT NULL,
  `tglakhir` date NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_tahunajaran_departemen` (`departemen`),
  KEY `IX_tahunajaran_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tahunajaran`
--

INSERT INTO `tahunajaran` (`replid`, `tahunajaran`, `departemen`, `tglmulai`, `tglakhir`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, '2013/2014', 'SMAN 1 Malang', '2012-06-01', '2013-06-02', 0, '', NULL, NULL, NULL, '2013-04-23 16:24:10', 36129, 0),
(2, '2014/2015', 'SMAN 1 Malang', '2015-07-01', '2016-06-01', 1, 'Tahun Ajaran Sekarang', NULL, NULL, NULL, '2013-04-23 16:24:10', 49630, 0),
(3, '2015/2016', 'SMAN 1 Malang', '2013-07-01', '2014-06-30', 0, '', NULL, NULL, NULL, '2013-04-23 16:24:10', 49957, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tes`
--

CREATE TABLE IF NOT EXISTS `tes` (
  `sembarang` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tes`
--

INSERT INTO `tes` (`sembarang`) VALUES
(' REFERENSI'),
('ü Pegawai'),
('ü Identitas'),
('ü Departemen'),
('ü Angkatan'),
('ü Tingkat'),
('ü Tahun Ajaran'),
('ü Semester'),
('ü Kelas'),
(''),
('ü GURU DAN PELAJARAN'),
('ü Pelajaran'),
('ü  Pendataan pelajaran'),
('ü  Aspek Penilaian'),
('ü  RPP'),
('ü  Jenis-Jenis Pengujian'),
('ü  Aturan Grading'),
('ü  Perhitungan Nilai Rapor'),
(''),
('ü Guru'),
('ü  Status Guru'),
('ü  Pendataan Guru'),
(''),
('ü JADWAL DAN KALENDER'),
('ü Jadwal'),
('ü  Jam belajar'),
('ü  Jadwal Guru'),
('ü  Jadwal Kelas'),
('ü  Rekap Jadwal'),
(''),
(''),
('ü Kalender akademik'),
(''),
(''),
('ü KESISWAAN'),
('ü  Pendataan Siswa'),
('ü  Cari siswa'),
('ü  Statistik Kesiswaan'),
('ü  Pindah Kelas'),
(''),
('ü PRESENSI'),
('ü Presensi harian'),
('ü  Cetak Absensi'),
('ü  Presensi Harian'),
('ü  Laporan'),
('ü  Presensi Harian Siswa'),
('ü  Presensi Harian per Kelas'),
('ü  Harian Data Siswa yang Tidak Hadir'),
(''),
('ü  Statistik'),
('ü  Kehadiran Siswa'),
('ü  Kehadiran per Kelas'),
(''),
('ü Presensi Pelajaran'),
('ü  Cetak Presensi Pelajaran'),
('ü  Presensi pelajaran'),
('ü  Laporan'),
('ü  Presesnsi Pengajar'),
('ü  Presensi Siswa'),
('ü  Siswa tidak Hadir'),
('ü  Refleksi Mengajar'),
(''),
('ü  Statistik'),
('ü  Kehadiran siswa'),
('ü  Kehadiran Setiap kelas'),
(''),
(''),
('ü PENILAIAN'),
('ü Cetak Form Penilaian'),
('ü Penilaian Pelajaran'),
('ü Perhitungan Nilai Rapor'),
('ü  Komentar Rapor'),
('ü  Laporan Akhir Hasil Belajar Setiap Siswa'),
(''),
('ü Laporan'),
('ü  Daftar Nilai RPP Setiap Kelas'),
('ü  Rata-rata RPP Setiap Siswa'),
('ü  Laporan Nilai Setiap Siswa'),
(''),
('ü KENAIKAN DAN KELULUSAN'),
('ü Kenaikan Kelas'),
('ü Tidak Naik Kelas'),
('ü Kelulusan'),
('ü Pendataan Alumni'),
('ü Daftar Alumni'),
('ü Pencarian Alumni'),
(''),
('ü MUTASI '),
('ü Jenis-Jenis Mutasi'),
('ü Mutasi Siswa'),
('ü Statistik Mutasi Siswa'),
('ü Daftar Mutasi Siswa'),
(''),
('ü PENGATURAN'),
('ü Daftar Pengguna'),
('ü Ganti Password'),
('ü Audit Perubahan Nilai');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE IF NOT EXISTS `tingkat` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(50) NOT NULL,
  `idkurikulum` int(10) unsigned NOT NULL DEFAULT '1',
  `departemen` varchar(50) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `urutan` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_tingkat_departemen` (`departemen`),
  KEY `IX_tingkat_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`replid`, `tingkat`, `idkurikulum`, `departemen`, `aktif`, `keterangan`, `urutan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'X', 2, 'SMAN 1 Malang', 1, 'Sekedar contoh. Nama tingkatan kelas yang ada di sekolah. Ubah atau tambahkan data ini sesuai dengan nama tingkatan kelas di sekolah.', 1, NULL, NULL, NULL, '2013-02-03 18:55:17', 6098, 0),
(2, 'XI', 2, 'SMAN 1 Malang', 1, '', 1, NULL, NULL, NULL, '2013-02-03 18:55:23', 28378, 0),
(3, 'XII', 2, 'SMAN 1 Malang', 1, 'Edit', 3, NULL, NULL, NULL, '2013-02-03 18:55:30', 58064, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tingkatpendidikan`
--

CREATE TABLE IF NOT EXISTS `tingkatpendidikan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(20) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pendidikan`),
  UNIQUE KEY `UX_tingkatpendidikan` (`replid`),
  KEY `IX_tingkatpendidikan_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tingkatpendidikan`
--

INSERT INTO `tingkatpendidikan` (`replid`, `pendidikan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(25, 'D1', NULL, NULL, NULL, '2010-03-02 10:07:22', 26946, 0),
(24, 'D3', NULL, NULL, NULL, '2010-03-02 10:07:22', 50668, 0),
(23, 'S1', NULL, NULL, NULL, '2010-03-02 10:07:22', 41443, 0),
(22, 'S2', NULL, NULL, NULL, '2010-03-02 10:07:22', 55209, 0),
(21, 'S3', NULL, NULL, NULL, '2010-03-02 10:07:22', 20656, 0),
(28, 'SD', NULL, NULL, NULL, '2010-03-02 10:07:22', 3183, 0),
(26, 'SMA', NULL, NULL, NULL, '2010-03-02 10:07:22', 19478, 0),
(27, 'SMP', NULL, NULL, NULL, '2010-03-02 10:07:22', 22307, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE IF NOT EXISTS `ujian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpelajaran` int(10) unsigned NOT NULL DEFAULT '0',
  `idkelas` int(10) unsigned NOT NULL DEFAULT '0',
  `idsemester` int(10) unsigned NOT NULL DEFAULT '0',
  `idjenis` int(10) unsigned NOT NULL DEFAULT '0',
  `deskripsi` varchar(100) NOT NULL,
  `tanggal` date NOT NULL DEFAULT '0000-00-00',
  `tglkirimSMS` date DEFAULT NULL,
  `idaturan` int(10) unsigned NOT NULL,
  `idrpp` int(10) unsigned DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_ujian_idpelajaran` (`idpelajaran`),
  KEY `FK_ujian_idsemester` (`idsemester`),
  KEY `FK_ujian_idjenis` (`idjenis`),
  KEY `FK_ujian_idaturan` (`idaturan`),
  KEY `FK_ujian_rpp` (`idrpp`),
  KEY `FK_ujian_kelas` (`idkelas`),
  KEY `IX_ujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ujian`
--

INSERT INTO `ujian` (`replid`, `idpelajaran`, `idkelas`, `idsemester`, `idjenis`, `deskripsi`, `tanggal`, `tglkirimSMS`, `idaturan`, `idrpp`, `kode`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 45, 50, 1, 10, '', '2016-04-22', NULL, 15, 4, '', NULL, NULL, NULL, '2016-04-22 08:49:51', 0, 0),
(2, 45, 50, 1, 11, '', '2016-04-22', NULL, 16, NULL, '', NULL, NULL, NULL, '2016-04-22 08:52:40', 0, 0),
(4, 45, 50, 1, 12, 'UAS', '2016-04-22', NULL, 17, NULL, '', NULL, NULL, NULL, '2016-04-22 09:10:15', 0, 0),
(5, 47, 50, 1, 10, '', '2016-04-22', NULL, 15, NULL, '', NULL, NULL, NULL, '2016-04-22 09:11:44', 0, 0),
(6, 47, 50, 1, 11, '', '2016-04-22', NULL, 16, NULL, '', NULL, NULL, NULL, '2016-04-22 09:56:36', 0, 0),
(7, 47, 50, 1, 12, 'Tes', '2016-04-22', NULL, 17, NULL, '', NULL, NULL, NULL, '2016-04-22 09:58:33', 0, 0),
(9, 47, 50, 1, 11, '', '2016-04-22', NULL, 16, NULL, '', NULL, NULL, NULL, '2016-04-22 11:40:15', 0, 0),
(10, 45, 50, 1, 10, '', '2016-04-22', NULL, 15, NULL, '', NULL, NULL, NULL, '2016-04-22 11:55:05', 0, 0),
(11, 47, 50, 1, 13, '', '2016-04-22', NULL, 18, NULL, '', NULL, NULL, NULL, '2016-04-22 12:32:01', 0, 0),
(12, 47, 50, 1, 13, 'Baru', '2016-04-23', NULL, 18, NULL, '', NULL, NULL, NULL, '2016-04-22 12:32:33', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wilayah`
--

CREATE TABLE IF NOT EXISTS `wilayah` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namawilayah` varchar(45) NOT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `koordinat` text,
  `rootid` int(10) unsigned NOT NULL,
  `titikpusat` varchar(15) DEFAULT NULL,
  `zoom` varchar(30) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_wilayah_provinsi` (`rootid`),
  KEY `IX_wilayah_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `wilayah`
--


-- --------------------------------------------------------

--
-- Table structure for table `wilayah1`
--

CREATE TABLE IF NOT EXISTS `wilayah1` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namawilayah` varchar(45) NOT NULL,
  `gambar` blob,
  `koordinat` text,
  `rootid` int(10) unsigned NOT NULL,
  `titikpusat` varchar(15) DEFAULT NULL,
  `zoom` varchar(30) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_wilayah_provinsi` (`rootid`),
  KEY `IX_wilayah1_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `wilayah1`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktivitaskalender`
--
ALTER TABLE `aktivitaskalender`
  ADD CONSTRAINT `FK_aktivitaskalender_kalenderakademik` FOREIGN KEY (`idkalender`) REFERENCES `kalenderakademik` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `alumni`
--
ALTER TABLE `alumni`
  ADD CONSTRAINT `FK_alumni_departemen` FOREIGN KEY (`departemen`) REFERENCES `departemen` (`departemen`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_alumni_kelas` FOREIGN KEY (`klsakhir`) REFERENCES `kelas` (`replid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_alumni_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_alumni_tingkat` FOREIGN KEY (`tktakhir`) REFERENCES `tingkat` (`replid`) ON UPDATE CASCADE;

--
-- Constraints for table `angkatan`
--
ALTER TABLE `angkatan`
  ADD CONSTRAINT `FK_angkatan_departemen` FOREIGN KEY (`departemen`) REFERENCES `departemen` (`departemen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditpenerimaaniuran`
--
ALTER TABLE `auditpenerimaaniuran`
  ADD CONSTRAINT `FK_auditpenerimaaniuran_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditpenerimaaniurancalon`
--
ALTER TABLE `auditpenerimaaniurancalon`
  ADD CONSTRAINT `FK_auditpenerimaaniurancalon_audit` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditpenerimaanjtt`
--
ALTER TABLE `auditpenerimaanjtt`
  ADD CONSTRAINT `FK_auditpenerimaanjtt_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditpenerimaanjttcalon`
--
ALTER TABLE `auditpenerimaanjttcalon`
  ADD CONSTRAINT `FK_auditpenerimaanjttcalon_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditpenerimaanlain`
--
ALTER TABLE `auditpenerimaanlain`
  ADD CONSTRAINT `FK_auditpenerimaanlain_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditpengeluaran`
--
ALTER TABLE `auditpengeluaran`
  ADD CONSTRAINT `FK_auditpengeluaran_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD CONSTRAINT `kurikulum_ibfk_1` FOREIGN KEY (`departemen`) REFERENCES `departemen` (`departemen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nap`
--
ALTER TABLE `nap`
  ADD CONSTRAINT `nap_ibfk_1` FOREIGN KEY (`idaturan`) REFERENCES `dasarpenilaian` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE;
