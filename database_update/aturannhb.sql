-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2016 at 09:20 AM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `aturannhb`
--

CREATE TABLE IF NOT EXISTS `aturannhb` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idtingkat` int(10) unsigned NOT NULL,
  `idkurikulum` int(1) unsigned NOT NULL DEFAULT '1',
  `dasarpenilaian` varchar(50) NOT NULL,
  `idjenisujian` int(10) unsigned NOT NULL,
  `bobot` tinyint(3) unsigned NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_aturannhb_jenisujian` (`idjenisujian`),
  KEY `FK_aturannhb_dasarpenilaian` (`dasarpenilaian`),
  KEY `FK_aturannhb_pelajaran` (`idkurikulum`),
  KEY `FK_aturannhb_tingkat` (`idtingkat`),
  KEY `IX_aturannhb_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `aturannhb`
--

INSERT INTO `aturannhb` (`replid`, `idtingkat`, `idkurikulum`, `dasarpenilaian`, `idjenisujian`, `bobot`, `aktif`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(2, 1, 1, 'KOG', 1, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:17', 0, 0),
(4, 2, 2, 'KI1', 3, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:34', 0, 0),
(5, 2, 2, 'KI1', 5, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:34', 0, 0),
(6, 2, 2, 'KI1', 8, 30, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:34', 0, 0),
(7, 2, 1, 'AF', 9, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:04:53', 0, 0),
(9, 1, 1, 'KOG', 2, 20, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:18:19', 0, 0),
(10, 3, 2, 'KI1', 3, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:18:52', 0, 0),
(11, 3, 2, 'KI1', 5, 15, 1, NULL, NULL, NULL, NULL, '2016-04-05 09:18:52', 0, 0);
