-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2016 at 08:52 AM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` varchar(3) NOT NULL DEFAULT '',
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nama`, `idmenu`, `idParent`, `idUrut`, `url`, `img`, `isVisible`, `child`) VALUES
('0', 'PENGATURAN', '1', '0', '1', '../_main/menu_referensi.php', '', '1', '1'),
('1', 'Header Dokumen', '1', '1', '1', 'referensi/identitas.php', '', '1', '0'),
('10', 'Pengajar Mapel', '3', '1', '3', 'guru/guru_main.php', '', '1', '0'),
('11', 'Data Guru/Pegawai', '2', '1', '2', 'referensi/pegawai.php', '', '1', '0'),
('12', 'Jadwal Guru', '2', '1', '3', 'jadwal/jadwal_guru_main.php', '', '1', '0'),
('13', 'Rekapitulasi Jadwal', '2', '1', '4', 'jadwal/rekap_jadwal_main.php', '', '1', '0'),
('14', 'Lap. Presensi Guru', '2', '1', '5', 'presensi/lap_pengajar_main.php', '', '1', '0'),
('15', 'Lap. Rekapitulasi Absensi Guru', '2', '1', '6', 'presensi/lap_refleksi_main.php', '', '1', '0'),
('16', 'PENGAJARAN', '3', '0', '1', '', '', '1', '1'),
('17', 'Jam belajar', '3', '1', '1', 'jadwal/definisi_jam.php', '', '1', '0'),
('18', 'Jadwal Mapel', '3', '1', '4', 'jadwal/jadwal_kelas_main.php', '', '1', '0'),
('19', 'Data Mapel', '3', '1', '2', 'guru/pelajaran.php', '', '1', '0'),
('2', 'Identitas Sekolah', '1', '1', '2', 'referensi/departemen.php', '', '1', '0'),
('20', 'RPP Guru', '3', '1', '5', 'guru/rpp_main.php', '', '1', '0'),
('21', 'RPP Guru Perkelas', '3', '1', '6', 'penilaian/ujian_rpp_siswa.php', '', '1', '0'),
('22', 'PRESENSI PELAJARAN', '4', '0', '1', '', '', '1', '1'),
('23', 'Presensi Pelajaran', '4', '1', '1', 'presensi/presensi_main.php', '', '1', '0'),
('24', 'Cetak Form Presensi Pelajaran', '4', '1', '2', 'presensi/formpresensi_pelajaran.php', '', '1', '0'),
('25', 'Lap. Absensi Perpelajaran Berdasarkan Kelas', '4', '1', '4', 'presensi/lap_kelas_main.php', '', '1', '0'),
('26', 'Lap. Absensi Perpelajaran Berdasarkan Tingkat', '4', '1', '3', 'presensi/statistik_kelas_main.php', '', '1', '0'),
('27', 'Statistik Kehadiran Siswa Per-Pelajaran', '4', '1', '5', 'presensi/statistik_hariansiswa_main.php', '', '1', '1'),
('28', 'PRESENSI HARIAN', '5', '0', '1', '', '', '1', '1'),
('29', 'Presensi Harian', '5', '1', '1', 'presensi/input_presensi_main.php', '', '1', '0'),
('3', 'Tahun Ajaran', '1', '1', '3', 'referensi/tahunajaran.php', '', '1', '0'),
('30', 'Cetak Form Presensi Harian', '5', '1', '2', 'presensi/formpresensi_harian.php', '', '1', '0'),
('31', 'Lap. Presensi Harian Siswa', '5', '1', '5', 'presensi/lap_hariansiswa_main.php', '', '1', '0'),
('32', 'Lap. Presensi Harian Siswa PerKelas', '5', '1', '3', 'presensi/lap_hariankelas_main.php', '', '1', '0'),
('33', 'Lap. Harian Absensi Siswa Yang Tidak Hadir', '5', '1', '4', 'presensi/lap_harianabsen_main.php', '', '1', '0'),
('34', 'Statistik Kehadiran Siswa', '5', '1', '7', 'presensi/statistik_hariansiswa_main.php', '', '1', '0'),
('35', 'Statistik Kehadiran Siswa PerKelas', '5', '1', '6', 'presensi/statistik_hariankelas_main.php', '', '1', '0'),
('36', 'PENILAIAN', '6', '0', '1', '', '', '1', '1'),
('37', 'Aspek Penilaian', '6', '1', '1', 'guru/aspeknilai.php', '', '1', '0'),
('38', 'Jenis Evaluasi', '6', '1', '2', 'guru/jenis_pengujian.php', '', '1', '0'),
('39', 'Aturan Grading', '6', '1', '3', 'guru/aturannilai_main.php', '', '1', '0'),
('4', 'Semester', '1', '1', '4', 'referensi/semester.php', '', '1', '0'),
('40', 'Bobot Evaluasi', '6', '1', '4', 'guru/perhitungan_rapor.php', '', '1', '0'),
('41', 'Penilaian Pelajaran', '6', '1', '6', 'penilaian/lihat_nilai_pelajaran.php', '', '1', '0'),
('44', 'Cetak Form Penilaian', '6', '1', '7', 'penilaian/formpenilaian.php', '', '1', '0'),
('45', 'DAFTAR NILAI', '7', '0', '1', '', '', '1', '1'),
('46', 'Daftar Nilai Siswa Perpelajaran', '7', '1', '1', 'penilaian/lap_pelajaran_main.php', '', '1', '0'),
('47', 'Laporan Akhir Hasil Belajar Setiap Siswa', '7', '1', '3', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('48', 'Rata-rata Nilai Setiap', '7', '1', '4', 'penilaian/rataus.main.php', '', '1', '0'),
('49', 'Audit Perubahan Nilai', '7', '1', '5', 'referensi/auditnilai.php', '', '1', '0'),
('5', 'Tingkat', '1', '1', '5', 'referensi/tingkat.php', '', '1', '0'),
('50', 'Legger Nilai', '7', '1', '2', 'penilaian/lap_legger.php', '', '1', '0'),
('51', 'RAPOR', '8', '0', '1', '', '', '1', '1'),
('52', 'Penentuan Nilai Rapor', '8', '1', '2', 'penilaian/lihat_penentuan.php', '', '1', '0'),
('53', 'Komentar Rapor', '8', '1', '2', 'penilaian/komentar_main.php', '', '1', '0'),
('54', 'Nilai Rapor Setiap Siswa', '8', '1', '3', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('55', 'Laporan Rapor', '8', '1', '4', 'penilaian/lap_rapor_main.php', '', '1', '0'),
('56', 'Pindah/Naik Kelas', '8', '1', '5', 'siswa/siswa_pindah_main.php', '', '1', '0'),
('57', 'USER', '9', '0', '1', 'usermenu.php', '', '1', '1'),
('58', 'Daftar Pengguna', '9', '1', '1', 'user/user.php', '', '1', '0'),
('59', 'Ganti Password', '9', '1', '2', 'user/user_ganti.php', '', '1', '0'),
('6', 'Angkatan', '1', '1', '6', 'referensi/angkatan.php', '', '1', '0'),
('60', 'Logout', '9', '1', '3', 'logout.php', '', '1', '0'),
('61', 'KENAIKAN & KELULUSAN', '8', '0', '1', 'kelulusan.php', '', '1', '0'),
('62', 'BUKU INDUK', '8', '0', '1', 'penilaian/lap_buku_induk_main.php', NULL, '1', '0'),
('63', 'Master Sikap', '6', '1', '5', 'guru/jenis_sikap.php', '', '1', '0'),
('64', 'Penetuan Sikap', '8', '1', '1', 'penilaian/lihat_penentuan_sikap.php', '', '1', '0'),
('7', 'Kelas', '1', '1', '7', 'referensi/kelas.php', '', '1', '0'),
('8', 'GURU/PEGAWAI', '2', '0', '1', '', '', '1', '1'),
('9', 'Status Guru', '2', '1', '1', 'guru/statusguru.php', '', '1', '0');
