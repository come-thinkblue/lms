-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2016 at 07:35 AM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_sikap`
--

CREATE TABLE IF NOT EXISTS `master_sikap` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_sikap` varchar(50) NOT NULL,
  `idkurikulum` int(1) unsigned NOT NULL DEFAULT '1',
  `iddasarpenilaian` varchar(10) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned DEFAULT '0',
  `issync` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jenisujian_pelajaran` (`idkurikulum`),
  KEY `IX_jenisujian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `master_sikap`
--

INSERT INTO `master_sikap` (`replid`, `nama_sikap`, `idkurikulum`, `iddasarpenilaian`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(14, 'Deskripsi Sosial 1', 2, 'KI2', 'Sudah menunjukkan kebiasaan berdoa sebelum dan sesudah melaksanakan kegiatan, menjalankan ibadah tepat waktu, memberi salam, bersyukur, menghormati orang lain dalam menjalankan ibadah, dan menjaga kebersihan lingkungan sekolah', '', '', '', '2016-04-23 16:31:04', 0, 0),
(15, 'Deskripsi Spiritual 1', 2, 'KI1', 'Sudah menunjukkan sikap jujur, disiplin, tanggung jawab, santun, peduli, dan percaya diri', '', '', '', '2016-04-23 16:31:04', 0, 0),
(16, 'Deskripsi Afektif', 1, 'AF', 'Isi Deskripsi', NULL, NULL, NULL, '2016-04-23 16:35:21', 0, 0),
(17, 'Deskripsi Spiritual 2', 2, 'KI1', 'Sudah menunjkkan', NULL, NULL, NULL, '2016-04-25 18:00:52', 0, 0),
(18, 'Deskripsi Spiritual 3', 2, 'KI1', 'Coba Deskripsi', NULL, NULL, NULL, '2016-04-25 18:57:55', 0, 0);
