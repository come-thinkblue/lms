-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2016 at 05:05 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE IF NOT EXISTS `tingkat` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(50) NOT NULL,
  `idkurikulum` int(10) unsigned NOT NULL DEFAULT '1',
  `departemen` varchar(50) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `urutan` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_tingkat_departemen` (`departemen`),
  KEY `IX_tingkat_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`replid`, `tingkat`, `idkurikulum`, `departemen`, `aktif`, `keterangan`, `urutan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(1, 'X', 1, 'SMAN 1 Malang', 1, 'Sekedar contoh. Nama tingkatan kelas yang ada di sekolah. Ubah atau tambahkan data ini sesuai dengan nama tingkatan kelas di sekolah.', 1, NULL, NULL, NULL, '2013-02-03 18:55:17', 6098, 0),
(2, 'XI', 1, 'SMAN 1 Malang', 1, '', 1, NULL, NULL, NULL, '2013-02-03 18:55:23', 28378, 0),
(3, 'XII', 2, 'SMAN 1 Malang', 1, 'Edit', 3, NULL, NULL, NULL, '2013-02-03 18:55:30', 58064, 0);
