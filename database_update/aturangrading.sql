-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2016 at 05:07 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `aturangrading`
--

CREATE TABLE IF NOT EXISTS `aturangrading` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idtingkat` int(10) unsigned NOT NULL,
  `dasarpenilaian` varchar(50) NOT NULL,
  `nmin` decimal(6,1) NOT NULL,
  `nmax` decimal(6,1) NOT NULL,
  `grade` varchar(2) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_aturangrading_tingkat` (`idtingkat`),
  KEY `FK_aturangrading_dasarpenilaian` (`dasarpenilaian`),
  KEY `IX_aturangrading_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `aturangrading`
--

INSERT INTO `aturangrading` (`replid`, `idtingkat`, `dasarpenilaian`, `nmin`, `nmax`, `grade`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`) VALUES
(21, 1, 'AF', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(22, 1, 'AF', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(23, 1, 'AF', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(24, 1, 'AF', '55.0', '64.0', 'D', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(25, 1, 'KOG', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(26, 1, 'KOG', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(27, 1, 'KOG', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:30:06', 0, 0),
(31, 2, 'AF', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(32, 2, 'AF', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(33, 2, 'AF', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(34, 2, 'AF', '55.0', '64.0', 'D', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(35, 2, 'KOG', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(36, 2, 'KOG', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(37, 2, 'KOG', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(38, 2, 'PSI', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(39, 2, 'PSI', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(40, 2, 'PSI', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:07', 0, 0),
(41, 3, 'KI1', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(42, 3, 'KI1', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(43, 3, 'KI1', '55.0', '64.0', 'D', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(44, 3, 'KI1', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(45, 3, 'KI2', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(46, 3, 'KI2', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(47, 3, 'KI2', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(48, 3, 'KI3', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(49, 3, 'KI3', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(50, 3, 'KI3', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(51, 3, 'KI4', '85.0', '100.0', 'A', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(52, 3, 'KI4', '75.0', '84.0', 'B', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(53, 3, 'KI4', '65.0', '74.0', 'C', NULL, NULL, NULL, '2016-04-03 23:33:15', 0, 0),
(54, 1, 'PSI', '81.0', '90.0', 'A', NULL, NULL, NULL, '2016-04-03 23:53:00', 0, 0),
(55, 1, 'PSI', '71.0', '80.0', 'B', NULL, NULL, NULL, '2016-04-03 23:53:00', 0, 0),
(61, 1, 'PSI', '61.0', '70.0', 'C', NULL, NULL, NULL, '2016-04-04 00:53:41', 0, 0),
(62, 1, 'PSI', '51.0', '60.0', 'D', NULL, NULL, NULL, '2016-04-04 00:53:41', 0, 0);
