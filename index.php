<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016

 * 
 * **[N]**/ 
 
 	//$link = $_GET["link"];
	//if ($link=="") $link="akademik";	
	//$link=$_GET["link"];
	//!empty($link) ? $link:"akademik";
	$link = isset($_GET['link']) ? $_GET['link'] : 'akademik';
	
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
e-Madani MAN Kota Blitar
</title>
<link href="images/man.ico" rel="shortcut icon" />
<link href="akademik/css/button.css" rel="stylesheet" type="text/css" />
<link href="akademik/css/zice.style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function link_go(){
if (document.link_form.link_sel.options[document.link_form.link_sel.selectedIndex].value != "none") {
location = document.link_form.link_sel.options[document.link_form.link_sel.selectedIndex].value
}}
</script>

<style type="text/css">
html,body {
	background-image: -ms-radial-gradient(center top, circle closest-corner, #B4E85A 0%, #2C500B 100%);
    background-image: -moz-radial-gradient(center top, circle closest-corner, #B4E85A 0%, #2C500B 100%);
    background-image: -o-radial-gradient(center top, circle closest-corner, #B4E85A 0%, #2C500B 100%);
    background-image: -webkit-gradient(radial, center top, 0, center top, 487, color-stop(0, #B4E85A), color-stop(1, #2C500B));
    background-image: -webkit-radial-gradient(center top, circle closest-corner, #B4E85A 0%, #2C500B 100%);
    background-image: radial-gradient(circle closest-corner at center top, #B4E85A 0%, #2C500B 100%);
	 -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
	height:100%;
	padding:0
	margin:0;
	 background-repeat:no-repeat;
}

.logoLMS
{
	width:429px;
}
#versionBar {
    height: 50px;
    background-color: #1587E4;
}
.wadahBesar {
    width: 100%;
    height: 500px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 61px;
    position: absolute;
}

/* The CSS */
select {
    padding:3px;
    margin: 0;
	width:230px;
	height:30px;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    background: #f8f8f8;
    color:#888;
    border:#999999 solid 1px;
    outline:none;
    display: inline-block;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
}
</style>
</head>
<body> 
<div class="wadahBesar" >
<div class="logoLMS"></div>
<div id="login">
<div class="ribbon"></div>
  <div class="inner">
  
  <div  class="logo" ><img src="<?php echo $link ?>/images/logoLogin.png" alt="ziceAdmin" /></div>
<div class="userbox"></div>
<div class="formLogin">
   <form method="post" name="form" id="form" action="<?php echo $link ?>/redirect.php" onSubmit="return cek_form()">
          <div class="tip">
          <input name="username" id="username" type="text"   />
          </div>
          <div class="tip">
           <input name="password" id="password" type="password"   />
          </div>
              <div style="float:right;padding:2px 0px ; margin-top:20px;">
              <div> 
                <ul class="uibutton-group">
                 <input type="submit"  class="uibutton normal" value="LOGIN" />
                 </form>
               </ul>
              </div>
            </div>
           
            <div style="float:left; margin-top:12px;">
               <form name="link_form" class="formEl_b">
              <select  name="link_sel"  onchange="link_go()" >
             
			  <option <?php if ($link=="akademik"){echo "selected";}?> value="index.php?link=akademik">Kurikulum</option>
			    <option <?php if ($link=="infoguru"){echo "selected";}?> value="index.php?link=infoguru">Guru</option>
              <option <?php if ($link=="infosiswa"){echo "selected";}?> value="index.php?link=infosiswa">Siswa</option>
			    <option <?php if ($link=="kesiswaan"){echo "selected";}?> value="index.php?link=kesiswaan">Kesiswaan</option>
			  <option <?php if ($link=="ema"){echo "selected";}?> value="index.php?link=ema">Kepala Sekolah</option>			  
			  <option <?php if ($link=="kepegawaian"){echo "selected";}?> value="index.php?link=kepegawaian">Kepegawaian</option>
            
			   <option <?php if ($link=="keuangan"){echo "selected";}?> value="index.php?link=keuangan">Keuangan</option>
			  <option <?php if ($link=="sarpras"){echo "selected";}?> value="index.php?link=aset">Sarana dan Prasarana</option>           
              <option <?php if ($link=="keuangan"){echo "selected";}?> value="index.php?link=keuangan">Keuangan</option>
             
			  <option <?php if ($link=="perpustakaan"){echo "selected";}?> value="index.php?link=perpustakaan">Perpustakaan</option>              
			  <option <?php if ($link=="timtatib"){echo "selected";}?> value="index.php?link=tatib">Tatib</option>
            
              </select>
              </form>
              </div>

    
      
  </div>

<div id="versionBar" >
  <div class="copyright" > &copy; Copyright 2013  All Rights Reserved   <span class="tip"><a  href="#" title="Zice Admin" >  MAN Kota Blitar</a> </span> </div>
  <div class="shadow"></div>
  
  </div>
  <!-- // copyright-->
<script type="text/javascript" src="akademik/js/jquery.min.js"></script>
<script type="text/javascript" src="akademik/js/jquery-jrumble.js"></script>
<script type="text/javascript" src="akademik/js/jquery.ui.min.js"></script>     
<script type="text/javascript" src="akademik/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="akademik/js/iphone.check.js"></script>
<script type="text/javascript" src="akademik/js/login.js"></script>

</body>
</html>