<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/common.php');
require_once('../inc/db_functions.php');
require_once('statistik.pinjam.class.php');
OpenDb();
$S = new CStat();
$S->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../scr/tables.js"></script>
<script type="text/javascript" src="../scr/tools.js"></script>
<script type="text/javascript" src="statistik.pinjam.js"></script>
</head>

<body>
	<div id="waitBox" style="position:absolute; visibility:hidden;">
		<img src="../img/loading2.gif" border="0" />&nbsp;<span class="tab2">Please&nbsp;wait...</span>
	</div>
	<div id="title" align="right">
        <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
        <font style="font-size:18px; color:#999999">Statistik Peminjaman</font><br />
        <a href="peminjaman.php" class="welc">Peminjaman</a><span class="welc"> > Statistik Peminjaman Pustaka</span><br /><br /><br />
    </div>
    <div id="content">
      <?php echo$S->Content()?>
    </div>
</body>
<?php echo$S->OnFinish()?>
</html>
<?php CloseDb(); ?>