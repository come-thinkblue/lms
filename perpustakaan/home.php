<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php

include('../charts/FusionCharts.php');	

require_once('inc/errorhandler.php');
require_once('inc/db_functions.php');
require_once('inc/sessioninfo.php');
//require_once('inc/common.php');
require_once('inc/config.php');
//require_once('cek.php');

OpenDb();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled-1</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="script/tooltips.js"></script>
<script type="text/javascript">
function get_fresh(){
	document.location.reload();
}

</script>
</head>
<?php
include ('menu.php');

?>
<br /> <br /> <br /> <br /> <br />
<body >
<!-- ImageReady Slices (Untitled-1) -->
<div id="content"> 
<div class="wrapper">





<?php 
$bulan = array ("kosong","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","Sepetember","Oktober","Nopember","Desember");
$que11="SELECT tglmulai FROM dbakademik.tahunajaran WHERE aktif='1'";
$result11=mysql_query($que11);
$row11 = mysql_fetch_array($result11);

$que1="SELECT count(*) as jumlah, MONTH(p.tglpinjam) as bulan,YEAR(p.tglpinjam) as tahun FROM pinjam p, daftarpustaka d, pustaka pu WHERE p.tglpinjam BETWEEN '".$row11."' AND  CURDATE() AND d.kodepustaka=p.kodepustaka GROUP BY MONTH(p.tglpinjam),YEAR(p.tglpinjam) ORDER BY p.tglpinjam ASC";
$result1=mysql_query($que1);
//$row1 = mysql_fetch_array($result1);

$data ="<chart showvalues='1' caption=''  canvasBorderColor='1D8BD1' canvasBorderAlpha='60'
 showlegend='1' enablesmartlabels='1' showlabels='0'>";
 



				$no1=0;
while ($row1 = mysql_fetch_array($result1)) {
 $nilaiBulan = $bulan[$row1['bulan']];
					$data .= "<set label='" . $nilaiBulan."' value='" . $row1['jumlah'] . "' />";
					$no1++;
				}
				$data.="</chart>";
              
?>
        <div class="fluid">
            <div class="widget gridWelcome">
              
                <div class="body">
                <h1 style="color:#128f97; font-weight:800;">Selamat Datang</h1>
                <h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;">Administrator Perpustakaan </h1>
				<h3 style="color:#81bdba; font-weight:500; margin-bottom:5px;">MAN Kota Blitar</h1>
                <p>Sebelum menggunakan sistem ini, silahkan anda melakukan setting pada data master berikut.</p></div>
                
        <ul class="middleNavA">
            <li><a href="ref/pustaka.php" title="Pegawai"><img src="css/icon bawah/homefinal.jpg" alt="" /><span>Perpustakaan</span></a></li>
            <li><a href="ref/format.php" title="Setting Tahun Ajaran"><img src="css/icon bawah/format.png" alt="" /><span>Format</span></a></li>
            <li><a href="ref/penerbit.php" title="Setting Tingkat"><img src="css/icon bawah/publisher.png" alt="" /><span>Penerbit</span></a></li>
            <li><a href="ref/rak.php" title="Setting Angkatan"><img src="css/icon bawah/rak.png" alt="" /><span>Rak</span></a></li>
            <li><a href="ref/katalog.php" title="Setting Kelas"><img src="css/icon bawah/katalog.png" alt="" /><span>Katalog</span></a></li>
            <li><a href="ref/penulis.php" title="Setting Semester"><img src="css/icon bawah/penulis.png" alt="" /><span>Penulis</span></a></li>
        </ul>
            </div>
            <div class="widget grid5">
                <div class="whead"><h6>Grafik Data Peminjaman Buku Perpustakaan MAN Kota Blitar</h6><div class="clear"></div></div>
                <div class="body">
                <?php echo renderChartHTML("../charts/Column3D.swf", "", $data, "tes", "100%", "300", false);?>
                </div>
            </div>
            
    <?php
function jumlahData($count, $from){
$quePeg="SELECT Count(".$count.") as jumlah FROM ".$from."";
$resultPeg=mysql_query($quePeg);
$jumPeg=mysql_fetch_array($resultPeg);
$jumlah= $jumPeg['jumlah'];
return $jumlah;
}
	?> 
    
    
 <?php
function jmlKelas($x)
{
$kelas = $x;
$queryJml= "SELECT sum(jumlah) as jml from (SELECT
Count(siswa.nis)as jumlah, kelas.kelas FROM
siswa
Inner Join kelas ON siswa.idkelas = kelas.replid
WHERE
kelas.kelas REGEXP '$kelas'
GROUP BY
siswa.idkelas) as total";

$resultJml=mysql_query($queryJml);
$jumSis=mysql_fetch_array($resultJml);
$jumlah= $jumSis['jml'];
return $jumlah;

}
?>
          
            
            <div class="widget grid3">
                <div class="whead"><h6>Tabel Data</h6><div class="clear"></div></div>
                <div class="body">
                <table cellpadding="0" cellspacing="0" width="100%" class="tAlt wGeneral">
                        <thead>
                            <tr>
                                <td width="60%">Jenis </td>
                                <td>Jumlah Data</td>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td ><a href="images/big.png" title="" class="lightbox"><img src="css/icon atas/pengembalian.png" width="40" height="40"alt="" /><span>Jumlah Pustaka</span></a></td>
                               <?php $jumlahPegawai = jumlahData("pustaka.replid","$g_db_perpustakaan.pustaka"); ?>  <td align="center"><strong class="income"><?php echo $jumlahPegawai;?></strong></td>
                            </tr>
							              <tr>
								<?php $jumlah = jumlahData("replid","daftarpustaka"); ?> <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon atas/pustakabaru.png" width="40" height="40"alt="" /><span>Jumlah Daftar Pustaka</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?></strong></td>
                               
                            </tr>
                            <tr>
                              <?php $jumlahTahunAjaran = jumlahData("pinjam.replid","$g_db_perpustakaan.pinjam where status='1'"); ?>    <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/buku.jpg" width="40" height="40"alt="" /><span>Jumlah Buku Terpinjam</span></a></td>
                                <td align="center"><strong class="income"><?php echo $jumlahTahunAjaran;?></strong></td>
                                
                            </tr>
                            <tr>
							<?php $jumlah = jumlahData("anggota.replid","$g_db_perpustakaan.anggota"); ?> <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/anggota.jpg" width="40" height="40"alt="" /><span>Jumlah Anggota</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?> </strong></td>
                               
                            </tr>
                            <tr>
<?php $jumlah = jumlahData("replid","penerbit"); ?> <td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/publisher.png" width="40" height="40"alt="" /><span>Jumlah Penerbit</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?></strong></td>
                               
                            </tr>
                            
                             <tr>
<?php $jumlah = jumlahData("replid","penulis"); ?><td><a href="images/big.png" title="" class="lightbox"><img src="css/icon bawah/penulis.png" width="40" height="40"alt="" /><span>Jumlah Penulis</span></a></td>
                                 <td align="center"><strong class="income"><?php echo $jumlah;?></strong></td>
                               
                            </tr>
                            
               
                        </tbody>
                    </table>            
                
                        
                
                </div>
            </div>
        </div>


</div>


</div>
<!-- End ImageReady Slices -->
</body>
</html>