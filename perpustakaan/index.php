<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016

 *
 * **[N]**/ ?>
<?php
session_name("dbperpustakaan");
session_start();

if (!isset($_SESSION['login']))
{ 
	include('login.php');
	exit;
}  
?>
<link href="css/favicon.ico" rel="shortcut icon" />
<title>Perpustakaan MAN Kota Blitar</title>
<script language="javascript">
function backToMenu(i) {
	var addr;
	switch (i) {
		case '0' : 
			addr = "../../referensi/referensi.php";
			break;
		case '1' :
			addr = "../../transaksi/transaksi.php";
			break;
	}
	content.location.href = addr;
}
</script>
<frameset rows="20%,*,31" frameborder="no" border="0" framespacing="0">
	<frame name="frameatas" src="frameatas.php" scrolling="no" noresize="noresize" style="overflow:hidden" />
    <frameset >
    	
        <frame name="content" src="home.php"/>
        
    </frameset>
	<frame name="framebawah" src="framebawah.php" scrolling="no" noresize="noresize" style="overflow:hidden" />
</frameset><noframes></noframes>