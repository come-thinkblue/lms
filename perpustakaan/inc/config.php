<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php

// ------------------------------------------------------------
// PATCH MANAGEMENT FRAMEWORK                                  
// ------------------------------------------------------------

if (file_exists('../include/global.patch.manager.php'))
{
	require_once('../include/global.patch.manager.php');
	ApplyGlobalPatch("..");	
}
elseif (file_exists('../../include/global.patch.manager.php'))
{
	require_once('../../include/global.patch.manager.php');
	ApplyGlobalPatch("../..");
}
elseif (file_exists('../../../include/global.patch.manager.php'))
{
	require_once('../../../include/global.patch.manager.php');
	ApplyGlobalPatch("../../..");
}

require_once('module.patch.manager.php');
ApplyModulePatch();

// ------------------------------------------------------------
// MAIN CONFIGURATION                                          
// ------------------------------------------------------------

if (file_exists('../include/mainconfig.php'))
{
	require_once('../include/mainconfig.php');
}
elseif (file_exists('../../include/mainconfig.php'))
{
	require_once('../../include/mainconfig.php');
}
elseif (file_exists('../../../include/mainconfig.php'))
{
	require_once('../../../include/mainconfig.php');
}

// ------------------------------------------------------------
// DEFAULT CONSTANTS                                           
// ------------------------------------------------------------

$db_name = "dbperpustakaan";
$db_name_umum = "dbakademik";
$db_name_user = $g_db_user;
$db_name_sdm = "dbpegawai";
$db_name_akad = "dbakademik";
$db_name_fina = "dbkeuangan";

$simaka_full_url = "http://$G_SERVER_ADDR/akademik/";
$full_url = "http://$G_SERVER_ADDR/perpustakaan/";

$G_ENABLE_QUERY_ERROR_LOG = false;

function get_db_name($name)
{
	global $db_name_umum;
	global $db_name_user;
	global $db_name_sdm;
	global $db_name_akad;
	global $db_name_fina;
	
	if ($name == 'user')
		return $db_name_user;
	if ($name == 'sdm')
		return $db_name_sdm;
	if ($name == 'akad')
		return $db_name_akad;
	if ($name == 'fina')
		return $db_name_fina;
	if ($name == 'umum')
		return $db_name_umum;
}
?>