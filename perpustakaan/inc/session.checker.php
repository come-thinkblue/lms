<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("dbperpustakaan");
session_start();

if (!isset($_SESSION['login']))
{ 
   if (file_exists("index.php")) 
	  $addr = "";
   elseif (file_exists("../index.php")) 
	  $addr = "../";
   elseif(file_exists("../../index.php")) 
	  $addr = "../../";
   else	
	  $addr = "../../../";
   ?>  
   <script language="javascript">
	  if (self != self.top)
	  {
		  top.window.location.href='<?php echo$addr?>';
	  }
	  else if(self.name!="")
	  {
		  opener.top.window.location.href='<?php echo$addr?>';
		  window.close();
	  }	
	  else
	  {
		  window.location.href='<?php echo$addr?>';	
	  }
   </script>
<?php	}  ?>