<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("dbperpustakaan");
session_start();

function SI_USER_NAME()
{
	return $_SESSION['nama'];
}

function SI_USER_ID()
{
	return $_SESSION['login'];
}

function SI_USER_LEVEL()
{
	return ($_SESSION['tingkat']);
}

function SI_USER_DEPT()
{
	if ($_SESSION['tingkat'] == 2)
		return ($_SESSION['perpustakaan']);	
	else
		return ('ALL');
}

function SI_USER_IDPERPUS()
{
	if ($_SESSION['tingkat'] == 2)
		return ($_SESSION['idperpustakaan']);	
	else
		return ('ALL');
}

function IsAdmin()
{
	if ($_SESSION['tingkat'] == 0 || $_SESSION['tingkat'] == 1) 
		return true;
	else 
		return false;
}
?>