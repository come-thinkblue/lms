<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once("../inc/session.checker.php");
require_once("../inc/config.php");
require_once("../inc/db_functions.php");
require_once("../inc/common.php");//imageresizer.php
require_once("../inc/imageresizer.php");
require_once("../inc/fileinfo.php");
OpenDb();
$op = $_REQUEST[op];
$perpustakaan = $_REQUEST[perpustakaan]; 
$logo = $_FILES['filegambar'];
$uploadedfile = $logo['tmp_name'];
$uploadedfile_name = $logo['name'];
$OL='';
$dis='';
if (strlen($uploadedfile)!=0){
	$filename='temp'.GetFileExt($uploadedfile_name);
	ResizeImage($logo, 100, 80, 70, $filename);
	$handle = fopen($filename, "r");
	$fn = $filename;
	$OL = "1";
	$dis = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pengaturan Logo Header</title>
<script language="javascript" src="../scr/ajax.js"></script>
<script language="javascript">
function ShowWait(id){
	var x = document.getElementById('WaitBox').innerHTML;
	document.getElementById(id).innerHTML = x;
}
function Load(){
	var perpustakaan = document.getElementById('perpustakaan').value;
	var op = document.getElementById('op').value;
	document.getElementById('notification').value=1;
	ShowWait('ImageArea');
	sendRequestText("GetImage.php",ShowImage,"perpustakaan="+perpustakaan+"&op="+op);
}
function ShowImage(x){
	document.getElementById('ImageArea').innerHTML = x;
}
function SaveSuccess(){
	parent.opener.Fresh();
	window.close();
}
function Close(){
	var x = document.getElementById('notification').value;
	if (x.length>0){
		if (confirm('Gambar belum disimpan!\nAnda yakin akan menutup jendela ini?'))
			window.close();
	} else {
			window.close();
	}
}
function SimpanGambar(){
	var fn = '<?php echo$fn?>';
	var x  = document.getElementById('perpustakaan').value;
	if (fn!='')
		parent.HiddenFrame.location.href='CopyTmpToFix.php?op=CopyTempImage&perpustakaan='+x+'&filename='+fn;
	else
		alert ('Silakan Browse gambar terlebih dahulu!');
}
function formSubmit(){
	var file = document.getElementById("filegambar").value;
	if (file.length>0){
		var ext = "";
		var i = 0;
		var string4split='.';

		z = file.split(string4split);
		ext = z[z.length-1];
		
		if (ext!='JPG' && ext!='jpg' && ext!='Jpg' && ext!='JPg' && ext!='JPEG' && ext!='jpeg'){
			alert ('Format Gambar harus ber-extensi jpg atau JPG !');
			//document.getElementById("foto").value='';
			//document.form1.foto.focus();
    		//document.form1.foto.select();
			return false;
		} 
	}
	document.getElementById('FrmLogo').submit();
}
</script>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
</head>
<body >
<div id="WaitBox" style="position:absolute; visibility:hidden">
	<img src="../img/loading.gif" />
</div>
<div id="title" align="right">
	<font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
	<font style="font-size:18px; color:#999999">Logo Header</font><br />
</div>
<form id="FrmLogo" action="AddLogo.php" method="post" enctype="multipart/form-data" >
<input type="hidden" name="op" id="op" value="<?php echo$op?>" />
<input type="hidden" id="notification" value="<?php echo$OL?>" />
<input type="hidden" name="perpustakaan" id="perpustakaan" value="<?php echo$perpustakaan?>" />
<table width="100%" border="0">
  <tr>
    <td align="center" id="ImageArea" height="150">
    	<?php
		if ($op=='Edit'){
			$sql = "SELECT replid FROM ".$db_name_umum.".identitas WHERE status='1' AND perpustakaan='$perpustakaan'";
			$result = QueryDb($sql);
			$row  =@mysql_fetch_array($result);
			echo "<img src='../lib/gambar.php?replid=".$row[replid]."&table=".$db_name_umum.".identitas&field=foto'><br>Logo Lama";
		}
		?>
    </td>
    </tr>
  <tr>
    <td>File Gambar : <input type="file" id="filegambar" name="filegambar" onchange="formSubmit()" /></td>
    </tr>
  <tr>
    <td align="center">
    <input name="Simpan2" type="submit" value="Simpan2" class="btnfrm2" style="display:none" />
    <input name="Simpan" <?php echo$dis?> type="button" value="Simpan" class="btnfrm2" onclick="SimpanGambar()" />
    &nbsp;&nbsp;<input type="button" value="Batal" onClick="Close()" class="btnfrm2" /></td>
  </tr>
</table>
</form>
<?php if ($OL!=""){ ?>
<script language="javascript">
	Load('<?php echo$fn?>');
</script>
<?php } ?>
<iframe name="HiddenFrame" style="display:none"></iframe>
</body>
</html>