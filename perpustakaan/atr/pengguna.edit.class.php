<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
class CPenggunaAdd{
	function OnStart(){
		$this->nip="";
		if (isset($_REQUEST[nip]))
			$this->nip=$_REQUEST[nip];
		$this->nama="";
		if (isset($_REQUEST[nama]))
			$this->nama=$_REQUEST[nama];
		//echo "Tkt=".$this->tingkat;		
		if (isset($_REQUEST[simpan])){
			if ($_REQUEST[tingkat]=='1')
				$sql = "UPDATE ".get_db_name('user').".hakakses SET tingkat='$_REQUEST[tingkat]',departemen=NULL WHERE login='$this->nip' AND modul='SIMTAKA'";
			else
				$sql = "UPDATE ".get_db_name('user').".hakakses SET tingkat='$_REQUEST[tingkat]',departemen='$_REQUEST[perpustakaan]' WHERE login='$this->nip' AND modul='SIMTAKA'";
			//echo $sql;		
			$result = QueryDb($sql);
			if ($result)
			{
				$this->success();
			} else {
				$this->success();
			}		
		} else {
			$sql = "SELECT * FROM ".get_db_name('user').".hakakses WHERE login='$this->nip' AND modul='SIMTAKA' ";
			$result = QueryDb($sql);
			$row = @mysql_fetch_array($result);
			$this->keterangan=$row[keterangan];
			$this->nip=$row[login];
			$this->tingkat=$row[tingkat];
			if (isset($_REQUEST[tingkat]))
				$this->tingkat=$_REQUEST[tingkat];	
			$this->perpustakaan=$row[departemen];
			$sql = "SELECT * FROM ".get_db_name('sdm').".pegawai WHERE nip='$this->nip'";
			//$echo $$this->tingkat;
			$result = QueryDb($sql);
			$row = @mysql_fetch_array($result);
			$this->nama=$row[nama];

		}
		
	}
	function exist(){
		?>
        <script language="javascript">
			alert('Kode sudah digunakan!');
			document.location.href="format.add.php";
		</script>
        <?php
	}
	function success(){
		?>
        <script language="javascript">
			parent.opener.getfresh();
			window.close();
        </script>
        <?php
	}
	function add(){
		
		?>
        <link href="../sty/style.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
		<!--
		.style1 {color: #FF9900}
		-->
        </style>
        
        <form enctype="multipart/form-data" name="addpengguna" action="pengguna.edit.php" onSubmit="return validate()" method="post">
		<table width="100%" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td colspan="2" align="left">
            	<font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
        		<font style="font-size:18px; color:#999999">Ubah Pengguna</font></td>
  		  </tr>
          <tr>
            <td width="7%">&nbsp;<strong>Pegawai</strong></td>
            <td width="93%"><input name="nip" type="text" class="cmbfrm2" id="nip" size="10" readonly="readonly" value="<?php echo$this->nip?>">&nbsp;<input name="nama" type="text" class="cmbfrm2" id="nama" size="35" readonly="readonly" value="<?php echo$this->nama?>"></td>
          </tr>
          <tr>
            <td>&nbsp;Tingkat</td>
            <td>
            	<select name="tingkat" id="tingkat" onchange="ChgTkt(1)">
                	<option value="1" <?php echo StringIsSelected('1',$this->tingkat)?> >Manajer Perpustakaan</option>
                    <option value="2" <?php echo StringIsSelected('2',$this->tingkat)?> >Staff Perpustakaan</option>
                </select>
            </td>
          </tr>
          <tr>
            <td>&nbsp;Perpustakaan</td>
            <td>
            	<select name="perpustakaan" id="perpustakaan">
                	<?php if ($this->tingkat=='1' || $this->tingkat=='') { ?>
                	<option value="-1" >Semua Perpustakaan</option>
                	<?php } else { ?>
                    <?php	$sql = "SELECT * FROM perpustakaan ORDER BY replid"; ?>
                    <?php	$result = QueryDb($sql); ?>
					<?php  while ($row = @mysql_fetch_array($result)){ ?>
                    	<option value="<?php echo$row[nama]?>" <?php echo StringIsSelected($row[nama],$this->perpustakaan)?>><?php echo$row[nama]?></option>
					<?php	} ?>	
					<?php } ?>
                </select>
            </td>
          </tr>
          <tr>
            <td>&nbsp;Keterangan</td>
            <td><textarea name="keterangan" cols="45" rows="5" class="areatxt" id="keterangan"><?php echo$this->keterangan?></textarea></td>
          </tr>
          <tr>
            <td colspan="2" align="center"><input type="submit" class="cmbfrm2" name="simpan" value="Simpan" >&nbsp;<input type="button" class="cmbfrm2" name="batal" value="Batal" onClick="window.close()" ></td>
          </tr>
        </table>
		</form>
		<?php
	}
	function get_noreg(){
		return "ANG".date(YmdHis);
	}
}
?>