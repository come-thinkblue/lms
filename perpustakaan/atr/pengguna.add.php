<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/common.php');
require_once('../inc/fileinfo.php');
require_once('../inc/imageresizer.php');
require_once('../inc/db_functions.php');
require_once('pengguna.add.class.php');
OpenDb();
$PA = new CPenggunaAdd();
$PA->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tambah Pengguna</title>
<script type="text/javascript" src="../scr/tools.js"></script>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="pengguna.js"></script>
</head>

<body style="margin-top:0px; margin-left:0px">
<?php echo$PA->add()?>
</body>
</html>