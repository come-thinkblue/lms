<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/sessioninfo.php');
require_once('../inc/common.php');
require_once('../inc/config.php');
require_once('../inc/db_functions.php');
require_once('../lib/GetHeaderCetak.php');
OpenDb();
$departemen='yayasan';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../sty/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Perpustakaan MAN Kota Blitar [Cetak Daftar Pengguna]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader('alls')?>

<center><font size="4"><strong>DATA PENGGUNA</strong></font><br /> </center><br /><br />

<br />
		<?php
		$sql = "SELECT h.login, h.aktif, h.lastlogin, h.departemen, h.tingkat, h.keterangan FROM ".get_db_name('user').".hakakses h, ".get_db_name('user').".login l WHERE h.modul='SIMTAKA' AND l.login=h.login";
		$result = QueryDb($sql);
		$num = @mysql_num_rows($result);
		?>
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="tab" id="table">
          <tr>
            <td height="30" align="center" class="header">NIP</td>
            <td height="30" align="center" class="header">Nama</td>
            <td align="center" class="header">Tingkat</td>
            <td align="center" class="header">Perpustakaan</td>
			<td align="center" class="header">Keterangan</td>
		  </tr>
          <?php
		  if ($num>0){
			  while ($row=@mysql_fetch_row($result)){
			  $sql = "SELECT nama FROM ".get_db_name('sdm').".pegawai WHERE nip='$row[0]'";
			  $res = QueryDb($sql);
			  $r = @mysql_fetch_row($res);
			  $namapeg = $r[0];
			  if ($row[4]==2){
				  $sql = "SELECT nama FROM perpustakaan WHERE replid='$row[3]'";
				  $res = QueryDb($sql);
				  $r = @mysql_fetch_row($res);
				  $namaperpus = $r[0];
				  $namatingkat = "Staf Perpustakaan";
			  } else {
			  	  $namaperpus = "<i>Semua</i>";
				  $namatingkat = "Manajer Perpustakaan";
			  }
			  ?>
			  <tr>
				<td height="25" align="center"><?php echo$row[0]?></td>
				<td height="25" align="center"><div class="tab_content"><?php echo$namapeg?></div></td>
				<td align="center"><?php echo$namatingkat?></td>
				<td align="center"><?php echo$namaperpus?></td>
				<td align="center"><?php echo$row[5]?></td>
			  </tr>
			  <?php
			  }
		  } else {
		  ?>
          <tr>
            <td height="25" colspan="7" align="center" class="nodata">Tidak ada data</td>
          </tr>
		  <?php
		  }
		  ?>	
        </table>
</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>