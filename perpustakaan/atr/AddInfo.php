<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once("../inc/session.checker.php");
require_once("../inc/config.php");
require_once("../inc/db_functions.php");
require_once("../inc/common.php");
$err = '';
$perpustakaan=$_REQUEST[perpustakaan];
OpenDb();
$sql	= "SELECT * FROM ".$db_name_umum.".identitas WHERE status=1 AND perpustakaan='$perpustakaan'";
$result = QueryDb($sql);
$row	= @mysql_fetch_array($result);
$nama 	= $row[nama];
$alamat	= $row[alamat1];
$telp1 	= $row[telp1];
$telp2 	= $row[telp2];
$fax 	= $row[fax1];
$website= $row[situs];
$email 	= $row[email]; 
if (isset($_REQUEST['Simpan'])){
	$nama 	= CQ($_REQUEST['nama']);
	$alamat	= CQ($_REQUEST['alamat']);
	$telp1 	= CQ($_REQUEST['telp1']);
	$telp2 	= CQ($_REQUEST['telp2']);
	$fax 	= CQ($_REQUEST['fax']);
	$website= CQ($_REQUEST['website']);
	$email 	= CQ($_REQUEST['email']); 
	$sql 	= "SELECT * FROM ".$db_name_umum.".identitas WHERE status=1 AND nama='$nama' AND perpustakaan<>'$perpustakaan'";
	$result = QueryDb($sql);
	$num	= @mysql_num_rows($result);
	if ($num>0){
		$err = "Nama perpustakaan $nama sudah digunakan!";
	} else {
		$sql2 	= "UPDATE ".$db_name_umum.".identitas SET nama='$nama',alamat1='$alamat',telp1='$telp1',telp2='$telp2',fax1='$fax',situs='$website',email='$email' WHERE status=1 AND perpustakaan='$perpustakaan'";
		QueryDb($sql2);
		?>
        <script language="javascript">
			parent.opener.Fresh();
			window.close();
        </script>
        <?php
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pengaturan Informasi Header</title>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="title" align="right">
	<font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
	<font style="font-size:18px; color:#999999">Informasi Header</font><br />
</div>
<form action="AddInfo.php" method="post">
<input type="hidden" name="Action" value="<?php echo$_REQUEST[op]?>" />
<input type="hidden" name="perpustakaan" value="<?php echo$perpustakaan?>" /> 
<table width="100%" border="0" cellspacing="3">
  <tr>
    <td width="34%" align="right">Nama Perpustakaan</td>
    <td width="66%"><input type="text" name="nama" id="nama" class="inputtxt" style="width:175px" value="<?php echo$row[nama]?>" /></td>
  </tr>
  <tr>
    <td align="right">Alamat</td>
    <td><textarea name="alamat" class="areatxt" id="alamat" cols="28" rows="3"><?php echo$row[alamat1]?></textarea></td>
  </tr>
  <tr>
    <td align="right">Telepon 1</td>
    <td><input type="text" name="telp1" id="telp1" class="inputtxt" style="width:175px" value="<?php echo$row[telp1]?>" /></td>
  </tr>
  <tr>
    <td align="right">Telepon 2</td>
    <td><input type="text" name="telp2" id="telp2" class="inputtxt" style="width:175px" value="<?php echo$row[telp2]?>" /></td>
  </tr>
  <tr>
    <td align="right">Fax</td>
    <td><input type="text" name="fax" id="fax" class="inputtxt" style="width:175px" value="<?php echo$row[fax1]?>" /></td>
  </tr>
  <tr>
    <td align="right">Website</td>
    <td><input type="text" name="website" id="website" class="inputtxt" style="width:175px" value="<?php echo$row[situs]?>" /></td>
  </tr>
  <tr>
    <td align="right">Email</td>
    <td><input type="text" name="email" id="email" class="inputtxt" style="width:175px" value="<?php echo$row[email]?>" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Simpan" type="submit" value="Simpan" class="btnfrm2" />&nbsp;&nbsp;<input type="button" value="Batal" onClick="window.close()" class="btnfrm2" /></td>
  </tr>
</table>

</body>
</html>