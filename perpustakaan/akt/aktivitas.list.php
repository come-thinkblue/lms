<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/common.php');
require_once('../inc/sessioninfo.php');
require_once('../inc/db_functions.php');
require_once('aktivitas.list.class.php');
OpenDb();
$A = new CAktivitas();
$A->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../scr/tables.js"></script>
<script type="text/javascript" src="../scr/tools.js"></script>
<script type="text/javascript" src="aktivitas.list.js"></script>
</head>

<body>
	<div id="title" align="right">
        <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
      <font style="font-size:18px; color:#999999">Daftar Aktivitas</font><br />
        <a href="aktivitas.php" class="welc">Aktivitas</a><span class="welc"> > Daftar Aktivitas</span><br /><br /><br />
    </div>
<div id="content">
      <?php echo$A->Content()?>
    </div>
</body>
<?php echo$A->OnFinish()?>
</html>
<?php CloseDb(); ?>