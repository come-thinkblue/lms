<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
class CPustakaEdit
{
	var $rak, $replid, $keterangan;
	
	function OnStart()
	{
		if (isset($_REQUEST[simpan]))
		{
			$sql = "SELECT nama FROM perpustakaan WHERE nama='".CQ($_REQUEST['nama'])."' AND replid <> '$_REQUEST[replid]'";
			$result = QueryDb($sql);
			$num = @mysql_num_rows($result);
			if ($num>0)
			{
				$this->exist();
			}
			else
			{
				$sql = "UPDATE perpustakaan SET nama='".CQ($_REQUEST['nama'])."', keterangan='".CQ($_REQUEST['keterangan'])."' WHERE replid='$_REQUEST[replid]'";
				$result = QueryDb($sql);
				
				$sql = "UPDATE dbakademik.identitas SET perpustakaan='$_REQUEST[replid]' WHERE departemen='$_REQUEST[dep]'";
				$result = QueryDb($sql);

				$this->success();
			}
		}
		else
		{
			$sql = "SELECT * FROM perpustakaan WHERE replid='$_REQUEST[id]'";
			$result = QueryDb($sql);
			$row = @mysql_fetch_array($result);
			$this->replid = $_REQUEST[id];
			$this->nama = $row[nama];
			$this->keterangan = $row[keterangan];

			$sql = "SELECT departemen FROM dbakademik.identitas WHERE perpustakaan='$this->replid'";
			$result = QueryDb($sql);
			$row = @mysql_fetch_array($result);
			$this->dep = $row[departemen];
		}
	}
	
	function exist()
	{	?>
      <script language="javascript">
			alert('Perpustakaan sudah digunakan!');
			document.location.href="pustaka.edit.php?id=<?php echo$_REQUEST[replid]?>";
		</script>
<?php }
	
	function success()
	{	?>
		<script language="javascript">
		parent.opener.getfresh();
		window.close();
		</script>
<?php	}

	function edit()
	{	?>
      <form name="editpustaka" onSubmit="return validate()">
      <input name="replid" type="hidden" id="replid" value="<?php echo$this->replid?>">
		<table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
         <td colspan="2" align="left">
			   <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
				<font style="font-size:18px; color:#999999">Ubah Perpustakaan</font>
			</td>
  		</tr>
      <tr>
         <td>&nbsp;<strong>Departemen</strong></td>
         <td>
            <select id="dep" name="dep" class="cmbfrm2">
<?php				$sql = "SELECT departemen FROM dbakademik.departemen ORDER BY urutan ASC";
				$res = QueryDb($sql);
				while ($row = @mysql_fetch_row($res))
				{
					echo "<option value='".$row[0]."' ";
					if ($this->dep==$row[0])
						echo "Selected";	
					echo ">".$row[0]."</option>";
				}	?>
            </select>
         </td>
      </tr>
		<tr>
         <td width="6%">&nbsp;<strong>Nama</strong></td>
         <td width="94%"><input name="nama" type="text" class="inputtxt" id="nama" value="<?php echo$this->nama?>"></td>
      </tr>
      <tr>
         <td>&nbsp;Keterangan</td>
         <td><textarea name="keterangan" cols="45" rows="5" class="areatxt" id="keterangan"><?php echo$this->keterangan?></textarea></td>
      </tr>
      <tr>
			<td colspan="2" align="center"><input type="submit" class="cmbfrm2" name="simpan" value="Simpan" >&nbsp;<input type="button" class="cmbfrm2" name="batal" value="Batal" onClick="window.close()" ></td>
      </tr>
      </table>
		</form>
		<?php
	}
}
?>