<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/sessioninfo.php');
require_once('../inc/common.php');
require_once('../inc/config.php');
require_once('../inc/db_functions.php');
require_once('../lib/GetHeaderCetak.php');
OpenDb();
$departemen='yayasan';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../sty/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Perpustakaan MAN Kota Blitar [Cetak Format Pustaka]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader('alls')?>
 
<center><font size="4"><strong>DATA FORMAT PUSTAKA</strong></font><br /> </center><br /><br />

<br />
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="tab" id="table">
	  <tr>
		<td height="30" align="center" class="header">Kode</td>
		<td height="30" align="center" class="header">Nama</td>
		<td height="30" align="center" class="header">Jumlah Judul</td>
		<td height="30" align="center" class="header">Jumlah Pustaka</td>
		<td height="30" align="center" class="header">Keterangan</td>
	  </tr>
	  <?php
	  $sql = "SELECT * FROM format ORDER BY kode";
	  $result = QueryDb($sql);
	  $num = @mysql_num_rows($result);
	  if ($num>0){
		  while ($row=@mysql_fetch_array($result)){
		  $num_judul = @mysql_num_rows(QueryDb("SELECT * FROM pustaka WHERE format='$row[replid]'"));
		  $num_pustaka = @mysql_fetch_row(QueryDb("SELECT COUNT(d.replid) FROM pustaka p, daftarpustaka d WHERE d.pustaka=p.replid AND p.format='$row[replid]'"));
		  ?>
		  <tr>
			<td height="25" align="center"><?php echo$row[kode]?></td>
			<td height="25" align="center">&nbsp;<?php echo$row[nama]?></td>
			<td height="25" align="center">&nbsp;<?php echo$num_judul?></td>
			<td height="25" align="center">&nbsp;<?php echo(int)$num_pustaka[0]?></td>
			<td height="25" align="center">&nbsp;<?php echo$row[keterangan]?></td>
		  </tr>
		  <?php
		  }
	  } else {
	  ?>
	  <tr>
		<td height="25" colspan="5" align="center" class="nodata">Tidak ada data</td>
	  </tr>
	  <?php
	  }
	  ?>	
	</table>
</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>