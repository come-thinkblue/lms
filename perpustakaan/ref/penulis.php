<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');
require_once('../inc/sessioninfo.php'); 
require_once('penulis.class.php');
OpenDb();
$P = new CPenulis();
$P->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../scr/tables.js"></script>
<script type="text/javascript" src="../scr/tools.js"></script>
<script type="text/javascript" src="penulis.js"></script>
</head>

<body>
	<div id="title" align="right">
        <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
        <font style="font-size:18px; color:#999999">Daftar Penulis</font><br />
        <a href="referensi.php" class="welc">Referensi</a><span class="welc"> > Daftar Penulis</span><br /><br /><br />
    </div>
    <div id="content">
      <?php echo$P->Content()?>
    </div>
</body>
<?php echo$P->OnFinish()?>
</html>
<?php CloseDb(); ?>