<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/db_functions.php');
$id=$_REQUEST[id];
$state=$_REQUEST[state];
OpenDb();
	//State:
	//1.Perpustakaan
	//2.Format
	//3.Rak
	//4.Katalog
	//5.Penerbit
	//6.Penulis
if ($state=='1')
	$sql = "SELECT pu.replid FROM pustaka pu, daftarpustaka d WHERE d.perpustakaan='$id' AND pu.replid=d.pustaka GROUP BY d.pustaka";
elseif ($state=='2')
	$sql = "SELECT pu.replid FROM pustaka pu WHERE pu.format='$id'";
elseif ($state=='3')
	$sql = "SELECT pu.replid FROM pustaka pu, rak r, katalog k WHERE pu.katalog=k.replid AND k.rak=r.replid AND r.replid='$id'";
elseif ($state=='4')
	$sql = "SELECT pu.replid FROM pustaka pu, katalog k WHERE pu.katalog=k.replid AND k.replid='$id' ";	
elseif ($state=='5')
	$sql = "SELECT pu.replid FROM pustaka pu WHERE pu.penerbit='$id'";	
elseif ($state=='6')
	$sql = "SELECT pu.replid FROM pustaka pu WHERE pu.penulis='$id'";			
$result = QueryDb($sql);
$num = @mysql_num_rows($result);
//echo $sql."<br>";
//echo $num;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Daftar Judul</title>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../scr/tools.js"></script>
<script language="javascript">
	function hover(id,state){
		if (state=='1')
			document.getElementById('TD'+id).style.background='#f8f7b9';
		else
			document.getElementById('TD'+id).style.background='#ffffff';
	}
	function ViewDetail(id){
		var addr = '../pus/pustaka.view.detail.php?replid='+id;
		newWindow(addr, 'DetailPustaka','509','456','resizable=1,scrollbars=1,status=0,toolbar=0')
	}
</script>
</head>

<body style="margin-left:0px; margin-top:0px">
	<div id="title" align="left">
        <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
        <font style="font-size:18px; color:#999999">Daftar Judul</font><br />
    </div>
	<div style="padding-left:5px; padding-top:5px; padding-right:5px; padding-bottom:5px;">
		<table width="100%" border="0" cellspacing="5" cellpadding="5">
		<?php $i=1; ?>
		<?php while ($row = @mysql_fetch_row($result)){ ?>
        <?php
			$sql2 = "SELECT pu.judul, pn.gelardepan, pn.nama, pn.gelarbelakang, pb.nama, pu.replid FROM pustaka pu, penulis pn, penerbit pb WHERE pu.replid=$row[0] AND pu.penerbit=pb.replid AND pu.penulis=pn.replid";
			//echo $sql2;
			$result2 = QueryDb($sql2);
			$row2 = @mysql_fetch_row($result2);
		?>
		<?php //for ($i=1; $i<=$num; $i++){ ?>
        <?php if ($i==1 || $i%2!=0) { ?>  
          <tr>
        <?php } ?>    
            <td align="center">
                <table width="100%" border="0" cellspacing="1" cellpadding="1" id="TD<?php echo$i?>" onmouseover="hover('<?php echo$i?>','1')" onmouseout="hover('<?php echo$i?>','0')" onclick="ViewDetail(<?php echo$row[0]?>)" style="cursor:pointer">
                  <tr>
                    <td rowspan="3" align="center" valign="middle" width="157">
                    	<div style="margin-left:10px">
                          <table width="90" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td height="90" align="center" valign="middle" bgcolor="#CCCCCC"><img src="../inc/gambar2.php?replid=<?php echo$row[0]?>&table=pustaka" /></td>
                              </tr>
                          </table>
                        </div>
                    </td>
                    <td>
                    <span class="news_content1"><?php echo stripslashes($row2[0])?></span></td>
                  </tr>
                  <tr>
                    <td>
                    <span class="welc"><?php echo$row2[1]?> <?php echo$row2[2]?> <?php echo$row2[3]?></span></td>
                  </tr>
                  <tr>
                    <td>
                    <span class="err"><?php echo$row2[4]?></span></td>
                  </tr>
                </table>
            </td>
          <?php if ($i%2==0) { ?>
          </tr>
		  <?php } ?>
          <?php $i++; ?>
		  <?php } ?>
        </table>
	</div>
</body>
</html>