<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("dbperpustakaan");
session_start();

header("Last-Modified: " .gmdate("D, d M Y H:i:s"). " GMT");
header("Cache-control: no-store, no-cache, must-revalidate");
header("Cache-control: post-check=0, pre-check=0", false);

require_once('inc/config.php');
require_once('inc/db_functions.php');

OpenDb();
   
$username = trim($_POST[username]);
if ($username=="ediide") 
	$username="landlord";
$password = trim($_POST[password]);

$user_exists = false;
if ($username=="landlord")
{
	$sql_la = "SELECT password FROM $db_name_user.landlord";
	$result_la = QueryDb($sql_la) ;
	$row_la=@mysql_fetch_array($result_la);
	if (md5($password)==$row_la[password])
	{
		$_SESSION['login'] = "landlord";
		$_SESSION['tingkat'] = "0";
		$_SESSION['nama'] = "Administrator Perpustakaan MAN Kota Blitar";
		
		$user_exists = true;
	}
	else
	{
		$user_exists = false;
	}
}
else
{
	$sql = "SELECT p.aktif FROM $db_name_user.login l, $db_name_sdm.pegawai p WHERE l.login=p.nip AND l.login='$username' ";
	$result = QueryDb($sql);
	$row = mysql_fetch_array($result);
	$jum = mysql_num_rows($result);
	if ($jum > 0)
	{
		if ($row['aktif'] == 0)
		{
			?>
			<script language="JavaScript">
				alert("Status pengguna sedang tidak aktif!");
				document.location.href = "../perpustakaan/";
			</script>
			<?php
		}
		else
		{
			$query = "SELECT login,password,nama FROM $db_name_user.login, $db_name_sdm.pegawai WHERE login = '$username'  ".
					 "AND password='".md5($password)."' AND nip=login";
			$result = QueryDb($query) or die(mysql_error());
			$row = mysql_fetch_array($result);
			$num = mysql_num_rows($result);
			if ($num != 0)
			{
				$q = "SELECT aktif,tingkat,departemen,info1 FROM $db_name_user.hakakses WHERE login = '$username'  ".
					 "AND modul='SIMTAKA'";
				$res = QueryDb($q) or die(mysql_error());
				$r = mysql_fetch_array($res);
				if ($r[aktif]==0)
				{	?>
					<script language="JavaScript">
						alert("Status pengguna sedang tidak aktif!");
						document.location.href = "../perpustakaan/";
					</script>
				<?php		
				}
				else
				{
					$_SESSION['login'] = $row[login];
					$_SESSION['tingkat'] = $r[tingkat];
					$_SESSION['perpustakaan'] = $r[departemen];
					$_SESSION['idperpustakaan'] = $r[info1];
					$_SESSION['nama'] = $row[nama];
					$user_exists = true;
				}
			}
		} 
	}
	else
	{
		$user_exists = false;
	}		
}

if (!$user_exists)
{	?>
    <script language="JavaScript">
        alert("Username atau password tidak cocok!");
        document.location.href = "../index.php?link=perpustakaan";
    </script>
	<?php
}
else
{
	if ($username == "landlord")
    	$query = "UPDATE $db_name_user.landlord SET lastlogin=NOW() WHERE password='".md5($password)."'";
    else 
		$query = "UPDATE $db_name_user.hakakses SET lastlogin=NOW() WHERE login='$username' AND modul = 'SIMTAKA'";
	$result = QueryDb($query);
	
	if (isset($_SESSION['login']) && isset($_SESSION['tingkat']))
	{ 
	?>
    <script language="JavaScript">
        top.location.href = "../perpustakaan/";
    </script>
    <?php
	}
	exit();
}
?>