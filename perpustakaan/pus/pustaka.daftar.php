<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');
require_once('../inc/fileinfo.php');
require_once('../inc/imageresizer.php');
require_once('../inc/sessioninfo.php');
require_once('pustaka.daftar.class.php');
OpenDb();
$P = new CPustaka();
$P->OnStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../sty/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../scr/tables.js"></script>
<script type="text/javascript" src="../scr/tools.js"></script>
<script type="text/javascript" src="pustaka.daftar.js"></script>
<script type="text/javascript" src="../scr/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		mode : "exact",
        elements : "abstraksi", 
		theme : "simple"
	});
</script>
</head>
<body>
	<div id="title" align="right">
        <font style="color:#FF9900; font-size:30px;"><strong>.:</strong></font>
        <font style="font-size:18px; color:#999999">Daftar Pustaka</font><br />
        <a href="pustaka.php" class="welc">Pustaka</a><span class="welc"> > Daftar Pustaka</span><br /><br /><br />
    </div>
    <div id="content">
      <?php echo$P->Content()?>
    </div>
</body>
<?php echo$P->OnFinish()?>
</html>
<?php CloseDb(); ?>