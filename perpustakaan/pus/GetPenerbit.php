<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../inc/config.php');
require_once('../inc/db_functions.php');
require_once('../inc/common.php');
$penerbit=$_REQUEST[penerbit];
OpenDb();
$sql = "SELECT replid,kode,nama FROM penerbit ORDER BY nama";
$result = QueryDb($sql);
$max=0;
while ($row = @mysql_fetch_row($result)){
	$newmax = strlen($row[2]);
	if ($newmax>$max)
		$max=$newmax;
		
}
?>
<select name="penerbit" id="penerbit" class="cmbfrm" style="width:100%; font-family:'Courier New'">
<?php
$sql = "SELECT replid,kode,nama FROM penerbit ORDER BY nama";
$result = QueryDb($sql);
while ($row = @mysql_fetch_row($result)){
$len = strlen($row[2]);
$space = GetSpace($max,$len);
if ($penerbit=="")
	$penerbit = $row[0];	
?>
	<option value="<?php echo$row[0]?>" <?php echo IntIsSelected($row[0],$penerbit)?>><?php echo$row[2]?><?php echo$space?> - <?php echo$row[1]?></option>
<?php
}
?>
</select>
<?php
function GetSpace($maxlength,$length){
	$spacer="";
	for ($i=1;$i<=$maxlength-$length;$i++)
		$spacer .="&nbsp;";
	return $spacer;	
}
?>