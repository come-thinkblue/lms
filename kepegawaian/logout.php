<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("s_kepeg");
if(!isset($_SESSION)){ session_start();}

unset($_SESSION['login']);
unset($_SESSION['tingkatsimpeg']);
unset($_SESSION['temasimpeg']);
unset($_SESSION['namasimpeg']);
unset($_SESSION['departemensimpeg']);
?>
<script language="javascript">
top.window.location='../../man/index.php?link=kepegawaian';
</script>