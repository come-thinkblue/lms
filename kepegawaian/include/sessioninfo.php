<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once('config.php');

session_name("s_kepeg");
if(!isset($_SESSION)){ session_start();}

$SI_USER_LANDLORD = 0;
$SI_USER_MANAGER = 1;
$SI_USER_STAFF = 2;

function getUserName()
{
	return $_SESSION['namasimpeg'];
}

function getUserTheme()
{
	return $_SESSION['temasimpeg'];
}

function getLevel()
{
	return $_SESSION['tingkatsimpeg'];
}

function getAccess()
{
	return $_SESSION['tingkatsimpeg'];
}

function getUserId()
{
	return $_SESSION['login'];
}

function SI_USER_LEVEL()
{
	switch ($_SESSION['tingkatsimpeg'])
	{
		case 0:
		{
			global $SI_USER_LANDLORD;
			return $SI_USER_LANDLORD;
			break;
		}
		case 1:
		{
			global $SI_USER_MANAGER;
			return $SI_USER_MANAGER;
			break;
		}
		case 2:
		{
			global $SI_USER_STAFF;
			return $SI_USER_STAFF;
			break;
		}
	}
}
?>