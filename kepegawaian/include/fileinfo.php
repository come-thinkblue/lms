<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
function GetFileExt($filename) 
{
	$part = explode(".", $filename);
	return "." . $part[count($part) - 1];
}

function GetFileName($filename)
{
	$part = explode(".", $filename);
	$ndot = count($part);
	
	$name = "";
	for ($i = 0; $i < $ndot - 1; $i++) 
	{
		if (strlen($name) > 0)
			$name = $name . ".";
		$name = $name . $part[$i];
	}
	return $name;
}
?>