<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
$stat = $_REQUEST['stat'];
if ($stat == 5) 
{
	header("location: statdiklat.php");
	exit();
} 
elseif ($stat == 6) 
{
	header("location: statjk.php");
	exit();
} 
elseif ($stat == 7) 
{
	header("location: statnikah.php");
	exit();
} 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar Kepegawaian</title>
<link rel="stylesheet" href="../style/style.css" />
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function Refresh() {
	parent.parent.location.href = "statcontent.php?stat=<?php echo$stat?>";
}

function Cetak() {
	newWindow('stat_cetak.php?stat=<?php echo$stat?>', 'CetakStatistik','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
}

</script>
</head>

<body>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td align="center">
    <a href="JavaScript:Cetak()"><img src="../images/ico/print.png" border="0" />&nbsp;Cetak</a>&nbsp;
    <a href="JavaScript:Refresh()"><img src="../images/ico/refresh.png" border="0" />&nbsp;Refresh</a>
</td></tr>
</table>
<br />
<table width="100%" border="0">
<tr><td>

	<div id="grafik" align="center">
	<table width="100%" border="0" align="center">
    <tr><td>
    	<div align="center">
        <img src="<?php echo "statimage.php?type=bar&stat=$stat" ?>" />
        </div>
    </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>
    	<div align="center">
        <img src="<?php echo "statimage.php?type=pie&stat=$stat" ?>" />
        </div>
    </td></tr>
	</table>
	</div>
    
</td></tr>
</table>

</body>
</html>