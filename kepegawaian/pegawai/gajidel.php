<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/config.php");
require_once("../include/db_functions.php");
require_once("../include/common.php");
require_once("../include/sessioninfo.php");
require_once('../include/theme.php');

$nip = $_REQUEST['nip'];
$id = $_REQUEST['id'];

if (isset($_REQUEST['Hapus'])) {
	OpenDb();
	
	$user = getUserId();
	$alasan = $_REQUEST['alasan'];
	
	$sql = "INSERT INTO dataaction SET tabel='peggaji', id=$id, op='D', user='$user', alasan='$alasan'";
	QueryDb($sql);
	
	$sql = "DELETE FROM peggaji WHERE replid=$id";
	QueryDb($sql);
	
	CloseDb(); ?>
    <script language="javascript">
		opener.Refresh();
		window.close();
    </script>
<?php
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hapus Riwayat Gaji</title>
<link rel="stylesheet" href="../style/style<?php echo GetThemeDir2()?>.css" />
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript" src="../script/rupiah.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function validate() {
	return validateEmptyText('alasan', 'Alasan Penghapusan Data Gaji Pegawai') && 
		   confirm("Apakah anda yakin akan menghapus data ini?");
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>

<body>
<form name="main" method="post" onSubmit="return validate()">
<input type="hidden" name="nip" id="nip" value="<?php echo$nip?>" />
<input type="hidden" name="id" id="id" value="<?php echo$id?>" />
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr height="30">
	<td width="100%" class="header" align="center">Hapus Riwayat Gaji</td>
</tr>
<tr>
	<td width="100%" align="center">
    
    <table border="0" cellpadding="0" cellspacing="5" width="100%">
    <tr>
    	<td align="right" valign="top"><strong>Alasan Penghapusan :</strong></td>
	    <td align="left" valign="top">
        <textarea id="alasan" name="alasan" rows="2" cols="50"><?php echo$alasan?></textarea>
        </td>
    </tr>
    <tr>
    	<td align="right" valign="top">&nbsp;</td>
	    <td align="left" valign="top">
        <input type="submit" value="Hapus" name="Hapus" style="color:#FF0000" class="but" />
        <input type="button" value="Tutup" onClick="window.close()" class="but" />
        </td> 
    </tr>
    </table>
    
    </td>
</tr>
</table>
</form>

</body>
</html>