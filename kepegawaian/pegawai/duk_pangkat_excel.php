<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once('../include/sessionchecker.php');
require_once('../include/config.php');
require_once('../include/common.php');
require_once('../include/db_functions.php');

$satker = "all";
if (isset($_REQUEST['satker']))
	$satker = $_REQUEST['satker'];	
	
/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=DUK_Pangkat.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar Kepegawaian</title>
<style type="text/css">
<!--
.style1 {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<?php
OpenDb();

$arridpeg;

$namasatker = "Semua";
if ($satker == "all")
{
	$sql = "SELECT p.replid FROM pegawai p, peglastdata pl, peggol pg, golongan g 
    	    WHERE p.aktif=1 AND p.nip = pl.nip AND pl.idpeggol = pg.replid AND pg.golongan = g.golongan 
			ORDER BY g.urutan DESC";
}
else
{
	$sql = "SELECT nama FROM satker WHERE satker='$satker'";
	$result = QueryDb($sql);
	$row = mysql_fetch_row($result);
	$namasatker = $row[0];
	
	$sql = "SELECT p.replid FROM pegawai p, peglastdata pl, peggol pg, golongan g, pegjab pj, jabatan j 
    	    WHERE p.aktif=1 AND p.nip = pl.nip AND pl.idpeggol = pg.replid AND pg.golongan = g.golongan 
			AND pl.idpegjab = pj.replid AND pj.idjabatan = j.replid AND j.satker = '$satker' 
			ORDER BY g.urutan DESC";
}
	
$result = QueryDb($sql);
while ($row = mysql_fetch_row($result)) 
	$arridpeg[] = $row[0];
	
$ndata = mysql_num_rows($result);
?>

<table border="1" cellpadding="2" cellspacing="0" width="1235" class="tab" id="table">
    <tr height="40">
      <td height="40" colspan="17" align="center" valign="top"><p class="style1">Daftar Urut Kepangkatan Pegawai Negeri Sipil</p></td>
    <tr height="20">
      <td height="20" colspan="17" align="left" valign="top">Satuan Kerja: <?php echo$namasatker?></td>
    </tr>
  <tr height="20">
    <td  width="20" rowspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>No</strong></td>
    <td  width="160" rowspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Nama</strong></td>
    <td  width="120" rowspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>NIP</strong></td>
	<td  width="120" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Pangkat</strong></td>
    <td  width="200" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Jabatan</strong></td>
    <td  width="100" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Masa Kerja</strong></td>
    <td  width="100" colspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Diklat</strong></td>
    <td  width="125" colspan="3" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Pendidikan</strong></td>
    <td  width="40" rowspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Usia</strong></td>
    <td  width="120" rowspan="2" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Kelahiran</strong></td>
	<td  width="200" rowspan="2" align="left" valign="middle" bgcolor="#CCCCCC"><strong>Ket.</strong></td>
  </tr>
<tr height="20">
  <td  width="120" align="center" valign="middle" bgcolor="#CCCCCC"><strong>GOL</strong></td>
    <td  width="60" align="center" valign="middle" bgcolor="#CCCCCC"><strong>TMT</strong></td>
    
    <td  width="200" align="center" valign="middle" bgcolor="#CCCCCC"><strong>NAMA</strong></td>
    <td  width="60" align="center" valign="middle" bgcolor="#CCCCCC"><strong>TMT</strong></td>
    
    <td  width="100" align="center" valign="middle" bgcolor="#CCCCCC"><strong>GOL</strong></td>
    <td  width="50" align="center" valign="middle" bgcolor="#CCCCCC"><strong>SEL</strong></td>
    
    <td  width="100" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Nama</strong></td>
    <td  width="50" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Th</strong></td>
    
    <td  width="125" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Nama</strong></td>
    <td  width="30" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Lls</strong></td>
    <td  width="30" align="center" valign="middle" bgcolor="#CCCCCC"><strong>Tk</strong></td>
  </tr>

<?php
for($i = 0; $i < $ndata; $i++)
{
	$idpeg = $arridpeg[$i];
	if (strlen(trim($idpeg)) == 0)
		continue;
	
	$cnt = $i;
	
	$sql = "SELECT CONCAT(p.gelarawal, ' ', p.nama, ' ', p.gelarakhir) AS pnama, p.nip, pg.golongan, 
				   DATE_FORMAT(pg.tmt, '%m %y') AS tgltmtgol, DATEDIFF(now(), pg.tmt) AS tmtgol,
				   j.jabatan, DATE_FORMAT(pj.tmt, '%d-%m-%y') AS tmtjab, DATEDIFF(now(), mulaikerja) AS tglmulai,
				   pl.idpegdiklat, pj.jenis,
				   ps.sekolah, ps.lulus, ps.tingkat,
				   DATEDIFF(now(), tgllahir) AS difflahir,
				   tmplahir, DATE_FORMAT(tgllahir, '%d-%m-%Y') AS tgllahir, p.keterangan
	          FROM pegawai p
			  LEFT JOIN peglastdata pl
			    ON p.nip = pl.nip
			  LEFT JOIN peggol pg
			    ON pl.idpeggol = pg.replid
			  LEFT JOIN golongan g
			    ON pg.golongan = g.golongan
			  LEFT JOIN pegjab pj
			    ON pl.idpegjab = pj.replid
			  LEFT JOIN jabatan j
			    ON pj.idjabatan = j.replid
			  LEFT JOIN pegsekolah ps	
			    ON pl.idpegsekolah = ps.replid
			 WHERE p.replid = $idpeg
			   AND p.aktif = 1 
			 ORDER BY g.urutan DESC, p.nama ASC";
			
	$result = QueryDb($sql);			
	$row = mysql_fetch_array($result);
?>
<tr>
	<td align="center" valign="middle"><?php echo++$cnt?></td>
    <td align="left" valign="middle"><?php echo$row['pnama']?></td>
    <td align="left" valign="middle"><?php echo$row['nip']?></td>
    <td align="center" valign="middle"><?php echo$row['golongan']?></td>
    <td align="center" valign="middle"><?php echo$row['tgltmtgol']?></td>
    <td align="left" valign="middle"><?php echo$row['jenis'] . " " . $row['jabatan']?></td>
    <td align="center" valign="middle"><?php echo$row['tmtjab']?></td>
    <td align="center" valign="middle"><?
    	$thn = floor($row['tmtgol'] / 365);
		$bln = $row['tmtgol'] % 365;
		$bln = floor($bln / 30);
		echo "$thn-$bln.";
	?></td>
    <td align="center" valign="middle">
	<?php	$thn = floor($row['tglmulai'] / 365);
		$bln = $row['tglmulai'] % 365;
		$bln = floor($bln / 30);
		echo "$thn-$bln.";
	?>    </td>
    <?php
	$diklat = "&nbsp;";
	$thndiklat = "&nbsp;";
	if ($row['idpegdiklat'] != NULL) {
		$idpegdiklat = $row['idpegdiklat'];
		$sql = "SELECT d.diklat, pd.tahun FROM pegdiklat pd, diklat d WHERE pd.replid=$idpegdiklat AND pd.iddiklat=d.replid";
		$rs = QueryDb($sql);
		$rw = mysql_fetch_row($rs);
		$diklat = $rw[0];
		$thndiklat = $rw[1];
	};
	?>
    <td align="center" valign="middle"><?php echo$diklat?></td>
    <td align="center" valign="middle"><?php echo$thndiklat?></td>
    <td align="center" valign="middle"><?php echo$row['sekolah']?></td>
    <td align="center" valign="middle"><?php echo$row['lulus']?></td>
    <td align="center" valign="middle"><?php echo$row['tingkat']?></td>
    <td align="center" valign="middle">
	<?php	$thn = floor($row['difflahir'] / 365);
		$bln = $row['difflahir'] % 365;
		$bln = floor($bln / 30);
		echo "$thn,$bln";
	?>    </td>
    <td align="left" valign="middle"><?php echo$row['tmplahir'] . ", " . $row['tgllahir'] ?></td>
	<td align="left" valign="middle"><?php echo$row['keterangan']?></td>
</tr>
<?php
}
?>
</table>

</body>
</html>