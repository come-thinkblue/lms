<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/sessionchecker.php");
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/theme.php');
require_once("../include/sessioninfo.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="../style/style<?php echo GetThemeDir2()?>.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar Kepegawaian</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function ShowDetail(sat, nikah) {
	parent.statdetail.location.href = "statdetailnikah.php?sat="+sat+"&nikah="+nikah;
}

function CetakWord() {
	var addr = "cetakword.php?key=<?php echo$key?>&keyword=<?php echo$keyword?>";
	newWindow(addr, 'StatWord','790','630','resizable=1,scrollbars=1,status=0,toolbar=0');
}
</script>
</head>

<body style="background-color:#DFDFDF">
<table id="table" class="tab" border="1" cellpadding="2" cellspacing="0" width="100%">
<tr height="25">
	<td class="header" align="center" width="5%">No</td>
    <td class="header" align="center" width="60%">Satuan Kerja</td>
    <td class="header" align="center" width="15%">Nikah</td>
    <td class="header" align="center" width="15%">Belum</td>
</tr>
<?php
OpenDb();
$sql = "SELECT j.satker, SUM(IF(p.nikah = 'menikah', 1, 0)) AS Nikah, SUM(IF(p.nikah = 'belum', 1, 0)) AS Belum
		  FROM pegawai p, peglastdata pl, pegjab pj, jabatan j
		  WHERE p.aktif = 1 AND p.nip = pl.nip AND pl.idpegjab = pj.replid AND pj.idjabatan = j.replid AND NOT j.satker IS NULL
		  GROUP BY j.satker";	
$result = QueryDb($sql);
while ($row = mysql_fetch_row($result)) {
?>
<tr height="20">
	<td align="center" valign="top"><?php echo++$cnt?></td>
    <td align="center" valign="top"><?php echo$row[0]?></td>
    <td align="center" valign="top">
    	<a href="JavaScript:ShowDetail('<?php echo$row[0]?>','menikah')">
		<?php echo$row[1]?>&nbsp;<img src="../images/ico/lihat.png" border="0" />
        </a>
    </td>
    <td align="center" valign="top">
    	<a href="JavaScript:ShowDetail('<?php echo$row[0]?>','belum')">
		<?php echo$row[2]?>&nbsp;<img src="../images/ico/lihat.png" border="0" />
        </a>
    </td>
</tr>
<?php
}
CloseDb();
?>
</table>
<script language='JavaScript'>
   Tables('table', 1, 0);
</script>

</body>
</html>