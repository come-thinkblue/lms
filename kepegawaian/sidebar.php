<?php
/**[N]**

 **[N]**/ ?>
<?php
require_once("include/config.php");
require_once("include/db_functions.php");
require_once("include/common.php");
require_once('include/theme.php');
require_once("include/sessioninfo.php");
OpenDb();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kepegawaian</title>
<link rel="stylesheet" href="style/style.css" />
<link rel="stylesheet" href="css/custom.css" />

<script type='text/javascript' src='menu/kwicks.js'></script>
<script type='text/javascript' src='menu/custom.js'></script>
<script type="text/javascript" src="frameatas.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/custom.js"></script>

<style>

html, body {
       background: #151515;
    padding: 0;
    margin: 0;
}
/**
 * Styling navigation
 */

 ol, ul { list-style: none; padding:0;}

blockquote, q { quotes: none }

blockquote:before, blockquote:after, q:before, q:after {
  content: '';
  content: none
}

header {
  margin-right: auto;
  margin-left: auto;
 
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.25);
}

/**
 * Styling top level items
 */

.nav a, .nav label {
  display: block;
 padding: .60rem;
  color: #fff;
  background-color: #151515;
  box-shadow: inset 0 -1px #1d1d1d;
  -webkit-transition: all .25s ease-in;
  transition: all .25s ease-in;
}

.nav a:focus, .nav a:hover, .nav label:focus, .nav label:hover {
  color: rgba(255, 255, 255, 0.5);
  background: #030303;
}

.nav label { cursor: pointer; }

/**
 * Styling first level lists items
 */

.group-list a, .group-list label {
  padding-left: 2rem;
  background: #252525;
  box-shadow: inset 0 -1px #373737;
}

.group-list a:focus, .group-list a:hover, .group-list label:focus, .group-list label:hover { background: #61892a; color:#fff; text-decoration:none; }

/**
 * Styling second level list items
 */

.sub-group-list a, .sub-group-list label {
  padding-left: 4rem;
  background: #353535;
  box-shadow: inset 0 -1px #474747;
}

.sub-group-list a:focus, .sub-group-list a:hover, .sub-group-list label:focus, .sub-group-list label:hover { background: #232323; }

/**
 * Styling third level list items
 */

.sub-sub-group-list a, .sub-sub-group-list label {
  padding-left: 6rem;
  background: #454545;
  box-shadow: inset 0 -1px #575757;
}

.sub-sub-group-list a:focus, .sub-sub-group-list a:hover, .sub-sub-group-list label:focus, .sub-sub-group-list label:hover { background: #333333; }

/**
 * Hide nested lists
 */

.group-list, .sub-group-list, .sub-sub-group-list {
  height: 100%;
  max-height: 0;
  overflow: hidden;
  -webkit-transition: max-height .5s ease-in-out;
  transition: max-height .5s ease-in-out;
}

.nav__list input[type=checkbox]:checked + label + ul { /* reset the height when checkbox is checked */
max-height: 1000px; }

/**
 * Rotating chevron icon
 */

label > span {
  float: right;
  -webkit-transition: -webkit-transform .65s ease;
  transition: transform .65s ease;
}

.nav__list input[type=checkbox]:checked + label > span {
  -webkit-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  transform: rotate(90deg);
}




</style>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="http://www.cssscript.com/wp-includes/css/sticky.css" rel="stylesheet" type="text/css">


</head>

<body>  

<?php

function submenu($parent)
{
$querySub = "select * from menu where idParent=1 and idmenu =".$parent." order by idUrut";
$ekseSub = mysql_query ($querySub);

                 while ($row = mysql_fetch_array ($ekseSub))
	{
			?>
                <li> <a href="<?php echo $row ['url'];?>" target="content"><?php echo $row ['nama'];?></a> </li> 
                 <?php
		}

}
               ?>
	
	
	
	
	<?php
$query = "select * from menu where idParent=0 ORDER BY menu.idmenu ASC";
$ekse = mysql_query ($query);
	?>
	 <header role="banner">
  <nav class="nav" role="navigation">
	<ul class="nav__list">
	<?php
	$x=1;
    while ($row = mysql_fetch_array ($ekse))
			{
				
          if ( $row ['child']==0){
				?>
               <li> <a href="<?php echo $row ['url'];?>" target="content"><?php echo $row ['nama'];?></a> </li>
               <?php  // submenu ($row ['nama']);
			   	}
				
				else 
				{?>
				<li>
				<input id="group-<?php echo $x;?>" type="checkbox" hidden />
				<label for="group-<?php echo $x;?>"><span class="fa fa-angle-right"></span><?php echo $row ['nama'];?></label>
				       
					<ul class="group-list">
					<?php submenu ($row ['idmenu']); ?>				
					</ul>						
				</li>
	    <?php }
		$x++;
    }
	?>
	</ul>
	  </nav>
</header>
	

	
	
<?php
CloseDb();
?> 	


</body>
</html>