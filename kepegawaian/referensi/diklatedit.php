<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/config.php");
require_once("../include/db_functions.php");
require_once("../include/common.php");
require_once('../include/theme.php');
require_once("../include/sessioninfo.php");

OpenDb();

$id = $_REQUEST['id'];

if (isset($_REQUEST['btSimpan'])) 
{
	$diklat = $_REQUEST['txDiklat'];
	$sql = "UPDATE diklat SET diklat='$diklat' WHERE replid = $id";
	QueryDb($sql);
	CloseDb($sql);
	?>
	<script language="javascript">
		opener.RefreshPage(<?php echo$id?>);
		window.close();
    </script>    
    <?php
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ubah Diklat</title>
<link rel="stylesheet" href="../style/style<?php echo GetThemeDir2()?>.css" />
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#ffffff">

<?php
OpenDb();

$sql = "SELECT diklat FROM diklat WHERE replid=$id";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$diklat = $row[0];
?>
<form name="main" method="post">
<input type="hidden" name="id" id="id" value="<?php echo$id?>" />
<table border="0" cellpadding="0" cellspacing="5" width="100%" id="table56">
<tr>
	<td class="header" colspan="2" align="center">Ubah Diklat</td>
</tr>
<tr>
	<td align="right" width="120">Diklat :</td>
    <td align="left"><input type="text" name="txDiklat" id="txDiklat" value="<?php echo$diklat?>" size="30" maxlength="255" /></td>
</tr>
<tr>
	<td colspan="2" align="center" bgcolor="#EAEAEA">
    <input type="submit" class="but" name="btSimpan" value="Simpan" />&nbsp;
    <input type="button" class="but" name="btClose" value="Tutup" onClick="window.close()" />
    </td>
</tr>
</table>
</form>
<?php
CloseDb();
?>
</body>
</html>