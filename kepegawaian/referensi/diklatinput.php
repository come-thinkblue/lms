<?php
/**[N]**
 * LMS MAN Kota Blitar 
 **[N]**/ ?>
<?php
require_once("../include/config.php");
require_once("../include/db_functions.php");
require_once("../include/common.php");
require_once('../include/theme.php');
require_once("../include/sessioninfo.php");

OpenDb();

$rootid = $_REQUEST['rootid'];
$tingkat = $_REQUEST['tingkat'];
$jenis = $_REQUEST['jenis'];

if (isset($_REQUEST['btSimpan'])) 
{
	$diklat = $_REQUEST['txDiklat'];
	$sql = "INSERT INTO diklat SET rootid=$rootid, tingkat=$tingkat, diklat='$diklat', jenis='$jenis'";
	QueryDb($sql);
	CloseDb($sql);
	?>
	<script language="javascript">
		opener.RefreshPage(<?php echo$rootid?>);
		window.close();
    </script>    
    <?php
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tambah Diklat</title>
<link rel="stylesheet" href="../style/style<?php echo GetThemeDir2()?>.css" />
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#ffffff">

<?php
OpenDb();
?>
<form name="main" method="post">
<input type="hidden" name="rootid" id="rootid" value="<?php echo$rootid?>" />
<input type="hidden" name="tingkat" id="tingkat" value="<?php echo$tingkat?>" />
<input type="hidden" name="jenis" id="jenis" value="<?php echo$jenis?>" />
<table border="0" cellpadding="0" cellspacing="5" width="100%" id="table56">
<tr>
	<td class="header" colspan="2" align="center">Tambah Diklat</td>
</tr>
<?php 
if ($rootid != 0) { 
	$sql = "SELECT diklat FROM diklat WHERE replid = $rootid";
	$result = QueryDb($sql);
	$row = mysql_fetch_row($result);
	$diklat = $row[0];
?>
<tr>
	<td align="right" width="120">Diklat :</td>
    <td align="left"><input type="text" name="txParentDiklat" id="txParentDiklat" readonly size="30" maxlength="255" value="<?php echo$diklat?>" /></td>
</tr>
<?php } ?>
<tr>
	<td align="right" width="120">Sub Diklat :</td>
    <td align="left"><input type="text" name="txDiklat" id="txDiklat" size="30" maxlength="255" /></td>
</tr>
<tr>
	<td colspan="2" align="center" bgcolor="#EAEAEA">
    <input type="submit" class="but" name="btSimpan" value="Simpan" />&nbsp;
    <input type="button" class="but" name="btClose" value="Tutup" onClick="window.close()" />
    </td>
</tr>
</table>
</form>
<?php
CloseDb();
?>
</body>
</html>