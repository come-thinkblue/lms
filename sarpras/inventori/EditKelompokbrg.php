<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/config.php');
require_once('../include/db_functions.php');
OpenDb();
$idkelompok = $_REQUEST[idkelompok];
$sql = "SELECT * FROM $g_db_keuangan.kelompok_brg_lab WHERE id_klmpk_lab='$_REQUEST[idkelompok]'";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
$idlab = $row[id_lab];
if (isset($_REQUEST[idlab]))
	$idlab = $_REQUEST[idlab];
$kelompokname = $row[kelompok];
if (isset($_REQUEST[kelompokname]))
	$kelompokname = $_REQUEST[kelompokname];
$keterangan = $row[keterangan];
if (isset($_REQUEST[keterangan]))
	$keterangan = $_REQUEST[keterangan];
if (isset($_REQUEST['Simpan'])){
	$sql = "SELECT * FROM $g_db_keuangan.kelompok_brg_lab WHERE kelompok='$kelompokname' AND id_lab='$idlab'";
	if (@mysql_num_rows(QueryDb($sql))>0){
		?>
        <script language="javascript">
			alert ('Kelompok <?php echo$_REQUEST[kelompokname]?> sudah digunakan!');
        </script>
        <?php
	} else {
		QueryDb("UPDATE $g_db_keuangan.kelompok_brg_lab SET kelompok='$kelompokname', keterangan='$keterangan', id_lab='$idlab' WHERE id_klmpk_lab='$_REQUEST[idkelompok]'");
		?>
        <script language="javascript">
			parent.opener.GetFresh();
			window.close();
        </script>
        <?php
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../style/style.css" />
<title>Edit Kelompok Barang</title>
<script language="javascript">
function validate(){
	var namakelompok = document.getElementById('kelompokname').value;
	if (namakelompok.length==0){
		alert ('Anda harus mengisikan Nama Kelompok!'); 
		document.getElementById('kelompokname').focus();
		return false;
	}
	return true;
}
</script>
</head>
<body onLoad="document.getElementById('kelompokname').focus()">
<fieldset style="border:#336699 1px solid; background-color:#eaf4ff" >
<legend style="background-color:#336699; color:#FFFFFF; font-size:12px; font-weight:bold; padding:5px; ">&nbsp;Ubah&nbsp;Kelompok&nbsp;</legend>
<form action="EditKelompokbrg.php" onSubmit="return validate()" method="post">
<input type="hidden" name="idkelompok" id="idkelompok" value="<?php echo$_REQUEST[idkelompok]?>" />
<input type="hidden" name="idlab" id="idlab" value="<?php echo$idlab?>" />
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td>Nama Kelompok</td>
    <td><input name="kelompokname" id="kelompokname" type="text" maxlength="45" style="width:100%" value="<?php echo$kelompokname?>" /></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td><textarea name="keterangan" id="keterangan" style="width:100%" rows="5"><?php echo$keterangan?></textarea></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input class="but" type="submit" name="Simpan" value="Simpan" />&nbsp;&nbsp;<input type="button" value="Batal" onClick="window.close()" class="but" /></td>
  </tr>
</table>
</form>
</fieldset>
</body>
<?php
CloseDb();
?>
</html>