<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

OpenDb();
$sql = "SELECT * FROM $g_db_keuangan.barang_lab WHERE id_brglab='$_REQUEST[idbarang]'";
$result = QueryDb($sql);
$row = mysql_fetch_array($result);
$kode = $row[kode];
$nama = $row[nama];
$jumlah = (int)$row[jumlah];
$kondisi = $row[id_kondisi];
$keterangan = $row[keterangan];
$sumber= $row[sumber];
$tgl = $row[tgl];
$harga = (int)$row[harga];
$total = $harga * $jumlah;

if (isset($_REQUEST[Simpan])){
	
		$foto=$_FILES["foto"];
		$uploadedfile = $foto['tmp_name'];
		$uploadedtypefile = $foto['type'];
		$uploadedsizefile = $foto['size'];
		if (strlen($uploadedfile)!=0)
		{
			$tmp_path = realpath(".") . "/../../temp";
			$tmp_exists = file_exists($tmp_path) && is_dir($tmp_path);
			if (!$tmp_exists)
				mkdir($tmp_path, 0755);
		
			$filename = "$tmp_path/ed-inv-tmp.jpg";
			ResizeImage($foto, 120, 90, 70, $filename);

			$fh = fopen($filename, "r");
			$foto_data = addslashes(fread($fh, filesize($filename)));
			fclose($fh);
			
			$isifoto = ", foto='$foto_data'";
		} else {
			$isifoto = "";
		}
		
		$tgl =$_REQUEST[tanggal];
		
		$sql = "UPDATE $g_db_keuangan.barang_lab
				   SET kode='".trim($_REQUEST[kode])."', nama='".trim($_REQUEST[nama])."',
					   jumlah='".trim($_REQUEST[jumlah])."',id_kondisi='".addslashes(trim($_REQUEST[kondisi]))."',tgl='$tgl',sumber='$sumber',
					   keterangan='".addslashes(trim($_REQUEST[keterangan]))."',id_klmpk_lab='$_REQUEST[idkelompok]',
					   harga='$_REQUEST[angkaharga]' $isifoto
				 WHERE id_brglab='$_REQUEST[idbarang]'";
		$result = QueryDb($sql);
		if ($result){
			?>
            <script language="javascript">
				parent.opener.GetFresh();
				window.close();
            </script>
            <?php
		}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar [Tambah Data Pelanggaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/cal2.js"></script>
<script language="javascript" src="../script/cal_conf2.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">
function TakeDate(elementid){
	var addr = "../library/cals.php?elementid="+elementid;
	newWindow(addr, 'CariTanggal','338','216','resizable=0,scrollbars=0,status=0,toolbar=0')
}
function AcceptDate(date,elementid){
	document.getElementById(elementid).value=date;
}
function validate(){
	var kode = document.getElementById('kode').value;
	var nama = document.getElementById('nama').value;
	var jumlah = document.getElementById('jumlah').value;
	var tgl = document.getElementById('tgl').value;
	var foto = document.getElementById('foto').value;
	if (kode.length==0){
		alert ('Anda harus mengisikan data untuk Kode Barang!');
		document.getElementById('kode').focus();
		return false;
	} else {
		if (kode.length>20){
			alert ('Kode Barang maksimal 20 karakter!');
			document.getElementById('kode').focus();
			return false;
		}
	}
	
	if (nama.length==0){
		alert ('Anda harus mengisikan data untuk Nama Barang!');
		document.getElementById('nama').focus();
		return false;
	} else {
		if (nama.length>50){
			alert ('Nama Barang maksimal 50 karakter!');
			document.getElementById('nama').focus();
			return false;
		}
	}
	
	if (jumlah.length==0){
		alert ('Anda harus mengisikan nilai untuk Jumlah Barang!');
		document.getElementById('jumlah').focus();
		return false;
	} else {
		if (jumlah.length>10){
			alert ('Jumlah Barang maksimal 10 digit!');
			document.getElementById('jumlah').focus();
			return false;
		}
		if (isNaN(jumlah)){
			alert ('Jumlah Barang harus berupa bilangan!');
			document.getElementById('jumlah').value="";;
			document.getElementById('jumlah').focus();
			return false;
		}
	}
	
	if (tgl.length==0){
		alert ('Anda harus mengisikan data untuk Tanggal Perolehan!');
		document.getElementById('tgl').focus();
		return false;
	} else {
		if (tgl.length>10){
			alert ('Tanggal Perolehan maksimal 10 karakter!');
			document.getElementById('tgl').focus();
			return false;
		}
	}

	if (foto.length>0){
		var ext = "";
		var i = 0;
		var string4split='.';

		z = foto.split(string4split);
		ext = z[z.length-1];
		
		if (ext!='JPG' && ext!='jpg' && ext!='Jpg' && ext!='JPg' && ext!='JPEG' && ext!='jpeg'){
			alert ('Format Gambar harus ber-extensi jpg atau JPG !');
			//document.getElementById('cover').value='';
	
			return false;
		} 
	}
}

IsNumber = function(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function CalculatePrice()
{
	var jumlah = trim(document.getElementById("jumlah").value);
	var harga = trim(document.getElementById("harga").value);
	harga = rupiahToNumber(harga);
	
	jumlah = IsNumber(jumlah) ? jumlah : 0;
	harga = IsNumber(harga) ? harga : 0;
	
	var total = jumlah * harga;
	document.getElementById("total").value = numberToRupiah(total);
}

function salinharga()
{	
	var harga = document.getElementById("harga").value;
	document.getElementById("angkaharga").value = harga;
}
</script>
</head>
<body onLoad="document.getElementById('kode').focus()">
<fieldset style="border:#336699 1px solid; background-color:#eaf4ff" >
<legend style="background-color:#336699; color:#FFFFFF; font-size:10px; font-weight:bold; padding:5px">&nbsp;Ubah&nbsp;Barang&nbsp;</legend>
<form action="EditBaranglab.php" name="main" onSubmit="return validate()">
<input type="hidden" name="idkelompok" id="idkelompok" value="<?php echo$_REQUEST[idkelompok]?>" />
<input type="hidden" name="idbarang" id="idbarang" value="<?php echo$_REQUEST[idbarang]?>" />
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
    <td width="14%" align="right"><strong>Kode&nbsp;Barang</strong></td>
    <td width="86%"><input type="text" id="kode" name="kode" maxlength="20" value="<?php echo stripslashes($kode)?>" readonly/></td>
    <td width="86%" rowspan="5" align="center" valign="middle"><img src="gambar.php?table=$g_db_keuangan.barang_lab&idbarang=<?php echo$_REQUEST[idbarang]?>" style="padding:2px" /></td>
  </tr>
  <tr>
   <td align="right"><strong>Nama&nbsp;Barang</strong></td>
    <td><input type="text" id="nama" name="nama" style="width:95%" maxlength="50" value="<?php echo stripslashes($nama)?>" /></td>
  </tr>
  <tr>
   <td align="right"><strong>Jumlah</strong></td>
    <td><input type="text" id="jumlah" name="jumlah" size="5" maxlength="10" value="<?php echo stripslashes($jumlah)?>" />
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Harga&nbsp;Satuan</strong></td>
    <td>
		<input type="text" id="harga" name="harga" size="15" maxlength="14" value="<?php echo$harga?>" onblur="CalculatePrice(); formatRupiah('harga');" onfocus="unformatRupiah('harga')" onkeyup="salinharga();" />
		<input type="hidden" id="angkaharga" name="angkaharga" size="15" maxlength="14" value="<?php echo$harga?>" />
	</td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Kondisi</strong></td>
    <td><select name="kondisi" id="kondisi">
		<?php
		$sql2=mysql_query("select nama from $g_db_keuangan.kondisi where id_kondisi='$kondisi'");
		$data=mysql_fetch_array($sql2);
		?>
			<option value="<?php echo$kondisi?>"><?php echo$data[0]?></option>
		<?php
		$sql=mysql_query("select * from $g_db_keuangan.kondisi order by id_kondisi asc");
		while($hasil=mysql_fetch_array($sql)){
		?>
			<option value="<?php echo$hasil[0]?>"><?php echo$hasil[1]?></option>
		<?php
		}
		?>
		</select>
	</td>
  </tr>
<tr>
			<td width="14%" align="right"><strong>Tanggal&nbsp;Pengadaan</strong></td>
			<td><input type="text" onClick="showCal('Calendar2');" style="width:140px;" name="tanggal" id ="tanggal" readonly class="disabled" value = "<?php echo$tgl?>" />
         
				<a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
			</td>
</tr>
<tr>
    <td align="right"><strong>Sumber Dana</strong></td>
    <td><input type="text" id="sumber" name="sumber" style="width:95%" maxlength="50" value="<?php echo stripslashes($sumber)?>" /></td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Foto</strong></td>
    <td><input type="file" id="foto" name="foto" /></td>
  </tr>
  <tr>
   <td align="right">Keterangan</td>
    <td colspan="2"><textarea name="keterangan" rows="4" id="keterangan"  style="width:95%"><?php echo stripslashes($keterangan)?></textarea></td>
    </tr>
  <tr>
    <td colspan="3" align="center"><input name="Simpan" type="submit" class="but" value="Simpan" />
    &nbsp;&nbsp;<input name="Batal" type="button" class="but" onClick="window.close()" value="Batal" /></td>
  </tr>
</table>
</form>
</fieldset>
</body>
<?php
CloseDb();
?>
</html>