<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/common.php');
require_once('../library/imageresizer.php');

OpenDb();
 
//$sql = "SELECT MONTH(NOW()), YEAR(NOW()), DAY(NOW())";
//$result = QueryDb($sql);
//$row = mysql_fetch_row($result);
//$now = $row[2]."-".$row[0]."-".$row[1];
//$tgl = $now;
//if (isset($_REQUEST[tgl]))
	//$tgl = $_REQUEST[tgl];
	
$tgl = date(d)."-".date(m)."-".date(Y);
if (isset($_REQUEST['tanggal']))
	$tgl= $_REQUEST['tanggal'];
if (isset($_REQUEST[Simpan]))
{
	$sql = "SELECT kode FROM $g_db_keuangan.barang WHERE kode='$_REQUEST[kode]'";
	$result = QueryDb($sql);
	$num = @mysql_num_rows($result);
	if ($num>0){
		?>
		<script language="javascript">
			alert('Kode Barang \'<?php echo$_REQUEST[kode]?>\' sudah digunakan!');
        </script>
		<?php
	} else {
		$foto=$_FILES["foto"];
		$uploadedfile = $foto['tmp_name'];
		$uploadedtypefile = $foto['type'];
		$uploadedsizefile = $foto['size'];
		if (strlen($uploadedfile) != 0)
		{
			$tmp_path = realpath(".") . "/../../temp";
			$tmp_exists = file_exists($tmp_path) && is_dir($tmp_path);
			if (!$tmp_exists)
				mkdir($tmp_path, 0755);
		
			$filename = "$tmp_path/ad-inv-tmp.jpg";
			ResizeImage($foto, 120, 90, 70, $filename);

			$fh = fopen($filename,"r");
			$foto_data = addslashes(fread($fh, filesize($filename)));
			fclose($fh);
			
			$isifoto = ", foto='$foto_data'";
		} else {
			$isifoto="";
		}
		
		$tgl = MySqlDateFormat($_REQUEST[tgl]);
		$sumber=$_REQUEST['sumber'];
		$sql = "INSERT INTO $g_db_keuangan.barang_lab SET kode='".trim($_REQUEST[kode])."', nama='".trim($_REQUEST[nama])."',
					   jumlah='".trim($_REQUEST[jumlah])."',id_kondisi='".addslashes(trim($_REQUEST[kondisi]))."',tgl='$tgl', sumber='$sumber',
					   keterangan='".addslashes(trim($_REQUEST[keterangan]))."',id_klmpk_lab='$_REQUEST[idkelompok]',
					   harga='$_REQUEST[harga]' $isifoto";
		$result = QueryDb($sql);
		if ($result){
			?>
            <script language="javascript">
				parent.opener.GetFresh();
				window.close();
            </script>
            <?php
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tambah Barang</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<link href="../style/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/string.js"></script>
<script language="javascript" src="../script/rupiah.js"></script>
<script language="javascript" src="../script/cal2.js"></script>
<script language="javascript" src="../script/cal_conf2.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript">
//function TakeDate(elementid){
	//var addr = "../library/cals.php?elementid="+elementid;
	//newWindow(addr, 'CariTanggal','338','216','resizable=0,scrollbars=0,status=0,toolbar=0')
//}
//function AcceptDate(date,elementid){
	//document.getElementById(elementid).value=date;
//}
function validate(){
	var kode = document.getElementById('kode').value;
	var nama = document.getElementById('nama').value;
	var jumlah = document.getElementById('jumlah').value;
	var tgl = document.getElementById('tgl').value;
	var foto = document.getElementById('foto').value;
	
	if (kode.length==0){
		alert ('Anda harus mengisikan data untuk Kode Barang!');
		document.getElementById('kode').focus();
		return false;
	} 
	if (nama.length==0){
		alert ('Anda harus mengisikan data untuk Nama Barang!');
		document.getElementById('nama').focus();
		return false;
	}
	
}

IsNumber = function(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function CalculatePrice()
{
	var jumlah = trim(document.getElementById("jumlah").value);
	var harga = trim(document.getElementById("harga").value);
	harga = rupiahToNumber(harga);
	
	jumlah = IsNumber(jumlah) ? jumlah : 0;
	harga = IsNumber(harga) ? harga : 0;
	
	var total = jumlah * harga;
	document.getElementById("total").value = numberToRupiah(total);
}

function salinharga()
{	
	var harga = document.getElementById("harga").value;
	document.getElementById("angkaharga").value = harga;
}
</script>
</head>
<body onLoad="document.getElementById('kode').focus()">
<fieldset style="border:#336699 1px solid; background-color:#eaf4ff" >
<legend style="background-color:#336699; color:#FFFFFF; font-size:10px; font-weight:bold; padding:5px">&nbsp;Tambah&nbsp;Barang&nbsp;</legend>
<form action="AddBaranglab.php" method="post" enctype="multipart/form-data" onSubmit="return validate()">
<input type="hidden" name="idkelompok" id="idkelompok" value="<?php echo$_REQUEST[idkelompok]?>" />
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="14%" align="right"><strong>Kode&nbsp;Barang</strong></td>
    <td width="86%"><input type="text" id="kode" name="kode" maxlength="20" /></td>
  </tr>
  <tr>
    <td align="right"><strong>Nama&nbsp;Barang</strong></td>
    <td><input type="text" id="nama" name="nama" style="width:95%" maxlength="50" /></td>
  </tr>
  <tr>
    <td align="right"><strong>Jumlah</strong></td>
    <td>
		<input type="text" id="jumlah" name="jumlah" size="5" maxlength="10" onblur="CalculatePrice()" />
	</td>
  </tr>
  <tr>
    <td align="right">Harga Satuan</td>
    <td>
		<input type="text" id="harga" name="harga" size="15" maxlength="14" onblur="CalculatePrice(); formatRupiah('harga');" onfocus="unformatRupiah('harga')" onkeyup="salinharga();" />
		<input type="hidden" id="angkaharga" name="angkaharga" size="15" maxlength="14" />
	</td>
  </tr>
  <tr>
    <td align="right">Kondisi</td>
    <td><select name="kondisi" id="kondisi">
		<?php
		$sql=mysql_query("select * from $g_db_keuangan.kondisi order by id_kondisi asc");
		while($hasil=mysql_fetch_array($sql)){
		?>
			<option value="<?php echo$hasil[0]?>"><?php echo$hasil[1]?></option>
		<?php
		}
		?>
		</select>
	</td>
  </tr>
  <tr>
    <td align="right"><strong>Tanggal Pembelian</strong></td>
    <td><input type="text" onClick="showCal('Calendar2');" style="width:140px;" name="tanggal" id ="tanggal" readonly class="disabled" value = "<?php echo$tgl?>" />
         
				<a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
			 </tr>
  <tr>
    <td align="right"><strong>Sumber Dana</strong></td>
    <td><input type="text" id="sumber" name="sumber" size="20" /></td></td>
  </tr>
  <tr>
    <td align="right">Foto</td>
    <td><input type="file" id="foto" name="foto" /></td>
  </tr>
  <tr>
    <td align="right">Keterangan</td>
    <td><textarea name="keterangan" rows="2" id="keterangan"  style="width:95%"></textarea></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Simpan" type="submit" class="but" value="Simpan" />
    &nbsp;&nbsp;<input name="Batal" type="button" class="but" onClick="window.close()" value="Batal" /></td>
  </tr>
</table>
</form>
</fieldset>
<?php
CloseDb();
?>
</body>
</html>