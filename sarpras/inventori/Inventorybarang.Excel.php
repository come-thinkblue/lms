<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessionchecker.php');
require_once('../include/common.php');
require_once('../include/rupiah.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php'); 

$idkelompok = $_REQUEST['idkelompok'];

OpenDb();

$sql = "SELECT a.kelompok, b.nama FROM $g_db_keuangan.kelompok_brg_lab a, $g_db_keuangan.data_lab b WHERE id_klmpk_lab='$idkelompok' AND a.id_lab=b.id_lab";
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$namakelompok = $row[0];
$ruang=$row[1];

header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Inventory'.$idkelompok.'.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMAN 1 Malang KEU [Inventory]</title>
</head>

<body>
<center><font size="4" face="Verdana"><strong></strong>SARANA DAN PRASARANA</font><br /> 
</center>
<br /><br />
<table border="0">
<tr>
	<td width="90"><font size="2" face="Arial"><strong>Ruang </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$ruang ?>
    </strong></font></td>
</tr>
<tr>
	<td width="90"><font size="2" face="Arial"><strong>Kelompok </strong></font></td>
    <td><font size="2" face="Arial"><strong>: 
      <?php echo$namakelompok ?>
    </strong></font></td>
</tr>
</table>
<br />

<table border="1" style="border-collapse:collapse" cellpadding="5" width="100%" class="tab" bordercolor="#000000">
<tr height="30">
	<td width="25" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">No</font></strong></td>
    <td width="120" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Kode</font></strong></td>
    <td width="250" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Barang</font></strong></td>
    <td width="250" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Tgl Pengadaan</font></strong></td>
    <td width="250" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Kondisi</font></strong></td>
    <td width="250" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Jumlah</font></strong></td>
    <td width="250" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Harga</font></strong></td>
    <td width="250" align="center" bgcolor="#CCCCCC" class="header"><strong><font size="2" face="Arial">Total</font></strong></td>
</tr>
<?php
$sql = "SELECT * FROM $g_db_keuangan.barang_lab WHERE id_klmpk_lab='$idkelompok'";
$result = QueryDb($sql);
$no = 0;
while($row = mysql_fetch_array($result))
{
    $no += 1;
    
    $jumlah = (int)$row[jumlah];
    $harga = (int)$row[harga];
    $total = $jumlah * $harga;
	$sql2=mysql_query("select nama from $g_db_keuangan.kondisi where id_kondisi='$row[id_kondisi]'");
		$data=mysql_fetch_array($sql2);
		$kondisi=$data[0];
?>
<tr>
    <td><?php echo$no?></td>
    <td><?php echo$row[kode]?></td>
    <td><?php echo$row[nama]?></td>
    <td><?php echo$row[tgl]?></td>
    <td><?php echo$kondisi?></td>
    <td><?php echo$jumlah . " " . $row[satuan]?></td>
    <td><?php echo formatRupiah($harga)?></td>
    <td><?php echo formatRupiah($total)?></td>
</tr>
<?php
}
CloseDb();
?>
</table>

</body>
</html>
