<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/config.php');
require_once('../include/db_functions.php');
OpenDb();
$idlab = $_REQUEST[idlab];
if (isset($_REQUEST['Simpan'])){
	$sql = "SELECT * FROM $g_db_keuangan.kelompok_brg_lab WHERE kelompok='".addslashes(trim($_REQUEST[kelompokname]))."' AND id_lab='$_REQUEST[idlab]'";
	if (@mysql_num_rows(QueryDb($sql))>0){
		?>
        <script language="javascript">
			alert ('Kelompok <?php echo$_REQUEST[kelompokname]?> sudah digunakan!');
        </script>
        <?php
	} else {
		QueryDb("INSERT INTO $g_db_keuangan.kelompok_brg_lab SET kelompok='".addslashes(trim($_REQUEST[kelompokname]))."', keterangan='".addslashes(trim($_REQUEST[keterangan]))."',id_lab='$_REQUEST[idlab]'");
		?>
        <script language="javascript">
			parent.opener.GetFresh();
			window.close();
        </script>
        <?php
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../style/style.css" />
<title>Tambah Kelompok Barang</title>
<script language="javascript">
function validate(){
	var namakelompok = document.getElementById('kelompokname').value;
	if (namakelompok.length==0){
		alert ('Anda harus mengisikan Nama Kelompok!'); 
		document.getElementById('kelompokname').focus();
		return false;
	}
	return true;
}
</script>
</head>
<body onLoad="document.getElementById('kelompokname').focus()">
<fieldset style="border:#336699 1px solid; background-color:#eaf4ff" >
<legend style="background-color:#336699; color:#FFFFFF; font-size:12px; font-weight:bold; padding:5px; ">&nbsp;Tambah&nbsp;Kelompok&nbsp;</legend>
<form action="AddKelompokbrg.php" onSubmit="return validate()" method="post">
<input type="hidden" name="idlab" id="idlab" value="<?php echo$_REQUEST[idlab]?>" />
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td>Nama Kelompok</td>
    <td><input name="kelompokname" id="kelompokname" type="text" style="width:100%" value="<?php echo stripslashes($_REQUEST[kelompokname])?>" /></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td><textarea name="keterangan" id="keterangan" style="width:100%" rows="5"><?php echo stripslashes($_REQUEST[keterangan])?></textarea></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input class="but" type="submit" name="Simpan" value="Simpan" />&nbsp;&nbsp;<input type="button" value="Batal" onClick="window.close()" class="but" /></td>
  </tr>
</table>
</form>
</fieldset>
</body>
<?php
CloseDb();
?>
</html>