<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessionchecker.php');
require_once('../include/common.php');
require_once('../include/rupiah.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php'); 

$varkolom=4;

$idkelompok = "";
if (isset($_REQUEST['idkelompok']))
	$idkelompok = $_REQUEST['idkelompok'];
	
$departemen = "SMAN 1 MALANG";

OpenDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMAN 1 Malang KEU [Jenis Penerimaan]</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>

<center><font size="4"><strong>SARANA DAN PRASANA</strong></font><br /> </center><br /><br />
<table border="0">
<tr>
	<td width="90"><strong>Ruang</strong></td>
    <td><strong>:
<?php	$sql = "SELECT a.kelompok, b.nama FROM $g_db_keuangan.kelompokbarang a, $g_db_keuangan.data_ruang b WHERE replid='$idkelompok' AND a.id_ruang=b.id_ruang";
    $result = QueryDb($sql);
    $row = @mysql_fetch_row($result);
    $namakelompok = $row[0];
	$ruang=$row[1];
	echo  $ruang; ?>
    </strong>
    </td>
</tr>
<tr>
<td width="90"><strong>Kelompok</strong></td><td><strong>:<?php echo$namakelompok?></strong></td>
</tr>
</table>
<br />

<?php
$sql = "SELECT * FROM $g_db_keuangan.barang WHERE idkelompok='$idkelompok'";
$result = QueryDb($sql);
$num = @mysql_num_rows($result);
$total = ceil(mysql_num_rows($result)/(int)$varkolom);
if ($num > 0)
{   ?>
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
<?php
    $cnt=1;
    while ($row = @mysql_fetch_array($result))
    {
    	if ($cnt==1 || $cnt%(int)$varkolom==1)
            echo "<tr>";
	
        $jumlah = (int)$row[jumlah];
        $harga = (int)$row[satuan];
        $total = $jumlah * $harga;	
		$sql2=mysql_query("select nama from $g_db_keuangan.kondisi where id_kondisi='$row[id_kondisi]'");
		$data=mysql_fetch_array($sql2);
		$kondisi=$data[0];
    ?>
    <td valign="top" align="center">
        <div id="div<?php echo$row[replid]?>" style="padding:5px; width:200px; margin:5px; border:2px solid #eaf4ff; cursor:default">
        <div align="left">
            <span style="font-family:Arial; font-size:14px; font-weight:bold; color:#990000"><?php echo$row[kode]?></span><br />
            <span style="font-family:Arial; font-size:12px; font-weight:bold; color:#006600; cursor:pointer"><?php echo$row[nama]?></span><br />
        </div>
        <img src="gambar.php?table=$g_db_keuangan.barang&replid=<?php echo$row[replid]?>"  style="padding:2px" />
        <div align="left">
            Jumlah: <?php echo$jumlah?>&nbsp;@<?php echo formatRupiah($harga)?><br />
            Total: <?php echo formatRupiah($total)?><br>
            Tanggal: <?php echo$row[tglperolehan]?><br />
			Kondisi :<?php echo$kondisi?><br />
        </div>
        </div>
    </td>
<?php
    if ($num < $varkolom)
    {
    	if ($num==1)
    		echo  "<td width='157'>&nbsp;</td><td width='157'>&nbsp;</td><td width='157'>&nbsp;</td><td width='157'>&nbsp;</td>";
    	elseif ($num==2)
    		echo  "<td width='157'>&nbsp;</td><td width='157'>&nbsp;</td><td width='157'>&nbsp;</td>";	
    	elseif ($num==3)
    		echo  "<td width='157'>&nbsp;</td><td width='157'>&nbsp;</td>";
    	elseif ($num==4)
    		echo  "<td width='157'>&nbsp;</td>";
    }
    
    if ($cnt%(int)$varkolom==0)
        echo "</tr>";
    
    $cnt++;
}
?>
</table>
<?php
}
else
{
    ?>
    <div align="center"><span style="font-family:verdana; font-size:12px; font-style:italic; color:#666666">Tidak ada Data Barang Untuk Kelompok <?php echo stripslashes($namakelompok)?></span></div>
<?php
}
?>

</td></tr>
</table>
<?php
CloseDb();
?>
</body>
</html>
<script language="javascript">window.print();</script>