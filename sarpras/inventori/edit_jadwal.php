<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
//require_once('../cek.php');

$replid = $_REQUEST['replid'];
$ERROR_MSG = "";

if (isset($_REQUEST['Simpan'])) {
$ruang = $_REQUEST['ruang'];
$gedung = $_REQUEST['gedung'];
$nip = $_REQUEST['nipwali'];
$ket = $_REQUEST['ket'];
	OpenDb();
		$nama ="update data_lab set nama='$ruang', lokasi='$gedung', penjab='$nip', ket='$ket' where id_lab='$replid'";
		$result = QueryDb($nama);
		CloseDb();
	
		if ($result) { ?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
<?php		}		
	
}

OpenDb();

$no = 1;
$nama =mysql_query("SELECT * FROM jadwal_lab where id_jadwal='$replid'");
$hasil=mysql_fetch_array($nama);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar [Ubah Kategori Pelanggaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

function validate() {
	return validateEmptyText('kategori', 'kategori');
}

function caripegawai() {
	newWindow('../library/pegawai.php?flag=0&bagian=Akademik', 'CariPegawai','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function acceptPegawai(nip, nama, flag) {
	document.getElementById('nipwali').value = nip;
	document.getElementById('nip').value = nip;
	document.getElementById('nama').value = nama;
	document.getElementById('namawali').value = nama;
	document.getElementById('kapasitas').focus();
}
function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4"  onload="document.getElementById('kategori').focus();">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
		.: Ubah Data Jadwal Lab :.
	</div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
<form name="main" onSubmit="return validate()" action="edit_lab.php">
<input type="hidden" name="replid" id="replid" value="<?php echo$replid ?>" />
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
	<tr>
	<?php
		OpenDb();
        $nama=mysql_query("SELECT id_lab, nama from data_lab order by id_lab asc ");
	?>
	<td width="120"><strong>Laboratorium</strong></td>
    <td><select name="lab" id="lab">
		<?php
			while($hasil=mysql_fetch_array($nama)){
			?>
			<option value="<?php echo$hasil[0]?>"><?php echo$hasil[1]?></option>
			<?php
			}
		?>
		</select>
	</td>
</tr>
<tr>
	<td width="14%" align="left"><strong>Tanggal&nbsp;Penggunaan</strong></td>
	<td><input type="text" onClick="showCal('Calendar2');" style="width:140px;" name="tanggal" id ="tanggal" readonly class="disabled" value = "<?php echo$tgl?>" />
        <a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
	</td>
</tr>
<tr>
    <td width="14%" align="left"><strong>Jam</strong></td>
    <td><input type="text" id="jam" name="jam" maxlength="2" size="2" />:<input type="text" id="menit" name="menit" size="2" maxlength="2" />
		<select name="waktu" id="waktu"><option value="am">am</option><option value="pm">pm</option></select>
	</td>
  </tr>
  <tr>
    <td width="14%" align="left"><strong>Pengguna</strong></td>
    <td><input type="text" id="pengguna" name="pengguna" /></td>
  </tr>

	<tr>
		<td colspan="2" align="center">
			<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" />&nbsp;
			<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
		</td>
	</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>
<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo$ERROR_MSG?>');
</script>
<?php } ?>

<!-- Pilih inputan pertama -->

</body>
</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("kategori");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("jenis");
var sprytextfield1 = new Spry.Widget.ValidationTextField("poin");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("keterangan");
</script>