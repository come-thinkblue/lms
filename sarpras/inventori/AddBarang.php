<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
//require_once('../cek.php');


$idkategori = "";
if (isset($_REQUEST['idkategori']))
	$idkategori = $_REQUEST['idkategori'];
	
$tgl = date(d)."-".date(m)."-".date(Y);
if (isset($_REQUEST['tanggal']))
	$tgl= $_REQUEST['tanggal'];
	
$ERROR_MSG = "";
if (isset($_REQUEST['Simpan'])) {
	OpenDb();
	$sql = "SELECT kode FROM $g_db_keuangan.barang WHERE kode='$_REQUEST[kode]'";
	$result = QueryDb($sql);
	$num = @mysql_num_rows($result);
	if ($num>0){
		?>
		<script language="javascript">
			alert('Kode Barang \'<?php echo$_REQUEST[kode]?>\' sudah digunakan!');
        </script>
		<?php
	} else {
		$foto=$_FILES["foto"];
		$uploadedfile = $foto['tmp_name'];
		$uploadedtypefile = $foto['type'];
		$uploadedsizefile = $foto['size'];
		if (strlen($uploadedfile) != 0)
		{
			$tmp_path = realpath(".") . "/../../temp";
			$tmp_exists = file_exists($tmp_path) && is_dir($tmp_path);
			if (!$tmp_exists)
				mkdir($tmp_path, 0755);
		
			$filename = "$tmp_path/ad-inv-tmp.jpg";
			ResizeImage($foto, 120, 90, 70, $filename);

			$fh = fopen($filename,"r");
			$foto_data = addslashes(fread($fh, filesize($filename)));
			fclose($fh);
			
			$isifoto = ", foto='$foto_data'";
		} else {
			$isifoto="";
		}
		
		$tgl = MySqlDateFormat($_REQUEST[tgl]);
		$sumber=$_REQUEST['sumber'];
		$sql = "INSERT INTO $g_db_keuangan.barang SET kode='".trim($_REQUEST[kode])."', nama='".trim($_REQUEST[nama])."',
					   jumlah='".trim($_REQUEST[jumlah])."',id_kondisi='".addslashes(trim($_REQUEST[kondisi]))."',tglperolehan='$_REQUEST[tanggal]', sumber='$sumber',
					   keterangan='".addslashes(trim($_REQUEST[keterangan]))."',idkelompok='$_REQUEST[idkelompok]',
					   satuan='$_REQUEST[harga]' $isifoto";
		$result = QueryDb($sql);
		if ($result){
			?>
            <script language="javascript">
				parent.opener.GetFresh();
				window.close();
            </script>
            <?php
		}
	}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar [Tambah Data Pelanggaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/cal2.js"></script>
<script language="javascript" src="../script/cal_conf2.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

function validate() {
	return validateEmptyText('angkatan', 'Nama Angkatan') && 
		   validateMaxText('keterangan', 255, 'Keterangan');			
}



function change_jenis() {
	var idkategori = document.getElementById('idkategori').value;
	var nis = document.getElementById('nis').value;
	document.location.href = "tambah_pelanggaran.php?idkategori="+idkategori+"&nis="+nis;
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
    .: Tambah Data Barang :.
    </div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
<form action="AddBarang.php" name="main" onSubmit="return validate()">
<input type="hidden" name="idkelompok" id="idkelompok" value="<?php echo$_REQUEST[idkelompok]?>" />
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
    <td width="14%" align="right"><strong>Kode&nbsp;Barang</strong></td>
    <td width="86%"><input type="text" id="kode" name="kode" maxlength="20" /></td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Nama&nbsp;Barang</strong></td>
    <td><input type="text" id="nama" name="nama" style="width:95%" maxlength="50" /></td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Jumlah</strong></td>
    <td>
		<input type="text" id="jumlah" name="jumlah" size="5" maxlength="10" onblur="CalculatePrice()" />
	</td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Harga&nbsp;Satuan</strong></td>
    <td>
		<input type="text" id="harga" name="harga" size="15" maxlength="14" onblur="CalculatePrice(); formatRupiah('harga');" onfocus="unformatRupiah('harga')" onkeyup="salinharga();" />
		<input type="hidden" id="angkaharga" name="angkaharga" size="15" maxlength="14" />
	</td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Kondisi</strong></td>
    <td><select name="kondisi" id="kondisi">
		<?php
		OpenDb();
		$sql=mysql_query("select * from $g_db_keuangan.kondisi order by id_kondisi asc");
		while($hasil=mysql_fetch_array($sql)){
		?>
			<option value="<?php echo$hasil[0]?>"><?php echo$hasil[1]?></option>
		<?php
		}
		?>
		</select>
	</td>
  </tr>
<tr>
			<td width="14%" align="right"><strong>Tanggal&nbsp;Pengadaan</strong></td>
			<td><input type="text" onClick="showCal('Calendar2');" style="width:140px;" name="tanggal" id ="tanggal" readonly class="disabled" value = "<?php echo$tgl?>" />
         
				<a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
			</td>
</tr>
<tr>
    <td width="14%" align="right"><strong>Sumber Dana</strong></td>
    <td><input type="text" id="sumber" name="sumber" size="20" /></td></td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Foto</strong></td>
    <td><input type="file" id="foto" name="foto" /></td>
  </tr>
  <tr>
    <td width="14%" align="right"><strong>Keterangan</strong></td>
    <td><textarea name="keterangan" rows="2" id="keterangan"  style="width:95%"></textarea></td>
  </tr>
<tr>
	<td colspan="2" align="center"><br />
    <input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" />&nbsp;
    <input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
    </td>
</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>

<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo$ERROR_MSG?>');
</script>
<?php } ?>

<!-- Pilih inputan pertama -->

</body>
</html>
<script language="javascript">
var spryselect = new Spry.Widget.ValidationSelect("kategori");
var spryselect = new Spry.Widget.ValidationSelect("jenis");
var sprytextfield1 = new Spry.Widget.ValidationTextField("poin");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("keterangan");
</script>