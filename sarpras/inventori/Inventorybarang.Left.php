<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once("../include/config.php");
require_once("../include/db_functions.php");
//require_once("../include/errorhandler.php");
OpenDb();
$op="";
if (isset($_REQUEST[op]))
	$op = $_REQUEST[op];
if ($op=="EraseGroup"){
	$sql = "DELETE FROM $g_db_keuangan.groupbarang WHERE replid='$_REQUEST[idgroup]'";
	$result = mysql_query($sql);
	$MYSQL_ERRNO = mysql_errno();
	if ($MYSQL_ERRNO>0){
	?>
    <script language="javascript">
		alert('Data sedang digunakan \nsehingga tidak dapat dihapus!');
    </script>
    <?php
	//echo  "<span style='font-family:Verdana; color:red;'>Gagal Menghapus Data<br>Data Sedang Digunakan!</span>";
	//exit;
	}
}

if ($op=="EraseKelompok"){
    OpenDb();
	$query=mysql_query("select * from barang_lab where id_klmpk_lab='$_REQUEST[idkelompok]'");
	$ada=mysql_num_rows($query);
	
	if($ada>0){
	?>
    <script language="javascript">
		alert('Data sedang digunakan \nsehingga tidak dapat dihapus!');
    </script>
    <?php
	}else{
	$sql = "DELETE FROM $g_db_keuangan.kelompok_brg_lab WHERE id_klmpk_lab='$_REQUEST[idkelompok]'";
	$result = mysql_query($sql);
	}
}

function getNSubDir($idroot) {
	global $idvolume;
	
	$sql = "SELECT count(*) FROM $g_db_keuangan.kelompokbarang WHERE id_ruang='$idroot'";
	$result = QueryDb($sql);
	$row = mysql_fetch_row($result);
	return $row[0];
}

function spacing($count) {
	$str = "";
	for ($i = 0; $i < $count * 2; $i++) 
		$str = $str . " ";
	return $str;
}
?>
<html>
<head>
<link rel="stylesheet" href="../style/style.css" />
<link rel="stylesheet" href="../script/mktree.css" />
<script language="javascript" src="../script/mktree.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function AddGroup(){
	var addr="AddGroup.php";
	newWindow(addr,'AddGroup',360,207,'');
}
function EditGroup(idgroup){
	var addr="EditGroup.php?idgroup="+idgroup;
	newWindow(addr,'EditGroup',360,207,'');
}
function GetFresh(){
	document.location.href="Inventorybarang.Left.php";
}
function EraseGroup(idgroup){
	var msg = "Anda yakin akan menghapus Group ini?";
	if (confirm(msg))
		document.location.href="Inventorybarang.Left.php?op=EraseGroup&idgroup="+idgroup;
}
function EraseKelompok(idkelompok){
	var msg = "Anda yakin akan menghapus Kelompok ini?";
	if (confirm(msg))
		document.location.href="Inventorybarang.Left.php?op=EraseKelompok&idkelompok="+idkelompok;
}
function AddKelompok(idlab){
	var addr="AddKelompokbrg.php?idlab="+idlab;
	newWindow(addr,'AddKelompokbrg',360,207,'');
}
function EditKelompok(idkelompok){
	var addr="EditKelompokbrg.php?idkelompok="+idkelompok;
	newWindow(addr,'EditKelompok',360,207,'');
}
function OnLoad(){
	parent.Right.location.href="Inventory.Right.php";
}
function Hover(id,state){
	if (state=='1')
		document.getElementById(id).style.background='#fffcca';
	else
		document.getElementById(id).style.background='';
}
function SelectKelompok(idkelompok){
	parent.Right.location.href="Inventorybarang.Right.php?idkelompok="+idkelompok;
}
</script>
</head>
<body onLoad="OnLoad()">
<!--
<a href="#" onClick="document.location.reload()"><img src="../../images/ico/refresh.png" border="0" /></a>&nbsp;|&nbsp;<a href="#" onClick="expandTree('tree1'); return false;">Expand All</a>&nbsp;|&nbsp;
<a href="#" onClick="collapseTree('tree1'); return false;">Collapse All</a><br /><br /><br />
-->
<fieldset style="border:#336699 1px solid; background-color:#ffffff" >
<legend style="background-color:#336699; color:#FFFFFF; font-size:10px; font-weight:bold; padding:5px">&nbsp;Data&nbsp;Laboratorium&nbsp;</legend>
<div align="right">
</div>
<?php
$sql = "SELECT id_lab,nama FROM $g_db_keuangan.data_lab ORDER BY id_lab asc";
$result = QueryDb($sql);
$num = @mysql_num_rows($result);
if ($num>0){
?>
	<ul class='mktree' id='tree1'>
<?php
	while ($row = @mysql_fetch_row($result)){
		$class="liOpen";
		if (getNSubDir($row[0])==0)
		$class="liClose";
?>
		<li class="liOpen" style="cursor:default">&nbsp;<img src='../images/ico/folder.gif' border='0'>&nbsp;<strong><?php echo stripslashes($row[1])?></strong>&nbsp;<a href="javascript:AddKelompok('<?php echo$row[0]?>')"><img src="../images/ico/tambah.png" border='0' title="Tambah Kelompok"></a>
<?php
		$sql2 = "SELECT id_klmpk_lab,kelompok FROM $g_db_keuangan.kelompok_brg_lab WHERE id_lab='$row[0]'"; 
		$result2 = QueryDb($sql2);
		$num2 = @mysql_num_rows($result2);
			if ($num2>0){
			echo  "<ul>";
			while ($row2 = @mysql_fetch_row($result2)){
?>
				<li class="liOpen" id="liOpen<?php echo$row2[0]?>" onMouseOver="Hover('liOpen<?php echo$row2[0]?>','1')" onMouseOut="Hover('liOpen<?php echo$row2[0]?>','0')" onClick="SelectKelompok('<?php echo$row2[0]?>')">
<!--<span >-->
				<span ><img src='../images/ico/page.gif' border='0'>&nbsp;<?php echo stripslashes($row2[1])?></span>&nbsp;<img src="../images/ico/ubah.png" border='0' onClick="EditKelompok('<?php echo$row2[0]?>')" title="Ubah Kelompok" style="cursor:pointer"><img src="../images/ico/hapus.png" border='0' onClick="EraseKelompok('<?php echo$row2[0]?>')" title="Hapus Kelompok" style="cursor:pointer">
<!--</span>-->
				</li>
<?php
			}
echo  "</ul>";
			} 
	}
?>
</li>
</ul>
<script language="javascript">
collapseTree('tree1');
</script>
<?php
} else {
echo  "<div align='center'><br><em>Tidak ada Data Ruang</em><br>Silahkan Tambah Data Ruang Pada Menu Data Ruang<br></div>";
}
?>
</fieldset>
</body>
<?php
CloseDb();
?>
</html>