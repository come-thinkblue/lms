<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("jbskeu");
session_start();

unset($_SESSION['login']);
unset($_SESSION['namakeuangan']);
unset($_SESSION['tingkatkeuangan']);
unset($_SESSION['departemenkeuangan']);
unset($_SESSION['errtype']);
unset($_SESSION['errfile']);
unset($_SESSION['errno']);
unset($_SESSION['errmsg']);
unset($_SESSION['issend']);
?>
<script language="javascript">
	top.window.location='../../lms/index.php?link=sarpras';
</script>
