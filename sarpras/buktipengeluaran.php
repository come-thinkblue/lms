<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/rupiah.php');
require_once('include/sessioninfo.php');
require_once('include/db_functions.php');
require_once('include/getheader.php');

$idtransaksi = $_REQUEST['idtransaksi'];

OpenDb();

$sql = "SELECT jenispemohon, nip, nis, pemohonlain, penerima,
			   date_format(tanggal, '%Y-%m-%d') as tanggal, date_format(tanggalkeluar, '%Y-%m-%d') as tanggalkeluar,
			   jumlah, keperluan, petugas, keterangan, idjurnal
		  FROM pengeluaran
		 WHERE replid='$idtransaksi'";
$result = QueryDb($sql);
$row = mysql_fetch_array($result);
$jpemohon = $row['jenispemohon'];
if ($jpemohon == 1)
	$idpemohon = $row['nip'];
else if ($jpemohon == 2)
	$idpemohon = $row['nis'];
else
	$idpemohon = $row['pemohonlain'];
	
$penerima = $row['penerima'];
$tanggal = $row['tanggal'];
$tanggalkeluar = $row['tanggalkeluar'];
$jumlah = $row['jumlah'];
$keperluan = $row['keperluan'];
$keterangan = $row['keterangan'];
$petugas = $row['petugas'];
$idjurnal = $row['idjurnal'];

if ($jpemohon == 1) 
	$sql = "SELECT nama FROM $g_db_pegawai.pegawai WHERE nip = '$idpemohon'";
else if ($jpemohon == 2)
	$sql = "SELECT nama FROM dbakademik.siswa WHERE nis = '$idpemohon'";
else
	$sql = "SELECT nama FROM pemohonlain WHERE replid = $idpemohon";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$namapemohon = $row[0];
if ($jpemohon == 3) 
	$idpemohon = "";

$sql = "SELECT date_format(now(), '%Y-%m-%d') as tanggal";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$tglcetak = $row[0];

$sql = "SELECT nokas FROM jurnal WHERE replid = '$idjurnal'";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$nokas = $row[0];

$sql = "SELECT departemen FROM pengeluaran p, jurnal j, tahunbuku t WHERE p.replid='$idtransaksi' AND p.idjurnal=j.replid AND j.idtahunbuku=t.replid";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
$departemen = $row[departemen];

$sql = "SELECT replid, nama, alamat1 FROM dbakademik.identitas WHERE departemen='$departemen'";
$result = QueryDb($sql); 
$row = @mysql_fetch_array($result);
$idHeader = $row[replid];
$namaHeader = $row[nama];
$alamatHeader = $row[alamat1];
CloseDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMAN 1 Malang KEU [Bukti Pengeluaran]</title>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	
<table border="0" cellpadding="0" cellspacing="0" width="340" align="center">
<?php for($i = 0; $i < 2; $i++) { ?>
<tr>
<td align="center" valign="top">
	<table border="0" cellpadding="0" cellspacing="3" width="330" align="center">
	<?php if ($i == 0) { ?>		
	<tr>
		<td align="center" width='15%'>
			<img src='<?php echo $full_url."library/gambar.php?replid=$idHeader&table=dbakademik.identitas" ?>' height='30' />
		</td>
		<td align="left">
			<font style='font-size:14px'><strong><?php echo$namaHeader?></strong></font><br>
			<font style='font-size:10px'><?php echo$alamatHeader?></font>
		</td>
	</tr>
	<?php } else { ?>
	<tr height="1">
		<td align="center" width='15%'>&nbsp;</td>
		<td align="left">&nbsp;</td>
	</tr>
	<?php } ?>
	<tr>
		<td align="right" colspan='2'>
			<font size="1"><strong>No. <?php echo$nokas ?></strong></font>
		</td>
	</tr>
    <tr>
		<td align="center" colspan='2'>
			<font size="1"><strong>BUKTI PENGELUARAN KAS</strong></font>
		</td>
	</tr>
    <tr>
		<td align="left" colspan='2'>
		Telah dibayarkan kepada:
        <table border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
        	<td width="20">&nbsp;</td>
        	<td width="70">Nama</td>
            <td>:&nbsp;<strong><?php echo$idpemohon . "  " . $namapemohon ?></strong></td>
        </tr>
		<tr>
        	<td>&nbsp;</td>
        	<td>Tanggal</td>
            <td>:&nbsp;<strong><?php echo LongDateFormat($tanggal) ?></strong></td>
        </tr>
        <tr>
        	<td colspan="3" valign="top">uang sejumlah
            <font style="font-size:11px; font-weight:bold; font-style:italic;">
			<?php echo FormatRupiah($jumlah) ?> (<?php echo KalimatUang($jumlah) ?>) 
            </font>
			untuk <?php echo$keperluan ?>
            </td>
        </tr>
        </table>
		<br>
			
        <table border="0" width="100%">
		<tr><td valign="top">
		&#149;&nbsp;<em>Tgl cetak: <?php echo date('d/m/Y H:i:s') ?></em><br>
		&#149;&nbsp;<em>Keterangan: <?php echo$keterangan ?></em>
		</td></tr>
		</table>
		<br>
		<table border="1" width="100%" style="border-collapse:collapse">
		<tr height="60">
		<td align="center" width="33%" valign="top">Menyetujui<br /><br /><br />______________</td>
		<td align="center" width="33%" valign="top">Staf Keuangan<br /><br /><br /><?php echo$petugas ?></td>
		<td align="center" width="33%" valign="top">Penerima<br /><br /><br /><?php echo$namapemohon ?></td>
		</tr>
		</table>
		
    </td></tr>
    </table>
</td></tr>	
<tr>
	<td align='right'>
<?php if ($i == 0) { ?>
	<hr width="350" style="border-style:dashed; line-height:1px; color:#666;" />
<?php	} ?>	
	</td>
</tr>	
<?php } //for ?>
</table>

</body>
<script language="javascript">
window.print();
</script>
</html>