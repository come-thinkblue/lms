<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/sessioninfo.php');
require_once('include/common.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php');
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMAN 1 Malang KEU [Cetak Pengguna]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader('yayasan')?>

	<center><font size="4"><strong>DAFTAR PENGGUNA<br />SISTEM MANAJEMEN KEUANGAN</strong></font><br /> </center><br /><br />

    <table id="table" class="tab" cellpadding="2" cellspacing="0" border="1" style="border-collapse:collapse" width="100%" bordercolor="#000000">
	<tr height="30">
        <td class="header" width="5%" align="center">No</td>
        <td class="header" width="10%" align="center">Login</td>
        <td class="header" width="20%" align="center">Nama</td>
        <td class="header" width="12%" align="center">Departemen</td>
        <td class="header" width="15%" align="center">Tingkat</td>
        <td width="10%" class="header" align="center">Status</td>
        <td class="header" width="*" align="center">Keterangan</td>
        <td class="header" width="16%" align="center">Login Terakhir</td>
	</tr>
<?php	//$sql = "SELECT p.nama,p.nip,h.tingkat,h.departemen,date_format(l.lastlogin, '%d-%b-%Y %h:%i:%s') AS lastlogin,h.keterangan,l.replid as replid FROM $g_db_user.hakakses h, $g_db_user.login l, $g_db_pegawai.pegawai p WHERE h.modul='KEUANGAN' AND h.login=p.nip AND p.nip=l.login AND l.login=h.login ORDER BY lastlogin"; 
	OpenDb();
	$sql="SELECT h.login, h.replid,  h.tingkat, h.departemen, h.keterangan, p.nama, p.aktif,  DATE_FORMAT(h.lastlogin,'%Y-%m-%d') AS tanggal, TIME(h.lastlogin) as jam FROM $g_db_user.hakakses h, $g_db_pegawai.pegawai p, $g_db_user.login l WHERE h.modul='KEUANGAN' AND h.login = l.login AND l.login = p.nip ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	
	$result = QueryDb($sql);
	
	if ($page==0)
		$cnt = 0;
	else
		$cnt = (int)$page*(int)$varbaris;
	
	while ($row = mysql_fetch_array($result)) {
	?>
    <tr height="25">
    	<td align="center"><?php echo++$cnt ?></td>
        <td align="center"><?php echo$row['login'] ?></td>
        <td><?php echo$row['nama'] ?></td>
        <td align="center"><?php if ($row['tingkat'] == 1) {
					echo  "Semua Departemen";
				} else {
					echo  $row['departemen'];
				}
		?></td>
        <td align="center"><?php if ($row['tingkat'] == 1)
					echo  "Manajer Keuangan";
				else
					echo  "Staf Keuangan"; ?>  </td>
        <td align="center"><?php if ($row['aktif'] == 1) echo  'Aktif'; else echo  'Tidak Aktif'; ?></td>
        <td><?php echo$row['keterangan'] ?></td>
        <td align="center"><?php echo LongDateFormat($row['tanggal'])?> <?php echo$row['jam']?></td>
    </tr>
<?php	} 
	CloseDb();
?>
    <!-- END TABLE CONTENT -->
    </table>
	</td>
</tr>
<tr>
   	<td align="right">Halaman <strong><?php echo$page+1?></strong> dari <strong><?php echo$total?></strong> halaman</td>
</tr>
</table>
</body>
<script language="javascript">window.print();</script>
</html>