<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('include/errorhandler.php');
require_once('include/sessionchecker.php');
require_once('include/common.php');
require_once('include/rupiah.php');
require_once('include/config.php');
require_once('include/db_functions.php');
require_once('include/getheader.php'); 
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMAN 1 Malang KEU [Tahun Buku]</title>
<script language="javascript" src="script/tables.js"></script>
<script language="javascript" src="script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>


<center><font size="4"><strong>TAHUN BUKU</strong></font><br /> </center><br /><br />

<table border="0">
<tr>
	<td><strong>Departemen : <?php echo$departemen ?></strong></td>
</tr>
</table>
<br />

    <table id="table" class="tab" bordercolor="#000000" border="1" style="border-collapse:collapse" width="100%">
	<tr height="30" align="center">
        <td class="header" width="5%">No</td>
        <td class="header" width="12%">Tahun Buku</td>
        <td class="header" width="18%">Tanggal Mulai</td>
        <td class="header" width="15%">Awalan Kuitansi</td>
        <td class="header" width="*">Keterangan</td>
	</tr>
    <?php
	OpenDb();
	$sql = "SELECT * FROM tahunbuku WHERE departemen='$departemen' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	$result = QueryDb($sql);
	
	//if ($page==0)
		$cnt = 0;
	//else 
		//$cnt = (int)$page*(int)$varbaris;
		
	while($row = mysql_fetch_array($result)) {
	?>
    <tr height="25">
    	<td align="center"><?php echo++$cnt ?></td>
        <td align="center"><?php echo$row['tahunbuku'] ?></td>
        <td align="center"><?php echo LongDateFormat($row['tanggalmulai']) ?></td>
        <td align="center"><?php echo$row['awalan'] ?></td>
        <td><?php echo$row['keterangan'] ?></td>
    </tr>
<?php  }
	CloseDb(); ?>
     <!-- END TABLE CONTENT -->
    </table>
	</td>
</tr>
    </table>
</td></tr></table>
</body>
</html>
<script language="javascript">window.print();</script>