/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : dbpegawai

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-01-31 13:23:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bagianpegawai`
-- ----------------------------
DROP TABLE IF EXISTS `bagianpegawai`;
CREATE TABLE `bagianpegawai` (
  `bagian` varchar(50) NOT NULL,
  `urutan` tinyint(2) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bagian`),
  KEY `UXBagianPegawai` (`replid`),
  KEY `IX_bagianpegawai_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bagianpegawai
-- ----------------------------
INSERT INTO `bagianpegawai` VALUES ('Akademik', '1', '1', null, null, null, '2010-03-02 10:08:32', '53202', '0');
INSERT INTO `bagianpegawai` VALUES ('Non Akademik', '2', '2', null, null, null, '2010-03-02 10:08:32', '19723', '0');

-- ----------------------------
-- Table structure for `diklat`
-- ----------------------------
DROP TABLE IF EXISTS `diklat`;
CREATE TABLE `diklat` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(10) unsigned NOT NULL,
  `allowselect` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `diklat` varchar(45) NOT NULL,
  `tingkat` tinyint(3) unsigned NOT NULL,
  `jenis` varchar(1) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_diklat_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of diklat
-- ----------------------------
INSERT INTO `diklat` VALUES ('11', '0', '1', 'DIKLAT STRUKTURAL', '0', 'S', null, null, null, '2012-06-19 07:00:00', '23097', '0');
INSERT INTO `diklat` VALUES ('12', '0', '0', 'DIKLAT FUNGSIONAL', '0', 'F', null, null, null, '2012-06-19 07:00:00', '29220', '0');
INSERT INTO `diklat` VALUES ('26', '11', '1', 'Diklat Kepemimpinan', '2', '', null, null, null, '2012-06-19 07:00:00', '11276', '0');
INSERT INTO `diklat` VALUES ('28', '12', '1', 'Diklat Pengajaran', '2', '', null, null, null, '2012-06-19 07:00:00', '34253', '0');

-- ----------------------------
-- Table structure for `eselon`
-- ----------------------------
DROP TABLE IF EXISTS `eselon`;
CREATE TABLE `eselon` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eselon` varchar(15) NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`eselon`),
  UNIQUE KEY `UX_eselon` (`replid`),
  KEY `IX_eselon_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eselon
-- ----------------------------
INSERT INTO `eselon` VALUES ('5', '(Tidak Ada)', '1', '1', null, null, null, '2012-06-19 07:00:00', '6374', '0');
INSERT INTO `eselon` VALUES ('1', 'Eselon I', '2', '0', null, null, null, '2012-06-19 07:00:00', '60172', '0');
INSERT INTO `eselon` VALUES ('2', 'Eselon II', '3', '0', null, null, null, '2012-06-19 07:00:00', '19617', '0');
INSERT INTO `eselon` VALUES ('3', 'Eselon III', '4', '0', null, null, null, '2012-06-19 07:00:00', '48627', '0');
INSERT INTO `eselon` VALUES ('4', 'Eselon IV', '5', '0', null, null, null, '2012-06-19 07:00:00', '53226', '0');

-- ----------------------------
-- Table structure for `golongan`
-- ----------------------------
DROP TABLE IF EXISTS `golongan`;
CREATE TABLE `golongan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `golongan` varchar(14) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL,
  `nama` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`golongan`),
  UNIQUE KEY `UX_golongan` (`replid`),
  KEY `IX_golongan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of golongan
-- ----------------------------
INSERT INTO `golongan` VALUES ('18', '(Tidak Ada)', '1', '1', 'Tidak Ada Golongan', null, null, null, '2012-06-19 07:00:00', '54715', '0');
INSERT INTO `golongan` VALUES ('1', 'IA', '1', '2', 'Juru Muda', null, null, null, '2012-06-19 07:00:00', '48370', '0');
INSERT INTO `golongan` VALUES ('2', 'IB', '1', '3', 'Juru Muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '12175', '0');
INSERT INTO `golongan` VALUES ('3', 'IC', '1', '4', 'Juru', null, null, null, '2012-06-19 07:00:00', '46822', '0');
INSERT INTO `golongan` VALUES ('4', 'ID', '1', '5', 'Juru Tingkat 1', null, null, null, '2012-06-19 07:00:00', '997', '0');
INSERT INTO `golongan` VALUES ('5', 'IIA', '2', '6', 'Pengatur Muda', null, null, null, '2012-06-19 07:00:00', '61109', '0');
INSERT INTO `golongan` VALUES ('6', 'IIB', '2', '7', 'Pengatur Muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '40431', '0');
INSERT INTO `golongan` VALUES ('7', 'IIC', '2', '8', 'Pengatur', null, null, null, '2012-06-19 07:00:00', '18827', '0');
INSERT INTO `golongan` VALUES ('8', 'IID', '2', '9', 'Pengatur Tingkat 1', null, null, null, '2012-06-19 07:00:00', '38373', '0');
INSERT INTO `golongan` VALUES ('9', 'IIIA', '3', '10', 'Penata Muda', null, null, null, '2012-06-19 07:00:00', '4324', '0');
INSERT INTO `golongan` VALUES ('10', 'IIIB', '3', '11', 'Penata muda Tingkat 1', null, null, null, '2012-06-19 07:00:00', '37558', '0');
INSERT INTO `golongan` VALUES ('11', 'IIIC', '3', '12', 'Penata', null, null, null, '2012-06-19 07:00:00', '43759', '0');
INSERT INTO `golongan` VALUES ('12', 'IIID', '3', '13', 'Penata Tingkat 1', null, null, null, '2012-06-19 07:00:00', '40589', '0');
INSERT INTO `golongan` VALUES ('13', 'IVA', '4', '14', 'Pembina', null, null, null, '2012-06-19 07:00:00', '6140', '0');
INSERT INTO `golongan` VALUES ('14', 'IVB', '4', '15', 'Pembina Tingkat 1', null, null, null, '2012-06-19 07:00:00', '39991', '0');
INSERT INTO `golongan` VALUES ('15', 'IVC', '4', '16', 'Pembina Utama Muda', null, null, null, '2012-06-19 07:00:00', '50477', '0');
INSERT INTO `golongan` VALUES ('16', 'IVD', '4', '17', 'Pembina Utama Madya', null, null, null, '2012-06-19 07:00:00', '1348', '0');
INSERT INTO `golongan` VALUES ('17', 'IVE', '4', '18', 'Pembuna Utama', null, null, null, '2012-06-19 07:00:00', '51900', '0');

-- ----------------------------
-- Table structure for `jabatan`
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(10) unsigned NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `singkatan` varchar(255) NOT NULL,
  `satker` varchar(255) DEFAULT NULL,
  `eselon` varchar(15) DEFAULT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_jabatan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('8', '0', 'NA', 'STRUKTUR ORGANISASI SEKOLAH', null, '(Tidak Ada)', '1', null, null, null, '2012-06-19 07:00:00', '58865', '0');
INSERT INTO `jabatan` VALUES ('27', '8', 'PENGURUS SEKOLAH', 'SEKOLAH', '(Tidak Ada)', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '7566', '0');
INSERT INTO `jabatan` VALUES ('28', '27', 'Kepala Sekolah', 'KEPSEK', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '57825', '0');
INSERT INTO `jabatan` VALUES ('29', '28', 'Kepala TU', 'KEPTU', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '4305', '0');
INSERT INTO `jabatan` VALUES ('30', '27', 'Ketua Komite Sekolah', 'K.KOMITE', '(Tidak Ada)', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '44641', '0');
INSERT INTO `jabatan` VALUES ('31', '28', 'Wakil Kepala Sekolah Bidang Kurikulum', 'WK.KURIKULUM', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '13697', '0');
INSERT INTO `jabatan` VALUES ('32', '28', 'Wakil Kepala Sekolah Bidang Kesiswaan', 'WK.KESISWAAN', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '95', '0');
INSERT INTO `jabatan` VALUES ('33', '28', 'Wakil Kepala Sekolah Bidang Humas', 'WK.HUMAS', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '24911', '0');
INSERT INTO `jabatan` VALUES ('34', '32', 'Pembina OSIS', 'PB.OSIS', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '58740', '0');
INSERT INTO `jabatan` VALUES ('35', '32', 'Pembina Ekstrakulikuler', 'PB.EKSTRA', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '22378', '0');
INSERT INTO `jabatan` VALUES ('36', '28', 'Wakil Kepala Sekolah Bidang Sarana Prasaran', 'WK.SARANA', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '1198', '0');
INSERT INTO `jabatan` VALUES ('37', '33', 'Kordinator Rumah Tangga', 'KO.RT', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '4384', '0');
INSERT INTO `jabatan` VALUES ('38', '28', 'Koordinator BP', 'KO.BP', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '18328', '0');
INSERT INTO `jabatan` VALUES ('39', '28', 'Koordinator IT', 'KO.IT', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '12955', '0');
INSERT INTO `jabatan` VALUES ('40', '28', 'Koordinator Guru', 'KO.GURU', 'GURU', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '9791', '0');
INSERT INTO `jabatan` VALUES ('41', '40', 'Guru', 'GURU', 'GURU', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:00:00', '10089', '0');
INSERT INTO `jabatan` VALUES ('42', '8', 'Komite Sekolah', 'KOMSEK', 'MANAJEMEN', '(Tidak Ada)', '0', null, null, null, '2012-06-19 07:09:52', '45480', '0');
INSERT INTO `jabatan` VALUES ('43', '31', 'Staff Kurikulum', 'S. KUR', '(Tidak Ada)', '(Tidak Ada)', '0', null, null, null, '2013-02-13 21:51:19', '58199', '0');

-- ----------------------------
-- Table structure for `jadwal`
-- ----------------------------
DROP TABLE IF EXISTS `jadwal`;
CREATE TABLE `jadwal` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `jenis` varchar(45) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `exec` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jadwal_pegawai` (`nip`),
  KEY `FK_jadwal_jenisagenda` (`jenis`),
  KEY `IX_jadwal_ts` (`ts`,`issync`),
  CONSTRAINT `FK_jadwal_jenisagenda` FOREIGN KEY (`jenis`) REFERENCES `jenisagenda` (`agenda`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jadwal_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jadwal
-- ----------------------------

-- ----------------------------
-- Table structure for `jenisagenda`
-- ----------------------------
DROP TABLE IF EXISTS `jenisagenda`;
CREATE TABLE `jenisagenda` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agenda` varchar(45) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `urutan` tinyint(1) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`agenda`),
  UNIQUE KEY `UX_agenda` (`replid`),
  KEY `IX_jenisagenda_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jenisagenda
-- ----------------------------
INSERT INTO `jenisagenda` VALUES ('5', 'cpns', 'Pengangkatan CPNS', '6', null, null, null, '2012-06-19 07:00:00', '17386', '0');
INSERT INTO `jenisagenda` VALUES ('8', 'gaji', 'Penyesuaian Gaji', '4', null, null, null, '2012-06-19 07:00:00', '6748', '0');
INSERT INTO `jenisagenda` VALUES ('2', 'golongan', 'Kenaikan Golongan', '2', null, null, null, '2012-06-19 07:00:00', '47111', '0');
INSERT INTO `jenisagenda` VALUES ('3', 'jabatan', 'Kenaikan Jabatan', '1', null, null, null, '2012-06-19 07:00:00', '18723', '0');
INSERT INTO `jenisagenda` VALUES ('7', 'lainnya', 'Lainnya', '9', null, null, null, '2012-06-19 07:00:00', '17810', '0');
INSERT INTO `jenisagenda` VALUES ('1', 'pangkat', 'Kenaikan Pangkat', '3', null, null, null, '2012-06-19 07:00:00', '32879', '0');
INSERT INTO `jenisagenda` VALUES ('4', 'pensiun', 'Pensiun', '8', null, null, null, '2012-06-19 07:00:00', '45437', '0');
INSERT INTO `jenisagenda` VALUES ('6', 'pns', 'Pengangkatan PNS', '7', null, null, null, '2012-06-19 07:00:00', '63018', '0');

-- ----------------------------
-- Table structure for `jenisjabatan`
-- ----------------------------
DROP TABLE IF EXISTS `jenisjabatan`;
CREATE TABLE `jenisjabatan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `urutan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `jabatan` varchar(2) NOT NULL DEFAULT 'F',
  `isdefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`jenis`),
  UNIQUE KEY `Index_replid` (`replid`),
  KEY `IX_jenisjabatan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jenisjabatan
-- ----------------------------
INSERT INTO `jenisjabatan` VALUES ('8', 'KEPALA', '', '2', 'F', '0', null, null, null, '2012-06-19 07:00:00', '47718', '0');
INSERT INTO `jenisjabatan` VALUES ('16', 'KOORDINATOR', '', '4', 'F', '0', null, null, null, '2012-06-19 07:00:00', '49537', '0');
INSERT INTO `jenisjabatan` VALUES ('17', 'STAF', '', '5', 'F', '0', null, null, null, '2012-06-19 07:00:00', '38997', '0');
INSERT INTO `jenisjabatan` VALUES ('11', 'WAKIL KEPALA', '', '3', 'F', '0', null, null, null, '2012-06-19 07:00:00', '46376', '0');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Setting', '1', '0', '1', 'index.php', null, '1', '1');
INSERT INTO `menu` VALUES ('2', 'Struktur Jabatan', '1', '1', '2', 'referensi/jabatan.php', null, '1', '0');
INSERT INTO `menu` VALUES ('3', 'Jenis Jabatan', '1', '1', '3', 'referensi/jenjab.php', null, '1', '0');
INSERT INTO `menu` VALUES ('4', 'Diklat', '1', '1', '4', 'referensi/diklat.php', null, '1', '0');
INSERT INTO `menu` VALUES ('5', 'Satuan Kerja', '1', '1', '5', 'referensi/satker.php', null, '1', '0');
INSERT INTO `menu` VALUES ('6', 'Data Pegawai', '2', '0', '1', 'pegawai/pegawai.php', null, '1', '1');
INSERT INTO `menu` VALUES ('7', 'Cari Pegawai', '2', '1', '2', 'pegawai/daftar.php', null, '1', '0');
INSERT INTO `menu` VALUES ('8', 'Statistik Pegawai', '2', '1', '3', 'pegawai/statistik.php', null, '1', '0');
INSERT INTO `menu` VALUES ('9', 'Lain-lain', '3', '0', null, null, null, '1', '1');
INSERT INTO `menu` VALUES ('10', 'Jadwal Kepegawaian', '3', '1', '1', 'pegawai/jadwal.php', null, '1', '0');
INSERT INTO `menu` VALUES ('11', 'Struktur Organisasi', '3', '1', '2', 'pegawai/struktur.php', null, '1', '0');
INSERT INTO `menu` VALUES ('12', 'Daftar Urut Kepangkatan', '3', '1', '3', 'pegawai/dukpangkat.php', null, '1', '0');
INSERT INTO `menu` VALUES ('13', 'Pengaturan', '4', '0', '1', 'pengaturan/pengaturan.php', null, '1', '1');
INSERT INTO `menu` VALUES ('14', 'Tambah User', '4', '1', '2', 'pengaturan/user.php', null, '1', '0');
INSERT INTO `menu` VALUES ('15', 'Ganti Password', '4', '1', '3', 'pengaturan/pengaturan.php#', null, '1', '0');
INSERT INTO `menu` VALUES ('16', 'Keluar', '5', '0', '1', 'frameatas.php#', null, '1', '1');

-- ----------------------------
-- Table structure for `menuguru`
-- ----------------------------
DROP TABLE IF EXISTS `menuguru`;
CREATE TABLE `menuguru` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menuguru
-- ----------------------------
INSERT INTO `menuguru` VALUES ('27', 'Notifikasi', '6', '1', '3', 'home.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('16', 'Berita Sekolah', '5', '1', '2', 'buletin/rubriksekolah/beritasekolah.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('28', 'Berita Guru', '5', '1', '3', 'buletin/beritaguru/beritaguru.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('14', 'Berita Siswa', '5', '1', '4', 'buletin/beritasiswa/beritasiswa.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('17', 'Pesan', '6', '1', '2', 'buletin/pesan/pesan.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('19', 'Agenda', '6', '1', '4', 'buletin/agendaguru/agenda.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('20', 'Galeri Photo', '6', '1', '5', 'buletin/galerifoto/galerifoto.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('21', 'File Sharing', '6', '1', '6', 'buletin/filesharing/main.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('1', 'Akademik', '1', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('2', 'Kalender Pendidikan', '1', '1', '4', 'jadwal/kalender_main.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('3', 'Jadwal', '1', '1', '2', 'jadwal.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('4', 'Pelajaran', '1', '1', '3', 'pelajaran.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('5', 'Penilaian', '1', '1', '1', 'penilaian.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('10', 'Presensi', '3', '1', '2', 'presensi.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('12', 'Siswa', '3', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('11', 'Kejadian Siswa', '3', '1', '3', 'catatankejadian.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('13', 'Perpustakaan', '4', '0', '1', 'perpustakaan/perpustakaan.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('6', 'Kepegawaian', '2', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('7', 'Data Pegawai', '2', '1', '2', 'pegawai/data.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('8', 'Struktur Jabatan', '2', '1', '3', 'pegawai/struktur.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('9', 'Daftar Urut Kepangkatan', '2', '1', '4', 'pegawai/dukpangkat.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('24', 'Ganti Password', '6', '1', '7', 'frametop.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('25', 'Keluar', '7', '0', '1', 'logout.php', null, '1', '1');
INSERT INTO `menuguru` VALUES ('26', 'Cari Siswa', '3', '1', '2', 'siswa/siswa.php', null, '1', '0');
INSERT INTO `menuguru` VALUES ('15', 'Berita', '5', '0', '1', null, null, '1', '1');
INSERT INTO `menuguru` VALUES ('18', 'Lainya', '6', '0', '1', null, null, '1', '1');

-- ----------------------------
-- Table structure for `menusiswa`
-- ----------------------------
DROP TABLE IF EXISTS `menusiswa`;
CREATE TABLE `menusiswa` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menusiswa
-- ----------------------------
INSERT INTO `menusiswa` VALUES ('1', 'Akademik', '1', '0', '1', null, null, '1', '1');
INSERT INTO `menusiswa` VALUES ('2', 'Info Akademik', '1', '1', '2', 'infosiswa/infosiswa_content.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('3', 'Penilaian', '1', '1', '3', 'akademik/penilaian/lap_pelajaran_main.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('4', 'Jadwal Pelajaran', '1', '1', '4', 'akademik/jadwal/jadwal_kelas_footer.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('5', 'Kalender Pendidikan', '1', '1', '5', 'akademik/jadwal/kalender_footer.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('6', 'Perpustakaan', '2', '0', '1', 'perpustakaan/perpustakaan.php', null, '1', '1');
INSERT INTO `menusiswa` VALUES ('7', 'Berita', '3', '0', '1', null, null, '1', '1');
INSERT INTO `menusiswa` VALUES ('8', 'Berita Sekolah', '3', '1', '2', 'buletin/rubriksekolah/beritasekolah.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('9', 'Beritas Siswa', '3', '1', '3', 'buletin/beritasiswa/beritasiswa.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('10', 'Lainya', '4', '0', '1', null, null, '1', '1');
INSERT INTO `menusiswa` VALUES ('11', 'Pesan', '4', '1', '2', 'buletin/pesan/pesan.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('12', 'Notifikasi', '4', '1', '3', 'framecenter.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('13', 'Agenda', '4', '1', '4', 'buletin/agendasiswa/agenda.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('14', 'Galery Fhoto', '4', '1', '5', 'buletin/galerifoto/galerifoto.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('15', 'File Sharing', '4', '1', '6', 'buletin/filesharing/main.php', null, '1', '0');
INSERT INTO `menusiswa` VALUES ('16', 'Keluar', '5', '0', '1', 'infosiswa/logout.php', null, '1', '1');

-- ----------------------------
-- Table structure for `pegawai`
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `nrp` varchar(30) DEFAULT NULL,
  `nuptk` varchar(30) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `panggilan` varchar(50) DEFAULT NULL,
  `gelarawal` varchar(45) DEFAULT NULL,
  `gelarakhir` varchar(45) DEFAULT NULL,
  `gelar` varchar(50) DEFAULT NULL,
  `tmplahir` varchar(50) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `suku` varchar(50) DEFAULT NULL,
  `noid` varchar(50) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telpon` varchar(20) DEFAULT NULL,
  `handphone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `foto` blob,
  `bagian` varchar(50) NOT NULL,
  `nikah` varchar(10) NOT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `aktif` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `kelamin` varchar(1) NOT NULL,
  `pinpegawai` varchar(25) DEFAULT NULL,
  `mulaikerja` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `ketnonaktif` varchar(45) DEFAULT NULL,
  `pensiun` date DEFAULT NULL,
  `doaudit` tinyint(1) DEFAULT '0',
  `info1` varchar(20) DEFAULT NULL,
  `info2` varchar(20) DEFAULT NULL,
  `info3` varchar(20) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`nip`),
  UNIQUE KEY `UX_pegawai_replid` (`replid`),
  KEY `FK_pegawai_agama` (`agama`),
  KEY `FK_pegawai_suku` (`suku`),
  KEY `FK_pegawai_bagian` (`bagian`),
  KEY `IX_pegawai_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pegawai_agama` FOREIGN KEY (`agama`) REFERENCES `jbsumum`.`agama` (`agama`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pegawai_bagian` FOREIGN KEY (`bagian`) REFERENCES `bagianpegawai` (`bagian`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pegawai_suku` FOREIGN KEY (`suku`) REFERENCES `jbsumum`.`suku` (`suku`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES ('24', '1', '', '', 'robait usman', 'bet', '', 's,Kom', null, 'blitar', '1988-03-13', 'Islam', 'Jawa', '2', 'landungsari', '', '085646742211', 'come.thinkblue@gmail.com', '', '', '', null, 'Non Akademik', 'belum', '', '1', 'l', '78462', '2012-01-01', '', '', null, '1', null, null, null, '2013-02-06 15:15:24', '9926', '0');
INSERT INTO `pegawai` VALUES ('33', '10', null, null, 'Drs. SUWARTO', '', '', '', null, 'Malang', '1980-01-02', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '01187', null, null, null, null, '0', null, null, null, '2013-02-03 19:32:10', '6758', '0');
INSERT INTO `pegawai` VALUES ('23', '101', null, null, 'Stefano', '', '', '', '', 'Malang', '1987-11-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, 0xFFD8FFE000104A46494600010100000100010000FFFE003B43524541544F523A2067642D6A7065672076312E3020287573696E6720494A47204A50454720763632292C207175616C697479203D2037300AFFDB0043000A07070807060A0808080B0A0A0B0E18100E0D0D0E1D15161118231F2524221F2221262B372F26293429212230413134393B3E3E3E252E4449433C48373D3E3BFFDB0043010A0B0B0E0D0E1C10101C3B2822283B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3BFFC00011080050003A03012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00DED5B55D4A3D62F238EFEE5116770AAB2300064D543ACEABFF00411BAFFBFADFE34ED617FE2757C7FE9BBFFE846B2B53BAFB069F2DC6DDCCA3E51EA4F028026BEF165DD80026D5EE831E8A25624FEB5872FC4BBF56216E6FB00E0969C83FCEB4F4AF00C9ADE9EB7F7F7522C93FCE153B0ED9A9E6F86563E56D599E497FBCFC7F2A00AF61E34BBD4C116DABDDEE03251A56047EB565B5DD63B6A977FF007FDBFC6B2753F87EFA6E9E6FACE574B8806E2A0FDE1EDEF4BA55CC977621E61FBD43B5CFA9F5A00D07D7B5A038D56F3FEFFB7F8D7AEE9CECFA65ABBB1666850924E493815E30E99AF66D37FE41769FF5C13FF411401E7BAB80759BCE3FE5BBFF00335CF789E32DA4657A09109FA671FD6BA4D5D7FE2717871FF2DDFF0099A822B64BB578648C491BAED652077E33ED8EBC7A5006F47A8C1A3E991472453481100FDD2E6A45D46D25B137E8B218C0C9F970454F73A3E9DA9D92ADD448E060E586718E6A7934AB33A51D3C458B62BB703D280321EE63D5F4C902C32C6B229197C7F426BCF7C396CF159DC0933FEBC81F40057A7DA69165A5E9E62B68D62400F41D7BD7377F690DB5BC51C5188C4795C0FE3E8777FE84280315D074AF5FD3B8D32D7FEB8A7FE822BC959475AF5AD3FF00E41B6BFF005C53F90A00E0355FF90BDE73FF002DDFF99A860964824DD1B956C6323D2AC6A9FF00217BCFFAECFF00CCD53791225DD23AA2FAB1C50074C9748BA7472946940C6517924D508AF105C348BA75E22993CCC346F8DD9CE7AD56D2EF3CDB7636EE09570707A3023834FB5BABC5D4589D2B009FBC1F8FAD006C5CDD2B69CF36C31E01C2B0C106B959A5966C195CB95181935B7AC4B2C5A63BCA543C92222A0E9CB0CFE38CD73F15C4372B98A453EDDC50042E39E6BD5AC3FE41D6DFF5C53F90AF2D75F6AF52B0FF00907DB7FD725FE42803CBF5FBE99B54D49620D1224F2279B8CFCD93D2B32F4496D69189C6F9271CBB8CE063B0ED5B9AB7906E7510E58817F272077AADE218ADBEC96F90E485E30476028033DB517B0B8B3BD8519ADE58C432AA8E78E0371DC71F81ADFB7D6ADE09489AF9123DB928E79FC2B85F11DE496B663468519AE1E5CEE1FC208E47E3C565E9D0CB21F2B056E222630371F95BEA3B7D2803B997599356BF7BA990C76568A4C287AEE3C063EFEDD866B32DCC2F1CCC5C42D130C4A80F20F1C8AA5A4EA0DA8DA8D29A378EEE390994EDC071D01FF3EB566D611269D7AF1CD1E3728C1C8EFEE280231ABDCC21A69801096C2B0E847D3FFD55EDFA638934AB471FC5021FFC745785EAB6922E996CA9B792A061811D0D7B8E9000D1AC40391F678F07FE022803CDF5C923F335058433BAEA337981474E98FEB50EA8B34DF66CC32000742A7DABB5D7ACF5437D8D2EC2178E481C9768A2204BB5B05B772DCECF4EA7AF6A8C3C5130958E936B1809214522362A593F76076255861B2467777C50070DAEC5E5EBEB3B4241C0CB15FF0066B9FD37FE43916EF90C921E7F967EB8FD6BDA52DB5396C6192E34FB4176B70E255F2D5834637052A73C03F29E79C1E6B356C3594D3F4A90E936E6F3CD6377B60841DA1C6DCF6FB99E8473F950079E690009EEA6206F25416F5E4D1126DD1E765FE295011EBC357A12DB7891228CFF0063D8872D89822467866539078FBAA5948EF8CE4F195FB36B8B613471E928F2385F25A58600411B4B6F00E39F9C0C0EC33EB401E6BAC9FF008945BA8E1DF62AFD5B23FAD7B969600D26CC2F410263FEF915CDC365AC35F42B77A45AB42932AE638E2D85470CE73961EAA073C9C93800F5CAA1542A8000180076A00FFFD9, 'Akademik', 'tak_ada', '', '1', 'l', '12637', null, null, null, null, '0', null, null, null, '2013-02-03 00:41:31', '4536', '0');
INSERT INTO `pegawai` VALUES ('34', '11', null, null, 'Dra. Hj. INDAH ARIANI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'p', '73629', null, null, null, null, '0', null, null, null, '2013-02-03 19:33:28', '37269', '0');
INSERT INTO `pegawai` VALUES ('36', '12', null, null, 'Dra. FARAH NIRWANA', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'p', '08110', null, null, null, null, '0', null, null, null, '2013-02-03 19:35:22', '34368', '0');
INSERT INTO `pegawai` VALUES ('95', '123456', '12345', '12345', 'Ali Robbani', 'Pak Ali', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', null, '', '', '', '', '', '', '', null, 'Non Akademik', 'menikah', '', '1', 'L', '09827', '2012-01-01', 'HONORER', null, null, '0', null, null, null, '2013-02-14 00:20:29', '44937', '0');
INSERT INTO `pegawai` VALUES ('94', '123456789', '1', '234567', 'Ahsanun Naseh Khudori', 'Naseh', '', 'S. Kom', null, 'Tulungagung', '1988-01-01', 'Islam', 'Jawa', null, 'Jalan Gajayana 50 Malang', '12344', '085649847221', 'ahsanunnaseh@yahoo.com', 'www.facebook.com/ahsanunnaseh', '', 'www.cahjosari.com', null, 'Non Akademik', 'belum', 'ss', '1', 'l', '76279', '2012-01-01', 'HONORER', '', null, '1', null, null, null, '2013-02-06 15:14:38', '10176', '0');
INSERT INTO `pegawai` VALUES ('37', '13', null, null, 'Dra. SRI SUSILOWATI', '', '', '', null, 'Malang', '1098-01-02', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'p', '13193', null, null, null, null, '0', null, null, null, '2013-02-03 19:36:08', '34253', '0');
INSERT INTO `pegawai` VALUES ('38', '14', null, null, 'Drs. SYAMSUL HUDA, M. Hum.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '24959', null, null, null, null, '0', null, null, null, '2013-02-03 19:36:58', '51621', '0');
INSERT INTO `pegawai` VALUES ('39', '15', null, null, 'Dra. SRI HERDIYANTI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '24709', null, null, null, null, '0', null, null, null, '2013-02-03 19:38:44', '61502', '0');
INSERT INTO `pegawai` VALUES ('40', '16', null, null, 'Dra. HANA INDRAWATI R', '', '', '', null, 'Malang', '1980-01-02', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '54211', null, null, null, null, '0', null, null, null, '2013-02-03 19:39:17', '57389', '0');
INSERT INTO `pegawai` VALUES ('41', '17', null, null, 'Hj. HERMIN SUSETIYOWATI,S.Pd', '', '', '', null, 'Malang', '1980-01-02', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '96479', null, null, null, null, '0', null, null, null, '2013-02-03 19:39:44', '719', '0');
INSERT INTO `pegawai` VALUES ('42', '18', null, null, 'Dra. Hj.NURACI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '54321', null, null, null, null, '0', null, null, null, '2013-02-03 19:40:24', '52098', '0');
INSERT INTO `pegawai` VALUES ('43', '19', null, null, 'Dra. EFFI HARSIWINIWATI', '', '', '', null, 'Malang', '1980-01-02', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '36823', null, null, null, null, '0', null, null, null, '2013-02-03 19:40:58', '49557', '0');
INSERT INTO `pegawai` VALUES ('96', '1963112419940320004', '', '', 'Tutik Kusmini', 'Tutik', '', 'S.Pd', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', null, '', '', '', '', '', '', '', null, 'Akademik', 'menikah', '', '1', 'L', '66252', '2005-01-01', 'PNS', null, null, '0', null, null, null, '2013-02-14 00:30:24', '28193', '0');
INSERT INTO `pegawai` VALUES ('25', '2', null, null, 'Ahasun Naseh', '', '', 's,Kom', null, 'Tulunguagung', '1988-01-05', 'Islam', 'Jawa', '2', 'Malang', '', '085646742211', 'come.thinkblue@gmail.com', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '08782', null, null, null, null, '0', null, null, null, '2013-02-02 11:21:58', '23366', '0');
INSERT INTO `pegawai` VALUES ('44', '20', null, null, 'Dra. TRI RAHAYU PS.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '83554', null, null, null, null, '0', null, null, null, '2013-02-03 19:41:28', '57653', '0');
INSERT INTO `pegawai` VALUES ('45', '21', null, null, 'Dra. KUN WIDAYATI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'p', '03042', null, null, null, null, '0', null, null, null, '2013-02-03 20:09:45', '11745', '0');
INSERT INTO `pegawai` VALUES ('46', '22', null, null, 'Drs. BAMBANG TRIBAGJO, M. Psi', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '38585', null, null, null, null, '0', null, null, null, '2013-02-03 20:10:07', '5053', '0');
INSERT INTO `pegawai` VALUES ('47', '23', null, null, 'Drs. H. ABDUL KHOLIQ', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '91665', null, null, null, null, '0', null, null, null, '2013-02-03 20:10:27', '58508', '0');
INSERT INTO `pegawai` VALUES ('48', '24', null, null, 'Dra. DJOEWARIJAH BOEDISANTOSA', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '54408', null, null, null, null, '0', null, null, null, '2013-02-03 20:11:12', '55145', '0');
INSERT INTO `pegawai` VALUES ('49', '25', null, null, 'SRI WARDHANI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '20236', null, null, null, null, '0', null, null, null, '2013-02-03 20:12:10', '43577', '0');
INSERT INTO `pegawai` VALUES ('50', '26', null, null, 'Drs. MUNAS FAUZI ANWAR', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '39604', null, null, null, null, '0', null, null, null, '2013-02-03 20:13:14', '13460', '0');
INSERT INTO `pegawai` VALUES ('51', '27', null, null, 'HALIK BASUNI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '26899', null, null, null, null, '0', null, null, null, '2013-02-03 20:13:36', '33088', '0');
INSERT INTO `pegawai` VALUES ('52', '28', null, null, 'SRI RETNO LESTARI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'p', '34908', null, null, null, null, '0', null, null, null, '2013-02-03 20:14:11', '20034', '0');
INSERT INTO `pegawai` VALUES ('53', '29', null, null, 'TEGUH PRASETYO AJI,S.Pd.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '47722', null, null, null, null, '0', null, null, null, '2013-02-03 20:14:33', '34597', '0');
INSERT INTO `pegawai` VALUES ('26', '3', null, null, 'Hamidan', '', '', 's,Kom', null, 'Tulunguagung', '1980-01-04', 'Islam', 'Jawa', '3', 'Malang', '', '085646742211', 'come.thinkblue@gmail.com', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '40669', null, null, null, null, '0', null, null, null, '2013-02-02 11:23:22', '55031', '0');
INSERT INTO `pegawai` VALUES ('54', '30', null, null, 'TANTO PRIHADI , S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '15295', null, null, null, null, '0', null, null, null, '2013-02-03 20:15:20', '60092', '0');
INSERT INTO `pegawai` VALUES ('55', '31', null, null, 'LUDFI SETIAWAN, S.E.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '91017', null, null, null, null, '0', null, null, null, '2013-02-03 20:15:53', '48769', '0');
INSERT INTO `pegawai` VALUES ('56', '32', null, null, 'Dra. DWI AGUSTIN PILIANDARI', '', '', '', null, 'Malang', '1980-01-14', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '54983', null, null, null, null, '0', null, null, null, '2013-02-03 20:16:28', '32479', '0');
INSERT INTO `pegawai` VALUES ('57', '33', null, null, 'Dra. ISLAMIJATI SETYANINGSIH', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '86904', null, null, null, null, '0', null, null, null, '2013-02-03 20:16:59', '31098', '0');
INSERT INTO `pegawai` VALUES ('58', '34', null, null, 'Dra. SRI UTAMI WAHYUNI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '95409', null, null, null, null, '0', null, null, null, '2013-02-03 20:17:26', '4424', '0');
INSERT INTO `pegawai` VALUES ('59', '35', null, null, 'Drs. RUDJONO', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '49093', null, null, null, null, '0', null, null, null, '2013-02-03 20:17:46', '32550', '0');
INSERT INTO `pegawai` VALUES ('60', '36', null, null, 'JOEDWI LOEKI S ,S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '62847', null, null, null, null, '0', null, null, null, '2013-02-03 20:18:24', '38586', '0');
INSERT INTO `pegawai` VALUES ('61', '37', null, null, 'Dra. UMI FAUZIAH', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '85800', null, null, null, null, '0', null, null, null, '2013-02-03 20:18:52', '10318', '0');
INSERT INTO `pegawai` VALUES ('62', '38', null, null, 'Drs. SUPRAYOGI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '22077', null, null, null, null, '0', null, null, null, '2013-02-03 20:19:09', '10362', '0');
INSERT INTO `pegawai` VALUES ('63', '39', null, null, 'ZAKARIAH, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '69211', null, null, null, null, '0', null, null, null, '2013-02-03 20:19:47', '13663', '0');
INSERT INTO `pegawai` VALUES ('27', '4', null, null, 'Pepi', 'Pep', '', 's,Kom', null, 'Malang', '1990-06-08', 'Islam', 'Jawa', '3', 'Singosari Malang', '', '085646742211', 'come.thinkblue@gmail.com', null, null, null, null, 'Akademik', 'belum', '', '1', 'l', '43288', null, null, null, null, '0', null, null, null, '2013-02-02 11:25:44', '26000', '0');
INSERT INTO `pegawai` VALUES ('64', '40', null, null, 'DULARI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '36593', null, null, null, null, '0', null, null, null, '2013-02-03 20:20:03', '32358', '0');
INSERT INTO `pegawai` VALUES ('65', '41', null, null, 'AGNES YUNI PUJIASTUTI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '25298', null, null, null, null, '0', null, null, null, '2013-02-03 20:20:27', '4044', '0');
INSERT INTO `pegawai` VALUES ('66', '42', null, null, 'Hj. CHUSNA.HIDAYATI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '78407', null, null, null, null, '0', null, null, null, '2013-02-03 20:21:16', '36455', '0');
INSERT INTO `pegawai` VALUES ('67', '43', null, null, 'Drs. YULI SASONGKO', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '57890', null, null, null, null, '0', null, null, null, '2013-02-03 20:21:39', '40717', '0');
INSERT INTO `pegawai` VALUES ('68', '44', null, null, 'Dra. AGUSTIN. TJ', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '19617', null, null, null, null, '0', null, null, null, '2013-02-03 20:22:04', '34754', '0');
INSERT INTO `pegawai` VALUES ('69', '45', null, null, 'SITTY FATHONA, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '93972', null, null, null, null, '0', null, null, null, '2013-02-03 20:22:24', '56449', '0');
INSERT INTO `pegawai` VALUES ('70', '46', null, null, 'Dra. Hj. INDAH YULISFIATI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '19078', null, null, null, null, '0', null, null, null, '2013-02-03 20:23:08', '32466', '0');
INSERT INTO `pegawai` VALUES ('71', '47', null, null, 'Dra. CHUSNUL CHOTIMAH', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '67135', null, null, null, null, '0', null, null, null, '2013-02-03 20:23:50', '31083', '0');
INSERT INTO `pegawai` VALUES ('72', '48', null, null, 'DEWI ENDAHSARI, M. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '09133', null, null, null, null, '0', null, null, null, '2013-02-03 20:24:07', '51101', '0');
INSERT INTO `pegawai` VALUES ('73', '49', null, null, 'Dra. RULIYATI', '', '', '', null, 'Malang', '1980-01-02', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '33414', null, null, null, null, '0', null, null, null, '2013-02-03 20:24:44', '3511', '0');
INSERT INTO `pegawai` VALUES ('28', '5', null, null, 'Drs.SUPRIYONO, M.Si.', '', '', '', null, 'Malang', '1960-03-14', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '89791', null, null, null, null, '0', null, null, null, '2013-02-03 19:25:34', '24624', '0');
INSERT INTO `pegawai` VALUES ('74', '50', null, null, 'EKO PURWANTO, S. Pd', '', '', '', null, 'Malang', '1980-03-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '19226', null, null, null, null, '0', null, null, null, '2013-02-03 20:25:18', '59444', '0');
INSERT INTO `pegawai` VALUES ('75', '51', null, null, 'Dra. DWI ASTUTIK', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '88512', null, null, null, null, '0', null, null, null, '2013-02-03 20:25:35', '60999', '0');
INSERT INTO `pegawai` VALUES ('76', '52', null, null, 'Dra. ERTY WURYANINGSIH', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '12503', null, null, null, null, '0', null, null, null, '2013-02-03 20:25:50', '30877', '0');
INSERT INTO `pegawai` VALUES ('77', '53', null, null, 'Dra. BERTHA WARTINI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '72033', null, null, null, null, '0', null, null, null, '2013-02-03 20:26:17', '18645', '0');
INSERT INTO `pegawai` VALUES ('78', '54', null, null, 'ROCHMAD PRIYANTO, S.Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '88243', null, null, null, null, '0', null, null, null, '2013-02-03 20:26:44', '62970', '0');
INSERT INTO `pegawai` VALUES ('79', '55', null, null, 'Drs. PITONO', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '87810', null, null, null, null, '0', null, null, null, '2013-02-03 20:27:04', '30440', '0');
INSERT INTO `pegawai` VALUES ('80', '56', null, null, 'M. AKHIRI, S. Pd.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '26527', null, null, null, null, '0', null, null, null, '2013-02-03 20:27:27', '12907', '0');
INSERT INTO `pegawai` VALUES ('81', '57', null, null, 'HESTI PURWIDIASTUTI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '39589', null, null, null, null, '0', null, null, null, '2013-02-03 20:27:49', '16664', '0');
INSERT INTO `pegawai` VALUES ('82', '58', null, null, 'ISMI RAHAYU, SP', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '05294', null, null, null, null, '0', null, null, null, '2013-02-03 20:28:07', '14136', '0');
INSERT INTO `pegawai` VALUES ('83', '59', null, null, 'Drs. MOCHAMAD SHOLEH', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '65289', null, null, null, null, '0', null, null, null, '2013-02-03 20:28:50', '44948', '0');
INSERT INTO `pegawai` VALUES ('29', '6', null, null, 'MUKARROMAH, S. Ag', '', '', '', null, 'Malang', '1988-03-14', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '26827', null, null, null, null, '0', null, null, null, '2013-02-03 19:26:38', '60845', '0');
INSERT INTO `pegawai` VALUES ('84', '60', null, null, 'IRIANTO DJOKO BASUKI, BA', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '95545', null, null, null, null, '0', null, null, null, '2013-02-03 20:29:09', '35535', '0');
INSERT INTO `pegawai` VALUES ('85', '61', null, null, 'AHDHANI FAJAR S., S. Pd.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '94020', null, null, null, null, '0', null, null, null, '2013-02-03 20:29:41', '60533', '0');
INSERT INTO `pegawai` VALUES ('86', '62', null, null, 'Dra. MUSHLIHAH YASIN', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '40863', null, null, null, null, '0', null, null, null, '2013-02-03 20:29:58', '48727', '0');
INSERT INTO `pegawai` VALUES ('87', '63', null, null, 'Dra. ASFA CHORIWATI', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '15883', null, null, null, null, '0', null, null, null, '2013-02-03 20:30:24', '3800', '0');
INSERT INTO `pegawai` VALUES ('88', '64', null, null, 'DJULIAH, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '79183', null, null, null, null, '0', null, null, null, '2013-02-03 20:30:44', '44003', '0');
INSERT INTO `pegawai` VALUES ('89', '65', null, null, 'ENDAH PURWANTI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '76529', null, null, null, null, '0', null, null, null, '2013-02-03 20:32:06', '63587', '0');
INSERT INTO `pegawai` VALUES ('90', '66', null, null, 'M. AGUS SALIM, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '05801', null, null, null, null, '0', null, null, null, '2013-02-03 20:32:37', '10223', '0');
INSERT INTO `pegawai` VALUES ('91', '67', null, null, 'SILVANI HANDAYANI, S. Pd', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'p', '02069', null, null, null, null, '0', null, null, null, '2013-02-03 20:33:06', '61491', '0');
INSERT INTO `pegawai` VALUES ('92', '68', '1234567', '123456', 'AHMAD MAKKI HASAN', 'Makki', '', 'S. Hum', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', 'Jalan Gajayana 50 Malang', '', '', 'ahsanunnaseh@yahoo.com', 'www.facebook.com/ahsanunnaseh', '@cahjosari2010', 'www.cahjosari.com', 0xFFD8FFE000104A46494600010100000100010000FFFE003B43524541544F523A2067642D6A7065672076312E3020287573696E6720494A47204A50454720763632292C207175616C697479203D2037300AFFDB0043000A07070807060A0808080B0A0A0B0E18100E0D0D0E1D15161118231F2524221F2221262B372F26293429212230413134393B3E3E3E252E4449433C48373D3E3BFFDB0043010A0B0B0E0D0E1C10101C3B2822283B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3BFFC000110800F000A603012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00E9314B8A314EC56E7308052D2E28C500369691BAD28A062521A71E2AA4B7D6C9B879F197519DBBC669013134D24573D7BE258AD1F79700138C1ED5837FE3D3148DE4F2147CA3B13EF45C76677F9A51C1AF2EB3F19333B5D6AB7773273FBB8606D807D79AD5B7F88F68645C5B4E01E0AB386CD2BA0B33BEA0D66E99AED96A6A7C966575C6E49176B0AD2CD300A5A28A004A2834531094514138A0033452514013814B8A5028C5021314114EC518A006E29A78A7E2A29B1E5B67918A43395F13EAB2BC4D1452BC16E382EBC173F53DABCE6EF5660CF182723A303915B7E32F1334D70F676ACCA8870C73C67D057124E4E4D4499A451626BFB8B850B2485B07A9EB501624E49CD2609ED4E58DDBA29351734486939A553839A97EC93E33E5363E9519465EAA4524D0DC5ADD17B4CD56EB4FBB59E098A32FBF07EA3D2BD53C2DE29875987CA9311DC2F55CF5F715E375BDE159658F514922620C64138F4EFF005AD22CCE4B43DAF34555B4B8F3A1527838E6AC66ACC87525213466800CF3413452134C419A2928A605C14A050294520128C52D140C61ACAF115E7D8343BAB8070550EDFAD6B35713F12EFF00ECFA2A5B2B10D3BF4F61FE4526347955C4AD2CACC4F24E6ACE9FA63DE364FCA9EB4FD334D37921673850791EB5D45B5AAC4A1146057156ABCBA23D2C3D0E7D65B14E0D0A155C6DCFBD5C8748821190A093ED5A70C38519A9B60C57139C99E9AA515D0C992D50A1508306B22EB4B249EC2BA821724718AAF2A2B1A233682704D6A70D79A7C96FF3632B5158DD3595E473A6728C0F1DEBB0BAB55643B97208AE36E23F2A77403806BBE954E6479588A4A0F43D9B45BD8EF2D229E2395750456C835C07C3FBE57B036E49DF13F7F43D2BBC46CAD75F43CDD9D89292933484D003B3499A4A29885CD14DCD14C5734052814014A052282971452F6A43236AF31F8A0D21BEB5527F7610ED1EA78CFF004AF4F6AF3BF8A566C63B2BC19C06319FC79FE949EC5477398F0FC25A12DEA6BA048C16C0ED5474CB6682CA3441F3100FE75AF6FA6398F2652A7A935E5D5D647BB42EA0913C301229ED6CF838AAF1A496B273316FA9AD8B69D1A2F9941AE7675266535BEC425C803D4D5491E04E43AB7A0157B526490EC2481E82A1B65B288658AE7DCD35625DD941E58E40548DADD81AE5F5CD34C4C6E107CA7AFB57712C16F700956527B62B36F6CFCC89E261C32E2BA294926725785D6A667801B6DFDC2E7F801C7E35E9F6ED98C57987824082FEE4487078419F5CD7A65A906315E9C763C49A6A458A4A43466A8816909A4CD19A62B8514945311AB4A2929454961DE968A2818C35CAF8E9EDE7D1A5B265769B0248F68CE08F5FC335D51AE7BC436C64B988E38752B9FA7FFAEB1AB2718DD1D18782A93E5673B66122894919C28AAB75AA5D4BE725B05431AE543FF11F4ABF042CCFB48FBA31529B04CEE2A3EB5E5CA4B9B53DBA717C96460D95DDE5C6E92E09DBB805050827D78F6AD9B19DA3B87859BA0E29D222420B7520702AAD8A992E59C9E4F5A8934F6368C5C559B091DA79E619F980F94564EA3A4C975B3C8CAB052AE1DBDF3915AF2A05B925490C0D5C455C73C9FA5119B88A505356663DB584F1CE1D33180A0601EB8AD0B84FDDFBE2B411542E6AA5DE369AA8B6E57329C52898BA5DAAC3732C98E3ED1935DD591F93AD7110CAE3CD080ED27E638AEBB4B7630C65BA9519AF470F2BB67958B82518334F14869734D35D479E19A426909A4CD3218B9A2901A2981B14B45152682D14525031A6B1FC4314AF64B2C40931B6481E878AD76A8E445910A30CAB0C115138F345A35A53E49A91C6DBEE52449CB139C8EF56C06917007156B55B04B781658F3F29C1FA5436EE3CB1CD7935A9B8BD4F730F5633BD8A7716C110E06E63C0151D858B472E24600E339356AF5885F9783D7354623CFF0074772A7AD608EB1F736224918AC8148E983459AB3A10FF00794E33EB513C61B2579CFF0011193562D4AC636E7EB4D89684E57F766B3EF9BE4207357659303DAB32762CEA3DEB5A4AF2397112B45962CED818D23EA0F2D915D05AC3E5A8ACBD3D72735B2A70057AB4E9A82D0F0AAD695569B24CD349A42D4DDD5A9836293499A4269334C81E28A4068A606DD1499A5CD49A852669334D2D40C18D34D04D465A91488EE6259E0789BA30C573AA1A2768DB86538AE8CB5616BCC2078A751CB12ADEF5CB8887346E76E12A72CEDDCCBBD9EE137911EF23A02715989757339C2AB06F418E2B604F1CE01CF3DEA19A08D7E61F78F715E6A6968CF6A3DCCE956F546F04A81DCB0FE545B36A329F9E41E58FF679356842720B02C2AD1DB1A0268724396BA91B3EC8403D4D5556F32E07A0A8EFAF0670BC9F4A65813BB2DC92DCD74E1A3EF6A79B8D9DA1A1D2D8A6C8C1ABBBEA9C0E3CA18A937FBD7A678C4FBE8DF55BCCA51253219637501AA1DF4E56A68927068A603453037CD2668269A4F1526C216A616A6BBE0F5A89A4C77A43242D51B3D44F301D4E2B3EF759B4B34DD3CE918F566C5228D0692B07C43768D1448A723CDDA4FBE3A5646A5E39B6446166C657E4020719AB7AF593DA784ACE52499617496427A92DF7BFF42ACAAFC2D1D143E34CA2EAC9FBC8BF115243A82E3122907E951DB38950107A8A1E1C3F4C578EDF467BB6EA8B526A907963D47B567DCEA0D292225C67B9A95A1E3A54262033C0A6AC269B2058F6A9673927BD134AD69A7DA6A033E4C970D0B8F6EC7F4350DF4FE5C642F5ABBE228BECBF0FADE1230C24427EBC935D986DEE7062D2E5B1AD65760800B0AB9E6E7A1AF2EB7F105FDB050240EA38C30ADBB1F17C6702E14C4DEA3915DF74790E2D1DB7998A50E6B22D75682E937472AB8FF64D5B49C3F20D519B2F07A951AA88947AD598DC1AA4416D4D15186A2981D19351BB605389C5719E34F16BE8E4595995FB4B2E59CF3B07D3D6A0D8DEBDD420B542F34AA8077638AE5F52F1D58DB656163330FEE74FCEBCFAF755BBBF94C9737124AC7FBC6A917A342D26749A8F8DB52BB38858409E8BC9AC0B8BCB8BC977CF3348DEAC6AB934035372D2B172DBE79E2403765C0C7AF35EDBAC5A8BFD227B629F7A0200F7C578640FB278DB38DAC0D7BBEA734C9A4433DABAEEC276E36923351335A7A1E73A2DC9D8227FBE870735B67E71915997B64D69E21BAF290B231DE0023BFF00F5F3532CD2A6418641F85793520F9B43DAA55138ABB2C4980B54E76DA09A73DC487A44FF008F154AE64918618E09EC3935318498E5560BA89690FDB754863232A0EE23E95378FAE5A3D3AD2D07DC73BB3EB8AD4D0B489EDC99275DAF38C053D71EFE9587F11CC71DE59DBAB65A38C93F8D7A1461CA8F2F11539DE8717451456E728F8E592170F1BB230EEA715AF67E28BDB6C0971328F5E0FE758B4534DA1349EE77961E28B1B9C2C8E6173FDFE9F9D7456F28750CAC181E841EB5E439C569695AE5DE972031C85A3CFCD1B1E0FF00855A918CA9763D5D5B228ACDD2B558354B413DB938E8CA7AA9A2B431B58DEF156B4743D125BA8F065242460FF78FF9CD78B5E5E4D7970F3DC48D248E72CC4F5AEF3E28EA00B5A5829E466561FA0FEB5E72C73537B1D1157D409A6D19A2B36CD029452514863875AF72D0DFFB47C29623392F0286FE5FD2BC2C57B37C38B9FB4785E14EA62664FD73FD6896C5C3724D7B4669A45BBB618741C8F5AE7DA52A4AB82ACA70411822BD09A2120646EF58DA8E8F1DFA3003CAB98870FD987BFB561285F5378CAC71B34A4FCAA1893C0005755A1E849696E269D035D30C9C8CF97ED54BC3BA4CB26A4D3DD44556DDB6807BB7F85754E4AA9007279A211B6AC9948CF90224E1CF00679AF1CF136A0752D7EEA7CE543EC5FA0E2BD53C457634FD1AEAE18FCCA876FD7B578B1258924E49EB5B19498514514101451450014514500747E0FD456CEEA7495F08E99FC41FFEBD15CF2394390714568A5A194A177736FC5DA8FF0068F88AEE50D9457289F41C7F4AC2A7CAE5DCB13924E4D329365C5590514515050514514000AF4EF851739B4BCB627EEB8603EA3FFAD5E635DB7C2EBAF2F5F9ADC9E25873F883FF00D7A3A0E3B9EB0180938A8A72036E0707D69FFC46A101A5B9084F039A46A4914622C3F017A914973302094181D2A6DC5300F4F7AA57B284DC0118CD049C0FC46D44A59C364A7995B737D0579DD6DF8BF50FB7EBF3107290FEED7F0EBFAD62532185145140828A28A0028A28A0028A28A000D14514D805145148028A28A002B77C1577F63F15D8B938567D87F118AC2A9AD2736D770CEA706370C3F0340D1F42A0C9C8A644AC6E59C740B4DB2985C5B472A9C8740C0FD454B03E247F4C0A46849BC39DA460D60788EF52C34DB89D8FFAB527EA7B56E4AA3EF2D79C7C47D44ADA4766AF9699CB37D07FF5E8259E7923B49233B1CB31249F7A6D14532028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A00F6CF05DEFDB3C3368E4E4AA6C3F8715BF6FB4AB83D735C0FC2FBEDFA6DDD993CC4E187D0FF00FAABBA814FD9FCC07A92691A2D86CF2341963D2BC53C59A9FF00696B92B03948BE45FCF27F5AF5BD77514B3D2EE2694FDC427EB5E16EE64919DBAB1C9A64C84A28A28242A682D649F918551D59BA53A0B7054CD364463A01D58FA0A944D33FFAB5DA17EE85E00A4DD896EC569ADE5B77DB22E0F63D8D47574DC11FBB993721FBC3FA8F7AAD3C3E5370772B72ADEA284EE34EE88E8A28A630A28A2800A28A2800A28A2800A28A2800A28A2803ADF87379F67D7A480B604F111F88E7FC6BD5ED1C2411094E108E7DABC2744BB363AC5B5C038DAFCFD0F15EDF0CCB369D131E09407F4A4CB89C8FC4CBF8A2D362B68986F9DBE60A78C0EBFD2BCC2BA2F1B5E9BAD79E30D9581428FAF535CED325EE145145022E5D4859A351C2ED181E82AF4712C7186E781CE3B567A30312311F77E527F955C8AED4A0C91C7A9AC2A27D0CF4BD98DD4151A1DC061C1E6B39D8EC452781C8AB5713ABB844391DB8AA929CB9C741C55D34D2D4B1B451456830A28A2800A28A2800A28A2800A28A2800A28A2800538208EA2BD8EDEF57FE11786F037CAB0027DB02BC72BA94D70C7E053661FE7673101E83A9FD291516737733BDCDCCB3B9CB48C589FAD454514C90A28A28026B7911772499D8DE9D8FAD39A383B5C0C7D0E7F9557A281589F30C7CAB177EDC600A828A28185145140051451401FFD9, 'Akademik', 'menikah', '', '1', 'L', '67025', '2010-01-01', null, null, null, '0', null, null, null, '2013-02-14 12:07:21', '31437', '0');
INSERT INTO `pegawai` VALUES ('93', '69', null, null, 'PRATISIA INDRIA W.,S.Pd.', '', '', '', null, 'Malang', '1980-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'tak_ada', '', '1', 'l', '87708', null, null, null, null, '0', null, null, null, '2013-02-03 20:33:58', '43527', '0');
INSERT INTO `pegawai` VALUES ('30', '7', null, null, 'Drs. H. JUNAIDI', '', '', '', null, 'Malang', '1988-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '02526', null, null, null, null, '0', null, null, null, '2013-02-03 19:27:24', '2906', '0');
INSERT INTO `pegawai` VALUES ('31', '8', null, null, 'Drs. MANSUR, M. Ag', '', '', '', null, 'Malang', '1988-01-14', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'l', '43619', null, null, null, null, '0', null, null, null, '2013-02-03 19:30:17', '25270', '0');
INSERT INTO `pegawai` VALUES ('32', '9', null, null, 'KASTIN, S. Ag', '', '', '', null, 'Malang', '1988-01-01', 'Islam', 'Jawa', '', '', '', '', '', null, null, null, null, 'Akademik', 'menikah', '', '1', 'p', '01020', null, null, null, null, '0', null, null, null, '2013-02-03 19:31:28', '48724', '0');

-- ----------------------------
-- Table structure for `pegdiklat`
-- ----------------------------
DROP TABLE IF EXISTS `pegdiklat`;
CREATE TABLE `pegdiklat` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `iddiklat` int(10) unsigned NOT NULL,
  `tahun` int(10) unsigned NOT NULL,
  `sk` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pegdiklat_pegawai` (`nip`),
  KEY `FK_pegdiklat_diklat` (`iddiklat`),
  KEY `IX_pegdiklat_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pegdiklat_diklat` FOREIGN KEY (`iddiklat`) REFERENCES `diklat` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pegdiklat_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegdiklat
-- ----------------------------

-- ----------------------------
-- Table structure for `peggaji`
-- ----------------------------
DROP TABLE IF EXISTS `peggaji`;
CREATE TABLE `peggaji` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `gaji` varchar(15) NOT NULL,
  `sk` varchar(45) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `judulsk` varchar(255) DEFAULT NULL,
  `tanggalsk` varchar(255) DEFAULT NULL,
  `dok` text,
  `doaudit` tinyint(1) DEFAULT '1',
  `terakhir` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peggaji_pegawai` (`nip`),
  KEY `IX_peggaji_ts` (`ts`,`issync`),
  CONSTRAINT `FK_peggaji_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of peggaji
-- ----------------------------

-- ----------------------------
-- Table structure for `peggol`
-- ----------------------------
DROP TABLE IF EXISTS `peggol`;
CREATE TABLE `peggol` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `golongan` varchar(14) NOT NULL,
  `tmt` date NOT NULL,
  `sk` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `judulsk` varchar(255) DEFAULT NULL,
  `tanggalsk` varchar(45) DEFAULT NULL,
  `dok` text,
  `petugas` varchar(45) DEFAULT NULL,
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peggol_pegawai` (`nip`),
  KEY `FK_peggol_golongan` (`golongan`),
  KEY `IX_peggol_ts` (`ts`,`issync`),
  CONSTRAINT `FK_peggol_golongan` FOREIGN KEY (`golongan`) REFERENCES `golongan` (`golongan`) ON UPDATE CASCADE,
  CONSTRAINT `FK_peggol_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of peggol
-- ----------------------------
INSERT INTO `peggol` VALUES ('1', '68', 'IIIA', '2012-01-01', '12122', '', '1', null, null, null, null, '1', null, null, null, '2013-02-14 12:07:47', '40595', '0');

-- ----------------------------
-- Table structure for `pegjab`
-- ----------------------------
DROP TABLE IF EXISTS `pegjab`;
CREATE TABLE `pegjab` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `idjabatan` int(10) unsigned DEFAULT NULL,
  `tmt` date NOT NULL,
  `sk` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `jenis` varchar(50) NOT NULL,
  `namajab` varchar(255) DEFAULT NULL,
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `fk_pegjab_jabatan` (`idjabatan`),
  KEY `fk_pegjab_pegawai` (`nip`),
  KEY `IX_pegjab_ts` (`ts`,`issync`),
  CONSTRAINT `fk_pegjab_jabatan` FOREIGN KEY (`idjabatan`) REFERENCES `jabatan` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `fk_pegjab_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegjab
-- ----------------------------
INSERT INTO `pegjab` VALUES ('1', '5', '28', '2012-01-01', '12121', '', '1', 'KEPALA', 'Kepala Sekolah', '1', null, null, null, '2013-02-13 21:44:29', '39345', '0');
INSERT INTO `pegjab` VALUES ('2', '40', '31', '2012-01-01', '12121', '', '1', 'WAKIL KEPALA', 'Wakil Kepala Sekolah Bidang Kurikulum', '1', null, null, null, '2013-02-13 21:46:08', '5589', '0');
INSERT INTO `pegjab` VALUES ('3', '17', '36', '2012-01-01', '12121', '', '1', 'WAKIL KEPALA', 'Wakil Kepala Sekolah Bidang Sarana Prasaran', '1', null, null, null, '2013-02-13 21:46:42', '49344', '0');
INSERT INTO `pegjab` VALUES ('4', '59', '32', '2012-01-01', '12121', '', '1', 'WAKIL KEPALA', 'Wakil Kepala Sekolah Bidang Kesiswaan', '1', null, null, null, '2013-02-13 21:47:15', '29422', '0');
INSERT INTO `pegjab` VALUES ('5', '25', '33', '2012-01-01', '12121', '', '1', 'KEPALA', 'Wakil Kepala Sekolah Bidang Humas', '1', null, null, null, '2013-02-13 21:48:26', '8360', '0');

-- ----------------------------
-- Table structure for `pegkeluarga`
-- ----------------------------
DROP TABLE IF EXISTS `pegkeluarga`;
CREATE TABLE `pegkeluarga` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hubungan` varchar(50) DEFAULT NULL,
  `tgllahir` varchar(50) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pegkeluarga_pegawai` (`nip`),
  KEY `IX_pegkeluarga_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegkeluarga
-- ----------------------------

-- ----------------------------
-- Table structure for `pegkerja`
-- ----------------------------
DROP TABLE IF EXISTS `pegkerja`;
CREATE TABLE `pegkerja` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `thnawal` varchar(4) NOT NULL,
  `thnakhir` varchar(4) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `fk_pegkerja_pegawai` (`nip`),
  KEY `IX_pegkerja_ts` (`ts`,`issync`),
  CONSTRAINT `fk_pegkerja_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegkerja
-- ----------------------------
INSERT INTO `pegkerja` VALUES ('1', '132 456 001', 'Pikiran Rakyan', '1996', '1999', 'Wartawan', '', '1', null, null, null, '2012-06-19 07:00:01', '52383', '0');
INSERT INTO `pegkerja` VALUES ('2', '131 924 825', 'SMA Negeri 3 Durian', '1992', '1995', 'Guru', '', '1', null, null, null, '2012-06-19 07:00:01', '43673', '0');

-- ----------------------------
-- Table structure for `peglastdata`
-- ----------------------------
DROP TABLE IF EXISTS `peglastdata`;
CREATE TABLE `peglastdata` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `idpeggol` int(10) unsigned DEFAULT NULL,
  `idpegjab` int(10) unsigned DEFAULT NULL,
  `idpegdiklat` int(10) unsigned DEFAULT NULL,
  `idpegsekolah` int(10) unsigned DEFAULT NULL,
  `idpeggaji` int(10) unsigned DEFAULT NULL,
  `idpegserti` int(10) unsigned DEFAULT NULL,
  `idpegkerja` int(10) unsigned DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_peglastdata_pegawai` (`nip`),
  KEY `IX_peglastdata_ts` (`ts`,`issync`),
  CONSTRAINT `FK_peglastdata_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of peglastdata
-- ----------------------------
INSERT INTO `peglastdata` VALUES ('1', '5', null, '1', null, null, null, null, null, null, null, null, '2013-02-13 21:44:29', '35079', '0');
INSERT INTO `peglastdata` VALUES ('2', '40', null, '2', null, null, null, null, null, null, null, null, '2013-02-13 21:46:08', '2501', '0');
INSERT INTO `peglastdata` VALUES ('3', '17', null, '3', null, null, null, null, null, null, null, null, '2013-02-13 21:46:42', '48040', '0');
INSERT INTO `peglastdata` VALUES ('4', '59', null, '4', null, null, null, null, null, null, null, null, '2013-02-13 21:47:15', '20513', '0');
INSERT INTO `peglastdata` VALUES ('5', '25', null, '5', null, null, null, null, null, null, null, null, '2013-02-13 21:48:26', '52796', '0');
INSERT INTO `peglastdata` VALUES ('6', '123456', null, null, null, null, null, null, null, null, null, null, '2013-02-14 00:20:29', '59241', '0');
INSERT INTO `peglastdata` VALUES ('7', '1963112419940320004', null, null, null, null, null, null, null, null, null, null, '2013-02-14 00:30:24', '14170', '0');
INSERT INTO `peglastdata` VALUES ('8', '68', '1', null, null, null, null, null, null, null, null, null, '2013-02-14 12:07:47', '27558', '0');

-- ----------------------------
-- Table structure for `pegsekolah`
-- ----------------------------
DROP TABLE IF EXISTS `pegsekolah`;
CREATE TABLE `pegsekolah` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tingkat` varchar(20) NOT NULL,
  `sekolah` varchar(255) NOT NULL,
  `lulus` int(10) unsigned NOT NULL,
  `sk` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `doaudit` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pegsekolah_pegawai` (`nip`),
  KEY `IX_pegsekolah_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pegsekolah_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegsekolah
-- ----------------------------

-- ----------------------------
-- Table structure for `pegserti`
-- ----------------------------
DROP TABLE IF EXISTS `pegserti`;
CREATE TABLE `pegserti` (
  `replid` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `sertifikat` varchar(255) NOT NULL,
  `lembaga` varchar(255) NOT NULL,
  `tahun` smallint(6) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terakhir` tinyint(1) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `fk_pegserti_pegawai` (`nip`),
  KEY `IX_pegserti_ts` (`ts`,`issync`),
  CONSTRAINT `fk_pegserti_pegawai` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pegserti
-- ----------------------------

-- ----------------------------
-- Table structure for `satker`
-- ----------------------------
DROP TABLE IF EXISTS `satker`;
CREATE TABLE `satker` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `satker` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`satker`),
  UNIQUE KEY `UX_satker` (`replid`),
  KEY `IX_satker_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of satker
-- ----------------------------
INSERT INTO `satker` VALUES ('19', '(Tidak Ada)', 'Tidak Ada Satuan Kerja', '1', null, null, null, '2012-06-19 07:00:01', '17795', '0');
INSERT INTO `satker` VALUES ('21', 'GURU', 'Pengajar', '0', null, null, null, '2012-06-19 07:00:01', '51312', '0');
INSERT INTO `satker` VALUES ('23', 'KOPERASI', 'Koperasi', '0', null, null, null, '2012-06-19 07:00:01', '6583', '0');
INSERT INTO `satker` VALUES ('24', 'MANAJEMEN', 'Manajemen Sekolah', '0', null, null, null, '2012-06-19 07:00:01', '10040', '0');
INSERT INTO `satker` VALUES ('22', 'TATA USAHA', 'Tata Usaha Sekolah', '0', null, null, null, '2012-06-19 07:00:01', '30450', '0');
INSERT INTO `satker` VALUES ('20', 'YAYASAN', 'Pengurus Yayasan', '0', null, null, null, '2012-06-19 07:00:01', '56600', '0');
