/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : dbkeuangan

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-01-31 13:22:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `auditbesarjtt`
-- ----------------------------
DROP TABLE IF EXISTS `auditbesarjtt`;
CREATE TABLE `auditbesarjtt` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `nis` varchar(20) NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditbesarjtt_auditinfo` (`idaudit`),
  KEY `IX_auditbesarjtt_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditbesarjtt_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditbesarjtt
-- ----------------------------

-- ----------------------------
-- Table structure for `auditbesarjttcalon`
-- ----------------------------
DROP TABLE IF EXISTS `auditbesarjttcalon`;
CREATE TABLE `auditbesarjttcalon` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idcalon` int(10) unsigned NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditbesarjttcalon_auditinfo` (`idaudit`),
  KEY `IX_auditbesarjttcalon_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditbesarjttcalon_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditbesarjttcalon
-- ----------------------------

-- ----------------------------
-- Table structure for `auditinfo`
-- ----------------------------
DROP TABLE IF EXISTS `auditinfo`;
CREATE TABLE `auditinfo` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sumber` varchar(100) NOT NULL,
  `idsumber` int(10) unsigned NOT NULL,
  `tanggal` datetime NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `departemen` varchar(50) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_auditinfo_departemen` (`departemen`),
  KEY `IX_auditinfo_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditinfo_departemen` FOREIGN KEY (`departemen`) REFERENCES `dbakademik`.`departemen` (`departemen`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `auditjurnal`
-- ----------------------------
DROP TABLE IF EXISTS `auditjurnal`;
CREATE TABLE `auditjurnal` (
  `status` tinyint(1) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `transaksi` varchar(255) NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `nokas` varchar(100) NOT NULL,
  `idtahunbuku` int(10) unsigned NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `sumber` varchar(40) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditjurnal_auditinfo` (`idaudit`),
  KEY `IX_auditjurnal_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditjurnal_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of auditjurnal
-- ----------------------------

-- ----------------------------
-- Table structure for `auditjurnaldetail`
-- ----------------------------
DROP TABLE IF EXISTS `auditjurnaldetail`;
CREATE TABLE `auditjurnaldetail` (
  `status` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idjurnal` int(10) unsigned NOT NULL,
  `koderek` varchar(15) NOT NULL,
  `debet` decimal(15,0) NOT NULL DEFAULT '0',
  `kredit` decimal(15,0) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_auditjurnaldetail_auditinfo` (`idaudit`),
  KEY `IX_auditjurnaldetail_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditjurnaldetail_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditjurnaldetail
-- ----------------------------

-- ----------------------------
-- Table structure for `auditpenerimaaniuran`
-- ----------------------------
DROP TABLE IF EXISTS `auditpenerimaaniuran`;
CREATE TABLE `auditpenerimaaniuran` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `nis` varchar(20) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaaniuran_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaaniuran_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditpenerimaaniuran_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditpenerimaaniuran
-- ----------------------------

-- ----------------------------
-- Table structure for `auditpenerimaaniurancalon`
-- ----------------------------
DROP TABLE IF EXISTS `auditpenerimaaniurancalon`;
CREATE TABLE `auditpenerimaaniurancalon` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `idcalon` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaaniurancalon_audit` (`idaudit`),
  KEY `IX_auditpenerimaaniurancalon_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditpenerimaaniurancalon_audit` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditpenerimaaniurancalon
-- ----------------------------

-- ----------------------------
-- Table structure for `auditpenerimaanjtt`
-- ----------------------------
DROP TABLE IF EXISTS `auditpenerimaanjtt`;
CREATE TABLE `auditpenerimaanjtt` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idbesarjtt` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaanjtt_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaanjtt_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditpenerimaanjtt_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditpenerimaanjtt
-- ----------------------------

-- ----------------------------
-- Table structure for `auditpenerimaanjttcalon`
-- ----------------------------
DROP TABLE IF EXISTS `auditpenerimaanjttcalon`;
CREATE TABLE `auditpenerimaanjttcalon` (
  `statusdata` tinyint(1) NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idbesarjttcalon` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaanjttcalon_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaanjttcalon_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditpenerimaanjttcalon_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditpenerimaanjttcalon
-- ----------------------------

-- ----------------------------
-- Table structure for `auditpenerimaanlain`
-- ----------------------------
DROP TABLE IF EXISTS `auditpenerimaanlain`;
CREATE TABLE `auditpenerimaanlain` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `sumber` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpenerimaanlain_auditinfo` (`idaudit`),
  KEY `IX_auditpenerimaanlain_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditpenerimaanlain_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditpenerimaanlain
-- ----------------------------

-- ----------------------------
-- Table structure for `auditpengeluaran`
-- ----------------------------
DROP TABLE IF EXISTS `auditpengeluaran`;
CREATE TABLE `auditpengeluaran` (
  `statusdata` tinyint(1) unsigned NOT NULL,
  `idaudit` int(10) unsigned NOT NULL,
  `replid` int(10) unsigned NOT NULL,
  `idpengeluaran` int(10) unsigned NOT NULL,
  `keperluan` varchar(255) NOT NULL,
  `jenispemohon` tinyint(1) unsigned NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `pemohonlain` int(10) unsigned DEFAULT NULL,
  `penerima` varchar(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `tanggalkeluar` datetime NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `petugas` varchar(45) DEFAULT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `keterangan` text,
  `namapemohon` varchar(100) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `FK_auditpengeluaran_auditinfo` (`idaudit`),
  KEY `IX_auditpengeluaran_ts` (`ts`,`issync`),
  CONSTRAINT `FK_auditpengeluaran_auditinfo` FOREIGN KEY (`idaudit`) REFERENCES `auditinfo` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auditpengeluaran
-- ----------------------------

-- ----------------------------
-- Table structure for `barang`
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idkelompok` int(10) unsigned NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(255) DEFAULT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `tglperolehan` date NOT NULL DEFAULT '0000-00-00',
  `foto` blob,
  `keterangan` varchar(255) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT 'unit',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_barang_kelompok` (`idkelompok`),
  KEY `IX_barang_ts` (`ts`,`issync`),
  CONSTRAINT `FK_barang_kelompok` FOREIGN KEY (`idkelompok`) REFERENCES `kelompokbarang` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of barang
-- ----------------------------

-- ----------------------------
-- Table structure for `besarjtt`
-- ----------------------------
DROP TABLE IF EXISTS `besarjtt`;
CREATE TABLE `besarjtt` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `cicilan` decimal(15,0) NOT NULL DEFAULT '0',
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pembayaranjtt_siswa` (`nis`),
  KEY `FK_pembayaranjtt_penerimaan` (`idpenerimaan`),
  KEY `IX_besarjtt_ts` (`ts`,`issync`),
  CONSTRAINT `FK_besarjtt_siswa` FOREIGN KEY (`nis`) REFERENCES `dbakademik`.`siswa` (`nis`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pembayaranjtt_penerimaan` FOREIGN KEY (`idpenerimaan`) REFERENCES `datapenerimaan` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of besarjtt
-- ----------------------------
INSERT INTO `besarjtt` VALUES ('1', '15203', '9', '250000', '100000', '0', '', 'landlord', '1', '1', null, '2013-03-21 19:30:46', '9066', '0');
INSERT INTO `besarjtt` VALUES ('2', '15819', '9', '100000', '100000', '1', 'lunas', 'Tutik Kusmini', '5', '1', '', '2016-01-22 21:46:59', '0', '0');
INSERT INTO `besarjtt` VALUES ('3', '12345', '9', '10000', '100000', '0', '', 'landlord', '6', '1', null, '2016-01-22 21:47:46', '0', '0');
INSERT INTO `besarjtt` VALUES ('4', '15836', '9', '10000000', '1000000', '0', '', 'landlord', '7', '1', null, '2016-01-22 21:48:03', '0', '0');
INSERT INTO `besarjtt` VALUES ('5', '15855', '9', '800000', '100000', '0', '', 'Tutik Kusmini', '20', '1', null, '2016-01-30 10:32:29', '0', '0');
INSERT INTO `besarjtt` VALUES ('6', '15877', '9', '100000', '10000', '0', '', 'Tutik Kusmini', '21', '1', null, '2016-01-30 10:33:35', '0', '0');

-- ----------------------------
-- Table structure for `besarjttcalon`
-- ----------------------------
DROP TABLE IF EXISTS `besarjttcalon`;
CREATE TABLE `besarjttcalon` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcalon` int(10) unsigned NOT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `besar` decimal(15,0) NOT NULL,
  `cicilan` decimal(15,0) NOT NULL DEFAULT '0',
  `lunas` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `pengguna` varchar(100) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_besarjttcalon_penerimaan` (`idpenerimaan`),
  KEY `FK_besarjttcalon_calonsiswa` (`idcalon`),
  KEY `IX_besarjttcalon_ts` (`ts`,`issync`),
  CONSTRAINT `FK_besarjttcalon_calonsiswa` FOREIGN KEY (`idcalon`) REFERENCES `dbakademik`.`calonsiswa` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_besarjttcalon_penerimaan` FOREIGN KEY (`idpenerimaan`) REFERENCES `datapenerimaan` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of besarjttcalon
-- ----------------------------

-- ----------------------------
-- Table structure for `datadsp`
-- ----------------------------
DROP TABLE IF EXISTS `datadsp`;
CREATE TABLE `datadsp` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `dsp` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `operator` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_datadsp_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of datadsp
-- ----------------------------

-- ----------------------------
-- Table structure for `datapenerimaan`
-- ----------------------------
DROP TABLE IF EXISTS `datapenerimaan`;
CREATE TABLE `datapenerimaan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `besar` decimal(15,0) DEFAULT NULL,
  `idkategori` varchar(15) NOT NULL,
  `rekkas` varchar(15) NOT NULL,
  `rekpendapatan` varchar(15) NOT NULL,
  `rekpiutang` varchar(15) DEFAULT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_datapenerimaan_rekakun_kas` (`rekkas`),
  KEY `FK_datapenerimaan_rekakun_pendapatan` (`rekpendapatan`),
  KEY `FK_datapenerimaan_rekakun_piutang` (`rekpiutang`),
  KEY `FK_datapenerimaan_kategoripenerimaan` (`idkategori`),
  KEY `FK_datapenerimaan_departemen` (`departemen`),
  KEY `IX_datapenerimaan_ts` (`ts`,`issync`),
  CONSTRAINT `FK_datapenerimaan_departemen` FOREIGN KEY (`departemen`) REFERENCES `dbakademik`.`departemen` (`departemen`) ON UPDATE CASCADE,
  CONSTRAINT `FK_datapenerimaan_kategoripenerimaan` FOREIGN KEY (`idkategori`) REFERENCES `kategoripenerimaan` (`kode`) ON UPDATE CASCADE,
  CONSTRAINT `FK_datapenerimaan_rekakun_kas` FOREIGN KEY (`rekkas`) REFERENCES `rekakun` (`kode`) ON UPDATE CASCADE,
  CONSTRAINT `FK_datapenerimaan_rekakun_pendapatan` FOREIGN KEY (`rekpendapatan`) REFERENCES `rekakun` (`kode`) ON UPDATE CASCADE,
  CONSTRAINT `FK_datapenerimaan_rekakun_piutang` FOREIGN KEY (`rekpiutang`) REFERENCES `rekakun` (`kode`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of datapenerimaan
-- ----------------------------
INSERT INTO `datapenerimaan` VALUES ('9', 'SPP Bulanan', null, 'JTT', '111', '411', '150', '1', 'Sekedar contoh. Menu ini mengatur setiap jenis penerimaan yang mungkin diterima sekolah. Anda harus menentukan rekening Kas, Pendapatan dan Piutang untuk setiap transaksi penerimaan.', 'SMAN 1 Malang', '421', null, null, '2012-01-02 07:14:49', '41237', '0');
INSERT INTO `datapenerimaan` VALUES ('11', 'Sumbangan Biaya Pengembangan Pendidikan', null, 'CSWJB', '111', '412', '153', '1', 'Sekedar contoh. Menu ini mengatur setiap jenis penerimaan yang mungkin diterima sekolah. Anda harus menentukan rekening Kas, Pendapatan dan Piutang untuk setiap transaksi penerimaan.', 'SMAN 1 Malang', '422', null, null, '2013-03-22 16:06:48', '12756', '0');
INSERT INTO `datapenerimaan` VALUES ('12', 'Sumbangan BOS', null, 'LNN', '113', '414', '154', '1', 'Sekedar contoh. Menu ini mengatur setiap jenis penerimaan yang mungkin diterima sekolah. Anda harus menentukan rekening Kas, Pendapatan dan Piutang untuk setiap transaksi penerimaan.', 'SMAN 1 Malang', '424', null, null, '2012-01-02 08:01:04', '5601', '0');
INSERT INTO `datapenerimaan` VALUES ('13', 'Tabungan Siswa', null, 'SKR', '112', '413', '150', '1', 'Sekedar contoh. Menu ini mengatur setiap jenis penerimaan yang mungkin diterima sekolah. Anda harus menentukan rekening Kas, Pendapatan dan Piutang untuk setiap transaksi penerimaan.', 'SMAN 1 Malang', '423', null, null, '2012-01-02 07:57:14', '55264', '0');
INSERT INTO `datapenerimaan` VALUES ('14', 'Sumbangan Pendidikan', null, 'CSSKR', '112', '415', '153', '1', 'Sekedar contoh. Menu ini mengatur setiap jenis penerimaan yang mungkin diterima sekolah. Anda harus menentukan rekening Kas, Pendapatan dan Piutang untuk setiap transaksi penerimaan.', 'SMAN 1 Malang', '425', null, null, '2012-01-02 07:59:39', '62929', '0');

-- ----------------------------
-- Table structure for `datapengeluaran`
-- ----------------------------
DROP TABLE IF EXISTS `datapengeluaran`;
CREATE TABLE `datapengeluaran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departemen` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `rekdebet` varchar(15) NOT NULL,
  `rekkredit` varchar(15) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `besar` decimal(15,0) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_datapengeluaran_departemen` (`departemen`),
  KEY `FK_datapengeluaran_rekakun` (`rekdebet`),
  KEY `FK_datapengeluaran_rekakun2` (`rekkredit`),
  KEY `IX_datapengeluaran_ts` (`ts`,`issync`),
  CONSTRAINT `FK_datapengeluaran_departemen` FOREIGN KEY (`departemen`) REFERENCES `dbakademik`.`departemen` (`departemen`) ON UPDATE CASCADE,
  CONSTRAINT `FK_datapengeluaran_rekakun` FOREIGN KEY (`rekdebet`) REFERENCES `rekakun` (`kode`) ON UPDATE CASCADE,
  CONSTRAINT `FK_datapengeluaran_rekakun2` FOREIGN KEY (`rekkredit`) REFERENCES `rekakun` (`kode`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of datapengeluaran
-- ----------------------------
INSERT INTO `datapengeluaran` VALUES ('4', 'SMAN 1 Malang', 'Bayar Listrik', '501', '111', '1', 'Sekedar contoh. Menu ini mengatur setiap jenis pengeluaran yang mungkin dikeluarkan sekolah. Anda harus menentukan rekening Kas dan Beban untuk setiap transaksi pengeluaran.', '0', null, null, null, '2010-03-02 10:06:52', '17792', '0');

-- ----------------------------
-- Table structure for `groupbarang`
-- ----------------------------
DROP TABLE IF EXISTS `groupbarang`;
CREATE TABLE `groupbarang` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(45) DEFAULT NULL,
  `namagroup` varchar(45) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_groupbarang_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of groupbarang
-- ----------------------------

-- ----------------------------
-- Table structure for `jurnal`
-- ----------------------------
DROP TABLE IF EXISTS `jurnal`;
CREATE TABLE `jurnal` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `transaksi` varchar(255) NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `nokas` varchar(100) NOT NULL,
  `idtahunbuku` int(10) unsigned NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `sumber` varchar(40) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jurnal_tahunbuku` (`idtahunbuku`),
  KEY `IX_jurnal_tanggal` (`tanggal`),
  KEY `IX_jurnal_idtahunbuku` (`idtahunbuku`),
  KEY `IX_jurnal_ts` (`ts`,`issync`),
  CONSTRAINT `FK_jurnal_tahunbuku` FOREIGN KEY (`idtahunbuku`) REFERENCES `tahunbuku` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jurnal
-- ----------------------------
INSERT INTO `jurnal` VALUES ('1', '2013-03-21', 'Pendataan besar pembayaran SPP Bulanan siswa ANGGITA ROSIANA PUTRI (15203)', 'landlord', '1000001', '1', '', 'penerimaanjtt', null, null, null, '2013-03-21 19:30:46', '0', '0');
INSERT INTO `jurnal` VALUES ('2', '2013-03-21', 'Pembayaran cicilan ke-1 SPP Bulanan siswa ANGGITA ROSIANA PUTRI (15203)', 'landlord', '1000002', '1', '', 'penerimaanjtt', null, null, null, '2013-03-21 19:31:17', '0', '0');
INSERT INTO `jurnal` VALUES ('3', '2013-03-21', 'Pembayaran cicilan ke-2 SPP Bulanan siswa ANGGITA ROSIANA PUTRI (15203)', 'landlord', '1000003', '1', '', 'penerimaanjtt', null, null, null, '2013-03-21 19:31:53', '0', '0');
INSERT INTO `jurnal` VALUES ('4', '2013-03-21', 'Pembayaran listrik bulan mei', 'landlord', '1000004', '1', '', 'pengeluaran', null, null, null, '2013-03-21 19:58:22', '0', '0');
INSERT INTO `jurnal` VALUES ('5', '2016-01-22', 'Pendataan besar pembayaran SPP Bulanan siswa AISYAH SUGIASTU PUTRI (15819)', 'landlord', '1000005', '1', '', 'penerimaanjtt', null, null, null, '2016-01-22 21:46:59', '0', '0');
INSERT INTO `jurnal` VALUES ('6', '2016-01-22', 'Pendataan besar pembayaran SPP Bulanan siswa AFNAN (12345)', 'landlord', '1000006', '1', '', 'penerimaanjtt', null, null, null, '2016-01-22 21:47:46', '0', '0');
INSERT INTO `jurnal` VALUES ('7', '2016-01-22', 'Pendataan besar pembayaran SPP Bulanan siswa AMATULLOH ZAINA JANNATI (15836)', 'landlord', '1000007', '1', '', 'penerimaanjtt', null, null, null, '2016-01-22 21:48:03', '0', '0');
INSERT INTO `jurnal` VALUES ('8', '2016-01-22', 'Pelunasan SPP Bulanan siswa AISYAH SUGIASTU PUTRI (15819)', 'landlord', '1000008', '1', '', 'penerimaanjtt', null, null, null, '2016-01-22 21:48:28', '0', '0');
INSERT INTO `jurnal` VALUES ('9', '2016-01-22', 'Pembayaran cicilan ke-2 SPP Bulanan siswa AISYAH SUGIASTU PUTRI (15819)', 'landlord', '1000009', '1', '', 'penerimaanjtt', null, null, null, '2016-01-22 21:49:08', '0', '0');
INSERT INTO `jurnal` VALUES ('10', '2016-01-22', 'Pembayaran Tabungan Siswa tanggal 22-01-2016 siswa AFNAN (12345)', 'Tutik Kusmini', '16000010', '1', '', 'penerimaaniuran', null, null, null, '2016-01-22 23:05:17', '0', '0');
INSERT INTO `jurnal` VALUES ('11', '2016-01-22', 'Pembayaran Tabungan Siswa tanggal 22-01-2016 siswa AISYAH SUGIASTU PUTRI (15819)', 'Tutik Kusmini', '16000011', '1', '', 'penerimaaniuran', null, null, null, '2016-01-22 23:05:31', '0', '0');
INSERT INTO `jurnal` VALUES ('12', '2016-01-22', 'Pembayaran Tabungan Siswa tanggal 22-01-2016 siswa AISYAH SUGIASTU PUTRI (15819)', 'Tutik Kusmini', '16000012', '1', '', 'penerimaaniuran', null, null, null, '2016-01-22 23:05:40', '0', '0');
INSERT INTO `jurnal` VALUES ('13', '2016-01-22', 'Pembayaran Tabungan Siswa tanggal 22-01-2016 siswa AISYAH SUGIASTU PUTRI (15819)', 'Tutik Kusmini', '16000013', '1', '', 'penerimaaniuran', null, null, null, '2016-01-22 23:05:55', '0', '0');
INSERT INTO `jurnal` VALUES ('14', '2016-01-22', 'Dana Sumbangan BOS tanggal 22-01-2016 dari APBD', 'Tutik Kusmini', '16000014', '1', '', 'penerimaanlain', null, null, null, '2016-01-22 23:07:06', '0', '0');
INSERT INTO `jurnal` VALUES ('15', '2016-01-22', 'Dana Sumbangan BOS tanggal 22-01-2016 dari APBN', 'Tutik Kusmini', '16000015', '1', '', 'penerimaanlain', null, null, null, '2016-01-22 23:07:35', '0', '0');
INSERT INTO `jurnal` VALUES ('16', '2016-01-22', 'Pembayaran Listrik Bulan Januari', 'Tutik Kusmini', '16000016', '1', '', 'pengeluaran', null, null, null, '2016-01-22 23:10:39', '0', '0');
INSERT INTO `jurnal` VALUES ('17', '2016-01-22', 'Pembayaran Listrik', 'Tutik Kusmini', '16000017', '1', 'Robait Usman', 'jurnalumum', null, null, null, '2016-01-22 23:14:43', '0', '0');
INSERT INTO `jurnal` VALUES ('18', '2016-01-22', 'BEBAN GAJI', 'Tutik Kusmini', '16000018', '1', '', 'jurnalumum', null, null, null, '2016-01-22 23:17:31', '0', '0');
INSERT INTO `jurnal` VALUES ('19', '2016-01-26', 'bayar listrik', 'Tutik Kusmini', '16000019', '1', '', 'pengeluaran', null, null, null, '2016-01-26 21:38:12', '0', '0');
INSERT INTO `jurnal` VALUES ('20', '2016-01-30', 'Pendataan besar pembayaran SPP Bulanan siswa ARYA PRABA P (15855)', 'Tutik Kusmini', '16000020', '1', '', 'penerimaanjtt', null, null, null, '2016-01-30 10:32:29', '0', '0');
INSERT INTO `jurnal` VALUES ('21', '2016-01-30', 'Pendataan besar pembayaran SPP Bulanan siswa DEVI HASNA SALSABELA (15877)', 'Tutik Kusmini', '16000021', '1', '', 'penerimaanjtt', null, null, null, '2016-01-30 10:33:35', '0', '0');

-- ----------------------------
-- Table structure for `jurnaldetail`
-- ----------------------------
DROP TABLE IF EXISTS `jurnaldetail`;
CREATE TABLE `jurnaldetail` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idjurnal` int(10) unsigned NOT NULL,
  `koderek` varchar(15) NOT NULL,
  `debet` decimal(15,0) NOT NULL DEFAULT '0',
  `kredit` decimal(15,0) NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_jurnaldetail_jurnal` (`idjurnal`),
  KEY `IX_jurnaldetail_koderek` (`koderek`),
  KEY `IX_jurnaldetail_ts` (`ts`,`issync`),
  CONSTRAINT `FK_jurnaldetail_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jurnaldetail
-- ----------------------------
INSERT INTO `jurnaldetail` VALUES ('1', '1', '150', '250000', '0', null, null, null, '2013-03-21 19:30:46', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('2', '1', '411', '0', '250000', null, null, null, '2013-03-21 19:30:46', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('3', '2', '111', '100000', '0', null, null, null, '2013-03-21 19:31:17', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('4', '2', '150', '0', '100000', null, null, null, '2013-03-21 19:31:17', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('5', '3', '111', '50000', '0', null, null, null, '2013-03-21 19:31:53', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('6', '3', '150', '0', '50000', null, null, null, '2013-03-21 19:31:53', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('7', '4', '501', '500000', '0', null, null, null, '2013-03-21 19:58:22', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('8', '4', '111', '0', '500000', null, null, null, '2013-03-21 19:58:22', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('9', '5', '150', '100000', '0', null, null, null, '2016-01-22 21:46:59', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('10', '5', '411', '0', '100000', null, null, null, '2016-01-22 21:46:59', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('11', '6', '150', '10000', '0', null, null, null, '2016-01-22 21:47:46', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('12', '6', '411', '0', '10000', null, null, null, '2016-01-22 21:47:46', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('13', '7', '150', '10000000', '0', null, null, null, '2016-01-22 21:48:03', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('14', '7', '411', '0', '10000000', null, null, null, '2016-01-22 21:48:03', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('15', '8', '111', '80000', '0', null, null, null, '2016-01-22 21:48:28', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('16', '8', '150', '0', '80000', null, null, null, '2016-01-22 21:48:28', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('17', '9', '111', '20000', '0', null, null, null, '2016-01-22 21:49:08', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('18', '9', '150', '0', '20000', null, null, null, '2016-01-22 21:49:08', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('19', '10', '112', '100000', '0', null, null, null, '2016-01-22 23:05:17', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('20', '10', '413', '0', '100000', null, null, null, '2016-01-22 23:05:17', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('21', '11', '112', '20000', '0', null, null, null, '2016-01-22 23:05:31', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('22', '11', '413', '0', '20000', null, null, null, '2016-01-22 23:05:31', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('23', '12', '112', '30000', '0', null, null, null, '2016-01-22 23:05:40', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('24', '12', '413', '0', '30000', null, null, null, '2016-01-22 23:05:40', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('25', '13', '112', '23000', '0', null, null, null, '2016-01-22 23:05:55', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('26', '13', '413', '0', '23000', null, null, null, '2016-01-22 23:05:55', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('27', '14', '113', '100000000', '0', null, null, null, '2016-01-22 23:07:06', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('28', '14', '414', '0', '100000000', null, null, null, '2016-01-22 23:07:06', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('29', '15', '113', '200000000', '0', null, null, null, '2016-01-22 23:07:35', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('30', '15', '414', '0', '200000000', null, null, null, '2016-01-22 23:07:35', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('31', '16', '501', '1000000', '0', null, null, null, '2016-01-22 23:10:39', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('32', '16', '111', '0', '1000000', null, null, null, '2016-01-22 23:10:39', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('33', '17', '501', '100000', '0', null, null, null, '2016-01-22 23:14:43', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('34', '17', '111', '0', '100000', null, null, null, '2016-01-22 23:14:43', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('35', '18', '505', '10000000', '0', null, null, null, '2016-01-22 23:17:31', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('36', '18', '112', '0', '9000000', null, null, null, '2016-01-22 23:17:31', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('37', '18', '111', '0', '1000000', null, null, null, '2016-01-22 23:17:31', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('38', '19', '501', '500000', '0', null, null, null, '2016-01-26 21:38:12', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('39', '19', '111', '0', '500000', null, null, null, '2016-01-26 21:38:12', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('40', '20', '150', '800000', '0', null, null, null, '2016-01-30 10:32:29', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('41', '20', '411', '0', '800000', null, null, null, '2016-01-30 10:32:29', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('42', '21', '150', '100000', '0', null, null, null, '2016-01-30 10:33:35', '0', '0');
INSERT INTO `jurnaldetail` VALUES ('43', '21', '411', '0', '100000', null, null, null, '2016-01-30 10:33:35', '0', '0');

-- ----------------------------
-- Table structure for `kategoripenerimaan`
-- ----------------------------
DROP TABLE IF EXISTS `kategoripenerimaan`;
CREATE TABLE `kategoripenerimaan` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siswa` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode`),
  UNIQUE KEY `Index_1` (`replid`),
  KEY `IX_kategoripenerimaan_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kategoripenerimaan
-- ----------------------------
INSERT INTO `kategoripenerimaan` VALUES ('1', 'CSSKR', 'Penerimaan lainnya', '4', '1', null, null, null, '2013-03-22 16:05:05', '31230', '0');
INSERT INTO `kategoripenerimaan` VALUES ('2', 'CSWJB', 'SBPT', '2', '1', null, null, null, '2013-03-22 15:51:07', '37247', '0');
INSERT INTO `kategoripenerimaan` VALUES ('4', 'JTT', 'SPP', '1', '1', null, null, null, '2013-03-22 16:04:18', '27011', '0');
INSERT INTO `kategoripenerimaan` VALUES ('5', 'LNN', 'Block Grand', '5', '1', null, null, null, '2013-03-22 16:04:38', '23315', '0');
INSERT INTO `kategoripenerimaan` VALUES ('3', 'SKR', 'Iuran Siswa Insidental', '3', '1', null, null, null, '2013-03-22 15:51:14', '35541', '0');

-- ----------------------------
-- Table structure for `katerekakun`
-- ----------------------------
DROP TABLE IF EXISTS `katerekakun`;
CREATE TABLE `katerekakun` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  `urutan` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`kategori`),
  UNIQUE KEY `Index_1` (`replid`),
  KEY `IX_katerekakun_ts` (`ts`,`issync`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of katerekakun
-- ----------------------------
INSERT INTO `katerekakun` VALUES ('7', 'BIAYA', '7', null, null, null, '2010-03-02 10:06:52', '42231', '0');
INSERT INTO `katerekakun` VALUES ('1', 'HARTA', '1', null, null, null, '2010-03-02 10:06:52', '38999', '0');
INSERT INTO `katerekakun` VALUES ('3', 'INVENTARIS', '3', null, null, null, '2010-03-02 10:06:52', '2775', '0');
INSERT INTO `katerekakun` VALUES ('5', 'MODAL', '5', null, null, null, '2010-03-02 10:06:52', '27935', '0');
INSERT INTO `katerekakun` VALUES ('6', 'PENDAPATAN', '6', null, null, null, '2010-03-02 10:06:52', '289', '0');
INSERT INTO `katerekakun` VALUES ('2', 'PIUTANG', '2', null, null, null, '2010-03-02 10:06:52', '48701', '0');
INSERT INTO `katerekakun` VALUES ('4', 'UTANG', '4', null, null, null, '2010-03-02 10:06:52', '46047', '0');

-- ----------------------------
-- Table structure for `kelompokbarang`
-- ----------------------------
DROP TABLE IF EXISTS `kelompokbarang`;
CREATE TABLE `kelompokbarang` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kelompok` varchar(255) NOT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `idgroup` int(10) unsigned NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_kelompokbarang_group` (`idgroup`),
  KEY `IX_kelompokbarang_ts` (`ts`,`issync`),
  CONSTRAINT `FK_kelompokbarang_group` FOREIGN KEY (`idgroup`) REFERENCES `groupbarang` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kelompokbarang
-- ----------------------------

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` varchar(3) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `idmenu` varchar(2) DEFAULT NULL,
  `idParent` varchar(1) DEFAULT NULL,
  `idUrut` varchar(2) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `isVisible` varchar(1) DEFAULT NULL,
  `child` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Pemasukan', '1', '0', '1', 'penerimaan.php', null, '1', '1');
INSERT INTO `menu` VALUES ('2', 'Jenis Penerimaan', '1', '1', '2', 'jenispenerimaan.php?from=Penerimaan&sourcefrom=penerimaan.php', null, '1', '0');
INSERT INTO `menu` VALUES ('3', 'Transaksi', '1', '1', '3', 'pembayaran_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('4', 'Pembayaran Tunggakan', '1', '1', '4', 'pembayaran_tunggak_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('5', 'Pembayaran Perkelas', '1', '1', '5', 'lapbayarsiswa_kelas_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('6', 'Pembayaran Siswa Yang Menunggak', '1', '1', '6', 'lapbayarsiswa_nunggak_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('7', 'Lap. Pembayaran Calon Siswa', '1', '1', '7', 'lapbayarcalon_kelompok_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('8', 'Lap. Pembayaran Perkelompok Calon Siswa', '1', '1', '8', 'lapbayarcalon_all_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('9', 'Lap. Pembayaran Perkelompok Calon Siswa Yang Menunggak', '1', '1', '9', 'lapbayarcalon_nunggak_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('10', 'Lap. Penerimaan Lain', '1', '1', '10', 'lappenerimaanlain_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('11', 'Jurnal Penerimaan', '1', '1', '11', 'jurnalpenerimaan_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('12', 'Pengeluaran', '2', '0', '1', 'pengeluaran.php', null, '1', '1');
INSERT INTO `menu` VALUES ('13', 'Jenis Pengeluaran', '2', '1', '2', 'jenispengeluaran.php', null, '1', '0');
INSERT INTO `menu` VALUES ('14', 'Transaksi', '2', '1', '3', 'pengeluaran_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('15', 'Lap. Transaksi', '2', '1', '4', 'lappengeluaran_jenis_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('16', 'Pencaria Data', '2', '1', '5', 'lappengeluaran_cari_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('17', 'Jurnal Pengeluaran', '2', '1', '6', 'jurnalpengeluaran_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('18', 'Jurnal Umum', '3', '0', '1', 'jurnalumum.php', null, '1', '1');
INSERT INTO `menu` VALUES ('19', 'Input Jurnal Umum', '3', '1', '2', 'inputjurnal.php', null, '1', '0');
INSERT INTO `menu` VALUES ('20', 'Cari Data Jurnal Umum', '3', '1', '3', 'carijurnal_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('21', 'Laporan Keuangan', '4', '0', '1', 'lapkeuangan.php', null, '1', '1');
INSERT INTO `menu` VALUES ('22', 'Lap. Transaksi Keuangan', '4', '1', '2', 'laptransaksi_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('23', 'Lap. Audit Perubahan', '4', '1', '3', 'lapbukubesar_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('24', 'Lap. Buku Besar', '4', '1', '4', 'lapbukubesar_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('25', 'Lap. Rugi Laba', '4', '1', '5', 'laprugilaba_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('26', 'Lap. Neraca', '4', '1', '6', 'lapneraca_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('27', 'Lap. Neraca Percobaan', '4', '1', '7', 'lapneracaper_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('28', 'Lap. Perubahan Modal', '4', '1', '8', 'lapmodal_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('29', 'Lap. Arus Kas', '4', '1', '9', 'lapcashflow_main.php', null, '1', '0');
INSERT INTO `menu` VALUES ('30', 'Pengaturan', '5', '0', '1', 'usermenu.php', null, '1', '1');
INSERT INTO `menu` VALUES ('31', 'Daftar Pengguna', '5', '1', '2', 'user.php', null, '1', '0');
INSERT INTO `menu` VALUES ('32', 'Ganti Password', '5', '1', '3', 'user_ganti.php', null, '1', '0');
INSERT INTO `menu` VALUES ('33', 'Keluar', '6', '0', '1', 'logout.php', null, '1', '1');

-- ----------------------------
-- Table structure for `pemohonlain`
-- ----------------------------
DROP TABLE IF EXISTS `pemohonlain`;
CREATE TABLE `pemohonlain` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `IX_pemohonlain_ts` (`ts`,`issync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pemohonlain
-- ----------------------------

-- ----------------------------
-- Table structure for `penerimaaniuran`
-- ----------------------------
DROP TABLE IF EXISTS `penerimaaniuran`;
CREATE TABLE `penerimaaniuran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `nis` varchar(20) NOT NULL,
  `alasan` varchar(500) DEFAULT '" "',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pembayaraniuran_jurnal` (`idjurnal`),
  KEY `FK_pembayaraniuran_datapenerimaan` (`idpenerimaan`),
  KEY `FK_pembayaraniuran_siswa` (`nis`),
  KEY `IX_penerimaaniuran_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pembayaraniuran_datapenerimaan` FOREIGN KEY (`idpenerimaan`) REFERENCES `datapenerimaan` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pembayaraniuran_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pembayaraniuran_siswa` FOREIGN KEY (`nis`) REFERENCES `dbakademik`.`siswa` (`nis`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of penerimaaniuran
-- ----------------------------
INSERT INTO `penerimaaniuran` VALUES ('1', '10', '2016-01-22', '100000', '', 'Tutik Kusmini', '13', '12345', '\" \"', null, null, null, '2016-01-22 23:05:17', '0', '0');
INSERT INTO `penerimaaniuran` VALUES ('2', '11', '2016-01-22', '20000', '', 'Tutik Kusmini', '13', '15819', '\" \"', null, null, null, '2016-01-22 23:05:31', '0', '0');
INSERT INTO `penerimaaniuran` VALUES ('3', '12', '2016-01-22', '30000', '', 'Tutik Kusmini', '13', '15819', '\" \"', null, null, null, '2016-01-22 23:05:40', '0', '0');
INSERT INTO `penerimaaniuran` VALUES ('4', '13', '2016-01-22', '23000', '', 'Tutik Kusmini', '13', '15819', '\" \"', null, null, null, '2016-01-22 23:05:55', '0', '0');

-- ----------------------------
-- Table structure for `penerimaaniurancalon`
-- ----------------------------
DROP TABLE IF EXISTS `penerimaaniurancalon`;
CREATE TABLE `penerimaaniurancalon` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `idcalon` int(10) unsigned NOT NULL,
  `alasan` varchar(500) DEFAULT '" "',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_penerimaaniurancalon_datapenerimaan` (`idpenerimaan`),
  KEY `FK_penerimaaniurancalon_calon` (`idcalon`),
  KEY `FK_penerimaaniurancalon_jurnal` (`idjurnal`),
  KEY `IX_penerimaaniurancalon_ts` (`ts`,`issync`),
  CONSTRAINT `FK_penerimaaniurancalon_calon` FOREIGN KEY (`idcalon`) REFERENCES `dbakademik`.`calonsiswa` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_penerimaaniurancalon_datapenerimaan` FOREIGN KEY (`idpenerimaan`) REFERENCES `datapenerimaan` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_penerimaaniurancalon_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of penerimaaniurancalon
-- ----------------------------

-- ----------------------------
-- Table structure for `penerimaanjtt`
-- ----------------------------
DROP TABLE IF EXISTS `penerimaanjtt`;
CREATE TABLE `penerimaanjtt` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idbesarjtt` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `alasan` varchar(500) DEFAULT ' ',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pembayaranjtt_besatjtt` (`idbesarjtt`),
  KEY `FK_pembayaranjtt_jurnal` (`idjurnal`),
  KEY `IX_penerimaanjtt_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pembayaranjtt_besatjtt` FOREIGN KEY (`idbesarjtt`) REFERENCES `besarjtt` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pembayaranjtt_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of penerimaanjtt
-- ----------------------------
INSERT INTO `penerimaanjtt` VALUES ('1', '1', '2', '2013-03-21', '100000', '', 'landlord', ' ', '0', null, null, '2013-03-21 19:31:17', '31256', '0');
INSERT INTO `penerimaanjtt` VALUES ('2', '1', '3', '2013-03-21', '50000', '', 'landlord', ' ', '0', null, null, '2013-03-21 19:31:53', '34081', '0');
INSERT INTO `penerimaanjtt` VALUES ('3', '2', '8', '2016-01-22', '80000', '', 'Tutik Kusmini', '80000', '0', null, null, '2016-01-22 21:48:28', '0', '0');
INSERT INTO `penerimaanjtt` VALUES ('4', '2', '9', '2016-01-22', '20000', '', 'landlord', ' ', '0', null, null, '2016-01-22 21:49:08', '0', '0');

-- ----------------------------
-- Table structure for `penerimaanjttcalon`
-- ----------------------------
DROP TABLE IF EXISTS `penerimaanjttcalon`;
CREATE TABLE `penerimaanjttcalon` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idbesarjttcalon` int(10) unsigned NOT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `alasan` varchar(500) DEFAULT '" "',
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_penerimaanjttcalon_jurnal` (`idjurnal`),
  KEY `FK_penerimaanjttcalon_besarjtt` (`idbesarjttcalon`),
  KEY `IX_penerimaanjttcalon_ts` (`ts`,`issync`),
  CONSTRAINT `FK_penerimaanjttcalon_besarjttcalon` FOREIGN KEY (`idbesarjttcalon`) REFERENCES `besarjttcalon` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_penerimaanjttcalon_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of penerimaanjttcalon
-- ----------------------------

-- ----------------------------
-- Table structure for `penerimaanlain`
-- ----------------------------
DROP TABLE IF EXISTS `penerimaanlain`;
CREATE TABLE `penerimaanlain` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idjurnal` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `petugas` varchar(100) DEFAULT NULL,
  `idpenerimaan` int(10) unsigned NOT NULL,
  `sumber` varchar(100) NOT NULL,
  `alasan` varchar(500) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pembayaranlain_jurnal` (`idjurnal`),
  KEY `FK_pembayaranlain_datapenerimaan` (`idpenerimaan`),
  KEY `IX_penerimaanlain_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pembayaranlain_datapenerimaan` FOREIGN KEY (`idpenerimaan`) REFERENCES `datapenerimaan` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pembayaranlain_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of penerimaanlain
-- ----------------------------
INSERT INTO `penerimaanlain` VALUES ('1', '14', '2016-01-22', '100000000', '', 'Tutik Kusmini', '12', 'APBD', null, null, null, null, '2016-01-22 23:07:06', '0', '0');
INSERT INTO `penerimaanlain` VALUES ('2', '15', '2016-01-22', '200000000', '', 'Tutik Kusmini', '12', 'APBN', null, null, null, null, '2016-01-22 23:07:35', '0', '0');

-- ----------------------------
-- Table structure for `pengeluaran`
-- ----------------------------
DROP TABLE IF EXISTS `pengeluaran`;
CREATE TABLE `pengeluaran` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idpengeluaran` int(10) unsigned NOT NULL,
  `keperluan` varchar(255) NOT NULL,
  `jenispemohon` tinyint(1) unsigned NOT NULL,
  `nip` varchar(30) DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `pemohonlain` int(10) unsigned DEFAULT NULL,
  `penerima` varchar(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `tanggalkeluar` datetime NOT NULL,
  `jumlah` decimal(15,0) NOT NULL,
  `petugas` varchar(45) DEFAULT NULL,
  `idjurnal` int(10) unsigned NOT NULL,
  `keterangan` text,
  `namapemohon` varchar(100) NOT NULL,
  `alasan` varchar(500) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pengeluaran_nis` (`nis`),
  KEY `FK_pengeluaran_nip` (`nip`),
  KEY `FK_pengeluaran_jurnal` (`idjurnal`),
  KEY `FK_pengeluaran_pemohonlain` (`pemohonlain`),
  KEY `IX_pengeluaran_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pengeluaran_jurnal` FOREIGN KEY (`idjurnal`) REFERENCES `jurnal` (`replid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pengeluaran_nip` FOREIGN KEY (`nip`) REFERENCES `dbpegawai`.`pegawai` (`nip`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pengeluaran_nis` FOREIGN KEY (`nis`) REFERENCES `dbakademik`.`siswa` (`nis`) ON UPDATE CASCADE,
  CONSTRAINT `FK_pengeluaran_pemohonlain` FOREIGN KEY (`pemohonlain`) REFERENCES `pemohonlain` (`replid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pengeluaran
-- ----------------------------
INSERT INTO `pengeluaran` VALUES ('1', '4', 'Pembayaran listrik bulan mei', '1', '1', null, null, 'robait usman', '2013-03-21', '2013-03-21 19:58:22', '500000', 'landlord', '4', '', 'robait usman', null, null, null, null, '2013-03-21 19:58:22', '0', '0');
INSERT INTO `pengeluaran` VALUES ('2', '4', 'Pembayaran Listrik Bulan Januari', '1', '1', null, null, 'robait usman', '2016-01-22', '2016-01-22 23:10:39', '1000000', 'Tutik Kusmini', '16', '', 'robait usman', null, null, null, null, '2016-01-22 23:10:39', '0', '0');
INSERT INTO `pengeluaran` VALUES ('3', '4', 'bayar listrik', '1', '2', null, null, 'Ahasun Naseh', '2016-01-26', '2016-01-26 21:38:12', '500000', 'Tutik Kusmini', '19', 'bayar listrik', 'Ahasun Naseh', null, null, null, null, '2016-01-26 21:38:12', '0', '0');

-- ----------------------------
-- Table structure for `pengguna`
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) NOT NULL,
  `tingkat` tinyint(1) unsigned NOT NULL,
  `departemen` varchar(50) CHARACTER SET latin1 NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_pengguna_pegawai` (`nip`),
  KEY `IX_pengguna_ts` (`ts`,`issync`),
  CONSTRAINT `FK_pengguna_pegawai` FOREIGN KEY (`nip`) REFERENCES `dbpegawai`.`pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pengguna
-- ----------------------------

-- ----------------------------
-- Table structure for `rekakun`
-- ----------------------------
DROP TABLE IF EXISTS `rekakun`;
CREATE TABLE `rekakun` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(15) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode`),
  UNIQUE KEY `UX_rekakun` (`replid`),
  KEY `FK_rekakun_katerekakun` (`kategori`),
  KEY `IX_rekakun_ts` (`ts`,`issync`),
  CONSTRAINT `FK_rekakun_katerekakun` FOREIGN KEY (`kategori`) REFERENCES `katerekakun` (`kategori`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rekakun
-- ----------------------------
INSERT INTO `rekakun` VALUES ('1', '111', 'HARTA', 'Kas', 'Kas yang ada disekolah', null, null, null, '2010-03-02 10:06:53', '18600', '0');
INSERT INTO `rekakun` VALUES ('2', '112', 'HARTA', 'Kas Bank', 'Kas yang ada di bank yang digunakan sekolah', null, null, null, '2010-03-02 10:06:53', '20390', '0');
INSERT INTO `rekakun` VALUES ('3', '113', 'HARTA', 'Kas BOS', 'Kas yang diterima dari sumbangan Bantuan Operasional Sekolah', null, null, null, '2010-03-02 10:06:53', '46147', '0');
INSERT INTO `rekakun` VALUES ('4', '150', 'PIUTANG', 'Piutang Siswa', 'Piutang siswa kepada sekolah', null, null, null, '2010-03-02 10:06:53', '38508', '0');
INSERT INTO `rekakun` VALUES ('5', '151', 'PIUTANG', 'Piutang Karyawan', 'Piutang karyawan kepada sekolah', null, null, null, '2010-03-02 10:06:53', '54096', '0');
INSERT INTO `rekakun` VALUES ('6', '152', 'PIUTANG', 'Piutang Usaha', 'Piutang yang lain kepada sekolah', null, null, null, '2010-03-02 10:06:53', '23895', '0');
INSERT INTO `rekakun` VALUES ('24', '153', 'PIUTANG', 'Piutang Calon Siswa', '', null, null, null, '2012-01-02 07:58:13', '0', '0');
INSERT INTO `rekakun` VALUES ('27', '154', 'PIUTANG', 'Piutang BOS', '', null, null, null, '2012-01-02 08:00:51', '0', '0');
INSERT INTO `rekakun` VALUES ('7', '411', 'PENDAPATAN', 'Pendapatan SPP', 'Pendapatan dari pembayaran SPP siswa', null, null, null, '2010-03-02 10:06:53', '22719', '0');
INSERT INTO `rekakun` VALUES ('8', '412', 'PENDAPATAN', 'Pendapatan DSP', 'Pendapatan dari pembayaran DSP siswa', null, null, null, '2010-03-02 10:06:53', '41907', '0');
INSERT INTO `rekakun` VALUES ('9', '413', 'PENDAPATAN', 'Pendapatan Sukarela Siswa', 'Pendapatan dari perolehan dana sukarela', null, null, null, '2010-03-02 10:06:53', '10317', '0');
INSERT INTO `rekakun` VALUES ('10', '414', 'PENDAPATAN', 'Pendapatan BOS', 'Pendaptan dari penerimaan sumbangan Bantuan Operasional Sekolah (BOS)', null, null, null, '2010-03-02 10:06:53', '56924', '0');
INSERT INTO `rekakun` VALUES ('25', '415', 'PENDAPATAN', 'Pendapatan Sukarela Calon Siswa', '', null, null, null, '2012-01-02 07:58:41', '0', '0');
INSERT INTO `rekakun` VALUES ('20', '421', 'PENDAPATAN', 'Diskon SPP', '', null, null, null, '2012-01-02 07:14:27', '0', '0');
INSERT INTO `rekakun` VALUES ('21', '422', 'PENDAPATAN', 'Diskon DSP', '', null, null, null, '2012-01-02 07:56:29', '0', '0');
INSERT INTO `rekakun` VALUES ('22', '423', 'PENDAPATAN', 'Diskon Sukarela Siswa', '', null, null, null, '2012-01-02 07:56:43', '0', '0');
INSERT INTO `rekakun` VALUES ('23', '424', 'PENDAPATAN', 'Diskon BOS', '', null, null, null, '2012-01-02 07:56:53', '0', '0');
INSERT INTO `rekakun` VALUES ('26', '425', 'PENDAPATAN', 'Diskon Sukarela Calon Siswa', '', null, null, null, '2012-01-02 07:59:15', '0', '0');
INSERT INTO `rekakun` VALUES ('11', '500', 'BIAYA', 'Beban Transportasi', 'Beban yang dikeluarkan untuk pembiayaan transportasi', null, null, null, '2010-03-02 10:06:53', '57077', '0');
INSERT INTO `rekakun` VALUES ('12', '501', 'BIAYA', 'Beban Listrik', 'Beban yang dikeluarkan untuk melunasi tagihan PLN', null, null, null, '2010-03-02 10:06:53', '49084', '0');
INSERT INTO `rekakun` VALUES ('13', '502', 'BIAYA', 'Beban Telpon', 'Beban yang dikeluarkan untuk pembiayaan tagihan telpon', null, null, null, '2010-03-02 10:06:53', '8658', '0');
INSERT INTO `rekakun` VALUES ('14', '503', 'BIAYA', 'Beban Internet', 'Beban yang dikeluarkan untuk pembiayaan taghan Internet', null, null, null, '2010-03-02 10:06:53', '27097', '0');
INSERT INTO `rekakun` VALUES ('15', '504', 'BIAYA', 'Beban ATK', 'Beban yang dikeluarkan untuk pembelian rutin ATK', null, null, null, '2010-03-02 10:06:53', '43981', '0');
INSERT INTO `rekakun` VALUES ('28', '505', 'BIAYA', 'BEBAN GAJI', '', null, null, null, '2016-01-22 23:15:41', '0', '0');
INSERT INTO `rekakun` VALUES ('16', '611', 'INVENTARIS', 'Peralatan Mengajar', 'Inventaris alat-alat kegiatan belajar mengajar', null, null, null, '2010-03-02 10:06:53', '7554', '0');
INSERT INTO `rekakun` VALUES ('17', '612', 'INVENTARIS', 'Kendaraan', 'Inventaris kendaraan sekolah', null, null, null, '2010-03-02 10:06:53', '36888', '0');
INSERT INTO `rekakun` VALUES ('18', '700', 'MODAL', 'Modal Usaha', 'Modal yang ditanamkan oleh pemodal kepada sekolah', null, null, null, '2010-03-02 10:06:53', '30715', '0');
INSERT INTO `rekakun` VALUES ('19', '900', 'UTANG', 'Utang Usaha', 'Utang sekolah kepada kreditur', null, null, null, '2010-03-02 10:06:53', '42913', '0');

-- ----------------------------
-- Table structure for `tahunbuku`
-- ----------------------------
DROP TABLE IF EXISTS `tahunbuku`;
CREATE TABLE `tahunbuku` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tahunbuku` varchar(100) NOT NULL,
  `awalan` varchar(5) NOT NULL,
  `aktif` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keterangan` varchar(255) DEFAULT NULL,
  `cacah` bigint(20) unsigned NOT NULL DEFAULT '0',
  `departemen` varchar(50) NOT NULL,
  `tanggalmulai` date NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_tahunbuku_departemen` (`departemen`),
  KEY `IX_tahunbuku_ts` (`ts`,`issync`),
  CONSTRAINT `FK_tahunbuku_departemen` FOREIGN KEY (`departemen`) REFERENCES `dbakademik`.`departemen` (`departemen`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tahunbuku
-- ----------------------------
INSERT INTO `tahunbuku` VALUES ('1', '2016', '16', '1', '', '21', 'SMAN 1 Malang', '2016-01-01', null, null, null, '2013-03-21 19:58:22', '2163', '0');

-- ----------------------------
-- Table structure for `transaksilog`
-- ----------------------------
DROP TABLE IF EXISTS `transaksilog`;
CREATE TABLE `transaksilog` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sumber` varchar(45) NOT NULL,
  `idsumber` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `transaksi` varchar(255) NOT NULL,
  `petugas` varchar(100) NOT NULL,
  `nokas` varchar(100) NOT NULL,
  `idtahunbuku` int(10) unsigned NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `debet` decimal(15,0) NOT NULL DEFAULT '0',
  `kredit` decimal(15,0) NOT NULL DEFAULT '0',
  `departemen` varchar(50) NOT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`replid`),
  KEY `FK_transaksilog_departemen` (`departemen`),
  KEY `IX_transaksilog_ts` (`ts`,`issync`),
  CONSTRAINT `FK_transaksilog_departemen` FOREIGN KEY (`departemen`) REFERENCES `dbakademik`.`departemen` (`departemen`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transaksilog
-- ----------------------------
