<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$kelas = $_REQUEST['kelas'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

OpenDb();
$sql = "SELECT a.tahunajaran, t.tingkat, k.kelas, a.departemen FROM kelas k, tahunajaran a, tingkat t WHERE k.replid = '$kelas' AND k.idtahunajaran = a.replid AND k.idtingkat = t.replid";
$result = QueryDb($sql);
$row =@mysql_fetch_array($result);
$namatahun = $row['tahunajaran'];
$namatingkat = $row['tingkat'];
$namakelas = $row['kelas'];
$departemen = $row['departemen'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Pendataan PIN]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center>
  <font size="4"><strong>PENDATAAN PIN</strong></font><br />
 </center><br /><br />

<table width="100%">    
	<tr>
		<td width="15%"><strong>Departemen</strong> </td> 
		<td width="*"><strong>:&nbsp;<?php echo $departemen?></strong></td>
	</tr>
    <tr>
		<td><strong>Tahun Ajaran </strong></td>
		<td><strong>:&nbsp;<?php echo $namatahun?></strong></td>        		
    </tr>
    <tr>
		<td><strong>Kelas</strong></td>
		<td><strong>:&nbsp;<?php echo $namatingkat." - ".$namakelas?></strong></td>        		
    </tr>
	</table>
<br />
	<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="15%" class="header" align="center">N I S</td>
        <td width="*" class="header" align="center">Nama</td>
        <td width="12%" class="header" align="center">PIN Siswa</td>        
        <td width="12%" class="header" align="center">PIN Ayah</td>
		<td width="12%" class="header" align="center">PIN Ibu</td>
    </tr>
<?php 	OpenDb();
	$sql = "SELECT * FROM $g_db_akademik.siswa s WHERE s.idkelas = '$kelas' AND s.aktif = 1 ORDER BY $urut $urutan ";   
	$result = QueryDB($sql);
	$cnt = 0;
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">    	
    	<td align="center"><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row['nis'] ?></td>        
        <td><?php echo $row['nama'] ?></td>
        <td align="center"><?php echo $row['pinsiswa'] ?></td>
        <td align="center"><?php echo $row['pinortu'] ?></td>
		<td align="center"><?php echo $row['pinortuibu'] ?></td>
   	</tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>

</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>