<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
$departemen=$_REQUEST['departemen'];
?>
		<select name="dep_asal" id="dep_asal"  onKeyPress="return focusNext('sekolah', event)" onChange="change_departemen()">
        <option value="">[Departemen]</option>
      	<?php // Olah untuk combo sekolah
		OpenDb();
		$sql_dep_asal="SELECT DISTINCT departemen FROM asalsekolah ORDER BY departemen";
		$result_dep_asal=QueryDB($sql_dep_asal);
		while ($row_dep_asal = mysql_fetch_array($result_dep_asal)) {
			if ($departemen=="")
				$departemen=$row_dep_asal['departemen'];
		?>
       <option value="<?php echo $row_dep_asal['departemen']?>" <?php echo StringIsSelected($row_dep_asal['departemen'],$departemen)?>>
        <?php echo $row_dep_asal['departemen']?>
        </option>
      <?php
    	} 
		CloseDb();
		// Akhir Olah Data sekolah
		?>
    	</select>