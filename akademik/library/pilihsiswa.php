<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php'); 

$flag = 0;
if (isset($_REQUEST['flag']))
	$flag = (int)$_REQUEST['flag'];

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if ($departemen == -1) 
	$departemen = 'Semua departemen';
	
?>


	<table border="0" width="100%" cellpadding="2" cellspacing="2" align="center">
	<tr>
    	<td>
		<input type="hidden" name="flag" id="flag" value="<?php echo $flag ?>" />
		<font size="2"><strong>Daftar Siswa</strong></font><br />	
		Departemen: 
        <strong><input type="text" name="departemen" id="departemen" value="<?php echo $departemen ?>" size="10" readonly style="background-color:#CCCCCC" /></strong>&nbsp;&nbsp;</td>
	</tr>
	<tr>
    <td>
		<br />
		<table width="100%" id="table" class="tab" align="center" cellpadding="2" cellspacing="0" bordercolor="#000000">
		<tr height="30">
			<td class="header" width="7%" align="center">No</td>
    		<td class="header" width="15%" align="center">N I S</td>
    		<td class="header" >Nama</td>
    		<td class="header" width="10%">&nbsp;</td>
		</tr>
		<?php
		OpenDb();
		$sql = "SELECT p.nip, p.nama FROM $g_db_pegawai.pegawai p LEFT JOIN (guru g LEFT JOIN pelajaran l ON l.replid = g.idpelajaran) ON p.nip = g.nip GROUP BY p.nip";
		//$sql = "SELECT p.nip, p.nama FROM $g_db_pegawai.pegawai p LEFT JOIN (guru g LEFT JOIN pelajaran l ON l.replid = g.idpelajaran) ON p.nip = g.nip GROUP BY p.nip";
		$result = QueryDb($sql);
		$cnt = 0;
		while($row = mysql_fetch_row($result)) { ?>
		<tr>
			<td align="center"><?php echo ++$cnt ?></td>
    		<td align="center"><?php echo $row[0] ?></td>
    		<td><?php echo $row[1] ?></td>
    		<td align="center">
    		<input type="button" name="pilih" class="but" id="pilih" value="Pilih" onClick="pilih('<?php echo $row[0]?>', '<?php echo $row[1]	?>')" />
    	   	</td>
		</tr>
		<?php 	} ?>
		<tr height="26">
			<td colspan="4" align="center" >
        	<input type="button" class="but" name="tutup" id="tutup" value="Tutup" onClick="window.close()" /></td>
		</tr>	
		</table>
		</td>
	</tr>
	</table>