<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once("../include/theme.php");

$replid = $_REQUEST['replid'];
$tahunajaran = $_REQUEST['tahunajaran'];

OpenDb();
$sql = "SELECT t.departemen, t.tahunajaran, i.deskripsi, i.idtahunajaran FROM tahunajaran t, infojadwal i WHERE i.replid = '$replid' AND i.idtahunajaran = t.replid";
$result = QueryDb($sql);
$row = mysql_fetch_array($result);
$departemen = $row['departemen'];
$tahun = $row['tahunajaran'];
$tahunajaran = $row['idtahunajaran'];
$deskripsi = $row['deskripsi'];
if (isset($_REQUEST['deskripsi']))
	$deskripsi=CQ($_REQUEST['deskripsi']);

$ERROR_MSG = "";
if (isset($_REQUEST['Simpan'])) {	
	
	OpenDb();
	$sql_simpan_cek="SELECT * FROM $g_db_akademik.infojadwal WHERE deskripsi='$deskripsi' AND idtahunajaran = '$tahunajaran' AND replid='$replid'"; 	
	
	$result_simpan_cek=QueryDb($sql_simpan_cek);	
	if (mysql_num_rows($result_simpan_cek) > 0) {
		CloseDb();
		$ERROR_MSG = $deskripsi." sudah digunakan!";
	} else {
		$sql_simpan="UPDATE $g_db_akademik.infojadwal SET deskripsi='$deskripsi' WHERE replid = '$replid'";
		$result_simpan=QueryDb($sql_simpan);
		CloseDb();
		if ($result_simpan){		
		?>
		<script language="javascript">				
			opener.refresh('<?php echo $replid?>');			
			window.close();
		</script>
		<?php
		}
	}	
	CloseDb();
}




?>
<html>
<head>
<title>LMS MAN Kota Blitar[Ubah Info Jadwal]</title>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript" src="../script/tables.js"></script>
<script type="text/javascript" language="javascript" src="../script/tools.js"></script>
<script type="text/javascript" language="javascript" src="../script/validasi.js"></script>
<script type="text/javascript" language="javascript">
function validate() {
	return validateEmptyText('deskripsi', 'Jadwal'); 
}
function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('deskripsi').focus();">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
    .: Ubah Info Jadwal :.
    </div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
    <!-- CONTENT GOES HERE //--->
	<form name="main" id="main" onSubmit="return validate()" action="info_jadwal_edit.php">
    <input type="hidden" name="replid" id="replid" value="<?php echo $replid ?>" />   
	<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
    <!-- TABLE CONTENT -->
    <tr>
        <td width="35%"><strong>Departemen</strong></td>
        <td><input type="text" name="dept" size="10" maxlength="50" class="disabled" readonly value="<?php echo $departemen?>"/>                
        </td>
    </tr>
    <tr>
        <td><strong>Tahun Ajaran</strong></td>
        <td><input type="text" name="tahun" size="10" value="<?php echo $tahun ?>" readonly class="disabled"/>
		<input type="hidden" name="tahunajaran" id="tahunajaran" value="<?php echo $tahunajaran?>">       
        </td>
    </tr>
    <tr>
        <td><strong>Info Jadwal</strong></td>
        <td><input type="text" name="deskripsi" id="deskripsi"  size="30" value="<?php echo $deskripsi?>" onKeyPress="return focusNext('Simpan', event)"></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" name="Simpan" value="Simpan" class="but" id="Simpan">
            <input type="button" name="Tutup" value="Tutup" class="but" onClick="window.close()">
        </td>
    </tr>
	</table>
	</form>

<!-- END OF CONTENT //--->
    </td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>
<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo $ERROR_MSG?>');		
</script>
<?php } ?>
</body>
</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("deskripsi");
</script>