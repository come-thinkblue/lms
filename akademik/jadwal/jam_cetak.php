<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../cek.php');
require_once('../include/getheader.php');
$departemen = "";
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
OpenDb();
$sql_jam="SELECT replid, jamke, HOUR(jam1) As jammulai, MINUTE(jam1) As menitmulai, HOUR(jam2) As jamakhir, MINUTE(jam2) As menitakhir FROM $g_db_akademik.jam WHERE departemen='$departemen' ORDER BY jamke ASC";
$result_jam=QueryDb($sql_jam);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Definisi Jam <?php echo $departemen?>]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center><font size="4"><strong>DEFINISI JAM</strong></font><br /> </center><br /><br />

<br />
	<table id="table" border="0" width="95%" />
    <tr><td align="left">
    &nbsp;&nbsp;&nbsp;&nbsp;<strong>Departemen :</strong> <?php echo $departemen?>
    </td></tr>
    </table><br />
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="95%" align="center" bordercolor="#000000" />
    <!-- TABLE CONTENT -->
    <tr height="30" class="header" align="center">	
		<td width="20%">Jam ke</td>
	  	<td width="*">Waktu</td>
	</tr>
	<?php 
		while ($row_jam=@mysql_fetch_row($result_jam)){
			if ((int)$row_jam[2]<10) 
				$jammulai="0".$row_jam[2]; 
			else  
				$jammulai=$row_jam[2]; 
					
			if ((int)$row_jam[3]<10) 
				$menitmulai="0".$row_jam[3]; 
			else  
				$menitmulai=$row_jam[3]; 
				
			if ((int)$row_jam[4]<10) 
				$jamakhir="0".$row_jam[4]; 
			else  
				$jamakhir=$row_jam[4]; 
					
			if ((int)$row_jam[5]<10) 
				$menitakhir="0".$row_jam[5]; 
			else  
				$menitakhir=$row_jam[5]; 
				
                    
	?> 
	<tr height="25">
		<td align="center"><?php echo $row_jam[1] ?> </td>
		<td><?php echo $jammulai.":".$menitmulai ?> - <?php echo $jamakhir.":".$menitakhir ?></td>
	</tr>
	<?php	}	CloseDb();	?> 
	</table>  

</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>