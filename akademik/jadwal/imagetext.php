<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	header("Content-Type: image/png");
	if(!isset($_SESSION)){ session_start();}
	$string = $_REQUEST["string"];
	$string = str_replace(' ','',$string);
	$span = (int)$_REQUEST["span"];
	//----------------
	//create image
  	$height=25;
	$width=$span*$height;
  	 	
  	$fontwidth = ImageFontWidth($font) * strlen($string);
  	$fontheight = ImageFontHeight($font);
  	$w = ($width/2)-($fontwidth-3);
	$font=5; 
	if ($span==1){
		$w=7;
		$font=2;
		$s=split('-',$string);
		$string=$s[0]."\n-\n".$s[1];
	}
	$im = imagecreate ($width,$height);
  	$background_color = imagecolorallocate ($im, 181, 181, 181);
  	$text_color = imagecolorallocate ($im, 0, 0, 0);
  	imagerectangle($im,0,0,$width-1,$height-1,imagecolorallocate ($im, 181, 181, 181));
  	if ($span==1){
		$w1=$w;
		$w2=$w;
		if (strlen($s[0])==1)
			$w1=$w1+$font+1;
		if (strlen($s[1])==1)
			$w2=$w2+$font+1;
		imagestring ($im, $font, $w1, 0,  $s[0], $text_color);
		imagestring ($im, $font, $w+3, $font*3,  '-', $text_color);
		imagestring ($im, $font, $w2, $font*6,  $s[1], $text_color);
  	} else {
		imagestring ($im, $font, $w, $font-1,  $string, $text_color);
	}
	imagepng ($im);
  	ImageDestroy($im);
  	//----------------
?>