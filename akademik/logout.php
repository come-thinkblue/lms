<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
session_name("dbakademik");
if(!isset($_SESSION)){ session_start();}

unset($_SESSION['login']);
unset($_SESSION['namasimaka']);
unset($_SESSION['tingkatsimaka']);
unset($_SESSION['departemensimaka']);
unset($_SESSION['errtype']);
unset($_SESSION['errfile']);
unset($_SESSION['errno']);
unset($_SESSION['errmsg']);
unset($_SESSION['issend']);
$_SESSION['maintenance'] = false;
?>
<script language="javascript">
	top.window.location='../index.php?link=akademik';
</script>
