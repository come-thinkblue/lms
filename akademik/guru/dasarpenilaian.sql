-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 23, 2016 at 10:14 AM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `dasarpenilaian`
--

CREATE TABLE IF NOT EXISTS `dasarpenilaian` (
  `replid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dasarpenilaian` varchar(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `info1` varchar(255) DEFAULT NULL,
  `info2` varchar(255) DEFAULT NULL,
  `info3` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` smallint(5) unsigned NOT NULL DEFAULT '0',
  `issync` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_ktsp` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`dasarpenilaian`),
  UNIQUE KEY `UX_dasarpenilaian_replid` (`replid`),
  KEY `IX_dasarpenilaian_ts` (`ts`,`issync`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dasarpenilaian`
--

INSERT INTO `dasarpenilaian` (`replid`, `dasarpenilaian`, `keterangan`, `info1`, `info2`, `info3`, `ts`, `token`, `issync`, `is_ktsp`) VALUES
(3, 'AF', 'AFEKTIF', NULL, NULL, NULL, '2016-01-22 11:32:59', 0, 0, 1),
(4, 'KI1', 'KOMPETENSI INTI 1', NULL, NULL, NULL, '2016-03-23 09:30:00', 0, 0, 0),
(5, 'KI2', 'KOMPETENSI INTI 2', NULL, NULL, NULL, '2016-03-23 09:30:00', 0, 0, 0),
(6, 'KI3', 'KOMPETENSI INTI 3', NULL, NULL, NULL, '2016-03-23 09:30:42', 0, 0, 0),
(7, 'KI4', 'KOMPETENSI INTI 4', NULL, NULL, NULL, '2016-03-23 09:50:59', 0, 0, 0),
(1, 'KOG', 'KOGNITIF', NULL, NULL, NULL, '2016-01-22 11:32:28', 0, 0, 1),
(2, 'PSI', 'PSIKOMOTORIK', NULL, NULL, NULL, '2016-01-22 11:32:46', 0, 0, 1);