<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../cek.php');
require_once('../library/dpupdate.php');

OpenDb();
$kurikulum = 2;
if (isset($_REQUEST['kurikulum']))
	$kurikulum = $_REQUEST['kurikulum'];
$op = "";
if (isset($_REQUEST['op']))
	$op = $_REQUEST['op'];

if ($op == "xm8r389xemx23xb2378e23") 
{
	$sql = "DELETE FROM $g_db_akademik.dasarpenilaian WHERE replid = '$_REQUEST[replid]'";
	$result = QueryDb($sql);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tooltips.js"></script>
<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function tambah(kurikulum) {	
	newWindow('aspeknilai_add.php?kurikulum='+kurikulum, 'TambahAspekNilai','530','350','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function refresh() {
	var kurikulum = document.getElementById('kurikulum').value;
	document.location.href = "aspeknilai.php?kurikulum="+kurikulum;
}

function change() {
	var kurikulum = document.getElementById('kurikulum').value;
	document.location.href = "aspeknilai.php?kurikulum="+kurikulum;
}

function edit(replid) {
	newWindow('aspeknilai_edit.php?replid='+replid, 'UbahAspekNilai','530','350','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function hapus(replid) {
	if (confirm("Apakah anda yakin akan menghapus aspek penilaian ini?"))
		document.location.href = "aspeknilai.php?op=xm8r389xemx23xb2378e23&replid="+replid;
}

function cetak(kurikulum) {
	newWindow('aspeknilai_cetak.php?kurikulum='+kurikulum, 'CetakAspekNilai','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
}
</script>
</head>

<body>



<tr>

<table border="0" width="100%" align="center">
<!-- TABLE CENTER -->
<tr height="300">
  <td align="left" valign="top">

	<table border="0"width="95%" align="center">
    <tr>
        <td align="right"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Aspek Penilaian</font></td>
    </tr>
    <tr>
        <td align="right"><a href="../guru.php?page=p" target="content">
          <font size="1" face="Verdana" color="#000000"><b>Guru & Pelajaran</b></font></a>&nbsp>&nbsp <font size="1" face="Verdana" color="#000000"><b>Aspek Penilaian</b></font>
        </td>
    </tr>
     <tr>
      <td align="left">&nbsp;</td>
      </tr>
	</table>
	<br /><br />
    <?php
	
	$sql = "SELECT replid, dasarpenilaian, keterangan 
			  FROM dasarpenilaian WHERE idkurikulum='$kurikulum'";
	
	$result = QueryDb($sql);
	if (@mysql_num_rows($result) > 0)
	{
	?>
    <table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
    <!-- TABLE CONTENT -->
    <tr>
	<td align="left">Kurikulum 
		<select name='kurikulum' id='kurikulum' onChange='change()'>
			<?php 
				$select = "SELECT replid, kurikulum FROM kurikulum";
				$hasil = QueryDb($select);
				while ($row = mysql_fetch_row($hasil)){
					if($kurikulum==$row[0])
						echo "<option value='$row[0]' selected='selected'>$row[1]</option>";
					else
						echo "<option value='$row[0]'>$row[1]</option>";
				}
			?>
		</select>
	</td>
	<td align="right">
    
    <a href="#" onClick="refresh()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
    <a href="JavaScript:cetak(<?php echo $kurikulum ?>)"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')" />&nbsp;Cetak</a>&nbsp;&nbsp;    
<?php	if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
	    <a href="JavaScript:tambah(<?php echo $kurikulum ?>)"><img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')" />&nbsp;Tambah Aspek Penilaian</a>
<?php	} ?>    
    </td></tr>
    </table><br />
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="95%" align="center" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="15%" class="header" align="center">Kode</td>
        <td width="*" class="header" align="center">Aspek Penilaian</td>
        <?php	if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
        <td width="8%" class="header">&nbsp;</td>
        <?php	} ?>
    </tr>
<?php 	
	$cnt = 0;
	while ($row = mysql_fetch_row($result)) { ?>
    <tr height="25">
    	<td align="center"><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row[1] ?></td>
        <td><?php echo $row[2] ?></td>
<?php		if (SI_USER_LEVEL() != $SI_USER_STAFF) {  ?>         
		<td align="center">
            <a href="JavaScript:edit(<?php echo $row[0] ?>)"><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Departemen!', this, event, '80px')" /></a>&nbsp;
            <a href="JavaScript:hapus(<?php echo $row[0] ?>)"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Departemen!', this, event, '80px')"/></a>
        </td>
<?php		} ?>  
    </tr>
<?php	} ?>
    <!-- END TABLE CONTENT -->
    </table>
    <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>

	</td></tr>
<!-- END TABLE CENTER -->    
</table>
<?php	} else { ?>

<table width="100%" border="0" align="center">

<tr>
	<td align="center" valign="middle" height="250" colspan="2">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data.
       <?php if (SI_USER_LEVEL() != $SI_USER_STAFF) { ?>
        <br />Klik &nbsp;<a href="JavaScript:tambah()" ><font size = "2" color ="green">di sini</font></a>&nbsp;untuk mengisi data baru.
        <?php } ?>
        </p></b></font>
	</td>
</tr>
</table>  
<?php } ?> 
</td></tr>
<!-- END TABLE BACKGROUND IMAGE -->
</table>    

</body>
</html>
<?php CloseDb();?>
<script language="javascript">
	var spryselect1 = new Spry.Widget.ValidationSelect("kurikulum");
</script>