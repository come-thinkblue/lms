<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

$replid = $_REQUEST['replid'];
$fr_nil = $_REQUEST['fr_nil'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['pelajaran']))
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kode']))
	$kode = CQ($_REQUEST['kode']);
if (isset($_REQUEST['kode_lama']))
	$kode_lama = CQ($_REQUEST['kode_lama']);
if (isset($_REQUEST['materi']))
	$materi = CQ($_REQUEST['materi']);	
if (isset($_REQUEST['deskripsi']))
	$deskripsi = CQ($_REQUEST['deskripsi']);	

if (isset($_REQUEST['Simpan'])) {
  OpenDb();
  if($kode_lama==$kode){
	$sql = "UPDATE rpp SET koderpp = '$kode', rpp = '$materi', deskripsi = '$deskripsi' WHERE replid = '$replid'";
	$result = QueryDb($sql);
	CloseDb();
	
	if ($result) { ?>
		<script language="javascript">			
			opener.refresh(<?php echo $replid?>);
			//opener.location.href = "rpp_footer.php?semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&pelajaran=<?php echo $pelajaran?>";
			window.close();
		</script> 
	<?php	}
  }else{
	$sql = "SELECT * FROM rpp WHERE koderpp = '$kode' AND idpelajaran='$pelajaran' AND aktif='1'";
	$result = QueryDb($sql);
	
	if (mysql_num_rows($result) > 0) {
		CloseDb();
		$ERROR_MSG = "Kode pembelajaran $kode sudah digunakan!";	
	} else {
		$sql = "UPDATE rpp SET koderpp = '$kode', rpp = '$materi', deskripsi = '$deskripsi' WHERE replid = '$replid'";
		$result = QueryDb($sql);
		CloseDb();
	
		if ($result) { ?>
			<script language="javascript">			
				opener.refresh(<?php echo $replid?>);
				//opener.location.href = "rpp_footer.php?semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&pelajaran=<?php echo $pelajaran?>";
				window.close();
			</script> 
<?php		}
	}
	}
	
}
OpenDb();
$sql = "SELECT r.koderpp, r.rpp, r.deskripsi, r.idsemester, r.idtingkat, r.idpelajaran, s.semester, t.tingkat, p.nama, s.departemen FROM rpp r, semester s, tingkat t, pelajaran p WHERE r.replid = '$replid' AND s.replid = r.idsemester AND t.replid = r.idtingkat AND p.replid = r.idpelajaran";

$result = QueryDb($sql);
$row = mysql_fetch_array($result);
$kode = $row['koderpp'];
if (isset($_REQUEST['kode']))
	$kode = $_REQUEST['kode'];
$materi = $row['rpp'];
if (isset($_REQUEST['materi']))
	$materi = $_REQUEST['materi'];
$deskripsi = $row['deskripsi'];
if (isset($_REQUEST['deskripsi']))
	$deskripsi = $_REQUEST['deskripsi'];
$semester = $row['idsemester'];
$tingkat = $row['idtingkat'];
$pelajaran = $row['idpelajaran'];
$namasemester = $row['semester'];
$namatingkat = $row['tingkat'];
$namapel = $row['nama'];
$departemen = $row['departemen'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Ubah Rencana Program Pengajaran]</title>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script src="../script/tiny_mce/tiny_mce.js" type="text/javascript"></script>
<script language="javascript">
//textarea
tinyMCE.init({
	mode : "textareas",
	theme : "simple",
});

function validate() {
	return 	validateEmptyText('kode', 'Kode pembelajaran') && 
			validateEmptyText('materi', 'Materi pembelajaran') && 
		   	validateMaxText('materi', 255, 'Materi pembelajaran');
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
		return false;
    } 
    return true;
}

function panggil(elem){
	var lain = new Array('kode','materi','deskripsi');
	for (i=0;i<lain.length;i++) {
		if (lain[i] == elem) {
			document.getElementById(elem).style.background='#4cff15';
		} else {
			document.getElementById(lain[i]).style.background='#FFFFFF';
		}
	}
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('kode').focus();">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
    .: Ubah Rencana Program Pembelajaran :.
    </div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
    <!-- CONTENT GOES HERE //--->
<form name="main" onSubmit="return validate()" action="rpp_edit.php" method="post">
<input type="hidden" name="replid" id="replid" value="<?php echo $replid?>"/>
<input type="hidden" name="semester" id="semester" value="<?php echo $semester?>"/>
<input type="hidden" name="tingkat" id="tingkat" value="<?php echo $tingkat?>"/>
<input type="hidden" name="pelajaran" id="pelajaran" value="<?php echo $pelajaran?>"/>
<input type="hidden" name="fr_nil" id="fr_nil" value="<?php echo $fr_nil?>"/>
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
	<td width="12%"><strong>Departemen</strong></td>
	<td width="12%">
    	<input type="text" name="departemen1" id="departemen1" size="10" readonly value="<?php echo $departemen ?>" class="disabled" />
    	<input type="hidden" name="departemen" id="departemen" value="<?php echo $departemen ?>" />    
    <td width="12%"><strong>Tingkat</strong></td>
    <td>
        <input type="text" name="tingkat1" id="tingkat1" size="10" readonly value="<?php echo $namatingkat ?>" class="disabled" />
        <input type="hidden" name="tingkat" id="tingkat" value="<?php echo $tingkat?>"/>
        
        	</td>
    <td><strong>&nbsp;Semester&nbsp;</strong></td>
    <td><input type="text" name="semester1" id="semester1" size="21" readonly value="<?php echo $namasemester ?>" class="disabled" />
        <input type="hidden" name="semester" id="semester" value="<?php echo $semester?>"/></td>
</tr>
<tr>
	<td width="50"><strong>No KD</strong></td>
	<td>
		<input type="hidden" name="kode_lama" value="<?php echo $kode ?>" />
    	<input type="text" name="kode" id="kode" size="10" maxlength="20" value="<?php echo $kode?>" onKeyPress="return focusNext('materi', event)"/>    </td>
    <td><strong>Pelajaran</strong></td>
    <td colspan="3">
         <input type="text" name="pelajaran1" id="pelajaran1" size="48" readonly value="<?php echo $namapel ?>" class="disabled" />
        <input type="hidden" name="pelajaran" id="pelajaran" value="<?php echo $pelajaran?>"/>    </td>
</tr>
<tr>
	<td valign="top"><strong>Kompetensi Inti</strong></td>
	<td colspan="5">
		<select name="materi" id="materi" maxlength="225" value="<?php echo $materi?>" onFocus="panggil('materi')"  onKeyPress="return focusNext('deskripsi', event)">
			<?php
			if($materi=='KI3'){
				echo 
				"<option selected='select' value='KI3'>KI3</option>
				<option value='KI4'>KI4</option>";
			}else{
				echo 
				"<option value='KI3'>KI3</option>
				<option selected='select' value='KI4'>KI4</option>";
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td colspan = "6" height="200" valign="top">
	<fieldset><legend><b>Uraian Kompetensi Dasar (KD)</b></legend>
    <br />
    <textarea name="deskripsi" id="deskripsi" rows="20" onFocus="panggil('deskripsi')" style="width:100%"><?php echo $deskripsi?></textarea>
    </fieldset></tr>
<tr>
	<td colspan="6" align="center">
    <input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
    <input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
 <!-- END OF CONTENT //--->
    </td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>

<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo $ERROR_MSG?>');
</script>
<?php } ?>

</body>
</html>