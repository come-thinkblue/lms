<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	$tingkat = $_GET['idtingkat'];
	$aspek = $_GET['aspek'];
	
	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		for($i=1; $i<=$_REQUEST['jml']; $i++){
			if(CQ($_REQUEST['huruf'.$i])!=null && CQ($_REQUEST['min'.$i])!=null && CQ($_REQUEST['max'.$i])!=null){
				$queryInsert ="UPDATE aturangrading SET nmin='".CQ($_REQUEST['min'.$i])."',nmax='".CQ($_REQUEST['max'.$i])."',grade='".CQ($_REQUEST['huruf'.$i])."'
								WHERE replid='".CQ($_REQUEST['id'.$i])."'";
				$result = QueryDb($queryInsert);
			}else{
				$del = "DELETE FROM aturangrading WHERE replid='".CQ($_REQUEST['id'.$i])."'";
				$result = QueryDb($del);
			}
		}
		if ($result) {
			?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
		<?php
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar [Edit Aturan Grading Nilai]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function validate() {
				return validateEmptyText('min', 'Nilai Min') && 
					validateEmptyText('max', 'Nilai Max') && 
					validateEmptyText('huruf', 'Huruf') && 
			}

			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'id_gradenilai')
						caripegawai();
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('min','max','huruf');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nilai_min').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo  GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Edit Aturan Grading Nilai :.
					</div>
				</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="urut" id="urut" value="<?php echo  $urut ?>"/>
						<input type="hidden" name="urutan" id="urutan" value="<?php echo  $urutan ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<tr>
								<td width="15"><strong>Huruf</strong></td>
								<td><strong>Range Nilai</strong></td>
							</tr>
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT replid,grade,nmin,nmax FROM aturangrading WHERE idtingkat='$tingkat' AND dasarpenilaian='$aspek' ORDER BY grade";
								//echo "$sql";
								$rs = QueryDb($sql);
								$jml = mysql_num_rows($rs);
								$i=0;
								while ($row = mysql_fetch_array($rs)){
								$i++;
							?>
							<tr>
								<input type="hidden" name="id<?php echo $i;?>" value="<?php echo $row['replid'];?>"/>
								<td><input type="text" name="huruf<?php echo $i;?>" size="5" value="<?php echo $row['grade'];?>"/></td>
								<td>
								<input type="text" name="min<?php echo $i;?>" id="min" size="5" onFocus="showhint('Nilai Min tidak boleh kosong !', this, event, '120px');panggil('min')" value="<?php echo  $row['nmin'];?>" onKeyPress="return focusNext('max', event)"/>-
								<input type="text" name="max<?php echo $i;?>" id="max" size="5" onFocus="showhint('Nilai Max tidak boleh kosong !', this, event, '120px');panggil('max')" value="<?php echo  $row['nmax'];?>"/>
								</td>
							</tr>
							<?php
								}
							?>
							<tr>
								<input type="hidden" name="jml" value="<?php echo $jml;?>"/>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo  GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo  $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>