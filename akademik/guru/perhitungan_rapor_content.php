<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');
require_once('../library/dpupdate.php');

$cetak = 0;
$id_pelajaran = $_REQUEST['id_pelajaran'];
$nip_guru = $_REQUEST['nip_guru'];
$id_tingkat = $_REQUEST['id_tingkat'];
$aspek = $_REQUEST['aspek'];

OpenDb();
$sql = "SELECT departemen FROM departemen WHERE aktif=1"; 
$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$departemen = $row[0];

$op = $_REQUEST['op'];
if ($op == "dw8dxn8w9ms8zs22") 
{
	$newaktif = (int)$_REQUEST['newaktif'];
	$replid = (int)$_REQUEST['replid'];
	$sql = "UPDATE aturannhb SET aktif = '$newaktif' WHERE replid = '$replid' ";
	QueryDb($sql);
} 
elseif ($op == "xm8r389xemx23xb2378e23") 
{	
	$sql = "DELETE FROM aturannhb WHERE idtingkat = '$id_tingkat' AND dasarpenilaian = '$aspek'"; 
	QueryDb($sql);	?>
    <script>refresh();</script> 
<?php
}	
?>
<html>
<head>
<title></title>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="JavaScript">

var win = null;
function newWindow(mypage,myname,w,h,features) 
{
      var winl = (screen.width-w)/2;
      var wint = (screen.height-h)/2;
      if (winl < 0) winl = 0;
      if (wint < 0) wint = 0;
      var settings = 'height=' + h + ',';
      settings += 'width=' + w + ',';
      settings += 'top=' + wint + ',';
      settings += 'left=' + winl + ',';
      settings += features;
      win = window.open(mypage,myname,settings);
      win.window.focus();
}

function refresh() {
    document.location.reload;
}

function changepage() {
	var departemen = document.tampil_aturannhb.departemen.value;
	
	document.location.href = "tampil_daftarpelajaran.php?departemen="+departemen;
}
function setaktif(replid,aktif) {
	var msg;
	var newaktif;	
	var nip = document.getElementById('nip_guru').value;
	var id_pelajaran = document.getElementById('id_pelajaran').value;
	if (aktif == 1) {
		msg = "Apakah anda yakin akan mengubah bobot penilaian ini menjadi TIDAK AKTIF?";
		newaktif = 0;
	} else	{	
		msg = "Apakah anda yakin akan mengubah bobot penilaian ini menjadi AKTIF?";
		newaktif = 1;
	}
	
	if (confirm(msg)) 
	document.location.href = "perhitungan_rapor_content.php?op=dw8dxn8w9ms8zs22&replid="+replid+"&newaktif="+newaktif+"&nip_guru="+nip+"&id_pelajaran="+id_pelajaran;
}

function tambah(tingkat, kurikulum) {
	newWindow('perhitungan_rapor_add.php?id_tingkat='+tingkat+'&idkurikulum='+kurikulum, 'TambahAturanPerhitunganNilaiRapor','360','600','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function edit(tingkat,aspek,kurikulum) {
	newWindow('perhitungan_rapor_edit.php?id_tingkat='+tingkat+'&idkurikulum='+kurikulum+"&aspek="+aspek, 'UbahAturanPerhitunganNilaiRapor','360','600','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function hapus(tingkat,aspek) {
	
	if (confirm("Apakah anda yakin akan menghapus aspek penilaian ini?"))
		document.location.href = "perhitungan_rapor_content.php?op=xm8r389xemx23xb2378e23&id_tingkat="+tingkat+"&aspek="+aspek;
}

function cetak() {
	var nip = document.getElementById('nip_guru').value;
	var id = document.getElementById('id_pelajaran').value;
	var cetak = document.getElementById('cetak').value;	
	if (cetak == '1') 
		newWindow('perhitungan_rapor_cetak.php?id_pelajaran='+id+'&nip_guru='+nip, 'CetakPerhitunganNilaiRapor','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
	else 
		alert ('Tidak ada data yang dapat dicetak');
}
</script>
</head>
<body topmargin="0" leftmargin="0">

<form name="tampil_aturannhb" action="perhitungan_rapor_content.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td background="../images/ico/b_aturannilai.png" style="background-attachment:fixed; background-repeat:no-repeat" valign="top">

<!-- TABLE TITLE -->
<table border="0" width="100%">
<tr>     
  <td align="right" valign="top"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Aturan Perhitungan Nilai Rapor</font></td>
</tr>
<tr>
  <td align="right" valign="top"><a onClick="javascript:top.content.location.href='../guru.php?page=p'" href="#">
<font size="1" color="#000000"><b>Guru & Pelajaran</b></font></a>&nbsp>&nbsp <font size="1" color="#000000"><b>Aturan Perhitungan Nilai Rapor</b></font></td>
</tr>
</table>
    
<br /><br /><br>
<table width="100%" border="0">   
<tr>
    <td width="20%" rowspan="4"></td>
    <td width="10%"><b>Departemen</b></td>
    <td><strong>: <?php echo $departemen ?></strong>
    <input type="hidden" name="departemen" id="departemen" value="<?php echo $departemen ?>">	
    </td>
    <td rowspan="2"></td>
</tr>
<tr>
<?php 	
$sql = "SELECT tingkat,idkurikulum, replid FROM tingkat WHERE departemen = '$departemen' AND aktif=1 ORDER BY urutan";
$result = QueryDb($sql);
if (mysql_num_rows($result) > 0)
{ 
?>	
 	<td align="right" colspan="2" valign="top"><a href="#" onClick="document.location.reload()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
    <a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp;  
   	</td>
</tr>
</table>

<?php 	
	$cnt = 0;  
    while ($row_tkt = @mysql_fetch_array($result)) 
	{
		++$cnt;
		$query_at = "SELECT a.dasarpenilaian, dp.keterangan
		               FROM aturannhb a, tingkat t, dasarpenilaian dp 
			 		  WHERE a.idtingkat='$row_tkt[replid]' AND t.departemen='$departemen' 
					    AND a.dasarpenilaian = dp.dasarpenilaian
  					    AND t.replid = a.idtingkat AND a.idkurikulum='$row_tkt[idkurikulum]' GROUP BY a.dasarpenilaian";
		$result_at = QueryDb($query_at);  
											?>
  		<br>
  		<fieldset>
        <legend>
        <b>Tingkat <?php echo $row_tkt[tingkat] ?> &nbsp;&nbsp;&nbsp;
<?php		if (@mysql_num_rows($result_at)>0)
		{ 
			$cetak = 1; ?>	
    		<a href = "JavaScript:tambah(<?php echo $row_tkt['replid'].",".$row_tkt['idkurikulum'];?>)">
            <img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')">&nbsp;Input Aturan Perhitungan Nilai Rapor</a>
		</b>
        </legend>
        <br />
   
	  	<!--<table border="1" width="100%" id="table<?php echo $cnt?>" class="tab">-->
    	<table class="tab" id="table<?php echo $cnt?>" border="1" style="border-collapse:collapse" width="100%" align="center">
	  	<tr>
			<td class="header" align="center" height="30" width="10%">No</td>
			<td class="header" align="center" height="30">Aspek Penilaian</td>
			<td class="header" align="center" height="30">Bobot Perhitungan Nilai Rapor </td>
    	    <td class="header" colspan="2" height="30">&nbsp;</td>
		</tr>
<?php		$i = 1;
		while($row_at = mysql_fetch_row($result_at))
		{	?>
		<tr height="25">
			<td align="center"><?php echo $i ?></td>
			<td><?php echo $row_at[1] ?></td>
			<td>
<?php			$query_ju = "SELECT j.jenisujian, a.bobot, a.aktif, a.replid FROM aturannhb a, tingkat t, jenisujian j ".
					 	"WHERE a.idtingkat = '$row_tkt[replid]' AND j.replid = a.idjenisujian ".
						"AND t.departemen = '$departemen' AND a.dasarpenilaian = '$row_at[0]'".
						"AND t.replid = a.idtingkat AND a.idkurikulum='$row_tkt[idkurikulum]'";
			$result_ju = QueryDb($query_ju);
			while($row_ju = mysql_fetch_row($result_ju))
			{
				if ($row_ju[2] == 1) 
				{ ?>
					<a href="JavaScript:setaktif(<?php echo $row_ju[3] ?>, <?php echo $row_ju[2] ?>)">
						<img src="../images/ico/aktif.png" border="0" onMouseOver="showhint('Status Aktif!', this, event, '50px')" /></a>&nbsp;
			        <?php echo $row_ju[0]." = ".$row_ju[1]."<br>";
				} 
				else 
				{ ?>
					<a href="JavaScript:setaktif(<?php echo $row_ju[3] ?>, <?php echo $row_ju[2] ?>)">
						<img src="../images/ico/nonaktif.png" border="0" onMouseOver="showhint('Status Tidak Aktif!', this, event, '50px')" /></a>&nbsp;
					<?php echo $row_ju[0]." = ".$row_ju[1]."<br>";
                } // if ($row_ju[2] == 1) 
			} // while($row_ju = mysql_fetch_row($result_ju))   ?>
			</td>
    	 	<td align="center" width="*"> 
            	<a href = "JavaScript:edit('<?php echo $row_tkt['replid']?>','<?php echo $row_at[0]?>','<?php echo $row_tkt['idkurikulum']?>')">
	            <img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah!', this, event, '50px')"></a>
<?php			if (SI_USER_LEVEL() != $SI_USER_STAFF) 
			{  ?>
            	<a href = "JavaScript:hapus('<?php echo $row_tkt['replid']?>','<?php echo $row_at[0]?>')">
            	<img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus!', this, event, '50px')" /></a>    
<?php 			} ?>
   			</td>
        </tr>
<?php			$i++;
		} // while($row_at = mysql_fetch_row($result_at)) 	?>
  	</table>
    <script language='JavaScript'>
          Tables('table<?php echo $cnt?>', 1, 0);
    </script>
    
<?php	
} 
else 
{ 
?>
	</legend>	
	<table width="100%" border="0" align="center">          
	<tr>
		<td align="center" valign="middle">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data.    
		<br />Klik <a href="JavaScript:tambah(<?php echo $row_tkt['replid'].",".$row_tkt['idkurikulum']?>)" ><font size = "2" color ="green">di sini</font></a> untuk mengisi data baru pada tingkat <?php echo $row_tkt['tingkat']?>. 
		</b></font>
		</td>
		</tr>
	</table>
<?php 
} 	
?> 
	<br>
  	</fieldset>
<?php }	?>
 	<input type="hidden" name="cetak" id="cetak" value="<?php echo $cetak ?>" />
    <!-- END TABLE CONTENT -->
 	</td>
  </tr>
</table>
<?php	} else { ?>
</td><td width = "50%"></td>
</tr>
<tr height="60"><td colspan="4"><hr style="border-style:dotted" /></td>
</tr>
</table>
<table width="100%" border="0" align="center">          
<tr>
	<td align="center" valign="middle" height="200">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data. <?php echo  $sql ?>
        <br />Tambah tingkat kelas pada departemen <?php echo $departemen?> di menu referensi
        </b></font>
	</td>
</tr>
</table>  
<?php } ?>
</td>
</tr>
</table>
</form>
</body>
</html>
<?php
CloseDb();
?>