<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$tingkat = $_REQUEST['tingkat'];
$semester = $_REQUEST['semester'];
$pelajaran = $_REQUEST['pelajaran'];

if (isset($_REQUEST['urut'])){
	$urut = $_REQUEST['urut'];
	} else {
	$urut = "koderpp";	
	}	

	
if (isset($_REQUEST['urutan'])){
	$urutan = $_REQUEST['urutan'];
	} else {
	$urutan = "ASC";
	}

$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];

OpenDb();
$sql = "SELECT t.departemen, t.tingkat, s.semester, p.nama FROM tingkat t, semester s, pelajaran p WHERE t.replid = '$tingkat' AND s.replid = '$semester' AND p.replid = '$pelajaran'";
//echo $sql;
$result = QueryDb($sql);
CloseDb();
$row = mysql_fetch_array($result);
$departemen = $row['departemen'];
$namatingkat = $row['tingkat'];
$namasemester = $row['semester'];
$namapelajaran = $row['nama'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Daftar Kompetensi Dasar]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center>
  <font size="4"><strong>DAFTAR KOMPETENSI DASAR </strong></font><br />
 </center><br /><br />

<br />
<table>
<tr>
	<td><strong>Departemen</strong> </td> 
	<td><strong>:&nbsp;<?php echo $departemen?></strong></td>
</tr>
<tr>
	<td><strong>Tingkat</strong></td>
	<td><strong>:&nbsp;<?php echo $namatingkat?></strong></td>
</tr>
<tr>
	<td><strong>Semester</strong></td>
	<td><strong>:&nbsp;<?php echo $namasemester?></strong></td>
</tr>
<tr>
	<td><strong>Pelajaran</strong></td>
	<td><strong>:&nbsp;<?php echo $namapelajaran?></strong></td>
</tr>

</table>
<br />
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="8%" class="header" align="center">No KD</td>
        <td width="25%" class="header" align="center">Kompetensi Inti</td>
        <td width="*" class="header" align="center">Uraian Kompetensi Dasar</td>
        <td width="10%" class="header" align="center">Status</td>        
        
    </tr>
<?php 	OpenDb();
	$sql = "SELECT replid, koderpp, rpp, deskripsi, aktif FROM rpp WHERE idtingkat='$tingkat' AND idsemester='$semester' AND idpelajaran='$pelajaran' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
	$result = QueryDb($sql);
	//if ($page==0)
		$cnt = 0;
	//else
		//$cnt = (int)$page*(int)$varbaris;
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">    	
    	<td align="center" ><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row['koderpp'] ?></td>        
        <td align="center"><?php echo $row['rpp'] ?></td>        
        <td><?php echo $row['deskripsi'] ?></td>
        <td align="center">
			<?php if ($row['aktif'] == 1) 
					echo 'Aktif';
				else
					echo 'Tidak Aktif';
			?>		
        </td>
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>
</td>
</tr>
<!-- END TABLE CENTER -->    
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>