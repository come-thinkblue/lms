<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../cek.php');
require_once('../library/dpupdate.php');
require_once('../library/departemen.php');

OpenDb();

$op = "";
if (isset($_REQUEST['op']))
	$op = $_REQUEST['op'];

if ($op == "xm8r389xemx23xb2378e23") 
{
	$sql = "DELETE FROM $g_db_akademik.dasarpenilaian WHERE replid = '$_REQUEST[replid]'";
	$result = QueryDb($sql);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tooltips.js"></script>
<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function refresh() {
	document.location.href = "resume.php";
}


function cetak() {
    var departemen = document.getElementById('departemen').value;
	newWindow('resume_cetak.php?departemen='+departemen, 'Cetakresume','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
}
</script>
</head>

<body>



<tr>

<table border="0" width="100%" align="center">
<!-- TABLE CENTER -->
<tr height="300">
  <td align="left" valign="top">

	<table border="0"width="95%" align="center">
    <tr>
        <td align="right"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Resume Nilai</font></td>
    </tr>
    <tr>
        <td align="right"><a href="../guru.php?page=p" target="content">
          <font size="1" face="Verdana" color="#000000"><b>Guru & Pelajaran</b></font></a>&nbsp>&nbsp <font size="1" face="Verdana" color="#000000"><b>Aspek Penilaian</b></font>
        </td>
    </tr>
     <tr>
      <td align="left">&nbsp;</td>
      </tr>
	</table>
	<br /><br />
    <table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
    <!-- TABLE CONTENT -->
    <tr>
    <td align="right" width="35%">
     <strong>Departemen</strong>&nbsp;
        <select name="departemen" id="departemen" onchange="tampil()">
          <?php $dep = getDepartemen(SI_USER_ACCESS());    
    foreach($dep as $value) {
        if ($departemen == "")
            $departemen = $value; ?>
          <option value="<?php echo $value ?>" <?php echo StringIsSelected($value, $departemen) ?> > 
            <?php echo $value ?> 
            </option>
          <?php } ?>
        </select>
      </td>
	<td align="right">
    
    <a href="#" onClick="refresh()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
    <a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')" />&nbsp;Cetak</a>&nbsp;&nbsp;    
    </td></tr>
    </table><br />
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="95%" align="center" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="13%" class="header" align="center">NIP</td>
        <td width="*" class="header" align="center">Nama Dosen</td>
        <td width="*" class="header" align="center">Mata Pelajaran</td>
        <td width="6%" class="header" align="center">UAS</td>
        <td width="6%" class="header" align="center">UTS</td>
        <td width="6%" class="header" align="center">Tugas</td>
        <td width="6%" class="header" align="center">Lisan</td>
        <td width="6%" class="header" align="center">Tulis</td>
        <td width="6%" class="header" align="center">PK1</td>
        <td width="6%" class="header" align="center">PK2</td>
    </tr>
<?php 	
    //tahun ajaran
    $sql_thn = "SELECT replid,tahunajaran FROM tahunajaran WHERE departemen='$departemen' AND aktif=1 ORDER BY replid DESC";
    $result_thn = QueryDb($sql_thn);
    $row_thn = @mysql_fetch_array($result_thn); 
    $tahunajaran = $row_thn['replid'];
    //info jadwal
    $sql_info = "SELECT replid FROM infojadwal WHERE idtahunajaran='$tahunajaran' AND aktif=1";
    $result_info = QueryDb($sql_info);
    $row_info = @mysql_fetch_array($result_info); 
    $infojadwal = $row_info['replid'];
    
    $sql = "SELECT DISTINCT j.nipguru, p.nama, peg.nama, j.idpelajaran 
            FROM jadwal j, pelajaran p, $g_db_pegawai.pegawai peg 
            WHERE j.idpelajaran=p.replid AND j.nipguru=peg.nip 
            AND j.infojadwal='$infojadwal'";
    $result = QueryDb($sql);
	$cnt = 0;
	while ($row = mysql_fetch_row($result)) { ?>
    <tr height="25">
    	<td align="center"><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row[0] ?></td>
        <td><?php echo $row[2] ?></td>
        <td><?php echo $row[1] ?></td>
        <?php
        
        $sql_kls = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls = QueryDb($sql_kls);
        $num = @mysql_num_rows($res_kls);      
        $awal=0;
        while ($row_kls = mysql_fetch_row($res_kls)) {      
                
                $sql_uji = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls[0]' AND idpelajaran=$row[3] AND idjenis='3'";
                $res_uji = QueryDb($sql_uji);
                $num_uji = @mysql_num_rows($res_uji);
                $awal = $awal+$num_uji;
            }  
        echo "<td align='center'>".$awal."/".$num."</td>";

        $sql_kls2 = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls2 = QueryDb($sql_kls2);
        $num2 = @mysql_num_rows($res_kls2);
        $awal2=0;
        while ($row_kls2 = mysql_fetch_row($res_kls2)) {      
                
                $sql_uji2 = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls2[0]' AND idpelajaran=$row[3] AND idjenis='5'";
                $res_uji2 = QueryDb($sql_uji2);
                $num_uji2 = @mysql_num_rows($res_uji2);
                $awal2 = $awal2+$num_uji2;
            }  
        echo "<td align='center'>".$awal2."/".$num2."</td>";

        $sql_kls3 = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls3 = QueryDb($sql_kls3);
        $num3 = @mysql_num_rows($res_kls3);
        $awal3 =0;
        while ($row_kls3 = mysql_fetch_row($res_kls3)) {      
                
                $sql_uji3 = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls3[0]' AND idpelajaran=$row[3] AND idjenis='8'";
                $res_uji3 = QueryDb($sql_uji3);
                $num_uji3 = @mysql_num_rows($res_uji3);
                $awal3 = $awal3+$num_uji3;
            }  
        echo "<td align='center'>".$awal3."/".$num3."</td>";

        $sql_kls4 = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls4 = QueryDb($sql_kls4);
        $num4 = @mysql_num_rows($res_kls4);
        $awal4=0;
        while ($row_kls4 = mysql_fetch_row($res_kls4)) {      
                
                $sql_uji4 = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls4[0]' AND idpelajaran=$row[3] AND idjenis='12'";
                $res_uji4 = QueryDb($sql_uji4);
                $num_uji4 = @mysql_num_rows($res_uji4);
                $awal4 = $awal4+$num_uji4;
            }  
        echo "<td align='center'>".$awal4."/".$num4."</td>";

        $sql_kls5 = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls5 = QueryDb($sql_kls5);
        $num5 = @mysql_num_rows($res_kls5);
        $awal5=0;
        while ($row_kls5 = mysql_fetch_row($res_kls5)) {      
                
                $sql_uji5 = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls5[0]' AND idpelajaran=$row[3] AND idjenis='13'";
                $res_uji5 = QueryDb($sql_uji5);
                $num_uji5 = @mysql_num_rows($res_uji5);
                $awal5 = $awal5+$num_uji5;
            }  
        echo "<td align='center'>".$awal5."/".$num5."</td>";

        $sql_kls6 = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls6 = QueryDb($sql_kls6);
        $num6 = @mysql_num_rows($res_kls6);
        $awal6=0;
        while ($row_kls6 = mysql_fetch_row($res_kls6)) {      
                
                $sql_uji6 = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls6[0]' AND idpelajaran=$row[3] AND idjenis='10'";
                $res_uji6 = QueryDb($sql_uji6);
                $num_uji6 = @mysql_num_rows($res_uji6);
                $awal6 = $awal6+$num_uji6;
            }  
        echo "<td align='center'>".$awal6."/".$num6."</td>";

        $sql_kls7 = "SELECT idkelas FROM jadwal 
                    WHERE infojadwal='$infojadwal' 
                    AND nipguru='$row[0]' AND idpelajaran='$row[3]'";
        $res_kls7 = QueryDb($sql_kls7);
        $num7 = @mysql_num_rows($res_kls7);
        $awal7=0;
        while ($row_kls7 = mysql_fetch_row($res_kls7)) {      
                
                $sql_uji7 = "SELECT DISTINCT idjenis FROM ujian 
                        WHERE idkelas='$row_kls7[0]' AND idpelajaran=$row[3] AND idjenis='11'";
                $res_uji7 = QueryDb($sql_uji7);
                $num_uji7 = @mysql_num_rows($res_uji7);
                $awal7 = $awal7+$num_uji7;
            }  
        echo "<td align='center'>".$awal7."/".$num7."</td>";
        ?>
    </tr>
<?php	} ?>
    <!-- END TABLE CONTENT -->
    </table>
    <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>

	</td></tr>
<!-- END TABLE CENTER -->    
</table>  

</body>
</html>
<?php CloseDb();?>