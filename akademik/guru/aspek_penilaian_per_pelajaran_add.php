<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS MAN Kota Blitar
 * 
 * Copyright (C) 2013 SMAN 1 Malang
 * 
 **[N]**/ 
?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');
require_once('../library/dpupdate.php');

$id=$_REQUEST['id'];

if (isset($_REQUEST['aspek']))
	$aspek = $_REQUEST['aspek'];	

if (isset($_REQUEST['jum']))
	$jum = $_REQUEST['jum'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>SISTEM AKADEMIK [Tambah Aspek Penilaian]</title>
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextfield.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextfield.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function validate() {
				var isi = 0;
				var jum = document.getElementById('jum').value;	
				for (i=1;i<=jum;i++) {					
					var cek = document.getElementById('cek'+i).checked;	
					var ujian = document.getElementById('ujian'+i).value;		
					var bobot = document.getElementById('bobot'+i).value;
					if (cek == 1) {
						isi = 1;
						if (bobot.length > 0){
							if (isNaN(bobot)){
								alert("Bobot nilai harus berupa bilangan");
								document.getElementById('bobot'+i).focus();				
								return false;									
							} 
						} else {
							alert ("Anda harus mengisikan data untuk bobot nilai"); 
							document.getElementById('bobot'+i).focus();				
							return false;
						} 
					}
					
					if (bobot.length > 0) {
						if (cek != 1) {
							alert ("Anda harus memberi centang terlebih dahulu"); 
							document.getElementById('cek'+i).focus();				
							return false;
						}
					}
							
				}
				if (isi == 0) {
					alert ("Anda harus mengisi setidaknya satu data untuk bobot nilai rapor");
					document.getElementById('bobot1').focus;
					return false; 
				}
				document.getElementById('main').submit(); 
			}

		function simpan(evt) {
			evt = (evt) ? evt : event;
			var charCode = (evt.charCode) ? evt.charCode :
				((evt.which) ? evt.which : evt.keyCode);
			if (charCode == 13) {
				document.getElementById('main').submit();
				return false;
			}
			return true;
		}
		</script>
	</head>

	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('aspek').focus();">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
				<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
				.: Tambah Aspek Penilaian :.
				</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="150">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<form name="main" id="main" action="aspek_penilaian_per_pelajaran_simpan.php" method="POST">
						<input type="hidden" name="action" id="action" value="Add" />
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<tr>
								<?php
								OpenDb();
								$sql_max = "SELECT max(replid) FROM aturannhb";
								$rs_max = QueryDb($sql_max);
								$row_max = @mysql_fetch_row($rs_max);
								CloseDb();
								?>
								<td width="238"><strong>Id Aspek Penilaian</strong></td>
								<td width="615"><input type="text" name="id_aspek" id="id_aspek" size="5" maxlength="10" value="<?php echo $row_max[0]+1 ?>"  class="disabled" readonly /></td>
							</tr>
							<tr>
								<?php
								OpenDb();
								$sql_namapel = "SELECT nama FROM pelajaran WHERE replid=$id";
								$rs_namapel = QueryDb($sql_namapel);
								$row_namapel = @mysql_fetch_row($rs_namapel);
								CloseDb();
								?>
								<td><strong>Pelajaran</strong></td>
								<td>
									<input type="text" name="pelajaran" id="pelajaran" size="30" maxlength="50" value="<?php echo $row_namapel[0] ?>" class="disabled" readonly />
									<input type="hidden" name="id_pelajaran" id="id_pelajaran" value="<?php echo $id?>" />
								</td>
							</tr>
							<tr>
								<td><strong>Aspek Penilaian</strong></td>
								<td>
									<select name="aspek" id="aspek" onKeyPress="focusNext('cek1',event)">
										<?php
										OpenDb();
										$sql_aspek = "SELECT dasarpenilaian, keterangan 
												FROM dasarpenilaian 
												WHERE dasarpenilaian 
												NOT IN (SELECT DISTINCT dasarpenilaian FROM aturannhb WHERE idpelajaran=$id)";
										
										$result_aspek = QueryDb($sql_aspek);
										CloseDb();
										while ($row_aspek = @mysql_fetch_array($result_aspek)){
										if ($aspek == "")
											$aspek = $row_aspek['dasarpenilaian'];
											?>
												<option value="<?php echo $row_aspek['dasarpenilaian']?>" <?php echo StringIsSelected($row_aspek['dasarpenilaian'], $aspek_aspek)?> >
													<?php echo $row_aspek['keterangan'] ?>
												</option>
											<?php	
										} 
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan = "2" valign="top">
									<fieldset>
										<legend><b>Bobot Penilaian</b></legend>
										<br />
										<?php
										OpenDb();
										$sql = "SELECT replid, jenisujian FROM jenisujian"; 
										$result = QueryDb($sql);
										CloseDb();
										$num = mysql_num_rows($result);
										if (mysql_num_rows($result) > 0){
										?>
										<table border="0" width="100%"  id="table" class="tab">
											<tr>		
												<td class="header" align="center" width="3%" height="30"></td>
												<td class="header" align="center" width="3%" height="30">No</td>
												<td class="header" align="center" width="8%" height="30">Pengujian</td>			
												<td class="header" align="center" width="15%" height="30">Bobot</td>
											</tr>
											<?php
											$i = 1;
											$aBobot= array("15","15","30","40");
											while ($row = @mysql_fetch_array($result)){
												?>		
												<tr>
													<td align="center" height="25">
														<input type="checkbox" name="<?php echo 'cek'.$i ?>" id="<?php echo 'cek'.$i ?>" checked="checked" value = "1" onKeyPress="focusNext('bobot<?php echo $i?>',event)">
														<input type="hidden" name="jum" id="jum" value="<?php echo $num ?>" />
													</td>
													<td align="center" height="25"><?php echo $i ?>
														<input type="hidden" name="<?php echo 'ujian'.$i?>" id = "<?php echo 'ujian'.$i?>" value="<?php echo $row['replid'] ?>">
													</td>
													<td height="25"><?php echo $row['jenisujian'] ?></td>
													<td align="center" height="25">
														<input type="text"  value="<?php echo $aBobot[$i-1] ?>" name="<?php echo 'bobot'.$i ?>" id="<?php echo 'bobot'.$i ?>"  size="4" maxlength="3" <?php if ($i!=$num) { ?> onKeyPress="focusNext('cek<?php echo (int)$i+1?>',event)" <?php } else { ?> onkeypress="focusNext('Simpan',event)" <?php } ?> >
													</td>
												</tr>
												<?php
												$i++;	
											}	
										}
										?>
										</table>
									</fieldset>
								</td>
							</tr>
							<tr>
								<td colspan="2" height="25" width="100%" align="left" valign="top" style="border-width:1px; border-style:dashed; border-color:#03F; background-color:#CFF">
									<strong>Pilih aspek penilaian.<br>Centang jenis pengujian yang sesuai dengan aspek penilaian yang dipilih.<br>Kemudian berikan bobot nilainya.<br/>
									<font color="#FF0000">Contoh yang salah: Praktek-UTS-25, Pemahaman Konsep-UTS-25 </font><br />
									<font color="Blue">Contoh yang benar: Praktek-UTS Praktek-25, Pemahaman Konsep-UTS Pemahaman Konsep-25</font></strong>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
								<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onClick="return validate();document.getElementById('main').submit();" />&nbsp;
								<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<tr>
							  <td colspan="2" align="center">&nbsp;</td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>
					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<script type="text/javascript">
<!--
	var spryselect1 = new Spry.Widget.ValidationSelect("aspek");
	var jum=document.getElementById("jum").value;
	var x=1;
	while (x<=jum){
		var sprytextfield = new Spry.Widget.ValidationTextField("bobot"+x);
		x++;
	}
//-->
</script>