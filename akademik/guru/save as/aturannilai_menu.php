<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aturan Perhitungan Grading Nilai-Menu</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function refresh() {	
	document.location.reload();
}

function tampil(id,nip) {	
	parent.aturan_nilai_content.location.href="aturannilai_content.php?id="+id+"&nip="+nip;
}

</script>
</head>
<body topmargin="0">
	<?php
	OpenDb();
	$sql = "SELECT d.departemen,pg.nama FROM guru g, pelajaran p, departemen d,$g_db_pegawai.pegawai pg WHERE g.idpelajaran = p.replid AND d.departemen = p.departemen AND g.nip ='$_REQUEST[nip]' AND pg.nip=g.nip GROUP BY d.departemen ORDER BY d.urutan";	
	
	$result = QueryDb($sql);
	if (@mysql_num_rows($result)>0){
		echo "<div align='center'><strong>Pelajaran yang diajar oleh guru ".$_REQUEST['nama']."</strong><br></div>";
		$count = 0;
		while ($row = @mysql_fetch_row($result)) {				
		$count++;
	?>
   
<table border="0" width="100%" align="center">
<!-- TABLE CENTER -->
  	<tr><td valign="top">
      	
	<table class="tab" id="table<?php echo $count?>" border="1" style="border-collapse:collapse" width="100%" align="left">
    	<!-- TABLE CONTENT -->
    
    <tr height="30">    	
    	<td width="100%" class="header" align="center"><?php echo $row[0];?></td>
    </tr>
    <?php		
		$sql1 = "SELECT p.nama,p.replid FROM guru g, pelajaran p WHERE g.idpelajaran = p.replid AND g.nip='$_REQUEST[nip]' AND p.departemen = '$row[0]' GROUP BY p.nama";	
		$result1 = QueryDb($sql1); 				
		while ($row1 = @mysql_fetch_array($result1)) {
	?>
    <tr>   	
       	<td align="center" height="25" onclick="tampil('<?php echo $row1[1]?>','<?php echo $_REQUEST['nip']?>')" style="cursor:pointer">
        <u><b><?php echo $row1[0]?></b></u>
		</td>
    </tr>
    <!-- END TABLE CONTENT -->
    <?php 		} ?>
	</table>
		 <script language='JavaScript'>
	    Tables('table<?php echo $count?>', 1, 0);
    </script>
    </td></tr>
<!-- END TABLE CENTER -->    
</table> 
<?php	
		}
	CloseDb(); 
	} else { 
?>
<table width="100%" border="0" align="center">          
<tr>
    <td align="center" valign="middle" height="200">
    <font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br /><br />Tambah data pelajaran yang akan diajar oleh guru <?php echo $_REQUEST['nama']?> di menu Pendataan Guru pada bagian Guru & Pelajaran. </b></font>
    </td>
</tr>
</table> 
<?php } ?> 


   

</body>
</html>