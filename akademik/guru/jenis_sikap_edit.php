<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS SMAN 1 Malang
 * 
 * Copyright (C) 2013 SMAN 1 Malang
 * 
 **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

$replid = $_REQUEST['replid'];


$ERROR_MSG = "";
if (isset($_REQUEST['Edit'])) {
	OpenDb();
	$sql = "UPDATE master_sikap SET nama_sikap='".CQ($_REQUEST['master_sikapbaru'])."',keterangan='".CQ($_REQUEST['keterangan'])."',iddasarpenilaian='".CQ($_REQUEST['aspek'])."', info1='".CQ($_REQUEST['predikat'])."'  WHERE replid='$_REQUEST[replid]'";
	$result = QueryDb($sql);
	CloseDb();
	
	if ($result) { ?>
		<script language="javascript">
			opener.refresh();
			window.close();
		</script> 
<?php		
	}
}

	OpenDb();

	$sql = "SELECT * FROM master_sikap WHERE replid=$replid";   
	$result = QueryDb($sql);
	$row = mysql_fetch_row($result);
	$master_sikap = $row[1];
	if (isset($_REQUEST['master_sikap']))
		$master_sikap = $_REQUEST['master_sikap'];
	$keterangan = $row[4];
	if (isset($_REQUEST['keterangan']))
		$keterangan = $_REQUEST['keterangan'];
	$predikat = $row[5];
	if (isset($_REQUEST['predikat']))
		$predikat = $_REQUEST['predikat'];
	$aspek = $row[3];
	if (isset($_REQUEST['aspek']))
		$aspek = $_REQUEST['aspek'];

	CloseDb();

?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<link rel="stylesheet" type="text/css" href="../style/style.css">
			<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>SISTEM AKADEMIK[Ubah Jenis Pengujian]</title>
			<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
			<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
			<script src="../script/SpryValidationTextfield.js" type="text/javascript"></script>
			<link href="../script/SpryValidationTextfield.css" rel="stylesheet" type="text/css" />
			<script language="JavaScript" src="../script/tooltips.js"></script>
			<script language="javascript" src="../script/tables.js"></script>
			<script language="javascript" src="../script/tools.js"></script>
			<script language="javascript" src="../script/validasi.js"></script>
			<script language="javascript">
				function validate() {
					return validateEmptyText('master_sikapbaru', 'Jenis Pengujian') && 
						   validateEmptyText('predikat', 'Predikat') && 
						   validateMaxText('keterangan', 255, 'Keterangan');
				}

				function focusNext(elemName, evt) {
					evt = (evt) ? evt : event;
					var charCode = (evt.charCode) ? evt.charCode :
						((evt.which) ? evt.which : evt.keyCode);
					if (charCode == 13) {
						document.getElementById(elemName).focus();
						return false;
					} 
					return true;
				}
			</script>
		</head>

		<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('master_sikapbaru').focus();">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr height="58">
					<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
					<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
					.: Ubah Jenis Pengujian :.
					</div>
					</td>
					<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
				</tr>
				<tr height="150">
					<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
					<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //-->
						<form name="main" onSubmit="return validate()">
							<input type="hidden" name="replid" id="replid" value="<?php echo $replid ?>" />
							<input type="hidden" name="idpelajaran" id="idpelajaran" value="<?php echo $idpelajaran ?>" />
							<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
								<tr>
								<!-- TABLE CONTENT -->
									<td><strong>Id Deskripsi Sikap</strong></td>
									<td><input type="text" name="replid" id="replid" size="30" maxlength="50" value="<?php echo $replid ?>" class="disabled" readonly /></td>
								</tr>
								<tr>
									<td><strong>Nama Dskripsi Sikap</strong></td>
									<td>
										<input type="text" name="master_sikapbaru" id="master_sikapbaru" size="30" maxlength="50" value="<?php echo $master_sikap ?>" onFocus="showhint('Nama jenis pengujian tidak boleh lebih dari 50 karakter!', this, event, '120px')" onKeyPress="return focusNext('keterangan', event)" /> <input type="hidden" name="master_sikap" id="master_sikap" size="30" maxlength="50" value="<?php echo $master_sikap ?>" />   </td>
								</tr>
								<tr>
						<td><strong>Aspek</strong></td>
						<td>
							<select name="aspek">
							<?php 
								OpenDb();
								$sql = "SELECT dasarpenilaian, keterangan FROM dasarpenilaian WHERE idkurikulum='$row[2]'";
								$res = QueryDb($sql);
								while ($rows = @mysql_fetch_row($res)){
									if($aspek==$rows[0])
										echo "<option value='$rows[0]' selected='selected'>$rows[1]</option>";
									else
										echo "<option value='$rows[0]'>$rows[1]</option>";
								}
								CloseDb();
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td><strong>Predikat</strong></td>
						<td>
							<select name="predikat">
							<?php
								if($predikat=='A'){
									echo 
									"<option selected='select' value='A'>A</option>
									<option value='B'>B</option>
									<option value='C'>C</option>
									<option value='D'>D</option>";
								}else if($predikat=='B'){
									echo 
									"<option value='A'>A</option>
									<option selected='select' value='B'>B</option>
									<option value='C'>C</option>
									<option value='D'>D</option>";
								}else if($predikat=='C'){
									echo 
									"<option value='A'>A</option>
									<option value='B'>B</option>
									<option selected='select' value='C'>C</option>
									<option value='D'>D</option>";
								}else{
									echo 
									"<option value='A'>A</option>
									<option value='B'>B</option>
									<option value='C'>C</option>
									<option selected='select' value='D'>D</option>";
								}
							?>
							</select>
						</td>
					</tr>
								<tr>
									<td valign="top">Deskripsi</td>
									<td>
										<textarea name="keterangan" id="keterangan" rows="3" cols="45" onKeyPress="return focusNext('Edit', event)"><?php echo $keterangan ?></textarea>    </td>
								</tr>
								<tr>
									<td colspan="2" height="25" width="100%" align="left" valign="top" style="border-width:1px; border-style:dashed; border-color:#03F; background-color:#CFF">				      
										<strong>Anda hanya perlu mengisikan nama jenis pengujian dan singkatannya. Penamaan jenis pengujian juga ditambahkan dengan kriteria/aspek penilaiannya. </strong><br />
										<font color="#FF0000">Contoh yang salah : UTS 2010/2011 Semester 1 ke-1 </font><br />
										<font color="Blue">Contoh yang benar : UAS Praktek, UAS Pemahaman Konsep</font></strong>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
									<input type="submit" name="Edit" id="Edit" value="Edit" class="but" />&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
								</tr>
							<!-- END OF TABLE CONTENT -->
							</table>
						</form>
				 <!-- END OF CONTENT //--->
					</td>
					<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
				</tr>
				<tr height="28">
					<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
					<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
					<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
				</tr>
			</table>
			<!-- Tamplikan error jika ada -->
			<?php if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">
					alert('<?php echo $ERROR_MSG?>');
				</script>
			<?php } ?>
		</body>
	</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("master_sikapbaru");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("keterangan");
</script>