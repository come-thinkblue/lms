<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS SMAN 1 Malang
 * 
 * Copyright (C) 2013 SMAN 1 Malang
 * 
 **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');
	$kurikulum = 2;
	if (isset($_REQUEST['kurikulum']))
		$kurikulum = $_REQUEST['kurikulum'];
	$op = $_REQUEST['op'];
	if ($op == "hapus") {
	$replid=$_REQUEST['replid'];
		OpenDb();
		$sql = "DELETE FROM master_sikap WHERE replid = '$replid'";
		QueryDb($sql);
		$result=QueryDb($sql);
		if ($result) { 
			CloseDb();
		}	
	}	
	OpenDb();
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<link rel="stylesheet" type="text/css" href="../style/style.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Status Guru</title>
	<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
	<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
	<script language="javascript" src="../script/tooltips.js"></script>
	<script language="javascript" src="../script/tables.js"></script>
	<script language="javascript" src="../script/tools.js"></script>
	<script language="javascript">
	function tambah(kurikulum) {
		newWindow('jenis_sikap_add.php?kurikulum='+kurikulum, 'TambahJenisPengujian','550','400','resizable=1,scrollbars=1,status=0,toolbar=0')
	}
	
	function change() {
		var kurikulum = document.getElementById('kurikulum').value;
		document.location.href = "jenis_sikap_content.php?kurikulum="+kurikulum;
	}

	function refresh() {
		var kurikulum = document.getElementById('kurikulum').value;
		document.location.href = "jenis_sikap_content.php?kurikulum="+kurikulum;
	}

	function edit(replid) {
		newWindow('jenis_sikap_edit.php?replid='+replid, 'UbahJenisPengujian','550','400','resizable=1,scrollbars=1,status=1,toolbar=0')
	}

	function hapus(replid) {
		//var departemen = document.getElementById('departemen').value;
		if (confirm("Apakah anda yakin akan menghapus deskripsi sikap ini?"))
			document.location.href = "jenis_sikap_content.php?op=hapus&replid="+replid;
	}

	function cetak(kurikulum) {
		newWindow('jenis_sikap_cetak.php?kurikulum='+kurikulum, 'CetakSemester','790','650','resizable=1,scrollbars=1,status=1,toolbar=0')
	}
	</script>
	</head>
	<?php //echo $data->dump(true,true); 
	if ( !isset($_POST['oto']) )
		$_POST['oto'] = 0;
		
	?>
	<body topmargin="0" leftmargin="0">
		<table border="0" width="80%" align="center">
			<!-- TABLE CENTER -->
			<tr height="300">
				<td align="left" valign="top" background="../images/ico/b_jenisujian.png" style="background-repeat:no-repeat" />
				<table width="100%" border="0">
					<tr>
						<td>
							<table border="0"width="100%">
							<!-- TABLE TITLE -->
								<tr>     
									<td align="right"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Master Sikap</font></td>
								</tr>

								<tr>
									<td align="right">
										<a href="../guru.php?page=p" target="content"><font size="1" color="#000000"><b>Guru & Pelajaran</b></font></a>&nbsp>&nbsp 
										<font size="1" color="#000000"><b>Master Sikap</b></font>
									</td>
								</tr>    
							</table>
							<br/><br/>
						</td>
					</tr>
				</table>
				<?php  OpenDb();
				//$sql = "SELECT j.replid,j.jenisujian,j.idpelajaran,j.keterangan,p.replid,p.nama,p.departemen,j.info1 FROM jenisujian j, pelajaran p WHERE j.idpelajaran = '$id' AND j.idpelajaran = p.replid ORDER BY j.jenisujian";   
				$sql = "SELECT * FROM master_sikap WHERE idkurikulum='$kurikulum' ORDER BY iddasarpenilaian";
				$result = QueryDb($sql); 
				if (@mysql_num_rows($result) > 0){ 
				?>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
				<!-- TABLE LINK -->
					<tr>
						<td align="left">Kurikulum 
							<select name='kurikulum' id='kurikulum' onChange='change()'>
							<?php 
								$select = "SELECT replid, kurikulum FROM kurikulum";
								$hasil = QueryDb($select);
								while ($row = mysql_fetch_row($hasil)){
									if($kurikulum==$row[0])
										echo "<option value='$row[0]' selected='selected'>$row[1]</option>";
									else
										echo "<option value='$row[0]'>$row[1]</option>";
								}
							?>
							</select>
						</td>
						<td align="right">
							<a href="JavaScript:refresh()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
							<a href="JavaScript:cetak(<?php echo $kurikulum;?>)"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')" />&nbsp;Cetak</a>&nbsp;&nbsp;
							<a href="JavaScript:tambah(<?php echo $kurikulum;?>)"><img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')"/>&nbsp;Tambah Master Sikap</a>
						</td>
					</tr>
				</table>
				<br />
				<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">
				<!-- TABLE CONTENT -->
					<tr height="30">
						<td width="4%" class="header" align="center">No</td>
						<td width="20%" class="header" align="center">Aspek</td>
						<td width="20%" class="header" align="center">Jenis Pengujian</td>
						<td width="8%" class="header" align="center">Predikat</td>
						<td width="*" class="header" align="center">Deskripsi</td>
						<td width="8%" class="header" align="center">&nbsp;</td>
					</tr>
				<?php
				$cnt = 0;
				while ($row = @mysql_fetch_row($result)) {
					?>
					<tr height="25">   	
						<td align="center"><?php echo ++$cnt ?></td>
						<td align="center">
							<?php 
								$select = "SELECT keterangan FROM dasarpenilaian WHERE dasarpenilaian='$row[3]'";
								$hasil = QueryDb($select);
								$aspek = mysql_fetch_row($hasil);
								echo $aspek[0];
							?>
						</td>
						<td align="center"><?php echo $row[1]?></td>
						<td align="center"><?php echo $row[5]?></td>
						<td><?php echo $row[4]?></td>        
						<td align="center">
							<a href="JavaScript:edit(<?php echo $row[0] ?>)"><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Jenis Pengujian!', this, event, '80px')" /></a>&nbsp;
							<a href="JavaScript:hapus(<?php echo $row[0] ?>)"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Jenis Pengujian!', this, event, '80px')" /></a>
						</td>
					</tr>
					<?php
				}
				CloseDb();?>	
				<!-- END TABLE CONTENT -->
				</table>
				<script language='JavaScript'>
					Tables('table', 1, 0);
				</script>
				<?php
				} 
				else { 
					?>
					<table width="100%" border="0" align="center">          
						<tr>
							<td align="center" valign="middle" height="200">
								<font size = "2" color ="red">
									<b>Tidak ditemukan adanya data.
									<br />Klik &nbsp;<a href="JavaScript:tambah()" ><font size = "2" color ="green">di sini</font></a>&nbsp;untuk mengisi jenis pengujian. </b>
								</font>
							</td>
						</tr>
					</table>  
					<?php 
				} 
				?> 
				</td>
			</tr>
		<!-- END TABLE CENTER -->    
		</table>
	</body>
</html>
<script language="javascript">
	var spryselect1 = new Spry.Widget.ValidationSelect("kurikulum");
</script>