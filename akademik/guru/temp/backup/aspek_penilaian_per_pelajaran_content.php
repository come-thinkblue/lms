<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS MAN Kota Blitar
 * 
 * Copyright (C) 2013 SMAN 1 Malang
 * 
 **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');

	 
	$id = $_REQUEST['id'];
	
	$op = $_REQUEST['op'];
	if ($op == "hapus") {
		$idpel=$_REQUEST['idpelajaran'];
		$aspek=$_REQUEST['aspek'];
				
		OpenDb();
		$sql = "DELETE FROM aspek_penilaian_per_pelajaran WHERE idpelajaran = '$idpel' AND aspek_penilaian='$aspek'";
		//echo "$sql";
		QueryDb($sql);
		$result=QueryDb($sql);
		if ($result) { 
			CloseDb();
			?>
			<script language="javascript">
				parent.aspek_penilaian_per_pelajaran_content.location.href = "aspek_penilaian_per_pelajaran_content.php?id=<?php echo $idpel?>";
			</script> 
			<?php
		}		
	}	
	OpenDb();
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<link rel="stylesheet" type="text/css" href="../style/style.css">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Status Guru</title>
			<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
			<script language="javascript" src="../script/tooltips.js"></script>
			<script language="javascript" src="../script/tables.js"></script>
			<script language="javascript" src="../script/tools.js"></script>
			<script language="javascript">
				function tambah() {
					newWindow('aspek_penilaian_per_pelajaran_add.php?id=<?php echo $id?>', 'TambahJennisPengujian','550','400','resizable=1,scrollbars=1,status=0,toolbar=0')
				}

				function refresh() {	
					document.location.reload();
				}

				function edit(aspek) {
					newWindow('aspek_penilaian_per_pelajaran_edit.php?id=<?php echo $id?>&aspek='+aspek, 'UbahJenisPengujian','550','400','resizable=1,scrollbars=1,status=1,toolbar=0')
				}

				function hapus(aspek) {
					//alert('id=<?php echo $id?>');
					if (confirm("Apakah anda yakin akan menghapus jenis pengujian ini?"))
						document.location.href = "aspek_penilaian_per_pelajaran_content.php?op=hapus&idpelajaran=<?php echo $id?>&aspek="+aspek;
				}

				function cetak(id) {
					newWindow('aspek_penilaian_per_pelajaran_cetak.php?id='+id, 'CetakSemester','790','650','resizable=1,scrollbars=1,status=1,toolbar=0')
				}
			</script>
		</head>
		<?php //echo $data->dump(true,true); 
		if ( !isset($_POST['oto']) )
			$_POST['oto'] = 0;
		?>
		<body topmargin="0" leftmargin="0">
			<table border="0" width="80%" align="center">
				<!-- TABLE CENTER -->
				<input type="hidden" name="id" id="id" value="<?php echo $id?>">
				
				<tr height="300">
					<td align="left" valign="top" background="../images/ico/b_jenisujian.png" style="background-repeat:no-repeat">
						<table width="100%" border="0">
							<tr>
								<td>
									<table border="0"width="100%">
										<!-- TABLE TITLE -->
										<tr>     
											<td align="right">
												<font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;
												<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Jenis Pengujian</font>
											</td>
										</tr>
										<tr>
											<td align="right"><a href="../guru.php?page=p" target="content">
												<font size="1" color="#000000"><b>Guru & Pelajaran</b></font></a>&nbsp>&nbsp 
												<font size="1" color="#000000"><b>JenisPengujian</b></font>
											</td>
										</tr>    
									</table><br /><br />
								</td>
							</tr>
						</table>
					<?php  OpenDb();
						$sql = "SELECT DISTINCT a.aspek_penilaian, b.keterangan 
								FROM aspek_penilaian_per_pelajaran a, dasarpenilaian b
								WHERE a.aspek_penilaian=b.dasarpenilaian AND a.idpelajaran='$id' ORDER BY a.id_aspek_penilaian_per_pelajaran";   
						$result = QueryDb($sql); 
						if (@mysql_num_rows($result) > 0){ 
							?>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
							<!-- TABLE LINK -->
							<tr>
								<td align="right">
									<a href="#" onclick="document.location.reload()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
									<a href="JavaScript:cetak(<?php echo $id?>)"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')" />&nbsp;Cetak</a>&nbsp;&nbsp;
									<a href="JavaScript:tambah()"><img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')"/>&nbsp;Tambah Jenis Pengujian</a>
								</td>
							</tr>
							</table>
							<br />
							<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">
								<!-- TABLE CONTENT -->
								<tr height="30">
									<td width="4%" class="header" align="center">No</td>
									<td width="30%" class="header" align="center">Aspek Penilaian</td>
									<td width="*%" class="header" align="center">Bobot Penilaian</td>
									<td width="10%" class="header" align="center">&nbsp;</td>
								</tr>
							
								<?php
								$cnt = 0;
								while ($row = @mysql_fetch_row($result)) {
								?>
								<tr height="25">   	
									<td align="center"><?php echo ++$cnt ?></td>
									<td align="left"><?php echo $row[1]?></td>
									<td align="left">
									<?php
									$sql_bobot = "SELECT b.jenisujian, a.bobot
												  FROM aspek_penilaian_per_pelajaran a, jenisujian b 
												  WHERE a.id_jenisujian=b.replid AND a.idpelajaran='$id' AND a.aspek_penilaian='$row[0]'";
									$rs_bobot = QueryDb($sql_bobot);
									while($row_bobot = @mysql_fetch_row($rs_bobot)){
										?><img src="../images/ico/aktif.png" border="0"/> <?php echo $row_bobot[0]?> = <?php echo $row_bobot[1]?> <br /><?php
									}
									?>									
									</td>
									<td align="center">
										<a href="JavaScript:edit('<?php echo $row[0] ?>')"><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Jenis Pengujian!', this, event, '80px')" /></a>&nbsp;
										<?php		
										if (SI_USER_LEVEL() != $SI_USER_STAFF) {  
										?>
											<a href="JavaScript:hapus('<?php echo $row[0]?>')"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Jenis Pengujian!', this, event, '80px')" /></a>
										<?php 			
										} 
										?>
									</td>
								</tr>
								<?php	
								} 
								CloseDb(); ?>	
								<!-- END TABLE CONTENT -->
							</table>
							<script language='JavaScript'>
								Tables('table', 1, 0);
							</script>
						<?php	
						} else { 
						?>
							<table width="100%" border="0" align="center">          
								<tr>
									<td align="center" valign="middle" height="200">
										<font size = "2" color ="red">
										<b>Tidak ditemukan adanya data.
										<br />Klik &nbsp;<a href="JavaScript:tambah()" ><font size = "2" color ="green">di sini</font></a>&nbsp;untuk mengisi jenis pengujian untuk pelajaran <?php echo $nama_pel?>. 
										</b></font>
										</form>
									</td>
								</tr>
							</table>  
						<?php 
						} 
						?>
					</td>
				</tr>
			<!-- END TABLE CENTER -->    
			</table>
		</body>
	</html>