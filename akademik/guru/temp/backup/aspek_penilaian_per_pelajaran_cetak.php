<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS MAN Kota Blitar
 * 
 * Copyright (C) 2013 SMAN 1 Malang
 * 
 **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../include/getheader.php');

	$departemen="MAN Kota Blitar";
	$id = $_REQUEST['id'];
	$aspek = $_REQUEST['aspek'];

	OpenDb();
	$sql_title = "SELECT nama FROM pelajaran WHERE replid='$id'";
	$rs_title = QueryDb($sql_title);
	$row_title = @mysql_fetch_row($rs_title);
		
	$sql = "SELECT DISTINCT a.aspek_penilaian, b.keterangan, c.nama 
			FROM aspek_penilaian_per_pelajaran a, dasarpenilaian b, pelajaran c
			WHERE a.aspek_penilaian=b.dasarpenilaian AND a.idpelajaran=c.replid AND a.idpelajaran='$id' ORDER BY a.id_aspek_penilaian_per_pelajaran";
	//echo"$sql";
	$result = QueryDb($sql);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" href="../style/style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>SISTEM AKADEMIK [Cetak Aspek Penilaian Per Pelajaran]</title>
	</head>

	<body>
		<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
			<tr>
				<td align="left" valign="top">
					<?php echo getHeader($departemen)?>
					<center>
						<font size="3"><strong>ASPEK PENILAIAN PER PELAJARAN <?php echo $row_title[0]?></strong></font><br />
					</center>
					<br /><br />
					<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
						<!-- TABLE CONTENT -->
						<tr height="30">
							<td width="4%" class="header" align="center">No</td>
							<td width="30%" class="header" align="center">Aspek Penilaian</td>
							<td width="40%" class="header" align="center">Bobot Penilaian</td>
						</tr>
						
						<?php
						$cnt = 0;
						while ($row = @mysql_fetch_row($result)) {
						?>
						<tr height="25">   	
							<td align="center"><?php echo ++$cnt ?></td>
							<td align="left"><?php echo $row[1]?></td>
							<td align="left">
							<?php
							$sql_bobot = "SELECT b.jenisujian, a.bobot
										  FROM aspek_penilaian_per_pelajaran a, jenisujian b 
										  WHERE a.id_jenisujian=b.replid AND a.idpelajaran='$id' AND a.aspek_penilaian='$row[0]'";
							//echo"$sql_bobot";
							$rs_bobot = QueryDb($sql_bobot);
							while($row_bobot = @mysql_fetch_row($rs_bobot)){
								?><img src="../images/ico/aktif.png" border="0"/> <?php echo $row_bobot[0]?> = <?php echo $row_bobot[1]?> <br /><?php
							}
							?>									
							</td>      
						</tr>
						<?php	
						} 
						CloseDb(); 
						?>	
						<!-- END TABLE CONTENT -->
					</table>
				</td>
			</tr>
		</table>
	</body>
	<script language="javascript">
	window.print();
	</script>
</html>