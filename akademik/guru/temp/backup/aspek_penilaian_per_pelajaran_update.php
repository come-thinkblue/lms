<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php

require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/dpupdate.php');

if (isset($_REQUEST['id_aspek']))
	$id_aspek = $_REQUEST['id_aspek'];
//$id = (int)$id_aspek;
	
if (isset($_REQUEST['id_pelajaran']))
	$id_pelajaran = $_REQUEST['id_pelajaran'];	
	
if (isset($_REQUEST['aspek']))
	$aspek = $_REQUEST['aspek'];	
	
if (isset($_REQUEST['jum']))
	$jum = $_REQUEST['jum'];	
	
OpenDb();
	$sql_id = "SELECT id_aspek_penilaian_per_pelajaran 
			   FROM aspek_penilaian_per_pelajaran 
			   WHERE idpelajaran='$id_pelajaran' AND aspek_penilaian='$aspek' 
			   ORDER BY id_aspek_penilaian_per_pelajaran";
	//echo "$sql_id";
	$rs_id = QueryDb($sql_id);
	$index_id = 1;
	//$id_ne;
	while($row_id = @mysql_fetch_row($rs_id)){
		$id_ne[$index_id] = $row_id[0];
		$index_id++;
	}
if ($_REQUEST['action'] == 'Add') {
	for ($i = 1; $i <= $jum; $i++) {
	
		$id = $id_ne[$i];
		$jenis = $_REQUEST['ujian'.$i];
		$bobot = $_REQUEST['bobot'.$i];
		$cek = $_REQUEST['cek'.$i];
		
		/* echo "
		id = $id <br>
		jenis = $jenis <br>
		bobot = $bobot <br>
		cek = $cek <br>
		"; */
		
		if ($jenis && $cek == 1 && $bobot >= 0 && $id <> 0) {
			$sql1 = "UPDATE aspek_penilaian_per_pelajaran 
					SET idpelajaran='$id_pelajaran', aspek_penilaian='$aspek', id_jenisujian='$jenis', bobot='$bobot'
					WHERE id_aspek_penilaian_per_pelajaran='$id'";	
			//echo $sql1."<br><br>";
			QueryDb($sql1);
		}
		else if($jenis && $cek == 1 && $bobot >= 0 && $id == 0){
			$sql_max = "SELECT max(id_aspek_penilaian_per_pelajaran) FROM aspek_penilaian_per_pelajaran";
			$rs_max = QueryDb($sql_max);
			$row_max = @mysql_fetch_row($rs_max);
			$id_max = $row_max[0] + 1;
			$sql2 = "INSERT INTO aspek_penilaian_per_pelajaran 
					SET id_aspek_penilaian_per_pelajaran='$id_max', idpelajaran='$id_pelajaran', aspek_penilaian='$aspek', id_jenisujian='$jenis', bobot='$bobot'";	
			//echo $sql2."<br><br>";
			QueryDb($sql2);
		}
	}
	CloseDb();
	?>
	<script language="javascript">
		//alert('id=<?php echo $id_pelajaran?>');
		//document.location.href = "aspek_penilaian_per_pelajaran_content.php?id=<?php echo $id_pelajaran?>";
		opener.refresh();
		window.close();
	</script>
	<?php
}
