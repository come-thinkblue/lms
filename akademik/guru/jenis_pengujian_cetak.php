<?php
/**[N]**
 * 
 * @version: 3.0 (January 09, 2013)
 * @notes: LMS MAN Kota Blitar
 * 
 * Copyright (C) 2013 SMAN 1 Malang
 * 
 **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$id = $_REQUEST['kurikulum'];
OpenDb();
$sql = "SELECT jenisujian,iddasarpenilaian,keterangan,info1 FROM jenisujian WHERE idkurikulum='$id' ORDER BY iddasarpenilaian";   
$result = QueryDb($sql);
$cnt = 0;
$row = @mysql_fetch_row($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SISTEM AKADEMIK [Cetak Jenis Pengujian]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($row[6])?>

<center>
  <font size="3"><strong>DATA JENIS PENGUJIAN</strong></font><br />
 </center>
<br />
    <br />
<strong>
Pelajaran  : <?php echo $row[5]?>
<br />
Departemen : <?php echo $row[6]?>
<br /><br /><br /></strong>
<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
    	<td width="20%" class="header" align="center">Aspek</td>
        <td width="20%" class="header" align="center">Singkatan</td>
		<td width="30%" class="header" align="center">Jenis Pengujian</td>
        <td width="47%" class="header" align="center">Keterangan</td>
    </tr>
    
     <?php
		OpenDb();
		$sql = "SELECT jenisujian,iddasarpenilaian,keterangan,info1 FROM jenisujian WHERE idkurikulum='$id' ORDER BY iddasarpenilaian";   
		$result = QueryDb($sql);
		$cnt = 0;
		while ($row = @mysql_fetch_row($result)) {
		?>
    <tr>   	
       	<td align="center"><?php echo ++$cnt ?></td>
       	<?php 
			$select = "SELECT keterangan FROM dasarpenilaian WHERE dasarpenilaian='$row[1]'";
			$hasil = QueryDb($select);
			$aspek = mysql_fetch_row($hasil);
				echo $aspek[0];
		?>
		</td>
		<td align="center"><?php echo $row[3]?></td>
		<td align="center"><?php echo $row[0]?></td>
		<td align="center">
		<td><?php echo $row[2]?></td>           
    </tr>
<?php	} 
	CloseDb(); ?>	
    
    <!-- END TABLE CONTENT -->
    </table>

</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>