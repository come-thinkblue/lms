<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');

$departemen = $_REQUEST['departemen'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Daftar Pelajaran]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center>
  <font size="4"><strong>DAFTAR PELAJARAN </strong></font><br />
 </center><br /><br />

<br />
	<strong>Departemen : <?php echo $departemen?></strong>
<br /><br />
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="8%" class="header" align="center">Singkatan</td>
        <td width="25%" class="header" align="center">Nama</td>
        <td width="10%" class="header" align="center">Sifat KTSP</td>
		<td width="10%" class="header" align="center">Sifat K13</td>
        <td width="*" class="header" align="center">Lintas Minat</td>
        <td width="10%" class="header" align="center">Status</td>
    </tr>
<?php 	OpenDb();
	$sql = "SELECT kode,nama,sifat_ktsp,sifat_k13,keterangan,aktif FROM pelajaran WHERE departemen='$departemen' ORDER BY $urut $urutan";// LIMIT ".(int)$page*(int)$varbaris.",$varbaris";   
	$result = QueryDB($sql);
	//if ($page==0)
		$cnt = 0;
	//else
		//$cnt = (int)$page*(int)$varbaris;
		
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">    	
    	<td align="center"><?php echo ++$cnt ?></td>
        <td><?php echo $row['kode'] ?></td>        
        <td><?php echo $row['nama'] ?></td>        
        <td align="center">
			<?php 
				if ($row['sifat_ktsp'] == 1) 
					echo 'Wajib';
				else
					echo 'Tambahan';
			?>	
        </td>
		<td align="center">
			<?php 
				if ($row['sifat_k13'] == 0) 
					echo 'Kelompok A';
				else if($row['sifat_k13'] == 1)
					echo 'Kelompok B';
				else if($row['sifat_k13'] == 2)
					echo 'Kelompok MIA';
				else if($row['sifat_k13'] == 3)
					echo 'Kelompok IIS';
				else 
					echo 'Kelompok IIK';
			?>
		</td>
        <td><?php echo $row['keterangan'] ?></td>
        <td align="center">
			<?php if ($row['aktif'] == 1) 
					echo 'Aktif';
				else
					echo 'Tidak Aktif';
			?>		
        </td> 
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>
</td>
</tr>
<!-- END TABLE CENTER -->    
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>