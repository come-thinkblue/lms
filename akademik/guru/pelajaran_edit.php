
<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

$replid = $_REQUEST['replid'];

$ERROR_MSG = "";
if (isset($_REQUEST['Simpan'])) {
  if(CQ($_REQUEST['kode'])!=CQ($_REQUEST['kode_lama'])){
	OpenDb();
	$sql = "SELECT * FROM pelajaran WHERE kode = '".CQ($_REQUEST['kode'])."'";
	//echo 'sql '.$sql;
	$result = QueryDb($sql);
	
	if (mysql_num_rows($result) > 0) {
		CloseDb();
		$ERROR_MSG = "Singkatan $_REQUEST[kode] sudah digunakan!";
	} else {
		$sql = "UPDATE pelajaran SET kode='".CQ($_REQUEST['kode'])."',nama='".CQ($_REQUEST['nama'])."',sifat_ktsp='$_REQUEST[sifat_ktsp]',sifat_k13='$_REQUEST[sifat_k13]',keterangan='".CQ($_REQUEST['keterangan'])."' WHERE replid='$replid'";
		$result = QueryDb($sql);
		CloseDb();
	
		if ($result) { ?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
<?php		}
		exit();
	}
  }else{
	OpenDb();
	$sql = "UPDATE pelajaran SET kode='".CQ($_REQUEST['kode'])."',nama='".CQ($_REQUEST['nama'])."',sifat_ktsp='$_REQUEST[sifat_ktsp]',sifat_k13='$_REQUEST[sifat_k13]',keterangan='".CQ($_REQUEST['keterangan'])."' WHERE replid='$replid'";
	$result = QueryDb($sql);
	CloseDb();
	
	if ($result) { ?>
		<script language="javascript">
			opener.refresh();
			window.close();
		</script> 
<?php }
	exit();
  }
}

OpenDb();

$sql = "SELECT kode,nama,sifat_ktsp,sifat_k13,keterangan,departemen FROM pelajaran WHERE replid='$replid'";
$result = QueryDb($sql);
$row = mysql_fetch_row($result);
$kode = $row[0];
if (isset($_REQUEST['kode']))
	$kode = $_REQUEST['kode'];
$nama = $row[1];
if (isset($_REQUEST['nama']))
	$nama = $_REQUEST['nama'];
$sifat_ktsp = $row[2];
if (isset($_REQUEST['sifat_ktsp']))
	$sifat_ktsp = $_REQUEST['sifat_ktsp'];
$sifat_k13 = $row[3];
if (isset($_REQUEST['sifat_k13']))
	$sifat_k13 = $_REQUEST['sifat_k13'];
$keterangan = $row[4];
if (isset($_REQUEST['keterangan']))
	$keterangan = $_REQUEST['keterangan'];
$departemen = $row[5];
CloseDb();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Ubah Pelajaran]</title>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script language="javascript">

function validate() {
	return validateEmptyText('nama', 'Nama Pelajaran') &&
		   validateEmptyText('kode', 'Nama Singkatan') &&
		   validateMaxText('keterangan', 255, 'Keterangan');
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
		return false;
    } 
    return true;
}

function panggil(elem){
	var lain = new Array('kode','nama','keterangan');
	for (i=0;i<lain.length;i++) {
		if (lain[i] == elem) {
			document.getElementById(elem).style.background='#4cff15';
		} else {
			document.getElementById(lain[i]).style.background='#FFFFFF';
		}
	}
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('nama').focus()">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
    .: Ubah Pelajaran :.
    </div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr height="150">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
    <!-- CONTENT GOES HERE //--->
<form name="main" onSubmit="return validate()">
<input type="hidden" name="replid" id="replid" value="<?php echo $replid ?>" />
<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
<!-- TABLE CONTENT -->
<tr>
	<td width="120"><strong>Departemen</strong></td>
	<td><input type="text" name="departemen" id="departemen" size="10" value="<?php echo $departemen ?>" readonly class="disabled"/>
    </td>
</tr>
<tr>
    <td><strong>Nama</strong></td>
    <td>
    	<input type="text" name="nama" id="nama" size="30" maxlength="50" value="<?php echo $nama ?>" onFocus="showhint('Nama pelajaran tidak boleh lebih dari 50 karakter!', this, event, '120px');panggil('nama')"  onKeyPress="return focusNext('kode', event)" />
    </td>
</tr>
<tr>
    <td><strong>Singkatan</strong></td>
    <td>
		<input type="hidden" name="kode_lama" value="<?php echo $row[0] ?>"/>
    	<input type="text" name="kode" id="kode" size="10" maxlength="4" value="<?php echo $kode ?>" onFocus="showhint('Nama singkatan tidak boleh lebih dari 4 karakter!', this, event, '120px');panggil('kode')"  onKeyPress="return focusNext('keterangan', event)" />
    </td>
</tr>
<tr>
	<td><strong>Sifat KTSP</strong></td>
    <td>
	<?php 
		if ($sifat_ktsp == 0) {  ?> 	
    	<input type="radio" name="sifat_ktsp" value=0 checked />&nbsp;Wajib&nbsp;
    	<input type="radio" name="sifat_ktsp" value=1 />&nbsp;Tambahan&nbsp;
    <?php } else { ?>
		<input type="radio" name="sifat_ktsp" value=0 />&nbsp;Wajib&nbsp;
    	<input type="radio" name="sifat_ktsp" value=1 checked />&nbsp;Tambahan&nbsp;
    <?php }
	?>
    </td>
</tr>
<tr>
<td><strong>Sifat K13</strong></td>
<td>
	<?php 
		if ($sifat_k13 == 0) {  ?> 	
    	<input type="radio" name="sifat_k13" value=0 checked />&nbsp;Kelompok A&nbsp;
    	<input type="radio" name="sifat_k13" value=1 />&nbsp;Kelompok B&nbsp;
		<input type="radio" name="sifat_k13" value=2 />&nbsp;Kelompok MIA&nbsp;
		<input type="radio" name="sifat_k13" value=3 />&nbsp;Kelompok IIS&nbsp;
		<input type="radio" name="sifat_k13" value=4 />&nbsp;Kelompok IIK&nbsp;
    <?php } else if($sifat_k13 == 1){ ?>
		<input type="radio" name="sifat_k13" value=0 />&nbsp;Kelompok A&nbsp;
    	<input type="radio" name="sifat_k13" value=1 checked />&nbsp;Kelompok B&nbsp;
		<input type="radio" name="sifat_k13" value=2 />&nbsp;Kelompok MIA&nbsp;
		<input type="radio" name="sifat_k13" value=3 />&nbsp;Kelompok IIS&nbsp;
		<input type="radio" name="sifat_k13" value=4 />&nbsp;Kelompok IIK&nbsp;
    <?php }else if($sifat_k13 == 2){?> 
		<input type="radio" name="sifat_k13" value=0 />&nbsp;Kelompok A&nbsp;
    	<input type="radio" name="sifat_k13" value=1 />&nbsp;Kelompok B&nbsp;
		<input type="radio" name="sifat_k13" value=2 checked />&nbsp;Kelompok MIA&nbsp;
		<input type="radio" name="sifat_k13" value=3 />&nbsp;Kelompok IIS&nbsp;
		<input type="radio" name="sifat_k13" value=4 />&nbsp;Kelompok IIK&nbsp;
	<?php }else if($sifat_k13 == 3){?> 
		<input type="radio" name="sifat_k13" value=0 />&nbsp;Kelompok A&nbsp;
    	<input type="radio" name="sifat_k13" value=1 />&nbsp;Kelompok B&nbsp;
		<input type="radio" name="sifat_k13" value=2 />&nbsp;Kelompok MIA&nbsp;
		<input type="radio" name="sifat_k13" value=3 checked />&nbsp;Kelompok IIS&nbsp;
		<input type="radio" name="sifat_k13" value=4 />&nbsp;Kelompok IIK&nbsp;
	<?php }else if($sifat_k13 == 4){?> 
		<input type="radio" name="sifat_k13" value=0 />&nbsp;Kelompok A&nbsp;
    	<input type="radio" name="sifat_k13" value=1 />&nbsp;Kelompok B&nbsp;
		<input type="radio" name="sifat_k13" value=2 />&nbsp;Kelompok MIA&nbsp;
		<input type="radio" name="sifat_k13" value=3 />&nbsp;Kelompok IIS&nbsp;
		<input type="radio" name="sifat_k13" value=4 checked />&nbsp;Kelompok IIK&nbsp;
	<?php }?>
</td>
</tr>
<tr>
	<td><strong>Lintas Minat</strong></td>
	<td>
    	<select name="keterangan" id="keterangan">
    	<?php 
    	if($keterangan=='MIA'){
    		echo 
    		"<option value='Tidak Ada'>Tidak Ada</option>
    		<option selected='select' value='MIA'>MIA</option>
    		<option value='IIS'>IIS</option>
    		<option value='IIK'>IIK</option>";
    	}else if($keterangan=='IIS'){
    		echo 
    		"<option value='Tidak Ada'>Tidak Ada</option>
    		<option value='MIA'>MIA</option>
    		<option selected='select' value='IIS'>IIS</option>
    		<option value='IIK'>IIK</option>";
    	}else if($keterangan=='IIK'){
    		echo 
    		"<option value='Tidak Ada'>Tidak Ada</option>
    		<option value='MIA'>MIA</option>
    		<option value='IIS'>IIS</option>
    		<option selected='select' value='IIK'>IIK</option>";
    	}else{
    		echo 
    		"<option selected='select' value='Tidak Ada'>Tidak Ada</option>
    		<option value='MIA'>MIA</option>
    		<option value='IIS'>IIS</option>
    		<option value='IIK'>IIK</option>";
    	}
    	?>
    	</select>
    </td>
</tr>
<tr>
	<td colspan="2" align="center">
    <input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
    <input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
    </td>
</tr>
<!-- END OF TABLE CONTENT -->
</table>
</form>
 <!-- END OF CONTENT //--->
    </td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>
<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo $ERROR_MSG?>');
</script>
<?php } ?>

</body>
</html>