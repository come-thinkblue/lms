<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php

require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/theme.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/dpupdate.php');

if (isset($_REQUEST['id_tingkat']))
	$id_tingkat = $_REQUEST['id_tingkat'];
if (isset($_REQUEST['idkurikulum']))
	$kurikulum = $_REQUEST['idkurikulum'];	
if (isset($_REQUEST['aspek']))
	$aspek = $_REQUEST['aspek'];	
if (isset($_REQUEST['jum']))
	$jum = $_REQUEST['jum'];	
OpenDb();
if ($_REQUEST['action'] == 'Add') 
{
	$sql = "SELECT * FROM dasarpenilaian d, tingkat t, aturannhb a 
			WHERE a.dasarpenilaian = d.dasarpenilaian 
			AND a.idtingkat = t.replid AND a.idtingkat = '$id_tingkat' AND a.dasarpenilaian = '$aspek'"; 
	$result = QueryDb($sql);
	
	if (mysql_num_rows($result) > 0) 
	{
		CloseDb(); ?>
		<script language="javascript">
			alert ('Aspek <?php echo $aspek?> sudah digunakan!');
			window.self.history.back();
		</script>
		<?php
		exit;
	} 
}

BeginTrans();	
$success=0;
for ($i = 1; $i <= $jum; $i++) 
{
	$jenis = $_REQUEST['ujian'.$i];
	$bobot = $_REQUEST['bobot'.$i];
	$id = $_REQUEST['replid'.$i];
	$cek = $_REQUEST['cek'.$i];
	$isdel = $_REQUEST['isdel'.$i];
	
	if ($isdel)
	{
		$sql1 = "DELETE FROM aturannhb WHERE replid = $id";
		QueryDbTrans($sql1, $success);
	}
	else
	{
		if ($jenis && $cek == 1 && $bobot >= 0) 
		{
			if ($id != "" && $id<>0) 
				$sql1 = "UPDATE aturannhb SET bobot='$bobot', aktif=1, info1=NULL WHERE replid = '$id'";				
			else 
				$sql1 = "INSERT INTO aturannhb SET
						 idtingkat='$id_tingkat', idkurikulum='$kurikulum',
						 dasarpenilaian='$aspek', idjenisujian='$jenis', bobot='$bobot'";	
			//echo $sql1."_$id<br>";
			QueryDbTrans($sql1, $success);
		} 
	}
}
//exit;	
if ($success) 
{ 
	CommitTrans();
	CloseDb(); ?>
	<script language="javascript">
		opener.document.location.href="perhitungan_rapor_content.php";
		window.close();
	</script>
<?php 
} 
else 
{ 
	RollbackTrans();
	CloseDb(); ?>
	<script language="javascript">
        alert ('Data gagal disimpan !');
        window.close();
    </script>
<?php
}
?>