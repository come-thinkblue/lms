<?php
/**[N]**
 * LMS SMA Negeri 1 Malang
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../cek.php');
require_once('../library/dpupdate.php');

$cetak = 0;
$oto = 0;
if(isset($_GET['oto']))
	$oto = $_GET['oto'];
if(isset($_GET['aspek']))
	$aspek = $_GET['aspek'];
if(isset($_GET['idtingkat']))
	$idtingkat = $_GET['idtingkat'];

OpenDb();
$sql = "SELECT departemen FROM departemen WHERE aktif='1'"; 

$result = QueryDb($sql);
$row = @mysql_fetch_row($result);
$departemen = $row[0];

$op = $_REQUEST['op'];
if ($op == "xm8r389xemx23xb2378e23") 
{
	$sql = "DELETE FROM aturangrading WHERE idtingkat = '$idtingkat' AND dasarpenilaian = '$aspek'"; 
	QueryDb($sql);	?>
    <script>refresh();</script> 
<?php
}
if($_GET['kurikulum']==1){	
  if ( $oto == 1 ) {
  $id= $_GET['nTingkat'];
  $queryInsert ="insert into aturangrading (idtingkat, dasarpenilaian, nmin,nmax,grade) VALUES
  (".$id.",'AF',85, 100, 'A'),
  (".$id.",'AF',70, 84, 'B'),
  (".$id.",'AF',55, 69, 'C'),
  (".$id.",'AF',0, 54, 'D'),
  (".$id.",'KOG',85, 100, 'A'),
  (".$id.",'KOG',70, 84, 'B'),
  (".$id.",'KOG',55, 69, 'C'),
  (".$id.",'KOG',0, 54, 'D'),
  (".$id.",'PSI',85, 100, 'A'),
  (".$id.",'PSI',70, 84, 'B'),
  (".$id.",'PSI',55, 69, 'C'),
  (".$id.",'PSI',0, 54, 'D')";
  
  $hasil = mysql_query ($queryInsert)or die (mysql_error());
?>
<script>refresh();</script>
<?php 
  }
}else{
  if ( $oto == 1 ) {
  $id= $_GET['nTingkat'];
  $queryInsert ="insert into aturangrading (idtingkat, idkurikulum, dasarpenilaian, nmin,nmax,grade) VALUES
  (".$id .",'2','KI3',85, 100, 'A'),
  (".$id .",'2','KI3',70, 84, 'B'),
  (".$id .",'2','KI3',55, 69, 'C'),
  (".$id .",'2','KI3',0, 54, 'D'),
  (".$id .",'2','KI4',85, 100, 'A'),
  (".$id .",'2','KI4',70, 84, 'B'),
  (".$id .",'2','KI4',55, 69, 'C'),
  (".$id .",'2','KI4',0, 54, 'D')";
  
  $hasil = mysql_query ($queryInsert)or die (mysql_error());
?>
<script>refresh();</script>
<?php 
  }
}
 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aturan Penentuan Grading Nilai</title>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function refresh() {	
	document.location.href = "aturannilai_content.php?oto=0";
}

function edit(idtingkat,aspek) {
	newWindow('aturannilai_edit.php?idtingkat='+idtingkat+'&aspek='+aspek, 'UbahAturanPenentuanGradingNilai','360','500','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function tambah(tingkat,kurikulum) {
	newWindow('aturannilai_add.php?idtingkat='+tingkat+'&kurikulum='+kurikulum, 'TambahAturanPenentuanGradingNilai','360','500','resizable=1,scrollbars=1,status=0,toolbar=0')
}

function hapus(idtingkat,aspek) {
	if (confirm("Apakah anda yakin akan menghapus aspek penilaian ini?"))
		document.location.href = "aturannilai_content.php?op=xm8r389xemx23xb2378e23&idtingkat="+idtingkat+"&aspek="+aspek;		
}

function cetak() {
	var cetak = document.getElementById('cetak').value;	

	if (cetak == '1') 
		newWindow('aturannilai_cetak.php', 'CetakAturanPenentuanGradingNilai','790','650','resizable=1,scrollbars=1,status=0,toolbar=0');
	else 
		alert ('Tidak ada data yang dapat dicetak');
	
}
</script>
</head>
<body topmargin="0" leftmargin="0">
<?php 
if (!isset($_POST['oto']) )
	$_POST['oto'] = 0;
	
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<table width="100%" border="0" height="100%">
<tr>
	<td>
	<table width="100%" border="0">
    <!-- TABLE TITLE -->
	<tr>
		<td valign="top" align="right"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Aturan Perhitungan Grading Nilai</font></td>
	</tr>
    <tr>
        <td valign="top" align="right"><a href="../guru.php?page=p" target="content">
    <font size="1" color="#000000"><b>Guru & Pelajaran</b></font></a>&nbsp>&nbsp <font size="1" color="#000000"><b>Aturan Perhitungan Grading Nilai</b></font></td>
    </tr>
    </table>
    
    <br /><br />
	<table width="100%" border="0">
    <tr>
    	<td width="20%" rowspan="4"></td>
        <td width="10%"><strong>Departemen</strong></td>
    	<td><strong>: <?php echo $departemen ?>
        <input type="hidden" name="departemen" id="departemen" readonly value="<?php echo $departemen ?>" />
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
   		</strong></td>
        <td rowspan="2"></td>
  	</tr>
  	<tr>
<?php	$sql = "SELECT tingkat,replid,idkurikulum FROM tingkat WHERE departemen = '$departemen' AND aktif=1 ORDER BY urutan";
	$result = QueryDb($sql);
	if (@mysql_num_rows($result) > 0)
	{ ?>
		<td valign="top" align="right" colspan="2"> <a href="JavaScript:refresh()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
      <a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp;  
    	</td>
  	</tr>
  	</table>
<?php		$i = 0;
		while ($row = @mysql_fetch_array($result)) 
		{
			++$i;
			$sql1 = "SELECT g.dasarpenilaian, dp.keterangan 
					   FROM aturangrading g, tingkat t, dasarpenilaian dp
					  WHERE t.replid = g.idtingkat AND t.departemen = '$departemen' 
						AND g.dasarpenilaian = dp.dasarpenilaian 
						AND g.idtingkat = '$row[replid]' AND g.idkurikulum= '$row[idkurikulum]' GROUP BY g.dasarpenilaian";
			$result1 = QueryDb($sql1);	?>
    	<br />
    	<fieldset>
        <legend><b>Tingkat <?php echo $row['tingkat']?> &nbsp;&nbsp;&nbsp; 
        <input type="hidden" name="idtingkat" id="idtingkat" value="<?php echo $row['replid'] ?>" />
<?php		if (@mysql_num_rows($result1) > 0) 
		{ 
			$cetak = 1; ?>	    
	        <a href="JavaScript:tambah(<?php echo $row['replid'].",".$row['idkurikulum']?>)" ><img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')"/>&nbsp;Input Aturan Penentuan Grading Nilai</a>
			</b></legend><br />
		<!--<table border="1" width="100%" id="table<?php echo $i?>" class="tab">-->
        <table class="tab" id="table<?php echo $i?>" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">
		<tr>		
			<td class="header" align="center" height="30" width="10%">No</td>
			<td class="header" align="center" height="30" width="*">Aspek Penilaian</td>
			<td class="header" align="center" height="30" width="*">Grading</td>
            <td class="header" height="30" width="*">&nbsp;</td>
		</tr>
		<?php 
			$cnt= 0;
			while ($row1 = @mysql_fetch_row($result1)) 
			{	?>	
		<tr>        			
			<td align="center" height="25"><?php echo ++$cnt?></td>
			<td height="25"><?php echo $row1[1]?><input type="hidden" name="dasar" id="dasar" value="<?php echo $row1[0] ?>" /></td>
  			<td height="25">
<?php			$sql2 = "SELECT g.replid, grade, nmin, nmax
					   FROM aturangrading g, tingkat t
					  WHERE t.replid = g.idtingkat AND t.departemen = '$departemen' 
						AND g.idtingkat = '$row[replid]' AND g.dasarpenilaian = '$row1[0]'
						AND g.idkurikulum= '$row[idkurikulum]'
						ORDER BY grade";
			$result2 = QueryDb($sql2);			
			while ($row2 = @mysql_fetch_row($result2)) 
			{
				echo $row2[1].' : '.$row2[2].' s/d '.$row2[3]. '<br>'; 
			} ?>	
<!---->			
            </td>
            <td align="center" height="25" width="*">        
	<a href="JavaScript:edit('<?php echo $row['replid']?>','<?php echo $row1[0]?>')"><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah!', this, event, '50px')"/></a>&nbsp;   
 	<a href="JavaScript:hapus('<?php echo $row['replid']?>','<?php echo $row1[0]?>')">
    <img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus!', this, event, '50px')" /></a>
        	</td>
		</tr>
<?php		} ?>
		</table>
	<script language='JavaScript'>Tables('table<?php echo $i?>', 1, 0);</script>
    
<?php	} else { ?>
    	
	</legend>
		<table width="100%" border="0" align="center">          
		<tr>
			<td align="center" valign="middle">
    		<font size = "2" color ="red"><b>Tidak ditemukan adanya data.  
           <br />Klik <a href="JavaScript:tambah(<?php echo $row['replid']?>)" ><font size = "2" color ="green">di sini</font></a> untuk mengisi data baru pada tingkat <?php echo $row['tingkat']?>. 
            </b></font>
            
          <?php
          $qID ="select replid, idkurikulum from tingkat where tingkat='".$row['tingkat']."'";
		  $hqID= mysql_query($qID);
		  $hasilID= mysql_fetch_array($hqID);
		  $idtingkat2= $hasilID['replid'];
		  $kurikulum= $hasilID['idkurikulum'];
		  
		  ?>  
       <form action="aturannilai_content.php" method="get">
       <input type="hidden" name="oto" id="oto" value="1">
	   <input type="hidden" name="kurikulum" id="kurikulum" value="<?php echo $kurikulum?>">
       <input type="hidden" name="nTingkat" value=" <?php echo $idtingkat2?>">
        <input type="submit" value="Otomatis">
        </form>
        
      
			</td>
		</tr>
	 	</table>
<?php } ?><br /> 
  </fieldset>
<?php } ?>
 <input type="hidden" name="cetak" id="cetak" value="<?php echo $cetak ?>" />
    <!-- END TABLE CONTENT -->
 	</td>
  </tr>
</table>
<?php } else { ?>
</td><td width = "50%"></td>
</tr>
<tr height="60"><td colspan="4"><hr style="border-style:dotted" /></td>
</tr>
</table>
<table width="100%" border="0" align="center">          
<tr>
	<td align="center" valign="middle" height="200">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data.
        <br />Tambah tingkat kelas pada departemen <?php echo $departemen?> di menu Referensi.
        </b></font>
	</td>
</tr>
</table>  
<?php 
} 
?>
</body>
</html>
<?php
CloseDb();
?>