<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
 
$aktif = 0;
$guru = $_REQUEST['departemen'];
$departemen = $_REQUEST['departemen'];
$query ="AND j.departemen = '$departemen'";
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

if ($_REQUEST['aktif']) { 	
	$aktif = 1;
	$id = $_REQUEST['id'];
	OpenDb();
	$sql = "SELECT nama FROM pelajaran WHERE replid ='$id'";
	$result = QueryDb($sql); 
	CloseDb();
	$row = mysql_fetch_array($result);
	$guru = $row['nama'];
	$query = "AND g.idpelajaran='$id'";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Data Guru]</title>
</head>

<body>
<table border="0" cellspacing="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center>
  <font size="4"><strong>DATA GURU</strong></font><br />
 </center>
<br />
    <br />
    <strong>Guru <?php echo $guru?></strong>
    <br /><br />
    
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
    
    <tr height="30">
        <td width="4%" class="header" align="center">No</td>
        <td width="10%" class="header" align="center">NIP</td>
        <td width="15%" class="header" align="center">Guru</td>
        <?php if (!$aktif) { ?>
        	<td width="15%" class="header" align="center">Pelajaran</td>
        <?php } ?>
        <td width="15%" class="header" align="center">Status Guru</td>
        <td width="*" class="header" align="center">Keterangan</td>
    </tr>
   		 <?php
		OpenDb();
		$sql = "SELECT g.replid,g.nip,p.nama,g.statusguru,g.keterangan,j.nama FROM guru g, $g_db_pegawai.pegawai p, pelajaran j, statusguru s WHERE g.nip=p.nip AND g.idpelajaran = j.replid AND g.statusguru = s.status $query ORDER BY $urut $urutan"; 
		
		$result = QueryDb($sql);
		$cnt = 0;
		while ($row = @mysql_fetch_row($result)) {
	
		?>
    <tr height="25">   	
       	<td align="center"><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row[1]?></td>
        <td><?php echo $row[2] ?></td>
         <?php if (!$aktif) { ?>
        	<td><?php echo $row[5]?></td>
        <?php } ?> 
        <td><?php echo $row[3]?></td>
        <td><?php echo $row[4]?></td>        
    </tr>
<?php	} 
	CloseDb(); ?>	
    
    <!-- END TABLE CONTENT -->
    </table>

</body>
<script language="javascript">
window.print();
</script>
</html>