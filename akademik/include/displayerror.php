<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once("../../include/mainconfig.php");

session_name("dbakademik");
if(!isset($_SESSION)){ session_start();}

if ($_SESSION['errno'] != 0) 
{
	$rel = "./";
	if (file_exists("../style/style.css"))
		$rel = "../";
	elseif (file_exists("../../style/style.css"))
		$rel = "../../";
		
	date_default_timezone_set('Asia/Jakarta');
		
	$html1 .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><link rel="stylesheet" type="text/css" href="'. $rel .'style/style.css"></head><body>';
	
	$html1 .= "<input type='hidden' name='apptype' value='public'>\r\n";
	$html1 .= "<input type='hidden' name='appmodule' value='akademik'>\r\n";
	$html1 .= "<input type='hidden' name='appversion' value='$G_VERSION'>\r\n";
	$html1 .= "<input type='hidden' name='appbuilddate' value='$G_BUILDDATE'>\r\n";
	$html1 .= "<input type='hidden' name='source' value='application'>\r\n";
	$html1 .= "<input type='hidden' name='errtime' value='" . date('Y-m-d H:i:s') ."'>\r\n";
	$html1 .= "<input type='hidden' name='errtype' value='" . $_SESSION['errtype'] ."'>\r\n";
	$html1 .= "<input type='hidden' name='errno' value='" . $_SESSION['errno'] ."'>\r\n";
	$html1 .= "<input type='hidden' name='errmsg' value='" . urlencode($_SESSION['errmsg']) ."'>\r\n";
	$html1 .= "<input type='hidden' name='errfile' value='" . urlencode($_SESSION['errfile']) ."'>\r\n";
	$html1 .= "<table border='0' width='100%' height='100%'>\r\n";
	$html1 .= "<tr height='400'><td align='center' valign='middle' background='". $rel ."images/ico/b_warning.png' style='margin:0;padding:0;background-repeat:no-repeat;'>\r\n";
	$html1 .= "<table width='457' border='0' cellpadding='0' cellspacing='0'><tr><td><img src='". $rel ."images/bk_message_01.jpg' width='457' height='17'></td></tr><tr><td style='background-image:url(". $rel ."images/bk_message_02.jpg); padding-left:20px; padding-right:20px;'>\r\n";
	
	$html2  = "</td></tr><tr><td><img src='". $rel ."images/bk_message_03.jpg' width='457' height='18'></td></tr></table>";
	$html2  .= "</td></tr></table></td></tr></table></form></body></html>";
	
	if ($_SESSION['errtype'] == 1 && $_SESSION['errno'] == 1451) 
	{
		$errstr  = "<center><font familiy='Verdana' color='#666666' size='2' style='text-decoration:none'><strong>Maaf, anda tidak dapat menghapus data ini, karena telah digunakan oleh data lainnya!</strong></font></center>";
	}
	else 
	{	
		$errstr  = "<center><h2>Maaf, telah terjadi kesalahan</h2></center>\n";
		
		// errno 1146 : Table '%s.%s' doesn't exist 
		// errno 1449 : The user specified as a definer ('root'@'%') does not exist
		// errno 2006 : MySQL server has gone away
		if (in_array($_SESSION['errno'], array(1146, 1449, 2006)))
		{
			$errstr .= "<center>
							Ada kesalahan pada instalasi atau konfigurasi basis data. Silahkan periksa kembali instalasi dan konfigurasi basis data yang digunakan.
							
							</font><br><br></center>";
			$_SESSION['issend'] = false;				
		}
		
		
		if ($_SESSION['issend'])
			$errstr .= "<input type='submit' class='but' value='Kirim' ></center>\r\n";
	}
	echo $html1 . $errstr . $html2;
	
    // Dont not display error message again 
	// $_SESSION['errno'] = 0;
}
?>