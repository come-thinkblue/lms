<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
LMS MAN Kota Blitar
</title>
<link href="images/logo.ico" rel="shortcut icon" />
<link href="css/button.css" rel="stylesheet" type="text/css" />
<link href="css/zice.style.css" rel="stylesheet" type="text/css" />


</script>
<style type="text/css">
body {
	background-color:#0d929a;
	background-image:url(images/bgLogin.png);
	background-repeat:repeat-x;
	
	
}

   
</style>
</head>
<body> 
<div class="wadahBesar" >
<div class="logoLMS"></div>
<div id="login" >
<div class="ribbon"></div>
  <div class="inner">
  <div  class="logo" ><img src="images/logoLogin.png" alt="ziceAdmin" /></div>
<div class="userbox"></div>
<div class="formLogin">
   <form method="post" name="form" id="form" action="redirect.php" onSubmit="return cek_form()">
          <div class="tip">
          <input name="username" id="username" type="text"    />
          </div>
          <div class="tip">
           <input name="password" id="password" type="password"   />
          </div>
          <div style="padding:20px 0px 0px 0px ;">
            
          <div style="float:right;padding:2px 0px ;">
              <div> 
                <ul class="uibutton-group">
                 <input type="submit"  class="uibutton normal" value="LOGIN" />
               </ul>
              </div>
            </div>
</div>
    </form>
      
  </div>

<div id="versionBar" >
  <div class="copyright" > &copy; Copyright 2013  All Rights Reserved   <span class="tip"><a  href="#" title="Zice Admin" >  MAN Kota Blitar</a> </span> </div>
  <div class="shadow"></div>
  
  </div>
  <!-- // copyright-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-jrumble.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>     
<script type="text/javascript" src="js/jquery.tipsy.js"></script>
<script type="text/javascript" src="js/iphone.check.js"></script>
<script type="text/javascript" src="js/login.js"></script>

</body>
</html>