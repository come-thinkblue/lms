<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$departemen = $_REQUEST['departemen'];
$tahunajaran = $_REQUEST['tahunajaran'];
$semester = $_REQUEST['semester'];
$kelas = $_REQUEST['kelas'];

OpenDb();
$sql = "SELECT k.kelas, s.semester, a.tahunajaran, t.tingkat FROM kelas k, semester s, tahunajaran a, tingkat t WHERE k.replid = '$kelas' AND s.replid = $semester AND a.replid = '$tahunajaran' AND k.idtahunajaran = a.replid AND k.idtingkat = t.replid";
//echo $sql;
$result = QueryDb($sql);
CloseDb();
$row = mysql_fetch_array($result);
$namakelas = $row['kelas'];
$namasemester = $row['semester'];
$namatahunajaran = $row['tahunajaran'];
$namatingkat = $row['tingkat'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Form Presensi Harian]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">
<?php echo getHeader($departemen)?>
<center>
  <font size="4"><strong>FORM PRESENSI HARIAN </strong></font><br />
 </center><br /><br />

<br />
<table border="0">
<tr>
	<td><strong>Departemen</strong> </td> 
	<td><strong>:&nbsp;<?php echo $departemen?></strong></td>
</tr>
<tr>
	<td><strong>Tahun Ajaran</strong></td>
	<td><strong>:&nbsp;<?php echo $namatahunajaran?></strong></td>
</tr>
<tr>
	<td><strong>Semester</strong></td>
	<td><strong>:&nbsp;<?php echo $namasemester?></strong></td>
</tr>

	<td><strong>Kelas</strong></td>
	<td><strong>:&nbsp;<?php echo $namatingkat ?>&nbsp;-&nbsp;<?php echo $namakelas?></strong></td>
</tr>
<tr height="25">
    <td><strong>Tanggal</strong></td>
    <td>:&nbsp;<strong>_________________ s/d _________________</strong></td>
</tr>
	

</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left">
        <tr>
            <td width="5%" height="30" align="center" class="header">No</td>
            <td width="10%" height="30" align="center" class="header">N I S</td>
            <td width="30%" height="30" align="center" class="header">Nama</td>
            <td width="5%" height="30" align="center" class="header">H</td>	
            <td width="5%" height="30" align="center" class="header">I</td>	
            <td width="5%" height="30" align="center" class="header">S</td>	
            <td width="5%" height="30" align="center" class="header">A</td>	
            <td width="5%" height="30" align="center" class="header">C</td>	
            <td width="30%" height="30" align="center" class="header">Keterangan</td>
        </tr>
        
        <?php
        OpenDb();
        
        $sql = "SELECT nis, nama FROM siswa WHERE idkelas = '$kelas' AND alumni=0 AND aktif=1 ORDER BY nama";
        $result = QueryDb($sql);
        CloseDb();
        
        while($row = @mysql_fetch_array($result)){
        
        ?>
        <tr>
            <td height="25" align="center" class="tab"><?php echo ++$i ?></td>
            <td height="25" align="center" class="tab"><?php echo $row['nis'] ?></td>
            <td height="25" class="tab"><?php echo $row['nama'] ?></td>
            <td height="25" class="tab"></td>
            <td height="25" class="tab"></td>
            <td height="25" class="tab"></td>
            <td height="25" class="tab"></td>
            <td height="25" class="tab"></td>
            <td height="25" class="tab"></td>
        </tr>
        <?php } ?>
    </table>
    </td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0">
         <tr>
            <td colspan="2" align="left">
            Ket: H = Hadir; I = Ijin; S = Sakit; A = Alpa; C = Cuti
            </td>
        </tr><tr>
            <td width="80%" align="left"></td>
            <td width="20%" align="center"><br><br>Wali Kelas</td>
        </tr>
        <tr>
            <td colspan="2" align="right">&nbsp;<br /><br /><br /><br /><br /></td>
        </tr>
        <tr>		
            <td></td>
            <td align="center" valign="bottom">(&nbsp;__________________________&nbsp;)</td>
        </tr>
       
    </table>
    </td>
  </tr>
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>