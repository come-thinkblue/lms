<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
$semester = $_REQUEST['semester'];
$kelas = $_REQUEST['kelas'];
$tglawal = $_REQUEST['tglawal'];
$tglakhir = $_REQUEST['tglakhir'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

OpenDb();
$sql = "SELECT t.departemen, a.tahunajaran, s.semester, k.kelas, t.tingkat FROM tahunajaran a, kelas k, tingkat t, semester s, presensiharian p WHERE p.idkelas = k.replid AND k.idtingkat = t.replid AND k.idtahunajaran = a.replid AND p.idsemester = s.replid AND s.replid = $semester AND k.replid = $kelas";  
$result = QueryDB($sql);	
$row = mysql_fetch_array($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Laporan Presensi Harian Siswa Per Kelas]</title>
</head>

<body>

<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr>
	<td align="left" valign="top" colspan="2">
<?php echo getHeader($row[departemen])?>
	
<center>
  <font size="4"><strong>LAPORAN PRESENSI HARIAN SISWA PER KELAS</strong></font><br />
 </center><br /><br />
<table>
<tr>
	<td width="25%"><strong>Departemen</strong></td>
    <td><strong>: <?php echo $row['departemen']?></strong></td>
</tr>
<tr>
	<td><strong>Tahun Ajaran</strong></td>
    <td><strong>: <?php echo $row['tahunajaran']?></strong></td>
</tr>
<tr>
	<td><strong>Semester</strong></td>
    <td><strong>: <?php echo $row['semester']?></strong></td>
</tr>
<!--<tr>
	<td><strong>Tingkat</strong></td>
    <td><strong>: <?php echo $row['tingkat']?></strong></td>
</tr>-->
<tr>
	<td><strong>Kelas</strong></td>
    <td><strong>: <?php echo $row['tingkat'].' - '.$row['kelas']?></strong></td>
</tr>
<tr>
	<td><strong>Periode Presensi</strong></td>
    <td><strong>: <?php echo format_tgl($tglawal).' s/d '. format_tgl($tglakhir) ?></strong></td>
</tr>
</table>
<br />
<?php 	OpenDb();
	$sql = "SELECT s.nis, s.nama, SUM(ph.hadir) as hadir, SUM(ph.ijin) as ijin, SUM(ph.sakit) as sakit, SUM(ph.alpa) as alpa, SUM(ph.cuti) as cuti, s.idkelas, s.aktif FROM siswa s LEFT JOIN (phsiswa ph INNER JOIN presensiharian p ON p.replid = ph.idpresensi) ON ph.nis = s.nis WHERE s.idkelas = '$kelas' AND p.idsemester = '$semester' AND (((p.tanggal1 BETWEEN '$tglawal' AND '$tglakhir') OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) OR (('$tglawal' BETWEEN p.tanggal1 AND p.tanggal2) OR ('$tglakhir' BETWEEN p.tanggal1 AND p.tanggal2))) GROUP BY s.nis ORDER BY $urut $urutan";
	
	$result = QueryDb($sql);
	$jum = mysql_num_rows($result);
	if ($jum > 0) { 
?>
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
   	<tr height="30" align="center">
    	<td width="5%" class="header">No</td>
		<td width="15%" class="header">N I S</td>
        <td width="*" class="header">Nama</td>
   		<td width="7%" class="header">Hadir</td>
		<td width="7%" class="header">Ijin</td>            
		<td width="7%" class="header">Sakit</td>
        <td width="7%" class="header">Alpa</td>     
        <td width="7%" class="header">Cuti</td>
    </tr>
<?php		
	$cnt = 0;
	while ($row = mysql_fetch_row($result)) { 
		$tanda = "";
		if ($row[8] == 0) 
			$tanda = "*";
	?>
    <tr height="25">    	
    	<td align="center"><?php echo ++$cnt?></td>
        <td align="center"><?php echo $row[0]?><?php echo $tanda?></td>
        <td><?php echo $row[1]?></td>
        <td align="center"><?php echo $row[2]?></td>
        <td align="center"><?php echo $row[3]?></td>    
        <td align="center"><?php echo $row[4]?></td>
        <td align="center"><?php echo $row[5]?></td>    
        <td align="center"><?php echo $row[6]?></td>
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>	
<?php 	} ?>
	</td>
</tr>
<tr>
	<td><?php echo "Ket: *Status siswa tidak aktif lagi"; ?>
    </td>
</tr>    
</table>
</body>
<script language="javascript">
window.print();
</script>

</html>