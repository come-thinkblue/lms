<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];	
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];	
if (isset($_REQUEST['bln1']))
	$bln1 = $_REQUEST['bln1'];	
if (isset($_REQUEST['th1']))
	$th1 = $_REQUEST['th1'];	
if (isset($_REQUEST['bln2']))
	$bln2 = $_REQUEST['bln2'];	
if (isset($_REQUEST['th2']))
	$th2 = $_REQUEST['th2'];	
	
$tglawal = "$th1-$bln1-1";
if (isset($_REQUEST['tglawal']))
	$tglawal = $_REQUEST['tglawal'];	

if ($bln2 == 4 || $bln2 == 6|| $bln2 == 9 || $bln2 == 11) 
	$n = 30;
else if ($bln2 == 2 && $th2 % 4 <> 0) 
	$n = 28;
else if ($bln2 == 2 && $th2 % 4 == 0) 
	$n = 29;
else 
	$n = 31;

$tglakhir = "$th2-$bln2-$n";
if (isset($_REQUEST['tglakhir']))
	$tglakhir = $_REQUEST['tglakhir'];	
	


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Statistik Harian Kehadiran Setiap Siswa</title>
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">
function cetak() {
	var tglawal = document.getElementById('tglawal').value;
	var tglakhir = document.getElementById('tglakhir').value;	
	var semester = document.getElementById('semester').value;
	var kelas = document.getElementById('kelas').value;	
		
	newWindow('statistik_hariansiswa_cetak.php?tglawal='+tglawal+'&tglakhir='+tglakhir+'&semester='+semester+'&kelas='+kelas, 'CetakStatistikKehadiranHarianSiswa','800','650','resizable=1,scrollbars=1,status=0,toolbar=0');
}
</script>
</head>

<body>
<input type="hidden" name="tglawal" id="tglawal" value="<?php echo $tglawal?>">
<input type="hidden" name="tglakhir" id="tglakhir" value="<?php echo $tglakhir?>">
<input type="hidden" name="semester" id="semester" value="<?php echo $semester?>">
<input type="hidden" name="kelas" id="kelas" value="<?php echo $kelas?>">

<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background-repeat:no-repeat; background-attachment:fixed">
<!-- TABLE UTAMA -->
<tr>
	<td>
    <?php 		
	OpenDb();
	$sql = "SELECT DISTINCT(s.nis), s.nama FROM presensiharian p, phsiswa ph, siswa s WHERE ph.nis = s.nis AND ph.idpresensi = p.replid AND p.idkelas = '$kelas' AND p.idsemester = '$semester' AND (((p.tanggal1 BETWEEN '$tglawal' AND '$tglakhir') OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) OR (('$tglawal' BETWEEN p.tanggal1 AND p.tanggal2) OR ('$tglakhir' BETWEEN p.tanggal1 AND p.tanggal2))) ORDER BY s.nama, p.tanggal1 ";	
	$result = QueryDb($sql);			 
	$field = mysql_num_fields($result);
	$jum = mysql_num_rows($result);
	
	if ($jum > 0) { 
	?> 
    	<table width="100%" border="0" align="center">
        <!-- TABLE LINK -->
        <tr>
            <td align="right"> 	
            <a href="#" onClick="document.location.reload()"><img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh</a>&nbsp;&nbsp;
            <a href="JavaScript:cetak()"><img src="../images/ico/print.png" border="0" onmouseover="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp;
            </td>
        </tr>
        </table>
        <br />
        <table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">	
    	<tr height="30" align="center">		
			<td width="5%" class="header">No</td>
		  	<td width="10%" class="header">N I S</td>
            <td width="15%" class="header">Nama</td>
            <td width="*" class="header"></td>
		</tr>
		<?php 
		$cnt = 0;
		while ($row = @mysql_fetch_row($result)) {		
    		
		?>	
        <tr height="25">        			
			<td align="center"><?php echo ++$cnt?></td>
			<td align="center"><?php echo $row[0]?></td>
            <td align="center"><?php echo $row[1]?></td>
            <!--<td align="center"><img src="statistik_harianbatang.php?semester=<?php echo $semester?>&kelas=<?php echo $kelas?>&bln1=<?php echo $bln1?>&bln2=<?php echo $bln2?>&th1=<?php echo $th1?>&th2=<?php echo $th2?>&nis=<?php echo $row[0]?>" />-->
            <td align="center"><img src="statistik_harianbatang.php?semester=<?php echo $semester?>&kelas=<?php echo $kelas?>&tglawal=<?php echo $tglawal?>&tglakhir=<?php echo $tglakhir?>&nis=<?php echo $row[0]?>" />
            </td>
    	</tr>
 	<?php		
		} 
		CloseDb();	?>
		</table>
		<script language='JavaScript'>
   			Tables('table', 1, 0);
		</script>
<?php 	} else { ?>

	 <table width="100%" border="0" align="center">          
	<tr>
		<td align="center" valign="middle" height="250">
    	<font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br />Tambah data presensi kelas di menu Presensi Harian atau <br />Presensi Pelajaran pada bagian Presensi.</b></font>
		</td>
	</tr>
	</table>
<?php	} ?>    </td>
</tr>

<!-- END OF TABLE UTAMA -->
</table>

</body>
</html>