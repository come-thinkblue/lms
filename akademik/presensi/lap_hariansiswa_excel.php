<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Nilai_Pelajaran.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$nis = $_REQUEST['nis'];
$tglawal = $_REQUEST['tglawal'];
$tglakhir = $_REQUEST['tglakhir'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];


OpenDb();
$sql = "SELECT nama FROM siswa WHERE nis='$nis'";   
$result = QueryDB($sql);	
$row = mysql_fetch_array($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Laporan Presensi Harian Siswa]</title>
<style type="text/css">
<!--
.style3 {font-family: 'Droid Sans', sans-serif; font-weight: bold; font-size: 12px; }
.style4 {color: #FFFFFF}
.style5 {font-family: Verdana}
.style8 {
	font-family: 'Droid Sans', sans-serif;
	font-size: 16px;
}
.style10 {font-size: 14px; font-weight: bold; }
-->
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0">
  <tr>
    <th scope="row" colspan="2"><span class="style8">Laporan Presensi Harian Siswa</span></th>
  </tr>
</table>
<br />
<table width="17%">
<tr>
	<td width="65%"><span class="style3">Siswa</span></td>
    <td width="35%" colspan="3"><span class="style3">: 
      <?php echo $nis.' - '.$row['nama']?>
    </span></td>
</tr>
<!--<tr>
	<td><strong>Nama</strong></td>
    <td><strong>: <?php echo $row['nama']?></strong></td>
</tr>-->
<tr>
	<td><span class="style3">Periode Presensi</span></td>
    <td  colspan="3"><span class="style3">: <?php echo format_tgl($tglawal).' s/d '. format_tgl($tglakhir) ?></span></td>
</tr>
</table>
<br />
<?php 	OpenDb();
	$sql = "SELECT DAY(p.tanggal1), MONTH(p.tanggal1), YEAR(p.tanggal1), DAY(p.tanggal2), MONTH(p.tanggal2), YEAR(p.tanggal2), ph.hadir, ph.ijin, ph.sakit, ph.alpa, ph.cuti, ph.keterangan, s.nama, m.semester, k.kelas FROM presensiharian p, phsiswa ph, siswa s, semester m, kelas k WHERE ph.idpresensi = p.replid AND ph.nis = s.nis AND ph.nis = '$nis' AND p.idsemester = m.replid AND p.idkelas = k.replid AND (((p.tanggal1 BETWEEN '$tglawal' AND '$tglakhir') OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) OR (('$tglawal' BETWEEN p.tanggal1 AND p.tanggal2) OR ('$tglakhir' BETWEEN p.tanggal1 AND p.tanggal2))) ORDER BY $urut $urutan ";
	
	$result = QueryDb($sql);
	$jum = mysql_num_rows($result);
	if ($jum > 0) { 
?>
	<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left">
  <tr height="30" align="center">
    	<td width="5%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">No</span></td>
    <td width="25%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Tanggal</span></td>
     <td width="8%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Semester</span></td>
    <td width="8%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Kelas</span></td>
    <td width="5%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Hadir</span></td>
    <td width="5%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Ijin</span></td>            
    <td width="5%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Sakit</span></td>
    <td width="5%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Alpa</span></td>
    <td width="5%" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Cuti</span></td>
    <td width="*" bgcolor="#CCCCCC" class="style5 style4 header"><span class="style10">Keterangan</span></td>      
    </tr>
<?php		
	$cnt = 0;
	$h=0;
	$i=0;
	$s=0;
	$a=0;
	$c=0;
	while ($row = mysql_fetch_row($result)) { ?>
    <tr height="25">    	
    	<td align="center"><?php echo ++$cnt?></td>
		<td align="center"><?php echo $row[0].' '.$bulan[$row[1]].' '.$row[2].' - '.$row[3].' '.$bulan[$row[4]].' '.$row[5]?></td>
        <td align="center"><?php echo $row[13]?></td>
        <td align="center"><?php echo $row[14]?></td>
        <td align="center"><?php echo $row[6]?></td>
		<td align="center"><?php echo $row[7]?></td>
       	<td align="center"><?php echo $row[8]?></td> 
        <td align="center"><?php echo $row[9]?></td>
        <td align="center"><?php echo $row[10]?></td>
       
        <td><?php echo $row[11]?></td>
    </tr>
<?php	
	$h+=$row[6];
	$i+=$row[7];
	$s+=$row[8];
	$a+=$row[9];
	$c+=$row[10];
	} 
	CloseDb() ?>
    <tr>	
		<td width="5%" colspan="4" align="right" bgcolor="#CCCCCC"><strong>Jumlah&nbsp;&nbsp;</strong></td>
   		<td width="5%" height="25" align="center"><?php echo $h?></td>
		<td width="5%" height="25" align="center"><?php echo $i?></td>            
		<td width="5%" height="25" align="center"><?php echo $s?></td>
        <td width="5%" height="25" align="center"><?php echo $a?></td>
        <td width="5%" height="25" align="center"><?php echo $c?></td>      
        <td width="*" bgcolor="#CCCCCC"></td>
    </tr>	
    <!-- END TABLE CONTENT -->
    </table>	
<?php 	} ?>


<script language="javascript">
window.print();
</script>

</html>