<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];	

$sql_ta = "SELECT * FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'";
$result_ta = QueryDb($sql_ta);
$row_ta = @mysql_fetch_array($result_ta);
$tglawal = $row_ta['tglmulai'];
$tglakhir = $row_ta['tglakhir'];
	
	$sql_get_nama="SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result_get_nama=QueryDb($sql_get_nama);
	$row_get_nama=@mysql_fetch_array($result_get_nama);
	
	$sql_get_kls="SELECT kelas FROM $g_db_akademik.kelas WHERE replid='$kelas'";
	$result_get_kls=QueryDb($sql_get_kls);
	$row_get_kls=@mysql_fetch_array($result_get_kls);

	$sql_get_tkt="SELECT tingkat FROM $g_db_akademik.tingkat WHERE replid='$tingkat'";
	$result_get_tkt=QueryDb($sql_get_tkt);
	$row_get_tkt=@mysql_fetch_array($result_get_tkt);
	
	$sql_get_sem="SELECT semester FROM $g_db_akademik.semester WHERE replid='$semester'";
	$result_get_sem=QueryDb($sql_get_sem);
	$row_get_sem=@mysql_fetch_array($result_get_sem);

$sql_get_w_kls="SELECT p.nama as namawalikelas, p.nip as nipwalikelas FROM $g_db_pegawai.pegawai p, $g_db_akademik.kelas k WHERE k.replid='$kelas' AND k.nipwali=p.nip";
	//echo $sql_get_w_kls;
$rslt_get_w_kls=QueryDb($sql_get_w_kls);
$row_get_w_kls=@mysql_fetch_array($rslt_get_w_kls);

//<<<<<<< HEAD
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//=======
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//>>>>>>> 5d938ab94f14792511a4c03227c91c635e5388bf
	//echo $sql_get_kepsek;
	$rslt_get_kepsek=QueryDb($sql_get_kepsek);
	$row_get_kepsek=@mysql_fetch_array($rslt_get_kepsek);

	//dasar penilaian
	$sql_dp = "SELECT DISTINCT d.dasarpenilaian, d.keterangan, d.info1
		  	    FROM infonap i, nap n, dasarpenilaian d
			   WHERE i.replid = n.idinfo AND n.nis = '$nis' 
			     AND i.idsemester = '$semester' 
			     AND i.idkelas = '$kelas'
			     AND n.idaturan = d.replid";
	
	//peminatan kelas
	$sql_pem = "SELECT keterangan FROM kelas WHERE replid='$kelas'";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Laporan Hasil Belajar</TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<style type="text/css">
<!--
.style1{
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF
}
.style6 {
	font-size: 12px;
	font-weight: bold;
}
.style13 {
	font-size: 14px;
	font-weight: bold;
}
.style14 {color: #FFFFFF}
tr, td{
	padding: 5px;
}
-->
@page {
      size: 8.5in 13in;  /* width height */
   }
@media print
{
	 #Header, #Footer { display: none !important; }
}
   
</style>
<script language="javascript" src="../script/tables.js"></script>
</HEAD>
<BODY>
<table width="780" border="0">
  <tr>
    <td><?php echo getHeader($departemen)?></td>
  </tr>
  <tr>
    <td>
<table width="100%" border="0">
  <tr>
    <td>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
    <td height="16" colspan="2" bgcolor="#FFFFFF"><div align="center" class="style13">DESKRIPSI PENGETAHUAN DAN KETRAMPILAN</div></td>
    </tr>
  <tr>
  	<td width="20%" height="20"><span >Nama Siswa</span></td>
	<td height="20"><span >:&nbsp;
	    <?php echo $row_get_nama[nama]?>
	</span></td>
  </tr>
  <tr>
    <td  height="20"><span >No. Induk / NISN 
    </span></td>
    <td  height="20"><span >:&nbsp;
        <?php echo $nis?>
    </span></td>
  </tr>
  <tr>
    
	<td height="20" ><span >Kelas / Semester</span></td>
    <td height="20"><span >:&nbsp;
    	<?php echo $row_get_tkt[tingkat]."-".$row_get_kls[kelas]." / ".$row_get_sem[semester];?>
    </span></td>
  </tr>
  <tr>
    <td height="20"><span >Tahun Pelajaran
    </span></td>
    <td height="20"><span >:&nbsp;
        <?php echo $row_ta[tahunajaran]?>
    </span></td>
  </tr>
</table>
</td>
  </tr>
 <tr>
	<td>
<?php	
	$res = QueryDb($sql_dp);
	$i = 0;
	while($row = mysql_fetch_row($res))
	{
		$aspekarr[$i++] = array($row[0], $row[1], $row[2]);
	} ?>  
	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
	  <tr>
		<td width="30px"><div align="center"><b>No</b></div></td>
		<td width="25%" align="center"><b>MATA PELAJARAN</b></td>
		<td width="10%"><div align="center"><b>ASPEK</b></div></td>
		<td><div align="center"><b>DESKRIPSI</b></div></td>
	  </tr>
	<?php	
	//kelompok A, B dan peminatan
		$sifat_k13 = array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)');
		$respem = QueryDb($sql_pem);
		$rowpem = mysql_fetch_array($respem);
		$peminatan = $rowpem['keterangan'];
		if($peminatan=='IIK'){
			$kel = array(0, 1, 4);
		}else if ($peminatan=='IIS') {
			$kel = array(0, 1, 3);
		}else{
			$kel = array(0, 1, 2);
		}

		
  foreach ($kel as $kelompok) {
	$sql = "SELECT pel.replid, pel.nama
	  FROM ujian uji, nilaiujian niluji, siswa sis, pelajaran pel 
	  WHERE uji.replid = niluji.idujian AND niluji.nis = sis.nis 
	  AND uji.idpelajaran = pel.replid AND uji.idsemester = $semester 
	  AND uji.idkelas = $kelas AND sis.nis = '$nis' 
	  AND pel.sifat_k13='$kelompok' GROUP BY pel.nama";
	$respel = QueryDb($sql);
	$hitung = mysql_num_rows($respel);
	if($hitung>0){
	echo 
		"<tr>
			<td colspan='4'><b>".$sifat_k13[$kelompok]."</b></td>
		</tr>";
	$no = 0;
	while($rowpel = mysql_fetch_row($respel))
	{
		$no++;
		$idpel = $rowpel[0];
		$nmpel = $rowpel[1];
		//select KKM
		$sql = "SELECT nilaimin FROM infonap WHERE idpelajaran = $idpel 
				AND idsemester = $semester AND idkelas = $kelas";
		$res = QueryDb($sql);
		$row = mysql_fetch_row($res);
		$nilaimin = $row[0];
				
		echo "<tr height='25'>";
		echo "<td align='center' rowspan='2'>$no</td>";
		echo "<td align='left' rowspan='2'>$nmpel</td>";
		
		for($i = 0; $i < count($aspekarr); $i++)
		{
			$na = "";
			$nh = "";
			$temp = array();
			$asp = $aspekarr[$i][2];
			$dp = $aspekarr[$i][0];
		
			//select nilai akhir
			$sql = "SELECT nilaiangka
						FROM infonap i, nap n, dasarpenilaian a 
						WHERE i.replid = n.idinfo AND n.nis = '$nis' AND i.idpelajaran = '$idpel' 
						AND i.idsemester = '$semester' AND i.idkelas = '$kelas'
						AND n.idaturan = a.replid AND a.dasarpenilaian = '$dp'";
			$res = QueryDb($sql);
			
			if (mysql_num_rows($res) > 0)
			{
				$row = mysql_fetch_row($res);
				$na = $row[0];
				if($na > $nilaimin)
					$KKM = 'Sudah Tercapai KKM';
				else
					$KKM = 'Belum Tercapai KKM';
			}
			//select id ujian
			$sql_ujian = "SELECT n.nilaiujian, r.deskripsi FROM ujian u, aturannhb a, nilaiujian n, rpp r
							WHERE a.dasarpenilaian = '$dp' AND a.replid=u.idaturan 
							AND u.idrpp = r.replid AND u.idpelajaran=r.idpelajaran
							AND u.idsemester=r.idsemester AND u.replid=n.idujian AND n.nis = $nis
							AND u.idkelas='$kelas' AND u.idsemester='$semester' 
							AND u.idpelajaran='$idpel' AND u.idrpp IS NOT NULL";

			$res_ujian = QueryDb($sql_ujian);
			
			while ($row_ujian = mysql_fetch_row($res_ujian)) {
				$temp[] =  $row_ujian[0].' '.$row_ujian[1];
			}
		
			
			echo 	"<td align='center'>$asp</td><td align='left'>".$KKM.", Menguasai ".
					substr(max($temp), 5).", Belum menguasai". substr(min($temp), 5) ."</td></tr>"; 
		} 
		echo "</tr>";
	}
  }
}

	//lintas minat statis
?>

	</table>
	</td>
  </tr>
  
 <?php 
 //TANDA TANGAN
 if($semester=='3'){?>
	<tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td width="33%"><div align="center">Orang Tua/Wali Peserta Didik</div></td>
    <td width="33%"><div align="center">Wali Kelas</div></td>
    <td width="33%"><div align="center">Kepala Madrasah</div></td>
  </tr>
  
  <tr>
    <td width="33%" height="70px"><div align="center"></div></td>
    <td width="33%" height="70"><div align="center"></div></td>
    <td width="33%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="33%" rowspan="2"><div align="center">(...................................................)</div></td>
    <td width="33%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
    <td width="33%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_kepsek[namakepsek]?></u>
    </div></td>
  </tr>
  <tr>
    <td style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
    <td width="33%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_kepsek[nipkepsek]?>
    </div></td>
  </tr>
  </table>
  </td>
  </tr>
  <?php }else {?>
  <tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td rowspan="2" width="50%" style="padding: 0"><div align="center">Orang Tua/Wali Siswa</div></td>
    <td width="50%" style="padding: 0"><div align="center">a.n Kepala Madrasah</div></td>
  </tr>
  <tr><td align="center" style="padding: 0">Wali Kelas</td></tr>
  <tr>
    <td width="50%" height="70px"><div align="center"></div></td>
    <td width="50%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="50%" rowspan="2"><div align="center">(.............................................)</div></td>
    <td width="50%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
  </tr>
  <?php }?>

  
</table>
    </td>
  </tr>
</table>

</BODY>
</HTML>
<?php
CloseDb();
?>