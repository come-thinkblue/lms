<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];	

//<<<<<<< HEAD
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//=======
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//>>>>>>> 5d938ab94f14792511a4c03227c91c635e5388bf
	//echo $sql_get_kepsek;
	$rslt_get_kepsek=QueryDb($sql_get_kepsek);
	$row_get_kepsek=@mysql_fetch_array($rslt_get_kepsek);

	$sql="SELECT * FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result=QueryDb($sql);
	$row=@mysql_fetch_array($result);
	if($row[kelamin]=='L')
		$gender = 'Laki Laki';
	else
		$gender = 'Perempuan';

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Identitas Peserta Didik</TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<style type="text/css">
<!--
.style1{
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF
}
.style6 {
	font-size: 12px;
	font-weight: bold;
}
.style13 {
	font-size: 14px;
	font-weight: bold;
}
.style14 {color: #FFFFFF}
tr, td{
	padding: 5px;
}
-->
@page {
      size: 8.5in 13in;  /* width height */
   }
@media print
{
	 #Header, #Footer { display: none !important; }
}
   
</style>
<script language="javascript" src="../script/tables.js"></script>
</HEAD>
<BODY>
<table width="780" border="0">
  <tr>
    <td><?php echo getHeader($departemen)?></td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
    <td height="16" colspan="3" bgcolor="#FFFFFF"><div align="center" class="style13">IDENTITAS PESERTA DIDIK</div></td>
    </tr>
 <!--identitas-->
 <tr>
  		<td>1</td>
  		<td>Nama Siswa (Lengkap)</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[nama]; ?></td>
  	</tr>
  	<tr>
  		<td>2</td>
  		<td>Nomor Induk</td>
  		<td>:&nbsp;&nbsp;<?php echo $nis; ?></td>
  	</tr>
  	<tr>
  		<td>3</td>
  		<td>NISN</td>
  		<td>:&nbsp;&nbsp;<?php echo $nis; ?></td>
  	</tr>
  	<tr>
  		<td>4</td>
  		<td>Tempat dan Tgl. Lahir</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[tmplahir].",".$row[tgllahir]; ?></td>
  	</tr>
  	<tr>
  		<td>5</td>
  		<td>Jenis Kelamin</td>
  		<td>:&nbsp;&nbsp;<?php echo $gender; ?></td>
  	</tr>
  	<tr>
  		<td>6</td>
  		<td>Agama</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[agama]; ?></td>
  	</tr>
  	<tr>
  		<td>7</td>
  		<td>Anank ke</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[anakke]; ?></td>
  	</tr>
  	<tr>
  		<td>8</td>
  		<td>Status dalam keluarga</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td>9</td>
  		<td>Alamat Peserta Didik</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[alamatsiswa]; ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[telponsiswa]; ?></td>
  	</tr>
  	<tr>
  		<td>10</td>
  		<td colspan="2">Diterima di Madrasah ini</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Di Kelas</td>
  		<td>:&nbsp;&nbsp;X (Sepuluh)</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Pada Tanggal</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>c. Semester</td>
  		<td>:&nbsp;&nbsp;<?php echo '1 (satu)'; ?></td>
  	</tr>
  	<tr>
  		<td>11</td>
  		<td colspan="2">Madrasah / Sekolah Asal</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Nama Sekolah</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Alamat</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td>12</td>
  		<td colspan="2">Ijasah SMP / MTs / Paket B</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Tahun</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Nomor</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td>13</td>
  		<td colspan="2">Nama Orang Tua</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Ayah</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[namaayah] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Ibu</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[namaibu] ?></td>
  	</tr>
  	<tr>
  		<td>14</td>
  		<td>Alamat Orang Tua</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[alamatortu] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[teleponortu] ?></td>
  	</tr>
  	<tr>
  		<td>15</td>
  		<td colspan="2">Pekerjaan Orang Tua</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Ayah</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[pekerjaanayah] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Ibu</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[pekerjaanibu] ?></td>
  	</tr>
  	<tr>
  		<td>16</td>
  		<td>Nama Wali</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[wali] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Alamat Wali</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Pekerjaan Wali</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
 <!--/identitas-->

  <tr>
  	<td rowspan="4" align="right" colspan="2">
  		<?php 
  			if($row['foto']!=null){
  				echo $row['foto'];
  			}else{
  				echo "Foto 3 x 4";
  			}
  		?>
  	</td>
    <td>
     <table width="50%" border="0">
  <tr>
    <td width="50%" style="padding: 0"><div align="center">a.n Kepala Madrasah</div></td>
  </tr>
  <tr><td align="center" style="padding: 0">Kepala Sekolah</td></tr>
  <tr>
    <td width="50%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_kepsek[namakepsek]?></u>
    </div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_kepsek[nipkepsek]?>
    </div></td>
  </tr>

  
</table>
    </td>
  </tr>
</table>

</BODY>
</HTML>
<?php
CloseDb();
?>