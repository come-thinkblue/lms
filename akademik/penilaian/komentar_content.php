<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/dpupdate.php');

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['komentar']))
	$komentar = CQ($_REQUEST['komentar']);
	
if (isset($_REQUEST['Simpan']))
{
	$komentar = $_REQUEST['komentar'];
	$predikat = $_REQUEST['predikat'];
	$sql_update = "UPDATE $g_db_akademik.komennap SET komentar='$komentar', predikat=$predikat WHERE replid='$_REQUEST[replid]'";
	$result_update = QueryDb($sql_update);
	if ($result_update)
	{	?>
		<script language="javascript" type="text/javascript">
			//alert ('Berhasil menyimpan komentar');
			document.location.href="komentar_content.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>";
		</script>
<?php	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<script language="javascript" type="text/javascript" src="../script/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "simple",
	});
	
	function OpenUploader() {
	    var addr = "UploaderMain.aspx";
	    newWindow(addr, 'Uploader','720','630','resizable=1,scrollbars=1,status=0,toolbar=0');
    }
	
	function simpan() {
	}
    
</script>
</HEAD>

<BODY>
<?php
$sql = "SELECT k.komentar, k.replid, k.predikat
		    FROM $g_db_akademik.komennap k, $g_db_akademik.infonap i 
		   WHERE k.nis='$nis' AND i.replid=k.idinfo AND i.idpelajaran='$pelajaran' 
		     AND i.idsemester='$semester' AND i.idkelas='$kelas'";
$result_get_comment = QueryDb($sql);
$row_get_comment = @mysql_fetch_row($result_get_comment);
$ada_get_comment = @mysql_num_rows($result_get_comment);
?>
<form name="frm_komentar" id="frm_komentar" action="komentar_content.php" method="POST">
<table width="100%" border="0" height="100%">
<input type="hidden" name="departemen" id="departemen" value="<?php echo $departemen?>">
<input type="hidden" name="semester" id="semester" value="<?php echo $semester?>">
<input type="hidden" name="tingkat" id="tingkat" value="<?php echo $tingkat?>">
<input type="hidden" name="tahunajaran" id="tahunajaran" value="<?php echo $tahunajaran?>">
<input type="hidden" name="pelajaran" id="pelajaran" value="<?php echo $pelajaran?>">
<input type="hidden" name="kelas" id="kelas" value="<?php echo $kelas?>">
<input type="hidden" name="nis" id="nis" value="<?php echo $nis?>">
<tr height="50">
	<td valign="bottom">
		<table width="100%" border="0">
		  <tr>
			<td width="6%"><strong>NIS</strong></td>
			<td width="1%"><strong>:</strong></td>
			<td width="33%"><strong>
			  <?php echo $nis?>
			</strong></td>
			<td width="60%" rowspan="2" valign="middle"><div align="left">
			  <input type="submit" class="but" style="width:150px;" title="Simpan Komentar" name="Simpan" value="Simpan">
			</div></td>
			</tr>
		  <tr>
			<td><strong>Nama</strong></td>
			<td><strong>:</strong></td>
			<td><strong>
			  <?php
			$sql_get_nama="SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'";
			$result_get_nama=QueryDb($sql_get_nama);
			$row_get_nama=@mysql_fetch_array($result_get_nama);
			echo $row_get_nama['nama'];
			?>
			</strong></td>
		  </tr>
		</table>
	</td>
</tr>
<tr height="120">
   <td valign="top" ><textarea name="komentar" id="komentar" style="width:100%;height:100px;">
	<?php echo $row_get_comment[0]?>
	</textarea><br>
   <strong>Predikat:&nbsp;</strong>
   <select name="predikat" id="predikat">
   <option value="4" <?php echo IntIsSelected(4, $row_get_comment[2])?>>Istimewa</option>
   <option value="3" <?php echo IntIsSelected(3, $row_get_comment[2])?>>Baik</option>
   <option value="2" <?php echo IntIsSelected(2, $row_get_comment[2])?>>Cukup</option>
   <option value="1" <?php echo IntIsSelected(1, $row_get_comment[2])?>>Kurang</option>
   <option value="0" <?php echo IntIsSelected(0, $row_get_comment[2])?>>Buruk</option>
   </select>
   <input type="hidden" name="replid" id="replid" value="<?php echo $row_get_comment[1]?>">
    </td>
  </tr>
  <tr>
	  <td align="left"  valign="top">
<?php	$sql = "SELECT DISTINCT a.dasarpenilaian, d.keterangan
		  	  FROM infonap i, nap n, aturannhb a, dasarpenilaian d
			 WHERE i.replid = n.idinfo AND n.nis = '$nis' 
			   AND i.idpelajaran = '$pelajaran' 
			   AND i.idsemester = '$semester' 
			   AND i.idkelas = '$kelas'
			   AND n.idaturan = a.replid 	   
			   AND a.dasarpenilaian = d.dasarpenilaian";
	$res = QueryDb($sql);
	$i = 0;
	while($row = mysql_fetch_row($res))
	{
		$aspek[$i++] = array($row[0], $row[1]);
	} ?>
    <strong>Informasi Nilai Rapor Siswa:</strong>
	<table border="1" class="tab" id="table" bordercolor="#000000">  
	<tr align="center" height="25">
<?php	for($i = 0; $i < count($aspek); $i++)
		echo "<td class='header' colspan=2 align='center' width='180'>" . $aspek[$i][1] . "</td>"; ?>
    </tr>
    <tr align="center" height="25">
<?php	for($i = 0; $i < count($aspek); $i++)
		echo "<td class='header' align='center' width='80'>Angka</td><td class='header' align='center' width='80'>Huruf</td>"; ?>
    </tr>
    <tr align="center" height="25">
<?php	for($i = 0; $i < count($aspek); $i++)
	{
		$na = "";
		$nh = "";
		
		$asp = $aspek[$i][0];
		
		$sql = "SELECT nilaiangka, nilaihuruf
				  FROM infonap i, nap n, aturannhb a 
				 WHERE i.replid = n.idinfo 
				   AND n.nis = '$nis' 
				   AND i.idpelajaran = '$pelajaran' 
				   AND i.idsemester = '$semester' 
				   AND i.idkelas = '$kelas'
				   AND n.idaturan = a.replid 	   
				   AND a.dasarpenilaian = '$asp'";
		$res = QueryDb($sql);
		if (mysql_num_rows($res) > 0)
		{
			$row = mysql_fetch_row($res);
			$na = $row[0];
			$nh = $row[1];
		}
		echo "<td align='center'>$na</td><td align='center'>$nh</td>"; 
	} ?>
    </tr>    
    </table>
 	</td>
  </tr>
</table>
</form>
</BODY>
</HTML>
<?php
CloseDb();
?>