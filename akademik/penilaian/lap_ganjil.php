<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$departemen = $_REQUEST['departemen'];
$pelajaran = $_REQUEST['pelajaran'];
$kelas = $_REQUEST['kelas'];
$nis = $_REQUEST['nis'];


OpenDb();
$sql = "SELECT s.replid, s.semester, p.nama FROM semester s, pelajaran p WHERE s.departemen = '$departemen' AND p.replid = $pelajaran AND p.departemen = '$departemen' AND s.semester='Ganjil'"; 
$result = QueryDb($sql);

$i = 0;
while ($row = @mysql_fetch_row($result)) {
	$sem[$i]= array($row[0],$row[1]);
	$pel = $row[2];
	$i++;
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td valign="top" background="" style="background-repeat:no-repeat; background-attachment:fixed">
	<table width="100%" border="0" height="100%">
  	<tr>
    	<td valign="top" colspan="2"><font size="2" color="#000000"><b>Pelajaran <?php echo $pel?></b></font></td>      	
  	</tr>
  	<tr>
    	<td valign="right"></td>
  	</tr>
  	<tr>
   	 	<td valign="top" align="right">     
  		
        <a href="JavaScript:cetak('<?php echo $sem[0][0]?>')"><img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak</a>&nbsp;&nbsp; 
      	</td>
	</tr>
    
	<?php	$sql = "SELECT j.replid, j.jenisujian FROM jenisujian j, ujian u WHERE j.idpelajaran = '$pelajaran' AND u.idjenis = j.replid GROUP BY j.jenisujian";
		$result = QueryDb($sql);
		while($row = @mysql_fetch_array($result)){			
	?>
   	<tr>
        <td colspan="2">
        <br /><strong> <?php echo $row['jenisujian']?> </strong>
        <br /><br />		
		<table border="1" width="100%" id="table" class="tab">
		<tr>		
			<td width="5" height="30" align="center" class="header">No</td>
			<td width="250" class="header" align="center" height="30">Tanggal</td>
            <td width="10" height="30" align="center" class="header">Nilai</td>
			<td width="400" class="header" align="center" height="30">Keterangan</td>
		</tr>
		<?php 	OpenDb();		
			$sql1 = "SELECT u.tanggal, n.nilaiujian, n.keterangan FROM ujian u, pelajaran p, nilaiujian n WHERE u.idpelajaran = p.replid AND u.idkelas = $kelas AND u.idpelajaran = $pelajaran AND u.idsemester = ".$sem[0][0]." AND u.idjenis = $row[replid] AND u.replid = n.idujian AND n.nis = '$nis' ORDER BY u.tanggal";
			$result1 = QueryDb($sql1);
			$sql2 = "SELECT AVG(n.nilaiujian) as rata FROM ujian u, pelajaran p, nilaiujian n WHERE u.idpelajaran = p.replid AND u.idkelas = $kelas AND u.idpelajaran = $pelajaran AND u.idsemester = ".$sem[0][0]." AND u.idjenis = $row[replid] AND u.replid = n.idujian AND n.nis = '$nis' ";
			$result2 = QueryDb($sql2);
			$row2 = @mysql_fetch_array($result2);
			$rata = $row2[rata];

			/*
			$sql3 = "SELECT nau.nilaiAU as nilaiAU,nau.keterangan as keterangan  FROM ujian u, pelajaran p, nilaiujian n, nau nau WHERE u.idpelajaran = p.replid AND u.idkelas = $kelas AND u.idpelajaran = $pelajaran AND u.idsemester = ".$sem[0][0]." AND u.idjenis = $row[replid] AND u.replid = n.idujian AND n.nis = '$nis' AND nau.idjenis=$row[replid] AND nau.idpelajaran = $pelajaran AND nau.idsemester = ".$sem[0][0];
			$result3 = QueryDb($sql3);
			$row3 = @mysql_fetch_array($result3);
			$nilaiAU = $row3[nilaiAU];			
			*/
			$cnt = 1;
			if (@mysql_num_rows($result1)>0){
			while($row1 = @mysql_fetch_array($result1)){			
        ?>
        <tr>        			
			<td width="5" height="25" align="center"><?php echo $cnt?></td>
			<td width="250" height="25"><?php echo format_tgl($row1[0])?></td>
            <td width="10" height="25" align="center"><?php echo $row1[1]?></td>
            <td height="25"><?php echo $row1[2]?></td>            
		</tr>	
        <?php	$cnt++;
			} ?>
		<tr>        			
			<td colspan="2" height="25" align="center"><strong>Nilai rata rata</strong></td>
			<td width="10" height="25" align="center"><?php echo round($rata,2)?></td>
            <td height="25">&nbsp;</td>            
		</tr><!--
		<tr>        			
			<td colspan="2" height="25" align="center"><strong>Nilai Akhir</strong></td>
			<td width="8" height="25" align="center"><?php echo $nilaiAU?></td>
            <td height="25"><?php echo $row3[keterangan]?></td>           
		</tr>-->
		<?php } else { ?>
		<tr>        			
			<td colspan="4" height="25" align="center">Tidak ada nilai</td>
		</tr>
		<?php }
			?>
		</table>
		<script language="javascript">
			Tables('table', 1, 0);
		</script>
		</td>	
	</tr>
    <?php } ?> 
    
    <!-- END TABLE CONTENT -->
    </table>
    </td>
</tr>
</table>