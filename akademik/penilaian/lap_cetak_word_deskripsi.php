<?php
/**[N]**
 * LMS SMA Negeri 1 Malang
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2013 SMA Negeri1 Malang
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];

$sql_ta="SELECT * FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'";
$result_ta=QueryDb($sql_ta);
$row_ta=@mysql_fetch_array($result_ta);
$tglawal=$row_ta['tglmulai'];
$tglakhir=$row_ta['tglakhir'];

	//alamat sekolah
	$sql_alamat = "SELECT alamat1 FROM identitas WHERE departemen='$departemen'";
	$res_alamat = QueryDb($sql_alamat);
	$row_alamat =@mysql_fetch_array($res_alamat);
	
//echo "Dep=".$departemen.", Tkt=".$tingkat.", Kls=".$kelas.", Pelajaran=".$pelajaran.", Semester=".$semester.", Thn AJaran=".$tahun;
	$sql_get_nama="SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result_get_nama=QueryDb($sql_get_nama);
	$row_get_nama=@mysql_fetch_array($result_get_nama);
	
	$sql_get_kls="SELECT kelas FROM $g_db_akademik.kelas WHERE replid='$kelas'";
	$result_get_kls=QueryDb($sql_get_kls);
	$row_get_kls=@mysql_fetch_array($result_get_kls);

	$sql_get_tkt="SELECT tingkat FROM $g_db_akademik.tingkat WHERE replid='$tingkat'";
	$result_get_tkt=QueryDb($sql_get_tkt);
	$row_get_tkt=@mysql_fetch_array($result_get_tkt);
	
	$sql_get_sem="SELECT semester FROM $g_db_akademik.semester WHERE replid='$semester'";
	$result_get_sem=QueryDb($sql_get_sem);
	$row_get_sem=@mysql_fetch_array($result_get_sem);
	
	$sql_get_w_kls="SELECT p.nama as namawalikelas, p.nip as nipwalikelas FROM $g_db_pegawai.pegawai p, $g_db_akademik.kelas k WHERE k.replid='$kelas' AND k.nipwali=p.nip";
	//echo $sql_get_w_kls;
	$rslt_get_w_kls=QueryDb($sql_get_w_kls);
	$row_get_w_kls=@mysql_fetch_array($rslt_get_w_kls);
	
	$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
	//echo $sql_get_kepsek;
	$rslt_get_kepsek=QueryDb($sql_get_kepsek);
	$row_get_kepsek=@mysql_fetch_array($rslt_get_kepsek);
	$namafile="Siswa_".$nis;

	//dasar penilaian
	$sql_dp = "SELECT DISTINCT d.dasarpenilaian, d.keterangan, d.info1
		  	    FROM infonap i, nap n, dasarpenilaian d
			   WHERE i.replid = n.idinfo AND n.nis = '$nis' 
			     AND i.idsemester = '$semester' 
			     AND i.idkelas = '$kelas'
			     AND n.idaturan = d.replid";
	
	//peminatan kelas
	$sql_pem = "SELECT keterangan FROM kelas WHERE replid='$kelas'";
	
/**/
header('Content-Type: application/vnd.ms-word'); //IE and Opera  
header('Content-Type: application/w-msword'); // Other browsers  
header('Content-Disposition: attachment; filename=Deskripsi_Rapor_'.$namafile.'.doc');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="Doc1_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-06-16T08:31:00Z</o:Created>
  <o:LastSaved>2008-06-16T08:31:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
 <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
@page Section2
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section2
	{page:Section2;}
@page Section3
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section3
	{page:Section3;}
@page Section4
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section4
	{page:Section4;}
.style1 {
	color: #000000;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
}
.style2 {
	font-size: 14px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
}
.style5 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; }
.style13 {color: #000000}
.style14 {color: #FFFFFF}
.style17 {color: #FFFFFF; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; }
.style20 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold; font-size: 12px; }
.style21 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style22 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style24 {
	font-size: 12;
	color: #FFFFFF;
}
.style27 {color: #FFFFFF; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12; }
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>

<body lang=EN-US style='tab-interval:36.0pt'>

<div class=Section1>

<?php echo getHeader($departemen)?>
<table width="100%" border="0">
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
    <td height="20" width="20%"><span class="style5">Nama</span></td>
    <td height="20" width="30%"><span class="style5">:&nbsp;
        <?php echo $row_get_nama[nama]?>
    </span></td>
  </tr>
  <tr>
    <td height="20"><span class="style5">No. Induk / NISN</span></td>
    <td height="20"><span class="style5">:&nbsp;
		<?php echo $nis?>
    </span></td>
  </tr>
  <tr>
  	<td height="20"><span class="style5">Kelas/ Semester</span></td>
    <td height="20"><span class="style5">
		:&nbsp;<?php echo $row_get_tkt[tingkat]."-".$row_get_kls[kelas]." / ".$row_get_sem[semester];?>
    </span></td>
  </tr>
  <tr>
    <td width="6%" height="20"><span class="style5">Tahun Pelajaran
    </span></td>
    <td width="93%" height="20"><span class="style5">:&nbsp;
        <?php echo $row_ta[tahunajaran]?>
    </span></td>
  </tr>
</table>
</td>
  </tr>
<br>
  <tr>
	<td>
<?php	
	$res = QueryDb($sql_dp);
	$i = 0;
	while($row = mysql_fetch_row($res))
	{
		$aspekarr[$i++] = array($row[0], $row[1], $row[2]);
	} ?>  
	<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
	  <tr>
		<td width="30px"><div align="center"><b>No</b></div></td>
		<td width="25%" align="center"><b>MATA PELAJARAN</b></td>
		<td width="10%"><div align="center"><b>ASPEK</b></div></td>
		<td><div align="center"><b>DESKRIPSI</b></div></td>
	  </tr>
	<?php	
	//kelompok A, B dan peminatan
		$sifat_k13 = array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)');
		$respem = QueryDb($sql_pem);
		$rowpem = mysql_fetch_array($respem);
		$peminatan = $rowpem['keterangan'];
		if($peminatan=='IIK'){
			$kel = array(0, 1, 4);
		}else if ($peminatan=='IIS') {
			$kel = array(0, 1, 3);
		}else{
			$kel = array(0, 1, 2);
		}

		
  foreach ($kel as $kelompok) {
	$sql = "SELECT pel.replid, pel.nama
	  FROM ujian uji, nilaiujian niluji, siswa sis, pelajaran pel 
	  WHERE uji.replid = niluji.idujian AND niluji.nis = sis.nis 
	  AND uji.idpelajaran = pel.replid AND uji.idsemester = $semester 
	  AND uji.idkelas = $kelas AND sis.nis = '$nis' 
	  AND pel.sifat_k13='$kelompok' GROUP BY pel.nama";
	$respel = QueryDb($sql);
	$hitung = mysql_num_rows($respel);
	if($hitung>0){
	echo 
		"<tr>
			<td colspan='4'><b>".$sifat_k13[$kelompok]."</b></td>
		</tr>";
	$no = 0;
	while($rowpel = mysql_fetch_row($respel))
	{
		$no++;
		$idpel = $rowpel[0];
		$nmpel = $rowpel[1];
		//select KKM
		$sql = "SELECT nilaimin FROM infonap WHERE idpelajaran = $idpel 
				AND idsemester = $semester AND idkelas = $kelas";
		$res = QueryDb($sql);
		$row = mysql_fetch_row($res);
		$nilaimin = $row[0];
				
		echo "<tr height='25'>";
		echo "<td align='center' rowspan='2'>$no</td>";
		echo "<td align='left' rowspan='2'>$nmpel</td>";
		
		for($i = 0; $i < count($aspekarr); $i++)
		{
			$na = "";
			$nh = "";
			$temp = array();
			$asp = $aspekarr[$i][2];
			$dp = $aspekarr[$i][0];
		
			//select nilai akhir
			$sql = "SELECT nilaiangka
						FROM infonap i, nap n, dasarpenilaian a 
						WHERE i.replid = n.idinfo AND n.nis = '$nis' AND i.idpelajaran = '$idpel' 
						AND i.idsemester = '$semester' AND i.idkelas = '$kelas'
						AND n.idaturan = a.replid AND a.dasarpenilaian = '$dp'";
			$res = QueryDb($sql);
			
			if (mysql_num_rows($res) > 0)
			{
				$row = mysql_fetch_row($res);
				$na = $row[0];
				if($na > $nilaimin)
					$KKM = 'Sudah Tercapai KKM';
				else
					$KKM = 'Belum Tercapai KKM';
			}
			//select id ujian
			$sql_ujian = "SELECT n.nilaiujian, r.deskripsi FROM ujian u, aturannhb a, nilaiujian n, rpp r
							WHERE a.dasarpenilaian = '$dp' AND a.replid=u.idaturan 
							AND u.idrpp = r.replid AND u.idpelajaran=r.idpelajaran
							AND u.idsemester=r.idsemester AND u.replid=n.idujian AND n.nis = $nis
							AND u.idkelas='$kelas' AND u.idsemester='$semester' 
							AND u.idpelajaran='$idpel' AND u.idrpp IS NOT NULL";

			$res_ujian = QueryDb($sql_ujian);
			
			while ($row_ujian = mysql_fetch_row($res_ujian)) {
				$temp[] =  $row_ujian[0].' '.$row_ujian[1];
			}
		
			
			echo 	"<td align='center'>$asp</td><td align='left'>".$KKM.", Menguasai ".
					substr(max($temp), 5).", Belum menguasai". substr(min($temp), 5) ."</td></tr>"; 
		} 
		echo "</tr>";
	}
  }
}

	//lintas minat statis
?>

	</table>
	</td>
  </tr>
<br>
<?php if($semester=='3'){?>
	<tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td width="33%"><div align="center">Orang Tua/Wali Peserta Didik</div></td>
    <td width="33%"><div align="center">Wali Kelas</div></td>
    <td width="33%"><div align="center">Kepala Madrasah</div></td>
  </tr>
  
  <tr>
    <td width="33%" height="70px"><div align="center"></div></td>
    <td width="33%" height="70"><div align="center"></div></td>
    <td width="33%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="33%" rowspan="2"><div align="center">(.......................................)</div></td>
    <td width="33%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
    <td width="33%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_kepsek[namakepsek]?></u>
    </div></td>
  </tr>
  <tr>
    <td style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
    <td width="33%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_kepsek[nipkepsek]?>
    </div></td>
  </tr>
  </table>
  </td>
  </tr>
  <?php }else {?>
  <tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td rowspan="2" width="50%" style="padding: 0"><div align="center">Orang Tua/Wali Siswa</div></td>
    <td width="50%" style="padding: 0"><div align="center">a.n Kepala Madrasah</div></td>
  </tr>
  <tr><td align="center" style="padding: 0">Wali Kelas</td></tr>
  <tr>
    <td width="50%" height="70px"><div align="center"></div></td>
    <td width="50%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="50%" rowspan="2"><div align="center">(.............................................)</div></td>
    <td width="50%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
  </tr>
  <?php }?>

  
</table>

</div>

<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
mso-break-type:section-break'>
</span>

</body>

</html>