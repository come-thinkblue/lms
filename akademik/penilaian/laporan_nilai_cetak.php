<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];	

$sql_ta = "SELECT * FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'";
$result_ta = QueryDb($sql_ta);
$row_ta = @mysql_fetch_array($result_ta);
$tglawal = $row_ta['tglmulai'];
$tglakhir = $row_ta['tglakhir'];

	$sql_get_nama="SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result_get_nama=QueryDb($sql_get_nama);
	$row_get_nama=@mysql_fetch_array($result_get_nama);
	
	$sql_get_kls="SELECT kelas FROM $g_db_akademik.kelas WHERE replid='$kelas'";
	$result_get_kls=QueryDb($sql_get_kls);
	$row_get_kls=@mysql_fetch_array($result_get_kls);

	$sql_get_tkt="SELECT tingkat FROM $g_db_akademik.tingkat WHERE replid='$tingkat'";
	$result_get_tkt=QueryDb($sql_get_tkt);
	$row_get_tkt=@mysql_fetch_array($result_get_tkt);
	
	$sql_get_sem="SELECT semester FROM $g_db_akademik.semester WHERE replid='$semester'";
	$result_get_sem=QueryDb($sql_get_sem);
	$row_get_sem=@mysql_fetch_array($result_get_sem);

$sql_get_w_kls="SELECT p.nama as namawalikelas, p.nip as nipwalikelas FROM $g_db_pegawai.pegawai p, $g_db_akademik.kelas k WHERE k.replid='$kelas' AND k.nipwali=p.nip";
	//echo $sql_get_w_kls;
$rslt_get_w_kls=QueryDb($sql_get_w_kls);
$row_get_w_kls=@mysql_fetch_array($rslt_get_w_kls);

//<<<<<<< HEAD
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//=======
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//>>>>>>> 5d938ab94f14792511a4c03227c91c635e5388bf
	//echo $sql_get_kepsek;
	$rslt_get_kepsek=QueryDb($sql_get_kepsek);
	$row_get_kepsek=@mysql_fetch_array($rslt_get_kepsek);

	//dasar penilaian
	$sql_dp = "SELECT DISTINCT d.dasarpenilaian, d.keterangan, d.info1
		  	    FROM infonap i, nap n, dasarpenilaian d
			   WHERE i.replid = n.idinfo AND n.nis = '$nis' 
			     AND i.idsemester = '$semester' 
			     AND i.idkelas = '$kelas'
			     AND n.idaturan = d.replid";
	
	//peminatan kelas
	$sql_pem = "SELECT keterangan FROM kelas WHERE replid='$kelas'";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Laporan Hasil Belajar</TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<style type="text/css">
<!--
.style1{
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF
}
.style6 {
	font-size: 12px;
	font-weight: bold;
}
.style13 {
	font-size: 14px;
	font-weight: bold;
}
.style14 {color: #FFFFFF}
tr, td{
	padding: 5px;
}
-->
@page {
      size: 8.5in 13in;  /* width height */
   }
@media print
{
	 #Header, #Footer { display: none !important; }
}
   
</style>
<script language="javascript" src="../script/tables.js"></script>
</HEAD>
<BODY>
<table width="780" border="0">
  <tr>
    <td><?php echo getHeader($departemen)?></td>
  </tr>
  <tr>
    <td>
<table width="100%" border="0">
  <tr>
    <td>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
    <td height="16" colspan="4" bgcolor="#FFFFFF"><div align="center" class="style13">LAPORAN
        HASIL BELAJAR</div></td>
    </tr>
  
  <tr>
    <td width='100px' height="20"><span >Nama Siswa</span></td>
	<td height="20"><span >:&nbsp;
	    <?php echo $row_get_nama[nama]?>
	</span></td>
	<td width='100px' height="20" ><span >Kelas</span></td>
    <td height="20"><span >:&nbsp;
    <?php echo $row_get_tkt[tingkat]."-".$row_get_kls[kelas];?>
    </span></td>
  </tr>
  <tr>
    <td  height="20"><span >No. Induk
    </span></td>
    <td  height="20"><span >:&nbsp;
        <?php echo $nis?>
    </span></td>
	<td height="20"><span >Semester</span></td>
    <td height="20"><span >
		:&nbsp;<?php echo $row_get_sem[semester];?>
    </span></td>
  </tr>
  <tr>
    <td  height="20"><span >NISN 
    </span></td>
    <td  height="20"><span >:&nbsp;
        <?php echo $nis?>
    </span></td>
    <td height="20"><span >Tahun Pelajaran
    </span></td>
    <td height="20"><span >:&nbsp;
        <?php echo $row_ta[tahunajaran]?>
    </span></td>
  </tr>
</table>
</td>
  </tr>
  <tr>
  	<td><br><h2><strong>A. Sikap</strong></h2></td>
  </tr>
  <tr>
  	<td><strong>1. SIkap Spiritual</strong></td>
  </tr>
  <?php 
  	//idaturan
  $sql= "SELECT m.keterangan, m.info1 FROM sas s, master_sikap m 
  			WHERE s.nis='$nis' AND s.idsikap=m.replid AND s.idaturan=4
  			AND idkelas='$kelas' AND idsemester='$semester'";
  $res = QueryDb($sql);
  $row = @mysql_fetch_array($res);
  ?>
  <tr><td>
  <table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="100px"><div align="center"><b>PREDIKAT</b></div></td>
			<td align='center'><b>DESKRIPSI</b></td>
		</tr>
		<tr>
			<td align='center'><?php echo $row['info1'] ?></td>
			<td><?php echo $row['keterangan'] ?></td>
		</tr>
	</table>
	</td></tr>
   <tr>
  	<td><br><strong>2. SIkap Sosial</strong></td>
  </tr>
  <?php 
  	//idaturan
  $sql= "SELECT m.keterangan, m.info1 FROM sas s, master_sikap m 
  			WHERE s.nis='$nis' AND s.idsikap=m.replid AND s.idaturan=5
  			AND idkelas='$kelas' AND idsemester='$semester'";
			//echo $sql;
  $res = QueryDb($sql);
  $row = @mysql_fetch_array($res);
  ?>
  <tr><td>
  <table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="100px"><div align="center"><b>PREDIKAT</b></div></td>
			<td align='center'><b>DESKRIPSI</b></td>
		</tr>
		<tr>
			<td align='center'><?php echo $row['info1'] ?></td>
			<td><?php echo $row['keterangan'] ?></td>
		</tr>
	</table>
	</td></tr>

  <tr>
  	<td><br><h2><strong>B. Pengetahuan dan Ketrampilan</strong></h2></td>
  </tr>
  <tr>
	<td>
<?php	
	$res = QueryDb($sql_dp);
	$i = 0;
	while($row = mysql_fetch_row($res))
	{
		$aspekarr[$i++] = array($row[0], $row[1], $row[2]);
	} ?>  
	<table width="100%" border="1" class="tab" id="table" bordercolor="#000000">
	<tr>
		<td width="18%"  class='header'  rowspan="2" ><div align="center">Pelajaran</div></td>
		<td width="7%"   class='header' rowspan="2" ><div align="center">KKM</div></td>
<?php		for($i = 0; $i < count($aspekarr); $i++)
			echo "<td  colspan='3' class='header'  align='center' width='18%'>" . $aspekarr[$i][2] . "</td>"; ?>
  	</tr>
	<tr>
<?php	for($i = 0; $i < count($aspekarr); $i++)
		echo "<td class='header' align='center' width='7%'>Angka</td>
			   <td class='header' align='center' width='7%'>Predikat</td>
				<td class='header' align='center' width='20%'>Deskripsi</td>"; ?>   
   </tr>
   <?php	
	//kelompok A, B dan peminatan
		$sifat_k13 = array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok Peminatan', 'Kelompok Peminatan', 'Kelompok Peminatan');
		$respem = QueryDb($sql_pem);
		$rowpem = mysql_fetch_array($respem);
		$peminatan = $rowpem['keterangan'];
		if($peminatan=='IIK'){
			$kel = array(0, 1, 4);
		}else if ($peminatan=='IIS') {
			$kel = array(0, 1, 3);
		}else{
			$kel = array(0, 1, 2);
		}

		
  foreach ($kel as $kelompok) {
	$sql = "SELECT pel.replid, pel.nama
	  FROM ujian uji, nilaiujian niluji, siswa sis, pelajaran pel 
	  WHERE uji.replid = niluji.idujian AND niluji.nis = sis.nis 
	  AND uji.idpelajaran = pel.replid AND uji.idsemester = $semester 
	  AND uji.idkelas = $kelas AND sis.nis = '$nis' 
	  AND pel.sifat_k13='$kelompok' GROUP BY pel.nama";
	$respel = QueryDb($sql);
	$hitung = mysql_num_rows($respel);
	if($hitung>0){
	echo 
		"<tr>
			<td colspan='8'><b>".$sifat_k13[$kelompok]."</b></td>
		</tr>";
	while($rowpel = mysql_fetch_row($respel))
	{
		$idpel = $rowpel[0];
		$nmpel = $rowpel[1];
		
		$sql = "SELECT nilaimin 
					 FROM infonap
					WHERE idpelajaran = $idpel
					  AND idsemester = $semester
				     AND idkelas = $kelas";
		$res = QueryDb($sql);
		$row = mysql_fetch_row($res);
		$nilaimin = $row[0];
				
		echo "<tr height='25'>";
		echo "<td align='left' >$nmpel</td>";
		echo "<td align='center'>$nilaimin</td>";
		
		for($i = 0; $i < count($aspekarr); $i++)
		{
			$na = "";
			$nh = "";
			$asp = $aspekarr[$i][0];
		
			$sql = "SELECT nilaiangka, nilaihuruf, idpelajaran
						 FROM infonap i, nap n, dasarpenilaian a 
						WHERE i.replid = n.idinfo 
						  AND n.nis = '$nis' 
						  AND i.idpelajaran = '$idpel' 
						  AND i.idsemester = '$semester' 
						  AND i.idkelas = '$kelas'
						  AND n.idaturan = a.replid 	   
						  AND a.dasarpenilaian = '$asp'";
			$res = QueryDb($sql);
			
			$cntpel_komentar = 1;
			if (mysql_num_rows($res) > 0)
			{
				$row = mysql_fetch_row($res);
				$na = $row[0];
				$nh = $row[1];

				//komentar
				/**$sql = "SELECT k.komentar 
		          		FROM $g_db_akademik.komennap k, $g_db_akademik.infonap i 
				 		WHERE k.nis='$nis' AND i.idpelajaran='$row[2]' AND i.replid=k.idinfo 
				   		AND i.idsemester='$semester' AND i.idkelas='$kelas'";
				$res2 = QueryDb($sql);
				$row2 = @mysql_fetch_row($res2);**/
			}
			//$say = $NTT->Convert($na);
			echo "<td align='center'>$na</td><td align='center'>$nh</td><td align='left'>Terlampir</td>"; 
		} 
		echo "</tr>";
	}
  }
}

	//lintas minat statis

?>
	</table>
	<script language='JavaScript'>
	   	Tables('table', 1, 0);
	</script>
	</td>
  </tr>

  <br>
  <tr>
  	<td>Tabel interval predikat berdasarkan KKM</td>
  </tr>
  <tr>
  	<td>
  	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="100px" rowspan="2"><div align="center"><b>KKM</b></div></td>
			<td colspan="4" align="center"><b> PREDIKAT </b></td>
		</tr>
		<tr>
			<td align='center'><b>D = Kurang</b></td>
			<td align='center'><b>C = Cukup</b></td>
			<td align='center'><b>B = Baik</b></td>
			<td align='center'><b>A = Sangat Baik</b></td>
		</tr>
		<tr>
			<td align='center'>60</td>
			<td align='center'>< 60</td>
			<td align='center'>60</td>
			<td align='center'>>60 - <=80</td>
			<td align='center'>>80 - <=100</td>
		</tr>
		<tr>
			<td align='center'>70</td>
			<td align='center'>< 70</td>
			<td align='center'>70</td>
			<td align='center'>>70 - <=85</td>
			<td align='center'>>85 - <=100</td>
		</tr>
		<tr>
			<td align='center'>80</td>
			<td align='center'>< 80</td>
			<td align='center'>80</td>
			<td align='center'>>80 - <=90</td>
			<td align='center'>>90 - <=100</td>
		</tr>
	</table>
  	</td>
  </tr>


  <tr>
  	<td><br><h2><strong>C. Ekstra Kurikuler</strong></h2></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="30px"><div align="center"><b>No</b></div></td>
			<td><b> Ekstra Kurikuler</b></td>
			<td><div align="center"><b>Keterangan dalam Kegiatan</b></div></td>
		</tr>
		<tr>
			<td align='center'>1</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>2</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>3</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>4</td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</td>
  </tr>
  
  <tr>
  	<td><br><h2><strong>D. Prestasi</strong></h2></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="30px"><div align="center"><b>No</b></div></td>
			<td align='center'><b> Jenis Prestasi</b></td>
			<td><div align="center"><b>Keterangan dalam Kegiatan</b></div></td>
		</tr>
		<tr>
			<td align='center'>1</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>2</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>3</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>4</td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</td>
  </tr>
  
  <?php
  if ($harian!="false"){
  ?>
  <tr>
  	<td><br><h2><strong>E. Ketidakhadiran</strong></h2></td>
  </tr>
  <tr>
    <td>
	<?php


	 $sql_harian = "SELECT SUM(ph.hadir) as hadir, SUM(ph.ijin) as ijin, SUM(ph.sakit) as sakit, SUM(ph.cuti) as cuti, SUM(ph.alpa) as alpa, SUM(ph.hadir+ph.sakit+ph.ijin+ph.alpa+ph.cuti) as tot ".
			"FROM presensiharian p, phsiswa ph, siswa s ".
			"WHERE ph.idpresensi = p.replid ".
			"AND ph.nis = s.nis ".
			"AND ph.nis = '$nis' ".
			"AND ((p.tanggal1 ".
			"BETWEEN '$tglawal' ".
			"AND '$tglakhir') ".
			"OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) ".
			"ORDER BY p.tanggal1"; ;
	  ?>
	<!-- Content Presensi disini -->
	<table width="30%" border="1" class="tab" id="table"  bordercolor="#000000">
  <!-- Ambil pelajaran per departemen-->
	<?php
	$result_harian=QueryDb($sql_harian);
	$row_harian=@mysql_fetch_array($result_harian);
	$hadir=$row_harian['hadir'];
	$sakit=$row_harian['sakit'];
	$ijin=$row_harian['ijin'];
	$alpa=$row_harian['alpa'];
	$cuti=$row_harian['cuti'];
	$all=$row_harian['tot'];
	if ($hadir!=0 && $all !=0)
	$p_hadir=$hadir/$all*100;
	
	if ($sakit!=0 && $all !=0)
	$p_sakit=$sakit/$all*100;
	
	if ($ijin!=0 && $all !=0)
	$p_ijin=$ijin/$all*100;
	
	if ($alpa!=0 && $all !=0)
	$p_alpa=$alpa/$all*100;
	
	if ($cuti!=0 && $all !=0)
	$p_cuti=$cuti/$all*100;
	?>
	<tr>
	  <td height="25" bgcolor="#FFFFFF" colspan=3><b>Ketidakhadiran</b></td>
	</tr>
	<tr>
		<td height="25" bgcolor="#FFFFCC"><div align="center">1</div></td>
		<td height="25" bgcolor="#FFFFCC">Sakit</div></td>
		<td height="25" bgcolor="#FFFFCC"><div align="center">
      	<?php echo $sakit?> hari
   		</div></td>
	</tr>
	<tr>
	<td height="25" bgcolor="#FFFFFF"><div align="center">2</div></td>
		<td height="25" bgcolor="#FFFFFF">Ijin</div></td>
		<td height="25" bgcolor="#FFFFFF"><div align="center">
      	<?php echo $ijin?> hari
    	</div></td>
	</tr>
	<tr>
	<td height="25" bgcolor="#FFFFCC"><div align="center">3</div></td>
		<td height="25" bgcolor="#FFFFCC">Tanpa Keterangan</td>
		<td height="25" bgcolor="#FFFFCC"><div align="center">
      	<?php echo $alpa?> hari
    	</div></td>
	</tr>
	<!--<tr>
		<td height="25" bgcolor="#FFFFFF"><div align="center">Cuti</div></td>
		<td height="25" bgcolor="#FFFFFF"><div align="center">
      	<?php echo $cuti?>
      	</div></td>
	</tr>-->
    
</table>
<script language='JavaScript'>
	   	Tables('table', 1, 0);
</script>
    </td>
  </tr>
  
  
  
  
 <?php
 }?>
 <tr>
  	<td><br><h2><strong>F. Catatan Wali Kelas</strong></h2></td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="1" class="tab" id="table"  height='75px' bordercolor="#000000">
			<tr><td></td></tr>
		</table>
	</td>
 </tr>
 
 <tr>
  	<td><br><h2><strong>G. Tanggapan Orang Tua/Wali</strong></h2></td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="1" class="tab" id="table"  height='75px' bordercolor="#000000">
			<tr><td></td></tr>
		</table>
	</td>
 </tr>
 <?php if($semester=='3'){?>
	<tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td width="33%"><div align="center">Orang Tua/Wali Peserta Didik</div></td>
    <td width="33%"><div align="center">Wali Kelas</div></td>
    <td width="33%"><div align="center">Kepala Madrasah</div></td>
  </tr>
  
  <tr>
    <td width="33%" height="70px"><div align="center"></div></td>
    <td width="33%" height="70"><div align="center"></div></td>
    <td width="33%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="33%" rowspan="2"><div align="center">(...................................................)</div></td>
    <td width="33%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
    <td width="33%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_kepsek[namakepsek]?></u>
    </div></td>
  </tr>
  <tr>
    <td style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
    <td width="33%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_kepsek[nipkepsek]?>
    </div></td>
  </tr>
  </table>
  </td>
  </tr>
  <?php }else {?>
  <tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td rowspan="2" width="50%" style="padding: 0"><div align="center">Orang Tua/Wali Siswa</div></td>
    <td width="50%" style="padding: 0"><div align="center">a.n Kepala Madrasah</div></td>
  </tr>
  <tr><td align="center" style="padding: 0">Wali Kelas</td></tr>
  <tr>
    <td width="50%" height="70px"><div align="center"></div></td>
    <td width="50%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="50%" rowspan="2"><div align="center">(.............................................)</div></td>
    <td width="50%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
  </tr>
  <?php }?>

  
</table>
    </td>
  </tr>

 
 <?php
 
 /**if ($prespel!="false"){
 ?>
 <tr>
    <td><fieldset><legend><strong>Presensi Pelajaran</strong></legend>
	<!-- Content Presensi disini -->
	<table width="100%" border="1" class="tab" id="table" bordercolor="#000000">
  <tr>
    <td width="27%" rowspan="2" class=""><div align="center">Pelajaran</div></td>
    <td height="25" colspan="2" class=""><div align="center">Hadir</div></td>
    <td height="25" colspan="2" class=""><div align="center">Sakit</div></td>
    <td height="25" colspan="2" class=""><div align="center">Ijin</div></td>
    <td height="25" colspan="2" class=""><div align="center">Alpa</div></td>
    </tr>
  <tr>
    <td width="6" class=""><div align="center">Jumlah</div></td>
    <td width="6" class=""><div align="center">%</div></td>
    <td width="6" class=""><div align="center">Jumlah</div></td>
    <td width="6" class=""><div align="center">%</div></td>
    <td width="6" class=""><div align="center">Jumlah</div></td>
    <td width="6" class=""><div align="center">%</div></td>
    <td width="6" class=""><div align="center">Jumlah</div></td>
    <td width="6" class=""><div align="center">%</div></td>
  </tr>
  <!-- Ambil pelajaran per departemen-->
	<?php
	$sql_get_pelajaran_presensi="SELECT pel.replid as replid,pel.nama as nama FROM presensipelajaran ppel, ppsiswa pp, siswa sis, pelajaran pel ".
								"WHERE pp.nis=sis.nis ".
								"AND ppel.replid=pp.idpp ".
								"AND ppel.idpelajaran=pel.replid ".
								"AND ppel.idsemester='$semester' ".
								"AND ppel.idkelas='$kelas' ".
								"AND sis.nis='$nis' ".
								"GROUP BY pel.nama";
	$result_get_pelajaran_presensi=QueryDb($sql_get_pelajaran_presensi);
	$cntpel_presensi=1;
	
	while ($row_get_pelajaran_presensi=@mysql_fetch_array($result_get_pelajaran_presensi)){
		//ambil semua jumlah presensi per pelajaran 
		$sql_get_all_presensi="select count(*) as jumlah FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
							  "WHERE pel.idpelajaran='$row_get_pelajaran_presensi[replid]' AND pel.idsemester='$semester' AND pel.idkelas='$kelas' ".
							  "AND pel.replid=pp.idpp AND pp.nis='$nis'";
		$result_get_all_presensi=QueryDb($sql_get_all_presensi);
		$row_get_all_presensi=@mysql_fetch_array($result_get_all_presensi);
		//dapet nih jumlahnya
		$jumlah_presensi=$row_get_all_presensi[jumlah];
		
		//ambil yang hadir
		$sql_get_hadir="select count(*) as hadir FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
							  "WHERE pel.idpelajaran='$row_get_pelajaran_presensi[replid]' AND pel.idsemester='$semester' AND pel.idkelas='$kelas' ".
							  "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=0";
		$result_get_hadir=QueryDb($sql_get_hadir);
		$row_get_hadir=@mysql_fetch_array($result_get_hadir);
		$hadir=$row_get_hadir[hadir];
		$hh[$cntpel_presensi]=$hadir;
		//ambil yang sakit
		$sql_get_sakit="select count(*) as sakit FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
							  "WHERE pel.idpelajaran='$row_get_pelajaran_presensi[replid]' AND pel.idsemester='$semester' AND pel.idkelas='$kelas' ".
							  "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=1";
		$result_get_sakit=QueryDb($sql_get_sakit);
		$row_get_sakit=@mysql_fetch_array($result_get_sakit);
		$sakit=$row_get_sakit[sakit];
		$ss[$cntpel_presensi]=$sakit;
		//ambil yang ijin
		$sql_get_ijin="select count(*) as ijin FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
							  "WHERE pel.idpelajaran='$row_get_pelajaran_presensi[replid]' AND pel.idsemester='$semester' AND pel.idkelas='$kelas' ".
							  "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=2";
		$result_get_ijin=QueryDb($sql_get_ijin);
		$row_get_ijin=@mysql_fetch_array($result_get_ijin);
		$ijin=$row_get_ijin[ijin];
		$ii[$cntpel_presensi]=$ijin;
		//ambil yang alpa
		$sql_get_alpa="select count(*) as alpa FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
							  "WHERE pel.idpelajaran='$row_get_pelajaran_presensi[replid]' AND pel.idsemester='$semester' AND pel.idkelas='$kelas' ".
							  "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=3";
		$result_get_alpa=QueryDb($sql_get_alpa);
		$row_get_alpa=@mysql_fetch_array($result_get_alpa);
		$alpa=$row_get_alpa[alpa];
		$aa[$cntpel_presensi]=$alpa;
		//hitung prosentase kalo jumlahnya gak 0
		if ($jumlah_presensi<>0){
			$p_hadir=round(($hadir/$jumlah_presensi)*100);
			$p_sakit=round(($sakit/$jumlah_presensi)*100);
			$p_ijin=round(($ijin/$jumlah_presensi)*100);
			$p_alpa=round(($alpa/$jumlah_presensi)*100);
		} else {
			$p_hadir=0;
			$p_sakit=0;
			$p_ijin=0;
			$p_alpa=0;
		}
	?>
	<tr>
    <td height="25"><?php echo $row_get_pelajaran_presensi[nama]?></td>
    <td height="25"><div align="center">
      <?php echo $hadir?>
    </div></td>
    <td height="25"><div align="center">
      <?php echo $p_hadir?>%</div></td>
    <td height="25"><div align="center">
      <?php echo $sakit?>
    </div></td>
    <td height="25"><div align="center">
      <?php echo $p_sakit?>%</div></td>
    <td height="25"><div align="center">
      <?php echo $ijin?>
    </div></td>
    <td height="25"><div align="center">
      <?php echo $p_ijin?>%</div></td>
    <td height="25"><div align="center">
      <?php echo $alpa?>
    </div></td>
    <td height="25"><div align="center">
      <?php echo $p_alpa?>%</div></td>
	 </tr>
	<?php
	$cntpel_presensi++;
	}
	
	$hdr = 0;
	for ($i=1;$i<=count($hh);$i++)
		$hdr += $hh[$i];
	$skt = 0;
	for ($i=1;$i<=count($ss);$i++)
		$skt += $ss[$i];
	$ijn = 0;
	for ($i=1;$i<=count($ii);$i++)
		$ijn += $ii[$i];
	$alp = 0;
	for ($i=1;$i<=count($aa);$i++)
		$alp += $aa[$i];		
	/*	
	//sekarang hitung jumlah hadir semua pelajaran
	$sql_all_hadir="select count(*) as allhadir FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
	               "WHERE pel.idsemester='22' AND pel.idkelas='$kelas' ". 
                   "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=0";
    $result_all_hadir=QueryDb($sql_all_hadir);
	$row_all_hadir=@mysql_fetch_array($result_all_hadir);
	$all_hadir=$row_all_hadir[allhadir];
	//echo $sql_all_hadir;
	//sekarang hitung jumlah sakit semua pelajaran
	$sql_all_sakit="select count(*) as allsakit FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
	               "WHERE pel.idsemester='22' AND pel.idkelas='$kelas' ". 
                   "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=1";
    $result_all_sakit=QueryDb($sql_all_sakit);
	$row_all_sakit=@mysql_fetch_array($result_all_sakit);
	$all_sakit=$row_all_sakit[allsakit];

	//sekarang hitung jumlah ijin semua pelajaran
	$sql_all_ijin="select count(*) as allijin FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
	               "WHERE pel.idsemester='22' AND pel.idkelas='$kelas' ". 
                   "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=2";
    $result_all_ijin=QueryDb($sql_all_ijin);
	$row_all_ijin=@mysql_fetch_array($result_all_ijin);
	$all_ijin=$row_all_ijin[allijin];

	//sekarang hitung jumlah alpa semua pelajaran
	$sql_all_alpa="select count(*) as allalpa FROM $g_db_akademik.presensipelajaran pel, $g_db_akademik.ppsiswa pp ".
	               "WHERE pel.idsemester='22' AND pel.idkelas='$kelas' ". 
                   "AND pel.replid=pp.idpp AND pp.nis='$nis' AND pp.statushadir=3";
    $result_all_alpa=QueryDb($sql_all_alpa);
	$row_all_alpa=@mysql_fetch_array($result_all_alpa);
	$all_alpa=$row_all_alpa[allalpa];
	
	?>
  <tr>
    <td height="25" bgcolor="#FFFFFF"><div align="center" class="style6">Total</div></td>
    <td height="25" bgcolor="#CCCCCC"><div align="center"><?php echo $hdr?></div></td>
    <td height="25" bgcolor="#FFFFFF"><div align="center"></div></td>
    <td height="25" bgcolor="#CCCCCC"><div align="center"><?php echo $skt?></div></td>
    <td height="25" bgcolor="#FFFFFF"><div align="center"></div></td>
    <td height="25" bgcolor="#CCCCCC"><div align="center"><?php echo $ijn?></div></td>
    <td height="25" bgcolor="#FFFFFF"><div align="center"></div></td>
    <td height="25" bgcolor="#CCCCCC"><div align="center"><?php echo $alp?></div></td>
    <td height="25" bgcolor="#FFFFFF"><div align="center"></div></td>
  </tr>
  
</table>
<script language='JavaScript'>
	   	Tables('table', 1, 0);
</script>
    </fieldset></td>
  </tr>
  <?php } **/?>
</table>

</BODY>
</HTML>
<?php
CloseDb();
?>