<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');
	
	$departemen = $_REQUEST['departemen'];
	$tahunajaran = $_REQUEST['tahunajaran'];
	$tingkat = $_REQUEST['tingkat'];
	$kelas = $_REQUEST['kelas'];
	$semester = $_REQUEST['semester'];
	$pelajaran = $_REQUEST['pelajaran'];
	$aspek_penilaian = $_REQUEST['aspek_penilaian'];
	$predikat = $_REQUEST['predikat'];
	
	//echo "$departemen - $tahunajaran - $tingkat - $kelas - $semester - $pelajaran - $aspek_penilaian - $predikat <br>";
	
	if (isset($_REQUEST['Simpan'])) {
		OpenDb();
		$sql_cek = "SELECT deskripsi FROM ketercapaian_kompetensi 
					WHERE tingkat='$tingkat' AND semester='$semester' AND pelajaran='$pelajaran' AND tahunajaran='$tahunajaran' 
					AND aspek_penilaian='$aspek_penilaian' AND predikat='$predikat' ";
		//echo"$sql_cek";
		$result_cek = QueryDb($sql_cek);
		if (@mysql_num_rows($result_cek) > 0) {
			$sql_upd = "UPDATE ketercapaian_kompetensi SET deskripsi = '".CQ($_REQUEST['deskripsi'])."'
						WHERE semester='$semester' AND pelajaran='$pelajaran' AND aspek_penilaian='$aspek_penilaian' 
						AND predikat='$predikat'";
			//echo"$sql_upd";
			$result = QueryDb($sql_upd);
			if ($result) {
				?>
				<script language="javascript">
					opener.refresh();
					window.close();
				</script> 
			<?php
			}
		} 
		else {
			$sql_max = "SELECT max(id_ketercapaian_kompetensi) FROM ketercapaian_kompetensi";
			$rs_max = QueryDb($sql_max);
			$row_max = @mysql_fetch_row($rs_max);
			$next_id = $row_max[0]+1;
			
			$sql = "INSERT INTO ketercapaian_kompetensi SET 
					id_ketercapaian_kompetensi='$next_id',
					tingkat = '$tingkat',
					kelas = '$kelas',
					semester = '$semester',
					pelajaran = '$pelajaran',
					tahunajaran = '$tahunajaran',
					aspek_penilaian = '$aspek_penilaian',
					predikat = '$predikat',
					deskripsi = '$deskripsi'";
			//echo"$sql";
			$result = QueryDb($sql);
			if ($result) {
				?>
				<script language="javascript">
					opener.refresh();
					window.close();
				</script> 
			<?php
			}
		}
		CloseDb();
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Edit Ketercapaian Kompetensi]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function validate() {
				return validateEmptyText('deskripsi', 'Deskripsi');
			}
		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('deskripsi').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo  GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Edit Ketercapaian Kompetensi :.
					</div>
				</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="departemen" id="departemen" value="<?php echo  $departemen ?>"/>
												
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT max(id_ketercapaian_kompetensi) FROM ketercapaian_kompetensi";
								$rs = QueryDb($sql);
								$next_id = mysql_fetch_row($rs);
								
								$sql_tingkat = "SELECT tingkat FROM tingkat WHERE replid='$tingkat'";
								$result_tingkat = QueryDb($sql_tingkat);
								$row_tingkat = @mysql_fetch_row($result_tingkat);
								
								$sql_kelas = "SELECT kelas FROM kelas WHERE replid='$kelas'";
								$result_kelas = QueryDb($sql_kelas);
								$row_kelas = @mysql_fetch_row($result_kelas);
								
								$sql_semester = "SELECT semester FROM semester WHERE replid='$semester'";
								$result_semester = QueryDb($sql_semester);
								$row_semester = @mysql_fetch_row($result_semester);
								
								$sql_pelajaran = "SELECT nama FROM pelajaran WHERE replid='$pelajaran'";
								$result_pelajaran = QueryDb($sql_pelajaran);
								$row_pelajaran = @mysql_fetch_row($result_pelajaran);
								
								$sql_tahunajaran = "SELECT tahunajaran FROM tahunajaran WHERE replid='$tahunajaran'";
								$result_tahunajaran = QueryDb($sql_tahunajaran);
								$row_tahunajaran = @mysql_fetch_row($result_tahunajaran);
								
								$sql_aspek = "SELECT keterangan FROM dasarpenilaian WHERE dasarpenilaian='$aspek_penilaian'";
								$result_aspek = QueryDb($sql_aspek);
								$row_aspek = @mysql_fetch_row($result_aspek);
								
								$sql_predikat = "SELECT predikat FROM gradenilai WHERE id_gradenilai='$predikat'";
								$result_predikat = QueryDb($sql_predikat);
								$row_predikat = @mysql_fetch_row($result_predikat);
								
								$sql_desc = "SELECT deskripsi FROM ketercapaian_kompetensi 
											WHERE semester='$semester' AND pelajaran='$pelajaran' AND aspek_penilaian='$aspek_penilaian' 
											AND predikat='$predikat'";
								$result_desc = QueryDb($sql_desc);
								$row_desc = @mysql_fetch_row($result_desc);
							?>
							<tr>
								<td><strong>Tingkat</strong></td>
								<td>
									<input type="text" class="disabled" name="tingkat" id="tingkat" size="10" value="<?php echo  $row_tingkat[0] ?>" readonly />
									<input type="hidden" name="tingkat" id="tingkat" value="<?php echo $tingkat?>">
								</td>
							</tr>
							<tr>
								<td><strong>Kelas</strong></td>
								<td>
									<input type="text" class="disabled" name="kelas" id="kelas" size="10" value="<?php echo  $row_kelas[0]?>" readonly />
									<input type="hidden" name="kelas" id="kelas" value="<?php echo $kelas?>">
								</td>
							</tr>
							<tr>
								<td><strong>Semester</strong></td>
								<td>
									<input type="text" class="disabled" name="semester" id="semester" size="10" value="<?php echo  $row_semester[0]?>" readonly />
									<input type="hidden" name="semester" id="semester" value="<?php echo $semester?>">
								</td>
							</tr>
							<tr>
								<td><strong>Pelajaran</strong></td>
								<td>
									<input type="text" class="disabled" name="pelajaran" id="pelajaran" size="10" value="<?php echo  $row_pelajaran[0]?>" readonly />
									<input type="hidden" name="pelajaran" id="pelajaran" value="<?php echo $pelajaran?>">
								</td>
							</tr>
							<tr>
								<td><strong>Tahun Ajaran</strong></td>
								<td>
									<input type="text" class="disabled" name="tahunajaran" id="tahunajaran" size="10" value="<?php echo  $row_tahunajaran[0]?>" readonly />
									<input type="hidden" name="tahunajaran" id="tahunajaran" value="<?php echo $tahunajaran?>">
								</td>
							</tr>
							<tr>
								<td><strong>Aspek Penilaian</strong></td>
								<td>
									<input type="text" class="disabled" name="aspek_penilaian" id="aspek_penilaian" size="10" value="<?php echo  $row_aspek[0]?>" readonly />
									<input type="hidden" name="aspek_penilaian" id="aspek_penilaian" value="<?php echo $aspek_penilaian?>">
								</td>
							</tr>
							<tr>
								<td><strong>Predikat</strong></td>
								<td>
									<input type="text" class="disabled" name="predikat" id="predikat" size="10" value="<?php echo  $row_predikat[0]?>" readonly />
									<input type="hidden" name="predikat" id="predikat" value="<?php echo $predikat?>">
								</td>
							</tr>
							<tr>
								<td><strong>Deskripsi</strong></td>
								<td>
									<textarea name="deskripsi" id="deskripsi" rows="5" cols="45" ><?php echo  $row_desc[0] ?></textarea>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
								</td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo  GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo  $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>