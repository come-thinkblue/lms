<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');

if(isset($_REQUEST[nis])){
	$nis = $_REQUEST[nis];
}
if(isset($_REQUEST[nama])){
	$nama = $_REQUEST[nama];
}
if(isset($_REQUEST[departemen])){
	$departemen = $_REQUEST[departemen];
}
if(isset($_REQUEST[kelas])){
	$kelas = $_REQUEST[kelas];
}
if(isset($_REQUEST[jenis])){
	$jenis = $_REQUEST[jenis];
}
if(isset($_REQUEST[pelajaran])){
	$pelajaran = $_REQUEST[pelajaran];
}
if(isset($_REQUEST[semester])){
	$semester = $_REQUEST[semester];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Tambah Siswa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/mainmenu.css" type="text/css">
<script language="javascript">

</script>
<link rel="stylesheet" type="text/css" href="../style/style.css">

<script language="JavaScript" src="../script/tables.js"></script>
</head>

<body topmargin="0" leftmargin="10" marginheight="0" marginwidth="10"><br>
<?php
OpenDb();
?>
<form action="tambah_siswa_pp2.php" method="post" name="main" onSubmit="return tekan()">
<input type="hidden" name="selected">
<input type="hidden" value="<?php echo $departemen ?>" name="departemen">
<input type="hidden" value="<?php echo $kelas ?>" name="kelas">
<input type="hidden" value="<?php echo $jenis ?>" name="jenis">
<input type="hidden" value="<?php echo $pelajaran ?>" name="pelajaran">
<input type="hidden" value="<?php echo $semester ?>" name="semester">
<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="header">Input Siswa</td>
		<td class="header" align="right">Langkah 2 dari 2</td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td>NIS</td>
		<td><input type="text" name="nis" value="<?php echo $nis ?>" readonly></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td><input type="text" name="nama" value="<?php echo $nama ?>" readonly></td>
	</tr>
</table>
<br>
		<table border='1' cellspacing='0' cellpadding='0' bordercolor='#5A7594' width='100%'>
			<tr>
			  <td>
			 	<table border='1' width='100%' id="table" class="tab">
					<tr>
						<td class='header' align='center' height='30'>No</td>
						<td class='header' height='30'>Tanggal</td>
						<td class="header" height='30'>Deskripsi</td>
						<td class="header" height='30'>Status</td>
						<td class="header" height='30'>Nilai</td>
						<td class="header" height='30'>Keterangan</td>
					</tr>
					<?php
					$query = "SELECT replid, tanggal, deskripsi ".
							 "FROM $g_db_akademik.ujian ".
							 "WHERE idpelajaran = '$pelajaran' ".
							 "AND idkelas = '$kelas' ".
							 "AND idsemester = '$semester' ".
							 "AND idjenis = '$jenis' ";		
					$result = QueryDb($query);
					$jml_data = @mysql_num_rows($result);
					
					if($jml_data=="0"){
					?>
		<tr>
			<td colspan='3' align='center' colspan='6'>Data Tidak Ada</td>
		</tr>
		<?php 
	}else{
	?>
	<input type="hidden" name="num_data" value="<?php echo $jml_data ?>">	
	<?php
	$i = 1;
		while($row = @mysql_fetch_array($result)){
	?>
	<tr <?php echo "bgcolor=#".($cnt%2?"ffffff":"EAECEE").""; ?>>
		<td class='data'><?php echo $i ?>
		<input type="hidden" name="ujian<?php echo $i ?>" value="<?php echo $row[replid] ?>">
		</td>
		<td class='data'><?php echo $row[tanggal]; ?>
		<input type="hidden" name="tanggal<?php echo $i ?>" value="<?php echo $row[tanggal] ?>">
		</td>
		<td class='data'><?php echo $row[deskripsi]; ?>
		<input type="hidden" name="deskripsi<?php echo $i ?>" value="<?php echo $row[deskripsi] ?>">
		</td>
		<td class='data'><select name="status<?php echo $i ?>">
						<option value="0">Hadir/Mengumpulkan</option>
						<option value="1">Tidak Hadir</option>
						<option value="2">Tidak Mengumpulkan</option>
						<option value="3">Mencontek</option>
						<option value="4" selected>Lainnya</option>
						</select>
						</td>		
		<td class='data'><input type="text" name="nilai<?php echo $i ?>" size="2" value="0"></td>				
		<td class='data'><input type="text" name="keterangan<?php echo $i ?>" value="siswa baru, belum mengikuti ujian ini"></td>		
	</tr>
	<?php
	$i++;
	}
	?>
	<tr>
		<td colspan='6' align="right">
		<input type='submit' class='but' value='Simpan' name='simpan'></td>
		</form>
		</tr>	
	</table>
	 <script language='JavaScript'>
            Tables('table', 1, 0);
     </script> 
   </td>
  </tr>
</table>
<?php
if(isset($_POST[nis])) {
	$i=1;
	while($i < ($_POST[num_data]+1)){
		$uj = "ujian$i";
		$sts = "status$i";
		$nuj = "nilai$i";
		$ket = "keterangan$i";		

	OpenDBi();
		
	$result = mysqli_query($conni,"CALL spTambahNilaiUjian('$_POST[$uj]','$_POST[nis]','$_POST[$nuj]','$_POST[ket]','$_POST[$sts]')") or die (mysqli_error($conni));
	$i++;
	}
	$i=1;
	while($i < ($_POST[num_data]+1)){
		$uj = "ujian$i";
		$sts = "status$i";
		$nuj = "nilai$i";
		$ket = "keterangan$i";	
			
			$query_nuj1 = "SELECT * FROM $g_db_akademik.nilaiujian WHERE nilaiujian.idujian = '$_POST[$uj]'";
			$result_nuj1 = QueryDb($query_nuj1) or die (mysql_error());
	
				$t=1;
				while($row_nuj1 = @mysql_fetch_array($result_nuj1)){
					$tota_nuj1 += $row_nuj1[nilaiujian];
					$t++;
				}
				$ruk = $tota_nuj1/$t;	
				
				$query_ruk = "UPDATE $g_db_akademik.ratauk SET nilaiRk = '$ruk' ".
							 "WHERE idkelas = '$kelas' ".
							 "AND idsemester = '$semester' ".
							 "AND idujian = '$_POST[$uj]' ";
				
				$result_ruk = QueryDb($query_ruk) or die (mysql_error());	
			$i++;		 
		}	
	
	?>
	<script language="javascript">
		opener.document.location.href = "tampil_nilai_pelajaran.php?departemen=<?php echo $departemen ?>&kelas=<?php echo $kelas ?>&pelajaran=<?php echo $pelajaran ?>&semester=<?php echo $semester ?>&jenis_penilaian=<?php echo $jenis ?>";
		window.close();
	</script>
	<?php
	}
}
	CloseDb();
?>
</body>
</html>