<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

$idaturan = $_REQUEST['idaturan'];
$idkelas = $_REQUEST['kelas'];
$idsemester = $_REQUEST['semester'];
$pelajaran = $_REQUEST['idpelajaran'];
	
OpenDb();
BeginTrans();
$success = true;	

if ($_REQUEST['action'] <> "manual") 
{	
	for ($i = 1; $i <= $_REQUEST['jumujian']; $i++ && $success) 
	{
		$ujian = trim($_REQUEST['ujian'.$i]);
		$bobot = trim($_REQUEST['bobot'.$i]);
		$id = trim($_REQUEST['replid'.$i]);
		$cek = $_REQUEST['jenisujian'.$i];
		
		if ($cek)
		{
			if ($id == "")
				$sql = "INSERT INTO bobotnau SET idujian = '$ujian', bobot = '$bobot', idaturan = '$idaturan'";
			else
				$sql = "UPDATE bobotnau SET bobot = '$bobot' WHERE replid = '$id'";
			QueryDbTrans($sql, $success);
		}
		else
		{
			if ($id != "")
			{
				$sql = "DELETE FROM bobotnau WHERE replid = '$id'";
				QueryDbTrans($sql, $success);
			}
		}
	}
}

// ambil idjenisujian dan idpelajaran
$sql = "SELECT idjenisujian 
		FROM $g_db_akademik.aturannhb 
		WHERE replid = '$idaturan'";
$res = QueryDb($sql);
$row = mysql_fetch_array($res);
$jenis = $row['idjenisujian'];
	
$sql = "SELECT nis 
	 	FROM $g_db_akademik.siswa 
		WHERE idkelas = '$idkelas' AND aktif = 1 
		ORDER BY nama ASC ";
$result_get_nis_siswa = QueryDb($sql);

while ($success && ($row_get_nis_siswa = @mysql_fetch_array($result_get_nis_siswa)))
{
	$nis = $row_get_nis_siswa['nis'];
	
	if ($_REQUEST['action'] <> "manual") 
	{	
		$sql = "SELECT replid 
				FROM $g_db_akademik.ujian 
			    WHERE idkelas = '$idkelas' AND idsemester = '$idsemester' AND idaturan = '$idaturan' AND idpelajaran='$pelajaran'";
		$result_get_ujian = QueryDb($sql);
		
		$ujian_culip = 0;
		$nilai = 0.0;
		$bobot = 0.0;
		while ($row_get_ujian = @mysql_fetch_array($result_get_ujian))
		{	
			$idujian = $row_get_ujian['replid'];
			
			//Ambil bobot
			$sql = "SELECT bobot FROM $g_db_akademik.bobotnau WHERE idujian = '$idujian'";
			$result_get_bobot = QueryDb($sql);
			$row_get_bobot = @mysql_fetch_array($result_get_bobot);
			$b = (float)$row_get_bobot['bobot'];
			
			//Ambil nilai ujian
			$sql = "SELECT nilaiujian FROM $g_db_akademik.nilaiujian WHERE idujian = '$idujian' AND nis = '$nis'";
			$result_get_nilai = QueryDb($sql);
			$row_get_nilai = @mysql_fetch_array($result_get_nilai);
			$nu = (float)$row_get_nilai['nilaiujian'];
			
			//Hitung NA
			$nilai = $nilai + $b * $nu;
			$bobot = $bobot + $b;
			$ujian_culip++;
		}		
		$ratabulat = round(($nilai / $bobot), 2);	
	
		$sql = 	"SELECT nilaiAU, replid, keterangan 
				 FROM $g_db_akademik.nau 
				 WHERE nis = '$nis' AND idkelas = '$idkelas' AND idsemester ='$idsemester' AND idaturan = '$idaturan' AND idpelajaran='$pelajaran'";
		$result_nau = QueryDb($sql);
	
		if (mysql_num_rows($result_nau) > 0) 
		{	
			$row_nau = mysql_fetch_row($result_nau);
			$id_nau  = $row_nau[1];
			
			$sql_insert_nau = "UPDATE $g_db_akademik.nau SET nilaiAU = '$ratabulat' WHERE replid = '$id_nau'";
		}
		else
		{
			$sql_insert_nau = 
				"INSERT INTO $g_db_akademik.nau 
				 SET nis = '$nis', idkelas = '$idkelas', idsemester = '$idsemester', idjenis = '$jenis', 
				     idpelajaran = '$pelajaran', nilaiAU = '$ratabulat', idaturan = '$idaturan'";
		}
	} 
	else 
	{
		// if from manual 
		foreach($_REQUEST['nau'] as $key => $value) 
		{			
			if ($key == $nis) 
			{
				if ($value[1]) 
					$sql_insert_nau = "UPDATE $g_db_akademik.nau SET nilaiAU = '$value[0]', keterangan = 'Manual' WHERE replid = '$value[1]'";
				else 
					$sql_insert_nau = 
						"INSERT INTO $g_db_akademik.nau 
						 SET nis = '$key', idkelas = '$idkelas', idsemester = '$idsemester',
						     idjenis = '$jenis', idpelajaran = '$pelajaran', nilaiAU = '$value[0]',
							 idaturan = '$idaturan', keterangan = 'Manual'";				
			}		
		}
	}	
	
	QueryDbTrans($sql_insert_nau, $success);
}
	
if ($success) 
{
	CommitTrans();			
	CloseDb(); ?>
	<script language="javascript">
		window.history.back();
	</script>
<?php 	
} 
else 
{		
	RollbackTrans();		
	CloseDb();?>
	<script language="javascript">
		alert ('Data gagal disimpan');
	</script>
<?php 
}	
?>