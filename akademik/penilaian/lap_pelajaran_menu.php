<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');

$nis = $_REQUEST['nis_awal'];

$departemen = getDepartemen(SI_USER_ACCESS());
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

OpenDb();

/**$sql = "SELECT replid, departemen, nislama FROM riwayatdeptsiswa WHERE nis = '$nis_awal'";
echo $sql;
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
$dep[0] = array($row['departemen'], $nis_awal);
//$no[1] = $row['nislama'];
if ($row['nislama'] <> "") {
	$sql1 = "SELECT replid, departemen, nislama FROM riwayatdeptsiswa WHERE nis = '$row[nislama]'";
	$result1 = QueryDb($sql1);
	$row1 = @mysql_fetch_array($result1);	
	$dep[1] = array($row1['departemen'], $row['nislama']);
	//$no[2] = $row1['nislama'];	
	if ($row1['nislama'] <> "") {				
		$sql2 = "SELECT replid, departemen, nislama FROM riwayatdeptsiswa WHERE nis = '$row1[nislama]'";
		$result2 = QueryDb($sql2);
		$row2 = @mysql_fetch_array($result2);					
		$dep[2] = array($row2['departemen'],$row1['nislama']) ;
	}	
}		**/

//$nis = $dep[$departemen][1];

$sql_ajaran = "SELECT DISTINCT(t.replid), t.tahunajaran FROM riwayatkelassiswa r, kelas k, tahunajaran t WHERE r.nis = '$nis' AND r.idkelas = k.replid AND k.idtahunajaran = t.replid ORDER BY t.aktif DESC";

$result_ajaran = QueryDb($sql_ajaran);
$k = 0;
while ($row_ajaran = @mysql_fetch_array($result_ajaran)) {
	$ajaran[$k] = array($row_ajaran['replid'],$row_ajaran['tahunajaran']);
	$k++;
}

$tahunajaran = $ajaran[0][0];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];

$sql_kls = "SELECT DISTINCT(r.idkelas), k.kelas, t.tingkat, k.idtahunajaran FROM riwayatkelassiswa r, kelas k, tingkat t WHERE r.nis = '$nis' AND r.idkelas = k.replid AND k.idtingkat = t.replid";

$result_kls = QueryDb($sql_kls);
$j = 0;
while ($row_kls = @mysql_fetch_array($result_kls)) {
	$kls[$j] = array($row_kls['idkelas'],$row_kls['kelas'],$row_kls['tingkat'],$row_kls['idtahunajaran']);
	if ($row_kls['idtahunajaran']==$tahunajaran)
		$kelas = $row_kls['idkelas'];
	$j++;
}

//echo "<pre>";
//print_r($kls);
//echo "</pre>";



//$kelas = $kls[0][0];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Penilaian Pelajaran</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="javascript">

function change_dep() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;		
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}
/*
function change() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	var kelas = document.getElementById("kelas").value;
	
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&kelas="+kelas+"&tahunajaran="+tahunajaran+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}
*/
function change_kls() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	var kelas = document.getElementById("kelas").value;
	
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&kelas="+kelas+"&tahunajaran="+tahunajaran+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}

function change_ta() {
	var nis = document.getElementById("nis").value;
	var nis_awal = document.getElementById("nis_awal").value;
	var departemen = document.getElementById("departemen").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	//var kelas = document.getElementById("kelas").value;
	
	document.location.href = "lap_pelajaran_menu.php?departemen="+departemen+"&tahunajaran="+tahunajaran+"&nis="+nis+"&nis_awal="+nis_awal;
	parent.isi.location.href = "blank_lap_pelajaran.php";
}


function refresh() {	
	document.location.reload();
}
function tampil(pelajaran,kelas,nis,departemen) {	
	parent.isi.location.href="lap_pelajaran_content.php?pelajaran="+pelajaran+"&kelas="+kelas+"&nis="+nis+"&departemen="+departemen;
}

</script>
</head>

<body>
<input type="hidden" name="nis" id="nis" value="<?php echo $nis?>">
<input type="hidden" name="nis_awal" id="nis_awal" value="<?php echo $nis_awal?>">
<table border="0" width="100%" align="center" >
<!-- TABLE CENTER -->
<tr>	
	<td width="38%"><strong>Departemen </strong></td>
    <td width="*"> 
    	<select name="departemen" id="departemen" onChange="change_departemen()">
          <?php	$dep = getDepartemen(SI_USER_ACCESS());    
	foreach($dep as $value) {
		if ($departemen == "")
			$departemen = $value; ?>
          <option value="<?php echo $value ?>" <?php echo StringIsSelected($value, $departemen) ?> > 
            <?php echo $value ?> 
            </option>
            <?php	} ?>
        </select>
		
    </td>
</tr>

<tr>
	<td><strong>Tahun Ajaran</strong></td>
   	<td><select name="tahunajaran" id="tahunajaran" onchange="change_ta()" style="width:100px">
   		<?php for($k=0;$k<sizeof($ajaran);$k++) {?>
			<option value="<?php echo $ajaran[$k][0] ?>" <?php echo IntIsSelected($ajaran[$k][0], $tahunajaran) ?> > 
			<?php echo $ajaran[$k][1]?> </option>
		<?php } ?>
    	</select>    
	</td>
</tr>
<tr>
	<td><strong>Kelas </strong></td>
   	<td><select name="kelas" id="kelas" onchange="change_kls()" style="width:100px">
   		<?php for ($j=0;$j<sizeof($kls);$j++) {
				if ($kls[$j][3] == $tahunajaran) {
		?>
			<option value="<?php echo $kls[$j][0] ?>" <?php echo IntIsSelected($kls[$j][0], $kelas) ?> ><?php echo $kls[$j][2]." - ".$kls[$j][1] ?> </option>
		<?php 		}
			} ?>
    	</select>    
	</td>
</tr>   
<tr>
	<td colspan="2"><br />
    <table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="2" width="100%" align="left" bordercolor="#000000">
    <!-- TABLE CONTENT -->
    
    <tr height="30">    	
    	<td width="5%" class="header" align="center">No</td>
        <td width="*" class="header" align="center">Pelajaran</td>
    </tr>
    <?php 	OpenDb();		
		$sql = "SELECT DISTINCT p.replid, p.nama FROM ujian u, pelajaran p, nilaiujian n WHERE u.idpelajaran = p.replid AND u.idkelas = $kelas AND u.replid = n.idujian AND n.nis = '$nis' ORDER BY p.nama";
		//echo '<br> sql '.$sql;
		$result = QueryDb($sql); 				
		while ($row = @mysql_fetch_array($result)) {
	?>
    <tr>   	
       	<td height="25" align="center" onclick="tampil('<?php echo $row[0]?>','<?php echo $kelas?>','<?php echo $nis?>','<?php echo $departemen?>')" style="cursor:pointer"><?php echo ++$cnt?></td>
        <td height="25" onclick="tampil('<?php echo $row[0]?>','<?php echo $kelas?>','<?php echo $nis?>','<?php echo $departemen?>')" style="cursor:pointer">
        <?php echo $row[1]?>	</td>
    </tr>
    <!-- END TABLE CONTENT -->
    <?php 	} ?>
	</table>
	  <script language='JavaScript'>
	    Tables('table', 1, 0);
    </script>
	</td>	
</tr>
<!-- END TABLE CENTER -->    
</table> 
   

</body>
</html>