<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Nilai_Pelajaran.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

OpenDb();
if(isset($_REQUEST["semester"]))
	$semester = $_REQUEST["semester"];
if(isset($_REQUEST["kelas"]))
	$kelas = $_REQUEST["kelas"];
if(isset($_REQUEST["idaturan"]))
	$idaturan = $_REQUEST["idaturan"];

$sql = "SELECT p.nama, p.replid AS pelajaran, a.dasarpenilaian, j.jenisujian, j.replid AS jenis, dp.keterangan
		  FROM $g_db_akademik.aturannhb a, $g_db_akademik.pelajaran p, jenisujian j, dasarpenilaian dp 
		 WHERE a.dasarpenilaian = dp.dasarpenilaian AND a.replid='$idaturan' AND p.replid = a.idpelajaran AND a.idjenisujian = j.replid";
$result = QueryDb($sql);

$row = @mysql_fetch_array($result);
$namapel = $row['nama'];
$pelajaran = $row['pelajaran'];
$aspek = $row['dasarpenilaian'];
$aspekket = $row['keterangan'];
$namajenis = $row['jenisujian'];
$jenis = $row['replid'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aturan Perhitungan Nilai Rapor[Menu]</title>

<style type="text/css">
<!--
.style1 {
	color: #000099;
	font-weight: bold;
}
.style2 {color: #000099; font-weight: bold; font-size: 12px; }
-->
</style>
</head>
<body>
<form name="tampil_nilai_pelajaran" action="nilai_pelajaran_content.php" method="post" onSubmit="return validate();">
<input type="hidden" name="semester" value="<?php echo $semester?>" />
<input type="hidden" name="kelas" value="<?php echo $kelas?>" />
<input type="hidden" name="idaturan" value="<?php echo $idaturan?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top" background="" style="background-repeat:no-repeat; background-attachment:fixed">
    <table width="100%" border="0" height="100%">
   	<tr>
    	<td>
        <table width="100%" border="0">
        <tr>
            <td width="17%"><strong>Pelajaran</strong></td>
            <td><strong>: <?php echo $namapel ?> </strong></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <td><strong>Aspek Penilaian</strong></td>
            <td><strong>: <?php echo $aspekket?></strong></td>            
        </tr>
    	<tr>
            <td><strong>Jenis Pengujian</strong></td>
            <td><strong>: <?php echo $namajenis?></strong></td>  
<?php 	$sql_cek_ujian = "SELECT u.replid, u.tanggal, u.deskripsi, u.idrpp 
						FROM $g_db_akademik.ujian u 
					   WHERE u.idaturan='$idaturan' AND u.idkelas='$kelas' AND u.idsemester='$semester' ORDER by u.tanggal ASC";
    $result_cek_ujian = QueryDb($sql_cek_ujian);		
	$jumlahujian = @mysql_num_rows($result_cek_ujian); ?>            
  		</tr>
        </table>
        <br />
  		<table border="1" width="100%" id="table" class="tab">
       	<tr>
            <td height="30"  width="5" align='center'><font color="white"><strong>No.</strong></font></td>
    		<td height="30"  align='center'><font color="white"><strong>N I S</strong></font></td>
    		<td height="30"  align='center'><font color="white"><strong>Nama</strong></font></td>
    <?php
       
        $i=1;
        while ($row_cek_ujian=@mysql_fetch_array($result_cek_ujian)){
			$sql_get_rpp_name = "SELECT rpp FROM rpp WHERE replid='$row_cek_ujian[idrpp]'";
			if (!empty($row_cek_ujian[idrpp])) {
				$res_get_rpp_name = QueryDb($sql_get_rpp_name);
				$rpp = @mysql_fetch_array($res_get_rpp_name);
				$namarpp = $rpp[rpp];
			} else {
				$namarpp = "Tanpa RPP";
			}
			$nilaiujian[$i] = 0;
			$idujian[$i] = $row_cek_ujian['replid'];			
            $tgl = explode("-",$row_cek_ujian['tanggal']);
			
        ?>
    <td height="30"  align="center" ><font color="white"><strong><?php echo $namajenis."-".$i?>&nbsp;<br />
     
	<?php echo $tgl[2]."/".$tgl[1]."/".substr($tgl[0],2)?></strong></font></td>
    <?php
	$i++;
	}
	?>
    <td height="30" align="center"  width="10px"><font color="white"><strong>Rata- rata Siswa</strong></font></td>
    <td height="30" align="center" ><font color="white"><strong>Nilai Akhir <?php echo $namajenis?></strong>&nbsp;</font>
	</td>
  </tr>
  <?php
  $sql_siswa="SELECT * FROM $g_db_akademik.siswa WHERE idkelas='$kelas' AND aktif=1 ORDER BY nama ASC";
  $result_siswa=QueryDb($sql_siswa);
  $cnt=1;
  $jumsiswa = mysql_num_rows($result_siswa);
  while ($row_siswa=@mysql_fetch_array($result_siswa)){
  		$nilai = 0;	
  ?>
  <tr>
    <td height="25" align="center" width="5"><?php echo $cnt?></td>
    <td height="25" align="center"><?php echo $row_siswa['nis']?></td>
    <td height="25" align="left"><?php echo $row_siswa['nama']?></td>
    <?php 	for ($j=1;$j<=count($idujian);$j++) { ?>
            <td align="center">							
			<?php	$sql_cek_nilai_ujian="SELECT * FROM $g_db_akademik.nilaiujian WHERE idujian='$idujian[$j]' AND nis='$row_siswa[nis]'";
                $result_cek_nilai_ujian=QueryDb($sql_cek_nilai_ujian);
               	
                    $row_cek_nilai_ujian=@mysql_fetch_array($result_cek_nilai_ujian);
                	$nilaiujian[$j] = $nilaiujian[$j]+$row_cek_nilai_ujian['nilaiujian'];					
                	$nilai = $nilai+$row_cek_nilai_ujian['nilaiujian'];
                
                 
                    echo $row_cek_nilai_ujian['nilaiujian'];
					if ($row_cek_nilai_ujian[keterangan]<>"")
                        echo "<strong><font color='blue'>)*</font></strong>";               
				
			?>
            </td>
		<?php  } ?>
    		<td align="center"><?php echo round($nilai/count($idujian),2)?></td>
    		<td align="center">
	<?php					
			$sql_get_nau_per_nis="SELECT nilaiAU,replid,keterangan,info1 FROM $g_db_akademik.nau WHERE nis='$row_siswa[nis]' AND idkelas='$kelas' AND idsemester='$semester' AND idaturan='$idaturan'";
		
			//echo $sql_get_nau_per_nis;			
			$result_get_nau_per_nis=QueryDb($sql_get_nau_per_nis);
			if (mysql_num_rows($result_get_nau_per_nis) > 0) {
				$row_get_nau_per_nis=@mysql_fetch_array($result_get_nau_per_nis);
				
				echo $row_get_nau_per_nis['nilaiAU'];
				if ($row_get_nau_per_nis['keterangan']<>"")
					echo "<font color='#067900'><strong>)*</strong></font>";
				if ($row_get_nau_per_nis['info1']<>"")
					echo "&nbsp;<font color='blue'><strong>)*</strong></font>";
			} ?>
            </td>
    	</tr>
  <?php
  		$cnt++;
  		} ?>
        
		<tr>
        	<td height='25'  bgcolor='#666666' align='center' colspan='3'><font color="white"><strong>Rata-rata Kelas</strong></font></td>
	<?php 	$rata = 0;
        for ($j=1;$j<=count($idujian);$j++) { 
        	$rata = $rata+($nilaiujian[$j]/$jumsiswa);
    ?>
			<td align="center" bgcolor="#FFFFFF"><?php echo round(($nilaiujian[$j]/$jumsiswa),2)?></td>
	<?php 	} ?>
            <td align="center" bgcolor="#FFFFFF"><?php echo round($rata/count($idujian),2)?></td>
            <td align="center" bgcolor="#FFFFFF">
   	<?php					
		$sql_get_nau_per_nis="SELECT SUM(nilaiAU)/$jumsiswa FROM $g_db_akademik.nau WHERE idkelas='$kelas' AND idsemester='$semester' AND idaturan='$idaturan'";
		
		//echo $sql_get_nau_per_nis;			
		$result_get_nau_per_nis=QueryDb($sql_get_nau_per_nis);
		if (mysql_num_rows($result_get_nau_per_nis) > 0) {
			$row = mysql_fetch_row($result_get_nau_per_nis);     
			echo round($row[0],2);
     	} ?>        
            </td>
        </tr>
		</table>
</td>
    </tr>
    <tr>
    	<td><strong><font color="blue">)*</font> ada perubahan nilai akhir individual &nbsp;&nbsp; 
		<font color="#067900">)*</font> Nilai Akhir Siswa dihitung manual </strong>
        </td>
   	</tr>
  	<tr>
  		<td>
        <br />
        
        <table id="table" class="tab" width="350" border="1">
			<tr height="30" class="header" align="center">
				<td width="85%"  height="30" colspan="2" align="center"><strong><font color="white">Bobot Nilai Ujian</font></strong></td>
			</tr>
            <tr>
					<td width="85%"  height="30"><strong><font color="white"><?php echo $namajenis?></font></strong></td>
					<td width="15%"  align="center" height="30"><strong><font color="white">Bobot</font></strong></td>
				</tr>
     	<?php
			$sql_cek_ujian="SELECT * FROM $g_db_akademik.ujian WHERE idkelas='$kelas' AND idsemester='$semester' AND idaturan='$idaturan' ORDER by tanggal ASC";			
			$result_cek_ujian=QueryDb($sql_cek_ujian);
			$jumujian = mysql_num_rows($result_cek_ujian);
			$ibobot=1;
			
			while ($row_cek_ujian=@mysql_fetch_array($result_cek_ujian)){
				$sql_get_bobotnya="SELECT b.replid, b.bobot FROM $g_db_akademik.bobotnau b WHERE b.idujian='$row_cek_ujian[replid]'";								
				$result_get_bobotnya=QueryDb($sql_get_bobotnya);
				$nilai_bobotnya=@mysql_fetch_array($result_get_bobotnya);
		?>
    		<?php 	if (mysql_num_rows($result_get_bobotnya) > 0) { ?>
            <tr>
				<td width="85%" height="25">
            
				<!--<input <?php echo $dis?> type="checkbox" name="<?php echo 'jenisujian'.$ibobot ?>" id="<?php echo 'jenisujian'.$ibobot ?>" value="1" checked  onKeyPress="focusNext('bobot<?php echo $ibobot?>',event)">				            	
			<?php  //} else { ?>
				<input <?php echo $dis?> type="checkbox" name="<?php echo 'jenisujian'.$ibobot ?>" id="<?php echo 'jenisujian'.$ibobot ?>" value="1"  onKeyPress="focusNext('bobot<?php echo $ibobot?>',event)">--> 
            <?php //} ?>
                <?php echo $namajenis."-".$ibobot." (".format_tgl($row_cek_ujian['tanggal']).")"; ?>               
                </td>
                <td align="center">
                <?php echo $nilai_bobotnya['bobot']?>
                </td>
            </tr>
			<?php
				}
				$ibobot++;
			}
			?>
            	<input type="hidden" name="jumujian" id="jumujian" value="<?php echo $jumujian?>" />
            </table>
   		</td>
  	</tr>
	</table>
    </td>
</tr>
</table>

</form>
</body>
</html>