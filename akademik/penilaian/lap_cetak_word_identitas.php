<?php
/**[N]**
 * LMS SMA Negeri 1 Malang
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2013 SMA Negeri1 Malang
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];

//<<<<<<< HEAD
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//=======
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//>>>>>>> 5d938ab94f14792511a4c03227c91c635e5388bf
	//echo $sql_get_kepsek;
	$rslt_get_kepsek=QueryDb($sql_get_kepsek);
	$row_get_kepsek=@mysql_fetch_array($rslt_get_kepsek);

	$sql="SELECT * FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result=QueryDb($sql);
	$row=@mysql_fetch_array($result);
	if($row[kelamin]=='L')
		$gender = 'Laki Laki';
	else
		$gender = 'Perempuan';
	
/**/
header('Content-Type: application/vnd.ms-word'); //IE and Opera  
header('Content-Type: application/w-msword'); // Other browsers  
header('Content-Disposition: attachment; filename=Identitas_'.$nis.'.doc');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="Doc1_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-06-16T08:31:00Z</o:Created>
  <o:LastSaved>2008-06-16T08:31:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
 <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
@page Section2
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section2
	{page:Section2;}
@page Section3
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section3
	{page:Section3;}
@page Section4
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section4
	{page:Section4;}
.style1 {
	color: #000000;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
}
.style2 {
	font-size: 14px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
}
.style5 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; }
.style13 {color: #000000}
.style14 {color: #FFFFFF}
.style17 {color: #FFFFFF; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; }
.style20 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold; font-size: 12px; }
.style21 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style22 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style24 {
	font-size: 12;
	color: #FFFFFF;
}
.style27 {color: #FFFFFF; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12; }
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>

<body lang=EN-US style='tab-interval:36.0pt'>

<div class=Section1>

<?php echo getHeader($departemen)?>
<table width="100%" border="0">
 <tr>
  <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  	<tr>
    <td height="16" colspan="4" bgcolor="#FFFFFF"><div align="center" class="style13"><b>IDENTITAS PESERTA DIDIK</b></div></td>
    </tr>
 <!--identitas-->
 <tr>
  		<td width="5%">1</td>
  		<td width="33%">Nama Siswa (Lengkap)</td>
  		<td width="2%">:</td><td><?php echo $row[nama]; ?></td>
  	</tr>
  	<tr>
  		<td>2</td>
  		<td>Nomor Induk</td>
  		<td>:</td><td><?php echo $nis; ?></td>
  	</tr>
  	<tr>
  		<td>3</td>
  		<td>NISN</td>
  		<td>:</td><td><?php echo $nis; ?></td>
  	</tr>
  	<tr>
  		<td>4</td>
  		<td>Tempat dan Tgl. Lahir</td>
  		<td>:</td><td><?php echo $row[tmplahir].",".$row[tgllahir]; ?></td>
  	</tr>
  	<tr>
  		<td>5</td>
  		<td>Jenis Kelamin</td>
  		<td>:</td><td><?php echo $gender; ?></td>
  	</tr>
  	<tr>
  		<td>6</td>
  		<td>Agama</td>
  		<td>:</td><td><?php echo $row[agama]; ?></td>
  	</tr>
  	<tr>
  		<td>7</td>
  		<td>Anank ke</td>
  		<td>:</td><td><?php echo $row[anakke]; ?></td>
  	</tr>
  	<tr>
  		<td>8</td>
  		<td>Status dalam keluarga</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td>9</td>
  		<td>Alamat Peserta Didik</td>
  		<td>:</td><td><?php echo $row[alamatsiswa]; ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:</td><td><?php echo $row[telponsiswa]; ?></td>
  	</tr>
  	<tr>
  		<td>10</td>
  		<td colspan="3">Diterima di Madrasah ini</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Di Kelas</td>
  		<td>:</td><td>X (Sepuluh)</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Pada Tanggal</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>c. Semester</td>
  		<td>:</td><td><?php echo '1 (satu)'; ?></td>
  	</tr>
  	<tr>
  		<td>11</td>
  		<td colspan="3">Madrasah / Sekolah Asal</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Nama Sekolah</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Alamat</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td>12</td>
  		<td colspan="3">Ijasah SMP / MTs / Paket B</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Tahun</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Nomor</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td>13</td>
  		<td colspan="3">Nama Orang Tua</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Ayah</td>
  		<td>:</td><td><?php echo $row[namaayah] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Ibu</td>
  		<td>:</td><td><?php echo $row[namaibu] ?></td>
  	</tr>
  	<tr>
  		<td>14</td>
  		<td>Alamat Orang Tua</td>
  		<td>:</td><td><?php echo $row[alamatortu] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:</td><td><?php echo $row[teleponortu] ?></td>
  	</tr>
  	<tr>
  		<td>15</td>
  		<td colspan="3">Pekerjaan Orang Tua</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Ayah</td>
  		<td>:</td><td><?php echo $row[pekerjaanayah] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Ibu</td>
  		<td>:</td><td><?php echo $row[pekerjaanibu] ?></td>
  	</tr>
  	<tr>
  		<td>16</td>
  		<td>Nama Wali</td>
  		<td>:</td><td><?php echo $row[wali] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Alamat Wali</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:</td><td></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Pekerjaan Wali</td>
  		<td>:</td><td></td>
  	</tr>
 <!--/identitas-->

  <tr>
  	<td rowspan="4" align="right" colspan="3">
  		<?php 
  			if($row['foto']!=null){
  				echo $row['foto'];
  			}else{
  				echo "Foto 3 x 4";
  			}
  		?>
  	</td>
    <td>
     <table width="50%" border="0">
  <tr>
    <td width="50%" style="padding: 0"><div align="center">a.n Kepala Madrasah</div></td>
  </tr>
  <tr><td align="center" style="padding: 0">Kepala Sekolah</td></tr>
  <tr>
    <td width="50%" height="70px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      <u><?php echo $row_get_kepsek[namakepsek]?></u>
    </div></td>
  </tr>
  <tr>
    <td width="50%" style="padding: 0"><div align="center">
      NIP : <?php echo $row_get_kepsek[nipkepsek]?>
    </div></td>
  </tr>

  </table>
  </td>
 </tr>
</table>

</div>

<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
mso-break-type:section-break'>
</span>

</body>

</html>