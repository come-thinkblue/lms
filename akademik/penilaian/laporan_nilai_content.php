<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');
require_once('../include/getheader.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];		

$sql_ta = "SELECT * FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'";
$result_ta = QueryDb($sql_ta);
$row_ta = @mysql_fetch_array($result_ta);
$tglawal = $row_ta['tglmulai'];
$tglakhir = $row_ta['tglakhir'];

	//alamat sekolah
	$sql_alamat = "SELECT alamat1 FROM identitas WHERE departemen='$departemen'";
	$res_alamat = QueryDb($sql_alamat);
	$row_alamat =@mysql_fetch_array($res_alamat);
	
//echo "Dep=".$departemen.", Tkt=".$tingkat.", Kls=".$kelas.", Pelajaran=".$pelajaran.", Semester=".$semester.", Thn AJaran=".$tahun;
	
	$sql_get_nama="SELECT nama FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result_get_nama=QueryDb($sql_get_nama);
	$row_get_nama=@mysql_fetch_array($result_get_nama);
	
	$sql_get_kls="SELECT kelas FROM $g_db_akademik.kelas WHERE replid='$kelas'";
	$result_get_kls=QueryDb($sql_get_kls);
	$row_get_kls=@mysql_fetch_array($result_get_kls);

	$sql_get_tkt="SELECT tingkat FROM $g_db_akademik.tingkat WHERE replid='$tingkat'";
	$result_get_tkt=QueryDb($sql_get_tkt);
	$row_get_tkt=@mysql_fetch_array($result_get_tkt);
	
	$sql_get_sem="SELECT semester FROM $g_db_akademik.semester WHERE replid='$semester'";
	$result_get_sem=QueryDb($sql_get_sem);
	$row_get_sem=@mysql_fetch_array($result_get_sem);
	
	$sql_get_w_kls="SELECT p.nama as namawalikelas, p.nip as nipwalikelas FROM $g_db_pegawai.pegawai p, $g_db_akademik.kelas k WHERE k.replid='$kelas' AND k.nipwali=p.nip";
	//echo $sql_get_w_kls;
	$rslt_get_w_kls=QueryDb($sql_get_w_kls);
	$row_get_w_kls=@mysql_fetch_array($rslt_get_w_kls);
	
	//dasar penilaian
	$sql_dp = "SELECT DISTINCT d.dasarpenilaian, d.keterangan, d.info1
		  	    FROM infonap i, nap n, dasarpenilaian d
			   WHERE i.replid = n.idinfo AND n.nis = '$nis' 
			     AND i.idsemester = '$semester' 
			     AND i.idkelas = '$kelas'
			     AND n.idaturan = d.replid";
	
	//peminatan kelas
	$sql_pem = "SELECT keterangan FROM kelas WHERE replid='$kelas'";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<script language="javascript" src="../script/tools.js"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<style type="text/css">
.city {display:none;}
.tab tr, .tab td{
	padding: 5px;
	border: solid #000 1px;
}
</style>
<script language="javascript" src="../script/tables.js"></script>
</HEAD>
<BODY>
<ul class="w3-navbar w3-green">
  <li><a href="#" class="tablink" onclick="openCity(event, 'Cover');">Cover</a></li>
  <li><a href="#" class="tablink" onclick="openCity(event, 'Identitas');">Identitas</a></li>
  <li><a href="#" class="tablink" onclick="openCity(event, 'Rapor');">Rapor</a></li>
  <li><a href="#" class="tablink" onclick="openCity(event, 'Deskripsi');">Deskripsi</a></li>
</ul>

<div id="Cover" class="w3-container w3-border city">
<table>
	<tr>
    <td width="47%" align="right" valign="bottom"><div align="right"><a href="laporan_nilai_content.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>">
  <img src="../images/ico/refresh.png" border="0">Refresh</a>
    <a href="#" onClick="newWindow('laporan_nilai_cetak_cover.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',750,636,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/print.png" border="0">Cetak</a>
    <a href="#" onClick="newWindow('lap_cetak_word_cover.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',122,122,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/word.png" border="0">Cetak Word</a>
  <br>
	</td>
  </tr>
  </table>
  <img style="display: block; margin:auto;" src="http://localhost/lms/images/garuda.jpg">
  <h3 style="text-align: center;"><strong>LAPORAN <br>PENILAIAN HASIL BELAJAR</strong></h3>
  <img style="display: block; margin:auto;" src="http://localhost/lms/images/kemenag.jpg">
  <h3 style="text-align: center;"><strong>KOTA BLITAR</strong></h3>
  <p style="text-align: center;">Jln. Jati 78 Sukorejo Kota Blitar
  <br><br><br>NAMA SISWA</p>
  <h4 style="text-align: center; text-decoration: underline;"><strong><?php echo $row_get_nama[nama] ?></strong></h4>
  <h4 style="text-align: center;"><strong><?php echo $nis ?></strong></h4>
</div>

<div id="Identitas" class="w3-container w3-border city">
<table width="100%" border="0">
  <tr>
    <td width="47%" align="right" valign="bottom"><div align="right"><a href="laporan_nilai_content.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>">
  <img src="../images/ico/refresh.png" border="0">Refresh</a>
    <a href="#" onClick="newWindow('laporan_nilai_cetak_identitas.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',750,636,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/print.png" border="0">Cetak</a>
    <a href="#" onClick="newWindow('lap_cetak_word_identitas.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',122,122,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/word.png" border="0">Cetak Word</a>
  <br>
	</td>
  </tr>
  <?php 
  	echo getHeader($departemen);
  	$sql="SELECT * FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result=QueryDb($sql);
	$row=@mysql_fetch_array($result);
	if($row[kelamin]=='L')
		$gender = 'Laki Laki';
	else
		$gender = 'Perempuan';
  ?>
  <h3 style="text-align: center;"><strong>IDENTITAS PESERTA DIDIK</strong></h3>
  </tr>
  <table width="80%" border="0" bordercolor="#666666" cellpadding="0" cellspacing="0">
  	<tr>
  		<td>1</td>
  		<td>Nama Siswa (Lengkap)</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[nama]; ?></td>
  	</tr>
  	<tr>
  		<td>2</td>
  		<td>Nomor Induk</td>
  		<td>:&nbsp;&nbsp;<?php echo $nis; ?></td>
  	</tr>
  	<tr>
  		<td>3</td>
  		<td>NISN</td>
  		<td>:&nbsp;&nbsp;<?php echo $nis; ?></td>
  	</tr>
  	<tr>
  		<td>4</td>
  		<td>Tempat dan Tgl. Lahir</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[tmplahir].",".$row[tgllahir]; ?></td>
  	</tr>
  	<tr>
  		<td>5</td>
  		<td>Jenis Kelamin</td>
  		<td>:&nbsp;&nbsp;<?php echo $gender; ?></td>
  	</tr>
  	<tr>
  		<td>6</td>
  		<td>Agama</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[agama]; ?></td>
  	</tr>
  	<tr>
  		<td>7</td>
  		<td>Anank ke</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[anakke]; ?></td>
  	</tr>
  	<tr>
  		<td>8</td>
  		<td>Status dalam keluarga</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td>9</td>
  		<td>Alamat Peserta Didik</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[alamatsiswa]; ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[telponsiswa]; ?></td>
  	</tr>
  	<tr>
  		<td>10</td>
  		<td colspan="2">Diterima di Madrasah ini</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Di Kelas</td>
  		<td>:&nbsp;&nbsp;X (Sepuluh)</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Pada Tanggal</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>c. Semester</td>
  		<td>:&nbsp;&nbsp;<?php echo '1 (satu)'; ?></td>
  	</tr>
  	<tr>
  		<td>11</td>
  		<td colspan="2">Madrasah / Sekolah Asal</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Nama Sekolah</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Alamat</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td>12</td>
  		<td colspan="2">Ijasah SMP / MTs / Paket B</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Tahun</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Nomor</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td>13</td>
  		<td colspan="2">Nama Orang Tua</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Ayah</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[namaayah] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Ibu</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[namaibu] ?></td>
  	</tr>
  	<tr>
  		<td>14</td>
  		<td>Alamat Orang Tua</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[alamatortu] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[teleponortu] ?></td>
  	</tr>
  	<tr>
  		<td>15</td>
  		<td colspan="2">Pekerjaan Orang Tua</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>a. Ayah</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[pekerjaanayah] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>b. Ibu</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[pekerjaanibu] ?></td>
  	</tr>
  	<tr>
  		<td>16</td>
  		<td>Nama Wali</td>
  		<td>:&nbsp;&nbsp;<?php echo $row[wali] ?></td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Alamat Wali</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Telepon</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>
  	<tr>
  		<td></td>
  		<td>Pekerjaan Wali</td>
  		<td>:&nbsp;&nbsp;</td>
  	</tr>

  </table>
</table>
</div>

<div id="Rapor" class="w3-container w3-border city">
  <table width="100%" border="0">
  <tr>
    <td width="47%" align="right" valign="bottom"><div align="right"><a href="laporan_nilai_content.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>">
  <img src="../images/ico/refresh.png" border="0">Refresh</a>
    <a href="#" onClick="newWindow('laporan_nilai_cetak.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',750,636,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/print.png" border="0">Cetak</a>
    <a href="#" onClick="newWindow('lap_cetak_word.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',122,122,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/word.png" border="0">Cetak Word</a>
  <br>
</td>
  </tr>
  
  
<?php echo getHeader($departemen)?>
<table width="100%" border="0">
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
    <td height="20"><span >Nama Siswa</span></td>
	<td height="20"><span >:&nbsp;
	    <?php echo $row_get_nama[nama]?>
	</span></td>
	<td height="20" ><span >Kelas</span></td>
    <td height="20"><span >:&nbsp;
    <?php echo $row_get_tkt[tingkat]."-".$row_get_kls[kelas];?>
    </span></td>
  </tr>
  <tr>
    <td  height="20"><span >No. Induk
    </span></td>
    <td  height="20"><span >:&nbsp;
        <?php echo $nis?>
    </span></td>
	<td height="20"><span >Semester</span></td>
    <td height="20"><span >
		:&nbsp;<?php echo $row_get_sem[semester];?>
    </span></td>
  </tr>
  <tr>
    <td  height="20"><span >NISN 
    </span></td>
    <td  height="20"><span >:&nbsp;
        <?php echo $nis?>
    </span></td>
    <td height="20"><span >Tahun Pelajaran
    </span></td>
    <td height="20"><span >:&nbsp;
        <?php echo $row_ta[tahunajaran]?>
    </span></td>
  </tr>
</table>


</table>
  <tr>
  	<td><br><h4><strong>A. Sikap</strong></h4></td>
  </tr>
  <tr>
  	<td><p><strong>1. Sikap Spiritual</strong></p></td>
  </tr>
  <?php 
  	//idaturan
  $sql= "SELECT m.keterangan, m.info1 FROM sas s, master_sikap m 
  			WHERE s.nis='$nis' AND s.idsikap=m.replid AND s.idaturan=4
  			AND idkelas='$kelas' AND idsemester='$semester'";
  $res = QueryDb($sql);
  $row = @mysql_fetch_array($res);
  ?>
  <tr><td>
  <table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="100px"><div align="center"><b>PREDIKAT</b></div></td>
			<td align='center'><b>DESKRIPSI</b></td>
		</tr>
		<tr>
			<td align='center'><?php echo $row['info1'] ?></td>
			<td><?php echo $row['keterangan'] ?></td>
		</tr>
	</table>
	</td></tr>
   <tr>
  	<td><br><p><strong>2. Sikap Sosial</strong></p></td>
  </tr>
 <?php 
  	//idaturan
  $sql= "SELECT m.keterangan, m.info1 FROM sas s, master_sikap m 
  			WHERE s.nis='$nis' AND s.idsikap=m.replid AND s.idaturan=5
  			AND idkelas='$kelas' AND idsemester='$semester'";
			//echo $sql;
  $res = QueryDb($sql);
  $row = @mysql_fetch_array($res);
  ?>
  <tr><td>
  <table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="100px"><div align="center"><b>PREDIKAT</b></div></td>
			<td align='center'><b>DESKRIPSI</b></td>
		</tr>
		<tr>
			<td align='center'><?php echo $row['info1'] ?></td>
			<td><?php echo $row['keterangan'] ?></td>
		</tr>
	</table>
	</td></tr>

  <tr>
  	<td><br><h4><strong>B. Pengetahuan dan Ketrampilan</strong></h4></td>
  </tr>
  <tr>
	<td>
<?php	
	$res = QueryDb($sql_dp);
	$i = 0;
	while($row = mysql_fetch_row($res))
	{
		$aspekarr[$i++] = array($row[0], $row[1], $row[2]);
	} ?>  
	<table width="100%" border="1" class="tab" id="table" bordercolor="#000000">
	<tr>
		<td width="18%"  class='header'  rowspan="2" ><div align="center">Pelajaran</div></td>
		<td width="7%"   class='header' rowspan="2" ><div align="center">KKM</div></td>
<?php		for($i = 0; $i < count($aspekarr); $i++)
			echo "<td  colspan='3' class='header'  align='center' width='18%'>" . $aspekarr[$i][2] . "</td>"; ?>
  	</tr>
	<tr>
<?php	for($i = 0; $i < count($aspekarr); $i++)
		echo "<td class='header' align='center' width='7%'>Angka</td>
			   <td class='header' align='center' width='7%'>Predikat</td>
				<td class='header' align='center' width='20%'>Deskripsi</td>"; ?>   
   </tr>

<?php	
	//kelompok A, B dan peminatan
		$sifat_k13 = array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)');
		$respem = QueryDb($sql_pem);
		$rowpem = mysql_fetch_array($respem);
		$peminatan = $rowpem['keterangan'];
		if($peminatan=='IIK'){
			$kel = array(0, 1, 4);
		}else if ($peminatan=='IIS') {
			$kel = array(0, 1, 3);
		}else{
			$kel = array(0, 1, 2);
		}

		
  foreach ($kel as $kelompok) {
	$sql = "SELECT pel.replid, pel.nama
	  FROM ujian uji, nilaiujian niluji, siswa sis, pelajaran pel 
	  WHERE uji.replid = niluji.idujian AND niluji.nis = sis.nis 
	  AND uji.idpelajaran = pel.replid AND uji.idsemester = $semester 
	  AND uji.idkelas = $kelas AND sis.nis = '$nis' 
	  AND pel.sifat_k13='$kelompok' GROUP BY pel.nama";
	$respel = QueryDb($sql);
	$hitung = mysql_num_rows($respel);
	if($hitung>0){
	echo 
		"<tr>
			<td colspan='8'><b>".$sifat_k13[$kelompok]."</b></td>
		</tr>";
	while($rowpel = mysql_fetch_row($respel))
	{
		$idpel = $rowpel[0];
		$nmpel = $rowpel[1];
		
		$sql = "SELECT nilaimin 
					 FROM infonap
					WHERE idpelajaran = $idpel
					  AND idsemester = $semester
				     AND idkelas = $kelas";
		$res = QueryDb($sql);
		$row = mysql_fetch_row($res);
		$nilaimin = $row[0];
				
		echo "<tr height='25'>";
		echo "<td align='left' >$nmpel</td>";
		echo "<td align='center'>$nilaimin</td>";
		
		for($i = 0; $i < count($aspekarr); $i++)
		{
			$na = "";
			$nh = "";
			$asp = $aspekarr[$i][0];
		
			$sql = "SELECT nilaiangka, nilaihuruf, idpelajaran
						 FROM infonap i, nap n, dasarpenilaian a 
						WHERE i.replid = n.idinfo 
						  AND n.nis = '$nis' 
						  AND i.idpelajaran = '$idpel' 
						  AND i.idsemester = '$semester' 
						  AND i.idkelas = '$kelas'
						  AND n.idaturan = a.replid 	   
						  AND a.dasarpenilaian = '$asp'";
			$res = QueryDb($sql);
			
			$cntpel_komentar = 1;
			if (mysql_num_rows($res) > 0)
			{
				$row = mysql_fetch_row($res);
				$na = $row[0];
				$nh = $row[1];

				//komentar
				/**$sql = "SELECT k.komentar 
		          		FROM $g_db_akademik.komennap k, $g_db_akademik.infonap i 
				 		WHERE k.nis='$nis' AND i.idpelajaran='$row[2]' AND i.replid=k.idinfo 
				   		AND i.idsemester='$semester' AND i.idkelas='$kelas'";
				$res2 = QueryDb($sql);
				$row2 = @mysql_fetch_row($res2);**/
			}
			//$say = $NTT->Convert($na);
			echo "<td align='center'>$na</td><td align='center'>$nh</td><td align='left'>Terlampir</td>"; 
		} 
		echo "</tr>";
	}
  }
}

	//lintas minat statis

?>   
	</table>
	<script language='JavaScript'>
	   	Tables('table', 1, 0);
	</script>
	</td>
  </tr>
  <tr>
  	<td><p style="font-size: 12px">Tabel interval predikat berdasarkan KKM</p></td>
  </tr>
  <tr>
  	<td>
  	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="100px" rowspan="2"><div align="center"><b>KKM</b></div></td>
			<td colspan="4" align="center"><b> PREDIKAT </b></td>
		</tr>
		<tr>
			<td align='center'><b>D = Kurang</b></td>
			<td align='center'><b>C = Cukup</b></td>
			<td align='center'><b>B = Baik</b></td>
			<td align='center'><b>A = Sangat Baik</b></td>
		</tr>
		<tr>
			<td align='center'>60</td>
			<td align='center'>< 60</td>
			<td align='center'>60</td>
			<td align='center'>>60 - <=80</td>
			<td align='center'>>80 - <=100</td>
		</tr>
		<tr>
			<td align='center'>70</td>
			<td align='center'>< 70</td>
			<td align='center'>70</td>
			<td align='center'>>70 - <=85</td>
			<td align='center'>>85 - <=100</td>
		</tr>
		<tr>
			<td align='center'>80</td>
			<td align='center'>< 80</td>
			<td align='center'>80</td>
			<td align='center'>>80 - <=90</td>
			<td align='center'>>90 - <=100</td>
		</tr>
	</table>
  	</td>
  </tr>

  <tr>
  	<td><br><h4><strong>C. Ekstra Kurikuler</strong></h4></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="30px"><div align="center"><b>No</b></div></td>
			<td><b> Ekstra Kurikuler</b></td>
			<td><div align="center"><b>Keterangan dalam Kegiatan</b></div></td>
		</tr>
		<tr>
			<td align='center'>1</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>2</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>3</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>4</td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</td>
  </tr>
  
  <tr>
  	<td><br><h4><strong>D. Prestasi</strong></h4></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
		<tr>
			<td width="30px"><div align="center"><b>No</b></div></td>
			<td align='center'><b> Jenis Prestasi</b></td>
			<td><div align="center"><b>Keterangan dalam Kegiatan</b></div></td>
		</tr>
		<tr>
			<td align='center'>1</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>2</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>3</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>4</td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</td>
  </tr>
  
  <?php
  if ($harian!="false"){
  ?>
  <tr>
  	<td><br><h4><strong>E. Ketidakhadiran</strong></h4></td>
  </tr>
  <tr>
    <td>
	<?php


	 $sql_harian = "SELECT SUM(ph.hadir) as hadir, SUM(ph.ijin) as ijin, SUM(ph.sakit) as sakit, SUM(ph.cuti) as cuti, SUM(ph.alpa) as alpa, SUM(ph.hadir+ph.sakit+ph.ijin+ph.alpa+ph.cuti) as tot ".
			"FROM presensiharian p, phsiswa ph, siswa s ".
			"WHERE ph.idpresensi = p.replid ".
			"AND ph.nis = s.nis ".
			"AND ph.nis = '$nis' ".
			"AND ((p.tanggal1 ".
			"BETWEEN '$tglawal' ".
			"AND '$tglakhir') ".
			"OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) ".
			"ORDER BY p.tanggal1"; ;
	  ?>
	<!-- Content Presensi disini -->
	<table width="30%" border="1" class="tab" id="table"  bordercolor="#000000">
  <!-- Ambil pelajaran per departemen-->
	<?php
	$result_harian=QueryDb($sql_harian);
	$row_harian=@mysql_fetch_array($result_harian);
	$hadir=$row_harian['hadir'];
	$sakit=$row_harian['sakit'];
	$ijin=$row_harian['ijin'];
	$alpa=$row_harian['alpa'];
	$cuti=$row_harian['cuti'];
	$all=$row_harian['tot'];
	if ($hadir!=0 && $all !=0)
	$p_hadir=$hadir/$all*100;
	
	if ($sakit!=0 && $all !=0)
	$p_sakit=$sakit/$all*100;
	
	if ($ijin!=0 && $all !=0)
	$p_ijin=$ijin/$all*100;
	
	if ($alpa!=0 && $all !=0)
	$p_alpa=$alpa/$all*100;
	
	if ($cuti!=0 && $all !=0)
	$p_cuti=$cuti/$all*100;
	?>
	<tr>
	  <td height="25" bgcolor="#FFFFFF" colspan=3><b>Ketidakhadiran</b></td>
	</tr>
	<tr>
		<td height="25" bgcolor="#FFFFCC"><div align="center">1</div></td>
		<td height="25" bgcolor="#FFFFCC">Sakit</div></td>
		<td height="25" bgcolor="#FFFFCC"><div align="center">
      	<?php echo $sakit?> hari
   		</div></td>
	</tr>
	<tr>
	<td height="25" bgcolor="#FFFFFF"><div align="center">2</div></td>
		<td height="25" bgcolor="#FFFFFF">Ijin</div></td>
		<td height="25" bgcolor="#FFFFFF"><div align="center">
      	<?php echo $ijin?> hari
    	</div></td>
	</tr>
	<tr>
	<td height="25" bgcolor="#FFFFCC"><div align="center">3</div></td>
		<td height="25" bgcolor="#FFFFCC">Tanpa Keterangan</td>
		<td height="25" bgcolor="#FFFFCC"><div align="center">
      	<?php echo $alpa?> hari
    	</div></td>
	</tr>
	<!--<tr>
		<td height="25" bgcolor="#FFFFFF"><div align="center">Cuti</div></td>
		<td height="25" bgcolor="#FFFFFF"><div align="center">
      	<?php echo $cuti?>
      	</div></td>
	</tr>-->
    
</table>
<script language='JavaScript'>
	   	Tables('table', 1, 0);
</script>
    </td>
  </tr>
  
  
  
  
 <?php
 }?>
 <tr>
  	<td><br><h4><strong>F. Catatan Wali Kelas</strong></h4></td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="1" class="tab" id="table"  height='75px' bordercolor="#000000">
			<tr><td></td></tr>
		</table>
	</td>
 </tr>
 
 <tr>
  	<td><br><h4><strong>G. Tanggapan Orang Tua/Wali</strong></h4></td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="1" class="tab" id="table"  height='75px' bordercolor="#000000">
			<tr><td></td></tr>
		</table>
	</td>
 </tr>
 </table>
</div>

<!--
TAB DESKRIPSI
-->
<div id="Deskripsi" class="w3-container w3-border city">
<table width="100%" border="0">
  <tr>
    <td width="47%" align="right" valign="bottom"><div align="right"><a href="laporan_nilai_content.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>">
  <img src="../images/ico/refresh.png" border="0">Refresh</a>
    <a href="#" onClick="newWindow('laporan_nilai_cetak_deskripsi.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',750,636,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/print.png" border="0">Cetak</a>
    <a href="#" onClick="newWindow('lap_cetak_word_deskripsi.php?departemen=<?php echo $departemen?>&semester=<?php echo $semester?>&tingkat=<?php echo $tingkat?>&tahunajaran=<?php echo $tahunajaran?>&pelajaran=<?php echo $pelajaran?>&kelas=<?php echo $kelas?>&nis=<?php echo $nis?>&harian=<?php echo $harian?>&prespel=<?php echo $prespel?>','Cetak',122,122,'resizable=1,scrollbars=1,status=1,toolbar=0')">
  <img src="../images/ico/word.png" border="0">Cetak Word</a>
  <br>
</td>
  </tr>
  
  
<?php echo getHeader($departemen)?>
<table width="100%" border="0">
  <tr>
    <td>
	<table width="50%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
  	<td height="20" width="120px"><span >Nama Siswa</span></td>
	<td height="20"><span >:&nbsp;
	    <?php echo $row_get_nama[nama]?>
	</span></td>
  </tr>
  <tr>
    <td  height="20"><span >No. Induk / NISN 
    </span></td>
    <td  height="20"><span >:&nbsp;
        <?php echo $nis?>
    </span></td>
  </tr>
  <tr>
    
	<td height="20" ><span >Kelas / Semester</span></td>
    <td height="20"><span >:&nbsp;
    	<?php echo $row_get_tkt[tingkat]."-".$row_get_kls[kelas]." / ".$row_get_sem[semester];?>
    </span></td>
  </tr>
  <tr>
    <td height="20"><span >Tahun Pelajaran
    </span></td>
    <td height="20"><span >:&nbsp;
        <?php echo $row_ta[tahunajaran]?>
    </span></td>
  </tr>
	</table>
  </table>
<tr>
    <td>
	<table width="100%" border="1" class="tab" id="table"  bordercolor="#000000">
	  <tr>
		<td width="30px"><div align="center"><b>No</b></div></td>
		<td width="25%" align="center"><b>MATA PELAJARAN</b></td>
		<td width="10%"><div align="center"><b>ASPEK</b></div></td>
		<td><div align="center"><b>DESKRIPSI</b></div></td>
	  </tr>
	<?php	
	//kelompok A, B dan peminatan
		$sifat_k13 = array('Kelompok A (Wajib)', 'Kelompok B (Wajib)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)', 'Kelompok C (Peminatan)');
		$respem = QueryDb($sql_pem);
		$rowpem = mysql_fetch_array($respem);
		$peminatan = $rowpem['keterangan'];
		if($peminatan=='IIK'){
			$kel = array(0, 1, 4);
		}else if ($peminatan=='IIS') {
			$kel = array(0, 1, 3);
		}else{
			$kel = array(0, 1, 2);
		}

		
  foreach ($kel as $kelompok) {
	$sql = "SELECT pel.replid, pel.nama
	  FROM ujian uji, nilaiujian niluji, siswa sis, pelajaran pel 
	  WHERE uji.replid = niluji.idujian AND niluji.nis = sis.nis 
	  AND uji.idpelajaran = pel.replid AND uji.idsemester = $semester 
	  AND uji.idkelas = $kelas AND sis.nis = '$nis' 
	  AND pel.sifat_k13='$kelompok' GROUP BY pel.nama";
	$respel = QueryDb($sql);
	$hitung = mysql_num_rows($respel);
	if($hitung>0){
	echo 
		"<tr>
			<td colspan='4'><b>".$sifat_k13[$kelompok]."</b></td>
		</tr>";
	$no = 0;
	while($rowpel = mysql_fetch_row($respel))
	{
		$no++;
		$idpel = $rowpel[0];
		$nmpel = $rowpel[1];
		//select KKM
		$sql = "SELECT nilaimin FROM infonap WHERE idpelajaran = $idpel 
				AND idsemester = $semester AND idkelas = $kelas";
		$res = QueryDb($sql);
		$row = mysql_fetch_row($res);
		$nilaimin = $row[0];
				
		echo "<tr height='25'>";
		echo "<td align='center' rowspan='2'>$no</td>";
		echo "<td align='left' rowspan='2'>$nmpel</td>";
		
		for($i = 0; $i < count($aspekarr); $i++)
		{
			$na = "";
			$nh = "";
			$temp = array();
			$asp = $aspekarr[$i][2];
			$dp = $aspekarr[$i][0];
		
			//select nilai akhir
			$sql = "SELECT nilaiangka
						FROM infonap i, nap n, dasarpenilaian a 
						WHERE i.replid = n.idinfo AND n.nis = '$nis' AND i.idpelajaran = '$idpel' 
						AND i.idsemester = '$semester' AND i.idkelas = '$kelas'
						AND n.idaturan = a.replid AND a.dasarpenilaian = '$dp'";
			$res = QueryDb($sql);
			
			if (mysql_num_rows($res) > 0)
			{
				$row = mysql_fetch_row($res);
				$na = $row[0];
				if($na > $nilaimin)
					$KKM = 'Sudah Tercapai KKM';
				else
					$KKM = 'Belum Tercapai KKM';
			}
			//select id ujian
			$sql_ujian = "SELECT n.nilaiujian, r.deskripsi FROM ujian u, aturannhb a, nilaiujian n, rpp r
							WHERE a.dasarpenilaian = '$dp' AND a.replid=u.idaturan 
							AND u.idrpp = r.replid AND u.idpelajaran=r.idpelajaran
							AND u.idsemester=r.idsemester AND u.replid=n.idujian AND n.nis = $nis
							AND u.idkelas='$kelas' AND u.idsemester='$semester' 
							AND u.idpelajaran='$idpel' AND u.idrpp IS NOT NULL";

			$res_ujian = QueryDb($sql_ujian);
			
			while ($row_ujian = mysql_fetch_row($res_ujian)) {
				$temp[] =  $row_ujian[0].' '.$row_ujian[1];
			}
		
			
			echo 	"<td align='center'>$asp</td><td align='left'>".$KKM.", Menguasai ".
					substr(max($temp), 5).", Belum menguasai". substr(min($temp), 5) ."</td></tr>"; 
		} 
		echo "</tr>";
	}
  }
}

	//lintas minat statis
?>

	</table>
	</td>
  </tr>
</table>
</div>
<!--
TAB DESKRIPSI
-->

<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
</script>

</BODY>
</HTML>
<?php
CloseDb();
?>