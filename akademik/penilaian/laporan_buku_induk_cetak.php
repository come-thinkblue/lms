<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();

OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['nis']))
	$nis = $_REQUEST['nis'];
		

$sql_ta = "SELECT * FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'";
$result_ta = QueryDb($sql_ta);
$row_ta = @mysql_fetch_array($result_ta);
$tglawal = $row_ta['tglmulai'];
$tglakhir = $row_ta['tglakhir'];

$sql="SELECT * FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result=QueryDb($sql);
	$row=@mysql_fetch_array($result);
	
	if($row[kelamin]=='L')
		$gender = 'Laki Laki';
	else
		$gender = 'Perempuan';
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<script language="javascript" src="../script/tools.js"></script>
<style type="text/css">
<!--
.style1 {
	color: #FFFFFF;
	font-weight: bold;
}
.style4 {font-size: 12}
.style5 {color: #FFFFFF; font-weight: bold; font-size: 12; }
.style6 {
	font-size: 12px;
	font-weight: bold;
}
-->
hr {
  border:none;
  padding-top:10px;
  border-bottom:1px dotted #000;
  color:#fff;
  background-color:#fff;
  height:1px;
  width: 98%;
}
</style>
<script language="javascript" src="../script/tables.js"></script>
</HEAD>
<BODY>

<table width="100%" border="0">
  <tr>
    <td>
	<table width="100%" border="0" bordercolor="#666666" cellpadding="0" cellspacing="0">
	<tr>
		<td width='15%'>Foto</td>
		<td width='30%' colspan='3' height='30px'><b>I. Siswa</b></td>
		<td colspan='4'><b>Nomor Induk Siswa :  &nbsp;&nbsp;<?php echo $nis;?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>1</td>
		<td>Nama Siswa</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[nama]; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>2</td>
		<td>Jenis Kelamin</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $gender?></td>
	</tr>
	<tr>
		<td></td>
		<td>3</td>
		<td>Tempat dan Tgl. Lahir</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[tmplahir].",".$row[tgllahir] ?></td>
	</tr>
	<tr>
		<td></td>
		<td>4</td>
		<td>Anak ke</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[anakke]; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>5</td>
		<td>Jumlah Saudara</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Kandung</td>
		<td colspan='2' width='50%'>&nbsp;&nbsp;<?php echo $row[jsaudara]; ?></td>
		<td width='50%'>&nbsp;&nbsp;Orang</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>&nbsp;&nbsp;Tiri</td>
		<td colspan='2' width='50%'><hr></td>
		<td width='50%'>&nbsp;&nbsp;Orang</td>
	</tr>
	<tr>
		<td></td>
		<td>6</td>
		<td>Alamat</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[alamatsiswa]?></td>
	</tr>
	
	<!--orang tua-->
	<tr>
		<td width='15%'></td>
		<td width='20%' colspan='6' height='30px'><b>II. Orang Tua / Wali</b></td>		
	</tr>
	<tr>
		<td></td>
		<td>7</td>
		<td>Nama Orang Tua</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Ayah</td>
		<td width='30%'>&nbsp;&nbsp;<?php echo $row[namaayah]?></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Ibu&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row[namaibu]?></td>
		
	</tr>
	<tr>
		<td></td>
		<td>8</td>
		<td>Pekerjaan</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Ayah</td>
		<td width='30%'><hr></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Ibu  ...........................................................</td>
	</tr>
	<tr>
		<td></td>
		<td>9</td>
		<td>Agama</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Ayah</td>
		<td width='30%'><hr></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Ibu  ...........................................................</td>
	</tr>
	<tr>
		<td></td>
		<td>10</td>
		<td>Alamat</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[alamatortu]?></td>
	</tr>
	<tr>
		<td></td>
		<td>11</td>
		<td>Nama Wali</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[namaayah]?></td>
	</tr>
	<tr>
		<td></td>
		<td>12</td>
		<td>Pekerjaan</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>13</td>
		<td>Agama</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>14</td>
		<td>Alamat</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[alamatortu]?></td>
	</tr>
	<tr>
		<td width='15%'>Foto</td>
		<td width='30%' colspan='6' height='30px'><b>III. MASUK MADRASAH INI</b></td>
		
	</tr>
	<tr>
		<td></td>
		<td>15</td>
		<td>Dari Sekolah/Madrasah</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>16</td>
		<td>Tanggal</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>17</td>
		<td>Surat Pindah</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>18</td>
		<td>No. Induk Sekolah Asal</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>19</td>
		<td>Program Pilihan</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>20</td>
		<td>Nilai UN/UAM MTs/SLTP</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td colspan='7'>
			<table width="100%" border="1" class="tab" bordercolor="#000000">
				<tr>
					<td rowspan='2' align='center' width='5%'><b>No. Urut</b></td>
					<td rowspan='2' align='center'><b>MATA PELAJARAN</b></td>
					<td align='center' colspan='2'><b>NILAI</b></td>
					<td rowspan='2' align='center'><b>KETERANGAN</b></td>
				</tr>
				<tr>
					<td align='center'><b>Angka</b></td>
					<td align='center'><b>STTB/IJAZAH</b></td>
				</tr>
				<tr>
					<td align='center'>1</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>2</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>3</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>4</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>5</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>6</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>7</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='15%'></td>
		<td width='20%' colspan='6' height='30px'><b>III. MENINGGALKAN K. MADRASAH INI</b></td>
	</tr>
	<tr>
		<td></td>
		<td>21</td>
		<td>Tanggal</td>
		<td>:</td>
		<td width='30%' colspan='2'><hr></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Kelas  ...........................................................</td>
	</tr>
	<tr>
		<td></td>
		<td>22</td>
		<td>Alasan</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td width='15%'>Foto</td>
		<td width='20%' colspan='6' height='30px'><b>IV. P	EMBAYARAN UANG SEKOLAH/SPP</b></td>
	</tr>
	<tr>
		<td></td>
		<td colspan='7'>
			<table width="100%" border="1" class="tab" bordercolor="#000000">
				<tr>
					<td rowspan='2' align='center' width='5%'><b>No. Urut</b></td>
					<td rowspan='2' align='center'><b>WAJIB BAYAR</b></td>
					<td align='center' colspan='4'><b>TAHUN PELAJARAN</b></td>
				</tr>
				<tr>
					<td align='center'>&nbsp;&nbsp;</td>
					<td align='center'>&nbsp;&nbsp;</td>
					<td align='center'>&nbsp;&nbsp;</td>
					<td align='center'>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td align='center'>1</td>
					<td>Nama</td>
					<td colspan='4'></td>
				</tr>
				<tr>
					<td align='center'>2</td>
					<td>Alamat</td>
					<td colspan='4'></td>
				</tr>
				<tr>
					<td align='center'>3</td>
					<td>Penghasilan Orang Tua</td>
					<td colspan='4'></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
    
	</td>
</tr>

<tr>
	<td>
	<table width="100%" border="1" class="tab" id="table" bordercolor="#000000" style="margin-top:20px;">
	<thead>
		<tr>
		<td width="" rowspan="3" class="headerlong"><div align="center">No</div></td>
		<td width="" rowspan="3" class="headerlong"><div align="center">Pelajaran</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas X</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas XI</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas XII</div></td>
		</tr>
		<tr>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
		</tr>
		<tr>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
		</tr>
	</thead>
  	
	<tbody>
	<?php
		$sel = "SELECT replid FROM tingkat";
		$t = mysql_fetch_array(QueryDb($sel));
		$bt = $t['replid'] + 3;
		$sel2 = "SELECT replid FROM semester";
		$s = mysql_fetch_array(QueryDb($sel2));
		$bs = $s['replid'] + 1;
		for($h=0; $h<5; $h++){
		$sql = "SELECT replid, nama FROM pelajaran WHERE sifat_k13='$h' ORDER BY nama";
		$res = QueryDb($sql);
		echo "<tr><td colspan='8'>";
			if($h==0)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK A</b>";
			if($h==1)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK B</b>";
			if($h==2)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan IPA</b>";
			if($h==3)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan IPS</b>";
			if($h==4)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan AGAMA</b>";
		echo "</td></tr>";
		$awal = 0;
		$no = $awal + $no;
		while ($row = mysql_fetch_array($res)){
			$no++;
			echo "<tr>
				<td align='center'>".$no."</td>
				<td>".$row[nama]."</td>";
			for ($i=$t['replid']; $i < $bt; $i++) { 
				for ($j=$bs; $j >= $s['replid']; $j--) { 
				$sql2 = "SELECT n.nilaiangka FROM nap n, kelas k, infonap i, dasarpenilaian d WHERE i.replid=n.idinfo AND i.idkelas=k.replid AND k.idtingkat='$i' AND i.idsemester='$j' AND i.idpelajaran='$row[replid]' AND n.nis='$nis' AND d.dasarpenilaian='KI3' AND d.replid=n.idaturan";
				echo $sq;
				$res2 = QueryDb($sql2);
				$cek = mysql_num_rows($res2);
				if ($cek>0){
					while ($row2 = mysql_fetch_array($res2)) {
						echo "<td align='center'>".$row2[nilaiangka]."</td>";
					}
				}else{
					echo "<td></td>";
				}


				}
			}
			echo "</tr>";
			
		}
		}
	?>
	</tbody>
	</table>
	<script language='JavaScript'>
	   	Tables('table', 1, 0);
	</script>
	</td>
  </tr>
</table>

</BODY>
</HTML>
<?php
CloseDb();
?>