<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');

OpenDb();
if(isset($_REQUEST["replid"]))
	$replid = $_REQUEST["replid"];
if(isset($_REQUEST["dasarpenilaian"]))
  $dasarpenilaian = $_REQUEST["dasarpenilaian"];

$query = "SELECT u.idrpp, u.deskripsi, u.idsemester, u.idpelajaran, u.tanggal, k.idtingkat, u.kode FROM $g_db_akademik.ujian u, $g_db_akademik.kelas k WHERE u.replid = '$replid' AND u.idkelas = k.replid";
$result = QueryDb($query);
$row = @mysql_fetch_array($result);
$idrpp = $row['idrpp'];
$deskripsi = CQ($row['deskripsi']);
$tingkat = $row['idtingkat'];
$semester = $row['idsemester'];
$pelajaran = $row['idpelajaran'];
$tanggal = format_tgl_blnnmr($row['tanggal']);
$kode = $row['kode'];

if(isset($_REQUEST["deskripsi"]))
	$deskripsi = CQ($_REQUEST["deskripsi"]);
if(isset($_REQUEST["tanggal"]))
	$tanggal = $_REQUEST["tanggal"];
if(isset($_REQUEST["idrpp"]))
	$idrpp = $_REQUEST["idrpp"];
if(isset($_REQUEST["kode"]))
	$kode = $_REQUEST["kode"];

	
//$ERROR_MSG = "";
if(isset($_REQUEST["ubah"])){
	$tanggal=unformat_tgl($_REQUEST["tanggal"]);
	$deskripsi=CQ($_REQUEST["deskripsi"]);
	
	if ($idrpp == "")
		$sql_simpan="UPDATE $g_db_akademik.ujian SET idrpp=NULL, deskripsi='$deskripsi',tanggal='$tanggal', kode = '$kode' WHERE replid='$replid'";
	else
		$sql_simpan="UPDATE $g_db_akademik.ujian SET idrpp='$idrpp', deskripsi='$deskripsi',tanggal='$tanggal', kode = '$kode' WHERE replid='$replid'";
	$result_simpan=QueryDb($sql_simpan);
	if ($result_simpan) {
?>
	<script language="javascript">
        opener.refresh();
        window.close();
    </script>
<?php
	}
}	
?>
<html>
<head>
<title>LMS MAN Kota Blitar[Ubah Data Ujian]</title>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<link rel="stylesheet" type="text/css" href="../style/calendar-system.css">
<link rel="stylesheet" type="text/css" href="../style/calendar-win2k-1.css">
<script type="text/javascript" src="../script/calendar.js"></script>
<script type="text/javascript" src="../script/lang/calendar-en.js"></script>
<script type="text/javascript" src="../script/calendar-setup.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="JavaScript" src="../script/tools.js"></script>
<script language="javascript" src="../script/validasi.js"></script>
<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
function cek_form() {	
	return validateEmptyText('deskripsi', 'Deskripsi') && 
			validateEmptyText('tanggal', 'Tanggal');
}

function refresh_rpp(idrpp){
	var replid = document.ubah_ujian.replid.value;
	
	document.location.href="ubah_ujian.php?replid="+replid+"&idrpp="+idrpp+"&dasarpenilaian=<?php echo $dasarpenilaian?>";	
}

function get_rpp(tingkat,pelajaran,semester){
	newWindow('rpp_tampil.php?tingkat='+tingkat+'&semester='+semester+'&pelajaran='+pelajaran,'TambahRPP','750','450','resizable=1,scrollbars=1,status=0,toolbar=0');
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
        return false;
    }
    return true;
}
	
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="background-color:#dcdfc4" onLoad="document.getElementById('deskripsi').focus()">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="58">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
	<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
    .: Ubah Data Ujian :.
    </div>
	</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
</tr>
<tr>
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
    <td width="0" style="background-color:#FFFFFF">
    <!-- CONTENT GOES HERE //--->


    <form action="ubah_ujian.php" method="post" name="ubah_ujian" onSubmit="return cek_form()">
	<input type="hidden" name="replid" value="<?php echo $replid ?>">
	<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
	<!-- TABLE CONTENT -->
    <tr>
    	<td>Kode</td>
        <td><input type="text" name="kode" id="kode" size="25" value="<?php echo $kode?>" onKeyPress="return focusNext('idrpp', event);"></td>
    </tr>   
    <tr>
        <td><strong>Tanggal</strong></td>
        <td><input name="tanggal" type="text" class="disabled" id="tanggal" value="<?php echo $tanggal;?>" size="25" readonly onClick="Calendar.setup()"></td>
        <td width="50%"><img src="../images/calendar.jpg" name="tabel" border="0" id="btntanggal" onMouseOver="showhint('Buka kalendar!', this, event, '120px')"/></td>
    </tr>
    <tr>
      	<td><strong>KD</strong></td>
      	<td colspan="2">
        	<select name="idrpp" id="idrpp" style="width:170px;" onkeypress="return focusNext('deskripsi', event)">
            <option value="" <?php echo IntIsSelected("", $idrpp) ?> >Tanpa KD</option>
      	<?php $sql_rpp="SELECT * FROM rpp WHERE rpp='$dasarpenilaian' AND idtingkat='$tingkat' AND idsemester='$semester' AND idpelajaran='$pelajaran' AND aktif=1 ORDER BY koderpp";
      		$result_rpp=QueryDb($sql_rpp);
      		while ($row_rpp=@mysql_fetch_array($result_rpp)){
				if ($idrpp == "")
					$idrpp = $row_rpp['replid'];
      	?>
      			<option value="<?php echo $row_rpp['replid'] ?>" <?php echo IntIsSelected($row_rpp['replid'], $idrpp) ?> ><?php echo $row_rpp['koderpp'].' '.$row_rpp['deskripsi'] ?>
          		</option>
      	<?php } ?>
     
      		</select>
     		<img src="../images/ico/tambah.png" onClick="get_rpp('<?php echo $tingkat?>','<?php echo $pelajaran?>','<?php echo $semester?>')" onMouseOver="showhint('Tambah KD!', this, event, '80px')">
      	</td>
	</tr>
    <tr>
        <td><strong>Materi</strong></td>
        <td colspan="2"><input type="text" size="55" name="deskripsi" id="deskripsi" value="<?php echo $deskripsi ?>" onKeyPress="return focusNext('ubah', event)"></td>
    </tr>
    <tr>
        <td align="center" colspan="3">          
            <input type="submit" value="Simpan" name="ubah" class="but" id="ubah">
            <input type="button" value="Tutup" name="batal" class="but" onClick="window.close();">
         </td>
    </tr>
    </table>
    </form>
	<!-- END OF CONTENT //--->
    </td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
</tr>
<tr height="28">
	<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
    <td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
    <td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
</tr>
</table>
<!-- Tamplikan error jika ada -->
<?php if (strlen($ERROR_MSG) > 0) { ?>
<script language="javascript">
	alert('<?php echo $ERROR_MSG?>');
</script>
<?php } ?>

</body>
<script type="text/javascript">
  Calendar.setup(
    {
      //inputField  : "tanggalshow","tanggal"
	  inputField  : "tanggal",         // ID of the input field
      ifFormat    : "%d-%m-%Y",    // the date format
      button      : "btntanggal"       // ID of the button
    }
   );
   Calendar.setup(
    {
      //inputField  : "tanggalshow","tanggal"
	  inputField  : "tanggal",         // ID of the input field
      ifFormat    : "%d-%m-%Y",    // the date format
      button      : "tanggal"       // ID of the button
    }
   );
  
</script>
</html>
<script language="javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("deskripsi");
</script>