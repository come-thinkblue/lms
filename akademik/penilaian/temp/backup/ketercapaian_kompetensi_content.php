<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');
	require_once('../include/exceldata.php');
	require_once('../cek.php');

	$departemen = $_REQUEST['departemen'];
	$tahunajaran = $_REQUEST['tahunajaran'];
	$tingkat = $_REQUEST['tingkat'];
	$kelas = $_REQUEST['kelas'];
	$semester = $_REQUEST['semester'];
	$pelajaran = $_REQUEST['pelajaran'];
	
	$varbaris = 20;
	if (isset($_REQUEST['varbaris']))
		$varbaris = $_REQUEST['varbaris'];

	$page = 0;
	if (isset($_REQUEST['page']))
		$page = $_REQUEST['page'];

	$hal = 0;
	if (isset($_REQUEST['hal']))
		$hal = $_REQUEST['hal'];

	$urut = "kelas";
	if (isset($_REQUEST['urut']))
		$urut = $_REQUEST['urut'];
	$urutan = "ASC";
	if (isset($_REQUEST['urutan']))
		$urutan = $_REQUEST['urutan'];

	OpenDb();
	
	$op = $_REQUEST['op'];
	$id = $_REQUEST['id_ketercapaian_kompetensi'];
	if ($op == "hapus"){
		$sql = "DELETE FROM ketercapaian_kompetensi WHERE id_ketercapaian_kompetensi = $id_ketercapaian_kompetensi";
		QueryDb($sql);
		?>
			<script>refresh()</script>
		<?php
	} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Ketercapaian Kompetensi</title>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript">
			function refresh() {	
				document.location.href = "ketercapaian_kompetensi_content.php";
			}

			function tambah() {				
				newWindow('ketercapaian_kompetensi_add.php?departemen=<?php echo $departemen?>&tingkat=<?php echo $tingkat?>&semester=<?php echo $semester?>&kelas=<?php echo $kelas?>&pelajaran=<?php echo $pelajaran?>&tahunajaran=<?php echo $tahunajaran?>','Tambahketercapaian_kompetensi','600','400','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function edit(id_ketercapaian_kompetensi) {
				newWindow('ketercapaian_kompetensi_edit.php?id_ketercapaian_kompetensi='+id_ketercapaian_kompetensi, 'Ubahketercapaian_kompetensi','600','420','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function hapus(id_ketercapaian_kompetensi) {
				if (confirm('Apakah anda yakin akan menghapus ekstra kurikuler ini?'))
					document.location.href = "ketercapaian_kompetensi_content.php?op=hapus&id_ketercapaian_kompetensi="+id_ketercapaian_kompetensi;
			}

			function change_urut(urut,urutan) {
				if (urutan =="ASC"){
					urutan="DESC"
				} else {
					urutan="ASC"
				}
				document.location.href = "ketercapaian_kompetensi_content.php?urut="+urut+"&urutan="+urutan+"&page=<?php echo  $page ?>&hal=<?php echo  $hal ?>&varbaris=<?php echo  $varbaris ?>";
			}

			function cetak() {
				var urut = document.getElementById('urut').value;
				var urutan = document.getElementById('urutan').value;
				var total=document.getElementById("total").value;

				newWindow('ketercapaian_kompetensi_cetak.php?&urut='+urut+'&urutan='+urutan+'&varbaris=<?php echo  $varbaris ?>&page=<?php echo  $page ?>&total='+total, 'CetakSiswa','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function exel(){
				var urut = document.getElementById('urut').value;
				var urutan = document.getElementById('urutan').value;

				newWindow('ketercapaian_kompetensi_cetak_exel.php?urut='+urut+'&urutan='+urutan, 'Cetakketercapaian_kompetensi','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function change_page(page) {
				var varbaris=document.getElementById("varbaris").value;

				document.location.href = "ketercapaian_kompetensi_content.php?page="+page+"&urut=<?php echo  $urut ?>&urutan=<?php echo  $urutan ?>&varbaris="+varbaris+"&hal="+page;
			}

			function change_hal() {
				var hal = document.getElementById("hal").value;
				var varbaris=document.getElementById("varbaris").value;

				document.location.href="ketercapaian_kompetensi_content.php?page="+hal+"&hal="+hal+"&urut=<?php echo  $urut ?>&urutan=<?php echo  $urutan ?>&varbaris="+varbaris;
			}

			function change_baris() {
				var tingkat = document.getElementById('tingkat').value;
				var varbaris=document.getElementById("varbaris").value;

				document.location.href= "ketercapaian_kompetensi_content.php?urut=<?php echo  $urut ?>&urutan=<?php echo  $urutan ?>&varbaris="+varbaris;
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0">
		<input type="hidden" name="urut" id="urut" value="<?php echo  $urut ?>" />
		<input type="hidden" name="urutan" id="urutan" value="<?php echo  $urutan ?>" />
		
		<input type="hidden" id="departemen" value="<?php echo $departemen?>">
		<input type="hidden" id="tahunajaran" value="<?php echo $tahunajaran?>">
		<input type="hidden" id="tingkat" value="<?php echo $tingkat?>">
		<input type="hidden" id="kelas" value="<?php echo $kelas?>">
		<input type="hidden" id="semester" value="<?php echo $semester?>">
		<input type="hidden" id="pelajaran" value="<?php echo $pelajaran?>">
		
		<?php
		$sel_matpel = "SELECT pelajaran FROM ketercapaian_kompetensi WHERE pelajaran=$pelajaran";
		$rs_matpel = QueryDb($sel_matpel);
		$row_matpel = @mysql_fetch_row($rs_matpel);
		//echo "$row_matpel[0]";
		?>
		
		<table border="0" width="90%" align="center">
			<!-- TABLE CENTER -->
			<tr>
				<td align="right">
					<input type="hidden" name="total" id="total" value="<?php echo  $total ?>"/>
					<table width="100%" border="0" align="center">
						<tr>
							<td align="right">
								<a href="#" onClick="refresh()">
									<img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh
								</a>&nbsp;&nbsp;
								<a href="#" onClick="JavaScript:exel()">
									<img src="../images/ico/excel.png" border="0" onMouseOver="showhint('Cetak dalam format Excel!', this, event, '80px')"/>&nbsp;Cetak Excel
								</a>&nbsp;&nbsp;
								<a href="JavaScript:cetak()">
									<img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak
								</a>&nbsp;&nbsp; 
								<a href="#" onClick="JavaScript:tambah()">
									<img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')" />&nbsp;Tambah Ketercapaian Kompetensi
								</a>
							</td>
						</tr>    
					</table>
					<br />       
					<table border="1" width="100%" id="table" class="tab" align="center" style="border-collapse:collapse" bordercolor="#000000" >
						<tr class="header" height="30" align="center">		
							<td width="4%">No</td>
							<td width="20%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('aspek_penilaian','<?php echo  $urutan ?>')">Aspek Penilaian <?php echo  change_urut('aspek_penilaian', $urut, $urutan) ?></td>
							<td width="20%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('predikat','<?php echo  $urutan ?>')">Predikat <?php echo  change_urut('predikat', $urut, $urutan) ?></td>
							<td width="*"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('deskripsi','<?php echo  $urutan ?>')">Ketrcapaian Kompetensi<?php echo  change_urut('deskripsi', $urut, $urutan) ?></td>
							<td width="10%">&nbsp;</td>
						</tr>
						<?php
						CloseDb();
						if ($page == 0) {
							$cnt = 1;
						} else {
							$cnt = (int) $page * (int) $varbaris + 1;
						}
						
						
						OpenDb();							
						//cek syncron
						$sql_pel = "SELECT aspek_penilaian FROM aspek_penilaian_per_pelajaran";
						$rs_pel = QueryDb($sql_pel);
						$row_pel = @mysql_fetch_row($rs_pel);
						$pecah_aspek = explode(',',$row_pel[0]);
						$jum_aspek = count($pecah_aspek);
						
						//cek total predikat
						$sql_jumgrade = "SELECT count(predikat) FROM gradenilai";
						$rs_jumgrade = QueryDb($sql_jumgrade);
						$row_jumgrade = @mysql_fetch_row($rs_jumgrade);
						
						//cek predikat
						$sql_grade = "SELECT predikat FROM gradenilai ORDER BY huruf";
						$rs_grade = QueryDb($sql_grade);
						$no = 0;
						while($row_grade = @mysql_fetch_array($rs_grade)){
							$hasil[$no] = $row_grade[0];
							$no++;
						}
						CloseDb();
											
						for($i=0;$i<$jum_aspek;$i++){
							for($j=0;$j<$row_jumgrade[0];$j++){
								?>
								<tr>
									<td height="25" align="center"><?php echo  $cnt ?></td>
									<td height="25" align="center"><?php echo  $pecah_aspek[$i] ?></td>
									<td height="25" align="center"><?php echo  $hasil[$j] ?></td>
									<td height="25" align="center"></td>
									<td height="25" align="center">
										<a href="JavaScript:edit(<?php echo $row[0]?>)" /><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Ektra Kulikuler!', this, event, '80px')"/></a>&nbsp;
										<a href="JavaScript:hapus(<?php echo $row[0] ?>)"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Data Ekstra Kurikuler!', this, event, '80px')"/></a>
									</td>
								</tr>
								<?php
								$cnt++;
							}
						}
						?>
						<!-- END TABLE CONTENT -->
					</table>

					<script language='JavaScript'>
						Tables('table', 1, 0);
					</script>
				</td>
			</tr> 
		</table>  
		<?php
		CloseDb();
		?>
	</body>
</html>