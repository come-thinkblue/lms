<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	$departemen = $_REQUEST['departemen'];
	if (isset(
	$_REQUEST['departemen']))
		$departemen = CQ($_REQUEST['departemen']);
	
	$tahunajaran = $_REQUEST['tahunajaran'];
	if (isset($_REQUEST['tahunajaran']))
		$tahunajaran = CQ($_REQUEST['tahunajaran']);
	
	$tingkat = $_REQUEST['tingkat'];
	if (isset($_REQUEST['tingkat']))
		$tingkat = $_REQUEST['tingkat'];
	
	$kelas = $_REQUEST['kelas'];
	if (isset($_REQUEST['kelas']))
		$kelas = $_REQUEST['kelas'];
	
	$semester = $_REQUEST['semester'];
	if (isset($_REQUEST['semester']))
		$semester = CQ($_REQUEST['semester']);
	
	$pelajaran = $_REQUEST['pelajaran'];
	if (isset($_REQUEST['pelajaran']))
		$pelajaran = CQ($_REQUEST['pelajaran']);
	
	if (isset($_REQUEST['id_ketercapaian_kompetensi']))
		$id_ketercapaian_kompetensi = CQ($_REQUEST['id_ketercapaian_kompetensi']);
	if (isset($_REQUEST['predikat']))
		$predikat = CQ($_REQUEST['predikat']);
	if (isset($_REQUEST['nilai_1']))
		$nilai_1 = CQ($_REQUEST['nilai_1']);
	if (isset($_REQUEST['nilai_2']))
		$nilai_2 = CQ($_REQUEST['nilai_2']);
	if (isset($_REQUEST['nilai_3']))
		$nilai_3 = CQ($_REQUEST['nilai_3']);
		
	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		$sql_cek = "SELECT * FROM ketercapaian_kompetensi WHERE tingkat='$tingkat' AND kelas='$kelas' AND pelajaran='$pelajaran' 
					AND tahunajaran='$tahunajaran' AND semester='$semester'";
		$result_cek = QueryDb($sql_cek);

		if (@mysql_num_rows($result_cek) > 0) {
			CloseDb();
			$ERROR_MSG = "Data dengan spesifikasi tersebut sudah a";
		} 
		else {
			$sql = "INSERT INTO ekskul SET tingkat='$tingkat', kelas='$kelas', semester='$semester', pelajaran='$pelajaran', tahunajaran='$tahunajaran', predikat='$predikat', nilai_1='$nilai_1', nilai_2='$nilai_2', nilai_3='$nilai_3'";
			echo "$sql";
			$result = QueryDb($sql);
			if ($result) {
				?>
				<script language="javascript">
					opener.refresh();
					window.close();
				</script> 
			<?php
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Tambah Ketercapaian Kompetensi]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function validate() {
				return validateEmptyText('nama_ekskul', 'Nama Ekstra Kurikuler') && 
					validateEmptyText('nip', 'NIP dan Nama Pembina');
			}

			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nip')
						caripegawai();
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('nama_ekskul','keterangan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('predikat').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo  GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Tambah Ketercapaian Kompetensi :.
					</div>
				</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="urut" id="urut" value="<?php echo  $urut ?>"/>
						<input type="hidden" name="urutan" id="urutan" value="<?php echo  $urutan ?>"/>

						<input type="hidden" id="departemen" value="<?php echo $departemen?>">
						<input type="hidden" id="tahunajaran" value="<?php echo $tahunajaran?>">
						<input type="hidden" id="tingkat" value="<?php echo $tingkat?>">
						<input type="hidden" id="kelas" value="<?php echo $kelas?>">
						<input type="hidden" id="semester" value="<?php echo $semester?>">
						<input type="hidden" id="pelajaran" value="<?php echo $pelajaran?>">
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT max(id_ketercapaian_kompetensi) FROM ketercapaian_kompetensi";
								$rs = QueryDb($sql);
								$next_id = mysql_fetch_row($rs);
								
								$sql_tingkat = "SELECT tingkat FROM tingkat WHERE replid=$tingkat";
								$result_tingkat = QueryDb($sql_tingkat);
								$row_tingkat = @mysql_fetch_row($result_tingkat);
								
								$sql_kelas = "SELECT kelas FROM kelas WHERE replid=$kelas";
								$result_kelas = QueryDb($sql_kelas);
								$row_kelas = @mysql_fetch_row($result_kelas);
								
								$sql_semester = "SELECT semester FROM semester WHERE replid=$semester";
								$result_semester = QueryDb($sql_semester);
								$row_semester = @mysql_fetch_row($result_semester);
								
								$sql_tahunajaran = "SELECT tahunajaran FROM tahunajaran WHERE replid=$tahunajaran";
								$result_tahunajaran = QueryDb($sql_tahunajaran);
								$row_tahunajaran = @mysql_fetch_row($result_tahunajaran);
							?>
							<tr>
								<td><strong>Id Ketercapaian Kompetensi</strong></td>
								<td><input type="text" class="disabled" name="id_ketercapaian_kompetensi" id="id_ketercapaian_kompetensi" value="<?php echo  $next_id[0]+1?>" size="5" readonly /></td>
							</tr>
							<tr>
								<td><strong>Tingkat</strong></td>
								<td><input type="text" class="disabled" name="tingkat" id="tingkat" size="10" value="<?php echo  $row_tingkat[0] ?>" readonly /></td>
							</tr>
							<tr>
								<td><strong>Kelas</strong></td>
								<td><input type="text" class="disabled" name="kelas" id="kelas" size="10" value="<?php echo  $row_kelas[0]?>" readonly /></td>
							</tr>
							<tr>
								<td><strong>Semester</strong></td>
								<td><input type="text" class="disabled" name="semester" id="semester" size="10" value="<?php echo  $row_semester[0]?>" readonly /></td>
							</tr>
							<tr>
								<td><strong>Tahun Ajaran</strong></td>
								<td><input type="text" class="disabled" name="tahunajaran" id="tahunajaran" size="10" value="<?php echo  $row_tahunajaran[0]?>" readonly /></td>
							</tr>
							<tr>
								<td><strong>Predikat</strong></td>
								<td>
									<select name="predikat" id="predikat" onFocus="showhint('Nama Predikat tidak boleh kosong !', this, event, '120px');panggil('predikat')" value="<?php echo  $predikat?>" onKeyPress="return focusNext('nilai_1', event)">
										<option value="">--Pilih Predikat--</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><strong>Nilai 1</strong></td>
								<td>
									<textarea name="nilai_1" id="nilai_1" rows="5" cols="45"  onKeyPress="return focusNext('nilai_2', event)" onFocus="panggil('nilai_1')"><?php echo  $nilai_1 ?></textarea>
								</td>
							</tr>
							<tr>
								<td><strong>Nilai 2</strong></td>
								<td>
									<textarea name="nilai_2" id="nilai_2" rows="5" cols="45"  onKeyPress="return focusNext('nilai_3', event)" onFocus="panggil('nilai_1')"><?php echo  $nilai_2 ?></textarea>
								</td>
							</tr>
							<tr>
								<td><strong>Nilai 3</strong></td>
								<td>
									<textarea name="nilai_3" id="nilai_3" rows="5" cols="45"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('nilai_1')"><?php echo  $nilai_3 ?></textarea>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo  GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo  GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo  $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>