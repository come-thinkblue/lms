<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');
require_once('../library/dpupdate.php');

if(isset($_REQUEST["tahun"]))
	$tahun = $_REQUEST["tahun"];
if(isset($_REQUEST["departemen"]))
	$departemen = $_REQUEST["departemen"];
if(isset($_REQUEST["tingkat"]))
	$tingkat = $_REQUEST["tingkat"];
if(isset($_REQUEST["semester"]))
	$semester = $_REQUEST["semester"];
if(isset($_REQUEST["kelas"]))
	$kelas = $_REQUEST["kelas"];
if(isset($_REQUEST["nip"]))
	$nip = $_REQUEST["nip"];

$warna=array('fcf5ca','d5fcca','cafcf3','cae6fc','facafc');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Menu</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../style/style.css">
<script language="JavaScript" src="../script/tables.js"></script>
<script language="JavaScript">

function klik(idpelajaran,aspek,aspekket,kelas,semester,nip,tingkat,departemen,tahun){

	parent.penentuan_content.location.href="penentuan_content.php?pelajaran="+idpelajaran+"&aspek="+aspek+"&aspekket="+aspekket+"&kelas="+kelas+"&semester="+semester+"&nip="+nip+"&tingkat="+tingkat+"&departemen="+departemen+"&tahun="+tahun;
}
//function klik(departemen,tingkat,idpelajaran,kelas,semester,nip){
//	parent.penentuan_content.location.href="penentuan_content.php?departemen="+departemen+"&tingkat="+tingkat+"&pelajaran="+idpelajaran+"&kelas="+kelas+"&semester="+semester+"&nip="+nip;
//}
</script>
</head>
<body topmargin="0" leftmargin="0">
<?php
OpenDb();
$query_aturan = "SELECT DISTINCT g.idpelajaran, p.nama, t.idkurikulum 
				   FROM $g_db_akademik.jadwal g, $g_db_akademik.infojadwal i, $g_db_akademik.pelajaran p, 
				   		$g_db_akademik.aturannhb a, $g_db_akademik.tingkat t 
				   WHERE g.nipguru = '$nip' AND g.idpelajaran=p.replid AND g.idkelas='$kelas' 
				   		AND a.idtingkat=t.replid AND p.departemen='$departemen' AND  g.infojadwal=i.replid
				    	AND i.idtahunajaran='$tahun' AND a.idtingkat='$tingkat' AND a.aktif = 1 
				    	AND i.aktif=1 ORDER BY p.nama";

$result_aturan = QueryDb($query_aturan);
if (!mysql_num_rows($result_aturan)==0){ ?>

<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
<!-- TABLE CONTENT -->
    
<tr height="30">    	
    <td width="100%" class="header" align="center">Pelajaran</td>
</tr>	
<?php  $cnt = 0;
	while ($row_aturan=@mysql_fetch_array($result_aturan)) 
	{
		$idpelajaran = $row_aturan['idpelajaran'];
		$sql = "SELECT DISTINCT a.dasarpenilaian, dp.keterangan
						FROM $g_db_akademik.aturannhb a, $g_db_akademik.dasarpenilaian dp, $g_db_akademik.guru g
						WHERE g.idpelajaran =  '$row_aturan[idpelajaran]'
						AND a.dasarpenilaian = dp.dasarpenilaian AND a.idkurikulum ='$row_aturan[idkurikulum]'
						AND idtingkat = '$tingkat' AND nip = '$nip' ORDER BY keterangan";
		$res = QueryDb($sql); ?>
<tr>   	
    <td align="left" height="25">
    <b><font style="font-size:14px; font-family:Arial;"><?php echo $row_aturan['nama']?></font>:</b><br />
<?php		while($row = mysql_fetch_array($res)) 
		{ ?>
		<a href="#" onclick="klik('<?php echo $row_aturan['idpelajaran']?>','<?php echo $row['dasarpenilaian']?>','<?php echo $row['keterangan']?>','<?php echo $kelas?>','<?php echo $semester?>','<?php echo $nip?>','<?php echo $tingkat?>','<?php echo $departemen?>','<?php echo $tahun?>')"><font color="#0000FF"><strong><?php echo $row['keterangan']?></strong></font></a><br />
<?php		} ?>	
    </td>
</tr>
<!-- END TABLE CONTENT -->
<?php
 	} // while
?>	
</table>
<script language='JavaScript'>
	Tables('table', 1, 0);
</script>
<?php 
} 
else 
{ 
?>
<table width="100%" border="0" align="center">          
<tr>
    <td align="center" valign="middle" height="300">
    <font size = "2" color ="red"><b>Tidak ditemukan adanya data. <br /><br />Tambah aturan perhitungan grading nilai pelajaran yang akan diajar oleh guru <?php echo $_REQUEST['nama']?> di menu Aturan Perhitungan Grading Nilai pada bagian Guru & Pelajaran. </b></font>
    </td>
</tr>
</table> 
<?php 
} 
?> 
</body>
<?php CloseDb(); ?>
</html>