<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();


header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msword'); // Other browsers  
header('Content-Disposition: attachment; filename=Nilai_Pelajaran.doc');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
/**/
OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
	

$sql_get_siswa="SELECT nis,nama FROM $g_db_akademik.siswa WHERE idkelas='$kelas' AND aktif = 1 ORDER BY nama";
$result_get_siswa=QueryDb($sql_get_siswa);

$sql_get_siswa1="SELECT nis,nama FROM $g_db_akademik.siswa WHERE idkelas='$kelas' AND aktif = 1 ORDER BY nama";
$result_get_siswa1=QueryDb($sql_get_siswa1);

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="tes_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>1</o:TotalTime>
  <o:Created>2008-06-19T02:21:00Z</o:Created>
  <o:LastSaved>2008-06-19T02:22:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
	<?php
$cnt_siswa=1;
while ($row_get_siswa=@mysql_fetch_array($result_get_siswa)){
	$nis = $row_get_siswa['nis'];
	$nama = $row_get_siswa['nama'];
	$sql="SELECT * FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result=QueryDb($sql);
	$row=@mysql_fetch_array($result);
	
	if($row[kelamin]=='L')
		$gender = 'Laki Laki';
	else
		$gender = 'Perempuan';

?>
@page Section<?php echo $cnt_siswa?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa?>
	{page:Section<?php echo $cnt_siswa?>;}
@page Section<?php echo $cnt_siswa+1?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa+1?>
	{page:Section<?php echo $cnt_siswa+1?>;}
@page Section<?php echo $cnt_siswa+2?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa+2?>
	{page:Section<?php echo $cnt_siswa+2?>;}
@page Section<?php echo $cnt_siswa+3?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa+3?>
	{page:Section<?php echo $cnt_siswa+3?>;}
	<?php
	$cnt_siswa=$cnt_siswa+3;
	}
	?>
.style1 {font-weight: bold}
.style1 {
	color: #000000;
	font-weight: bold;
}
.style2 {
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
}
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style4 {font-size: 12px}
.style10 {font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style11 {font-size: 14px}
.style12 {font-size: 13px}
.style13 {font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style14 {font-size: 16px}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>

<body lang=EN-US style='tab-interval:36.0pt'>

<?php
$cnt_siswa1=1;
while ($row_siswa1=@mysql_fetch_array($result_get_siswa1)){
	$nis = $row_siswa1['nis'];
	$nama = $row_siswa1['nama'];
	$sql="SELECT * FROM $g_db_akademik.siswa WHERE nis='$nis'";
	$result=QueryDb($sql);
	$row=@mysql_fetch_array($result);
	
	if($row[kelamin]=='L')
		$gender = 'Laki Laki';
	else
		$gender = 'Perempuan';
?>
<div class=Section<?php echo $cnt_siswa1?>>
<!--                      Hal 1             -->
<table width="100%" border="0">
  <tr>
    <td>
	<table width="100%" border="0" bordercolor="#666666" cellpadding="0" cellspacing="0">
	<tr>
		<td width='30%'>Foto</td>
		<td width='30%' colspan='3' height='30px'><b>I. Siswa</b></td>
		<td colspan='4'><b>Nomor Induk Siswa :  &nbsp;&nbsp;<?php echo $nis;?></b></td>
	</tr>
	<tr>
		<td></td>
		<td>1</td>
		<td>Nama Siswa</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[nama]; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>2</td>
		<td>Jenis Kelamin</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $gender?></td>
	</tr>
	<tr>
		<td></td>
		<td>3</td>
		<td>Tempat dan Tgl. Lahir</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[tmplahir].",".$row[tgllahir] ?></td>
	</tr>
	<tr>
		<td></td>
		<td>4</td>
		<td>Anak ke</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[anakke]; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>5</td>
		<td>Jumlah Saudara</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Kandung</td>
		<td colspan='2' width='50%'>&nbsp;&nbsp;<?php echo $row[jsaudara]; ?></td>
		<td width='50%'>&nbsp;&nbsp;Orang</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>&nbsp;&nbsp;Tiri</td>
		<td colspan='2' width='50%'><hr></td>
		<td width='50%'>&nbsp;&nbsp;Orang</td>
	</tr>
	<tr>
		<td></td>
		<td>6</td>
		<td>Alamat</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[alamatsiswa]?></td>
	</tr>
	
	<!--orang tua-->
	<tr>
		<td width='15%'></td>
		<td width='20%' colspan='6' height='30px'><b>II. Orang Tua / Wali</b></td>		
	</tr>
	<tr>
		<td></td>
		<td>7</td>
		<td>Nama Orang Tua</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Ayah</td>
		<td width='30%'>&nbsp;&nbsp;<?php echo $row[namaayah]?></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Ibu&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row[namaibu]?></td>
		
	</tr>
	<tr>
		<td></td>
		<td>8</td>
		<td>Pekerjaan</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Ayah</td>
		<td width='30%'><hr></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Ibu</td>
	</tr>
	<tr>
		<td></td>
		<td>9</td>
		<td>Agama</td>
		<td>:</td>
		<td>&nbsp;&nbsp;Ayah</td>
		<td width='30%'><hr></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Ibu</td>
	</tr>
	<tr>
		<td></td>
		<td>10</td>
		<td>Alamat</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[alamatortu]?></td>
	</tr>
	<tr>
		<td></td>
		<td>11</td>
		<td>Nama Wali</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[namaayah]?></td>
	</tr>
	<tr>
		<td></td>
		<td>12</td>
		<td>Pekerjaan</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>13</td>
		<td>Agama</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>14</td>
		<td>Alamat</td>
		<td>:</td>
		<td colspan='4'>&nbsp;&nbsp;<?php echo $row[alamatortu]?></td>
	</tr>
	<tr>
		<td width='30%'>Foto</td>
		<td width='30%' colspan='6' height='30px'><b>III. MASUK MADRASAH INI</b></td>
		
	</tr>
	<tr>
		<td></td>
		<td>15</td>
		<td>Dari Sekolah/Madrasah</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>16</td>
		<td>Tanggal</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>17</td>
		<td>Surat Pindah</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>18</td>
		<td>No. Induk Sekolah Asal</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>19</td>
		<td>Program Pilihan</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td>20</td>
		<td>Nilai UN/UAM MTs/SLTP</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td></td>
		<td colspan='7'>
			<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
				<tr>
					<td rowspan='2' align='center' width='5%'><b>No. Urut</b></td>
					<td rowspan='2' align='center'><b>MATA PELAJARAN</b></td>
					<td align='center' colspan='2'><b>NILAI</b></td>
					<td rowspan='2' align='center'><b>KETERANGAN</b></td>
				</tr>
				<tr>
					<td align='center'><b>Angka</b></td>
					<td align='center'><b>STTB/IJAZAH</b></td>
				</tr>
				<tr>
					<td align='center'>1</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>2</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>3</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>4</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>5</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>6</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align='center'>7</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='15%'></td>
		<td width='20%' colspan='6' height='30px'><b>III. MENINGGALKAN K. MADRASAH INI</b></td>
	</tr>
	<tr>
		<td></td>
		<td>21</td>
		<td>Tanggal</td>
		<td>:</td>
		<td width='30%' colspan='2'><hr></td>
		<td width='50%' colspan='2'>&nbsp;&nbsp;Kelas</td>
	</tr>
	<tr>
		<td></td>
		<td>22</td>
		<td>Alasan</td>
		<td>:</td>
		<td colspan='4'><hr></td>
	</tr>
	<tr>
		<td width='30%'>Foto</td>
		<td width='20%' colspan='6' height='30px'><b>IV. P	EMBAYARAN UANG SEKOLAH/SPP</b></td>
	</tr>
	<tr>
		<td></td>
		<td colspan='7'>
			<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
				<tr>
					<td rowspan='2' align='center' width='5%'><b>No. Urut</b></td>
					<td rowspan='2' align='center'><b>WAJIB BAYAR</b></td>
					<td align='center' colspan='4'><b>TAHUN PELAJARAN</b></td>
				</tr>
				<tr>
					<td align='center'>&nbsp;&nbsp;</td>
					<td align='center'>&nbsp;&nbsp;</td>
					<td align='center'>&nbsp;&nbsp;</td>
					<td align='center'>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td align='center'>1</td>
					<td>Nama</td>
					<td colspan='4'></td>
				</tr>
				<tr>
					<td align='center'>2</td>
					<td>Alamat</td>
					<td colspan='4'></td>
				</tr>
				<tr>
					<td align='center'>3</td>
					<td>Penghasilan Orang Tua</td>
					<td colspan='4'></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
    
	</td>
</tr>

<!-- batas-->
 </table>

</div>

<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
mso-break-type:section-break'>
</span>

<div class=Section<?php echo $cnt_siswa1+1?>>
<!--                      Hal 2             -->
<table width="100%" border="0">
		<tr>
	<td>
	<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
		<td width="" rowspan="3" class="headerlong"><div align="center">No</div></td>
		<td width="" rowspan="3" class="headerlong"><div align="center">Pelajaran</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas X</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas XI</div></td>
		<td width="100" class="headerlong" colspan="2"><div align="center">Kelas XII</div></td>
		</tr>
		<tr>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
			<td width="100" class="headerlong" colspan="2"><div align="center">Semester</div></td>
		</tr>
		<tr>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
			<td width="50" align='center'>I</td>
			<td width="50" align='center'>II</td>
		</tr>
	</thead>
  	
	<tbody>
	<?php
		$sel = "SELECT replid FROM tingkat";
		$t = mysql_fetch_array(QueryDb($sel));
		$bt = $t['replid'] + 3;
		$sel2 = "SELECT replid FROM semester";
		$s = mysql_fetch_array(QueryDb($sel2));
		$bs = $s['replid'] + 1;
		for($h=0; $h<5; $h++){
		$sql = "SELECT replid, nama FROM pelajaran WHERE sifat_k13='$h' ORDER BY nama";
		$res = QueryDb($sql);
		echo "<tr><td colspan='8'>";
			if($h==0)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK A</b>";
			if($h==1)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK B</b>";
			if($h==2)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan IPA</b>";
			if($h==3)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan IPS</b>";
			if($h==4)
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>KELOMPOK Peminatan AGAMA</b>";
		echo "</td></tr>";
		$no = $awal + $no;
		while ($row = mysql_fetch_array($res)){
		$no++;
			echo "<tr>
				<td align='center'>".$no."</td>
				<td>".$row[nama]."</td>";
			for ($i=$t['replid']; $i < $bt; $i++) { 
				for ($j=$bs; $j >= $s['replid']; $j--) { 
				$sql2 = "SELECT n.nilaiangka FROM nap n, kelas k, infonap i, dasarpenilaian d WHERE i.replid=n.idinfo AND i.idkelas=k.replid AND k.idtingkat='$i' AND i.idsemester='$j' AND i.idpelajaran='$row[replid]' AND n.nis='$nis' AND d.dasarpenilaian='KI3' AND d.replid=n.idaturan";
				echo $sq;
				$res2 = QueryDb($sql2);
				$cek = mysql_num_rows($res2);
				if ($cek>0){
					while ($row2 = mysql_fetch_array($res2)) {
						echo "<td align='center'>".$row2[nilaiangka]."</td>";
					}
				}else{
					echo "<td></td>";
				}


				}
			}
			echo "</tr>";
		}
		}
	?>
	</tbody>
	</table>
	</td>
  </tr>
	</table>
</div>
<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
mso-break-type:section-break'>
</span>
<?php
$no = 0;
	$cnt_siswa1=$cnt_siswa1+2;
}
?>
</body>

</html>
<?php
CloseDb();
?>