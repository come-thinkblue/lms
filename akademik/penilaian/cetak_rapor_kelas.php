<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
//require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/theme.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../include/getheader.php');
require_once('../include/numbertotext.class.php');
require_once('../library/dpupdate.php');

$NTT = new NumberToText();


header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/w-msword'); // Other browsers  
header('Content-Disposition: attachment; filename=Nilai_Pelajaran.doc');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
/**/
OpenDb();

if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
if (isset($_REQUEST['semester']))
	$semester = $_REQUEST['semester'];
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
if (isset($_REQUEST['tahunajaran']))
	$tahunajaran = $_REQUEST['tahunajaran'];
if (isset($_REQUEST['pelajaran'])) 
	$pelajaran = $_REQUEST['pelajaran'];
if (isset($_REQUEST['kelas']))
	$kelas = $_REQUEST['kelas'];
if (isset($_REQUEST['harian']))
	$harian = $_REQUEST['harian'];
if (isset($_REQUEST['prespel']))
	$prespel = $_REQUEST['prespel'];
	
//alamat sekolah
	$sql_alamat = "SELECT alamat1 FROM identitas WHERE departemen='$departemen'";
	$res_alamat = QueryDb($sql_alamat);
	$row_alamat =@mysql_fetch_array($res_alamat);
	
$sql_ta="SELECT * FROM $g_db_akademik.tahunajaran WHERE replid='$tahunajaran'";
$result_ta=QueryDb($sql_ta);
$row_ta=@mysql_fetch_array($result_ta);
$tglawal=$row_ta['tglmulai'];
$tglakhir=$row_ta['tglakhir'];	

$sql_get_siswa="SELECT nis,nama FROM $g_db_akademik.siswa WHERE idkelas='$kelas' AND aktif = 1 ORDER BY nama";
$result_get_siswa=QueryDb($sql_get_siswa);

$sql_get_siswa1="SELECT nis,nama FROM $g_db_akademik.siswa WHERE idkelas='$kelas' AND aktif = 1 ORDER BY nama";
$result_get_siswa1=QueryDb($sql_get_siswa1);

$sql = "SELECT k.kelas AS namakelas, s.semester AS namasemester, a.tahunajaran, t.tingkat, l.nama, a.departemen FROM $g_db_akademik.kelas k, $g_db_akademik.semester s, $g_db_akademik.tahunajaran a, $g_db_akademik.tingkat t, $g_db_akademik.pelajaran l WHERE k.replid = $kelas AND s.replid = $semester AND a.replid = $tahunajaran AND k.idtahunajaran = a.replid AND k.idtingkat = t.replid";// AND l.replid = $pelajaran";
$result = QueryDb($sql);
$row = @mysql_fetch_array($result);
	
$sql_get_w_kls="SELECT p.nama as namawalikelas, p.nip as nipwalikelas FROM $g_db_pegawai.pegawai p, $g_db_akademik.kelas k WHERE k.replid='$kelas' AND k.nipwali=p.nip";
$rslt_get_w_kls=QueryDb($sql_get_w_kls);
$row_get_w_kls=@mysql_fetch_array($rslt_get_w_kls);
	
$sql_get_kepsek="SELECT d.nipkepsek as nipkepsek,p.nama as namakepsek FROM $g_db_pegawai.pegawai p, $g_db_akademik.departemen d WHERE  p.nip=d.nipkepsek AND d.departemen='$departemen'";
//echo $sql_get_kepsek;
$rslt_get_kepsek=QueryDb($sql_get_kepsek);
$row_get_kepsek=@mysql_fetch_array($rslt_get_kepsek);


$namakelas = $row[0];
$namasemester = $row[1];
$namatahunajaran = $row[2];
$namatingkat = $row[3];
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 11">
<meta name=Originator content="Microsoft Word 11">
<link rel=File-List href="tes_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>user</o:Author>
  <o:LastAuthor>user</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>1</o:TotalTime>
  <o:Created>2008-06-19T02:21:00Z</o:Created>
  <o:LastSaved>2008-06-19T02:22:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Characters>2</o:Characters>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>2</o:CharactersWithSpaces>
  <o:Version>11.5606</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Print</w:View>
 <w:Zoom>100</w:Zoom>
  <w:GrammarState>Clean</w:GrammarState>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" LatentStyleCount="156">
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
	<?php
$cnt_siswa=1;
while ($row_get_siswa=@mysql_fetch_array($result_get_siswa)){
	$nis = $row_get_siswa['nis'];
	$nama = $row_get_siswa['nama'];
?>
@page Section<?php echo $cnt_siswa?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa?>
	{page:Section<?php echo $cnt_siswa?>;}
@page Section<?php echo $cnt_siswa+1?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa+1?>
	{page:Section<?php echo $cnt_siswa+1?>;}
@page Section<?php echo $cnt_siswa+2?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa+2?>
	{page:Section<?php echo $cnt_siswa+2?>;}
@page Section<?php echo $cnt_siswa+3?>
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section<?php echo $cnt_siswa+3?>
	{page:Section<?php echo $cnt_siswa+3?>;}
	<?php
	$cnt_siswa=$cnt_siswa+3;
	}
	?>
.style1 {font-weight: bold}
.style1 {
	color: #000000;
	font-weight: bold;
}
.style2 {
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
}
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style4 {font-size: 12px}
.style10 {font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style11 {font-size: 14px}
.style12 {font-size: 13px}
.style13 {font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style14 {font-size: 16px}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-ansi-language:#0400;
	mso-fareast-language:#0400;
	mso-bidi-language:#0400;}
</style>
<![endif]-->
</head>

<body lang=EN-US style='tab-interval:36.0pt'>

<?php
$cnt_siswa1=1;
while ($row_siswa1=@mysql_fetch_array($result_get_siswa1)){
	$nis = $row_siswa1['nis'];
	$nama = $row_siswa1['nama'];
?>
<div class=Section<?php echo $cnt_siswa1?>>
<?php echo getHeader($departemen)?>
<table width="100%" border="0">
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#666666">
  <tr>
    <td height="20" width="35%"><span class="style5">Nama Sekolah</span></td>
    <td height="20" width="30%"><span class="style5">:&nbsp;
        <?php echo $departemen?>
    </span></td>
	<td height="20" width="30%"><span class="style5">Kelas</span></td>
    <td height="20" width="30%"><span class="style5">
		:&nbsp;<?php echo $namatingkat.'-'.$namakelas;?>
    </span></td>
  </tr>
  <tr>
    <td height="20"><span class="style5">Alamat</span></td>
    <td height="20"><span class="style5">:&nbsp;
		Jl. Jati No.78 Blitar
		<!-- alamat ada web dan telp
		<?php echo $row_alamat[alamat1]; ?>-->
    </span></td>
	<td height="20"><span class="style5">Semester</span></td>
    <td height="20"><span class="style5">
		:&nbsp;<?php echo $namasemester?>
    </span></td>
  </tr>
  <tr>
    <td height="20"><span class="style5">Nama Peserta Didik</span></td>
	<td height="20"><span class="style5">:&nbsp;
	    <?php echo $nama;?>
	</span></td>
    <td width="6%" height="20"><span class="style5">Tahun Pelajaran
    </span></td>
    <td width="93%" height="20"><span class="style5">:&nbsp;
        <?php echo $namatahunajaran?>
    </span></td>
  </tr>
  <tr>
    <td width="6%" height="20"><span class="style5">No. Induk / NISN 
    </span></td>
    <td width="93%" height="20"><span class="style5">:&nbsp;
        <?php echo $nis?>
    </span></td>
  </tr>
</table>
</td>
  </tr>
  <tr>
  	<td><br><h2><strong>A. Sikap</strong></h2></td>
  </tr>
  <tr>
  	<td><strong>1. Sikap Spiritual</strong></td>
  </tr>
  <tr>
  	<td><strong>Deskripsi</strong></td>
  </tr>
  <?php 
  	//idaturan
  $sql= "SELECT keterangan FROM sas s, master_sikap m 
  			WHERE s.nis='$nis' AND s.idsikap=m.replid AND s.idaturan=4
  			AND idkelas='$kelas' AND idsemester='$semester'";
  $res = QueryDb($sql);
  $row = @mysql_fetch_array($res);
  echo "<tr><td>".$row['keterangan']."</td></tr>";
  ?>
   <tr>
  	<td><br><strong>2. Sikap Sosial</strong></td>
  </tr>
  <tr>
  	<td><strong>Deskripsi</strong></td>
  </tr>
  <?php 
  	//idaturan
  $sql= "SELECT keterangan FROM sas s, master_sikap m 
  			WHERE s.nis='$nis' AND s.idsikap=m.replid AND s.idaturan=5
  			AND idkelas='$kelas' AND idsemester='$semester'";
			//echo $sql;
  $res = QueryDb($sql);
  $row = @mysql_fetch_array($res);
  echo "<tr><td>".$row['keterangan']."</td></tr>";
  ?>
  <tr>
  	<td><br><h2><strong>B. Pengetahuan dan Ketrampilan</strong></h2></td>
  </tr>
  <tr>
	<td>
<?php	$sql = "SELECT DISTINCT d.dasarpenilaian, d.keterangan
		  	    FROM infonap i, nap n, dasarpenilaian d
			   WHERE i.replid = n.idinfo AND n.nis = '$nis' 
			     AND i.idsemester = '$semester' 
			     AND i.idkelas = '$kelas'
			     AND n.idaturan = d.replid";
			     
	$res = QueryDb($sql);
	$i = 0;
	while($row = mysql_fetch_row($res))
	{
		$aspekarr[$i++] = array($row[0], $row[1]);
	} ?>  
	<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
	<tr>
		<td width="18%" rowspan="2" class="headerlong"><div align="center">Pelajaran</div></td>
		<td width="7%" rowspan="2" class="headerlong"><div align="center">KKM</div></td>
<?php		for($i = 0; $i < count($aspekarr); $i++)
			echo "<td class='headerlong' colspan='3' align='center' width='18%'>" . $aspekarr[$i][1] . "</td>"; ?>
  	</tr>
	<tr>
<?php	for($i = 0; $i < count($aspekarr); $i++)
		echo "<td class='header' align='center' width='7%'>Angka</td>
			   <td class='header' align='center' width='7%'>Predikat</td>
				<td class='header' align='center' width='20%'>Deskripsi</td>"; ?>   
   </tr>
<?php	$sql = "SELECT pel.replid, pel.nama
				 FROM ujian uji, nilaiujian niluji, siswa sis, pelajaran pel 
				WHERE uji.replid = niluji.idujian 
				  AND niluji.nis = sis.nis 
				  AND uji.idpelajaran = pel.replid 
				  AND uji.idsemester = $semester
				  AND uji.idkelas = $kelas
				  AND sis.nis = '$nis' 
			GROUP BY pel.nama";
			
	$respel = QueryDb($sql);
	while($rowpel = mysql_fetch_row($respel))
	{
		$idpel = $rowpel[0];
		$nmpel = $rowpel[1];
		
		$sql = "SELECT nilaimin 
					 FROM infonap
					WHERE idpelajaran = $idpel
					  AND idsemester = $semester
				     AND idkelas = $kelas";
		$res = QueryDb($sql);
		$row = mysql_fetch_row($res);
		$nilaimin = $row[0];
				
		echo "<tr height='25'>";
		echo "<td align='left'>$nmpel</td>";
		echo "<td align='center'>$nilaimin</td>";
		
		for($i = 0; $i < count($aspekarr); $i++)
		{
			$na = "";
			$nh = "";
			$asp = $aspekarr[$i][0];
		
			$sql = "SELECT nilaiangka, nilaihuruf, idpelajaran
						 FROM infonap i, nap n, dasarpenilaian a 
						WHERE i.replid = n.idinfo 
						  AND n.nis = '$nis' 
						  AND i.idpelajaran = '$idpel' 
						  AND i.idsemester = '$semester' 
						  AND i.idkelas = '$kelas'
						  AND n.idaturan = a.replid 	   
						  AND a.dasarpenilaian = '$asp'";
			$res = QueryDb($sql);
			
			$cntpel_komentar = 1;
			if (mysql_num_rows($res) > 0)
			{
				$row = mysql_fetch_row($res);
				$na = $row[0];
				$nh = $row[1];

				//komentar
				$sql = "SELECT k.komentar 
		          		FROM $g_db_akademik.komennap k, $g_db_akademik.infonap i 
				 		WHERE k.nis='$nis' AND i.idpelajaran='$row[2]' AND i.replid=k.idinfo 
				   		AND i.idsemester='$semester' AND i.idkelas='$kelas'";
				$res2 = QueryDb($sql);
				$row2 = @mysql_fetch_row($res2);
			}
			$say = $NTT->Convert($na);
			echo "<td align='center'>$na</td><td align='center'>$nh</td><td align='left'>$row2[0]</td>"; 
		} 
		
		echo "</tr>";
	}
?>   
	</table>
	<script language='JavaScript'>
	   	Tables('table', 1, 0);
	</script>
	</td>
  </tr>
  
  <tr>
  	<td><br><h2><strong>C. Ekstra Kurikuler</strong></h2></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
		<tr>
			<td width="30px"><div align="center"><b>No</b></div></td>
			<td><b> Ekstra Kurikuler</b></td>
			<td><div align="center"><b>Keterangan dalam Kegiatan</b></div></td>
		</tr>
		<tr>
			<td align='center'>1</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>2</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>3</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>4</td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</td>
  </tr>
  
  <tr>
  	<td><br><h2><strong>D. Prestasi</strong></h2></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
		<tr>
			<td width="30px"><div align="center"><b>No</b></div></td>
			<td align='center'><b> Jenis Prestasi</b></td>
			<td><div align="center"><b>Keterangan dalam Kegiatan</b></div></td>
		</tr>
		<tr>
			<td align='center'>1</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>2</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>3</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align='center'>4</td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</td>
  </tr>
  
  <?php
  if ($harian!="false"){
  ?>
  <tr>
  	<td><br><h2><strong>E. Ketidakhadiran</strong></h2></td>
  </tr>
  <tr>
    <td>
	<?php


	 $sql_harian = "SELECT SUM(ph.hadir) as hadir, SUM(ph.ijin) as ijin, SUM(ph.sakit) as sakit, SUM(ph.cuti) as cuti, SUM(ph.alpa) as alpa, SUM(ph.hadir+ph.sakit+ph.ijin+ph.alpa+ph.cuti) as tot ".
			"FROM presensiharian p, phsiswa ph, siswa s ".
			"WHERE ph.idpresensi = p.replid ".
			"AND ph.nis = s.nis ".
			"AND ph.nis = '$nis' ".
			"AND ((p.tanggal1 ".
			"BETWEEN '$tglawal' ".
			"AND '$tglakhir') ".
			"OR (p.tanggal2 BETWEEN '$tglawal' AND '$tglakhir')) ".
			"ORDER BY p.tanggal1"; ;
	  ?>
	<!-- Content Presensi disini -->
	<table width="100%" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
  <!-- Ambil pelajaran per departemen-->
	<?php
	$result_harian=QueryDb($sql_harian);
	$row_harian=@mysql_fetch_array($result_harian);
	$hadir=$row_harian['hadir'];
	$sakit=$row_harian['sakit'];
	$ijin=$row_harian['ijin'];
	$alpa=$row_harian['alpa'];
	$cuti=$row_harian['cuti'];
	$all=$row_harian['tot'];
	if ($hadir!=0 && $all !=0)
	$p_hadir=$hadir/$all*100;
	
	if ($sakit!=0 && $all !=0)
	$p_sakit=$sakit/$all*100;
	
	if ($ijin!=0 && $all !=0)
	$p_ijin=$ijin/$all*100;
	
	if ($alpa!=0 && $all !=0)
	$p_alpa=$alpa/$all*100;
	
	if ($cuti!=0 && $all !=0)
	$p_cuti=$cuti/$all*100;
	?>
	<tr>
	  <td height="25" colspan=3><b>Ketidakhadiran</b></td>
	</tr>
	<tr>
		<td height="25" ><div align="center">1</div></td>
		<td height="25" >Sakit</div></td>
		<td height="25" ><div align="center">
      	<?php echo $sakit?> hari
   		</div></td>
	</tr>
	<tr>
	<td height="25" ><div align="center">2</div></td>
		<td height="25" >Ijin</div></td>
		<td height="25" ><div align="center">
      	<?php echo $ijin?> hari
    	</div></td>
	</tr>
	<tr>
	<td height="25" ><div align="center">3</div></td>
		<td height="25" >Tanpa Keterangan</td>
		<td height="25" ><div align="center">
      	<?php echo $alpa?> hari
    	</div></td>
	</tr>
	<!--<tr>
		<td height="25" ><div align="center">Cuti</div></td>
		<td height="25" ><div align="center">
      	<?php echo $cuti?>
      	</div></td>
	</tr>-->
    
</table>
<script language='JavaScript'>
	   	Tables('table', 1, 0);
</script>
    </td>
  </tr>
  
  
  
  
 <?php
 }?>
 <tr>
  	<td><br><h2><strong>F. Catatan Wali Kelas</strong></h2></td>
  </tr>
  <tr>
    <td>
		<table width="100%" height='100px' border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
			<tr><td> </td></tr>
		</table>
	</td>
 </tr>
 
 <tr>
  	<td><br><h2><strong>G. Tanggapan Orang Tua/Wali</strong></h2></td>
  </tr>
  <tr>
    <td>
		<table width="100%" height="100px" border="1" bordercolor="#b8b8b8" class="tab" id="table" cellpadding="0" cellspacing="0">
			<tr><td height='100px'></td></tr>
		</table>
	</td>
 </tr>
 <?php if($semester=='3'){?>
	  <tr>
    <td>
	 <table width="100%" border="0">
  <tr>
    <td rowspan="2" width="33%"><div align="center">Orang Tua/Wali Siswa</div></td>
    <td width="33%"><div align="center">Mengetahui,</div></td>
    <td rowspan="2" width="33%"><div align="center">Wali Kelas</div></td>
  </tr>
  <tr>
    <td width="33%"><div align="center">Kepala Sekolah 
      <?php echo $departemen?>
    </div></td>
  </tr>
  <tr>
    <td width="33%" height="50"><div align="center"></div></td>
    <td width="33%" height="50"><div align="center"></div></td>
    <td width="33%" height="50"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="33%" rowspan="2"><div align="center">(.............................................)</div></td>
    <td width="33%"><div align="center">
      <u><?php echo $row_get_kepsek[namakepsek]?></u>
    </div></td>
    <td width="33%"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
  </tr>
  <tr>
    <td><div align="center">
      NIP : <?php echo $row_get_kepsek[nipkepsek]?>
    </div></td>
    <td width="33%"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
  </tr>
</table>
	</td>
  </tr>
  <?php }else {?>
  <tr>
    <td>
     <table width="100%" border="0">
  <tr>
    <td rowspan="2" width="50%"><div align="center">Orang Tua/Wali Siswa</div></td>
    <td rowspan="2" width="50%"><div align="center">Wali Kelas</div></td>
  </tr>
  <tr>
    <td width="50%" height="100px"><div align="center"></div></td>
    <td width="50%" height="100px"><div align="center"></div></td>
  </tr>
  <tr>
    <td width="50%" rowspan="2"><div align="center">(.............................................)</div></td>
    <td width="50%"><div align="center">
      <u><?php echo $row_get_w_kls[namawalikelas]?></u>
    </div></td>
  </tr>
  <tr>
    <td width="50%"><div align="center">
      NIP : <?php echo $row_get_w_kls[nipwalikelas]?>
    </div></td>
  </tr>
  </table>
  </td>
  </tr>
  <?php }?>
 </table>
</div>
<span style='font-size:12.0pt;font-family:"Times New Roman";mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
mso-break-type:section-break'>
</span>
<?php
	$cnt_siswa1=$cnt_siswa1+1;
}
?>
</body>

</html>
<?php
CloseDb();
?>