<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
OpenDb();
$departemen='SMAN';
$bagian=$_REQUEST["bagian"];

$urut = $_REQUEST['urut'];	
$urutan = $_REQUEST['urutan'];
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Kepegawaian]</title>
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center>
  <font size="4"><strong>DATA KEPEGAWAIAN</strong></font><br />
 </center><br /><br />
<br />
<strong>Bagian : <?php if ($bagian == "-1") echo "Semua Bagian"; else echo $bagian;?></strong></font>
<br /><br />
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="10%" class="header" align="center">NIP</td>
        <td width="30%" class="header" align="center">Nama</td>
		<td width="*" class="header" align="center">Tempat Tanggal Lahir</td>
        <td width="10%" class="header" align="center">PIN</td>
        <td width="10%" class="header" align="center">Status</td>
   	</tr>
<?php 	
	OpenDb();
	if ($bagian != "-1"){
		$sql_pegawai="SELECT * FROM $g_db_pegawai.pegawai WHERE bagian='$bagian' ORDER BY $urut $urutan";
		//$sql_pegawai="SELECT * FROM $g_db_pegawai.pegawai WHERE bagian='$bagian' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	} else {
		$sql_pegawai="SELECT * FROM $g_db_pegawai.pegawai ORDER BY $urut $urutan";
		//$sql_pegawai="SELECT * FROM $g_db_pegawai.pegawai ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
		//$sql_pegawai="SELECT * FROM $g_db_pegawai.pegawai ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	}
	
	$result_pegawai=QueryDb($sql_pegawai);
	if ($page==0)
		$cnt = 0;
	else
		$cnt = (int)$page*(int)$varbaris;
	
	while ($row_pegawai = mysql_fetch_array($result_pegawai)) { ?>
    <tr height="25">
    	<td align="center"><?php echo ++$cnt ?></td>
        <td align="center"><?php echo $row_pegawai['nip'] ?></td>
        <td><?php echo $row_pegawai['nama'] . " " . $row['nama'] ?></td>
        <td><?php echo $row_pegawai['tmplahir'] ?>, <?php echo format_tgl($row_pegawai['tgllahir']) ?></td>
        <td align="center"><?php echo $row_pegawai['pinpegawai']?></td>
        <td align="center">
        
<?php		if ($row_pegawai['aktif'] == 1) { 
			echo "Aktif";
			} else { 	
			echo "Tidak Aktif";
		}
		?>       </td>
        </tr>
<?php	} CloseDb();?>    
    <!-- END TABLE CONTENT -->
   	
    </table>
	</td>
</tr>
<!--<tr>
   	<td align="right">Halaman <strong><?php echo $page+1?></strong> dari <strong><?php echo $total?></strong> halaman</td>
</tr>-->
<!-- END TABLE CENTER -->    
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>
<?php
CloseDb();
?>