<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');
OpenDb();
$departemen='yayasan';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Departemen]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo getHeader($departemen)?>

<center><font size="4"><strong>DATA DEPARTEMEN</strong></font><br /> </center><br /><br />

<br />

	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="15%" class="header" align="center">Departemen</td>
        <td width="30%" class="header" align="center">Kepala Sekolah</td>
        <td width="*" class="header" align="center">Keterangan</td>
        <td width="10%" class="header" align="center">Status</td>
    </tr>
<?php 	OpenDb();
	$sql = "SELECT d.replid,d.departemen,d.nipkepsek,p.nama,d.keterangan,d.aktif FROM departemen d, $g_db_pegawai.pegawai p WHERE d.nipkepsek = p.nip ORDER BY urutan";    
	$result = QueryDB($sql);
	$cnt = 0;
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">
    	<td align="center"><?php echo ++$cnt ?></td>
        <td><?php echo $row['departemen'] ?></td>
        <td><?php echo $row['nipkepsek'] . " - " . $row['nama'] ?></td>
        <td><?php echo $row['keterangan'] ?></td>
        <td align="center">
			<?php if ($row['aktif'] == 1) 
					echo 'Aktif';
				else
					echo 'Tidak Aktif';
			?>		
        </td> 
    </tr>
<?php	} 
	CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>

</td></tr></table>
</body>
<script language="javascript">
window.print();
</script>
</html>