<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../cek.php');

OpenDb();
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];

if (isset($_REQUEST['tahunajaran'])) 
	$tahunajaran = $_REQUEST['tahunajaran'];
	
if (isset($_REQUEST['tingkat']))
	$tingkat = $_REQUEST['tingkat'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kenaikan Kelas</title>
<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="javascript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/ajax.js"></script>
<script language="javascript">

function change_departemen() {
	var departemen = document.getElementById("departemen").value;
	
	parent.mutasi_siswa_header.location.href = "mutasi_siswa_header.php?departemen="+departemen;
	parent.mutasi_siswa_footer.location.href = "blank_mutasi_all.php";
}

function change_tingkat() {
	var departemen = document.getElementById("departemen").value;
	var tingkat = document.getElementById("tingkat").value;
	var tahunajaran = document.getElementById("tahunajaran").value;
	
	parent.mutasi_siswa_header.location.href = "mutasi_siswa_header.php?departemen="+departemen+"&tingkat="+tingkat+"&tahunajaran="+tahunajaran;
	parent.mutasi_siswa_footer.location.href = "blank_mutasi_all.php";
}

function tampil() {
	var departemen = document.getElementById("departemen").value;
	var tingkat = document.getElementById("tingkat").value;
	var tahunajaran = document.getElementById("tahunajaran").value;	
	var kelas = document.getElementById("kelas").value;	
	
	if (tahunajaran==""){
		alert ('Tahun Ajaran tidak boleh kosong!');
		document.getElementById("tahunajaran").focus();
		return false;
	}
	if (tingkat==""){
		alert ('Tingkat tidak boleh kosong!');
		document.getElementById("tingkat").focus();
		return false;
	}	
	if (kelas == 0) {
		alert ('Belum ada Kelas yang aktif pada Tingkat ini!');	
		document.getElementById("departemen").focus();
		return false;
	}
	
	parent.mutasi_siswa_footer.location.href = "mutasi_siswa_footer.php?departemen="+departemen+"&tingkat="+tingkat+"&tahunajaran="+tahunajaran;	
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
		if (elemName == 'tabel') {
			tampil();
		} 
		return false;
    } 
    return true;
}

</script>
</head>
	
<body leftmargin="0" topmargin="0" onload="document.getElementById('departemen').focus()">
<table border="0" width="100%" cellpadding="0" cellspacing="0"  >
<!-- TABLE TITLE -->
<tr>
	<td rowspan="2" width="36%">
	<table width = "100%" border = "0">
    <tr>
      	<td width = "30%"><strong>Departemen</strong>
      	<td width = "*">
		<select name="departemen" id="departemen" onchange="change_departemen()" style="width:200px;" onKeyPress="return focusNext('tahunajaran', event)" >
        <?php	$dep = getDepartemen(SI_USER_ACCESS());    
			foreach($dep as $value) {
			if ($departemen == "")
				$departemen = $value; ?>
          	<option value="<?php echo $value ?>" <?php echo StringIsSelected($value, $departemen) ?> ><?php echo $value ?> 
            </option>
      	<?php	} ?>
      	</select>    	</td>
    </tr>
    <tr>
		<td><strong>Tahun Ajaran</strong></td>
        <td>
    	<select name="tahunajaran" id="tahunajaran" style="width:200px;"  onchange="change_tingkat()" onKeyPress="return focusNext('tingkat', event)">
   		<?php 	OpenDb();
			$sql_tahunajaran = "SELECT * FROM tahunajaran where departemen='$departemen' ORDER BY aktif DESC, tglmulai DESC";
			$result_tahunajaran = QueryDb($sql_tahunajaran);
			CloseDb();
			while ($row_tahunajaran = @mysql_fetch_array($result_tahunajaran)) {
				if ($tahunajaran == "")
					$tahunajaran = $row_tahunajaran['replid'];
				$ada = "";
				if ($row_tahunajaran['aktif'])
					$ada = "(Aktif)";	
		?>
        <option value="<?php echo urlencode($row_tahunajaran[replid])?>" <?php echo IntIsSelected($row_tahunajaran['replid'], $tahunajaran)?> >
		<?php echo $row_tahunajaran['tahunajaran']." ".$ada?></option>
        <?php  } ?>
      	</select>
    	</td>
   	</tr>
	<tr>
    	<td><strong>Tingkat</strong>
      	<td>
        <select name="tingkat" id="tingkat" onchange="change_tingkat()" style="width:200px;" onKeyPress="return focusNext('tabel', event)" >
		<?php 	OpenDb(); 
			$sql_tingkat = "SELECT replid,tingkat FROM tingkat where departemen='$departemen' AND aktif = 1 ORDER BY urutan";
			$result_tingkat = QueryDb($sql_tingkat);
			
			while ($row_tingkat = mysql_fetch_array($result_tingkat)) {
			if ($tingkat == "") 
				$tingkat = $row_tingkat['replid'];	
		?>
  		<option value="<?php echo $row_tingkat[replid]?>" <?php echo IntIsSelected($row_tingkat['replid'], $tingkat)?>>
		<?php echo $row_tingkat['tingkat']?></option>
  		<?php
  			} //while
			CloseDb();
		?>
		</select>
    <?php 	$total = 0;
		if ($tingkat <> "" && $tahunajaran <> ""){
			OpenDb();
        	$sql_kelas="SELECT k.replid,k.kelas FROM $g_db_akademik.kelas k WHERE k.idtingkat='$tingkat' AND k.idtahunajaran='$tahunajaran' AND k.aktif=1 ORDER BY k.kelas";
			
        	$result_kelas=QueryDb($sql_kelas);
			$total = mysql_num_rows($result_kelas);
		}
	?>
        <input type="hidden" name="kelas" id="kelas" value="<?php echo $total?>" />
		</td>  
  	</tr>
    </table>   	</td>
  	<td valign="middle"><a href="#" onclick="tampil()" ><img src="../images/view.png"  border="0" name="tabel" id="tabel" onmouseover="showhint('Klik untuk menampilkan daftar siswa yang akan mutasi!', this, event, '200px')"/></a></td>
  	<td colspan = "2" align="right" valign="top"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Mutasi Siswa</font><br />
    <a href="../mutasi.php" target="content">
      <font size="1" color="#000000"><b>Mutasi</b></font></a>&nbsp>&nbsp
        <font size="1" color="#000000"><b>Mutasi Siswa</b></font>
    </td>     
</tr>
</table>
	
</body>
</html>
<script language="javascript">
	var spryselect11 = new Spry.Widget.ValidationSelect("departemen");
	var spryselect12 = new Spry.Widget.ValidationSelect("tahunajaran");
	var spryselect12 = new Spry.Widget.ValidationSelect("tingkat");
</script>