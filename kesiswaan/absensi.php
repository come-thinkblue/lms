<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
include('../charts/FusionCharts.php');	

require_once('include/errorhandler.php');
require_once('include/db_functions.php');
require_once('include/sessioninfo.php');
require_once('include/common.php');
require_once('include/config.php');
require_once('cek.php');

OpenDb();



$page='p';
if (isset($_REQUEST[page]))
	$page = $_REQUEST[page];
?>
<html>
<head>
<title>pelajaran</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="style/tooltips.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script type="text/javascript" src="script/tooltips.js"></script>
<script type="text/javascript">
function over(id){
	var actmenu = document.getElementById('actmenu').value;
	if (actmenu==id)
		return false;
		
	if (actmenu=='g')
		document.getElementById('img').src='images/p_over.png';
	else 
		document.getElementById('img').src='images/g_over.png';
}
function out(id){
	var actmenu = document.getElementById('actmenu').value;
	if (actmenu==id)
		return false;
	
	if (actmenu=='g')
		document.getElementById('img').src='images/g.png';
	else
		document.getElementById('img').src='images/p.png';
}
function show(id){
	if (id=='g'){
		document.getElementById('actmenu').value='g';
		document.getElementById('img').src='images/g.png';
		document.getElementById('slice_g').style.display='';
		document.getElementById('slice_p').style.display='none';
	} else {
		document.getElementById('actmenu').value='p';
		document.getElementById('img').src='images/p.png';
		document.getElementById('slice_p').style.display='';
		document.getElementById('slice_g').style.display='none';
	}	
}
</script>
</head>
<body  onLoad="show('<?php echo$page?>')">
<!-- ImageReady Slices (Untitled-1) -->
<div id="content" style="margin-top:20px;"> 
<div class="wrapper">

<?php 
$que1="SELECT Count(asalsekolah.calonsiswa) as jumlah, pelajaran.nama, pelajaran.kode FROM pelajaran INNER JOIN guru ON guru.idpelajaran = pelajaran.replid
GROUP BY guru.idpelajaran, pelajaran.nama ";
$result1=mysql_query($que1);
$jumrows1 =mysql_num_rows($result1);

$data ="<chart showvalues='1' caption='' canvasBorderColor='1D8BD1' canvasBorderAlpha='60'
  enablesmartlabels='1' showlabels='1' showpercentvalues='0' logoURL='kecil.png'>";

if ($jumrows1>0) {
				$no1=0;
while ($row2 = mysql_fetch_array($result1)) {
					$data .= "<set label='" . $row2['kode']."' value='" . $row2['jumlah'] . "' />";
					$no1++;
				}
				$data.="</chart>";
              }
?>

        <div class="fluid">
            <div class="widget gridWelcome" style="width:40%;">
              
                <div class="body">
                <h1 style="color:#128f97; font-weight:800;">ABSENSI SISWA</h1>
                <p>Menu ini digunakan untuk mengelola data Absensi Siswa siswa MAN Kota Blitar, termasuk pendataan absensi dan statistik absesnsi siswa</p></div>
                
        <ul class="middleNavA">
            <li><a href="presensi/formpresensi_harian.php" title="Pendataan Pelajaran"><img src="css/icon_pelajaran/pelajaran.png" alt="" /><p>Cetak Form</p><p>Presesensi</p></a></li>
             <li><a href="presensi/lap_harianabsen_main.php" title="Setting RPP"><img src="css/icon_pelajaran/rpp.png" alt=""  /><p>Lap. Presensi</p><p>Tidak Hadir</p></a></li>
             <li><a href="presensi/lap_hariansiswa_main.php" title="Aspek Penilaian"><img src="css/icon_pelajaran/aspekPenilaian.png" alt="" /><p>Lap. Presensi</p> <p>Harian</p> </a></li>
            <li><a href="presensi/input_presensi_main.php" title="Setting Jenis Pengujian"><img src="css/icon_pelajaran/jenisPengujian.png" alt="" /><p>Presensi</p><p>Harian</p> </a></li>
            <li><a href="presensi/lap_hariankelas_main.php" title="Aturan Grading"><img src="css/icon_pelajaran/status.png"alt="" /><p>Lap. Presensi</p><p>Kelas</p> </a></li>
            <li><a href="presensi/statistik_hariansiswa_main.php" title="Aturan Rapor"><img src="css/icon_pelajaran/nilaiRapor.png" alt="" /><p>Statistik</p><p>Kehadiran</p></a></li>
            <li><a href="presensi/statistik_hariankelas_main.php" title="Status Guru"><img src="css/icon_pelajaran/grading.png" alt="" /><p>Statistik<p><p> Kehadiran Kelas</p></a></li>
        </ul>
            </div>
            <div class="widget grid7">
                <div class="whead"><h6>Hasil PPDB Berdasarkan Asal Sekolah</h6><div class="clear"></div></div>
                <div class="body">
                <?php echo renderChartHTML("../charts/Column3D.swf", "", $data, "tes", "100%", "300", false);?>
                </div>
            </div>
            
    <?php
function jumlahData($count, $from){
$quePeg="SELECT Count(".$count.") as jumlah FROM ".$from."";
$resultPeg=mysql_query($quePeg);
$jumPeg=mysql_fetch_array($resultPeg);
$jumlah= $jumPeg['jumlah'];
return $jumlah;
}
	?>        
            
            
        </div>


</div>


</div>
<!-- End ImageReady Slices -->
</body>
</html>