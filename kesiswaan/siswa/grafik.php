<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

$dasar=$_REQUEST['dasar'];
$departemen=$_REQUEST['departemen'];
$idangkatan=$_REQUEST['idangkatan'];
$tabel=$_REQUEST['tabel'];
$iddasar=$_REQUEST['iddasar'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script language="javascript" src="../script/tools.js"></script>
<link href="../style/style.css" rel="stylesheet" type="text/css" />
</head>
<body topmargin="0" leftmargin="0">
<table width="100%" border="0" cellpadding="2" cellspacing="2">
<tr>
    <td align="center">
    <!--calon_statistik_cetak.php?dasar=<?php echo $dasar?>&departemen=<?php echo $departemen?>&idproses=<?php echo $idproses?>&tabel=<?php echo $tabel?>&nama_judul=<?php echo $judul?>&iddasar=<?php echo $iddasar?>&lup=no'--->
    <a href="#" onclick="newWindow('statistik_cetak.php?dasar=<?php echo $dasar?>&departemen=<?php echo $departemen?>&idangkatan=<?php echo $idangkatan?>&tabel=<?php echo $tabel?>&iddasar=<?php echo $iddasar?>&lup=no','Cetak',787,551,'resizable=1,scrollbars=1,status=0,toolbar=0');"><img src="../images/ico/print.png" border="0"/>&nbsp;Cetak</a>
    </td>
</tr>
<tr>
    <td align="center" valign="top">
    <img src="statistik_batang.php?iddasar=<?php echo $iddasar?>&departemen=<?php echo $departemen?>&idangkatan=<?php echo $idangkatan?>"/>
    <p>
    <img src="statistik_pie.php?iddasar=<?php echo $iddasar?>&departemen=<?php echo $departemen?>&idangkatan=<?php echo $idangkatan?>" />
    </td>
</tr>
</table>

</body>
</html>