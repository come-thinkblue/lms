<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../cek.php');

//$departemen = -1;
if (isset($_REQUEST['departemen']))
	$departemen = $_REQUEST['departemen'];
$angkatan = -1;
if (isset($_REQUEST['angkatan']))
	$angkatan = $_REQUEST['angkatan'];
$dasar = 1;
if (isset($_REQUEST['dasar']))
	$dasar = $_REQUEST['dasar'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Statistik Penerimaan Siswa Baru</title>
<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript">

function tampil_statistik() {
	var departemen = document.getElementById("departemen").value;
	var angkatan = document.getElementById("angkatan").value;
	var iddasar = document.getElementById("dasar").value;
	alert('mau statistik');	
	parent.header.location.href = "statistik_header.php?departemen="+departemen+"&angkatan="+angkatan+"&dasar="+iddasar;			
	parent.footer.location.href = "statistik_footer.php?departemen="+departemen+"&angkatan="+angkatan+"&iddasar="+iddasar;
}

function change() {
	var departemen = document.getElementById("departemen").value;	
	var angkatan = document.getElementById("angkatan").value;
	var dasar = document.getElementById("dasar").value;
	
	parent.header.location.href = "statistik_header.php?departemen="+departemen+"&angkatan="+angkatan+"&dasar="+dasar;			
	parent.footer.location.href="blank_statistik.php";
}

function focusNext(elemName, evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode :
        ((evt.which) ? evt.which : evt.keyCode);
    if (charCode == 13) {
		document.getElementById(elemName).focus();
		if (elemName == 'tabel')
			tampil_statistik();
		return false;
    } 
    return true;
}

function panggil(elem){
	var lain = new Array('departemen','angkatan','dasar');
	for (i=0;i<lain.length;i++) {
		if (lain[i] == elem) {
			document.getElementById(elem).style.background='#4cff15';
		} else {
			document.getElementById(lain[i]).style.background='#FFFFFF';
		}
	}
}

</script>
</head>
	
<body topmargin="0" leftmargin="0" onload="document.getElementById('departemen').focus()">
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<!-- TABLE TITLE -->
<tr>
	<td rowspan="3" width="45%">
    <table width = "98%" border = "0" cellpadding="0" cellspacing="0">
 	<tr>
    	<td align="left" width = "35%"><strong>Departemen</strong>
      	<td width = "*">
        <?php 	if (SI_USER_LEVEL() != $SI_USER_STAFF) {	?>
        <select name="departemen" id="departemen" onchange="change()" style="width:240px;" onKeyPress="return focusNext('dasar', event)" onfocus="panggil('departemen')">
        	<option value="-1" >(Semua Departemen)</option>    
		<?php
			$sql = "SELECT * FROM $g_db_akademik.departemen where aktif=1 ORDER BY urutan";
			OpenDb();
			$result = QueryDb($sql);
			CloseDb();
			while($row = mysql_fetch_array($result)) {
			if ($departemen == "")
				$departemen = -1;	
		?>
        	<option value="<?php echo urlencode($row['departemen'])?>" <?php echo StringIsSelected($row['departemen'], $departemen) ?>><?php echo $row['departemen']?></option>
        <?php 	} //while 	?>
     	</select>
        <?php 	} else { ?>
        <select name="departemen" id="departemen" onchange="change()" style="width:240px;" onKeyPress="return focusNext('angkatan', event)" onfocus="panggil('departemen')">
        <?php
			$dep = getDepartemen(SI_USER_ACCESS());    
			foreach($dep as $value) {
			if ($departemen == "")
				$departemen = $value; ?>
                <option value="<?php echo $value ?>" <?php echo StringIsSelected($value, $departemen) ?> > 
                  <?php echo $value ?> 
                  </option>
              <?php } ?>
              </select>
        <?php  }
		
		if ($departemen == -1)  {
			$disable = 'disabled="disabled"';
			$angkatan = -1;
			$dep = "";
		} else	{
			$disable = "";
			$dep = "AND departemen = '$departemen'";
		}
		
		?>
        </td>
    </tr>
    <tr>
    	<td align="left"><strong>Proses Penerimaan</strong></td>    
	 	<td>
      	<select name="angkatan" id="angkatan" onchange="change()" <?php echo $disable?> style="width:240px;" onKeyPress="return focusNext('dasar', event)" onfocus="panggil('angkatan')">      	
	 	<option value="-1" >(Semua Angkatan)</option>	
		<?php
      		OpenDb();
			$sql = "SELECT replid,angkatan FROM $g_db_akademik.angkatan WHERE aktif = 1 $dep ";
			$result = QueryDb($sql);
			CloseDb();
			while ($row = mysql_fetch_array($result)) { 
		?>            	
    	<option value="<?php echo urlencode($row[replid])?>" <?php echo IntIsSelected($row['replid'], $angkatan) ?>><?php echo $row['angkatan']?></option>
  		<?php	} //while  	?>
		</select>
     	</td>
    </tr>
    <tr>
    	<td align="left" width = "13%"><strong>Berdasarkan</strong>
      	<td>
        <!--<div id="dasarInfo">-->
        <select name="dasar" id="dasar" onchange="change()" style="width:240px;" onKeyPress="return focusNext('tabel', event)" onfocus="panggil('dasar')">
        <?php for ($i=1;$i<=17;$i++) { ?>
        <option value ="<?php echo $i?>" <?php echo IntIsSelected($i, $dasar) ?>><?php echo $kriteria[$i] ?></option>
        <?php  } ?>
        </select>      
		</td> 
	</tr>
	</table>
    </td>
   	<td width="5%" rowspan="2" valign="middle"><a href="#" onclick="tampil_statistik()" ><img src="../images/view.png"  border="0" name="tabel" id="tabel" onmouseover="showhint('Klik untuk menampilkan statistik !', this, event, '120px')"/></a></td>
    <td width="50%" align="right" valign="top"><font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Statistik Kesiswaan</font><br />
    <a href="../siswa.php" target="content">
      	<font size="1" color="#000000"><b>Kesiswaan</b></font></a>&nbsp>&nbsp 
        <font size="1" color="#000000"><b>Statistik Kesiswaan</b></font>
    </td>    
</tr>
</table>
</td>
</tr>

</table>
</body>
</html>
<script language="javascript">
/*var spryselect = new Spry.Widget.ValidationSelect("departemen");
var spryselect = new Spry.Widget.ValidationSelect("proses");
var spryselect = new Spry.Widget.ValidationSelect("dasar");*/
</script>