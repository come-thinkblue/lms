<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php 
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
$dep_asal=$_REQUEST['dep_asal'];
$sekolah = $_REQUEST['sekolah'];
?>
	<select name="sekolah" id="sekolah" onKeyPress="return focusNext('ketsekolah', event)" style="width:150px;">
     <option value="">[Pilih Asal Sekolah]</option>
     <?php // Olah untuk combo sekolah
	OpenDb();
	$sql_sekolah="SELECT sekolah FROM $g_db_akademik.asalsekolah WHERE departemen='$dep_asal' ORDER BY sekolah";
	$result_sekolah=QueryDB($sql_sekolah);
	while ($row_sekolah = mysql_fetch_array($result_sekolah)) {
	if ($sekolah=="")
		$sekolah=$row_sekolah['sekolah'];
	?>
       <option value="<?php echo $row_sekolah['sekolah']?>" <?php echo StringIsSelected($row_sekolah['sekolah'],$sekolah)?>>
        <?php echo $row_sekolah['sekolah']?>
        </option>
      <?php
    } 
	CloseDb();
	// Akhir Olah Data sekolah
	?>
    </select>