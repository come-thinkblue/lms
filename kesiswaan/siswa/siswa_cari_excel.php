<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Data_Siswa.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$cari=$_REQUEST['cari'];
$jenis=$_REQUEST['jenis'];
$departemen=$_REQUEST['departemen'];
$urut = "nama";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	

$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];

$tipe = array("nisn" => "N I S N","nis" => "NIS", "nama" => "Nama","panggilan" => "Nama Panggilan", "agama" =>"Agama", "suku" => "Suku", "status" => "Status", "kondisi"=>"Kondisi Siswa", "darah"=>"Golongan Darah", "alamatsiswa" => "Alamat Siswa", "asalsekolah" => "Asal Sekolah", "namaayah" => "Nama Ayah", "namaibu" => "Nama Ibu", "alamatortu" => "Alamat Orang Tua", "keterangan" => "Keterangan");

if ($cari=="")
$namacari="";
else
$namacari=$cari;


OpenDb();
$sql = "SELECT s.replid,s.nis,s.nama,s.asalsekolah,s.tmplahir,DATE_FORMAT(s.tgllahir,'%d %M %Y') as tgllahir,s.status,t.tingkat,k.kelas,s.aktif,s.nisn from $g_db_akademik.siswa s, $g_db_akademik.kelas k, $g_db_akademik.tingkat t WHERE s.".$jenis." LIKE '%$cari%' AND k.replid=s.idkelas AND k.idtingkat=t.replid AND t.departemen='$departemen' ORDER BY $urut $urutan";
$result=QueryDb($sql);

if (@mysql_num_rows($result)<>0){
?>
<html>
<head>
<title>
Data Siswa
</title>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
.style2 {font-size: 14px}
-->
</style>
</head>
<body>
<table width="700" border="0">
  <tr>
    <td>
    <table width="100%" border="0">
  <tr>
    <td colspan="7"><div align="center">PENCARIAN SISWA</div></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="7"><strong>Departemen :&nbsp;<?php echo $departemen?></strong></td>
  </tr>
  <tr>
    <td colspan="7">Pencarian berdasarkan <strong><?php echo $tipe[$jenis]?></strong> dengan keyword <strong><?php echo $cari?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
   
  </tr>
</table>

    </td>
  </tr>
  <tr>
    <td><table border="1">
<tr height="30">
<td width="3" valign="middle" ><div align="center" class="style1">No.</div></td>
<td width="20" valign="middle" ><div align="center" class="style1">NIS</div></td>
<td width="20" valign="middle" ><div align="center" class="style1">N I S N</div></td>
<td valign="middle" ><div align="center" class="style1">Nama</div></td>
<td valign="middle" ><div align="center" class="style1">Tempat, Tanggal Lahir</div></td>
<td valign="middle" ><div align="center" class="style1">Tingkat</div></td>
<td valign="middle" ><div align="center" class="style1">Kelas</div></td>
<td valign="middle" ><div align="center" class="style1">Status</div></td>
</tr>
<?php
	$cnt=1;
	while ($row_siswa=@mysql_fetch_array($result)){
	?>
	<tr height="25">
	<td width="3" align="center"><?php echo $cnt?></td>
	<td align="left"><?php echo $row_siswa[nis]?></td>
	<td align="left"><?php echo $row_siswa[nisn]?></td>
	<td align="left"><?php echo $row_siswa[nama]?></td>
	<td align="left"><?php echo $row_siswa[tmplahir]?>, <?php echo LongDateFormat($row_siswa[tgllahir])?></td>
	<td align="center"><?php echo $row_siswa[tingkat]?></td>
	<td align="center"><?php echo $row_siswa[kelas]?></td>
	<td align="center"><?php
		if ($row_siswa['aktif']==1){
			echo "Aktif";
		} elseif ($row_siswa['aktif']==0){
			echo "Tidak Aktif ";
			if ($row_siswa['alumni']==1){
				$sql_get_al="SELECT DATE_FORMAT(a.tgllulus, '%d %M %Y') as tgllulus FROM $g_db_akademik.alumni a WHERE a.nis='$row_siswa[nis]'";	
				$res_get_al=QueryDb($sql_get_al);
				$row_get_al=@mysql_fetch_array($res_get_al);
				echo "<br><a style='cursor:pointer;' title='Lulus Tgl: ".$row_get_al[tgllulus]."'><u>[Alumnus]</u></a>";
			}
			if ($row_siswa['statusmutasi']!=NULL){
				$sql_get_mut="SELECT DATE_FORMAT(m.tglmutasi, '%d %M %Y') as tglmutasi,j.jenismutasi FROM $g_db_akademik.jenismutasi j, $g_db_akademik.mutasisiswa m WHERE j.replid='$row_siswa[statusmutasi]' AND m.nis='$row_siswa[nis]' AND j.replid=m.jenismutasi";	
				$res_get_mut=QueryDb($sql_get_mut);
				$row_get_mut=@mysql_fetch_array($res_get_mut);
				//echo "<br><a href=\"NULL\" onmouseover=\"showhint('".$row_get_mut[jenismutasi]."<br>".$row_get_mut['tglmutasi']."', this, event, '50px')\"><u>[Termutasi]</u></a>";
				echo "<br><a style='cursor:pointer;' title='".$row_get_mut[jenismutasi]."\n Tgl : ".$row_get_mut['tglmutasi']."'><u>[Termutasi]</u></a>";
			}
		}
		?></td>
	</tr>
	<?php
		$cnt++;
}
	?>
</table></td>
  </tr>
</table>


</body>
</html>
<?php
}
CloseDb();
?>