<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	$id_ekskul = $_REQUEST['id_ekskul'];
	
	if (isset($_REQUEST['id_jabatan']))
		$id_jabatan = CQ($_REQUEST['id_jabatan']);
	if (isset($_REQUEST['nama_jabatan']))
		$nama_jabatan = CQ($_REQUEST['nama_jabatan']);
	if (isset($_REQUEST['keterangan']))
		$keterangan = CQ($_REQUEST['keterangan']);

	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		$sql_cek = "SELECT * FROM ekskul_struktur WHERE nama_jabatan = '$_REQUEST[nama_jabatan]' AND id_ekskul='$_REQUEST[id_ekskul]' ";
		$result_cek = QueryDb($sql_cek);

		if (@mysql_num_rows($result_cek) > 0) {
			CloseDb();
			$ERROR_MSG = "Nama jabatan " . $nama_jabatan . " sudah digunakan!";
		} 
		else {
			$sql_s = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul='$_REQUEST[id_ekskul]'";
			$result_s = QueryDb($sql_s);
			$row_s = @mysql_fetch_row($result_s);
			
			//$ERROR_MSG = "" . $id_ekskul ."-" .$nama_ekskul. "-" .$nipwali. "-". $namawali ."-". $keterangan;
			$sql = "INSERT INTO ekskul_struktur SET id_jabatan='$id_jabatan', nama_jabatan='$nama_jabatan', id_ekskul='$id_ekskul', keterangan='$keterangan'";
			$result = QueryDb($sql);
			if ($result) {
				?>
				<script language="javascript">
					opener.refresh();
					window.close();
				</script> 
			<?php
			}
		}	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Tambah Struktur Ektra Kurikuler]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nip')
						caripegawai();
					return false;
				} 
				return true;
			}
			
			function validate() {
				return validateEmptyText('nama_jabatan', 'Nama Jabatan');
			}
			
			function panggil(elem){
				var lain = new Array('nama_jabatan','keterangan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nama_jabatan').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Tambah Struktur Ektra Kurikuler :.
					</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="id_ekskul" id="id_ekskul" value="<?php echo $id_ekskul ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT max(id_jabatan) FROM ekskul_struktur";
								$rs = QueryDb($sql);
								$next_id = mysql_fetch_row($rs);
							?>
							<tr>
								<td><strong>Id Jabatan</strong></td>
								<td><input type="text" class="disabled" name="id_jabatan" id="id_jabatan" value="<?php echo $next_id[0]+1?>" size="10" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nama Jabatan</strong></td>
								<td><input type="text" name="nama_jabatan" id="nama_jabatan" size="20" onFocus="showhint('Nama Jabatan tidak boleh kosong !', this, event, '120px');panggil('nama_jabatan')" value="<?php echo $nama_jabatan?>" onKeyPress="return focusNext('keterangan', event)"/></td>
							</tr>
							<tr>
								<td valign="top"><strong>Keterangan</strong></td>
								<td><textarea name="keterangan" id="keterangan" rows="3" cols="45"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('keterangan')"><?php echo $keterangan ?></textarea>    </td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>