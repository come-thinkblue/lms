<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../include/getheader.php');
	
	$departemen="MAN Kota Blitar";
	
	$namaekskul	= $_REQUEST['namaekskul'];
	$thnkep = $_REQUEST['thnkep'];
	
	$urut = $_REQUEST['urut'];
	$urutan = $_REQUEST['urutan'];
	$varbaris = $_REQUEST['varbaris'];	
	$page = $_REQUEST['page'];
	$total = $_REQUEST['total'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Struktur Ektra Kurikuler]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>

<center>
  <font size="4"><strong>DATA PENGURUS EKSTRA KURIKULER <?php echo$namaekskul?></strong></font><br />
 </center><br /><br />
</span>
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="15%" class="header" align="center">Nama Siswa</td>
        <td width="10%" class="header" align="center">Kelas</td>
		<td width="10%" class="header" align="center">Nama EksKul</td>
        <td width="10%" class="header" align="center">Nama Jabatan</td>
        <td width="10%" class="header" align="center">Awal Kepengurusan</td>
        <td width="10%" class="header" align="center">Periode</td>
		<td width="*" class="header" align="center">Keterangan</td>
    </tr>
	<?php OpenDb();
	$sql_cari = "SELECT id_ekskul FROM ekskul WHERE nama_ekskul='$namaekskul'";
	$result_cari = QueryDb($sql_cari);
	$row_cari = @mysql_fetch_row($result_cari);
	
	$sql = "SELECT * FROM ekskul_pengurus WHERE id_ekskul='$row_cari[0]' AND periode='$thnkep' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";  

	$result = QueryDB($sql);
	//if ($page==0)
		$cnt = 1;
	//else
		//$cnt = (int)$page*(int)$varbaris+1;
		
	while ($row = mysql_fetch_row($result)) { 
	?>
	
	<?php
		$sql_nama = "SELECT nama, idkelas FROM siswa WHERE nis=$row[1]";
		$rs_nama = QueryDb($sql_nama);
		$row_nama = @mysql_fetch_row($rs_nama);
		
		$sql_kelas = "SELECT kelas FROM kelas WHERE replid=$row_nama[1]";
		$rs_kelas = QueryDb($sql_kelas);
		$row_kelas = @mysql_fetch_row($rs_kelas);
		
		$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$row[2]";
		//echo"$sql_eks";
		$rs_eks = QueryDb($sql_eks);
		$row_eks = @mysql_fetch_row($rs_eks);
		
		$sql_jab = "SELECT nama_jabatan FROM ekskul_struktur WHERE id_jabatan=$row[3]";
		//echo"$sql_jab";
		$rs_jab = QueryDb($sql_jab);
		$row_jab = @mysql_fetch_row($rs_jab);
	?>
	
    <tr height="25">    	
    	<td align="center"><?php echo$cnt ?></td>
        <td><?php echo$row_nama[0] ?></td>      
        <td><?php echo$row_kelas[0] ?></td>
		<td><?php echo$row_eks[0] ?></td>
		<td><?php echo$row_jab[0] ?></td>
		<td><?php echo$row[4] ?></td>
		<td><?php echo$row[5] ?></td>
		<td><?php echo$row[6] ?></td>
    </tr>
	<?php	$cnt++;
	} 
	CloseDb() ?>
    </table>
<!-- END TABLE CENTER -->    
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>