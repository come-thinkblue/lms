<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../cek.php');

$namaekskul = $_REQUEST['namaekskul'];
if (isset($_REQUEST['namaekskul']))
    $namaekskul = $_REQUEST['namaekskul'];

OpenDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Struktur Ekstra Kurikuler</title>
		<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
		<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/ajax.js"></script>
		<script language="javascript">
			function refresh() {	
				//var namaekskul = document.getElementById("namaekskul").value;
				alert('struktur top');
				//parent.footer.location.href="struktur_bottom.php?namaekskul="+namaekskul;
			}
			
			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'tabel') {
						show_kelas();
						panggil('tabel');
					} 
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('namaekskul');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

			function show_kelas() {
				var namaekskul = document.getElementById("namaekskul").value;
				parent.footer.location.href="nilaiekskul_bottom.php?namaekskul="+namaekskul;
				
			}

		</script>
		<style type="text/css">
			<!--
			.style1 {font-weight: bold}
			-->
		</style>
	</head>
	<body>
		<table border="0" width="100%" >
			<!-- TABLE TITLE -->
			<tr>
				<td rowspan="1" width="60%" align="center">
					<table width = "80%" border = "0">
						<tr>
							<td align="left"><strong>Nama Ekstra Kurikuler</strong> <br /><br />
								<select name="namaekskul" id="namaekskul" onChange="show_kelas()" style="width:130px;" onKeyPress="return focusNext('tabel', event)" onfocus="panggil('namaekskul')">
								<option value="" > Pilih Nama Ekstra Kurikuler </option>
								<?php
									OpenDb();
									$sql = "SELECT nama_ekskul FROM ekskul ORDER BY nama_ekskul";
									$result = QueryDb($sql);
									CloseDb();
									while ($row = @mysql_fetch_array($result)) {
								?>
									<option value="<?php echo $row[0]?>" > <?php echo $row[0] ?> </option>
								<?php } ?>
								</select>
							</td>
						</tr>
					</table>
				</td>   
				<td align="right" valign="top">
					<font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Nilai Ekstra Kurikuler</font><br />
					<a href="../ekskul.php" target="content"><font size="1" color="#000000"><b>Main Ekstra Kurikuler</b></font></a>&nbsp>&nbsp	 	
					<font size="1" color="#000000"><b>Nilai Ekstra Kurikuler</b></font>
				</td>
			</tr>
		</table>
	</body>
</html>