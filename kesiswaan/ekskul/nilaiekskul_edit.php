<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	
	$id_pengurus = $_REQUEST['id_pengurus'];
	if (isset($_REQUEST['id_pengurus']))
		$id_pengurus = CQ($_REQUEST['id_pengurus']);
		
	$id_jabatan = $_REQUEST['id_jabatan'];
	if (isset($_REQUEST['id_jabatan']))
		$id_jabatan = CQ($_REQUEST['id_jabatan']);
		
	$namaekskul = $_REQUEST['namaekskul'];
	if (isset($_REQUEST['namaekskul']))
		$namaekskul = $_REQUEST['namaekskul'];
	
	if (isset($_REQUEST['nilai']))
		$nilai = CQ($_REQUEST['nilai']);
		
	if (isset($_REQUEST['deskripsi']))
		$deskripsi = CQ($_REQUEST['deskripsi']);
	
	$sql_cari = "SELECT id_ekskul FROM ekskul WHERE nama_ekskul='$namaekskul'";
	$result_cari = QueryDb($sql_cari);
	$row_cari = @mysql_fetch_row($result_cari);
	
	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		$sql = "UPDATE ekskul_pengurus SET nilai='$nilai', deskripsi='$deskripsi' WHERE id_pengurus = '$id_pengurus'";
		//echo "$sql";
		$result = QueryDb($sql);
		if ($result) {
			?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
		<?php
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Edit Nilai Ektra Kurikuler]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function validate() {
				return validateEmptyText('nama_jabatan', 'Nama Jabatan');
			}

			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nip')
						caripegawai();
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('nilai','deskripsi');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nilai').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Edit Nilai Ektra Kurikuler :.
					</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="id_pengurus" id="id_pengurus" value="<?php echo $id_pengurus ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT nis, id_jabatan, nilai, deskripsi FROM ekskul_pengurus WHERE id_pengurus='$id_pengurus'";
								//echo "$sql <br>";
								$rs = QueryDb($sql);
								$result = mysql_fetch_row($rs);
								
								$sql_nama = "SELECT nama FROM siswa WHERE nis=$result[0]";
								//echo "$sql_nama <br>";
								$rs_nama = QueryDb($sql_nama);
								$row_nama = @mysql_fetch_row($rs_nama);
								
								$sql_jab = "SELECT nama_jabatan FROM ekskul_struktur WHERE id_jabatan='$result[1]' AND id_ekskul='$row_cari[0]'";
								//echo "$sql_jab";
								$rs_jab = QueryDb($sql_jab);
								$row_jab = @mysql_fetch_row($rs_jab);
							?>
							<tr>
								<td><strong>NIS</strong></td>
								<td><input type="text" class="disabled" name="nis" id="nis" value="<?php echo $result[0] ?> - <?php echo$row_nama[0]?>" size="30" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nama Ekskul</strong></td>
								<td><input type="text" class="disabled" name="namaekskul" id="namaekskul" value="<?php echo $namaekskul?>" size="20" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nama Jabatan</strong></td>
								<td><input type="text" class="disabled" name="id_jabatan" id="id_jabatan" value="<?php echo $row_jab[0]?>" size="20" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nilai</strong></td>
								<td>
									<select name="nilai" id="nilai" onFocus="panggil('nilai')" value="<?php echo $nilai?>" onKeyPress="return focusNext('deskripsi', event)">
										<option value="<?php echo $result[2]?>"><?php echo $result[2]?></option>
										<?php
										$sql_grade = "SELECT predikat FROM gradenilai ORDER BY huruf";
										$rs_grade = QueryDb($sql_grade);
										while($row_grade =	@mysql_fetch_row($rs_grade)){
											?>
											<option value="<?php echo$row_grade[0]?>"><?php echo$row_grade[0]?></option>
											<?php
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td valign="top"><strong>Deskripsi</strong></td>
								<td><textarea name="deskripsi" id="deskripsi" rows="5" cols="45"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('deskripsi')"><?php echo $result[3]?></textarea>    </td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>