<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../include/getheader.php');
	
	$departemen="MAN Kota Blitar";
	$bulan	= $_REQUEST['bulan'];
	$urut = $_REQUEST['urut'];
	$urutan = $_REQUEST['urutan'];
	$varbaris = $_REQUEST['varbaris'];	
	$page = $_REQUEST['page'];
	$total = $_REQUEST['total'];
	
	$nama_bulan = "";
	if($bulan == 01){
		$nama_bulan = "JANUARI";
	}
	else if($bulan == 02){
		$nama_bulan = "FEBRUARI";
	}
	else if($bulan == 03){
		$nama_bulan = "MARET";
	}
	else if($bulan == 04){
		$nama_bulan = "APRIL";
	}
	else if($bulan == 05){
		$nama_bulan = "MEI";
	}
	else if($bulan == 06){
		$nama_bulan = "JUNI";
	}
	else if($bulan == 07){
		$nama_bulan = "JULI";
	}
	else if($bulan == 08){
		$nama_bulan = "AGUSTUS";
	}
	else if($bulan == 09){
		$nama_bulan = "SEPTEMBER";
	}
	else if($bulan == 10){
		$nama_bulan = "OKTOBER";
	}
	else if($bulan == 11){
		$nama_bulan = "NOVEMBER";
	}
	else if($bulan == 12){
		$nama_bulan = "DESEMBER";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" href="../style/style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Cetak Jadwal Kegiatan Ektra Kurikuler]</title>
	</head>
	<body>
		<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
			<tr>
				<td align="left" valign="top">
					<?php echo GetHeader($departemen)?>
					<center><font size="4"><strong>JADWAL KEGIATAN EKSTRA KURIKULER BULAN <?php echo$nama_bulan?></strong></font><br /></center>
					<br />
				</td>
			</tr>
		</table>
		<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="left" bordercolor="#000000">
			<tr height="30">
				<td width="4%" class="header" align="center">No</td>
				<td width="10%" class="header" align="center">Nama Kegiatan</td>
				<td width="15%" class="header" align="center">Nama Ektra Kurikuler</td>
				<td width="10%" class="header" align="center">Jam</td>
				<td width="10%" class="header" align="center">Tanggal</td>
				<td width="10%" class="header" align="center">Tempat</td>
				<td width="20%" class="header" align="center">Penanggungjawab</td>
				<td width="*" class="header" align="center">Keterangan</td>
			</tr>
			<?php OpenDb();
			$sql = "SELECT * FROM ekskul_kegiatan WHERE MONTH(tanggal)='$bulan' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";  
			$result = QueryDB($sql);
			//if ($page==0)
				$cnt = 1;
			//else
				//$cnt = (int)$page*(int)$varbaris+1;
				
			while ($row = mysql_fetch_row($result)) { 
				?>
			<tr height="25">    	
				<td align="center"><?php echo$cnt ?></td>
				<?php
				$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$row[2]";
				$result_eks = QueryDb($sql_eks);
				$row_eks = @mysql_fetch_row($result_eks);
				
				$sql_nip = "SELECT nama FROM $g_db_pegawai.pegawai WHERE nip=$row[6]";
				$result_nip = QueryDb($sql_nip);
				$row_nip = @mysql_fetch_row($result_nip);
				?>
				<td><?php echo$row[1] ?></td>
				<td><?php echo$row_eks[0] ?></td>
				<td><?php echo$row[3] ?></td>
				<td><?php echo$row[4] ?></td>
				<td><?php echo$row[5] ?></td>
				<td><?php echo$row_nip[0] ?></td>
				<td><?php echo$row[7] ?></td>
			</tr>
			<?php	$cnt++;
			} 
		CloseDb() ?>
		</table>
		<!-- END TABLE CENTER -->    
	</body>
	<script language="javascript">
	window.print();
	</script>
</html>