<?php 
	/* * [N]**
	* LMS MAN Kota Blitar
	* 
	* 
	* @version: 1.0 (January 09, 2013)
	* 
	* 
	* Copyright (C)2016
	* 
	* 
	* 
	*
	*
	* 
	* 
	* 
	* **[N]* */ 
?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');
	require_once('../include/exceldata.php');
	require_once('../cek.php');
	
	$namaekskul = $_REQUEST['namaekskul'];
	if (isset($_REQUEST['namaekskul']))
		$namaekskul = $_REQUEST['namaekskul'];
		
	$varbaris = 20;
	if (isset($_REQUEST['varbaris']))
		$varbaris = $_REQUEST['varbaris'];

	$page = 0;
	if (isset($_REQUEST['page']))
		$page = $_REQUEST['page'];

	$hal = 0;
	if (isset($_REQUEST['hal']))
		$hal = $_REQUEST['hal'];

	$urut = "nis";
	if (isset($_REQUEST['urut']))
		$urut = $_REQUEST['urut'];
	$urutan = "ASC";
	if (isset($_REQUEST['urutan']))
		$urutan = $_REQUEST['urutan'];

	OpenDb();	
	$sql_cari = "SELECT id_ekskul FROM ekskul WHERE nama_ekskul='$namaekskul'";
	$result_cari = QueryDb($sql_cari);
	$row_cari = @mysql_fetch_row($result_cari);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Nilai Ektra Kurikuler</title>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript">
			function refresh() {
				document.location.href = "nilaiekskul_bottom.php?namaekskul=<?php echo$namaekskul?>";
			}

			function edit(id_pengurus) {
				newWindow('nilaiekskul_edit.php?namaekskul=<?php echo$namaekskul?>&id_pengurus='+id_pengurus, 'UbahStruktur','600','420','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function change_urut(urut,urutan) {
				if (urutan =="ASC"){
					urutan="DESC"
				} else {
					urutan="ASC"
				}
				document.location.href = "nilaiekskul_bottom.php?namaekskul=<?php echo$namaekskul?>&urut="+urut+"&urutan="+urutan+"&page=<?php echo $page ?>&hal=<?php echo $hal ?>&varbaris=<?php echo $varbaris ?>";
			}

			function cetak() {
				var urut = document.getElementById('urut').value;
				var urutan = document.getElementById('urutan').value;
				var total=document.getElementById("total").value;

				newWindow('nilaiekskul_cetak.php?namaekskul=<?php echo$namaekskul?>&urut='+urut+'&urutan='+urutan+'&varbaris=<?php echo $varbaris ?>&page=<?php echo $page ?>&total='+total, 'CetakSiswa','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function exel(){
				var urut = document.getElementById('urut').value;
				var urutan = document.getElementById('urutan').value;

				newWindow('struktur_cetak_exel.php?namaekskul=<?php echo$namaekskul?>&urut='+urut+'&urutan='+urutan, 'CetakEkskul','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function change_page(page) {
				var varbaris=document.getElementById("varbaris").value;

				document.location.href = "nilaiekskul_bottom.php?namaekskul=<?php echo$namaekskul?>&page="+page+"&urut=<?php echo $urut ?>&urutan=<?php echo $urutan ?>&varbaris="+varbaris+"&hal="+page;
			}

			function change_hal() {
				var hal = document.getElementById("hal").value;
				var varbaris=document.getElementById("varbaris").value;

				document.location.href="nilaiekskul_bottom.php?namaekskul=<?php echo$namaekskul?>&page="+hal+"&hal="+hal+"&urut=<?php echo $urut ?>&urutan=<?php echo $urutan ?>&varbaris="+varbaris;
			}

			function change_baris() {
				var tingkat = document.getElementById('tingkat').value;
				var varbaris=document.getElementById("varbaris").value;

				document.location.href= "nilaiekskul_bottom.php?namaekskul=<?php echo$namaekskul?>&urut=<?php echo $urut ?>&urutan=<?php echo $urutan ?>&varbaris="+varbaris;
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0">
		<input type="hidden" name="urut" id="urut" value="<?php echo $urut ?>" />
		<input type="hidden" name="urutan" id="urutan" value="<?php echo $urutan ?>" />
		<input type="hidden" name="namaekskul" id="namaekskul" value="<?php echo $namaekskul ?>" />
		<?//echo"test : $namaekskul";?>
		<table border="0" width="70%" align="center">
			<!-- TABLE CENTER -->
			<tr>
				<td align="right">
					<?php
					$sql_tot = "SELECT id_pengurus, nis, id_jabatan, nilai, deskripsi FROM ekskul_pengurus WHERE id_ekskul='$row_cari[0]' ORDER BY id_pengurus";
					$result_tot = QueryDb($sql_tot);
					$total = ceil(mysql_num_rows($result_tot) / (int) $varbaris);
					$jumlah = mysql_num_rows($result_tot);
					$akhir = ceil($jumlah / 5) * 5;

					$sql = "SELECT id_pengurus, nis, id_jabatan, nilai, deskripsi FROM ekskul_pengurus WHERE id_ekskul='$row_cari[0]' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
					$result = QueryDb($sql);

					if (@mysql_num_rows($result) > 0) {
						?>
						<input type="hidden" name="total" id="total" value="<?php echo $total ?>"/>
						<table width="100%" border="0" align="center">
							<tr>
								<td align="right">
									<a href="#" onClick="refresh()">
										<img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh
									</a>&nbsp;&nbsp;
									<a href="JavaScript:cetak()">
										<img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak
									</a>&nbsp;&nbsp; 
								</td>
							</tr>    
						</table>
						<br />       
						<table border="1" width="100%" id="table" class="tab" align="center" style="border-collapse:collapse" bordercolor="#000000" />
						<tr class="header" height="30" align="center">		
							<td width="4%">No</td>
							<td width="20%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('nis','<?php echo $urutan ?>')">Nama Siswa <?php echo change_urut('nis', $urut, $urutan) ?></td>
							<td width="15%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('id_jabatan','<?php echo $urutan ?>')">Jabatan <?php echo change_urut('id_jabatan', $urut, $urutan) ?></td>
							<td width="15%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('nilai','<?php echo $urutan ?>')">Nilai <?php echo change_urut('nilai', $urut, $urutan) ?></td>
							<td width="*" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('deskripsi','<?php echo $urutan ?>')">Deskripsi <?php echo change_urut('deskripsi', $urut, $urutan) ?></td>
							<td width="15%">&nbsp;</td>
						</tr>
						<?php
							CloseDb();
							if ($page == 0) {
								$cnt = 1;
							} else {
								$cnt = (int) $page * (int) $varbaris + 1;
							}
							while ($row = @mysql_fetch_row($result)) {						
						?>	
						<tr>
							<td height="25" align="center"><?php echo $cnt ?></td>
							<?php
							OpenDb();
							$sql_nama = "SELECT nama FROM siswa WHERE nis=$row[1]";
							//echo "$sql_nama";
							$rs_nama = QueryDb($sql_nama);
							$row_nama = @mysql_fetch_row($rs_nama);
							
							$sql_jab = "SELECT nama_jabatan FROM ekskul_struktur WHERE id_jabatan='$row[2]' AND id_ekskul='$row_cari[0]'";
							//echo "<br> $sql_jab";
							$rs_jab = QueryDb($sql_jab);
							$row_jab = @mysql_fetch_row($rs_jab);
							CloseDb();
							?>
							<td height="25" align="center"><?php echo $row[1] ?> - <?php echo $row_nama[0] ?></td>
							<td height="25" align="center"><?php echo $row_jab[0] ?></td>
							<td height="25" align="center"><?php echo $row[3] ?></td>
							<td height="25" align="left"><?php echo $row[4] ?></td>
							<td height="25" align="center">
								<a href="JavaScript:edit(<?php echo$row[0]?>)" /><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Ektra Kulikuler!', this, event, '80px')"/></a>&nbsp;
							</td>
						</tr>
						<?php
								$cnt++;
							}
						?>			

						<!-- END TABLE CONTENT -->
						</table>

						<script language='JavaScript'>
							Tables('table', 1, 0);
						</script>

						<?php
						if ($page == 0) {
							$disback = "style='visibility:hidden;'";
							$disnext = "style='visibility:visible;'";
						}
						if ($page < $total && $page > 0) {
							$disback = "style='visibility:visible;'";
							$disnext = "style='visibility:visible;'";
						}
						if ($page == $total - 1 && $page > 0) {
							$disback = "style='visibility:visible;'";
							$disnext = "style='visibility:hidden;'";
						}
						if ($page == $total - 1 && $page == 0) {
							$disback = "style='visibility:hidden;'";
							$disnext = "style='visibility:hidden;'";
						}
						?>

					</td>
				</tr> 
				<tr>
					<td>
						<table border="0"width="100%" align="center" cellpadding="0" cellspacing="0">	
							<tr>
								<td width="30%" align="left">Halaman
									<select name="hal" id="hal" onChange="change_hal()">
										<?php for ($m = 0; $m < $total; $m++) { ?>
											<option value="<?php echo $m ?>" <?php echo IntIsSelected($hal, $m) ?>><?php echo $m + 1 ?></option>
										<?php } ?>
									</select>
									dari <?php echo $total ?> halaman

									<?php
									// Navigasi halaman berikutnya dan sebelumnya
									?>
								</td>
								<!--td align="center">
							<input <?php echo $disback ?> type="button" class="but" name="back" value=" << " onClick="change_page('<?php echo (int) $page - 1 ?>')" onMouseOver="showhint('Sebelumnya', this, event, '75px')">
								<?php
								/* for($a=0;$a<$total;$a++){
								  if ($page==$a){
								  echo "<font face='verdana' color='red'><strong>".($a+1)."</strong></font> ";
								  } else {
								  echo "<a href='#' onClick=\"change_page('".$a."')\">".($a+1)."</a> ";
								  }

								  } */
								?>
									 <input <?php echo $disnext ?> type="button" class="but" name="next" value=" >> " onClick="change_page('<?php echo (int) $page + 1 ?>')" onMouseOver="showhint('Berikutnya', this, event, '75px')">
										</td-->
								<td width="30%" align="right">Jumlah baris per halaman
									<select name="varbaris" id="varbaris" onChange="change_baris()">
										<?php for ($m = 10; $m <= 100; $m = $m + 10) { ?>
											<option value="<?php echo $m ?>" <?php echo IntIsSelected($varbaris, $m) ?>><?php echo $m ?></option>
										<?php } ?>

									</select></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>  
		<?php }
		else if($row_cari[0] == ""){
			?>
			<table width="100%" border="0" align="center">          
				<tr>
					<td align="center" valign="middle" height="300">
						<font size = "2" color ="red"><b>Silahkan melakukan pencarian dengan memilih nama ekstra kurikuler </b></font>
					</td>
				</tr>
			</table>  
			<?php
		}else { ?>
			<table width="100%" border="0" align="center">          
				<tr>
					<td align="center" valign="middle" height="300">
						<font size = "2" color ="red"><b>Tidak ditemukan adanya data. 
								<?php //if (SI_USER_LEVEL() != $SI_USER_STAFF) {  ?>
								<br />Klik &nbsp;<a href="JavaScript:tambah(<?php echo$row_cari[0]?>)" ><font size = "2" color ="green">di sini</font></a>&nbsp;untuk mengisi data baru. 
								<?php //} ?>        
							</b></font>
					</td>
				</tr>
			</table>  
		<?php } ?> 
		</td></tr>

		<!-- END TABLE BACKGROUND IMAGE -->
		</table> 
		<?php
		CloseDb();
		?>
	</body>
</html>