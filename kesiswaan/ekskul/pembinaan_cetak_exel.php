<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');

	header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
	header('Content-Type: application/x-msexcel'); // Other browsers  
	header('Content-Disposition: attachment; filename=Data_Ekstra_Kurikuler.xls');
	header('Expires: 0');  
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	
	$bulan	= $_REQUEST['bulan'];
	$urut = $_REQUEST['urut'];
	$urutan = $_REQUEST['urutan'];
	$varbaris = $_REQUEST['varbaris'];	
	$page = $_REQUEST['page'];
	$total = $_REQUEST['total'];
	
	$nama_bulan = "";
	if($bulan == 01){
		$nama_bulan = "Januari";
	}
	else if($bulan == 02){
		$nama_bulan = "Februari";
	}
	else if($bulan == 03){
		$nama_bulan = "Maret";
	}
	else if($bulan == 04){
		$nama_bulan = "April";
	}
	else if($bulan == 05){
		$nama_bulan = "Mei";
	}
	else if($bulan == 06){
		$nama_bulan = "Juni";
	}
	else if($bulan == 07){
		$nama_bulan = "Juli";
	}
	else if($bulan == 08){
		$nama_bulan = "Agustus";
	}
	else if($bulan == 09){
		$nama_bulan = "September";
	}
	else if($bulan == 10){
		$nama_bulan = "Oktober";
	}
	else if($bulan == 11){
		$nama_bulan = "November";
	}
	else if($bulan == 12){
		$nama_bulan = "Desember";
	}

	OpenDb();
	$sql = "SELECT * FROM ekskul_pembinaan WHERE MONTH(tanggal)='$bulan' ORDER BY $urut $urutan ";
	$result=QueryDb($sql);
	if (@mysql_num_rows($result)<>0){ ?>
	<html>
		<head>
			<title>
				Jadwal Pembinaan Ekstra Kurikuler <?php echo$namaekskul?>
			</title>
			<style type="text/css">
			<!--
			.style1 {color: #FFFFFF}
			.style2 {font-size: 14px}
			-->
			</style>
		</head>
		<body>
			<table width="700" border="0">
				<tr>
					<td><div align="center">Jadwal Pembinaan Ekstra Kurikuler Bulan <?php echo$namaebulan?></div></td>
				</tr>
				<tr>
					<td>
						<table border="1">
						<tr height="30">
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">No</div></td>
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Nama Ektra Kurikuler</div></td>
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Jam</div></td>
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Tanggal</div></td>
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Tempat</div></td>
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Pembina</div></td>
							<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Keterangan</div></td>
						</tr>
						<?php
						$cnt=1;
						while ($row=@mysql_fetch_array($result)){
						?>
						<tr height="25">
							<?php
							$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$row[1]";
							$result_eks = QueryDb($sql_eks);
							$row_eks = @mysql_fetch_row($result_eks);
							
							$sql_nip = "SELECT nama FROM $g_db_pegawai.pegawai WHERE nip=$row[5]";
							$result_nip = QueryDb($sql_nip);
							$row_nip = @mysql_fetch_row($result_nip);
							?>
							<td width="3" align="center"><?php echo$cnt?></td>
							<td align="left"><?php echo$row_eks[0]?></td>
							<td align="left"><?php echo$row[2]?></td>
							<td align="left"><?php echo$row[3]?></td>
							<td align="left"><?php echo$row[4]?></td>
							<td align="left"><?php echo$row_nip[0]?></td>
							<td align="left"><?php echo$row[6]?></td>
						</tr>
						<?php
						$cnt++;
						}
						?>
						</table>
					</td>
				</tr>
			</table>
		</body>
	</html>
<?php
}
CloseDb();
?>