<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	$id_ekskul = $_REQUEST['id_ekskul'];
	
	if (isset($_REQUEST['id_pengurus']))
		$id_pengurus = CQ($_REQUEST['id_pengurus']);
	if (isset($_REQUEST['nis']))
		$nis = CQ($_REQUEST['nis']);
	if (isset($_REQUEST['nama']))
		$nama = CQ($_REQUEST['nama']);
	if (isset($_REQUEST['nama_ekskul']))
		$nama_ekskul = CQ($_REQUEST['nama_ekskul']);
	if (isset($_REQUEST['nama_jabatan']))
		$nama_jabatan = CQ($_REQUEST['nama_jabatan']);
	if (isset($_REQUEST['tanggal']))
		$tanggal = CQ($_REQUEST['tanggal']);
	if (isset($_REQUEST['periode']))
		$periode = CQ($_REQUEST['periode']);
	if (isset($_REQUEST['keterangan']))
		$keterangan = CQ($_REQUEST['keterangan']);

	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		$sql_cek = "SELECT * FROM ekskul_pengurus WHERE id_jabatan=$nama_jabatan AND periode = '$_REQUEST[periode]' ";
		//echo"$sql_cek";
		$result_cek = QueryDb($sql_cek);

		if (@mysql_num_rows($result_cek) > 0) {
			$ERROR_MSG = $nama_jabatan . " / " . $periode . " sudah digunakan!";
		} 
		else {
			$sql_s = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul='$_REQUEST[id_ekskul]'";
			$result_s = QueryDb($sql_s);
			$row_s = @mysql_fetch_row($result_s);
			
			$sql = "INSERT INTO ekskul_pengurus SET id_pengurus='$id_pengurus', nis='$nis', id_ekskul='$id_ekskul', id_jabatan='$nama_jabatan', awal_kepengurusan='$tanggal', periode='$periode', keterangan='$keterangan'";
			//echo "$sql";
			$result = QueryDb($sql);
			if ($result) {
				?>
				<script language="javascript">
					opener.refresh();
					window.close();
				</script> 
				<?php
			}
		}	
	}
	CloseDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Tambah Pengurus Ektra Kurikuler]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../script/cal2.js"></script>
		<script language="javascript" src="../script/cal_conf2.js"></script>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nip')
						caripegawai();
					return false;
				} 
				return true;
			}

			function validate() {
				return validateEmptyText('nis', 'NIS dan Nama Siswa') &&
					   validateEmptyText('nama_ekskul', 'Nama Ekstra Kurikuler') && 
					   validateEmptyText('nama_jabatan', 'Nama Jabatan') &&
					   validateEmptyText('periode', 'Periode');
			}
			
			function carisiswa() {
				//parent.footer.location.href = "blank_presensi_siswa.php?tipe='harian'";	
				newWindow('../library/siswa.php?flag=0', 'CariSiswa','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			}

			function acceptSiswa(nis, nama) {
				document.getElementById('nis').value = nis;
				document.getElementById('nis1').value = nis;
				document.getElementById('nama').value = nama;
				document.getElementById('nama1').value = nama;
				document.getElementById('nama_ekskul').focus();
			}
			
			function panggil(elem){
				var lain = new Array('id_pengurus','nis','nama_ekskul','nama_jabatan','tanggal','periode','keterangan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nis').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Tambah Pengurus Ektra Kurikuler :.
					</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="id_ekskul" id="id_ekskul" value="<?php echo $id_ekskul ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								OpenDb();
								$sql = "SELECT max(id_pengurus) FROM ekskul_pengurus";
								$rs = QueryDb($sql);
								$next_id = mysql_fetch_row($rs);
							?>
							<tr>
								<td><strong>Id Pengurusan</strong></td>
								<td><input type="text" class="disabled" readonly name="id_pengurus" id="id_pengurus" value="<?php echo $next_id[0]+1?>" size="3"  /></td>
							</tr>
							<tr>
								<td><strong>NIS</strong></td>
								<td>
									<input name="nis" type="text" class="disabled" id="nis" value="<?php echo$_REQUEST['nis']?>" size="10" readonly onclick="carisiswa()"/>
									<input type="hidden" name="nis1" id="nis1" value="<?php echo$_REQUEST['nis']?>">
									<input name="nama" type="text" class="disabled" id="nama" value="<?php echo$_REQUEST['nama']?>" size="25" readonly onclick="carisiswa()"/>
									<input type="hidden" name="nama1" id="nama1" value="<?php echo$_REQUEST['nama']?>"> 
											
									<a href="JavaScript:carisiswa()"><img src="../images/ico/cari.png" border="0" /></a>
								</td>
							</tr>
							<?php
								$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$id_ekskul";
								$rs_eks = QueryDb($sql_eks);
								$row_eks = mysql_fetch_row($rs_eks);
								
								$sql_jab = "SELECT id_jabatan, nama_jabatan FROM ekskul_struktur WHERE id_ekskul=$id_ekskul ORDER BY id_jabatan";
								$rs_jab = QueryDb($sql_jab);
								
								$sql_per = "SELECT tahunajaran FROM tahunajaran ORDER BY tahunajaran";
								$rs_per = QueryDb($sql_per);
							?>
							<tr>
								<td><strong>Nama Ekskul</strong></td>
								<td><input type="text" name="nama_ekskul" id="nama_ekskul" class="disabled" size="20" onFocus="showhint('Nama Ekskul tidak boleh kosong !', this, event, '120px');panggil('nama_ekskul')" value="<?php echo $row_eks[0]?>" onKeyPress="return focusNext('nama_jabatan', event)" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nama Jabatan</strong></td>
								<td>
									<select name="nama_jabatan" id="nama_jabatan" style="width:130px;" onKeyPress="return focusNext('tanggal', event)" onfocus="panggil('nama_jabatan')">
										<option value="">--pilih jabatan--</option>
										<?php
										while($row_jab = mysql_fetch_row($rs_jab)){
										?><option value="<?php echo $row_jab[0]?>" > <?php echo $row_jab[1] ?> </option><?
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td><strong>Awal Kepengurusan</strong></td>
								<td>
									<input type="text" name="tanggal" id="tanggal" size="15" onClick="showCal('Calendar2');" onFocus="showhint('Awal Kepengurusan tidak boleh kosong !', this, event, '120px');panggil('tanggal')" value="<?php echo $tanggal?>" onKeyPress="return focusNext('periode', event)"/>
									<a href="javascript:showCal('Calendar2');"> <img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
								</td>
							</tr>
							<tr>
								<td><strong>Periode</strong></td>
								<td>
									<select name="periode" id="periode" style="width:130px;" onKeyPress="return focusNext('keterangan', event)" onfocus="panggil('periode')">
										<option value="">--Pilih Periode--</option>
										<?php
										while($row_per = mysql_fetch_row($rs_per)){
										?><option value="<?php echo $row_per[0]?>" > <?php echo $row_per[0] ?> </option><?
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td valign="top"><strong>Keterangan</strong></td>
								<td><textarea name="keterangan" id="keterangan" rows="3" cols="45"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('keterangan')"><?php echo $keterangan ?></textarea>    </td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>
		<?php CloseDb(); ?>
		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>