<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');

	header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
	header('Content-Type: application/x-msexcel'); // Other browsers  
	header('Content-Disposition: attachment; filename=Data_Pengurus_Ekstra_Kurikuler.xls');
	header('Expires: 0');  
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	
	$namaekskul	= $_REQUEST['namaekskul'];
	$thnkep = $_REQUEST['thnkep'];
	
	$urut = $_REQUEST['urut'];
	$urutan = $_REQUEST['urutan'];
	$varbaris = $_REQUEST['varbaris'];	
	$page = $_REQUEST['page'];
	$total = $_REQUEST['total'];

	OpenDb();
	$sql_cari = "SELECT id_ekskul FROM ekskul WHERE nama_ekskul='$namaekskul'";
	$result_cari = QueryDb($sql_cari);
	$row_cari = @mysql_fetch_row($result_cari);
	
	$sql = "SELECT * FROM ekskul_pengurus WHERE id_ekskul='$row_cari[0]' AND periode='$thnkep' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";  
	//echo"$sql";
	$result=QueryDb($sql);

	if (@mysql_num_rows($result)<>0){
?>
<html>
	<head>
		<title>
			Data Pengurus Ekstra Kurikuler <?php echo$namaekskul?>
		</title>
		<style type="text/css">
		<!--
		.style1 {color: #FFFFFF}
		.style2 {font-size: 14px}
		-->
		</style>
	</head>
	<body>
		<table width="700" border="0">
			<tr>
				<td><div align="center">Data Struktur Ekstra Kurikuler <?php echo$namaekskul?></div></td>
			</tr>
			<tr>
				<td>
					<table border="1">
					<tr height="30">
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">No</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Nama Siswa</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Kelas</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Nama Ekskul</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Nama Jabatan</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Awal Kepengurusan</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Periode</div></td>
						<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Keterangan</div></td>
					</tr>
					<?php
					$cnt=1;
					while ($row=@mysql_fetch_array($result)){
					?>

					<?php
						$sql_nama = "SELECT nama, idkelas FROM siswa WHERE nis=$row[1]";
						$rs_nama = QueryDb($sql_nama);
						$row_nama = @mysql_fetch_row($rs_nama);
						
						$sql_kelas = "SELECT kelas FROM kelas WHERE replid=$row_nama[1]";
						$rs_kelas = QueryDb($sql_kelas);
						$row_kelas = @mysql_fetch_row($rs_kelas);
						
						$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$row[2]";
						//echo"$sql_eks";
						$rs_eks = QueryDb($sql_eks);
						$row_eks = @mysql_fetch_row($rs_eks);
						
						$sql_jab = "SELECT nama_jabatan FROM ekskul_struktur WHERE id_jabatan=$row[3]";
						//echo"$sql_jab";
						$rs_jab = QueryDb($sql_jab);
						$row_jab = @mysql_fetch_row($rs_jab);
					?>
					
					<tr height="25">
						<td width="3" align="center"><?php echo$cnt ?></td>
						<td  align="left"><?php echo$row_nama[0] ?></td>      
						<td  align="left"><?php echo$row_kelas[0] ?></td>
						<td  align="left"><?php echo$row_eks[0] ?></td>
						<td  align="left"><?php echo$row_jab[0] ?></td>
						<td  align="left"><?php echo$row[4] ?></td>
						<td  align="left"><?php echo$row[5] ?></td>
						<td  align="left"><?php echo$row[6] ?></td>
					</tr>
					<?php
					$cnt++;
					}
					?>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php
}
CloseDb();
?>