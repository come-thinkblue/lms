<?php 
	/* * [N]**
	* LMS MAN Kota Blitar
	* 
	* 
	* @version: 1.0 (January 09, 2013)
	* 
	* 
	* Copyright (C)2016
	* 
	* 
	* 
	*
	*
	* 
	* 
	* 
	* **[N]* */ 
?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');
	require_once('../include/exceldata.php');
	require_once('../cek.php');
	
	$bulan = $_REQUEST['bulan'];
	if (isset($_REQUEST['bulan']))
		$bulan = $_REQUEST['bulan'];
		
	$varbaris = 20;
	if (isset($_REQUEST['varbaris']))
		$varbaris = $_REQUEST['varbaris'];

	$page = 0;
	if (isset($_REQUEST['page']))
		$page = $_REQUEST['page'];

	$hal = 0;
	if (isset($_REQUEST['hal']))
		$hal = $_REQUEST['hal'];

	$urut = "nama_kegiatan";
	if (isset($_REQUEST['urut']))
		$urut = $_REQUEST['urut'];
	$urutan = "ASC";
	if (isset($_REQUEST['urutan']))
		$urutan = $_REQUEST['urutan'];

	OpenDb();
	
	$op = $_REQUEST['op'];
	$id = $_REQUEST['id_kegiatan'];
	if ($op == "hapus"){
		$sql = "DELETE FROM ekskul_kegiatan WHERE id_kegiatan = $id";
		//echo "$bulan";
		QueryDb($sql);
		?>
			<script language="javascript">
				refresh();
			</script>
		<?php
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Struktur Ektra Kurikuler</title>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript">
			function refresh() {
				parent.footer.location.href="kegiatan_bottom.php?bulan=<?php echo$bulan?>";
			}

			function tambah() {
				newWindow('kegiatan_add.php','TambahStruktur','600','400','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function edit(id_kegiatan) {
				newWindow('kegiatan_edit.php?id_kegiatan='+id_kegiatan, 'UbahStruktur','600','420','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function hapus(id_kegiatan) {
				if (confirm('Apakah anda yakin akan menghapus jadwal kegiatan ekstra kurikuler ini?'))
					document.location.href = "kegiatan_bottom.php?bulan=<?php echo$bulan?>&op=hapus&id_kegiatan="+id_kegiatan;
			}

			function change_urut(urut,urutan) {
				if (urutan =="ASC"){
					urutan="DESC"
				} else {
					urutan="ASC"
				}
				document.location.href = "kegiatan_bottom.php?bulan=<?php echo$bulan?>&urut="+urut+"&urutan="+urutan+"&page=<?php echo $page ?>&hal=<?php echo $hal ?>&varbaris=<?php echo $varbaris ?>";
			}

			function cetak() {
				var urut = document.getElementById('urut').value;
				var urutan = document.getElementById('urutan').value;
				var total=document.getElementById("total").value;

				newWindow('kegiatan_cetak.php?bulan=<?php echo$bulan?>&urut='+urut+'&urutan='+urutan+'&varbaris=<?php echo $varbaris ?>&page=<?php echo $page ?>&total='+total, 'CetakSiswa','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function exel(){
				var urut = document.getElementById('urut').value;
				var urutan = document.getElementById('urutan').value;

				newWindow('kegiatan_cetak_exel.php?bulan=<?php echo$bulan?>&urut='+urut+'&urutan='+urutan, 'CetakEkskul','790','650','resizable=1,scrollbars=1,status=0,toolbar=0')
			}

			function change_page(page) {
				var varbaris=document.getElementById("varbaris").value;

				document.location.href = "kegiatan_bottom.php?bulan=<?php echo$bulan?>&page="+page+"&urut=<?php echo $urut ?>&urutan=<?php echo $urutan ?>&varbaris="+varbaris+"&hal="+page;
			}

			function change_hal() {
				var hal = document.getElementById("hal").value;
				var varbaris=document.getElementById("varbaris").value;

				document.location.href="kegiatan_bottom.php?bulan=<?php echo$bulan?>&page="+hal+"&hal="+hal+"&urut=<?php echo $urut ?>&urutan=<?php echo $urutan ?>&varbaris="+varbaris;
			}

			function change_baris() {
				var tingkat = document.getElementById('tingkat').value;
				var varbaris=document.getElementById("varbaris").value;

				document.location.href= "kegiatan_bottom.php?bulan=<?php echo$bulan?>&urut=<?php echo $urut ?>&urutan=<?php echo $urutan ?>&varbaris="+varbaris;
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0">
		<input type="hidden" name="urut" id="urut" value="<?php echo $urut ?>" />
		<input type="hidden" name="urutan" id="urutan" value="<?php echo $urutan ?>" />
		<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan ?>" />
		<?//echo"test : $bulan";?>
		<table border="0" width="90%" align="center">
			<!-- TABLE CENTER -->
			<tr>
				<td align="right">
					<?php
					if($bulan == 'all'){
						$sql_tot = "SELECT * FROM ekskul_kegiatan ORDER BY id_kegiatan";
					}
					else{
						$sql_tot = "SELECT * FROM ekskul_kegiatan WHERE MONTH(tanggal)='$bulan' ORDER BY id_kegiatan";
					}
					//echo"$sql_tot";
					$result_tot = QueryDb($sql_tot);
					$total = ceil(mysql_num_rows($result_tot) / (int) $varbaris);
					$jumlah = mysql_num_rows($result_tot);
					$akhir = ceil($jumlah / 5) * 5;

					if($bulan == 'all'){
						$sql = "SELECT * FROM ekskul_kegiatan ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
					}
					else{
						$sql = "SELECT * FROM ekskul_kegiatan WHERE MONTH(tanggal)='$bulan' ORDER BY $urut $urutan LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
					}
					//echo "$sql";
					$result = QueryDb($sql);

					if (@mysql_num_rows($result) > 0) {
						?>
						<input type="hidden" name="total" id="total" value="<?php echo $total ?>"/>
						<table width="100%" border="0" align="center">
							<tr>
								<td align="right">
									<a href="#" onClick="refresh()">
										<img src="../images/ico/refresh.png" border="0" onMouseOver="showhint('Refresh!', this, event, '50px')"/>&nbsp;Refresh
									</a>&nbsp;&nbsp;
									<a href="#" onClick="JavaScript:exel()">
										<img src="../images/ico/excel.png" border="0" onMouseOver="showhint('Cetak dalam format Excel!', this, event, '80px')"/>&nbsp;Cetak Excel
									</a>&nbsp;&nbsp;
									<a href="JavaScript:cetak()">
										<img src="../images/ico/print.png" border="0" onMouseOver="showhint('Cetak!', this, event, '50px')"/>&nbsp;Cetak
									</a>&nbsp;&nbsp; 
									<a href="#" onClick="JavaScript:tambah()">
										<img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah!', this, event, '50px')" />&nbsp;Tambah Jadwal Kegiatan Ekstra Kurikuler
									</a>
								</td>
							</tr>    
						</table>
						<br />       
						<table border="1" width="100%" id="table" class="tab" align="center" style="border-collapse:collapse" bordercolor="#000000" />
						<tr class="header" height="30" align="center">		
							<td width="4%">No</td>
							<td width="10%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('nama_kegiatan','<?php echo $urutan ?>')"> Nama Kegiatan <?php echo change_urut('nama_kegiatan', $urut, $urutan) ?></td>
							<td width="10%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('id_ekskul','<?php echo $urutan ?>')"> Penyelenggara <?php echo change_urut('id_ekskul', $urut, $urutan) ?></td>
							<td width="10%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('jam','<?php echo $urutan ?>')">Jam <?php echo change_urut('jam', $urut, $urutan) ?></td>
							<td width="10%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('tanggal','<?php echo $urutan ?>')">Tanggal <?php echo change_urut('tanggal', $urut, $urutan) ?></td>
							<td width="10%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('tempat','<?php echo $urutan ?>')">Tempat <?php echo change_urut('tempat', $urut, $urutan) ?></td>
							<td width="15%"onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('penanggungjawab','<?php echo $urutan ?>')">Penanggungjawab <?php echo change_urut('penanggungjawab', $urut, $urutan) ?></td>
							<td width="*" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('keterangan','<?php echo $urutan ?>')">Keterangan <?php echo change_urut('keterangan', $urut, $urutan) ?></td>
							<td width="15%">&nbsp;</td>
						</tr>
						<?php
							CloseDb();
							if ($page == 0) {
								$cnt = 1;
							} else {
								$cnt = (int) $page * (int) $varbaris + 1;
							}
							while ($row = @mysql_fetch_row($result)) {							
						?>	
						<tr>        			
							<td height="25" align="center"><?php echo $cnt ?></td>
							<?php
							OpenDB();
							$sql_eks = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$row[2]";
							$result_eks = QueryDb($sql_eks);
							$row_eks = @mysql_fetch_row($result_eks);
							
							$sql_nip = "SELECT nama FROM $g_db_pegawai.pegawai WHERE nip=$row[6]";
							$result_nip = QueryDb($sql_nip);
							$row_nip = @mysql_fetch_row($result_nip);
							CloseDb();
							?>
							<td height="25" align="left"><?php echo $row[1] ?> </td>
							<td height="25" align="left"><?php echo $row_eks[0] ?></td>
							<td height="25" align="center"><?php echo $row[3] ?> </td>
							<td height="25" align="left"><?php echo $row[4] ?> </td>
							<td height="25" align="left"><?php echo $row[5] ?> </td>
							<td height="25" align="left"><?php echo $row[6] ?> -  <?php echo $row_nip[0] ?></td>
							<td height="25" align="left"><?php echo $row[7] ?> </td>
							<td height="25" align="center">
								<a href="JavaScript:edit(<?php echo$row[0]?>)" /><img src="../images/ico/ubah.png" border="0" onMouseOver="showhint('Ubah Ektra Kulikuler!', this, event, '80px')"/></a>&nbsp;
								<?php 	if (SI_USER_LEVEL() != $SI_USER_STAFF) {	?>             	
									<a href="JavaScript:hapus(<?php echo$row[0]?>)"><img src="../images/ico/hapus.png" border="0" onMouseOver="showhint('Hapus Data Ekstra Kurikuler!', this, event, '80px')"/></a>
								<?php	} ?>
							</td>
						</tr>
						<?php
								$cnt++;
							}
						?>			

						<!-- END TABLE CONTENT -->
						</table>

						<script language='JavaScript'>
							Tables('table', 1, 0);
						</script>

						<?php
						if ($page == 0) {
							$disback = "style='visibility:hidden;'";
							$disnext = "style='visibility:visible;'";
						}
						if ($page < $total && $page > 0) {
							$disback = "style='visibility:visible;'";
							$disnext = "style='visibility:visible;'";
						}
						if ($page == $total - 1 && $page > 0) {
							$disback = "style='visibility:visible;'";
							$disnext = "style='visibility:hidden;'";
						}
						if ($page == $total - 1 && $page == 0) {
							$disback = "style='visibility:hidden;'";
							$disnext = "style='visibility:hidden;'";
						}
						?>

					</td>
				</tr> 
				<tr>
					<td>
						<table border="0"width="100%" align="center" cellpadding="0" cellspacing="0">	
							<tr>
								<td width="30%" align="left">Halaman
									<select name="hal" id="hal" onChange="change_hal()">
										<?php for ($m = 0; $m < $total; $m++) { ?>
											<option value="<?php echo $m ?>" <?php echo IntIsSelected($hal, $m) ?>><?php echo $m + 1 ?></option>
										<?php } ?>
									</select>
									dari <?php echo $total ?> halaman

									<?php
									// Navigasi halaman berikutnya dan sebelumnya
									?>
								</td>
								<!--td align="center">
							<input <?php echo $disback ?> type="button" class="but" name="back" value=" << " onClick="change_page('<?php echo (int) $page - 1 ?>')" onMouseOver="showhint('Sebelumnya', this, event, '75px')">
								<?php
								/* for($a=0;$a<$total;$a++){
								  if ($page==$a){
								  echo "<font face='verdana' color='red'><strong>".($a+1)."</strong></font> ";
								  } else {
								  echo "<a href='#' onClick=\"change_page('".$a."')\">".($a+1)."</a> ";
								  }

								  } */
								?>
									 <input <?php echo $disnext ?> type="button" class="but" name="next" value=" >> " onClick="change_page('<?php echo (int) $page + 1 ?>')" onMouseOver="showhint('Berikutnya', this, event, '75px')">
										</td-->
								<td width="30%" align="right">Jumlah baris per halaman
									<select name="varbaris" id="varbaris" onChange="change_baris()">
										<?php for ($m = 10; $m <= 100; $m = $m + 10) { ?>
											<option value="<?php echo $m ?>" <?php echo IntIsSelected($varbaris, $m) ?>><?php echo $m ?></option>
										<?php } ?>

									</select></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>  
		<?php } else { ?>

			<table width="100%" border="0" align="center">          
				<tr>
					<td align="center" valign="middle" height="300">
						<font size = "2" color ="red"><b>Tidak ditemukan adanya data. 
								<?php //if (SI_USER_LEVEL() != $SI_USER_STAFF) {  ?>
								<br />Klik &nbsp;<a href="JavaScript:tambah()" ><font size = "2" color ="green">di sini</font></a>&nbsp;untuk mengisi data baru. 
								<?php //} ?>        
							</b></font>
					</td>
				</tr>
			</table>  
		<?php } ?> 
		</td></tr>

		<!-- END TABLE BACKGROUND IMAGE -->
		</table> 
		<?php
		CloseDb();
		?>
	</body>
</html>