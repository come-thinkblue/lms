<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');
require_once('../cek.php');

$bulan = $_REQUEST['bulan'];
if (isset($_REQUEST['bulan']))
    $bulan = $_REQUEST['bulan'];

OpenDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Jadwal kegiatan Ekstra Kurikuler</title>
		<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
		<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/ajax.js"></script>
		<script language="javascript">
			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'tabel') {
						show_kelas();
						panggil('tabel');
					} 
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('bulan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

			function show_kelas() {
				var bulan = document.getElementById("bulan").value;
				parent.footer.location.href="kegiatan_bottom.php?bulan="+bulan;
			}

		</script>
		<style type="text/css">
			<!--
			.style1 {font-weight: bold}
			-->
		</style>
	</head>
	<body onLoad="show_kelas()">
		<table border="0" width="100%" >
			<!-- TABLE TITLE -->
			<tr>
				<td rowspan="3" width="53%">
					<table width = "50%" border = "0">
						<tr>
							<td align="center" rowspan="2"><strong>Jadwal Kegiatan Ekstra Kurikuler</strong> <br /><br />
								<select name="bulan" id="bulan" onChange="show_kelas()" style="width:130px;" onKeyPress="return focusNext('tabel', event)" onfocus="panggil('bulan')">
								<option value="<?php echodate("m")?>">Bulan Sekarang</option>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
								<option value="all">Tampilkan Semua</option>
								</select>
							</td>
						</tr>
					</table>
				</td>   
				<td align="right" valign="top">
					<font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Jadwal Kegiatan Ekstra Kurikuler</font><br />
					<a href="../ekskul.php" target="content">
					<font size="1" color="#000000"><b>Main Ekstra Kurikuler</b></font></a>&nbsp>&nbsp	 	
					<font size="1" color="#000000"><b>Jadwal kegiatan Ekstra Kurikuler</b></font>
				</td>
			</tr>
		</table>
	</body>
</html>