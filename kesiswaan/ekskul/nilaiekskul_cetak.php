<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../include/getheader.php');
	
	$departemen="MAN Kota Blitar";
	$namaekskul	= $_REQUEST['namaekskul'];
	$urut = $_REQUEST['urut'];
	$urutan = $_REQUEST['urutan'];
	$varbaris = $_REQUEST['varbaris'];	
	$page = $_REQUEST['page'];
	$total = $_REQUEST['total'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Nilai Ektra Kurikuler]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>

<center>
  <font size="4"><strong>NILAI EKSTRA KURIKULER <?php echo$namaekskul?></strong></font><br />
 </center><br /><br />
</span>
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="100%" align="center" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="30%" class="header" align="center">Nama Siswa</td>
        <td width="15%" class="header" align="center">Nama Jabatan</td>
        <td width="15%" class="header" align="center">Nilai</td>
		<td width="*" class="header" align="center">Deskripsi</td>
    </tr>
	<?php OpenDb();
	$sql_cari = "SELECT id_ekskul FROM ekskul WHERE nama_ekskul='$namaekskul'";
	$result_cari = QueryDb($sql_cari);
	$row_cari = @mysql_fetch_row($result_cari);
	
	$sql = "SELECT id_pengurus, nis, id_jabatan, nilai, deskripsi FROM ekskul_pengurus WHERE id_ekskul='$row_cari[0]' ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";  

	$result = QueryDB($sql);
	//if ($page==0)
		$cnt = 1;
	//else
		//$cnt = (int)$page*(int)$varbaris+1;
		
	while ($row = mysql_fetch_row($result)) { 
		?>
		<?php
		OpenDb();
		$sql_nama = "SELECT nama FROM siswa WHERE nis=$row[1]";
		//echo "$sql_nama";
		$rs_nama = QueryDb($sql_nama);
		$row_nama = @mysql_fetch_row($rs_nama);
		
		$sql_jab = "SELECT nama_jabatan FROM ekskul_struktur WHERE id_jabatan='$row[2]' AND id_ekskul='$row_cari[0]'";
		//echo "<br> $sql_jab";
		$rs_jab = QueryDb($sql_jab);
		$row_jab = @mysql_fetch_row($rs_jab);
		CloseDb();
		?>
    <tr height="25">    	
    	<td align="center"><?php echo$cnt ?></td>
        <td height="25" align="center"><?php echo $row[1] ?> - <?php echo $row_nama[0] ?></td>
		<td height="25" align="center"><?php echo $row_jab[0] ?></td>
		<td height="25" align="center"><?php echo $row[3] ?></td>
		<td height="25" align="left"><?php echo $row[4] ?></td>     
    </tr>
	<?php	$cnt++;
	} 
	CloseDb() ?>
    </table>
<!-- END TABLE CENTER -->    
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>