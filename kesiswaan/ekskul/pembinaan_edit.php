<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	
	if (isset($_REQUEST['id_pembinaan']))
		$id_pembinaan = CQ($_REQUEST['id_pembinaan']);
	if (isset($_REQUEST['nama_ekskul']))
		$nama_ekskul = CQ($_REQUEST['nama_ekskul']);
	if (isset($_REQUEST['jam']))
		$jam = CQ($_REQUEST['jam']);
	if (isset($_REQUEST['menit']))
		$menit = CQ($_REQUEST['menit']);
	if (isset($_REQUEST['tanggal']))
		$tanggal = CQ($_REQUEST['tanggal']);
	if (isset($_REQUEST['tempat']))
		$tempat = CQ($_REQUEST['tempat']);
	if (isset($_REQUEST['nip']))
		$nip = $_REQUEST['nip'];
	if (isset($_REQUEST['namawali']))
		$namawali = $_REQUEST['namawali'];
	if (isset($_REQUEST['keterangan']))
		$keterangan = CQ($_REQUEST['keterangan']);

	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {		
		//$ERROR_MSG = "" .$nama_ekskul. "-" .$nipwali. "-". $namawali ."-". $keterangan;
		$sql = "UPDATE ekskul_pembinaan SET id_ekskul='$nama_ekskul', jam='$jam:$menit', tanggal='$tanggal', tempat='$tempat', nip='$nip', keterangan='$keterangan' WHERE id_pembinaan='$id_pembinaan'";
		//echo "$sql";
		$result = QueryDb($sql);
		if ($result) {
			?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
		<?php
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Edit Struktur Ektra Kurikuler]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../script/cal2.js"></script>
		<script language="javascript" src="../script/cal_conf2.js"></script>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function caripegawai() {
				newWindow('../library/pegawai.php?flag=0&bagian=Akademik', 'CariPegawai','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			}

			function acceptPegawai(nip, nama, flag) {
				document.getElementById('nipwali').value = nip;
				document.getElementById('nip').value = nip;
				document.getElementById('nama').value = nama;
				document.getElementById('namawali').value = nama;
				document.getElementById('keterangan').focus();
			}
			
			function validate() {
				return validateEmptyText('nama_ekskul', 'Nama Ekstra Kulikuler') &&
					   validateEmptyText('jam', 'Jam') && 
					   validateEmptyText('menit', 'Menit') &&
					   validateEmptyText('tanggal', 'Tanggal');
			}
			
			function tutup() {
				document.getElementById('kapasitas').focus();
			}

			function validate() {
				return validateEmptyText('kelas', 'Nama Kelas') && 
					validateEmptyText('nip', 'NIP dan Nama Wali Kelas') &&
					validateEmptyText('kapasitas', 'Kapasitas Kelas') &&
					validateNumber('kapasitas', 'Kapasitas Kelas') &&
					validateMaxText('keterangan', 255, 'Keterangan');
			}

			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nip')
						caripegawai();
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('nama_ekskul','jam','menit','tanggal','tempat','pembina','keterangan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}

		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nama_ekskul').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Edit Jadwal Pembinaan Ektra Kurikuler :.
					</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="urut" id="urut" value="<?php echo $urut ?>"/>
						<input type="hidden" name="urutan" id="urutan" value="<?php echo $urutan ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT * FROM ekskul_pembinaan WHERE id_pembinaan=$id_pembinaan";
								//echo"$sql";
								$rs = QueryDb($sql);
								$rows = mysql_fetch_row($rs);
							?>
							<tr>
								<td><strong>Id Pembinaan</strong></td>
								<td><input type="text" class="disabled" name="id_pembinaan" id="id_pembinaan" value="<?php echo $rows[0]?>" size="3" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nama Ekstra Kurikuler</strong></td>
								<td>
									<select name="nama_ekskul" id="nama_ekskul" style="width:130px;" onFocus="panggil('nama_ekskul')" onKeyPress="return focusNext('jam', event)" >
									<?php
									$sql_namaeks2 = "SELECT nama_ekskul FROM ekskul WHERE id_ekskul=$rows[0]";
									$result_namaeks2 = QueryDb($sql_namaeks2);
									$row_namaeks2 = @mysql_fetch_row($result_namaeks2);
									?>
									<option value="<?php echo$rows[0]?>"><?php echo$row_namaeks2[0]?></option>
									<?php
									$sql_eks2 = "SELECT id_ekskul,nama_ekskul FROM ekskul ORDER BY id_ekskul";
									$result_eks2 = QueryDb($sql_eks2);
									while ($row_eks2 = @mysql_fetch_row($result_eks2)){
										?>
											<option value="<?php echo$row_eks2[0]?>"><?php echo$row_eks2[1]?></option>
										<?php
									}
									?>
									</select>
								</td>
							</tr>
							<tr>
								<?php
								$pecah_jam = explode(":",$rows[2]);
								?>
								<td><strong>Jam</strong></td>
								<td>
									<input type="text" name="jam" id="jam" size="5" value="<?php echo $pecah_jam[0] ?>" onFocus="panggil('jam')" onKeyPress="return focusNext('menit', event)" /> :
									<input type="text" name="menit" id="menit" size="5" value="<?php echo $pecah_jam[1] ?>" onFocus="panggil('menit')" onKeyPress="return focusNext('tanggal', event)" />
								</td>
							</tr>
							<tr>
								<td><strong>Tanggal</strong></td>
								<td>
									<input type="text" name="tanggal" id="tanggal" class="disabled" size="15" value="<?php echo $rows[3]?>" onClick="showCal('Calendar2');" onFocus="panggil('tanggal')" onKeyPress="return focusNext('tempat', event)" readonly />
									<a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
								</td>
								
							</tr>
							<tr>
								<td><strong>Tempat</strong></td>
								<td><input type="text" name="tempat" id="tempat" size="30" value="<?php echo $rows[4]?>" onFocus="panggil('tempat')" onKeyPress="return focusNext('nip', event)"/></td>
							</tr>
							<tr>
								<td><strong>Pembina</strong></td>
								<td>
									<?php
									$sql_peg = "SELECT nama FROM $g_db_pegawai.pegawai WHERE nip=$rows[5]";
									$rs_peg = QueryDb($sql_peg);
									$rows_peg = @mysql_fetch_row($rs_peg);
									?>
									<input type="text" size="10" class="disabled" name="nip" id="nip" readonly value="<?php echo$rows[5]?>" onClick="caripegawai()"/>
									<input type="hidden" name="nipwali" id="nipwali" value="<?php echo$nipwali?>"/>
									<input type="text" name="nama" id="nama" class="disabled" size="25" readonly value="<?php echo$rows_peg[0]?>" onClick="caripegawai()"/>
									<input type="hidden" name="namawali" id="namawali" value="<?php echo$namawali?>" />&nbsp;
									<a href="JavaScript:caripegawai()"><img src="../images/ico/lihat.png" border="0" onMouseOver="showhint('Cari Pegawai!', this, event, '50px')"/></a></td>
							</tr>
							<tr>
								<td valign="top"><strong>Keterangan</strong></td>
								<td><textarea name="keterangan" id="keterangan" rows="3" cols="45"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('keterangan')"><?php echo $rows[6] ?></textarea>    </td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>