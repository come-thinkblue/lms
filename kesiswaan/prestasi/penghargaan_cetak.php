<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../include/getheader.php');
	
	$departemen="MAN Kota Blitar";	
	$urut = $_REQUEST['urut'];
	$urutan = $_REQUEST['urutan'];
	$varbaris = $_REQUEST['varbaris'];	
	$page = $_REQUEST['page'];
	$total = $_REQUEST['total'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Penghargaan Siswa]</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="center">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>

<center>
  <font size="4"><strong>DATA PENGHARGAAN SISWA</strong></font><br />
 </center><br /><br />
</span>
	<table class="tab" id="table" border="1" style="border-collapse:collapse" width="90%" align="left" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
		<td width="10%" class="header" align="center">Nama Siswa</td>
		<td width="10%" class="header" align="center">Nama Penghargaan</td>
		<td width="10%" class="header" align="center">Bentuk Penghargaan</td>
		<td width="10%" class="header" align="center">Keterangan Penghargaan</td>
		<td width="10%" class="header" align="center">Kategori</td>
		<td width="10%" class="header" align="center">Tahun Ajaran</td>
		<td width="10%" class="header" align="center">Semester</td>
		<td width="10%" class="header" align="center">Waktu</td>
		<td width="*" class="header" align="center">Keterangan</td>
    </tr>
	<?php OpenDb();
	$sql = "SELECT * FROM penghargaan ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";  
	$result = QueryDB($sql);
	//if ($page==0)
		$cnt = 1;
	//else
		//$cnt = (int)$page*(int)$varbaris+1;
	
	while ($row = mysql_fetch_row($result)) { 
		?>
    <tr height="25">    	
    	<td align="center"><?php echo$cnt ?></td>
		<?php
		$sql_nama = "SELECT nama FROM siswa WHERE nis=$row[1]";
		$rs_nama = QueryDb($sql_nama);
		$row_nama = mysql_fetch_row($rs_nama);
		
		$sql_kat = "SELECT nama_kategori FROM kategori_penghargaan WHERE id_kategori_penghargaan=$row[5]";
		$rs_kat = QueryDb($sql_kat);
		$row_kat = mysql_fetch_row($rs_kat);
		?>
        <td><?php echo$row_nama[0] ?></td>
        <td><?php echo$row[2] ?></td>
        <td><?php echo$row[3] ?></td>
        <td><?php echo$row[4] ?></td>
        <td><?php echo$row_kat[0] ?></td>
        <td><?php echo$row[6] ?></td>
        <td><?php echo$row[7] ?></td>
        <td><?php echo$row[8] ?></td>
        <td><?php echo$row[9] ?></td>
    </tr>
	<?php	$cnt++;
	} 
	CloseDb() ?>
    </table>
<!-- END TABLE CENTER -->    
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>