<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	if (isset($_REQUEST['id_penghargaan']))
		$id_penghargaan = CQ($_REQUEST['id_penghargaan']);
	if (isset($_REQUEST['nama_penghargaan']))
		$nama_penghargaan = CQ($_REQUEST['nama_penghargaan']);
	if (isset($_REQUEST['nis']))
		$nis = CQ($_REQUEST['nis']);
	$bentuk_penghargaan = "--Pilih Kategori--";
	if (isset($_REQUEST['bentuk_penghargaan']))
		$bentuk_penghargaan = CQ($_REQUEST['bentuk_penghargaan']);
	if (isset($_REQUEST['keterangan_penghargaan']))
		$keterangan_penghargaan = CQ($_REQUEST['keterangan_penghargaan']);
	if (isset($_REQUEST['kategori']))
		$kategori = CQ($_REQUEST['kategori']);
	if (isset($_REQUEST['tahun_ajaran']))
		$tahun_ajaran = CQ($_REQUEST['tahun_ajaran']);
	if (isset($_REQUEST['semester']))
		$semester = CQ($_REQUEST['semester']);
	if (isset($_REQUEST['tanggal']))
		$tanggal = CQ($_REQUEST['tanggal']);
	if (isset($_REQUEST['keterangan']))
		$keterangan = CQ($_REQUEST['keterangan']);

	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		//$ERROR_MSG = "" . $id_penghargaan ."-" .$nama_penghargaan. "-" .$nipwali. "-". $namawali ."-". $keterangan;
		$sql = "INSERT INTO penghargaan SET id_penghargaan='$id_penghargaan', nis='$nis', nama_penghargaan='$nama_penghargaan', 
				bentuk_penghargaan='$bentuk_penghargaan', keterangan_penghargaan='$keterangan_penghargaan', kategori='$kategori', 
				tahun_ajaran='$tahun_ajaran', semester='$semester', waktu='$tanggal', keterangan='$keterangan'";
		//echo "$sql";
		$result = QueryDb($sql);
		if ($result) {
			?>
			<script language="javascript">
				opener.refresh();
				window.close();
			</script> 
			<?php
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Tambah Penghargaan Siswa]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../script/cal2.js"></script>
		<script language="javascript" src="../script/cal_conf2.js"></script>
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function refresh(){
				document.location.href = "penghargaan_add.php?id_penghargaan="+id_penghargaan+"&nama_penghargaan="+nama_penghargaan+"&nis="+nis+"&nama="+nama+"&bentuk_penghargaan="+bentuk_penghargaan+"&keterangan_penghargaan="+keterangan_penghargaan;
			}
			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nis')
						carisiswa();
					return false;
				} 
				return true;
			}

			function panggil(elem){
				var lain = new Array('nis','nama_penghargaan','bentuk_penghargaan','keterangan_penghargaan','kategori','tahun_ajaran','semester','tanggal','keterangan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}
			
			function carisiswa() {
				//parent.footer.location.href = "blank_presensi_siswa.php?tipe='harian'";	
				newWindow('../library/siswa.php?flag=0', 'CariSiswa','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			}

			function acceptSiswa(nis, nama) {
				document.getElementById('nis').value = nis;
				document.getElementById('nis1').value = nis;
				document.getElementById('nama').value = nama;
				document.getElementById('nama_penghargaan').focus();
			}
			
			function cek_bentuk(){
				//alert('test');
				if(document.getElementById('bentuk_penghargaan').value == 'Sertifikat'){
					document.getElementById('bentuk_penghargaan').value = 'Sertifikat';
					
					var nama_penghargaan = document.getElementById('nama_penghargaan').value;
					var nis = document.getElementById('nis').value;
					var nama = document.getElementById('nama').value;
					
					document.location.href = "penghargaan_add.php?nama_penghargaan="+nama_penghargaan+"&nis="+nis+"&nama="+nama+"&bentuk_penghargaan=Sertifikat";
				}
				else if(document.getElementById('bentuk_penghargaan').value == 'Piagam'){
					document.getElementById('bentuk_penghargaan').value = 'Piagam';
					var nama_penghargaan = document.getElementById('nama_penghargaan').value;
					var nis = document.getElementById('nis').value;
					var nama = document.getElementById('nama').value;
					
					document.location.href = "penghargaan_add.php?nama_penghargaan="+nama_penghargaan+"&nis="+nis+"&nama="+nama+"&bentuk_penghargaan=Piagam";
				}
				else if(document.getElementById('bentuk_penghargaan').value == 'Thropy'){
					document.getElementById('bentuk_penghargaan').value = 'Thropy';
					var nama_penghargaan = document.getElementById('nama_penghargaan').value;
					var nis = document.getElementById('nis').value;
					var nama = document.getElementById('nama').value;
					
					document.location.href = "penghargaan_add.php?nama_penghargaan="+nama_penghargaan+"&nis="+nis+"&nama="+nama+"&bentuk_penghargaan=Thropy";
				}
			}
			
			function tambah_kategori(){			
				newWindow("kategori.php", 'TambahKategori','600','420','resizable=1,scrollbars=1,status=0,toolbar=0');
				//close.window();
			}
			
			function validate() {
				return validateEmptyText('nis', 'NIS dan Nama Siswa') && 
					validateEmptyText('nama_penghargaan', 'Nama Penghargaan') &&
					validateEmptyText('bentuk_penghargaan', 'Bentuk Penghargaan') &&
					validateEmptyText('kategori', 'Kategori') &&
					validateEmptyText('tahun_ajaran', 'Tahun Ajaran') &&
					validateEmptyText('semester', 'Semster') &&
					validateEmptyText('waktu', 'Waktu');
			}			
		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nis').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Tambah Penghargaan Siswa :.
					</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="urut" id="urut" value="<?php echo $urut ?>"/>
						<input type="hidden" name="urutan" id="urutan" value="<?php echo $urutan ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT max(id_penghargaan) FROM penghargaan";
								$rs = QueryDb($sql);
								$next_id = mysql_fetch_row($rs);
							?>
							<tr>
								<td><strong>Id Penghargaan</strong></td>
								<td><input type="text" class="disabled" name="id_penghargaan" id="id_penghargaan" value="<?php echo $next_id[0]+1?>" size="10" readonly /></td>
							</tr>
							<tr>
								<td><strong>NIS</strong></td>
								<td>
									<input name="nis" type="text" class="disabled" id="nis" value="<?php echo$_REQUEST['nis']?>" size="10" readonly onclick="carisiswa()"/>
									<input type="hidden" name="nis1" id="nis1" value="<?php echo$_REQUEST['nis']?>">
									<input name="nama" type="text" class="disabled" id="nama" value="<?php echo$_REQUEST['nama']?>" size="25" readonly onclick="carisiswa()"/>
									<input type="hidden" name="nama1" id="nama1" value="<?php echo$_REQUEST['nama']?>"> 
											
									<a href="JavaScript:carisiswa()"><img src="../images/ico/cari.png" border="0" /></a>
								</td>
							</tr>
							<tr>
								<td><strong>Nama Penghargaan</strong></td>
								<td><input type="text" name="nama_penghargaan" id="nama_penghargaan" size="35" onFocus="showhint('Nama Penghargaan tidak boleh kosong !', this, event, '120px');panggil('nama_penghargaan')" value="<?php echo $nama_penghargaan?>" onKeyPress="return focusNext('bentuk_penghargaan', event)"/></td>
							</tr>
							<tr>
								<td><strong>Bentuk Penghargaan</strong></td>
								<td>
									<select name="bentuk_penghargaan" id="bentuk_penghargaan" style="width:130px;" onchange="cek_bentuk()" onKeyPress="return focusNext('keterangan_penghargaan', event)" onfocus="panggil('bentuk_penghargaan')">
										<option value="<?php echo$bentuk_penghargaan?>"><?php echo$bentuk_penghargaan?></option>
										<option value="Sertifikat">Sertifikat</option>
										<option value="Piagam">Piagam</option>
										<option value="Thropy">Thropy</option>
									</select>
								</td>
							</tr>
							<?php
							if($bentuk_penghargaan == "Sertifikat"){
								?>
								<tr>
									<td><strong>Masukkan No. Sertifikat</strong></td>
									<td><input type="text" name="keterangan_penghargaan" id="keterangan_penghargaan" size="35" value="<?php echo $keterangan_penghargaan?>" onKeyPress="return focusNext('kategori', event)"/></td>
								</tr>
								<?php
							}
							else if($bentuk_penghargaan == "Piagam"){
								?>
								<tr>
									<td><strong>Masukkan No. Piagam</strong></td>
									<td><input type="text" name="keterangan_penghargaan" id="keterangan_penghargaan" size="35" value="<?php echo $keterangan_penghargaan?>" onKeyPress="return focusNext('kategori', event)"/></td>
								</tr>
								<?php
							}
							else if($bentuk_penghargaan == "Thropy"){
								?>
								<tr>
									<td><strong>Masukkan Nama Thropy</strong></td>
									<td><input type="text" name="keterangan_penghargaan" id="keterangan_penghargaan" size="35" value="<?php echo $keterangan_penghargaan?>" onKeyPress="return focusNext('kategori', event)"/></td>
								</tr>
								<?php
							}
							?>
							<tr>
								<?php
								$sql_kat = "SELECT id_kategori_penghargaan, nama_kategori FROM kategori_penghargaan ORDER BY id_kategori_penghargaan";
								$rs_kat = QueryDb($sql_kat);
								?>
								<td><strong>Kategori</strong></td>
								<td>
									<select name="kategori" id="kategori" style="width:130px;" onKeyPress="return focusNext('tahun_ajaran', event)" onfocus="panggil('kategori')">
										<option value="">--Pilih Kategori--</option>
										<?php
										while($row_kat = mysql_fetch_row($rs_kat)){
										?><option value="<?php echo$row_kat[0]?>"><?php echo$row_kat[1]?></option><?
										}
										?>
									</select>
									<a href="#" onClick="JavaScript:tambah_kategori()">
										<img src="../images/ico/tambah.png" border="0" onMouseOver="showhint('Tambah Kategori!', this, event, '50px')" />
									</a>
								</td>
							</tr>
							<tr>
								<?php
								$sql_peraktif = "SELECT tahunajaran FROM tahunajaran WHERE aktif=1 ORDER BY tahunajaran";
								$rs_peraktif = QueryDb($sql_peraktif);
								$row_peraktif = mysql_fetch_row($rs_peraktif);
								
								$sql_per = "SELECT tahunajaran FROM tahunajaran ORDER BY tahunajaran";
								$rs_per = QueryDb($sql_per);
								?>
								<td><strong>Tahun Ajaran</strong></td>
								<td>
									<select name="tahun_ajaran" id="tahun_ajaran" style="width:130px;" onKeyPress="return focusNext('semester', event)" onfocus="panggil('tahun_ajaran')">
										<option value="<?php echo$row_peraktif[0]?>"><?php echo$row_peraktif[0]?></option>
										<?php
										while($row_per = mysql_fetch_row($rs_per)){
										?><option value="<?php echo$row_per[0]?>"><?php echo$row_per[0]?></option><?
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<?php
								$sql_semaktif = "SELECT semester FROM semester WHERE aktif=1 ORDER BY replid";
								$rs_semaktif = QueryDb($sql_semaktif);
								$row_semaktif = mysql_fetch_row($rs_semaktif);
								
								$sql_sem = "SELECT semester FROM semester ORDER BY replid";
								$rs_sem = QueryDb($sql_sem);
								?>
								<td><strong>Semester</strong></td>
								<td>
									<select name="semester" id="semester" style="width:130px;" onKeyPress="return focusNext('tanggal', event)" onfocus="panggil('semester')">
										<option value="<?php echo$row_semaktif[0]?>"><?php echo$row_semaktif[0]?></option>
										<?php
										while($row_sem = mysql_fetch_row($rs_sem)){
										?><option value="<?php echo$row_sem[0]?>"><?php echo$row_sem[0]?></option><?
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td><strong>Waktu</strong></td>
								<td>
									<input type="text" name="tanggal" id="tanggal" class="disabled" readonly size="15" onClick="showCal('Calendar2');" onFocus="panggil('tanggal')" value="<?php echo $waktu?>" onKeyPress="return focusNext('keterangan', event)"/>
									<a href="javascript:showCal('Calendar2');"><img src="../images/calendar.jpg" border="0" onMouseOver="showhint('Buka kalender!', this, event, '100px')"></a>
								</td>
							</tr>
							<tr>
								<td valign="top"><strong>Keterangan</strong></td>
								<td><textarea name="keterangan" id="keterangan" rows="3" cols="40"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('keterangan')"><?php echo $keterangan ?></textarea>    </td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>