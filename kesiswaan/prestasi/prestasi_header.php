<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../library/departemen.php');
	require_once('../cek.php');

	OpenDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Prestasi Siswa</title>
		<script src="../script/SpryValidationSelect.js" type="text/javascript"></script>
		<link href="../script/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css"/>
		<script language="javascript" src="../script/tooltips.js"></script>
		<script language="javascript">
			function show_prestasi() {
				parent.footer.location.href = "prestasi_content.php";
			}

		</script>
	</head>
	<body>
		<table border="0" width="100%" cellpadding="0" cellspacing="0" >
			<!-- TABLE TITLE -->
			<tr>
				<td rowspan="2" width="42%"></td>    
				<td width="50%" colspan="2" align="right" valign="top">
					<font size="4" face="Verdana, Arial, Helvetica, sans-serif" style="background-color:#ffcc66">&nbsp;</font>
					&nbsp;<font size="4" face="Verdana, Arial, Helvetica, sans-serif" color="Gray">Prestasi Siswa</font>
					<br />
					<a href="../ekskul.php" target="content"> <font size="1" color="#000000"><b>Prestasi Siswa</b></font></a>&nbsp>&nbsp
					<font size="1" color="#000000"><b>Prestasi Siswa</b></font>
					<script language="javascript">show_prestasi()</script>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php
CloseDb();
?>