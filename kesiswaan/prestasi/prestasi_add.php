<?php /* * [N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]* */ ?>
<?php
	require_once('../include/errorhandler.php');
	require_once('../include/sessioninfo.php');
	require_once('../include/common.php');
	require_once('../include/theme.php');
	require_once('../include/config.php');
	require_once('../include/db_functions.php');
	require_once('../cek.php');

	OpenDb();
	
	
	if (isset($_REQUEST['nipwali']))
		$nipwali = $_REQUEST['nipwali'];
	if (isset($_REQUEST['namawali']))
		$namawali = $_REQUEST['namawali'];
	if (isset($_REQUEST['keterangan']))
		$keterangan = CQ($_REQUEST['keterangan']);
		
	if (isset($_REQUEST['id_prestasi']))
		$id_prestasi = CQ($_REQUEST['id_prestasi']);
	if (isset($_REQUEST['nama_prestasi']))
		$nama_prestasi = CQ($_REQUEST['nama_prestasi']);
	$kategori = "--Pilih Kategori--";
	if (isset($_REQUEST['kategori']))
		$kategori = CQ($_REQUEST['kategori']);
	$jumlah_anggota_team = 0;
	if (isset($_REQUEST['jumlah_anggota_team']))
		$jumlah_anggota_team = CQ($_REQUEST['jumlah_anggota_team']);
	
	$ERROR_MSG = "";
	if (isset($_REQUEST['Simpan'])) {
		$sql_cek = "SELECT * FROM ekskul WHERE nama_ekskul = '$_REQUEST[nama_ekskul]' ";
		$result_cek = QueryDb($sql_cek);

		if (@mysql_num_rows($result_cek) > 0) {
			CloseDb();
			$ERROR_MSG = "Ektra Kurikuler " . $nama_ekskul . " sudah digunakan!";
		} 
		else {
			//$ERROR_MSG = "" . $id_ekskul ."-" .$nama_ekskul. "-" .$nipwali. "-". $namawali ."-". $keterangan;
			$sql = "INSERT INTO ekskul SET id_ekskul='$id_ekskul', nama_ekskul='$nama_ekskul', pembina=$nipwali, keterangan='$keterangan'";
			$result = QueryDb($sql);
			if ($result) {
				?>
				<script language="javascript">
					opener.refresh();
					window.close();
				</script> 
			<?php
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="../style/style.css">
		<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LMS MAN Kota Blitar[Tambah Prestasi Siswa]</title>
		<script src="../script/SpryValidationTextField.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
		<script src="../script/SpryValidationTextarea.js" type="text/javascript"></script>
		<link href="../script/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../script/tooltips.js"></script>
		<script language="javascript" src="../script/tables.js"></script>
		<script language="javascript" src="../script/tools.js"></script>
		<script language="javascript" src="../script/validasi.js"></script>
		<script language="javascript">
			function validate() {
				return validateEmptyText('nama_ekskul', 'Nama Ekstra Kurikuler') && 
					validateEmptyText('nip', 'NIP dan Nama Pembina');
			}

			function cek_isi_cbx(){
				if(document.getElementById('kategori').value == 'Individu'){
					document.getElementById('kategori').value = 'Individu';
					var nama_prestasi = document.getElementById('nama_prestasi').value;
					document.location.href = "prestasi_add.php?nama_prestasi="+nama_prestasi+"&kategori=Individu";
				}
				else if(document.getElementById('kategori').value == 'Team'){
					document.getElementById('kategori').value = 'Team';
					var nama_prestasi = document.getElementById('nama_prestasi').value;
					document.location.href = "prestasi_add.php?nama_prestasi="+nama_prestasi+"&kategori=Team";
				}
			}
			function generate_jumlah(){
				var nama_prestasi = document.getElementById('nama_prestasi').value;
				var kategori = document.getElementById('kategori').value;
				var jumlah_anggota_team = document.getElementById('jumlah_anggota_team').value;
				
				document.location.href = "prestasi_add.php?nama_prestasi="+nama_prestasi+"&kategori="+kategori+"&jumlah_anggota_team="+jumlah_anggota_team;
			}
			
			// function carisiswa() {
				// newWindow('../library/siswa.php?flag=0', 'CariSiswa','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			// }
			
			// function acceptSiswa(nis, nama) {
				// document.getElementById('nis'+i).value = nis;
				// document.getElementById('nama'+i).value = nama;
				// document.getElementById('tingkat').focus();
			// }
			
			// function carisiswa1() {
				// newWindow('../library/siswa.php?flag=0', 'CariSiswa','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			// }
			<?php
			for($i=0;$i<$jumlah_anggota_team;$i++){
			echo"
			function carisiswa$i() {
				newWindow('../library/siswa.php?flag=0', 'CariSiswa','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			}
			
			function acceptSiswa$i(nis, nama) {
				document.getElementById('nis$i').value = nis;
				document.getElementById('nama$i').value = nama;
				document.getElementById('tingkat').focus();
			}
			";
			}
			?> 
			// function acceptSiswa1(nis, nama) {
				// document.getElementById('nis1').value = nis;
				// document.getElementById('nama1').value = nama;
				// document.getElementById('tingkat').focus();
			// }
			
			// function carisiswa2() {
				// newWindow('../library/siswa.php?flag=0', 'CariSiswa','600','618','resizable=1,scrollbars=1,status=0,toolbar=0');
			// }
			
			// function acceptSiswa2(nis, nama) {
				// document.getElementById('nis2').value = nis;
				// document.getElementById('nama2').value = nama;
				// document.getElementById('tingkat').focus();
			// }
			
			function panggil(elem){
				var lain = new Array('nama_prestasi','kategori','nis','tingkat','waktu','peringkat','keterangan');
				for (i=0;i<lain.length;i++) {
					if (lain[i] == elem) {
						document.getElementById(elem).style.background='#4cff15';
					} else {
						document.getElementById(lain[i]).style.background='#FFFFFF';
					}
				}
			}
			
			function focusNext(elemName, evt) {
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode :
					((evt.which) ? evt.which : evt.keyCode);
				if (charCode == 13) {
					document.getElementById(elemName).focus();
					if (elemName == 'nis')
						caripegawai();
					return false;
				} 
				return true;
			}
		</script>
	</head>
	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"  style="background-color:#dcdfc4" onLoad="document.getElementById('nama_ekskul').focus()">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="58">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_01.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_02a.jpg">
					<div align="center" style="color:#FFFFFF; font-size:16px; font-weight:bold">
						.: Tambah Prestasi Siswa :.
					</div>
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_03.jpg">&nbsp;</td>
			</tr>
			<tr height="300">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_04a.jpg">&nbsp;</td>
				<td width="0" style="background-color:#FFFFFF">
					<!-- CONTENT GOES HERE //--->
					<form name="main" onSubmit="return validate()">
						<input type="hidden" name="urut" id="urut" value="<?php echo $urut ?>"/>
						<input type="hidden" name="urutan" id="urutan" value="<?php echo $urutan ?>"/>
						<table border="0" width="95%" cellpadding="2" cellspacing="2" align="center">
							<!-- TABLE CONTENT -->
							<?php
								$sql = "SELECT max(id_prestasi) FROM prestasi";
								$rs = QueryDb($sql);
								$next_id = mysql_fetch_row($rs);
							?>
							<tr>
								<td><strong>Id Prestasi</strong></td>
								<td><input type="text" class="disabled" name="id_prestasi" id="id_prestasi" value="<?php echo $next_id[0]+1?>" size="10" readonly /></td>
							</tr>
							<tr>
								<td><strong>Nama Prestasi</strong></td>
								<td><input type="text" name="nama_prestasi" id="nama_prestasi" size="35" onFocus="showhint('Nama Prestasi tidak boleh kosong !', this, event, '120px');panggil('nama_prestasi')" value="<?php echo $nama_prestasi?>" onKeyPress="return focusNext('kategori', event)"/></td>
							</tr>
							<tr>
								<td><strong>Kategori</strong></td>
								<td>
									<select name="kategori" id="kategori" style="width:130px;" onChange="cek_isi_cbx()" onKeyPress="return focusNext('jumlah_anggota_team', event)" onfocus="panggil('kategori')">
										<option value="<?php echo$kategori?>"><?php echo$kategori?></option>
										<option value="Individu">Individu</option>
										<option value="Team">Team</option>
									</select>
								</td>
							</tr>
							<?php
							if($kategori == "Team"){
							?>
							<tr>
								<td><strong>Jumlah Anggota</strong></td>
								<td>
									<input type='text' name='jumlah_anggota_team' id='jumlah_anggota_team' size='3' onFocus="showhint('jumlah anggota team tidak boleh kosong !', this, event, '120px');panggil('jumlah_anggota_team')" value="<?php echo$jumlah_anggota_team?>" onKeyPress="return focusNext('kategori', event)"/>
									<input type='button' name='submit_jumlah' id='submit_jumlah' class="but" value='Generate' onclick="generate_jumlah()" />
								</td>
							</tr>
							<?php
							}
							if($kategori == "Team" && $jumlah_anggota_team != 0){
								for($i=1;$i<=$jumlah_anggota_team;$i++){
									?>
									<tr>
										<td><strong>Siswa <?php echo$i?></strong></td>
										<td>
											nis<?php echo$i?> : nama<?php echo$i?> : carisiswa<?php echo$i?>
											<input name="nis<?php echo$i?>" type="text" class="disabled" id="nis<?php echo$i?>" size="10" readonly onclick="carisiswa<?php echo$i?>()"/>
											<input name="nama<?php echo$i?>" type="text" class="disabled" id="nama<?php echo$i?>" size="25" readonly onclick="carisiswa<?php echo$i?>()"/>
											
											<a href="JavaScript:carisiswa<?php echo$i?>()"><img src="../images/ico/cari.png" border="0" /></a>
										</td>
									</tr>
									<?php
								}
							}
							else{
								?>
								<tr>
									<td><strong>Siswa</strong></td>
									<td>
										<input name="nis" type="text" class="disabled" id="nis" value="<?php echo$_REQUEST['nis']?>" size="10" readonly onclick="carisiswa()"/>
										<input name="nama" type="text" class="disabled" id="nama" value="<?php echo$_REQUEST['nama']?>" size="25" readonly onclick="carisiswa()"/>
											
										<a href="JavaScript:carisiswa()"><img src="../images/ico/cari.png" border="0" /></a>
									</td>
								</tr>
								<?php
							}
							?>
							<tr>
								<td><strong>Tingkat</strong></td>
								<td>
								<select name="tingkat" id="tingkat" style="width:130px;" onKeyPress="return focusNext('waktu', event)" onfocus="panggil('tingkat')">
									<option value="">--Pilih Tingkat--</option>
								</select>
							</tr>
							<tr>
								<td valign="top"><strong>Keterangan</strong></td>
								<td><textarea name="keterangan" id="keterangan" rows="3" cols="45"  onKeyPress="return focusNext('Simpan', event)" onFocus="panggil('keterangan')"><?php echo $keterangan ?></textarea>    </td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" name="Simpan" id="Simpan" value="Simpan" class="but" onFocus="panggil('Simpan')"/>&nbsp;
									<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />    </td>
							</tr>
							<!-- END OF TABLE CONTENT -->
						</table>
					</form>

					<!-- END OF CONTENT //--->
				</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_06a.jpg">&nbsp;</td>
			</tr>
			<tr height="28">
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_07.jpg">&nbsp;</td>
				<td width="*" background="../<?php echo GetThemeDir() ?>bgpop_08a.jpg">&nbsp;</td>
				<td width="28" background="../<?php echo GetThemeDir() ?>bgpop_09.jpg">&nbsp;</td>
			</tr>
		</table>

		<!-- Tamplikan error jika ada -->
		<?php 	if (strlen($ERROR_MSG) > 0) { ?>
				<script language="javascript">alert('<?php echo $ERROR_MSG ?>');</script>
		<?php } ?>
	</body>
</html>