<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../cek.php');

OpenDb();
$idangkatan=(int)$_REQUEST['idangkatan'];
$departemen=$_REQUEST['departemen'];
$dasar = $_REQUEST['dasar'];
$tabel = $_REQUEST['tabel'];
$judul = $_REQUEST['judul'];
$iddasar = $_REQUEST['iddasar'];
$keyword = $_REQUEST['keyword'];

$urut = "s.nama";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	

$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];


if ($iddasar==""){
	if ($dasar == 'Golongan Darah') {
		if ($judul == 'Tidak ada data'){ 
			$filter = "s.$tabel = '' AND s.aktif = 1";
		} else {
			$filter = "s.$tabel = '$judul' AND s.aktif = 1";
		}
	} elseif ($dasar == 'Jenis Kelamin' ) {	
		if ($judul == 'Laki-laki') 
			$filter = "s.$tabel = 'l' AND s.aktif = 1";		
		else 
			$filter = "s.$tabel = 'p' AND s.aktif = 1";		
	/*} elseif ($dasar == 'Penghasilan Orang Tua' ) {	
		$judul = array(1 => '< Rp 1000000','Rp 1000000 s/d Rp 2500000','Rp 2500000 s/d Rp 5000000','> Rp 5000000');		
		if ($judul == '< Rp 1000000') 
			$filter = "s.$tabel < 1000000 AND s.aktif = 1";		
		elseif ($judul == 'Rp 1000000 s/d Rp 2500000') 
			$filter = "1000000 < s.$tabel < 2500000 AND s.aktif = 1";		
		elseif ($judul == 'Rp 2500000 s/d Rp 5000000') 
			$filter = "2500000 < s.$tabel < 5000000 AND s.aktif = 1";
		else 
			$filter = "s.$tabel > 5000000 AND s.aktif = 1";	*/			
	} elseif ($dasar == 'Status Aktif') {
		if ($judul == 'Aktif') 
			$filter = "s.$tabel = 1 ";
		else 
			$filter = "s.$tabel = 0 ";			
	} elseif ($dasar == 'Tahun Kelahiran') {
		$filter = "YEAR(tgllahir) = $judul AND s.aktif = 1 ";
	} elseif ($dasar == 'Usia') {
		$filter =  "YEAR(now()) - YEAR(tgllahir) = $judul AND s.aktif = 1"; 	
	} elseif ($judul == NULL) {
		$filter = "s.$tabel is NULL AND s.aktif = 1";
		$judul = "Tidak ada data";
	} else {
		$filter = "s.$tabel = '$judul' AND s.aktif = 1";
	}
	
	if ($departemen=="-1" && $idangkatan<0) {	
		
		$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE a.replid = s.idangkatan AND s.idkelas = k.replid AND k.idtingkat = t.replid AND $filter ORDER BY $urut $urutan";
	} if ($departemen<>"-1" && $idangkatan<0) {	
		$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE a.replid = s.idangkatan AND s.idkelas = k.replid AND a.departemen = '$departemen' AND k.idtingkat = t.replid AND $filter ORDER BY $urut $urutan";	
	} if ($departemen<>"-1" && $idangkatan>0) {	
		$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE a.replid = s.idangkatan AND s.idkelas = k.replid AND a.departemen = '$departemen' AND s.idangkatan = '$idangkatan' AND k.idtingkat = t.replid AND $filter ORDER BY $urut $urutan";	
	}
	//echo 'sql '.$query1;	
	$result1 = QueryDb($query1);
	$num = @mysql_num_rows($result1);
	
} elseif ($iddasar=="12"){

	if ($departemen=="-1" && $idangkatan<0)
		$kondisi=" AND a.replid=s.idangkatan AND s.idkelas = k.replid AND k.idtingkat = t.replid";
	if ($departemen<>"-1" && $idangkatan<0)
		$kondisi=" AND a.departemen='$departemen' AND a.replid=s.idangkatan AND s.idkelas = k.replid AND k.idtingkat = t.replid";
	if ($departemen<>"-1" && $idangkatan>0)
		$kondisi=" AND s.idangkatan=$idangkatan AND a.replid=s.idangkatan AND a.departemen='$departemen' AND s.idkelas = k.replid AND k.idtingkat = t.replid ";
	
	if ($keyword=="1")
	$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE s.aktif = '1' AND s.penghasilanayah+s.penghasilanibu<1000000 $kondisi ORDER BY $urut $urutan";
	if ($keyword=="2")
	$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE s.aktif = '1' AND k.idtingkat = t.replid AND s.penghasilanayah+s.penghasilanibu>=1000000 AND s.penghasilanayah+s.penghasilanibu<2500000 $kondisi ORDER BY $urut $urutan";
	if ($keyword=="3")
	$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE s.aktif = '1' AND s.penghasilanayah+s.penghasilanibu>=2500000 AND s.penghasilanayah+s.penghasilanibu<5000000 $kondisi ORDER BY $urut $urutan";
	if ($keyword=="4")
	$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE s.aktif = '1' AND s.penghasilanayah+s.penghasilanibu>=5000000 $kondisi ORDER BY $urut $urutan";
	if ($keyword=="5")
	$query1 = "SELECT s.nis,s.nama,s.foto,k.kelas,s.replid,a.departemen,t.tingkat FROM siswa s, angkatan a, kelas k, tingkat t WHERE s.aktif = '1' AND s.penghasilanayah+s.penghasilanibu = 0 $kondisi ORDER BY $urut $urutan";
	$result1 = QueryDb($query1);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<link rel="stylesheet" type="text/css" href="../style/tooltips.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tampil Statistik</title>
<script language="javascript" src="../script/tables.js"></script>
<script language="javascript" src="../script/tools.js"></script>
<script language="JavaScript" src="../script/tooltips.js"></script>
<script language="javascript" src="../script/rupiah.js"></script>
<script language="javascript">
function change_urut(urut,urutan) {

	if (urutan =="ASC"){
		urutan="DESC"
	} else {
		urutan="ASC"
	}
	
	document.location.href = "statistik_tampil.php?departemen=<?php echo $departemen?>&idangkatan=<?php echo $idangkatan?>&dasar=<?php echo $dasar?>&tabel=<?php echo $tabel?>&judul=<?php echo $judul?>&iddasar=<?php echo $iddasar?>&keyword=<?php echo $keyword?>&urut="+urut+"&urutan="+urutan;
}
</script>
</head>
<body topmargin="0" leftmargin="0">
<div align="right">
<?php	$str = array("'","+");
	$str_replace = array("\'","x123x");	
  	$sql = str_replace($str, $str_replace, $query1);
	//echo $sql;
?>
<!--<a href="#" onclick="newWindow('statistik_cetak_excel.php?idangkatan=<?php echo $idangkatan?>&departemen=<?php echo $departemen?>&dasar=<?php echo $dasar?>&tabel=<?php echo $tabel?>&judul=<?php echo $judul?>&iddasar=<?php echo $iddasar?>&keyword=<?php echo $keyword?>','CetakExcel',100,100,'');">-->
<a href="#" onclick="newWindow('statistik_cetak_excel.php?sql=<?php echo $sql?>&departemen=<?php echo $departemen?>&dasar=<?php echo $dasar?>&judul=<?php echo $judul?>&idangkatan=<?php echo $idangkatan?>','CetakExcel',100,100,'');"><img src="../images/ico/excel.png" border="0" onMouseOver="showhint('Cetak dalam format Excel!', this, event, '80px')"/>&nbsp;Cetak Excel</a></div>
<br />
<table width="100%" border="1" class="tab" id="table" align="center" bordercolor="#000000">
	<tr height="30" class="header" align="center">
  		<td width="5%" >No</td>
     	<td width="20%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('s.nis','<?php echo $urutan?>')">N I S <?php echo change_urut('s.nis',$urut,$urutan)?></td>
    	<td width="*" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('s.nama','<?php echo $urutan?>')">Nama <?php echo change_urut('s.nama',$urut,$urutan)?></td>
    	<td width="20%" onMouseOver="background='../style/formbg2agreen.gif';height=30;" onMouseOut="background='../style/formbg2.gif';height=30;" background="../style/formbg2.gif" style="cursor:pointer;" onClick="change_urut('k.kelas','<?php echo $urutan?>')">Kelas <?php echo change_urut('k.kelas',$urut,$urutan)?></td>
        <td width="8%">&nbsp;</td>
	</tr> 
	<?php  if (@mysql_num_rows($result1)<1) { ?> 
					<td colspan="5" align="center"><strong>Tidak Ada Data</strong></td>
					 
  	<?php } else{
	
    while ($row1 = @mysql_fetch_row($result1)) { ?>
	<tr height="25">
  		<td align="center"><?php echo ++$cnt?></td>
     	<td align="center"><?php echo $row1[0] ?></td>
    	<td><?php echo $row1[1] ?></td>
    	<td align="center"><?php echo $row1[5]?><br /><?php echo $row1[6]." - ".$row1[3] ?></td>
        <td ><div align="center"><a href="#" onclick="newWindow('../library/detail_siswa.php?replid=<?php echo $row1[4]?>','DetailSiswa',790,610,'resizable=1,scrollbars=1,status=0,toolbar=0')"><img src="../images/ico/lihat.png" width="16" height="16" border="0" onMouseOver="showhint('Detail Data Siswa!', this, event, '80px')"/></a></div></td>
   </tr>
		<?php }
    }
		CloseDb();
  	?>
	</table>
	
	<script language='JavaScript'>
	    	Tables('table', 1, 0);
	</script>
	
</body>
</html>