<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php 
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../library/departemen.php');

$departemen = $_REQUEST['departemen'];
OpenDb();
?>
<select name="departemen" id="departemen" onchange="change_dep()" style="width:280px">
<?php 	$dep = getDepartemen(SI_USER_ACCESS());    
 	foreach($dep as $value) {
		if ($departemen == "")
			$departemen = $value; ?>
  	<option value="<?php echo$value ?>" <?php echo StringIsSelected($value, $departemen) ?> >
  <?php echo$value ?>
  </option>
  <?php	} ?>
</select>