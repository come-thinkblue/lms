<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
/**/
header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Data_Siswa.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$cari=$_REQUEST['cari'];
$jenis=$_REQUEST['jenis'];
$departemen=$_REQUEST['departemen'];

$varbaris=30;
if (isset($_REQUEST['varbaris']))
	$varbaris = $_REQUEST['varbaris'];
	
$page=0;
if (isset($_REQUEST['page']))
	$page = $_REQUEST['page'];

$hal=0;
if (isset($_REQUEST['hal']))
	$hal = $_REQUEST['hal'];

$urut = "nama";	
if (isset($_REQUEST['urut']))
	$urut = $_REQUEST['urut'];	

$urutan = "ASC";	
if (isset($_REQUEST['urutan']))
	$urutan = $_REQUEST['urutan'];

if ($jenis=="tgllulus")
$namajenis="Tahun&nbsp;Lulus";
if ($jenis=="nis")
$namajenis="NIS";
if ($jenis=="nama")
$namajenis="Nama&nbsp;Siswa";
if ($jenis=="panggilan")
$namajenis="Panggilan&nbsp;Siswa";
if ($jenis=="agama")
$namajenis="Agama";
if ($jenis=="suku")
$namajenis="Suku";
if ($jenis=="status")
$namajenis="Status&nbsp;Siswa";
if ($jenis=="kondisi")
$namajenis="Kondisi&nbsp;Siswa";
if ($jenis=="darah")
$namajenis="Golongan&nbsp;Darah";
if ($jenis=="alamatsiswa")
$namajenis="Alamat&nbsp;Siswa";
if ($jenis=="asalsekolah")
$namajenis="Asal&nbsp;Sekolah";
if ($jenis=="namaayah")
$namajenis="Nama&nbsp;Ayah";
if ($jenis=="namaibu")
$namajenis="Nama&nbsp;Ibu";
if ($jenis=="alamatortu")
$namajenis="Alamat&nbsp;Orang&nbsp;Tua";
if ($jenis=="keterangan")
$namajenis="Keterangan";

if ($cari=="")
$namacari="";
else
$namacari=$cari;


OpenDb();
	if ($jenis!="kondisi" && $jenis!="status" && $jenis!="agama" && $jenis!="suku" && $jenis!="darah" && $jenis!="idangkatan" && $jenis!="tgllulus") {
		$sql = "SELECT s.replid, s.nis, s.nama, s.idkelas, k.kelas, s.tmplahir, s.tgllahir, s.statusmutasi, s.aktif, s.alumni, t.tingkat, a.tgllulus from dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t, dbakademik.alumni a WHERE s.$jenis LIKE '%$cari%' AND k.replid=a.klsakhir AND k.idtingkat=t.replid AND a.departemen='$departemen' AND s.nis = a.nis ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
	} elseif ($jenis == "tgllulus") {
		$sql = "SELECT s.replid, s.nis, s.nama, s.idkelas, k.kelas, s.tmplahir, s.tgllahir, s.statusmutasi, s.aktif, s.alumni, t.tingkat, a.tgllulus from dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t, dbakademik.alumni a WHERE YEAR(a.tgllulus) = '$cari' AND k.replid=a.klsakhir AND k.idtingkat=t.replid AND a.departemen='$departemen' AND s.nis = a.nis ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
	} else { 
		$sql = "SELECT s.replid, s.nis, s.nama, s.idkelas, k.kelas, s.tmplahir, s.tgllahir, s.statusmutasi, s.aktif, s.alumni, t.tingkat, a.tgllulus from dbakademik.siswa s, dbakademik.kelas k, dbakademik.tingkat t, dbakademik.alumni a WHERE s.$jenis = '$cari' AND k.replid=a.klsakhir AND k.idtingkat=t.replid AND a.departemen='$departemen' AND s.nis = a.nis ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris"; 
	}
$result=QueryDb($sql);

if (@mysql_num_rows($result)<>0){
?>
<html>
<head>
<title>
Data Alumni
</title>
<style type="text/css">
<!--
-->
</style>
<style type="text/css">
<!--
.style2 {
	font-size: 16px;
	font-weight: bold;
}
.style3 {font-weight: bold}
.style4 {color: #000000}
-->
</style>
</head>
<body>
<table width="100%" border="0">
  <tr>
    <td colspan="7">
    <table width="100%" border="0">
  <tr>
    <td colspan="2"><div align="center" class="style3 style2">PENCARIAN ALUMNI</div></td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>

    <td width="9%"><strong>Departemen</strong></td>
    <td width="91%" align="left"><strong>:&nbsp;
        <?php echo $departemen?>
    </strong></td>
  </tr>
  <tr>
    <td colspan="2">
    Pencarian berdasarkan <strong><?php echo $namajenis?></strong> dengan kata kunci <strong>'<?php echo $namacari;?>'</strong></td>
    <td align="left"><strong>:&nbsp;
           
	</strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>    </td>
  </tr>
  <tr>
    <td colspan="7"><table border="1" width="100%" bordercolordark="#000000">
<tr height="30" bordercolor="#000000">
<td width="3" valign="middle" bgcolor="#999999" class="header"><div align="center" class="style4">No.</div></td>
<td width="20" valign="middle" bgcolor="#999999" class="header"><div align="center" class="style4">NIS</div></td>
<td valign="middle" bgcolor="#999999" class="header"><div align="center" class="style4">Nama</div></td>
<td valign="middle" bgcolor="#999999" class="header"><div align="center" class="style4">Kelas Terakhir</div></td>
<td valign="middle" bgcolor="#999999" class="header"><div align="center" class="style4">Tingkat Terakhir</div></td>
<td valign="middle" bgcolor="#999999" class="header"><div align="center" class="style4">Tanggal Lulus</div></td>
</tr>
<?php
	$cnt=1;
	while ($row=@mysql_fetch_array($result)){
	?>
	<tr height="25" bordercolor="#000000">
	<td width="3" align="center"><?php echo $cnt?></td>
	<td align="left"><?php echo $row[nis]?></td>
	<td align="left"><?php echo $row[nama]?></td>
	<td align="left"><?php echo $row[kelas]?></td>
	<td align="left"><?php echo $row[tingkat]?></td>
	<td align="left"><?php echo $row[tgllulus]?></td>
	</tr>
	<?php
		$cnt++;
}
	?>
</table></td>
  </tr>
</table>


</body>
</html>
<?php
}
CloseDb();
?>