<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');
require_once('../include/getheader.php');

//echo $_REQUEST['departemen'];
$departemen = $_REQUEST['departemen'];
$proses = $_REQUEST['proses'];
$replid = $_REQUEST['replid'];

OpenDb();
$sql = "SELECT proses, kelompok FROM prosespenerimaansiswa p, kelompokcalonsiswa k WHERE p.replid = '$proses' AND k.replid = '$replid' AND k.idproses = p.replid";
$result=QueryDb($sql);
$row = @mysql_fetch_array($result);

CloseDb();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Daftar Calon Siswa</title>
</head>

<body>
<table border="0" cellpadding="10" cellpadding="5" width="780" align="left">
<tr><td align="left" valign="top">

<?php echo GetHeader($departemen)?>
<?php OpenDb(); ?>
<center>
  <font size="4"><strong>DAFTAR CALON SISWA</strong></font><br />
 </center><br /><br />
<table>
<tr>
	<td><strong>Departemen</strong> </td> 
	<td><strong>:&nbsp;<?php echo$departemen?></strong></td>
</tr>
<tr>
	<td><strong>Penerimaan</strong></td>
	<td><strong>:&nbsp;<?php echo$row['proses']?></strong></td>
</tr>
<tr>
	<td><strong>Kelompok</strong></td>
	<td><strong>:&nbsp;<?php echo$row['kelompok']?></strong></td>
</tr>

</table>
    <br />
	<table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="0" width="100%" align="left">
   <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="15%" class="header" align="center">No Pendaftaran</td>
        <td width="30%" class="header" align="center">Nama</td>
        <td width="*" class="header" align="center">Keterangan</td>
    </tr>
<?php 	
	OpenDb();
    $sql = "SELECT nopendaftaran, nama, keterangan FROM calonsiswa WHERE idkelompok = '$replid' ";  
	$result = QueryDB($sql);
	$cnt = 0;
	while ($row = @mysql_fetch_array($result)) { ?>
    <tr height="25">    	
    	<td align="center"><?php echo++$cnt ?></td>
        <td align="center"><?php echo$row['nopendaftaran'] ?></td>        
        <td><?php echo$row['nama'] ?></td>
        <td><?php echo$row['keterangan']?></td>
<?php	}
	CloseDb(); ?>
    <!-- END TABLE CONTENT -->
    </table>
	</td>
</tr>
<tr>
	<td align="center">
 	<input type="button" name="Tutup" id="Tutup" value="Tutup" class="but" onClick="window.close()" />
    </td>
</tr>
</table>	
</body>
</html>