<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/db_functions.php');

header('Content-Type: application/vnd.ms-excel'); //IE and Opera  
header('Content-Type: application/x-msexcel'); // Other browsers  
header('Content-Disposition: attachment; filename=Data_Siswa.xls');
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$kelas=$_REQUEST[kelas];
$tahunajaran=$_REQUEST[tahunajaran];
$tingkat=$_REQUEST[tingkat];
$urut= $_REQUEST[urut];
$urutan = $_REQUEST[urutan];

OpenDb();
$sql="SELECT * FROM dbakademik.siswa s, dbakademik.kelas k, dbakademik.tahunajaran t WHERE s.idkelas = '$kelas' AND k.idtahunajaran = '$tahunajaran' AND k.idtingkat = '$tingkat' AND s.idkelas = k.replid AND t.replid = k.idtahunajaran AND s.alumni=0 ORDER BY $urut $urutan";
$result=QueryDb($sql);

if (@mysql_num_rows($result)<>0){
?>
<html>
<head>
<title>
Data Siswa per Kelas
</title>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
.style2 {font-size: 14px}
-->
</style>
</head>
<body>
<table width="700" border="0">
  <tr>
    <td>
    <table width="100%" border="0">
  <tr>
    <td colspan="2"><div align="center">Data Siswa per Kelas</div></td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
  <?php
  	$sql_TA="SELECT * FROM dbakademik.tahunajaran WHERE replid='$tahunajaran'"; 
	$result_TA=QueryDb($sql_TA);
	$row_TA=@mysql_fetch_array($result_TA);
  ?>
    <td width="9%">Departemen</td>
    <td width="91%"><strong>:</strong>&nbsp;<?php echo $row_TA['departemen']?></td>
  </tr>
  <tr>
    <td>Tahunajaran</td>
    <td><strong>:</strong>&nbsp;
    <?php echo $row_TA['tahunajaran'];
	?>    </td>
  </tr>
  <tr>
    <td>Tingkat</td>
    <td><strong>:</strong>&nbsp;<?php
	$sql_Tkt="SELECT * FROM dbakademik.tingkat WHERE replid='$tingkat'"; 
	$result_Tkt=QueryDb($sql_Tkt);
	$row_Tkt=@mysql_fetch_array($result_Tkt);
	echo $row_Tkt[tingkat];
	?></td>
  </tr>
  <tr>
    <td>Kelas</td>
    <td><strong>:</strong>&nbsp;<?php
	$sql_Kls="SELECT * FROM dbakademik.kelas WHERE replid='$kelas'"; 
	$result_Kls=QueryDb($sql_Kls);
	$row_Kls=@mysql_fetch_array($result_Kls);
	echo $row_Kls[kelas];
	?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

    </td>
  </tr>
  <tr>
    <td><table border="1">
<tr height="30">
<td width="3" valign="middle" bgcolor="#666666"><div align="center" class="style1">No.</div></td>
<td width="20" valign="middle" bgcolor="#666666"><div align="center" class="style1">NIS</div></td>
<td width="20" valign="middle" bgcolor="#666666"><div align="center" class="style1">NISN</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">PIN</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Nama</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Kelamin</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Tahun Masuk</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Asal Sekolah</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Tempat, Tanggal Lahir</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Alamat</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Kode Pos</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Telpon</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">HP</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Email</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Status</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Kondisi</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Kesehatan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Bahasa</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Suku</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Agama</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Warga</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Berat</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Tinggi</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Gol.Darah</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Anak Ke</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Bersaudara</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Ayah</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Email</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">PIN Ayah</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Pendidikan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Pekerjaan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Penghasilan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Ibu</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Email</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">PIN Ibu</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Pendidikan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Pekerjaan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Penghasilan</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Alamat</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">Telpon</div></td>
<td valign="middle" bgcolor="#666666"><div align="center" class="style1">HP</div></td>
</tr>
<?php
	$cnt=1;
	while ($row=@mysql_fetch_array($result)){
	?>
	<tr height="25">
	<td width="3" align="center"><?php echo $cnt?></td>
	<td align="left"><?php echo $row['nis']?></td>
    <td align="left"><?php echo $row['nisn']?></td>
   <td align="left"><?php echo $row['pinsiswa']?></td>
	<td align="left"><?php echo $row['nama']?></td>
	<td align="left"><?php echo $row['kelamin']?></td>
   <td align="left"><?php echo $row['tahunmasuk']?></td>
   <td align="left"><?php echo $row['asalsekolah']?></td>
	<td align="left"><?php echo $row['tmplahir']?>, <?php echo format_tgl($row['tgllahir'])?></td>
	<td align="left"><?php echo $row['alamatsiswa']?></td>
   <td align="left"><?php echo $row['kodepossiswa']?></td>
   <td align="left"><?php echo $row['telponsiswa']?></td>
   <td align="left"><?php echo $row['hpsiswa']?></td>
   <td align="left"><?php echo $row['emailsiswa']?></td>
   <td align="left"><?php echo $row['status']?></td> 
   <td align="left"><?php echo $row['kondisi']?></td> 
   <td align="left"><?php echo $row['kesehatan']?></td> 
   <td align="left"><?php echo $row['bahasa']?></td> 
   <td align="left"><?php echo $row['suku']?></td> 
   <td align="left"><?php echo $row['agama']?></td> 
   <td align="left"><?php echo $row['warga']?></td> 
   <td align="left"><?php echo $row['berat']?></td> 
   <td align="left"><?php echo $row['tinggi']?></td> 
   <td align="left"><?php echo $row['darah']?></td> 
   <td align="left"><?php echo $row['anakke']?></td> 
   <td align="left"><?php echo $row['jsaudara']?></td> 
   <td align="left"><?php echo $row['namaayah']?></td>
   <td align="left"><?php echo $row['emailayah']?></td>
   <td align="left"><?php echo $row['pinortu']?></td>  
   <td align="left"><?php echo $row['pendidikanayah']?></td> 
   <td align="left"><?php echo $row['pekerjaanayah']?></td> 
   <td align="left"><?php echo $row['penghasilanayah']?></td> 
   <td align="left"><?php echo $row['namaibu']?></td> 
   <td align="left"><?php echo $row['emailibu']?></td> 
   <td align="left"><?php echo $row['pinortuibu']?></td> 
   <td align="left"><?php echo $row['pendidikanibu']?></td> 
   <td align="left"><?php echo $row['pekerjaanibu']?></td> 
   <td align="left"><?php echo $row['penghasilanibu']?></td> 
   <td align="left"><?php echo $row['alamatortu']?></td> 
   <td align="left"><?php echo $row['telponortu']?></td> 
   <td align="left"><?php echo $row['hportu']?></td> 
	</tr>
	<?php
		$cnt++;
}
	?>
</table></td>
  </tr>
</table>


</body>
</html>
<?php
}
CloseDb();
?>