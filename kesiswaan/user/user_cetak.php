<?php
/**[N]**
 * LMS MAN Kota Blitar
 * 
 * 
 * @version: 1.0 (January 09, 2013)
 * 
 * 
 * Copyright (C)2016
 * 
 * 
 * 
 *
 *
 * 
 * 
 * 
 * **[N]**/ ?>
<?php
require_once('../include/errorhandler.php');
require_once('../include/db_functions.php');
require_once('../include/sessioninfo.php');
require_once('../include/common.php');
require_once('../include/config.php');
require_once('../include/getheader.php');
$varbaris = $_REQUEST['varbaris'];	
$page = $_REQUEST['page'];
$total = $_REQUEST['total'];
$urut = $_REQUEST['urut'];
$urutan = $_REQUEST['urutan'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="../style/style.css">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LMS MAN Kota Blitar[Cetak Pengguna]</title>
</head>
<body>

<table border="0"  cellpadding="5" width="100%">
<tr><td align="left" valign="top">

<?php echo GetHeader('yayasan')?>

<center><font size="4"><strong>DAFTAR PENGGUNA<br />SISTEM MANAJEMEN AKADEMIK</strong></font><br /> </center><br />
<br />
	
	 <table class="tab" id="table" border="1" cellpadding="2" style="border-collapse:collapse" cellspacing="0" width="100%" align="center" bordercolor="#000000">
    <tr height="30">
    	<td width="4%" class="header" align="center">No</td>
        <td width="10%" class="header" align="center">Login</td>
        <td width="20%" class="header" align="center">Nama</td>
        <td width="12%" class="header" align="center">Departemen</td>
        <td width="15%" class="header" align="center">Tingkat</td>
        <td width="10%" class="header" align="center">Status</td>
        <td width="*" class="header" align="center">Keterangan</td>
        <td width="16%" class="header" align="center">Login Terakhir</td>
    </tr>
<?php 	OpenDb();
	$sql = "SELECT h.login, h.replid,  h.tingkat, h.departemen, h.keterangan, p.nama, p.aktif,  DATE_FORMAT(h.lastlogin,'%Y-%m-%d') AS tanggal, TIME(h.lastlogin) as jam FROM $g_db_user.hakakses h, $g_db_pegawai.pegawai p, $g_db_user.login l WHERE h.modul='KESISWAAN' AND h.login = l.login AND l.login = p.nip ORDER BY $urut $urutan ";//LIMIT ".(int)$page*(int)$varbaris.",$varbaris";
	
	$result = QueryDB($sql);
	
	//if ($page==0)
		$cnt = 0;
	//else
		//$cnt = (int)$page*(int)$varbaris;
	
	while ($row = mysql_fetch_array($result)) { ?>
    <tr height="25">
    	<td align="center"><?php echo++$cnt ?></td>
        <td align="center"><?php echo$row['login'] ?></td>
       <td><?php echo$row['nama']; ?></td>
        <td align="center"><?	if ($row['tingkat']==1){
					echo "Semua Departemen";
				} else {
					echo $row['departemen'];
				}
			?>
       	</td>
        <td align="center">
			<?php	switch ($row['tingkat']){
					case 0: echo "Landlord";
						break;
					case 1: echo "Manajer Akademik";
						break;
					case 2: echo "Staf Akademik";
						break;
				}
        	?>
        </td>
        
        <td align="center"><?php if ($row['aktif'] == 1) echo 'Aktif'; else echo 'Tidak Aktif'; ?></td>
        <td><?php echo$row['keterangan'] ?></td>
        <td align="center"><?php echo format_tgl($row['tanggal'])?> <?php echo$row['jam']?></td>
    </tr>
<?php	} CloseDb() ?>	
    <!-- END TABLE CONTENT -->
    </table>
	</td>
</tr>
</table>
</body>
<script language="javascript">
window.print();
</script>
</html>